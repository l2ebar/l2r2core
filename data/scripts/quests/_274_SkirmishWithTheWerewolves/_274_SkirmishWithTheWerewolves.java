package quests._274_SkirmishWithTheWerewolves;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.base.Race;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _274_SkirmishWithTheWerewolves extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 274: Skirmish With The Werewolves");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	private static final int MARAKU_WEREWOLF_HEAD = 1477;
	private static final int NECKLACE_OF_VALOR = 1507;
	private static final int NECKLACE_OF_COURAGE = 1506;
	private static final int ADENA_ID = 57;
	private static final int MARAKU_WOLFMEN_TOTEM = 1501;

	public _274_SkirmishWithTheWerewolves()
	{
		super(274, -1);
		addStartNpc(30569);

		addKillId(20363);
		addKillId(20364);

		addQuestItem(MARAKU_WEREWOLF_HEAD);
		addQuestItem(MARAKU_WOLFMEN_TOTEM);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("30569-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int id = st.getState();
		int cond = st.getInt("cond");

		if(npcId != 30569)
			return htmltext;

		if(id == CREATED)
		{
			if(st.getPlayer().getRace() != Race.orc)
			{
				htmltext = "30569-00.htm";
				st.exitCurrentQuest(true);
			}
			else if(st.getPlayer().getLevel() < 9)
			{
				htmltext = "30569-01.htm";
				st.exitCurrentQuest(true);
			}
			else if(st.getQuestItemsCount(NECKLACE_OF_VALOR) > 0 || st.getQuestItemsCount(NECKLACE_OF_COURAGE) > 0)
			{
				htmltext = "30569-02.htm";
				return htmltext;
			}
			else
				htmltext = "30569-07.htm";
		}
		else

		if(cond == 1)
			if(st.getQuestItemsCount(MARAKU_WEREWOLF_HEAD) < 40)
				htmltext = "30569-04.htm";
			else
			{
				st.takeItems(MARAKU_WEREWOLF_HEAD, -1);
				st.giveItems(ADENA_ID, 3500, true);
				if(st.getQuestItemsCount(MARAKU_WOLFMEN_TOTEM) >= 1)
				{
					st.giveItems(ADENA_ID, st.getQuestItemsCount(MARAKU_WOLFMEN_TOTEM) * 600, true);
					st.takeItems(MARAKU_WOLFMEN_TOTEM, -1);
				}
				htmltext = "30569-05.htm";
				st.exitCurrentQuest(true);
				st.playSound(SOUND_FINISH);
			}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getInt("cond") == 1 && st.getQuestItemsCount(MARAKU_WEREWOLF_HEAD) < 40)
		{
			if(st.getQuestItemsCount(MARAKU_WEREWOLF_HEAD) < 39)
				st.playSound(SOUND_ITEMGET);
			else
				st.playSound(SOUND_MIDDLE);
			st.giveItems(MARAKU_WEREWOLF_HEAD, 1);
			if(Rnd.chance(5))
				st.giveItems(MARAKU_WOLFMEN_TOTEM, 1);
		}
		return null;
	}
}
