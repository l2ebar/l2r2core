// made by XoTTa6bI4
package quests._623_TheFinestFood;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _623_TheFinestFood extends Quest implements ScriptFile
{
	public final int JEREMY = 31521;

	public static final int HOT_SPRINGS_BUFFALO = 21315;
	public static final int HOT_SPRINGS_FLAVA = 21316;
	public static final int HOT_SPRINGS_ANTELOPE = 21318;

	public static final int ADENA = 57;
	public static final int LEAF_OF_FLAVA = 7199;
	public static final int BUFFALO_MEAT = 7200;
	public static final int ANTELOPE_HORN = 7201;
	public static final int[][] REWARDS = { { 6849, 25000, 0, 0, 1, 11 }, { 6847, 65000, 0, 0, 12, 23 }, { 6851, 25000, 0, 0, 24, 33 }, { 0, 73000, 230000, 18250, 34, 100 } };

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 623: The Finest Food");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _623_TheFinestFood()
	{
		super(623, 0);

		addStartNpc(JEREMY);

		addTalkId(JEREMY);

		addKillId(HOT_SPRINGS_BUFFALO);
		addKillId(HOT_SPRINGS_FLAVA);
		addKillId(HOT_SPRINGS_ANTELOPE);

		addQuestItem(BUFFALO_MEAT);
		addQuestItem(LEAF_OF_FLAVA);
		addQuestItem(ANTELOPE_HORN);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("31521-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("31521-04.htm"))
		{
			st.takeItems(LEAF_OF_FLAVA, -1);
			st.takeItems(BUFFALO_MEAT, -1);
			st.takeItems(ANTELOPE_HORN, -1);
			int random = Rnd.get(100) + 1;
			for(int i = 0; i < REWARDS.length; i++)
				if(REWARDS[i][4] <= random && random <= REWARDS[i][5])
				{
					st.giveItems(ADENA, REWARDS[i][1]);
					st.addExpAndSp(REWARDS[i][2], REWARDS[i][3]);
					if(REWARDS[i][0] != 0)
						st.giveItems(REWARDS[i][0], 3);
				}
			st.set("cond", "0");
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int id = st.getState();
		if(id == CREATED)
			st.set("cond", "0");
		// На случай любых ошибок, если предметы есть - квест все равно пройдется.
		if(summ(st) >= 300)
			st.set("cond", "2");
		int cond = st.getInt("cond");
		if(npcId == JEREMY)
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 71)
					htmltext = "31521-01.htm";
				else
				{
					htmltext = "31521-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1 && summ(st) < 300)
				htmltext = "31521-02r.htm";
			else if(cond == 2 && summ(st) >= 300)
				htmltext = "31521-03.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int cond = st.getInt("cond");
		int npcId = npc.getNpcId();
		if(cond == 1) // Like off C4 PTS AI (убрали && Rnd.chance(50))
			if(npcId == HOT_SPRINGS_BUFFALO)
			{
				if(st.getQuestItemsCount(BUFFALO_MEAT) < 100)
				{
					st.giveItems(BUFFALO_MEAT, 1);
					if(st.getQuestItemsCount(BUFFALO_MEAT) == 100)
					{
						if(summ(st) >= 300)
							st.set("cond", "2");
						st.playSound(SOUND_MIDDLE);
					}
					else
						st.playSound(SOUND_ITEMGET);
				}
			}
			else if(npcId == HOT_SPRINGS_FLAVA)
			{
				if(st.getQuestItemsCount(LEAF_OF_FLAVA) < 100)
				{
					st.giveItems(LEAF_OF_FLAVA, 1);
					if(st.getQuestItemsCount(LEAF_OF_FLAVA) == 100)
					{
						if(summ(st) >= 300)
							st.set("cond", "2");
						st.playSound(SOUND_MIDDLE);
					}
					else
						st.playSound(SOUND_ITEMGET);
				}
			}
			else if(npcId == HOT_SPRINGS_ANTELOPE)
				if(st.getQuestItemsCount(ANTELOPE_HORN) < 100)
				{
					st.giveItems(ANTELOPE_HORN, 1);
					if(st.getQuestItemsCount(ANTELOPE_HORN) == 100)
					{
						if(summ(st) >= 300)
							st.set("cond", "2");
						st.playSound(SOUND_MIDDLE);
					}
					else
						st.playSound(SOUND_ITEMGET);
				}
		return null;
	}

	private long summ(QuestState st)
	{
		return st.getQuestItemsCount(LEAF_OF_FLAVA) + st.getQuestItemsCount(BUFFALO_MEAT) + st.getQuestItemsCount(ANTELOPE_HORN);
	}
}
