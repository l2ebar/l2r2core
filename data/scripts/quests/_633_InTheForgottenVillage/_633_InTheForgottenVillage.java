package quests._633_InTheForgottenVillage;

import java.util.HashMap;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _633_InTheForgottenVillage extends Quest implements ScriptFile
{
	// NPC
	private static int MINA = 31388;
	// ITEMS
	private static int RIB_BONE = 7544;
	private static int Z_LIVER = 7545;

	// Mobid : DROP CHANCES
	private static HashMap<Integer, Double> DAMOBS = new HashMap<Integer, Double>();
	private static HashMap<Integer, Double> UNDEADS = new HashMap<Integer, Double>();

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _633_InTheForgottenVillage()
	{
		super(633, 0);

		DAMOBS.put(21557, 32.8); // Bone Snatcher
		DAMOBS.put(21558, 32.8); // Bone Snatcher
		DAMOBS.put(21559, 33.7); // Bone Maker
		DAMOBS.put(21560, 33.7); // Bone Shaper
		DAMOBS.put(21563, 34.2); // Bone Collector
		DAMOBS.put(21564, 34.8); // Skull Collector
		DAMOBS.put(21565, 35.1); // Bone Animator
		DAMOBS.put(21566, 35.9); // Skull Animator
		DAMOBS.put(21567, 35.9); // Bone Slayer
		DAMOBS.put(21572, 36.5); // Bone Sweeper
		DAMOBS.put(21574, 38.3); // Bone Grinder
		DAMOBS.put(21575, 38.3); // Bone Grinder
		DAMOBS.put(21580, 38.5); // Bone Caster
		DAMOBS.put(21581, 39.5); // Bone Puppeteer
		DAMOBS.put(21583, 39.7); // Bone Scavenger
		DAMOBS.put(21584, 40.1); // Bone Scavenger

		UNDEADS.put(21553, 34.7); // Trampled Man
		UNDEADS.put(21554, 34.7); // Trampled Man
		UNDEADS.put(21561, 45.0); // Sacrificed Man
		UNDEADS.put(21578, 50.1); // Behemoth Zombie
		UNDEADS.put(21596, 35.9); // Requiem Lord
		UNDEADS.put(21597, 37.0); // Requiem Behemoth
		UNDEADS.put(21598, 44.1); // Requiem Behemoth
		UNDEADS.put(21599, 39.5); // Requiem Priest
		UNDEADS.put(21600, 40.8); // Requiem Behemoth
		UNDEADS.put(21601, 41.1); // Requiem Behemoth

		addStartNpc(MINA);
		addQuestItem(RIB_BONE);

		for(int i : UNDEADS.keySet())
			addKillId(i);

		for(int i : DAMOBS.keySet())
			addKillId(i);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("accept"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			htmltext = "31388-04.htm";
		}
		if(event.equalsIgnoreCase("quit"))
		{
			st.takeItems(RIB_BONE, -1);
			st.playSound(SOUND_FINISH);
			htmltext = "31388-10.htm";
			st.exitCurrentQuest(true);
		}
		else if(event.equalsIgnoreCase("stay"))
			htmltext = "31388-07.htm";
		else if(event.equalsIgnoreCase("reward"))
			if(st.getInt("cond") == 2)
				if(st.getQuestItemsCount(RIB_BONE) >= 200)
				{
					st.takeItems(RIB_BONE, -1);
					st.giveItems(57, 25000);
					st.addExpAndSp(305235, 0);
					st.playSound(SOUND_FINISH);
					st.set("cond", "1");
					htmltext = "31388-08.htm";
				}
				else
					htmltext = "31388-09.htm";
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		int id = st.getState();
		if(npcId == MINA)
			if(id == CREATED)
			{
				if(st.getPlayer().getLevel() > 64)
					htmltext = "31388-01.htm";
				else
				{
					htmltext = "31388-03.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "31388-06.htm";
			else if(cond == 2)
				htmltext = "31388-05.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(UNDEADS.containsKey(npcId))
			st.rollAndGive(Z_LIVER, 1, UNDEADS.get(npcId));
		else if(DAMOBS.containsKey(npcId))
		{
			long count = st.getQuestItemsCount(RIB_BONE);
			if(count < 200 && Rnd.chance(DAMOBS.get(npcId)))
			{
				st.giveItems(RIB_BONE, 1);
				if(count >= 199)
				{
					st.set("cond", "2");
					st.playSound(SOUND_MIDDLE);
				}
				else
					st.playSound(SOUND_ITEMGET);
			}
		}
		return null;
	}
}
