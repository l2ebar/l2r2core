package quests._293_HiddenVein;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _293_HiddenVein extends Quest implements ScriptFile
{
	private static String qnTutorial = "_293_HiddenVein";

	// NPCs
	private static int Filaur = 30535;
	private static int Chichirin = 30539;
	// Mobs
	private static int Utuku_Orc = 20446;
	private static int Utuku_Orc_Archer = 20447;
	private static int Utuku_Orc_Grunt = 20448;
	// Items
	private static int ADENA = 57;
	private static int Soulshot__No_Grade_for_Beginners = 5789;
	private static int Spiritshot__No_Grade_for_Beginners = 5790;
	// Quest Items
	private static int Chrysolite_Ore = 1488;
	private static int Torn_Map_Fragment = 1489;
	private static int Hidden_Ore_Map = 1490;
	// Chances
	private static int Torn_Map_Fragment_Chance = 5;
	private static int Chrysolite_Ore_Chance = 45;

	public _293_HiddenVein()
	{
		super(293, -1);
		addStartNpc(Filaur);
		addTalkId(Chichirin);
		addKillId(Utuku_Orc);
		addKillId(Utuku_Orc_Archer);
		addKillId(Utuku_Orc_Grunt);
		addQuestItem(Chrysolite_Ore);
		addQuestItem(Torn_Map_Fragment);
		addQuestItem(Hidden_Ore_Map);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		int _state = st.getState();
		if(event.equalsIgnoreCase("30535-03.htm") && _state == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30535-06.htm") && _state == STARTED)
		{
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		else if(event.equalsIgnoreCase("30539-03.htm") && _state == STARTED)
		{
			if(st.getQuestItemsCount(Torn_Map_Fragment) < 4)
				return "30539-02.htm";
			st.takeItems(Torn_Map_Fragment, 4);
			st.giveItems(Hidden_Ore_Map, 1);
		}

		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int _state = st.getState();
		int npcId = npc.getNpcId();

		if(_state == CREATED)
		{
			if(npcId != Filaur)
				return "noquest";
			if(st.getPlayer().getRace().ordinal() != 4)
			{
				st.exitCurrentQuest(true);
				return "30535-00.htm";
			}
			if(st.getPlayer().getLevel() < 6)
			{
				st.exitCurrentQuest(true);
				return "30535-01.htm";
			}
			st.set("cond", "0");
			return "30535-02.htm";
		}

		if(_state != STARTED)
			return "noquest";

		if(npcId == Filaur)
		{
			long Chrysolite_Ore_count = st.getQuestItemsCount(Chrysolite_Ore);
			long Hidden_Ore_Map_count = st.getQuestItemsCount(Hidden_Ore_Map);
			long reward = st.getQuestItemsCount(Chrysolite_Ore) * 10 + st.getQuestItemsCount(Hidden_Ore_Map) * 1000;
			if(reward == 0)
				return "30535-04.htm";

			if(Chrysolite_Ore_count > 0)
				st.takeItems(Chrysolite_Ore, -1);
			if(Hidden_Ore_Map_count > 0)
				st.takeItems(Hidden_Ore_Map, -1);
			st.giveItems(ADENA, reward);
			QuestState tutorial = st.getPlayer().getQuestState(qnTutorial);
			if(tutorial != null && tutorial.getInt("Ex") != 10)
			{
				st.showQuestionMark(26);
				tutorial.set("Ex", "10");
				if(st.getPlayer().getClassId().isMage())
				{
					st.giveItems(Spiritshot__No_Grade_for_Beginners, 3000);
					st.playTutorialVoice("tutorial_voice_027");
				}
				else
				{
					st.giveItems(Soulshot__No_Grade_for_Beginners, 3000);
					st.playTutorialVoice("tutorial_voice_026");
				}
			}
			return Chrysolite_Ore_count > 0 && Hidden_Ore_Map_count > 0 ? "30535-09.htm" : Hidden_Ore_Map_count > 0 ? "30535-08.htm" : "30535-05.htm";
		}

		if(npcId == Chichirin)
			return "30539-01.htm";

		return "noquest";
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		if(qs.getState() != STARTED)
			return null;

		if(Rnd.chance(Torn_Map_Fragment_Chance))
		{
			qs.giveItems(Torn_Map_Fragment, 1);
			qs.playSound(SOUND_ITEMGET);
		}
		else if(Rnd.chance(Chrysolite_Ore_Chance))
		{
			qs.giveItems(Chrysolite_Ore, 1);
			qs.playSound(SOUND_ITEMGET);
		}

		return null;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 293: Hidden Vein");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
