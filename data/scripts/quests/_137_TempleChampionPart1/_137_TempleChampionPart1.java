package quests._137_TempleChampionPart1;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Квест Temple Champion Part 1
 */
public class _137_TempleChampionPart1 extends Quest implements ScriptFile
{
	// NPC
	private final static int SYLVAIN = 30070;

	// ITEMS
	private final static int FRAGMENT = 10340;
	private final static int BADGE_TEMPLE_EXECUTOR = 10334;
	private final static int BADGE_TEMPLE_MISSIONARY = 10339;
	private final static int ADENA = 57;

	// MONSTER
	private final static int[] mobs = { 20083, 20144, 20199, 20200, 20201, 20202 };

	// MINLEVEL
	private final static int MINLEVEL = 35;

	// MAXLEVEL
	private final static int MAXLEVEL = 40;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _137_TempleChampionPart1()
	{
		super(137, 0);

		addStartNpc(SYLVAIN);
		addTalkId(SYLVAIN);
		addKillId(mobs);
		addQuestItem(FRAGMENT);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30070-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.set("talk", "0");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30070-05.htm"))
			st.set("talk", "1");
		else if(event.equalsIgnoreCase("30070-06.htm"))
			st.set("talk", "2");
		else if(event.equalsIgnoreCase("30070-08.htm"))
		{
			st.unset("talk");
			st.set("cond", "2");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30070-16.htm"))
		{
			st.takeItems(BADGE_TEMPLE_EXECUTOR, -1);
			st.takeItems(BADGE_TEMPLE_MISSIONARY, -1);
			st.giveItems(ADENA, 69146);
			st.playSound(SOUND_FINISH);
			st.unset("talk");
			st.unset("cond");
			st.exitCurrentQuest(false);
			if(st.getPlayer().getLevel() >= MINLEVEL && st.getPlayer().getLevel() <= MAXLEVEL)
				st.addExpAndSp(219975, 13047);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "30070-00.htm";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == SYLVAIN)
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= MINLEVEL && st.getQuestItemsCount(BADGE_TEMPLE_EXECUTOR) > 0 && st.getQuestItemsCount(BADGE_TEMPLE_MISSIONARY) > 0 && st.getPlayer().getLevel() <= MAXLEVEL)
					htmltext = "30070-01.htm";
				else
				{
					htmltext = "30070-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
			{
				if(st.getInt("talk") == 0)
					htmltext = "30070-03.htm";
				else if(st.getInt("talk") == 1)
					htmltext = "30070-05.htm";
				else if(st.getInt("talk") == 2)
					htmltext = "30070-06.htm";
			}
			else if(cond == 2)
				htmltext = "30070-08.htm";
			else if(cond == 3 && st.getQuestItemsCount(FRAGMENT) >= 30)
			{
				htmltext = "30070-09.htm";
				st.set("talk", "1");
				st.takeItems(FRAGMENT, -1);
			}
			else if(cond == 3 && st.getInt("talk") == 1)
				htmltext = "30070-10.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int cond = st.getInt("cond");
		if(cond == 2)
			if(st.getQuestItemsCount(FRAGMENT) < 30)
			{
				st.giveItems(FRAGMENT, 1);
				if(st.getQuestItemsCount(FRAGMENT) >= 30)
				{
					st.set("cond", "3");
					st.setState(STARTED);
					st.playSound(SOUND_MIDDLE);
				}
				else
					st.playSound("SOUND_ITEMGET");
			}
		return null;
	}
}
