package quests._625_TheFinestIngredientsPart2;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Spawn;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Rnd;

public class _625_TheFinestIngredientsPart2 extends Quest implements ScriptFile
{
	// NPCs
	private static int Jeremy = 31521;
	private static int Yetis_Table = 31542;
	// Mobs
	private static int RB_Icicle_Emperor_Bumbalump = 25296;
	// Items
	private static short Soy_Sauce_Jar = 7205;
	private static short Food_for_Bumbalump = 7209;
	private static short Special_Yeti_Meat = 7210;
	private static short Reward_First = 4589;
	private static short Reward_Last = 4594;

	public _625_TheFinestIngredientsPart2()
	{
		super(625, 1);
		addStartNpc(Jeremy);
		addTalkId(Yetis_Table);
		addKillId(RB_Icicle_Emperor_Bumbalump);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		int _state = st.getState();
		int cond = st.getInt("cond");
		if(event.equalsIgnoreCase("31521-02.htm") && _state == CREATED)
		{
			if(st.getQuestItemsCount(Soy_Sauce_Jar) == 0)
			{
				st.exitCurrentQuest(true);
				return "31521-00a.htm";
			}
			st.setState(STARTED);
			st.set("cond", "1");
			st.takeItems(Soy_Sauce_Jar, 1);
			st.giveItems(Food_for_Bumbalump, 1);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("31521-04.htm") && _state == STARTED && cond == 3)
		{
			st.exitCurrentQuest(true);
			if(st.getQuestItemsCount(Special_Yeti_Meat) == 0)
				return "31521-05.htm";
			st.takeItems(Special_Yeti_Meat, 1);
			st.takeItems(57, 23452);
			st.giveItems(Rnd.get(Reward_First, Reward_Last), 5, true);
		}
		else if(event.equalsIgnoreCase("31542-02.htm") && _state == STARTED && cond == 1)
		{
			if(st.getQuestItemsCount(Food_for_Bumbalump) == 0)
				return "31542-04.htm";
			if(BumbalumpSpawned())
				return "31542-03.htm";
			st.takeItems(Food_for_Bumbalump, 1);
			st.set("cond", "2");
			L2GameThreadPools.getInstance().scheduleGeneral(new BumbalumpSpawner(), 1000);
		}

		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int _state = st.getState();
		int npcId = npc.getNpcId();
		if(_state == CREATED)
		{
			if(npcId != Jeremy)
				return "noquest";
			if(st.getPlayer().getLevel() < 73)
			{
				st.exitCurrentQuest(true);
				return "31683-00b.htm";
			}
			if(st.getQuestItemsCount(Soy_Sauce_Jar) == 0)
			{
				st.exitCurrentQuest(true);
				return "31521-00a.htm";
			}
			st.set("cond", "0");
			return "31521-01.htm";
		}

		if(_state != STARTED)
			return "noquest";
		int cond = st.getInt("cond");

		if(npcId == Jeremy)
		{
			if(cond == 1)
				return "31521-02a.htm";
			if(cond == 2)
				return "31521-03a.htm";
			if(cond == 3)
				return "31521-03.htm";
		}

		if(npcId == Yetis_Table)
		{
			if(cond == 1)
				return "31542-01.htm";
			if(cond == 2)
			{
				if(BumbalumpSpawned())
					return "31542-03.htm";
				L2GameThreadPools.getInstance().scheduleGeneral(new BumbalumpSpawner(), 1000);
				return "31542-02.htm";
			}
			if(cond == 3)
				return "31542-05.htm";
		}

		return "noquest";
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getState() != STARTED || npc.getNpcId() != RB_Icicle_Emperor_Bumbalump)
			return null;

		if(st.getInt("cond") == 1 || st.getInt("cond") == 2)
		{
			if(st.getQuestItemsCount(Food_for_Bumbalump) > 0)
				st.takeItems(Food_for_Bumbalump, 1);
			st.giveItems(Special_Yeti_Meat, 1);
			st.set("cond", "3");
			st.playSound(SOUND_MIDDLE);
		}

		return null;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 625: The Finest Ingredients - Part 2");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	private static boolean BumbalumpSpawned()
	{
		return L2ObjectsStorage.getByNpcId(RB_Icicle_Emperor_Bumbalump) != null;
	}

	public class BumbalumpSpawner implements Runnable
	{
		private L2Spawn _spawn = null;
		private int tiks = 0;

		public BumbalumpSpawner()
		{
			if(BumbalumpSpawned())
				return;
			L2NpcTemplate template = NpcTable.getTemplate(RB_Icicle_Emperor_Bumbalump);
			if(template == null)
				return;
			try
			{
				_spawn = new L2Spawn(template);
			}
			catch(Exception E)
			{
				return;
			}
			_spawn.setLocx(158240);
			_spawn.setLocy(-121536);
			_spawn.setLocz(-2253);
			_spawn.setHeading(Rnd.get(0, 0xFFFF));
			_spawn.doSpawn(true);
			_spawn.stopRespawn();
		}

		public void Say(String _test)
		{
			for(L2NpcInstance _npc : _spawn.getAllSpawned())
				Functions.npcShout(_npc, _test);
		}

		@Override
		public void run()
		{
			if(_spawn == null)
				return;
			if(tiks == 0)
				Say("I will crush you!");
			if(tiks < 1200 && BumbalumpSpawned())
			{
				tiks++;
				if(tiks == 1200)
					Say("May the gods forever condemn you! Your power weakens!");
				L2GameThreadPools.getInstance().scheduleGeneral(this, 1000);
				return;
			}
			_spawn.despawnAll();
		}
	}

}
