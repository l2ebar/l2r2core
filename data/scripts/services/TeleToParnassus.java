package services;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.listeners.L2ZoneEnterLeaveListener;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Spawn;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.NpcTable;
import l2n.game.tables.ReflectionTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;

public class TeleToParnassus extends Functions implements ScriptFile
{
	private static GArray<L2Spawn> _spawns = new GArray<L2Spawn>();

	private L2Zone _zone = ZoneManager.getInstance().getZoneById(ZoneType.offshore, 500032, false);
	private ZoneListener _zoneListener = new ZoneListener();

	@Override
	public void onLoad()
	{
		if(Config.SERVICES_PARNASSUS_ENABLED)
		{
			try
			{
				ReflectionTable.getInstance().get(ReflectionTable.PARNASSUS, true).setCoreLoc(new Location(149384, 171896, -952));

				L2Spawn sp1 = new L2Spawn(NpcTable.getTemplate(30086));
				sp1.setLocx(149960);
				sp1.setLocy(174136);
				sp1.setLocz(-920);
				sp1.setAmount(1);
				sp1.setHeading(32768);
				sp1.setRespawnDelay(5);
				sp1.init();
				sp1.getAllSpawned().iterator().next().setReflection(ReflectionTable.PARNASSUS);
				_spawns.add(sp1);

				L2Spawn sp2 = new L2Spawn(NpcTable.getTemplate(30839));
				sp2.setLocx(149368);
				sp2.setLocy(174264);
				sp2.setLocz(-896);
				sp2.setAmount(1);
				sp2.setHeading(49152);
				sp2.setRespawnDelay(5);
				sp2.init();
				sp2.getAllSpawned().iterator().next().setReflection(ReflectionTable.PARNASSUS);
				_spawns.add(sp2);

				L2NpcTemplate t = NpcTable.getTemplate(36394);
				t.displayId = 36394;
				t.title = "Gatekeeper";
				t.ai_type = "npc";
				L2Spawn sp3 = new L2Spawn(t);
				sp3.setLocx(149368);
				sp3.setLocy(172568);
				sp3.setLocz(-952);
				sp3.setAmount(1);
				sp3.setHeading(49152);
				sp3.setRespawnDelay(5);
				sp3.init();
				sp3.getAllSpawned().iterator().next().setReflection(ReflectionTable.PARNASSUS);
				_spawns.add(sp3);

				L2Spawn sp6 = new L2Spawn(NpcTable.getTemplate(30300));
				sp6.setLocx(148760);
				sp6.setLocy(174136);
				sp6.setLocz(-920);
				sp6.setAmount(1);
				sp6.setHeading(0);
				sp6.setRespawnDelay(5);
				sp6.init();
				sp6.getAllSpawned().iterator().next().setReflection(ReflectionTable.PARNASSUS);
				_spawns.add(sp6);

				L2Spawn sp7 = new L2Spawn(NpcTable.getTemplate(32320));
				sp7.setLocx(149368);
				sp7.setLocy(173064);
				sp7.setLocz(-952);
				sp7.setAmount(1);
				sp7.setHeading(16384);
				sp7.setRespawnDelay(5);
				sp7.init();
				sp7.getAllSpawned().iterator().next().setReflection(ReflectionTable.PARNASSUS);
				_spawns.add(sp7);
			}
			catch(SecurityException e)
			{
				e.printStackTrace();
			}
			catch(ClassNotFoundException e)
			{
				e.printStackTrace();
			}

			_zone.getListenerEngine().addMethodInvokedListener(_zoneListener);

			ZoneManager.getInstance().getZoneById(ZoneType.peace_zone, 500031, false).setActive(true);
			ZoneManager.getInstance().getZoneById(ZoneType.offshore, 500032, false).setActive(true);
			ZoneManager.getInstance().getZoneById(ZoneType.dummy, 500033, false).setActive(true);

			System.out.println("Loaded Service: Teleport to Parnassus");
		}
	}

	@Override
	public void onReload()
	{
		_zone.getListenerEngine().removeMethodInvokedListener(_zoneListener);
		for(L2Spawn spawn : _spawns)
			spawn.despawnAll();
		_spawns.clear();
	}

	@Override
	public void onShutdown()
	{}

	public void toParnassus()
	{
		L2Player player = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		if(!L2NpcInstance.canBypassCheck(player, npc))
			return;

		if(player.getAdena() < Config.SERVICES_PARNASSUS_PRICE)
		{
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			return;
		}

		player.reduceAdena(Config.SERVICES_PARNASSUS_PRICE, true);
		player.setVar("backCoords", player.getLoc().toXYZString());
		player.teleToLocation(_zone.getSpawn().rnd(30, 200, false), ReflectionTable.PARNASSUS);
	}

	public void fromParnassus()
	{
		L2Player player = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		if(!L2NpcInstance.canBypassCheck(player, npc))
			return;

		String var = player.getVar("backCoords");
		if(var == null || var.equals(""))
		{
			teleOut();
			return;
		}
		player.teleToLocation(new Location(var), 0);
	}

	public void teleOut()
	{
		L2Player player = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;
		player.teleToLocation(46776, 185784, -3528, 0);
		show(player.isLangRus() ? "Я не знаю, как Вы попали сюда, но я могу Вас отправить за ограждение." : "I don't know from where you came here, but I can teleport you the another border side.", player);
	}

	public String DialogAppend_30059(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30080(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30177(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30233(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30256(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30320(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30848(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30878(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30899(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_31210(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_31275(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_31320(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_31964(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30006(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30134(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30146(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_32163(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30576(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30540(Integer val)
	{
		return getHtmlAppends(val);
	}

	private static final String en = "<br1>[scripts_services.TeleToParnassus:toParnassus @811;Parnassus|\"Move to Parnassus (offshore zone) - " + Config.SERVICES_PARNASSUS_PRICE + " Adena.\"]<br1>";
	private static final String ru = "<br1>[scripts_services.TeleToParnassus:toParnassus @811;Parnassus|\"Парнасус (торговая зона без налогов) - " + Config.SERVICES_PARNASSUS_PRICE + " Аден.\"]<br1>";

	public String getHtmlAppends(Integer val)
	{
		if(val != 0 || !Config.SERVICES_PARNASSUS_ENABLED)
			return "";
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return "";
		return player.isLangRus() ? ru : en;
	}

	public String DialogAppend_36394(Integer val)
	{
		return getHtmlAppends2(val);
	}

	private static final String en2 = "<br>[scripts_services.ManaRegen:DoManaRegen|Full MP Regeneration. (1 MP for 5 Adena)]<br1>[scripts_services.TeleToParnassus:fromParnassus @811;From Parnassus|\"Exit the Parnassus.\"]<br1>";
	private static final String ru2 = "<br>[scripts_services.ManaRegen:DoManaRegen|Полное восстановление MP. (1 MP за 5 Аден)]<br1>[scripts_services.TeleToParnassus:fromParnassus @811;From Parnassus|\"Покинуть Парнасус.\"]<br1>";

	public String getHtmlAppends2(Integer val)
	{
		if(val != 0 || !Config.SERVICES_PARNASSUS_ENABLED)
			return "";
		L2Player player = (L2Player) getSelf();
		if(player == null || player.getReflection().getId() != ReflectionTable.PARNASSUS)
			return "";
		return player.isLangRus() ? ru2 : en2;
	}

	public class ZoneListener extends L2ZoneEnterLeaveListener
	{
		@Override
		public void objectEntered(L2Zone zone, L2Character character)
		{

		}

		@Override
		public void objectLeaved(L2Zone zone, L2Character character)
		{
			L2Player player = character.getPlayer();
			if(Config.SERVICES_PARNASSUS_ENABLED && player.getReflection().getId() == ReflectionTable.PARNASSUS && player.isVisible())
			{
				L2Playable playable = (L2Playable) character;

				playable.setReflection(0);
			}
		}
	}
}
