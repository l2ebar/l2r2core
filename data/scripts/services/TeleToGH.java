package services;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.listeners.L2ZoneEnterLeaveListener;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Spawn;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.tables.NpcTable;
import l2n.game.tables.ReflectionTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;

public class TeleToGH extends Functions implements ScriptFile
{
	private static GArray<L2Spawn> _spawns = new GArray<L2Spawn>();

	private L2Zone _zone = ZoneManager.getInstance().getZoneById(ZoneType.offshore, 500014, false);
	private ZoneListener _zoneListener = new ZoneListener();

	@Override
	public void onLoad()
	{
		if(Config.SERVICES_GIRAN_HARBOR_ENABLED)
		{
			try
			{
				ReflectionTable.getInstance().get(ReflectionTable.GH, true).setCoreLoc(new Location(47416, 186568, -3480));

				L2Spawn sp1 = new L2Spawn(NpcTable.getTemplate(30086));
				sp1.setLocx(48059);
				sp1.setLocy(186791);
				sp1.setLocz(-3512);
				sp1.setAmount(1);
				sp1.setHeading(42000);
				sp1.setRespawnDelay(5);
				sp1.init();
				sp1.getAllSpawned().iterator().next().setReflection(ReflectionTable.GH);
				_spawns.add(sp1);

				L2Spawn sp2 = new L2Spawn(NpcTable.getTemplate(30081));
				sp2.setLocx(48146);
				sp2.setLocy(186753);
				sp2.setLocz(-3512);
				sp2.setAmount(1);
				sp2.setHeading(42000);
				sp2.setRespawnDelay(5);
				sp2.init();
				sp2.getAllSpawned().iterator().next().setReflection(ReflectionTable.GH);
				_spawns.add(sp2);

				L2NpcTemplate t = NpcTable.getTemplate(36394);
				t.displayId = 36394;
				t.title = "Gatekeeper";
				t.type = "L2Merchant";
				t.ai_type = "npc";
				L2Spawn sp3 = new L2Spawn(t);
				sp3.setLocx(47984);
				sp3.setLocy(186832);
				sp3.setLocz(-3445);
				sp3.setAmount(1);
				sp3.setHeading(42000);
				sp3.setRespawnDelay(5);
				sp3.init();
				sp3.getAllSpawned().iterator().next().setReflection(ReflectionTable.GH);
				_spawns.add(sp3);


				L2Spawn sp6 = new L2Spawn(NpcTable.getTemplate(30300));
				sp6.setLocx(48102);
				sp6.setLocy(186772);
				sp6.setLocz(-3512);
				sp6.setAmount(1);
				sp6.setHeading(42000);
				sp6.setRespawnDelay(5);
				sp6.init();
				sp6.getAllSpawned().iterator().next().setReflection(ReflectionTable.GH);
				_spawns.add(sp6);

				L2Spawn sp7 = new L2Spawn(NpcTable.getTemplate(32320));
				sp7.setLocx(47772);
				sp7.setLocy(186905);
				sp7.setLocz(-3480);
				sp7.setAmount(1);
				sp7.setHeading(42000);
				sp7.setRespawnDelay(5);
				sp7.init();
				sp7.getAllSpawned().iterator().next().setReflection(ReflectionTable.GH);
				_spawns.add(sp7);

				L2Spawn sp8 = new L2Spawn(NpcTable.getTemplate(32320));
				sp8.setLocx(46360);
				sp8.setLocy(187672);
				sp8.setLocz(-3480);
				sp8.setAmount(1);
				sp8.setHeading(42000);
				sp8.setRespawnDelay(5);
				sp8.init();
				sp8.getAllSpawned().iterator().next().setReflection(ReflectionTable.GH);
				_spawns.add(sp8);

				L2Spawn sp9 = new L2Spawn(NpcTable.getTemplate(32320));
				sp9.setLocx(49016);
				sp9.setLocy(185960);
				sp9.setLocz(-3480);
				sp9.setAmount(1);
				sp9.setHeading(42000);
				sp9.setRespawnDelay(5);
				sp9.init();
				sp9.getAllSpawned().iterator().next().setReflection(ReflectionTable.GH);
				_spawns.add(sp9);
			}
			catch(SecurityException e)
			{
				e.printStackTrace();
			}
			catch(ClassNotFoundException e)
			{
				e.printStackTrace();
			}

			_zone.getListenerEngine().addMethodInvokedListener(_zoneListener);

			ZoneManager.getInstance().getZoneById(ZoneType.offshore, 500014, false).setActive(true);
			ZoneManager.getInstance().getZoneById(ZoneType.peace_zone, 500023, false).setActive(true);
			ZoneManager.getInstance().getZoneById(ZoneType.dummy, 500024, false).setActive(true);

			System.out.println("Loaded Service: Teleport to Giran Harbor");
		}
	}

	@Override
	public void onReload()
	{
		if(Config.SERVICES_GIRAN_HARBOR_ENABLED)
		{
			try
			{
				_zone.getListenerEngine().removeMethodInvokedListener(_zoneListener);
				for(L2Spawn spawn : _spawns)
					spawn.despawnAll();
				_spawns.clear();
			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());
			}
		}
	}

	@Override
	public void onShutdown()
	{}

	public void toGH()
	{
		L2Player player = (L2Player) getSelf();

		if(!checkCondition(player))
			return;

		player.setVar("backCoords", player.getLoc().toXYZString());
		player.teleToLocation(_zone.getSpawn().rnd(30, 200, false), ReflectionTable.GH);
	}

	public void fromGH()
	{
		L2Player player = (L2Player) getSelf();

		if(!checkCondition(player))
			return;

		String var = player.getVar("backCoords");
		if(var == null || var.equals(""))
		{
			teleOut(player);
			return;
		}

		player.teleToLocation(new Location(var), 0);
	}

	public void teleOut(L2Player player)
	{
		player.teleToLocation(46776, 185784, -3528, 0);

		if(player.getVar("lang@").equalsIgnoreCase("en"))
			show("I don't know from where you came here, but I can teleport you the another border side.", player);
		else
			show("Я не знаю, как Вы попали сюда, но я могу Вас отправить за ограждение.", player);
	}

	public boolean checkCondition(L2Player player)
	{
		return !(player.isActionsDisabled() || player.isSitting());
	}

	public String DialogAppend_30059(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30080(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30177(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30233(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30256(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30320(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30848(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30878(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30899(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_31210(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_31275(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_31320(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_31964(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30006(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30134(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30146(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_32163(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30576(Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_30540(Integer val)
	{
		return getHtmlAppends(val);
	}

	private static final String en = "<br1>[scripts_services.TeleToGH:toGH @811;Giran Harbor|\"I want free admision to the Giran Harbor.\"]<br1>";
	private static final String ru = "<br1>[scripts_services.TeleToGH:toGH @811;Giran Harbor|\"Я хочу бесплатно попасть в Гиран Харбор.\"]<br1>";

	public String getHtmlAppends(Integer val)
	{
		if(val != 0 || !Config.SERVICES_GIRAN_HARBOR_ENABLED)
			return "";
		if(((L2Player) getSelf()).getVar("lang@").equalsIgnoreCase("ru"))
			return ru;
		return en;
	}

	public String DialogAppend_36394(Integer val)
	{
		return getHtmlAppends2(val);
	}

	private static final String en2 = "<br>[scripts_services.ManaRegen:DoManaRegen|Full MP Regeneration. (1 MP for 5 Adena)]<br1>[scripts_services.TeleToGH:fromGH @811;From Giran Harbor|\"Exit the Giran Harbor.\"]<br1>";
	private static final String ru2 = "<br>[scripts_services.ManaRegen:DoManaRegen|Полное восстановление MP. (1 MP за 5 Адена)]<br1>[scripts_services.TeleToGH:fromGH @811;From Giran Harbor|\"Покинуть Гиран Харбор.\"]<br1>";

	public String getHtmlAppends2(Integer val)
	{
		if(val != 0 || !Config.SERVICES_GIRAN_HARBOR_ENABLED)
			return "";
		L2Player player = (L2Player) getSelf();
		if(player == null || player.getReflection().getId() != -2)
			return "";
		return player.isLangRus() ? ru2 : en2;
	}

	public class ZoneListener extends L2ZoneEnterLeaveListener
	{
		@Override
		public void objectEntered(L2Zone zone, L2Character character)
		{

		}

		@Override
		public void objectLeaved(L2Zone zone, L2Character character)
		{
			if(!character.isPlayable())
				return;

			L2Player player = character.getPlayer();
			if(player == null)
				return;

			if(Config.SERVICES_GIRAN_HARBOR_ENABLED && player.getReflection().getId() == ReflectionTable.GH)
			{
				L2Playable playable = (L2Playable) character;

				playable.setReflection(0);
			}
		}
	}
}
