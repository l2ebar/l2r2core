package events.christmas;

import java.util.Calendar;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.instancemanager.ServerVariables;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.tables.SkillTable;

public class NewYearTimer implements ScriptFile
{
	private static NewYearTimer instance;

	public static NewYearTimer getInstance()
	{
		if(instance == null)
			new NewYearTimer();
		return instance;
	}

	public NewYearTimer()
	{
		if(instance != null)
			return;

		instance = this;

		if(!isActive())
			return;

		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, 2008);
		c.set(Calendar.MONTH, Calendar.JANUARY);
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);

		while (getDelay(c) < 0)
			c.set(Calendar.YEAR, c.get(Calendar.YEAR) + 1);

		L2GameThreadPools.getInstance().scheduleGeneral(new NewYearAnnouncer("С Новым, " + c.get(Calendar.YEAR) + ", Годом!!!"), getDelay(c));
		c.add(Calendar.SECOND, -1);
		L2GameThreadPools.getInstance().scheduleGeneral(new NewYearAnnouncer("1"), getDelay(c));
		c.add(Calendar.SECOND, -1);
		L2GameThreadPools.getInstance().scheduleGeneral(new NewYearAnnouncer("2"), getDelay(c));
		c.add(Calendar.SECOND, -1);
		L2GameThreadPools.getInstance().scheduleGeneral(new NewYearAnnouncer("3"), getDelay(c));
		c.add(Calendar.SECOND, -1);
		L2GameThreadPools.getInstance().scheduleGeneral(new NewYearAnnouncer("4"), getDelay(c));
		c.add(Calendar.SECOND, -1);
		L2GameThreadPools.getInstance().scheduleGeneral(new NewYearAnnouncer("5"), getDelay(c));
	}

	private long getDelay(Calendar c)
	{
		return c.getTime().getTime() - System.currentTimeMillis();
	}

	/**
	 * Вызывается при загрузке классов скриптов
	 */
	@Override
	public void onLoad()
	{}

	/**
	 * Вызывается при перезагрузке
	 * После перезагрузки onLoad() вызывается автоматически
	 */
	@Override
	public void onReload()
	{}

	/**
	 * Читает статус эвента из базы.
	 * 
	 * @return
	 */
	private static boolean isActive()
	{
		return ServerVariables.getString("Christmas", "off").equalsIgnoreCase("on");
	}

	/**
	 * Вызывается при выключении сервера
	 */
	@Override
	public void onShutdown()
	{}

	private class NewYearAnnouncer implements Runnable
	{
		private final String message;

		private NewYearAnnouncer(String message)
		{
			this.message = message;
		}

		@Override
		public void run()
		{
			Announcements.announceToAll(message);

			// Через жопу сделано, но не суть важно :)
			if(message.length() == 1)
				return;

			for(L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
			{
				L2Skill skill = SkillTable.getInstance().getInfo(3266, 1);
				MagicSkillUse msu = new MagicSkillUse(player, player, 3266, 1, skill.getHitTime(), 0);
				player.broadcastPacket(msu);
			}

			instance = null;
			new NewYearTimer();
		}
	}
}
