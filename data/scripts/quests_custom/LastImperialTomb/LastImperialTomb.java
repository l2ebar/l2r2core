package quests_custom.LastImperialTomb;

import l2n.Config;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.instancemanager.boss.LastImperialTombManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class LastImperialTomb extends Quest implements ScriptFile
{
	private static final int GUIDE = 32011;
	private static final int FRINTEZZA = 29045;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Custom Quest: Last Imperial Tomb");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public LastImperialTomb()
	{
		super(-1, "LastImperialTomb", "Last Imperial Tomb", 1);
		addStartNpc(GUIDE);
		addTalkId(FRINTEZZA);
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		L2Player player = st.getPlayer();

		if(npcId == GUIDE) 
		{
			if(player.isFlying())
				return "<html><body>Imperial Tomb Guide:<br>To enter, get off the wyvern.</body></html>";
			if(Config.LIT_REGISTRATION_MODE == 0)
			{
				if(LastImperialTombManager.getInstance().tryRegistrationCc(player))
					LastImperialTombManager.getInstance().registration(player, npc);
			}
			else if(Config.LIT_REGISTRATION_MODE == 1)
			{
				if(LastImperialTombManager.getInstance().tryRegistrationPt(player))
					LastImperialTombManager.getInstance().registration(player, npc);
			}
			else if(Config.LIT_REGISTRATION_MODE == 2)
			{
				if(LastImperialTombManager.getInstance().tryRegistrationPc(player))
					LastImperialTombManager.getInstance().registration(player, npc);
			}
		}
		return null;
	}
}
