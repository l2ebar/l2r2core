package actions;

import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2DoorInstance;
import l2n.util.Files;

public final class ActionOnDoor extends EventScript implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Script: " + getClass().getSimpleName());
		addEventId(ScriptEventType.ON_ACTION);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	@Override
	public boolean onAction(final L2Player player, final L2Object object, final boolean shift)
	{
		if(!OnActionShift.checkCondition(player, object, shift) || !player.getPlayerAccess().Door || !object.isDoor())
			return false;

		String dialog;
		final L2DoorInstance door = (L2DoorInstance) object;
		dialog = Files.read("data/scripts/actions/admin.L2DoorInstance.onActionShift.htm");
		dialog = dialog.replaceFirst("%CurrentHp%", String.valueOf(door.getCurrentHp()));
		dialog = dialog.replaceFirst("%MaxHp%", String.valueOf(door.getMaxHp()));
		dialog = dialog.replaceFirst("%ObjectId%", String.valueOf(door.getObjectId()));
		dialog = dialog.replaceFirst("%doorId%", String.valueOf(door.getDoorId()));
		dialog = dialog.replaceFirst("%pdef%", String.valueOf(door.getPDef(null)));
		dialog = dialog.replaceFirst("%mdef%", String.valueOf(door.getMDef(null, null)));

		dialog = dialog.replaceFirst("%attackeble%", String.valueOf(door.isAttackable(player)));
		dialog = dialog.replaceFirst("%unlock%", String.valueOf(door.isUnlockable()));
		dialog = dialog.replaceFirst("%siegeWeapon%", String.valueOf(door.isSiegeWeaponOnlyAttackable()));
		dialog = dialog.replaceFirst("%key%", String.valueOf(door.key));

		dialog = dialog.replaceFirst("%dis%", String.valueOf((int) player.getDistance(door)));

		dialog = dialog.replaceFirst("%geo_open%", String.valueOf(door.geoOpen));
		dialog = dialog.replaceFirst("%is_open%", String.valueOf(door.isOpen()));
		dialog = dialog.replaceFirst("%geoData%", String.valueOf(door.getGeodata()));
		dialog = dialog.replaceFirst("%invul%", String.valueOf(door.isInvul()));

		dialog = dialog.replaceFirst("bypass -h admin_open", "bypass -h admin_open " + door.getDoorId());
		dialog = dialog.replaceFirst("bypass -h admin_close", "bypass -h admin_close " + door.getDoorId());

		show(dialog, player);

		return true;
	}
}
