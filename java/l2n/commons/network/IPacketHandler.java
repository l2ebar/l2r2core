package l2n.commons.network;

import java.nio.ByteBuffer;

public interface IPacketHandler<T extends MMOClient<T, RP, SP>, RP extends ReceivablePacket<T, RP, SP>, SP extends SendablePacket<T, RP, SP>>
{
	public RP handlePacket(ByteBuffer buf, T client, final int opcode);
}
