package l2n.commons.util;

import l2n.commons.list.GArray;
import l2n.util.ArrayUtil;

import java.util.Calendar;

import static java.util.Calendar.*;

public class Crontab
{
	private static final int MINUTES_PER_HOUR = 59;
	private static final int HOURES_PER_DAY = 23;
	private static final int DAYS_PER_WEEK = 7;
	private static final int MONTHS_PER_YEAR = 12;
	private static final int DAYS_PER_MONTH = 31;

	private static final int MINUTESMIN = 0;
	private static final int HOURESMIN = 0;
	private static final int DAYSMIN = 1;
	private static final int MONTHSMIN = 1;
	private static final int DAYSPERWEEKMIN = 0;

	private GArray<Integer> _Minutes = new GArray<Integer>();
	private GArray<Integer> _Hours = new GArray<Integer>();
	private GArray<Integer> _DaysInMonth = new GArray<Integer>();
	private GArray<Integer> _Month = new GArray<Integer>();
	private GArray<Integer> _DaysInWeek = new GArray<Integer>();
	private String _configLine = "";

	public Crontab(String line)
	{
		_configLine = line;
		String[] params = _configLine.split(" ");
		_Minutes = parseRangeParam(params[0], MINUTES_PER_HOUR, MINUTESMIN);
		_Hours = parseRangeParam(params[1], HOURES_PER_DAY, HOURESMIN);
		_DaysInMonth = parseRangeParam(params[2], DAYS_PER_MONTH, DAYSMIN);
		_Month = parseRangeParam(params[3], MONTHS_PER_YEAR, MONTHSMIN);
		_DaysInWeek = parseRangeParam(params[4], DAYS_PER_WEEK, DAYSPERWEEKMIN);
	}

	private static GArray<Integer> parseRangeParam(String param, int timelength, int minlength)
	{
		String[] paramarray;
		if(param.indexOf(",") != -1)
			paramarray = param.split(",");
		else
			paramarray = new String[] { param };
		StringBuffer rangeitems = new StringBuffer();
		for(int i = 0; i < paramarray.length; ++i)
			if(paramarray[i].indexOf("/") != -1)
			{
				for(int a = 1; a <= timelength; ++a)
					if(a % Integer.parseInt(paramarray[i].substring(paramarray[i].indexOf("/") + 1)) == 0)
						if(a == timelength)
							rangeitems.append(minlength + ",");
						else
							rangeitems.append(a + ",");
			}
			else if(paramarray[i].equals("*"))
				rangeitems.append(fillRange(minlength + "-" + timelength));
			else
				rangeitems.append(fillRange(paramarray[i]));
		int[] val = ArrayUtil.toIntArray(rangeitems.toString(), ",");
		GArray<Integer> result = new GArray<Integer>();
		for(int i = 0; i < val.length; ++i)
			result.add(val[i]);
		return result;
	}

	private static String fillRange(String range)
	{
		if(range.indexOf("-") == -1)
			return range + ",";
		String[] rangearray = range.split("-");
		StringBuffer result = new StringBuffer();
		for(int i = Integer.parseInt(rangearray[0]); i <= Integer.parseInt(rangearray[1]); ++i)
			result.append(i + ",");
		return result.toString();
	}

	public boolean canRunAt(Calendar cal)
	{
		int month = cal.get(MONTH) + 1;
		int day = cal.get(DAY_OF_MONTH);
		int dayOfWeek = cal.get(DAY_OF_WEEK);
		int hour = cal.get(HOUR_OF_DAY);
		int minute = cal.get(MINUTE);

		return !_Minutes.contains(minute) || !_Hours.contains(hour) || !_DaysInMonth.contains(day) || !_Month.contains(month) || !_DaysInWeek.contains(dayOfWeek);
	}

	public boolean canRunNow()
	{
		return canRunAt(Calendar.getInstance());
	}

	public long timeNextUsage(long time)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		cal.set(SECOND, 0);
		cal.set(MILLISECOND, 0);

		cal.set(MINUTE, brute(_Minutes, cal.get(MINUTE), 0, MINUTES_PER_HOUR));

		if(cal.getTimeInMillis() < time)
			cal.add(HOUR, 1);

		cal.set(HOUR_OF_DAY, brute(_Hours, cal.get(HOUR_OF_DAY), 0, HOURES_PER_DAY));

		if(cal.getTimeInMillis() < time)
			cal.add(HOUR, 24);

		cal.set(DAY_OF_MONTH, brute(_DaysInMonth, cal.get(DAY_OF_MONTH), 1, DAYS_PER_MONTH));

		if(cal.getTimeInMillis() < time)
			cal.add(DAY_OF_YEAR, 31);

		cal.set(MONTH, brute(_Month, cal.get(MONTH) + 1, 1, MONTHS_PER_YEAR) - 1);

		if(cal.getTimeInMillis() < time)
			cal.add(DAY_OF_YEAR, 31);

		cal.set(DAY_OF_WEEK, brute(_DaysInWeek, cal.get(DAY_OF_WEEK), 0, DAYS_PER_WEEK));

		if(cal.getTimeInMillis() < time)
			cal.add(DAY_OF_YEAR, 7);

		if(cal.getTimeInMillis() < time)
			cal.add(HOUR, 24);

		return cal.getTimeInMillis();
	}

	private int brute(GArray<Integer> arr, int cur, int min, int max)
	{
		boolean antiloop = true;
		int start_value = cur;
		int next = cur;
		while (antiloop)
		{
			if(arr.contains(next))
				return next;
			if(next < max)
				next++;
			else
				next = min;
			if(start_value == next)
				antiloop = false;
		}

		return -1;
	}
}
