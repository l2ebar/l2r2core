package l2n.commons.util;

import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class LoggerObject
{
	public void error(final String st, final Exception e)
	{
		getLogger().log(Level.SEVERE, st, e);
	}

	public void error(final String st)
	{
		getLogger().log(Level.SEVERE, st);
	}

	public void warn(final String st, final Exception e)
	{
		getLogger().log(Level.WARNING, st, e);
	}

	public void warn(final String st)
	{
		getLogger().log(Level.WARNING, st);
	}

	public void info(final String st, final Exception e)
	{
		getLogger().log(Level.INFO, st, e);
	}

	public void info(final String st)
	{
		getLogger().log(Level.INFO, st);
	}

	protected abstract Logger getLogger();
}
