package l2n.commons.lang.reference;

import l2n.game.model.L2Object;

public class AbstractHardReference<T extends L2Object> implements IHardReference<T>
{
	private volatile T reference;

	public AbstractHardReference(final T reference)
	{
		this.reference = reference;
	}

	@Override
	public T get()
	{
		return reference;
	}

	@Override
	public void clear()
	{
		reference = null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(final Object o)
	{
		if(o == this)
			return true;
		if(o == null)
			return false;
		if(!(o instanceof AbstractHardReference))
			return false;
		if(((AbstractHardReference) o).get() == null)
			return false;
		return ((AbstractHardReference) o).get().equals(get());
	}
}
