package l2n.commons.listener;



import java.util.Collection;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

public class ListenerList<T extends Listener>
{
	protected Set<Listener<T>> listeners = new CopyOnWriteArraySet<>();
	protected CopyOnWriteArrayList<T> listeners2 =  new CopyOnWriteArrayList<T>();
	public ListenerList(final boolean isEmpty)
	{
		listeners2 = isEmpty ? null : new CopyOnWriteArrayList<T>();
	}
	public ListenerList(){

	}
	public Collection<Listener<T>> getListeners2()
	{
		return listeners;
	}
	public Object[] getListeners()
	{
		if(listeners2 == null){
			return (listeners2 = new CopyOnWriteArrayList<T>()).toArray();
		}
		return listeners2.toArray();
	}
	public boolean addListener(final T listener)
	{
		return listeners2.addIfAbsent(listener);
	}
	public boolean add(Listener<T> listener)
	{
		return listeners.add(listener);
	}
	public boolean removeListener(final T listener2)
	{
		return listeners2.remove(listener2);
	}

	public boolean isEmpty()
	{
		return listeners2.isEmpty();
	}


	public boolean remove(Listener<T> listener)
	{
		return listeners.remove(listener);
	}

}
