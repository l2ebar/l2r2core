package l2n.game.network.serverpackets;

import l2n.Config;
import l2n.game.model.actor.L2Player;
import l2n.game.skills.Stats;

public class ExStorageMaxCount extends L2GameServerPacket
{
	private static final String _S__FE_2F_EXSTORAGEMAXCOUNT = "[S] FE:2f ExStorageMaxCount";
	private final int _inventory;
	private final int _warehouse;
	private final int _clan;
	private int _privateSell;
	private final int _privateBuy;
	private final int _recipeDwarven;
	private final int _recipeCommon;
	private final int _inventoryExtraSlots;

	public ExStorageMaxCount(final L2Player player)
	{
		_inventory = player.getInventoryLimit();
		_warehouse = player.getWarehouseLimit();
		_clan = player.getClan() == null ? 0 : player.getClan().getWhBonus() + Config.WAREHOUSE_SLOTS_CLAN;
		_privateBuy = _privateSell = player.getTradeLimit();
		_recipeDwarven = player.getDwarvenRecipeLimit();
		_recipeCommon = player.getCommonRecipeLimit();
		_inventoryExtraSlots = (int) player.calcStat(Stats.INVENTORY_LIMIT, 0, null, null);
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x2f);

		writeD(_inventory);
		writeD(_warehouse);
		writeD(_clan);
		writeD(_privateSell);
		writeD(_privateBuy);
		writeD(_recipeDwarven);
		writeD(_recipeCommon);
		writeD(_inventoryExtraSlots); // belt inventory slots increase count
	}

	@Override
	public String getType()
	{
		return _S__FE_2F_EXSTORAGEMAXCOUNT;
	}
}
