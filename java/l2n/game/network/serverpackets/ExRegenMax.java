package l2n.game.network.serverpackets;

public class ExRegenMax extends L2GameServerPacket
{
	private final double _max;
	private final int _count;
	private final int _time;

	public ExRegenMax(final double max, final int count, final int time)
	{
		_max = max;
		_count = count;
		_time = time;
	}

	public static final short POTION_HEALING_GREATER = 16457;
	public static final short POTION_HEALING_MEDIUM = 16440;
	public static final short POTION_HEALING_LESSER = 16416;

	/**
	 * Пример пакета - Пришло после использования Healing Potion (инфа для Interlude, в Kamael пакет не изменился)
	 * FE 01 00 01 00 00 00 0F 00 00 00 03 00 00 00 00 00 00 00 00 00 38 40 // Healing Potion
	 * FE 01 00 01 00 00 00 0F 00 00 00 03 00 00 00 00 00 00 00 00 00 49 40 // Greater Healing Potion
	 * FE 01 00 01 00 00 00 0F 00 00 00 03 00 00 00 00 00 00 00 00 00 20 40 // Lesser Healing Potion
	 * FE - тип
	 * 01 00 - субтип
	 * 01 00 00 00 - хз что
	 * 0F 00 00 00 - хз что
	 * 03 00 00 00 - хз что
	 * 00 00 00 00 - хз что
	 * 00 00 - хз что
	 * 38 40 - хз что
	 */
	// dddf
	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x01);
		writeD(1);
		writeD(_count);
		writeD(_time);
		writeF(_max);
	}

	@Override
	public String getType()
	{
		return "[S] FE:01 ExRegenMax";
	}
}
