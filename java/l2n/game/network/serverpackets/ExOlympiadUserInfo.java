package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

public class ExOlympiadUserInfo extends L2GameServerPacket
{
	// cdSddddd
	private static final String _S__FE_29_OLYMPIADUSERINFO = "[S] FE:7A ExOlympiadUserInfo";
	private final int _side;
	private final int class_id;
	private final int curHp;
	private final int maxHp;
	private final int curCp;
	private final int maxCp;
	private int obj_id = 0;
	private final String _name;

	/**
	 * @param _player
	 * @param _side
	 *            (1 = right, 2 = left)
	 */
	public ExOlympiadUserInfo(final L2Player player, final int side)
	{
		_side = side;
		obj_id = player.getObjectId();
		class_id = player.getClassId().getId();
		_name = player.getName();
		curHp = (int) player.getCurrentHp();
		maxHp = player.getMaxHp();
		curCp = (int) player.getCurrentCp();
		maxCp = player.getMaxCp();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xfe);
		writeH(0x7a);
		writeC(_side);
		writeD(obj_id);
		writeS(_name);
		writeD(class_id);
		writeD(curHp);
		writeD(maxHp);
		writeD(curCp);
		writeD(maxCp);
	}

	@Override
	public String getType()
	{
		return _S__FE_29_OLYMPIADUSERINFO;
	}
}
