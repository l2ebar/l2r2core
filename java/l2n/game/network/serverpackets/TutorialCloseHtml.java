package l2n.game.network.serverpackets;

public class TutorialCloseHtml extends L2GameServerPacket
{
	@Override
	protected final void writeImpl()
	{
		writeC(0xa9);
	}

	@Override
	public String getType()
	{
		return "[S] a9 TutorialCloseHtml";
	}
}
