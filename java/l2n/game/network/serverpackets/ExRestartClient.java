package l2n.game.network.serverpackets;

public class ExRestartClient extends L2GameServerPacket
{
	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x48);
	}

	@Override
	public String getType()
	{
		return "[S] FE:48 ExRestartClient";
	}
}
