package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;

/**
 * sample
 * 0000: 85 02 00 10 04 00 00 01 00 4b 02 00 00 2c 04 00 .........K...,..
 * 0010: 00 01 00 58 02 00 00 ...X...
 * format h (dhd)
 * 
 * @version $Revision: 1.3.2.1.2.6 $ $Date: 2005/04/05 19:41:08 $
 */
public class AbnormalStatusUpdate extends L2GameServerPacket
{
	private static final String _S__85_ABNORMALSTATUSUPDATE = "[S] 85 AbnormalStatusUpdate";

	public static final int INFINITIVE_EFFECT = -1;
	private final GArray<int[]> _effects;

	public AbnormalStatusUpdate()
	{
		_effects = new GArray<int[]>();
	}

	public void addEffect(final int skillId, final int skillLevel, final int duration)
	{
		switch (skillId)
		{
			case 2031:
			case 2032:
			case 2037:
			case 26025:
			case 26026:
				break;
			default:
				_effects.add(new int[] { skillId, skillLevel, duration });
				break;
		}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x85);
		writeH(_effects.size());
		for(final int[] effect : _effects)
		{
			writeD(effect[0]);
			writeH(effect[1]);
			writeD(effect[2]);
		}
	}

	@Override
	public String getType()
	{
		return _S__85_ABNORMALSTATUSUPDATE;
	}
}
