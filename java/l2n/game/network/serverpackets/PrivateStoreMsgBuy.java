package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

public class PrivateStoreMsgBuy extends L2GameServerPacket
{
	private static final String _S__BF_PRIVATESTOREMSGBUY = "[S] BF PrivateStoreMsgBuy";
	private final int char_obj_id;
	private final String store_name;

	/**
	 * Название личного магазина покупки
	 * 
	 * @param player
	 */
	public PrivateStoreMsgBuy(final L2Player player)
	{
		char_obj_id = player.getObjectId();
		store_name = player.getTradeList() == null ? "" : player.getTradeList().getBuyStoreName();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xBF);
		writeD(char_obj_id);
		writeS(store_name);
	}

	@Override
	public String getType()
	{
		return _S__BF_PRIVATESTOREMSGBUY;
	}
}
