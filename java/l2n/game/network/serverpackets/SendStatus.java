package l2n.game.network.serverpackets;

import l2n.Config;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

import java.util.logging.Logger;

/**
 * @author L2System Project
 * @project L2_NextGen
 * @time 12:35:41
 * @date 07.04.2010
 */
public class SendStatus extends L2GameServerPacket
{
	protected static Logger _log = Logger.getLogger(SendStatus.class.getName());
	private static final String _S__00_SENDSTATUS = "[S] 00 SendStatus";

	private static final long MIN_UPDATE_PERIOD = 30000;
	private static int online_players = 0;
	private static int max_online_players = 0;
	private static int online_priv_store = 0;
	private static int offline_priv_store = 0;
	private static long last_update = 0;

	public SendStatus()
	{
		if(System.currentTimeMillis() - last_update < MIN_UPDATE_PERIOD)
			return;

		last_update = System.currentTimeMillis();

		int realOnline = 0;
		int trade = 0;
		int offlineTrade = 0;
		for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
		{
			if(!player.isInOfflineMode())
				realOnline++;
			if(player.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE && !player.isInOfflineMode())
				trade++;
			else if(player.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE && player.isInOfflineMode())
				offlineTrade++;
		}

		online_priv_store = trade;
		offline_priv_store = offlineTrade;

		switch (Config.ONLINE_COUNT_MODE)
		{
			case 1: // Реальный онлайн (с офф.торговцами), без накрутки онлайна
				online_players += offline_priv_store;
				online_priv_store += offline_priv_store;
				break;
			case 2:// Реальный онлайн (без офф.торговцев), без накрутки онлайна
				break;
			case 3:// Реальный онлайн (с офф.торговцами), с накруткой онлайна
				online_players += offline_priv_store + Config.ONLINE_ADD_COUNT;
				online_priv_store += offline_priv_store;
				break;
		}

		max_online_players = Math.max(max_online_players, online_players);
	}

	@Override
	protected final void writeImpl()
	{
		// Проверки рейтинга типа l2top
		_log.info("STATUS: Status request recieved from " + getClient().getIpAddr());

		writeC(0x2e); // Packet ID
		writeD(0x01); // World ID
		writeD(max_online_players);
		writeD(online_players + 2);
		writeD(online_players);
		writeD(online_priv_store);

		// TRASH DATA
		writeH(48);
		writeH(44);
		writeH(53);
		writeH(49);
		writeH(48);
		writeH(44);
		writeH(55);
		writeH(55);
		writeH(55);
		writeH(53);
		writeH(56);
		writeH(44);
		writeH(54);
		writeH(53);
		writeH(48);
		writeD(54);
		writeD(119);
		writeD(183);
		writeQ(159L);
		writeD(0);
		writeH(65);
		writeH(117);
		writeH(103);
		writeH(32);
		writeH(50);
		writeH(57);
		writeH(32);
		writeH(50);
		writeH(48);
		writeH(48);
		writeD(57);
		writeH(48);
		writeH(50);
		writeH(58);
		writeH(52);
		writeH(48);
		writeH(58);
		writeH(52);
		writeD(51);
		writeD(87);
		writeC(17);
		writeC(93);
		writeC(31);
		writeC(96);
	}

	@Override
	public String getType()
	{
		return _S__00_SENDSTATUS;
	}
}
