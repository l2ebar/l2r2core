package l2n.game.network.serverpackets;

public class NormalCamera extends L2GameServerPacket
{
	private static final String _S__d7_NORMALCAMERA = "[S] d7 NormalCamera";

	@Override
	protected final void writeImpl()
	{
		writeC(0xd7);
	}

	@Override
	public String getType()
	{
		return _S__d7_NORMALCAMERA;
	}
}
