package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.templates.L2Item;

import java.util.Set;
import java.util.TreeSet;

/**
 * @author L2System Project
 * @date 06.07.2010
 * @time 17:40:20
 */
public class ExReplyPostItemList extends L2GameServerPacket
{
	private final Set<L2ItemInstance> _itemslist = new TreeSet<L2ItemInstance>(Inventory.orderComparator);

	public ExReplyPostItemList(final L2Player cha)
	{
		if(!cha.getPlayerAccess().UseTrade)
			return;

		final String tradeBan = cha.getVar("tradeBan");
		if(tradeBan != null && (tradeBan.equals("-1") || Long.parseLong(tradeBan) >= System.currentTimeMillis()))
			return;

		for(final L2ItemInstance item : cha.getInventory().getItems())
			if(item != null && item.canBeTraded(cha))
				_itemslist.add(item);
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0xB2);

		writeD(_itemslist.size());
		for(final L2ItemInstance temp : _itemslist)
		{
			final L2Item item = temp.getItem();
			writeD(temp.getObjectId());
			writeD(temp.getItemId());
			writeQ(temp.getCount());
			writeH(item.getType2ForPackets());
			writeH(temp.getCustomType1());
			writeD(temp.getBodyPart());
			writeH(temp.getEnchantLevel());
			writeH(temp.getCustomType2());

			writeItemElements(temp);

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}
	}

	@Override
	public String getType()
	{
		return "[S] FE:B2 ExReplyPostItemList";
	}
}
