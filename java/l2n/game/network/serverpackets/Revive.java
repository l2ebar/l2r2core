package l2n.game.network.serverpackets;

import l2n.game.model.L2Object;

/**
 * sample
 * 0000: 0c 9b da 12 40 ....@
 * format d
 */
public class Revive extends L2GameServerPacket
{
	private static final String _S__01_REVIVE = "[S] 01 Revive";
	private final int _objectId;

	public Revive(final L2Object obj)
	{
		_objectId = obj.getObjectId();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x01);
		writeD(_objectId);
	}

	@Override
	public String getType()
	{
		return _S__01_REVIVE;
	}

}
