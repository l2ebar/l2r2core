package l2n.game.network.serverpackets;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.CharSelectInfoPackage;
import l2n.game.network.L2GameClient;
import l2n.game.tables.CharTemplateTable;
import l2n.game.templates.L2PlayerTemplate;

import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CharacterSelectionInfo extends L2GameServerPacket
{
	// d (SdSddddddddddffdQdddddddddddddddddddddddddddddddddddddddffdddchhd)
	private static final String _S__09_CHARSELECTINFO = "[S] 09 CharSelectInfo";

	private static Logger _log = Logger.getLogger(CharacterSelectionInfo.class.getName());

	private final String _loginName;
	private final int _sessionId;
	private final CharSelectInfoPackage[] _characterPackages;

	public CharacterSelectionInfo(final String loginName, final int sessionId)
	{
		_sessionId = sessionId;
		_loginName = loginName;
		_characterPackages = loadCharacterSelectInfo(loginName);
	}

	public CharSelectInfoPackage[] getCharInfo()
	{
		return _characterPackages;
	}

	@Override
	protected final void writeImpl()
	{
		final int size = _characterPackages != null ? _characterPackages.length : 0;

		writeC(0x09);
		writeD(size);
		writeD(0x07); // Говорят это макс. к-во чаров на аккаунте
		writeC(0x00); // Kamael разрешает УлУ запрещает созданУе Угроков

		long lastAccess = 0L;
		int lastUsed = -1;
		for(int i = 0; i < size; i++)
			if(lastAccess < _characterPackages[i].getLastAccess())
			{
				lastAccess = _characterPackages[i].getLastAccess();
				lastUsed = i;
			}

		for(int i = 0; i < size; i++)
		{
			final CharSelectInfoPackage charInfoPackage = _characterPackages[i];

			writeS(charInfoPackage.getName());
			writeD(charInfoPackage.getCharId()); // ?
			writeS(_loginName);
			writeD(_sessionId);
			writeD(charInfoPackage.getClanId());
			writeD(0x00); // ??

			writeD(charInfoPackage.getSex());
			writeD(charInfoPackage.getRace());
			writeD(charInfoPackage.getClassId());
			writeD(0x01); // active ??

			writeD(charInfoPackage.getX()); // x
			writeD(charInfoPackage.getY()); // y
			writeD(charInfoPackage.getZ()); // z

			writeF(charInfoPackage.getCurrentHp()); // hp cur
			writeF(charInfoPackage.getCurrentMp()); // mp cur

			writeD(charInfoPackage.getSp());
			writeQ(charInfoPackage.getExp());
			writeD(charInfoPackage.getLevel());
			writeD(charInfoPackage.getKarma()); // karma

			writeD(charInfoPackage.getPKKills()); // pk kills
			writeD(charInfoPackage.getPVPKills()); // pvp kills
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);

			for(final byte PAPERDOLL_ID : UserInfo.PAPERDOLL_ORDER)
				writeD(charInfoPackage.getPaperdollItemId(PAPERDOLL_ID));

			writeD(charInfoPackage.getHairStyle());
			writeD(charInfoPackage.getHairColor());
			writeD(charInfoPackage.getFace());

			writeF(charInfoPackage.getMaxHp()); // hp max
			writeF(charInfoPackage.getMaxMp()); // mp max

			writeD(charInfoPackage.getAccessLevel() > -100 ? charInfoPackage.getDeleteTimer() : -1);
			writeD(charInfoPackage.getClassId());
			writeD(i == lastUsed ? 1 : 0);
			writeC(Math.min(charInfoPackage.getEnchantEffect(), 127));

			writeD(0x00); // augmentation id (можно 0)
			writeD(0x00); // transform id тут всегда 0 на экране выбора чара
		}
	}

	public static CharSelectInfoPackage[] loadCharacterSelectInfo(final String loginName)
	{
		final GArray<CharSelectInfoPackage> characterList = new GArray<CharSelectInfoPackage>();

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet pl_rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM characters AS c LEFT JOIN character_subclasses AS cs ON (c.obj_Id = cs.char_obj_id AND cs.isBase = 1) WHERE account_name=? LIMIT 7");
			statement.setString(1, loginName);
			pl_rset = statement.executeQuery();

			while (pl_rset.next())// fills the package
			{
				final CharSelectInfoPackage charInfopackage = restoreChar(pl_rset, pl_rset);
				if(charInfopackage != null)
					characterList.add(charInfopackage);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "could not restore charinfo:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, pl_rset);
		}

		return characterList.toArray(new CharSelectInfoPackage[characterList.size()]);
	}

	private static CharSelectInfoPackage restoreChar(final ResultSet chardata, final ResultSet charclass)
	{
		CharSelectInfoPackage charInfopackage = null;
		try
		{
			final int classid = charclass.getInt("class_id");
			final boolean female = chardata.getInt("sex") == 1;
			final L2PlayerTemplate templ = CharTemplateTable.getInstance().getTemplate(classid, female);
			final int objectId = chardata.getInt("obj_Id");
			final String name = chardata.getString("char_name");
			charInfopackage = new CharSelectInfoPackage(objectId, name);
			charInfopackage.setLevel(charclass.getInt("level"));
			charInfopackage.setMaxHp(charclass.getInt("maxHp"));
			charInfopackage.setCurrentHp(charclass.getDouble("curHp"));
			charInfopackage.setMaxMp(charclass.getInt("maxMp"));
			charInfopackage.setCurrentMp(charclass.getDouble("curMp"));

			charInfopackage.setFace(chardata.getInt("face"));
			charInfopackage.setHairStyle(chardata.getInt("hairstyle"));
			charInfopackage.setHairColor(chardata.getInt("haircolor"));
			charInfopackage.setSex(female ? 1 : 0);

			charInfopackage.setExp(charclass.getLong("exp"));
			charInfopackage.setSp(charclass.getInt("sp"));
			charInfopackage.setClanId(chardata.getInt("clanid"));

			charInfopackage.setKarma(chardata.getInt("karma"));
			charInfopackage.setRace(templ.race.ordinal());
			charInfopackage.setClassId(classid);
			long deletetime = chardata.getLong("deletetime");
			int deletedays = 0;
			if(Config.DELETE_DAYS > 0)
				if(deletetime > 0)
				{
					deletetime = (int) (System.currentTimeMillis() / 1000 - deletetime);
					deletedays = (int) (deletetime / 3600 / 24);
					if(deletedays >= Config.DELETE_DAYS)
					{
						L2GameClient.deleteFromClan(objectId, charInfopackage.getClanId());
						L2GameClient.deleteCharByObjId(objectId);
						return null;
					}
					deletetime = Config.DELETE_DAYS * 3600 * 24 - deletetime;
				}
				else
					deletetime = 0;
			charInfopackage.setDeleteTimer((int) deletetime);
			charInfopackage.setLastAccess(chardata.getLong("lastAccess") * 1000);
			charInfopackage.setAccessLevel(chardata.getInt("accesslevel"));

			charInfopackage.setXYZ(chardata.getInt("x"), chardata.getInt("y"), chardata.getInt("x"));
			charInfopackage.setPVPKills(chardata.getInt("pvpkills"));
			charInfopackage.setPKKills(chardata.getInt("pkkills"));
		}
		catch(final Exception e)
		{
			_log.log(Level.INFO, "", e);
		}

		return charInfopackage;
	}

	@Override
	public String getType()
	{
		return _S__09_CHARSELECTINFO;
	}
}
