package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;

/**
 * Этот пакет отвечает за анимацию высасывания душ из трупов
 */
public class SpawnEmitter extends L2GameServerPacket
{
	private static final String SpawnEmitter = "[S] FE 5D SpawnEmitter";
	private final int _monsterObjId;
	private final int _playerObjId;

	public SpawnEmitter(final L2NpcInstance monster, final L2Player player)
	{
		_playerObjId = player.getObjectId();
		_monsterObjId = monster.getObjectId();
	}

	@Override
	protected final void writeImpl()
	{
		// ddd
		writeC(EXTENDED_PACKET);
		writeH(0x5d);

		writeD(_monsterObjId);
		writeD(_playerObjId);
		writeD(0x00); // unk
	}

	@Override
	public String getType()
	{
		return SpawnEmitter;
	}
}
