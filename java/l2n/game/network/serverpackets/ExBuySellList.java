package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.TradeController.NpcTradeList;
import l2n.game.model.TradeItem;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;

import java.util.TreeSet;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author L2System Project
 * @date 05.07.2010
 * @time 15:40:54
 */
public class ExBuySellList extends L2GameServerPacket
{
	private static final String _S__FE_B7_EXCURSEDWEAPONLOCATION = "[S] FE:B7 ExBuySellList";

	private int _listId;
	private int _done = 0x00;
	private final GArray<TradeItem> _buyList;
	private final TreeSet<L2ItemInstance> _sellList;
	private final ConcurrentLinkedQueue<L2ItemInstance> _refundList;
	private final long _money;
	private double _taxRate = 0;

	public ExBuySellList(final NpcTradeList Buylist, final L2Player activeChar, final double taxRate)
	{
		if(Buylist != null)
		{
			_listId = Buylist.getListId();
			_buyList = cloneAndFilter(Buylist.getItems());
			activeChar.setBuyListId(_listId);
		}
		else
			_buyList = null;
		_money = activeChar.getAdena();
		_taxRate = taxRate;
		_refundList = activeChar.getInventory().getRefundItemsList();

		_sellList = new TreeSet<L2ItemInstance>(Inventory.orderComparator);

		for(final L2ItemInstance item : activeChar.getInventory().getItemsList())
			if(item.getItem().isSellable() && item.canBeTraded(activeChar) && item.getReferencePrice() > 0)
				_sellList.add(item);
	}

	public ExBuySellList done()
	{
		_done = 0x01;
		return this;
	}

	protected static GArray<TradeItem> cloneAndFilter(final GArray<TradeItem> list)
	{
		if(list == null)
			return null;
		final GArray<TradeItem> ret = new GArray<TradeItem>(list.size());

		for(final TradeItem item : list)
		{
			if(item.getCurrentValue() < item.getCount() && item.getLastRechargeTime() + item.getRechargeTime() <= System.currentTimeMillis() / 60000L)
			{
				item.setLastRechargeTime(item.getLastRechargeTime() + item.getRechargeTime());
				item.setCurrentValue(item.getCount());
			}

			if(item.getCurrentValue() == 0 && item.getCount() != 0)
				continue;
			ret.add(item);
		}

		return ret;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0xB7);

		writeQ(_money);
		writeD(_listId);

		if(_buyList == null || _buyList.isEmpty())
			writeH(0x00);
		else
		{
			writeH(_buyList.size());
			for(final TradeItem item : _buyList)
			{
				writeH(item.getItem().getType1());
				writeD(item.getObjectId());
				writeD(item.getItemId());
				writeQ(item.getCurrentValue());
				writeH(item.getItem().getType2ForPackets());
				writeH(0x00); // ?

				writeD(item.getItem().getBodyPart());
				writeH(item.getEnchantLevel());
				writeH(0x00); // ?
				writeH(0x00);

				writeQ((long) (item.getOwnersPrice() * (1 + _taxRate)));

				writeItemElements(item);

				writeH(0x00); // Enchant effect 1
				writeH(0x00); // Enchant effect 2
				writeH(0x00); // Enchant effect 3
			}
		}

		if(_sellList == null || _sellList.isEmpty())
			writeH(0x00);
		else
		{
			writeH(_sellList.size());
			for(final L2ItemInstance item : _sellList)
			{
				writeH(item.getItem().getType1());
				writeD(item.getObjectId());
				writeD(item.getItemId());
				writeQ(item.getCount());
				writeH(item.getItem().getType2ForPackets());
				writeH(item.getCustomType1());
				writeD(item.getBodyPart());
				writeH(item.getEnchantLevel());
				writeH(item.getCustomType2());
				writeH(0x00);
				writeQ(item.getItem().getReferencePrice() / 2);

				writeItemElements(item);

				writeH(0x00); // Enchant effect 1
				writeH(0x00); // Enchant effect 2
				writeH(0x00); // Enchant effect 3
			}
		}

		if(_refundList == null)
			writeH(0x00);
		else
		{
			writeH(_refundList.size());
			for(final L2ItemInstance item : _refundList)
			{
				writeD(item.getObjectId());
				writeD(item.getItemId());
				writeQ(item.getCount());
				writeH(item.getItem().getType2ForPackets());
				writeH(item.getCustomType1());
				writeH(item.getEnchantLevel());
				writeH(item.getCustomType2());
				writeQ(item.getItem().getReferencePrice() / 2);

				writeItemElements(item);

				writeH(0x00); // Enchant effect 1
				writeH(0x00); // Enchant effect 2
				writeH(0x00); // Enchant effect 3
			}
		}

		writeC(_done);
		if(_buyList != null)
			_buyList.clear();
	}

	@Override
	public String getType()
	{
		return _S__FE_B7_EXCURSEDWEAPONLOCATION;
	}
}
