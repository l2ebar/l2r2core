package l2n.game.network.serverpackets;

import l2n.Config;
import l2n.game.instancemanager.CursedWeaponsManager;
import l2n.game.instancemanager.PartyRoomManager;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.Element;
import l2n.game.model.L2Transformation;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.model.instances.L2CubicInstance;
import l2n.game.model.items.Inventory;
import l2n.game.model.items.PcInventory;
import l2n.game.tables.NpcTable;
import l2n.util.Colors;
import l2n.util.Location;

public class UserInfo extends L2GameServerPacket
{
	private static final String _S__04_USERINFO = "[S] 04 UserInfo";

	private boolean can_writeImpl = false;
	private final int _runSpd, _walkSpd, _swimRunSpd, _swimWalkSpd,
			_flRunSpd, _flWalkSpd;

	private int _flyRunSpd;

	private int _flyWalkSpd;

	private int _relation;
	private final float move_speed, attack_speed;

	private float col_radius;

	private float col_height;
	private final PcInventory _inv;
	private final Location _loc, _fishLoc;
	private final int obj_id, _race, sex, base_class, level, curCp,
			maxCp;

	private int _enchant;
	private final long _exp;
	private final int curHp, maxHp, curMp, maxMp, curLoad, maxLoad,
			rec_left, rec_have;
	private final int _str, _con, _dex, _int, _wit, _men, _sp,
			ClanPrivs, InventoryLimit;
	private final int _patk, _patkspd, _pdef, evasion, accuracy,
			crit, _matk, _matkspd;
	private final int _mdef, pvp_flag, karma, hair_style, hair_color,
			face, gm_commands, fame, vitalityPoints;
	private final int clan_id;

	private int clan_crest_id;

	private final int ally_id;

	private int ally_crest_id;

	private int large_clan_crest_id;
	private final int private_store, can_crystalize, pk_kills,
			pvp_kills, class_id, agathion;
	private int team;

	private final int AbnormalEffect;

	private final int noble;

	private final int hero;

	private final int fishing;

	private int mount_id;

	private int cw_level;

	private final int _specialEffects;
	private final int name_color, running, pledge_class, pledge_type,
			title_color, transformation;
	private final int DefenceFire, DefenceWater, DefenceWind,
			DefenceEarth, DefenceHoly, DefenceUnholy;
	private byte mount_type;
	private String _name, title;
	private final L2CubicInstance[] cubics;
	private final int[] attackElement;
	private int _airShipObjectID;
	private final int _weaponEquipFlag;
	private final boolean isFlying;
	private final boolean partyRoom;
	private final int _territoryId;
	private int _airShipHelm;

	public UserInfo(final L2Player player)
	{
		if(player.isCursedWeaponEquipped())
		{
			_name = player.getVisName();
			clan_crest_id = 0;
			ally_crest_id = 0;
			large_clan_crest_id = 0;
			cw_level = CursedWeaponsManager.getInstance().getLevel(player.getCursedWeaponEquippedId());
		}
		else
		{
			_name = player.getName();
			clan_crest_id = player.getClanCrestId();
			ally_crest_id = player.getAllyCrestId();
			large_clan_crest_id = player.getClanCrestLargeId();
			cw_level = 0;
		}

		if(player.isMounted())
		{
			_enchant = 0;
			mount_id = player.getMountNpcId() + 1000000;
			mount_type = (byte) player.getMountType();
		}
		else
		{
			_enchant = (byte) player.getEnchantEffect();
			mount_id = 0;
			mount_type = 0;
		}

		move_speed = player.getMovementSpeedMultiplier();
		_runSpd = (int) (player.getRunSpeed() / move_speed);
		_walkSpd = (int) (player.getWalkSpeed() / move_speed);
		_flRunSpd = _flyRunSpd = _runSpd;
		_flWalkSpd = _flyWalkSpd = _walkSpd;
		_swimRunSpd = player.getSwimSpeed();
		_swimWalkSpd = player.getSwimSpeed();
		_inv = player.getInventory();
		_relation = player.isClanLeader() ? 0x40 : 0;
		_territoryId = player.getTerritorySiege();
		if(player.getSiegeState() == 1)
			if(_territoryId == 0)
				_relation |= 0x180;
			else
				_relation |= 0x1000;
		else if(player.getSiegeState() == 2)
			_relation |= 0x80;

		_loc = player.getLoc();
		obj_id = player.getObjectId();
		_race = player.getRace().ordinal();
		sex = player.getSex();
		base_class = player.getBaseClassId();
		level = player.getLevel();
		_exp = player.getExp();
		_str = player.getSTR();
		_dex = player.getDEX();
		_con = player.getCON();
		_int = player.getINT();
		_wit = player.getWIT();
		_men = player.getMEN();
		curHp = (int) player.getCurrentHp();
		maxHp = player.getMaxHp();
		curMp = (int) player.getCurrentMp();
		maxMp = player.getMaxMp();
		curLoad = player.getCurrentLoad();
		maxLoad = player.getMaxLoad();
		_sp = player.getSp();
		_patk = player.getPAtk(null);
		_patkspd = player.getPAtkSpd();
		_pdef = player.getPDef(null);
		evasion = player.getEvasionRate(null);
		accuracy = player.getAccuracy();
		crit = player.getCriticalHit(null, null);
		_matk = player.getMAtk(null, null);
		_matkspd = player.getMAtkSpd();
		_mdef = player.getMDef(null, null);
		pvp_flag = player.getPvpFlag(); // 0=white, 1=purple, 2=purpleblink
		karma = player.getKarma();
		attack_speed = player.getAttackSpeedMultiplier();

		// calc collision radius & height
		final L2Summon pet = player.getPet();
		final L2Transformation trans = player.getTransform();
		if(player.getMountType() != 0 && pet != null)
		{
			col_radius = pet.getColRadius();
			col_height = pet.getColHeight();
		}
		else if(trans != null)
		{
			col_radius = (float) trans.getCollisionRadius(player);
			col_height = (float) trans.getCollisionHeight(player);
		}
		else
		{
			col_radius = player.getColRadius();
			col_height = player.getColHeight();
		}

		hair_style = player.getHairStyle();
		hair_color = player.getHairColor();
		face = player.getFace();
		gm_commands = player.getPlayerAccess().IsGM || player.getPlayerAccess().UseGMComand || Config.ALLOW_SPECIAL_COMMANDS ? 1 : 0;
		// builder level активирует в клиенте админские команды
		title = player.getTitle();
		if(player.isInvisible() && player.isGM())
			title += "[I]";
		if(player.isPolymorphed())
			if(NpcTable.getTemplate(player.getPolyid()) != null)
				title += " - " + NpcTable.getTemplate(player.getPolyid()).name;
			else
				title += " - Polymorphed";
		clan_id = player.getClanId();
		ally_id = player.getAllyId();
		private_store = player.getPrivateStoreType();
		can_crystalize = player.getSkillLevel(L2Skill.SKILL_CRYSTALLIZE) > 0 ? 1 : 0;
		pk_kills = player.getPkKills();
		pvp_kills = player.getPvpKills();
		cubics = player.getCubics().toArray(new L2CubicInstance[0]);
		AbnormalEffect = player.getAbnormalEffect();
		_specialEffects = player.getSpecialEffect();
		ClanPrivs = player.getClanPrivileges();
		rec_left = player.getRecomLeft(); // c2 recommendations remaining
		rec_have = player.getPlayerAccess().IsGM ? 0 : player.getRecomHave(); // c2 recommendations received
		InventoryLimit = player.getInventoryLimit();
		class_id = player.getClassId().getId();
		maxCp = player.getMaxCp();
		curCp = (int) player.getCurrentCp();

		if(player.getTeam() < 3)
			team = player.getTeam(); // team circle around feet 1= Blue, 2 = red
		else
			team = 1;

		noble = player.isNoble() || player.isGM() && Config.GM_HERO_AURA ? 1 : 0; // 0x01: symbol on char menu ctrl+I
		hero = player.isHero() || player.isGM() && Config.GM_HERO_AURA ? 1 : 0; // 0x01: Hero Aura and symbol
		fishing = player.isFishing() ? 1 : 0; // Fishing Mode
		_fishLoc = player.getFishLoc();
		name_color = player.getNameColor();
		running = player.isRunning() ? 0x01 : 0x00; // changes the Speed display on Status Window
		pledge_class = player.getPledgeClass();
		pledge_type = player.getPledgeType();
		title_color = Config.TITLE_PVP_MODE && Config.TITLE_PVP_MODE_FOR_SELF ? Colors.getColor(pvp_kills) : player.getTitleColor();
		transformation = player.getTransformationId();
		attackElement = player.getAttackElement();
		DefenceFire = player.getDefence(Element.FIRE);
		DefenceWater = player.getDefence(Element.WATER);
		DefenceWind = player.getDefence(Element.WIND);
		DefenceEarth = player.getDefence(Element.EARTH);
		DefenceHoly = player.getDefence(Element.SACRED);
		DefenceUnholy = player.getDefence(Element.UNHOLY);
		agathion = player.getAgathion() != null ? player.getAgathion().getId() : 0; // агнишен
		fame = player.getFame();
		vitalityPoints = (int) player.getVitalityPoints();
		partyRoom = PartyRoomManager.getInstance().isLeader(player);

		if(player.getVehicle() != null && player.getVehicle().isAirShip())
		{
			_airShipObjectID = player.getVehicle().getObjectId();
			if(player.getVehicle().isClanAirShip() && player.getVehicle().getDriver() == player)
				_airShipHelm = L2AirShip.HELM;
		}
		else
		{
			_airShipObjectID = 0x00;
			_airShipHelm = 0x00;
		}

		_weaponEquipFlag = player.getActiveWeaponItem() != null ? 0x28 : 0x14;

		isFlying = player.isInFlyingTransform();
		can_writeImpl = true;
	}

	@Override
	protected final void writeImpl()
	{
		if(!can_writeImpl)
			return;

		writeC(0x32);

		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z + Config.CLIENT_Z_SHIFT);
		writeD(_airShipObjectID); // heading from CT2.3 no longer used inside userinfo, here is now vehicle id (boat,airship)
		writeD(obj_id);
		writeS(_name);
		writeD(_race);
		writeD(sex);
		writeD(base_class);
		writeD(level);
		writeQ(_exp);
		writeD(_str);
		writeD(_dex);
		writeD(_con);
		writeD(_int);
		writeD(_wit);
		writeD(_men);
		writeD(maxHp);
		writeD(curHp);
		writeD(maxMp);
		writeD(curMp);
		writeD(_sp);
		writeD(curLoad);
		writeD(maxLoad);
		writeD(_weaponEquipFlag);

		if(_airShipHelm == 0)
		{
			for(final byte PAPERDOLL_ID : PAPERDOLL_ORDER)
				writeD(_inv.getPaperdollObjectId(PAPERDOLL_ID));

			for(final byte PAPERDOLL_ID : PAPERDOLL_ORDER)
				writeD(_inv.getPaperdollItemId(PAPERDOLL_ID));

			for(final byte PAPERDOLL_ID : PAPERDOLL_ORDER)
				writeD(_inv.getPaperdollAugmentationId(PAPERDOLL_ID));
		}
		else
		{
			for(final byte PAPERDOLL_ID : PAPERDOLL_ORDER)
				if(PAPERDOLL_ID == Inventory.PAPERDOLL_RHAND || PAPERDOLL_ID == Inventory.PAPERDOLL_LHAND)
					writeD(0x00);
				else
					writeD(_inv.getPaperdollObjectId(PAPERDOLL_ID));

			for(final byte PAPERDOLL_ID : PAPERDOLL_ORDER)
				if(PAPERDOLL_ID == Inventory.PAPERDOLL_RHAND)
					writeD(_airShipHelm);
				else if(PAPERDOLL_ID == Inventory.PAPERDOLL_LHAND)
					writeD(0x00);
				else
					writeD(_inv.getPaperdollItemId(PAPERDOLL_ID));
			for(final byte PAPERDOLL_ID : PAPERDOLL_ORDER)
				if(PAPERDOLL_ID == Inventory.PAPERDOLL_RHAND)
					writeD(_airShipHelm);
				else if(PAPERDOLL_ID == Inventory.PAPERDOLL_LHAND)
					writeD(0x00);
				else
					writeD(_inv.getPaperdollAugmentationId(PAPERDOLL_ID));
		}

		writeD(_inv.getMaxTalismanCount());
		writeD(_inv.getCloakStatus()); // CT2.3 статус слота для плащя

		writeD(_patk);
		writeD(_patkspd);
		writeD(_pdef);
		writeD(evasion);
		writeD(accuracy);
		writeD(crit);
		writeD(_matk);
		writeD(_matkspd);
		writeD(_patkspd);
		writeD(_mdef);
		writeD(pvp_flag);
		writeD(karma);
		writeD(_runSpd);
		writeD(_walkSpd);
		writeD(_swimRunSpd); // swimspeed
		writeD(_swimWalkSpd); // swimspeed
		writeD(_flRunSpd); // 0x00 in SF
		writeD(_flWalkSpd); // 0x00 in SF
		writeD(_flyRunSpd); // should be 0x00 если чар на земле а не летит
		writeD(_flyWalkSpd);
		writeF(move_speed);
		writeF(attack_speed);
		writeF(col_radius);
		writeF(col_height);
		writeD(hair_style);
		writeD(hair_color);
		writeD(face);
		writeD(gm_commands);
		writeS(title);
		writeD(clan_id);
		writeD(clan_crest_id);
		writeD(ally_id);
		writeD(ally_crest_id);
		// 0x40 leader rights
		// siege flags: attacker - 0x180 sword over name, defender - 0x80 shield, 0xC0 crown (|leader), 0x1C0 flag (|leader)
		writeD(_relation);
		writeC(mount_type); // mount type
		writeC(private_store);
		writeC(can_crystalize);
		writeD(pk_kills);
		writeD(pvp_kills);
		writeH(cubics.length);
		for(final L2CubicInstance cubic : cubics)
			writeH(cubic == null ? 0 : cubic.getId());
		writeC(partyRoom ? 1 : 0); // 1-find party members
		writeD(AbnormalEffect);
		writeC(isFlying ? 2 : 0); // / writeC(_activeChar.isFlyingMounted() ? 2 : 0);
		writeD(ClanPrivs);
		writeH(rec_left);
		writeH(rec_have);
		writeD(mount_id);
		writeH(InventoryLimit);
		writeD(class_id);
		writeD(0x00); // special effects? circles around player...
		writeD(maxCp);
		writeD(curCp);
		writeC(_enchant);
		writeC(team);
		writeD(large_clan_crest_id);
		writeC(noble);
		writeC(hero);
		writeC(fishing);
		writeD(_fishLoc.x);
		writeD(_fishLoc.y);
		writeD(_fishLoc.z);
		writeD(name_color);
		writeC(running);
		writeD(pledge_class);
		writeD(pledge_type);
		writeD(title_color);
		writeD(cw_level);
		writeD(transformation); // Transformation id
		// AttackElement (0 - Fire, 1 - Water, 2 - Wind, 3 - Earth, 4 - Holy, 5 - Dark, -2 - None)
		writeH(attackElement == null ? -2 : attackElement[0]);
		writeH(attackElement == null ? 0 : attackElement[1]); // AttackElementValue
		writeH(DefenceFire); // DefAttrFire
		writeH(DefenceWater); // DefAttrWater
		writeH(DefenceWind); // DefAttrWind
		writeH(DefenceEarth); // DefAttrEarth
		writeH(DefenceHoly); // DefAttrHoly
		writeH(DefenceUnholy); // DefAttrUnholy
		writeD(agathion);

		// T2 Starts
		writeD(fame); // Fame
		writeD(0x01); // Minimap on Hellbound
		writeD(vitalityPoints); // Vitality Points

		// Gracia Final
		writeD(_specialEffects);
		writeD(_territoryId > 0 ? 80 + _territoryId : 0);
		writeD(0);
		writeD(_territoryId > 0 ? 80 + _territoryId : 0);
	}

	public static final byte[] PAPERDOLL_ORDER = {
			Inventory.PAPERDOLL_UNDER,
			Inventory.PAPERDOLL_REAR,
			Inventory.PAPERDOLL_LEAR,
			Inventory.PAPERDOLL_NECK,
			Inventory.PAPERDOLL_RFINGER,
			Inventory.PAPERDOLL_LFINGER,
			Inventory.PAPERDOLL_HEAD,
			Inventory.PAPERDOLL_RHAND,
			Inventory.PAPERDOLL_LHAND,
			Inventory.PAPERDOLL_GLOVES,
			Inventory.PAPERDOLL_CHEST,
			Inventory.PAPERDOLL_LEGS,
			Inventory.PAPERDOLL_FEET,
			Inventory.PAPERDOLL_BACK,
			Inventory.PAPERDOLL_LRHAND,
			Inventory.PAPERDOLL_HAIR,
			Inventory.PAPERDOLL_DHAIR,
			Inventory.PAPERDOLL_RBRACELET,
			Inventory.PAPERDOLL_LBRACELET,
			Inventory.PAPERDOLL_DECO1,
			Inventory.PAPERDOLL_DECO2,
			Inventory.PAPERDOLL_DECO3,
			Inventory.PAPERDOLL_DECO4,
			Inventory.PAPERDOLL_DECO5,
			Inventory.PAPERDOLL_DECO6,
			Inventory.PAPERDOLL_BELT };

	@Override
	public String getType()
	{
		return _S__04_USERINFO;
	}
}
