package l2n.game.network.serverpackets;

/**
 * Format: ch
 * Протокол 828: при отправке пакета клиенту ничего не происходит.
 */
public class ExPlayScene extends L2GameServerPacket
{
	private static final String _S_FE_5c_EXPLAYSCENE = "[S] FE:5c ExPlayScene";

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x5c);
		writeD(0x00); // Kamael
	}

	@Override
	public String getType()
	{
		return _S_FE_5c_EXPLAYSCENE;
	}
}
