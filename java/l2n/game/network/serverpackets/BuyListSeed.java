package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.TradeController.NpcTradeList;
import l2n.game.model.TradeItem;

/**
 * Format: c ddh[hdddhhd]
 * c - id (0xE8)
 * d - money
 * d - manor id
 * h - size
 * [
 * h - item type 1
 * d - object id
 * d - item id
 * d - count
 * h - item type 2
 * h
 * d - price
 * ]
 */
public final class BuyListSeed extends L2GameServerPacket
{
	private static final String _S__e9_BUYLISTSEED = "[S] e9 BuyListSeed";

	private final int _manorId;
	private GArray<TradeItem> _list = new GArray<TradeItem>();
	private final long _money;

	public BuyListSeed(final NpcTradeList list, final int manorId, final long currentMoney)
	{
		_money = currentMoney;
		_manorId = manorId;
		_list = list.getItems();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xe9);

		writeQ(_money); // current money // TODO: BuyListSeed
		writeD(_manorId); // manor id

		writeH(_list.size()); // list length

		for(final TradeItem item : _list)
		{
			writeH(0x04); // item->type1
			writeD(0x00); // objectId
			writeD(item.getItemId()); // item id
			writeQ(item.getCount()); // item count
			writeH(0x04); // item->type2
			writeH(0x00); // unknown :)
			writeQ(item.getOwnersPrice()); // price
		}
	}

	@Override
	public String getType()
	{
		return _S__e9_BUYLISTSEED;
	}
}
