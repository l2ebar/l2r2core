package l2n.game.network.serverpackets;

public class SunSet extends L2GameServerPacket
{
	private static final String _S__13_SUNSET = "[S] 13 SunSet";

	@Override
	protected final void writeImpl()
	{
		writeC(0x13);
	}

	@Override
	public String getType()
	{
		return _S__13_SUNSET;
	}
}
