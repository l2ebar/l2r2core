package l2n.game.network.serverpackets;

public class ExVitalityPointInfo extends L2GameServerPacket
{
	private static final String _S__FE_A0_EXVITALITYPOINTINFO = "[S] FE:A0 ExVitalityPointInfo";
	private final int _vitalityPoints;

	public ExVitalityPointInfo(final int vitPoints)
	{
		_vitalityPoints = vitPoints;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0xa0);
		writeD(_vitalityPoints);
	}

	@Override
	public String getType()
	{
		return _S__FE_A0_EXVITALITYPOINTINFO;
	}
}
