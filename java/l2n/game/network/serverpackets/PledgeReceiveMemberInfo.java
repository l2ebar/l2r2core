package l2n.game.network.serverpackets;

import l2n.game.model.L2Clan;
import l2n.game.model.L2ClanMember;

public class PledgeReceiveMemberInfo extends L2GameServerPacket
{
	private static final String _S__FE_3E_PLEDGERECEIVEMEMBERINFO = "[S] FE:3e PledgeReceiveMemberInfo";
	private final L2ClanMember _member;
	private final L2Clan _clan;

	public PledgeReceiveMemberInfo(final L2ClanMember member, final L2Clan clan)
	{
		_member = member;
		_clan = clan;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x3e);

		writeD(_member.getPledgeType());
		writeS(_member.getName());
		writeS(_member.getTitle());
		writeD(_member.getPowerGrade());

		if(_member.getPledgeType() != 0)
			writeS(_clan.getSubPledge(_member.getPledgeType()).getName());
		else
			writeS(_clan.getName());

		writeS(_member.getRelatedName()); // apprentice/sponsor name if any
	}

	@Override
	public String getType()
	{
		return _S__FE_3E_PLEDGERECEIVEMEMBERINFO;
	}
}
