package l2n.game.network.clientpackets;


import ftGuard.ftGuard;
import ftGuard.ftConfig;
import l2n.Config;
import l2n.game.network.L2GameClient;
import l2n.game.network.serverpackets.KeyPacket;
import l2n.game.network.serverpackets.SendStatus;

public final class ProtocolVersion extends L2GameClientPacket {
    private static final String _C__0E_PROTOCOLVERSION = "[C] 0e ProtocolVersion";
    private static final KeyPacket WRONG_PROTOCOL = new KeyPacket(null, 0);

    private int _version;
    private byte[] _data;
    private byte[] _check;
    private String _hwidHdd = "", _hwidMac = "", _hwidCPU = "";

    @Override
    protected void readImpl() {
        _version = readD();
        if (_buf.remaining() == 0x104) {
            _data = new byte[0x100];
            _check = new byte[4];
            readB(_data);
            readB(_check);
        }
        if (_buf.remaining() > 256) {
            _hwidHdd = readS();
            _hwidMac = readS();
            _hwidCPU = readS();
        }
    }

    @Override
    public void runImpl() {
        // this packet is never encrypted
        final L2GameClient client = getClient();
        if (_version == -2)
            // this is just a ping attempt from the new C2 client
            client.closeNow(false);
        else if (_version == -3)
            // Проверки рейтинга типа l2top
            client.close(new SendStatus());
        else if (_version < Config.MIN_PROTOCOL_REVISION || _version > Config.MAX_PROTOCOL_REVISION) {
            _log.info("Client: " + client.toString() + " -> Protocol Revision: " + _version + " is invalid. Supported protocols: from " + Config.MIN_PROTOCOL_REVISION + " to " + Config.MAX_PROTOCOL_REVISION + ". Closing connection.");
            client.close(WRONG_PROTOCOL);
        } else {
            client.setRevision(_version); // set client's Protocol vers
            if (ftGuard.isProtectionOn()) {
                switch (ftConfig.GET_CLIENT_HWID) {
                    case 1:
                        if (_hwidHdd == "") {
                            _log.info("Status HWID HDD : NoPatch!!!");
                            getClient().close(new KeyPacket(null, 0));
                        } else
                            getClient().setHWID(_hwidHdd);
                        break;
                    case 2:
                        if (_hwidMac == "") {
                            _log.info("Status HWID MAC : NoPatch!!!");
                            getClient().close(new KeyPacket(null, 0));
                        } else
                            getClient().setHWID(_hwidCPU+_hwidMac);
                        break;
                    case 3:
                        if (_hwidCPU == "") {
                            _log.info("Status HWID : NoPatch!!!");
                            getClient().close(new KeyPacket(null, 0));
                        } else
                            getClient().setHWID(_hwidCPU+_hwidMac);
                        break;
                }
            }

            final byte[] key = client.enableCrypt();
            final byte[] answer = new byte[8];
            System.arraycopy(key, 0, answer, 0, 8);
            client.sendPacket(new KeyPacket(answer, 0));
            return;
        }
    }


    @Override
    public boolean blockReadingUntilExecutionIsFinished() {
        return true;
    }

    @Override
    public String getType() {
        return _C__0E_PROTOCOLVERSION;
    }
}
