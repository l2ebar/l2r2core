package l2n.game.network.clientpackets;

import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.*;

public class RequestGMCommand extends L2GameClientPacket
{
	private static final String _C__6E_REQUESTGMCOMMAND = "[C] 6E RequestGMCommand";
	// format: cSdd
	private String _targetName;
	private int _command;

	@Override
	public void readImpl()
	{
		_targetName = readS();
		_command = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = L2ObjectsStorage.getPlayer(_targetName);
		if(activeChar == null)
			return;
		if(getClient().getActiveChar() == null)
			return;
		if(!getClient().getActiveChar().getPlayerAccess().EditChar)
			return;

		switch (_command)
		{
			case 1:
				sendPacket(new GMViewCharacterInfo(activeChar));
				sendPacket(new GMHennaInfo(activeChar));
				break;
			case 2:
				if(activeChar.getClan() != null)
					sendPacket(new GMViewPledgeInfo(activeChar.getClan(), activeChar));
				break;
			case 3:
				sendPacket(new GMViewSkillInfo(activeChar));
				break;
			case 4:
				sendPacket(new GMViewQuestInfo(activeChar));
				break;
			case 5:
				sendPacket(new GMViewItemList(activeChar));
				break;
			case 6:
				sendPacket(new GMViewWarehouseWithdrawList(activeChar));
				break;
		}
	}

	@Override
	public String getType()
	{
		return _C__6E_REQUESTGMCOMMAND;
	}
}
