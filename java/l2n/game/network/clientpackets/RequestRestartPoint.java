package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Zone;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.residence.ClanHall;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.model.entity.residence.ResidenceFunction;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.entity.siege.SiegeClan;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.network.serverpackets.Die;
import l2n.game.network.serverpackets.Revive;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.EffectType;
import l2n.game.tables.MapRegionTable;
import l2n.util.Location;

import java.util.logging.Level;

/**
 * PointType:
 * 0 - To Village
 * 1 - ClanHall
 * 2 - Castle
 * 3 - Fortress
 * 4 - SiegeFlag
 * 5 - ResurectFixed
 */
public class RequestRestartPoint extends L2GameClientPacket
{
	private final static String _C__7D_REQUESTRESTARTPOINT = "[C] 7D RequestRestartPoint";

	private int requestedPointType;

	private static final int TO_VILLAGE = 0;
	private static final int TO_CLANHALL = 1;
	private static final int TO_CASTLE = 2;
	private static final int TO_FORTRESS = 3;
	private static final int TO_SIEGEHQ = 4;
	private static final int FIXED = 5;
	private static final int FEATHER = 6;

	/**
	 * packet type id 0x7D
	 * format: cd
	 * 
	 * @param decrypt
	 */
	@Override
	public void readImpl()
	{
		requestedPointType = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();

		if(activeChar == null)
			return;

		if(activeChar.isFakeDeath())
		{
			activeChar.getEffectList().stopEffects(EffectType.FakeDeath);
			activeChar.broadcastPacket(new Revive(activeChar));
			return;
		}

		if(!activeChar.isDead() && !activeChar.getPlayerAccess().IsGM)
		{
			activeChar.sendActionFailed();
			return;
		}
		if(activeChar.isInZone(L2Zone.ZoneType.no_escape))
		{
			activeChar.setIsPendingRevive(true);
			activeChar.teleToLocation(activeChar.getLoc());
			return;
		}

		// проверка на тюрьму
		if(activeChar.isInJail())
		{
			// в тюряге разрешен рес только в тюрьме, какое бы место реса ни выбрал игрок
			activeChar.setIsPendingRevive(true);
			activeChar.teleToLocation(-114648, -249384, -2984);
		}

		try
		{
			Location loc = null;
			long ref = 0;

			boolean isInDefense = false;
			if(activeChar.isFestivalParticipant())
				requestedPointType = 5;

			final L2Clan clan = activeChar.getClan();
			final Siege siege = SiegeManager.getSiege(activeChar, true);

			switch (requestedPointType)
			{
				case TO_CLANHALL:
					if(clan == null || clan.getHasHideout() == 0)
						loc = MapRegionTable.getTeleToClosestTown(activeChar);
					else
					{
						final ClanHall clanHall = activeChar.getClanHall();
						if(clanHall != null)
						{
							loc = MapRegionTable.getTeleToClanHall(activeChar);
							if(clanHall.getFunction(ResidenceFunction.RESTORE_EXP) != null)
								activeChar.restoreExp(clanHall.getFunction(ResidenceFunction.RESTORE_EXP).getLevel());
						}
					}
					break;
				case TO_CASTLE:
					isInDefense = false;
					if(siege != null && siege.checkIsDefender(clan, false))
						isInDefense = true;
					if((clan == null || clan.getHasCastle() == 0) && !isInDefense)
					{
						activeChar.sendActionFailed();
						return;
					}
					final Castle castle = activeChar.getCastle();
					if(castle != null)
					{
						loc = MapRegionTable.getTeleToCastle(activeChar);
						if(castle.getFunction(ResidenceFunction.RESTORE_EXP) != null)
							activeChar.restoreExp(castle.getFunction(ResidenceFunction.RESTORE_EXP).getLevel());
					}
					break;
				case TO_FORTRESS:
					isInDefense = false;
					if(siege != null && siege.checkIsDefender(clan, false))
						isInDefense = true;
					if((clan == null || clan.getHasFortress() == 0) && !isInDefense)
					{
						activeChar.sendActionFailed();
						return;
					}
					final Fortress fort = activeChar.getFortress();
					if(fort != null)
					{
						loc = MapRegionTable.getTeleToFortress(activeChar);
						if(fort.getFunction(ResidenceFunction.RESTORE_EXP) != null)
							activeChar.restoreExp(fort.getFunction(ResidenceFunction.RESTORE_EXP).getLevel());
					}
					break;
				case TO_SIEGEHQ:
					SiegeClan siegeClan = null;
					if(siege != null)
						siegeClan = siege.getAttackerClan(clan);
					else if(TerritorySiege.checkIfInZone(activeChar))
						siegeClan = TerritorySiege.getSiegeClan(clan);
					if(siegeClan == null || siegeClan.getHeadquarter() == null)
					{
						sendPacket(new SystemMessage(SystemMessage.IF_A_BASE_CAMP_DOES_NOT_EXIST_RESURRECTION_IS_NOT_POSSIBLE), new Die(activeChar));
						return;
					}
					loc = MapRegionTable.getTeleToHeadquarter(activeChar);
					break;
				case FIXED:
					if(!activeChar.isFestivalParticipant() && !activeChar.getPlayerAccess().ResurectFixed)
					{
						activeChar.sendActionFailed();
						return;
					}
					loc = activeChar.getLoc();
					ref = activeChar.getReflection().getId();
					break;
				case FEATHER:
					sendPacket(Msg.YOU_HAVE_USED_THE_FEATHER_OF_BLESSING_TO_RESURRECT);
					for(final int itemId : Die.FEATHERS)
						if(activeChar.getInventory().destroyItemByItemId(itemId, 1, false) != null)
							break;
					loc = activeChar.getLoc();
					ref = activeChar.getReflection().getId();
					break;
				case TO_VILLAGE:
				default:
					loc = MapRegionTable.getTeleToClosestTown(activeChar);
					break;
			}

			activeChar.setIsPendingRevive(true);
			activeChar.teleToLocation(loc, ref);
		}
		catch(final Throwable e)
		{
			_log.log(Level.WARNING, "Error in RequestRestartPoint, point " + requestedPointType, e);
		}
	}

	@Override
	public String getType()
	{
		return _C__7D_REQUESTRESTARTPOINT;
	}
}
