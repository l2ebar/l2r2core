package l2n.game.network.clientpackets;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.tables.SkillTable;

public class RequestMagicSkillUse extends L2GameClientPacket
{
	private static final String _C__39_REQUESTMAGICSKILLUSE = "[C] 39 RequestMagicSkillUse";

	private int _magicId;
	private boolean _ctrlPressed;
	private boolean _shiftPressed;

	/**
	 * packet type id 0x39
	 * format: cddc
	 * 
	 * @param rawPacket
	 */
	@Override
	public void readImpl()
	{
		_magicId = readD();
		_ctrlPressed = readD() != 0; // True if it's a ForceAttack : Ctrl pressed
		_shiftPressed = readC() != 0; // True if Shift pressed
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.isOutOfControl())
		{
			activeChar.sendActionFailed();
			return;
		}

		// Get the level of the used skill
		final int level = activeChar.getSkillLevel(_magicId);
		if(level <= 0)
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2Skill skill = SkillTable.getInstance().getInfo(_magicId, level);
		if(skill != null)
		{
			if(!(skill.isActive() || skill.isToggle()))
			{
				activeChar.sendActionFailed();
				return;
			}

			// В режиме трансформации доступны только скилы трансформы
			if(activeChar.isTransformed() && !activeChar.getAllSkills().contains(skill))
			{
				activeChar.sendActionFailed();
				return;
			}

			if(skill.isToggle() && activeChar.getEffectList().getFirstEffect(skill) != null)
			{
				activeChar.getEffectList().stopEffect(skill.getId());
				activeChar.sendActionFailed();
				return;
			}

			final L2Character target = skill.getAimingTarget(activeChar, activeChar.getTarget());

			activeChar.setGroundSkillLoc(null);
			activeChar.getAI().Cast(skill, target, _ctrlPressed, _shiftPressed);
		}
		else
			activeChar.sendActionFailed();
	}

	@Override
	public String getType()
	{
		return _C__39_REQUESTMAGICSKILLUSE;
	}
}
