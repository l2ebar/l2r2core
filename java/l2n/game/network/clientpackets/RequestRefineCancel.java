package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExVariationCancelResult;
import l2n.game.network.serverpackets.InventoryUpdate;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.templates.L2Item;

public final class RequestRefineCancel extends L2GameClientPacket
{
	private static final String _C__D0_46_REQUESTREFINECANCEL = "[C] D0:46 RequestRefineCancel";
	// format: (ch)d
	private int _targetItemObjId;

	@Override
	protected void readImpl()
	{
		_targetItemObjId = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2ItemInstance targetItem = activeChar.getInventory().getItemByObjectId(_targetItemObjId);
		if(targetItem == null)
		{
			activeChar.sendPacket(ExVariationCancelResult.STATIC_FAIL);
			return;
		}

		
		// cannot remove augmentation from a not augmented item
		if(!targetItem.isAugmented())
		
		{
			activeChar.sendPacket(ExVariationCancelResult.STATIC_FAIL, Msg.AUGMENTATION_REMOVAL_CAN_ONLY_BE_DONE_ON_AN_AUGMENTED_ITEM);
			return;
		}
		
		
		if(targetItem.isEquipped())
			activeChar.getInventory().unEquipItem(targetItem);

		// get the price
		int price = 0;
		switch (targetItem.getItem().getItemGrade().cry)
		{
			case L2Item.CRYSTAL_C:
			{
				if(targetItem.getItem().getCrystalCount() < 1720)
					price = 95000;
				else if(targetItem.getItem().getCrystalCount() < 2452)
					price = 150000;
				else
					price = 210000;
				break;
			}
			case L2Item.CRYSTAL_B:
			{
				if(targetItem.getItem().getCrystalCount() < 1746)
					price = 240000;
				else
					price = 270000;
				break;
			}
			case L2Item.CRYSTAL_A:
			{
				if(targetItem.getItem().getCrystalCount() < 2160)
					price = 330000;
				else if(targetItem.getItem().getCrystalCount() < 2824)
					price = 390000;
				else
					price = 420000;
				break;
			}
			case L2Item.CRYSTAL_S:
			{
				if(targetItem.getItem().getCrystalCount() == 10394)
					price = 960000;
				else if(targetItem.getItem().getCrystalCount() == 7050)
					price = 720000;
				else
					price = 480000;
				break;
			}
			// any other item type is not augmentable
			default:
				activeChar.sendPacket(ExVariationCancelResult.STATIC_FAIL);
				return;
		}

		// try to reduce the players adena
		if(activeChar.getAdena() < price)
		{
			activeChar.sendPacket(ExVariationCancelResult.STATIC_FAIL, Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			return;
		}
		activeChar.reduceAdena(price, true);

		// cancel boni
		targetItem.getAugmentation().removeBoni(activeChar);

		// remove the augmentation
		targetItem.removeAugmentation();

		// send system message
		final SystemMessage sm = new SystemMessage(SystemMessage.AUGMENTATION_HAS_BEEN_SUCCESSFULLY_REMOVED_FROM_YOUR_S1);
		sm.addItemName(targetItem.getItemId());
		activeChar.sendPacket(ExVariationCancelResult.STATIC_SUCCESS, new InventoryUpdate(targetItem, L2ItemInstance.MODIFIED), sm);
		activeChar.sendChanges();
	}

	@Override
	public String getType()
	{
		return _C__D0_46_REQUESTREFINECANCEL;
	}
}
