package l2n.game.network.clientpackets;


import jonelo.jacksum.JacksumAPI;
import jonelo.jacksum.algorithm.AbstractChecksum;
import l2n.commons.captcha.CaptchaValidator;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.communitybbs.CommunityBoard;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.BypassHandler;
import l2n.game.handler.VoicedCommandHandler;
import l2n.game.handler.interfaces.IBypassHandler;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.BypassManager.DecodedBypass;
import l2n.game.model.L2Multisell;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.NpcHtmlMessage;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.logging.Level;

public class RequestBypassToServer extends L2GameClientPacket
{
	private static final String _C__23_REQUESTBYPASSTOSERVER = "[C] 23 RequestBypassToServer";

	private DecodedBypass bp = null;

	@Override
	public void readImpl()
	{
		String bypass;
		try
		{
			bypass = readS();
		}
		catch(final Exception e)
		{
			bypass = null;
		}
		if(bypass != null && !bypass.isEmpty())
			bp = getClient().getActiveChar().decodeBypass(bypass);
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null || bp == null)
			return;
		try
		{
			L2NpcInstance npc = activeChar.getLastNpc();
			final L2Object target = activeChar.getTarget();
			if(npc == null && target != null && target.isNpc())
				npc = (L2NpcInstance) target;

			if(bp.bbs)
				CommunityBoard.handleCommands(getClient(), bp.bypass);
			else if(bp.bypass.startsWith("admin_"))
				AdminCommandHandler.getInstance().useAdminCommandHandler(activeChar, bp.bypass);
			else if(bp.bypass.equals("come_here") && activeChar.isGM())
				comeHere(activeChar);
			else if(bp.bypass.startsWith("pc_"))
			{
				final IBypassHandler byp = BypassHandler.getInstance().getBypassHandler(bp.bypass);
				if(byp != null)
					byp.useBypass(bp.bypass, activeChar, null);
				else
					_log.log(Level.WARNING, "No handler registered for bypass '" + bp.bypass + "'");
			}
			else if(bp.bypass.startsWith("player_help "))
				playerHelp(activeChar, bp.bypass.substring(12));
			else if(bp.bypass.startsWith("scripts_"))
			{
				// System.out.println(bp.bypass);
				final String command = bp.bypass.substring(8).trim();
				final String[] word = command.split("\\s+");
				final String[] args = command.substring(word[0].length()).trim().split("\\s+");
				final String[] path = word[0].split(":");
				if(path.length != 2)
				{
					_log.warning("Bad Script bypass: '" + command + "'!");
					return;
				}

				final HashMap<String, Object> variables = new HashMap<String, Object>();

				if(npc != null)
					variables.put("npc", npc.getStoredId());
				else
					variables.put("npc", null);

				if(word.length == 1)
					activeChar.callScripts(path[0], path[1], new Object[] {}, variables);
				else
					activeChar.callScripts(path[0], path[1], new Object[] { args }, variables);
			}
			else if(bp.bypass.startsWith("user_"))
			{
				final String command = bp.bypass.substring(5).trim();
				final String word = command.split("\\s+")[0];
				final String args = command.substring(word.length()).trim();
				final IVoicedCommandHandler vch = VoicedCommandHandler.getInstance().getVoicedCommandHandler(word);
				if(vch != null)
					vch.useVoicedCommand(word, activeChar, args);
				else
					_log.warning("Unknow voiced command '" + word + "'");
			}
			else if(bp.bypass.startsWith("npc_"))
			{
				final int endOfId = bp.bypass.indexOf('_', 5);
				String id;
				if(endOfId > 0)
					id = bp.bypass.substring(4, endOfId);
				else
					id = bp.bypass.substring(4);
				final L2Object object = activeChar.getVisibleObject(Integer.parseInt(id));
				if(object != null && object.isNpc() && endOfId > 0 && activeChar.isInRange(object.getLoc(), L2Character.INTERACTION_DISTANCE))
				{
					activeChar.setLastNpc((L2NpcInstance) object);
					((L2NpcInstance) object).onBypassFeedback(activeChar, bp.bypass.substring(endOfId + 1));
				}
			}
			else if (bp.bypass.equalsIgnoreCase("pkrecovery"))
			{
				String htmContent = "data/html/passkey/recovery.htm";
				NpcHtmlMessage html = new NpcHtmlMessage(1);
				html.setFile(htmContent);
				html.replace("%question%", getPassKeyQuestion(activeChar));
				activeChar.sendPacket(html);
				html = null;
			}
			else if (bp.bypass.equalsIgnoreCase("pkchange"))
			{
				String htmContent = "data/html/passkey/change.htm";
				NpcHtmlMessage html = new NpcHtmlMessage(1);
				html.setFile(htmContent);
				activeChar.sendPacket(html);
				html = null;
			}
			else if (bp.bypass.startsWith("pkset"))
			{
				StringTokenizer st = new StringTokenizer(bp.bypass, "]");
				
				if (st.countTokens() != 5)
				{
					activeChar.sendMessage("Вы ввели не все данные!");
					String htmContent = "data/html/passkey/setup.htm";
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(htmContent);
					activeChar.sendPacket(html);
					html = null;
					return;
				}else{
                    String testToken;
                    int countTokens = st.countTokens();
                    for(int i = 0; i < countTokens; i++)
                    {
                        testToken = st.nextToken();
                        if(testToken.equals(" "))
                        {
                            activeChar.sendMessage("Вы ввели не все данные!");
                            String htmContent = "data/html/passkey/setup.htm";
                            NpcHtmlMessage html = new NpcHtmlMessage(1);
                            html.setFile(htmContent);
                            activeChar.sendPacket(html);
                            html = null;
                            return;
                        }
                    }
                }

                st = new StringTokenizer(bp.bypass, "]");

				String newCommand = st.nextToken();
				String pass1 = st.nextToken();
				pass1 = pass1.substring(1, pass1.length() - 1);
				String pass2 = st.nextToken();
				pass2 = pass2.substring(1, pass2.length() - 1);
				String question = st.nextToken();
				question = question.substring(1, question.length() - 1);
				String answer = st.nextToken();
				answer = answer.substring(1, answer.length());

				
				if (pass1 == null || pass2 == null || question == null || answer == null)
				{
					activeChar.sendMessage("Ошибка при вводе");
					String htmContent = "data/html/passkey/setup.htm";
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(htmContent);
					activeChar.sendPacket(html);
					html = null;
					return;
				}
				
				if (!pass1.equals(pass2))
				{
					activeChar.sendMessage("Вы ввели разные пароли");
					activeChar.sendMessage("pass1 = " + pass1);
					activeChar.sendMessage("pass2 = " + pass2);
					activeChar.sendMessage("Question = " + question);
					activeChar.sendMessage("answer = " + answer);
					
					String htmContent = "data/html/passkey/setup.htm";
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(htmContent);
					activeChar.sendPacket(html);
					html = null;
					return;
				}
				
				if(compare(pass1, getOldPassKey(activeChar)))
				{
					activeChar.sendMessage("Пароль не должен совпадать с паролем аккаунта");
					String htmContent = "data/html/passkey/setup.htm";
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(htmContent);
					activeChar.sendPacket(html);
					html = null;
					return;
				}

				insertPassKeyInformation(activeChar, pass1, question, answer);
				
				String htmContent = "data/html/passkey/login.htm";
				NpcHtmlMessage html = new NpcHtmlMessage(1);
				html.setFile(htmContent);
				activeChar.sendPacket(html);
				html = null;
			}
			else if (bp.bypass.startsWith("pklogin"))
			{
				StringTokenizer st = new StringTokenizer(bp.bypass, " ");
				if (st.countTokens() != 2)
				{
					activeChar.sendMessage("Вы допустили ошибку при вводе пароля!");
					String htmContent = "data/html/passkey/login.htm";
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(htmContent);
					activeChar.sendPacket(html);
					html = null;
					return;
				}
				String newCommand = st.nextToken();
				String pass = st.nextToken();

				ThreadConnection con = null;
				String query = "SELECT passkey FROM passkey WHERE obj_Id = ?";
				String pwdindb = "error";
				try
				{
					con = L2DatabaseFactory.getInstance().getConnection();
					FiltredPreparedStatement ps = con.prepareStatement(query);
					ps.setInt(1, activeChar.getObjectId());
					ResultSet rs = ps.executeQuery();

					while (rs.next())
						pwdindb = rs.getString(1);

					rs.close();
					ps.close();
					ps = null;
					rs = null;
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				finally
				{
					DbUtils.closeQuietly(con);
				}

				if (compare(pass, pwdindb))
				{
					activeChar.setPassCheck(true);
					activeChar.setPassParalyzedFalse();
				}
				else
				{
					activeChar.sendMessage("Вы ввели не правильный пароль");
					String htmContent = "data/html/passkey/login.htm";
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(htmContent);
					activeChar.sendPacket(html);
					html = null;
					return;
				}
			}
			else if (bp.bypass.startsWith("pkrec"))
			{
				StringTokenizer st = new StringTokenizer(bp.bypass, " ");
				if (st.countTokens() != 4)
				{
					activeChar.sendMessage("Вы допустили ошибку при вводе данных!");
					String htmContent = "data/html/passkey/recovery.htm";
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(htmContent);
					html.replace("%question%", getPassKeyQuestion(activeChar));
					activeChar.sendPacket(html);
					html = null;
					return;
				}

				String newCommand = st.nextToken();
				String answer = st.nextToken();
				String pass1 = st.nextToken();
				String pass2 = st.nextToken();

				if (!pass1.equals(pass2))
				{
					activeChar.sendMessage("Вы ввели разные пароли");
					String htmContent = "data/html/passkey/recovery.htm";
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(htmContent);
					html.replace("%question%", getPassKeyQuestion(activeChar));
					activeChar.sendPacket(html);
					html = null;
					return;
				}

				ThreadConnection con = null;
				String query = "SELECT answer FROM passkey WHERE obj_Id = ?";
				String anwindb = "error";
				try
				{
					con = L2DatabaseFactory.getInstance().getConnection();
					FiltredPreparedStatement ps = con.prepareStatement(query);
					ps.setInt(1, activeChar.getObjectId());
					ResultSet rs = ps.executeQuery();

					while (rs.next())
						anwindb = rs.getString(1);

					rs.close();
					ps.close();
					ps = null;
					rs = null;
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				finally
				{
					DbUtils.closeQuietly(con);
				}

				if (anwindb.equals(answer))
				{
					if(compare(pass1, getOldPassKey(activeChar)))
					{
                        activeChar.sendMessage("Пароль не должен совпадать с паролем аккаунта");
						String htmContent = "data/html/passkey/recovery.htm";
						NpcHtmlMessage html = new NpcHtmlMessage(1);
						html.setFile(htmContent);
						html.replace("%question%", getPassKeyQuestion(activeChar));
						activeChar.sendPacket(html);
						html = null;
						return;
					}
					updPassKey(activeChar, pass1);
					activeChar.sendMessage("Вы успешно изменили пароль.");
					String htmContent = "data/html/passkey/login.htm";
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(htmContent);
					activeChar.sendPacket(html);
					html = null;
				}
				else
				{
					activeChar.sendMessage("Вы ввели неправильный ответ на свой вопрос");
					String htmContent = "data/html/passkey/recovery.htm";
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(htmContent);
					html.replace("%question%", getPassKeyQuestion(activeChar));
					activeChar.sendPacket(html);
					html = null;
					return;
				}

			}
			else if (bp.bypass.startsWith("pkcha"))
			{
				StringTokenizer st = new StringTokenizer(bp.bypass, " ");
				if (st.countTokens() != 4)
				{
					activeChar.sendMessage("Вы допустили ошибку при вводе данных!");
					String htmContent = "data/html/passkey/change.htm";
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(htmContent);
					html.replace("%question%", getPassKeyQuestion(activeChar));
					activeChar.sendPacket(html);
					html = null;
					return;
				}

				String newCommand = st.nextToken();
				String oldpass1 = st.nextToken();
				String pass1 = st.nextToken();
				String pass2 = st.nextToken();

                ThreadConnection con = null;
                String query = "SELECT passkey FROM passkey WHERE obj_Id = ?";
                String pwdindb = "";
                try
                {
                    con = L2DatabaseFactory.getInstance().getConnection();
                    FiltredPreparedStatement ps = con.prepareStatement(query);
                    ps.setInt(1, activeChar.getObjectId());
                    ResultSet rs = ps.executeQuery();

                    while (rs.next())
                        pwdindb = rs.getString(1);

                    rs.close();
                    ps.close();
                    ps = null;
                    rs = null;
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    DbUtils.closeQuietly(con);
                }

                if(!compare(oldpass1, pwdindb))
                {
                    activeChar.sendMessage("Вы ввели не верный пароль");
                    String htmContent = "data/html/passkey/change.htm";
                    NpcHtmlMessage html = new NpcHtmlMessage(1);
                    html.setFile(htmContent);
                    html.replace("%question%", getPassKeyQuestion(activeChar));
                    activeChar.sendPacket(html);
                    html = null;
                    return;
                }

				if (!pass1.equals(pass2))
				{
					activeChar.sendMessage("Вы ввели разные пароли");
					String htmContent = "data/html/passkey/change.htm";
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(htmContent);
					html.replace("%question%", getPassKeyQuestion(activeChar));
					activeChar.sendPacket(html);
					html = null;
					return;
				}
				
				if(compare(pass1, getOldPassKey(activeChar)))
				{
					activeChar.sendMessage("Пароль не должен совпадать с паролем аккаунта");
					String htmContent = "data/html/passkey/change.htm";
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(htmContent);
					html.replace("%question%", getPassKeyQuestion(activeChar));
					activeChar.sendPacket(html);
					html = null;
					return;
				}
				
				if(oldpass1.equals(pass1))
				{
					activeChar.sendMessage("Пароль не должен совпадать сo старым паролем");
					String htmContent = "data/html/passkey/change.htm";
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(htmContent);
					html.replace("%question%", getPassKeyQuestion(activeChar));
					activeChar.sendPacket(html);
					html = null;
					return;
				}

				updPassKey(activeChar, pass1);
				activeChar.sendMessage("Вы успешно изменили пароль.");
				String htmContent = "data/html/passkey/login.htm";
				NpcHtmlMessage html = new NpcHtmlMessage(1);
				html.setFile(htmContent);
				activeChar.sendPacket(html);
				html = null;
			}
			// Navigate throught Manor windows
			else if(bp.bypass.startsWith("manor_menu_select?")) // Navigate throught Manor windows
			{
				final L2Object object = activeChar.getTarget();
				if(object != null && object.isNpc())
					((L2NpcInstance) object).onBypassFeedback(activeChar, bp.bypass);
			}
			else if(bp.bypass.startsWith("multisell "))
				L2Multisell.getInstance().separateAndSend(Integer.parseInt(bp.bypass.substring(10)), activeChar, 0);
			else if(bp.bypass.startsWith("Quest "))
			{
				final String p = bp.bypass.substring(6).trim();
				final int idx = p.indexOf(' ');
				if(idx < 0)
					activeChar.processQuestEvent(p, "", npc);
				else
					activeChar.processQuestEvent(p.substring(0, idx), p.substring(idx).trim(), npc);
			}
			else if(bp.bypass.startsWith("captcha_"))
				CaptchaValidator.processCaptchaBypass(bp.bypass, activeChar);
			else if(bp.bypass.startsWith("oly_"))
			{ 
				String id;
				id = bp.bypass.substring(4);
				
				if(!id.equals(""))
				{
					activeChar.leaveOlympiadObserverMode();
					Olympiad.addSpectator(Integer.parseInt(id), activeChar);
				}				
			}
		}
		catch(final Exception e)
		{
			String st = "Bad RequestBypassToServer: " + bp.bypass;
			if(activeChar.getTarget() instanceof L2NpcInstance)
				st = st + " via NPC #" + ((L2NpcInstance) activeChar.getTarget()).getNpcId();
			_log.log(Level.WARNING, st, e);
		}
	}

	/**
	 * @param client
	 */
	private void comeHere(final L2Player activeChar)
	{
		final L2Object obj = activeChar.getTarget();
		if(obj instanceof L2NpcInstance)
		{
			final L2NpcInstance temp = (L2NpcInstance) obj;
			temp.setTarget(activeChar);
			temp.moveToLocation(activeChar.getLoc(), 0, true);
		}
	}

	private void playerHelp(final L2Player activeChar, final String path)
	{
		final String filename = "data/html/" + path;
		final NpcHtmlMessage html = new NpcHtmlMessage(5);
		html.setFile(filename);
		activeChar.sendPacket(html);
	}

	private void updPassKey(L2Player player, String pass)
	{
		ThreadConnection con = null;
		String query = "UPDATE passkey SET passkey = ? WHERE obj_Id = ?";
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			FiltredPreparedStatement st = con.prepareStatement(query);
			st.setString(1, encrypt(pass));
			st.setInt(2, player.getObjectId());
			st.executeUpdate();
			st.close();
			st = null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con);
		}
	}

	private void insertPassKeyInformation(L2Player player, String pass, String question, String answer)
	{
		ThreadConnection con = null;
		String query = "INSERT INTO passkey (obj_Id, passkey, question, answer) VALUES (?,?,?,?)";
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			FiltredPreparedStatement st = con.prepareStatement(query);
			st.setInt(1, player.getObjectId());
			st.setString(2, encrypt(pass));
			st.setString(3, question);
			st.setString(4, answer);
			st.execute();
			st.close();
			st = null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con);
		}
	}

	private String getPassKeyQuestion(L2Player player)
	{
		ThreadConnection con = null;
		String query = "SELECT question FROM passkey WHERE obj_Id = ?";
		String question = "error";
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			FiltredPreparedStatement st = con.prepareStatement(query);
			st.setInt(1, player.getObjectId());
			ResultSet rs = st.executeQuery();
			
			while (rs.next())
				question = rs.getString(1);
			
			rs.close();
			st.close();
			st = null;
			rs = null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con);
		}
		
		return question;
	}

	public static boolean getPassKeyEnable(L2Player player)
	{
		ThreadConnection con = null;
		String query = "SELECT COUNT(*) FROM passkey WHERE obj_Id = ?";
		int count = 0;
		
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			FiltredPreparedStatement st = con.prepareStatement(query);
			st.setInt(1, player.getObjectId());
			ResultSet rs = st.executeQuery();
			
			while (rs.next())
				count = rs.getInt(1);
			
			rs.close();
			st.close();
			st = null;
			rs = null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con);
		}
		
		if (count == 1)
			return true;
		else
			return false;
	}
	
	private String getOldPassKey(L2Player player)
	{
		ThreadConnection con1 = null;
		ThreadConnection con2 = null;
		String query1 = "SELECT account_name FROM characters WHERE obj_Id = ?";
		String query2 = "SELECT password FROM accounts WHERE login = ?";
		String oldPass = null;
		String oldLogin = null;
		
		try
		{		
		
			con1 = L2DatabaseFactory.getInstance().getConnection();
			FiltredPreparedStatement st1 = con1.prepareStatement(query1);
			st1.setInt(1, player.getObjectId());
			ResultSet rs1 = st1.executeQuery();
			
			while (rs1.next())
				oldLogin = rs1.getString("account_name");
				
			con2 = L2DatabaseFactory.getInstanceLogin().getConnection();
			FiltredPreparedStatement st2 = con2.prepareStatement(query2);
			st2.setString(1, oldLogin);
			ResultSet rs2 = st2.executeQuery();
			
			while (rs2.next())
				oldPass = rs2.getString("password");
				
			rs1.close();
			st1.close();
			rs2.close();
			st2.close();
			st1 = null;
			rs1 = null;
			st2 = null;
			rs2 = null;
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con1);
			DbUtils.closeQuietly(con2);
		}
		
		return oldPass;
	}

    private boolean compare(String password, String expected)
    {
        try
        {
            return encrypt(password).equals(expected);
        }
        catch(NoSuchAlgorithmException nsee)
        {
            _log.log(Level.WARNING, "Could not check password, algorithm Whirlpool not found! Check jacksum library!", nsee);
            return false;
        }
        catch(UnsupportedEncodingException uee)
        {
            _log.log(Level.WARNING, "Could not check password, UTF-8 is not supported!", uee);
            return false;
        }
    }

    private String encrypt(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException
    {
        AbstractChecksum whirlpool2 = JacksumAPI.getChecksumInstance("whirlpool2");
        whirlpool2.setEncoding("BASE64");
        whirlpool2.update(password.getBytes("UTF-8"));
        return whirlpool2.format("#CHECKSUM");
    }
	
	@Override
	public String getType()
	{
		return _C__23_REQUESTBYPASSTOSERVER;
	}
}
