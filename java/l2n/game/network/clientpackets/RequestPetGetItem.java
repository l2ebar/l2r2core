package l2n.game.network.clientpackets;

import l2n.game.ai.CtrlIntention;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2PetInstance;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestPetGetItem extends L2GameClientPacket
{
	private static String _C__98_REQUESTPETGETITEM = "[C] 98 RequestPetGetItem";
	// format: cd
	private int _objectId;

	@Override
	public void readImpl()
	{
		_objectId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2ItemInstance item = L2ObjectsStorage.getItemByObjId(_objectId);
		if(item == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.getPet() instanceof L2PetInstance)
		{ // Если: время для поднятия итема хозяином != 0, хозяин итема задан, время для понятия итема ещё не вышло, игрок не является хозяином, игрок не в пати.
			if(item.getDropTimeOwner() != 0 && item.getItemDropOwner() != null && item.getDropTimeOwner() > System.currentTimeMillis() && activeChar != item.getItemDropOwner() && !activeChar.isInParty() || activeChar.isInParty() && item.getItemDropOwner().isInParty() && activeChar.getParty() != item.getItemDropOwner().getParty()) // или
			// если: игрок в пати, хозяин итема в пати, но они не в одной пати.
			{
				SystemMessage sm;
				if(item.getItemId() == 57)
				{
					sm = new SystemMessage(SystemMessage.YOU_HAVE_FAILED_TO_PICK_UP_S1_ADENA);
					sm.addNumber(item.getCount());
				}
				else
				{
					sm = new SystemMessage(SystemMessage.YOU_HAVE_FAILED_TO_PICK_UP_S1);
					sm.addItemName(item.getItemId());
				}
				sendPacket(sm);
				activeChar.sendActionFailed();
				return;
			}

			final L2PetInstance pet = (L2PetInstance) activeChar.getPet();
			if(pet == null || pet.isDead() || pet.isOutOfControl())
			{
				activeChar.sendActionFailed();
				return;
			}
			pet.getAI().setIntention(CtrlIntention.AI_INTENTION_PICK_UP, item, null);
		}
		else
		{
			activeChar.sendActionFailed();
			return;
		}
	}

	@Override
	public String getType()
	{
		return _C__98_REQUESTPETGETITEM;
	}
}
