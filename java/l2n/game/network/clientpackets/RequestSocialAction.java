package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SocialAction;
import l2n.util.Util;

public class RequestSocialAction extends L2GameClientPacket
{
	private static String _C__34_REQUESTSOCIALACTION = "[C] 34 RequestSocialAction";
	private int _actionId;

	/**
	 * packet type id 0x34
	 * format: cd
	 */
	@Override
	public void readImpl()
	{
		_actionId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.isOutOfControl() || activeChar.getTransformationId() != 0)
		{
			activeChar.sendActionFailed();
			return;
		}

		// You cannot do anything else while fishing
		if(activeChar.isFishing())
		{
			activeChar.sendPacket(Msg.YOU_CANNOT_DO_ANYTHING_ELSE_WHILE_FISHING);
			return;
		}

		// internal Social Action check . Added
		if(_actionId < 2 || _actionId > 14)
		{
			Util.handleIllegalPlayerAction(activeChar, "RequestSocialAction[43]", "Character " + activeChar.getName() + " at account " + activeChar.getAccountName() + "requested an internal Social Action " + _actionId, 1);
			return;
		}

		if(activeChar.getPrivateStoreType() == L2Player.STORE_PRIVATE_NONE && !activeChar.isInTransaction() && !activeChar.isActionsDisabled() && !activeChar.isSitting())
		{
			activeChar.broadcastPacket(new SocialAction(activeChar.getObjectId(), _actionId));
			if(Config.ALT_SOCIAL_ACTION_REUSE)
			{
				L2GameThreadPools.getInstance().scheduleAi(new SocialTask(activeChar), 2600, true);
				activeChar.block();
			}
		}
	}

	private class SocialTask implements Runnable
	{
		L2Player _player;

		SocialTask(final L2Player player)
		{
			_player = player;
		}

		@Override
		public void run()
		{
			_player.unblock();
		}
	}

	@Override
	public String getType()
	{
		return _C__34_REQUESTSOCIALACTION;
	}
}
