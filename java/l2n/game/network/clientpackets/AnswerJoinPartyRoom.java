package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.instancemanager.PartyRoomManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;

public class AnswerJoinPartyRoom extends L2GameClientPacket
{
	private final static String _C__D0_30_ANSWERJOINPARTYROOM = "[C] D0:30 AnswerJoinPartyRoom";
	private int _response; // not tested, just guessed

	/**
	 * @param buf
	 * @param client
	 *            format: (ch)d
	 */
	@Override
	public void readImpl()
	{
		if(_buf.hasRemaining())
			_response = readD();
		else
			_response = 0;
	}

	/**
	 * @see l2n.game.clientpackets.ClientBasePacket#runImpl()
	 */
	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final Transaction transaction = activeChar.getTransaction();

		if(transaction == null)
			return;
		if(!transaction.isValid() || !transaction.isTypeOf(Transaction.TransactionType.PARTY_ROOM))
		{
			transaction.cancel();
			activeChar.sendPacket(Msg.TIME_EXPIRED, Msg.ActionFail);
			return;
		}

		final L2Player requestor = transaction.getOtherPlayer(activeChar);
		transaction.cancel();

		if(_response == 1)
		{
			if(requestor.getPartyRoom() <= 0)
			{
				activeChar.sendActionFailed();
				return;
			}
			if(activeChar.getPartyRoom() > 0)
			{
				activeChar.sendActionFailed();
				return;
			}
			PartyRoomManager.getInstance().joinPartyRoom(activeChar, requestor.getPartyRoom());
		}
		else
			requestor.sendPacket(Msg.THE_PLAYER_DECLINED_TO_JOIN_YOUR_PARTY);

		// TODO проверить на наличие пакета ДОБАВЛЕНИЯ в список, в другом случае отсылать весь список всем мемберам
	}

	/**
	 * @see l2n.game.BasePacket#getType()
	 */
	@Override
	public String getType()
	{
		return _C__D0_30_ANSWERJOINPARTYROOM;
	}
}
