/**
 * 
 */
package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;

/**
 * @author L2System
 * @date 15.12.2009
 * @time 23:03:42
 */
public class RequestModifyBookMarkSlot extends L2GameClientPacket
{
	private static final String _C__51_REQUESTMODIFYBOOKMARKSLOT = "[C] d0:51:02 RequestModifyBookMarkSlot";

	private int id, icon;
	private String name, tag;

	@Override
	protected void readImpl()
	{
		id = readD();
		name = readS(32);
		icon = readD();
		tag = readS(4);
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		activeChar.teleportBookmarkModify(id, icon, tag, name);
	}

	@Override
	public String getType()
	{
		return _C__51_REQUESTMODIFYBOOKMARKSLOT;
	}
}
