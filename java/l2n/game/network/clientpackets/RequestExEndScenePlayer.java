package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;

public class RequestExEndScenePlayer extends L2GameClientPacket
{
	private int _moviveId;

	@Override
	protected void readImpl()
	{
		_moviveId = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player cha = getClient().getActiveChar();
		if(cha == null)
			return;

		if(cha.getMovieId() != _moviveId)
		{
			cha.sendActionFailed();
			return;
		}

		cha.setMovieId(0);
		cha.decayMe();
		cha.spawnMe();
	}
}
