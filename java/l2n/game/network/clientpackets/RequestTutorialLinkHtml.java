package l2n.game.network.clientpackets;

import l2n.game.instancemanager.QuestManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.quest.Quest;

public class RequestTutorialLinkHtml extends L2GameClientPacket
{
	private static String _C__85_REQUESTTUTORIALLINKHTML = "[C] 85 RequestTutorialLinkHtml";

	// format: cS

	String _bypass;

	@Override
	public void readImpl()
	{
		_bypass = readS();
	}

	@Override
	public void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;

		final Quest q = QuestManager.getQuest(255);
		if(q != null)
			player.processQuestEvent(q.getName(), _bypass, null);
	}

	@Override
	public String getType()
	{
		return _C__85_REQUESTTUTORIALLINKHTML;
	}
}
