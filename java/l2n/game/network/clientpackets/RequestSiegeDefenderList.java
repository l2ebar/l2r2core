package l2n.game.network.clientpackets;

import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.ClanHallManager;
import l2n.game.instancemanager.FortressManager;
import l2n.game.model.entity.residence.Residence;
import l2n.game.network.serverpackets.SiegeDefenderList;

public class RequestSiegeDefenderList extends L2GameClientPacket
{
	private static String _C__a3_RequestSiegeDefenderList = "[C] a3 RequestSiegeDefenderList";

	private int _unitId;

	@Override
	public void readImpl()
	{
		_unitId = readD();
	}

	@Override
	public void runImpl()
	{
		Residence unit = CastleManager.getInstance().getCastleByIndex(_unitId);
		if(unit == null)
			unit = FortressManager.getInstance().getFortressByIndex(_unitId);
		if(unit == null)
			unit = ClanHallManager.getInstance().getClanHall(_unitId);
		if(unit != null)
			sendPacket(new SiegeDefenderList(unit));
	}

	@Override
	public String getType()
	{
		return _C__a3_RequestSiegeDefenderList;
	}
}
