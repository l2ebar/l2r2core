package l2n.game.network.clientpackets;

/**
 * Lets drink to code!
 */
public class DummyPacket extends L2GameClientPacket
{
	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		_log.warning("DummyPacket? Disconnect this user");
		getClient().closeNow(false);
	}

	@Override
	public String getType()
	{
		return "DummyPacket";
	}
}
