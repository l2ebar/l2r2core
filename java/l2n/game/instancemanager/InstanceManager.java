package l2n.game.instancemanager;

import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TLongObjectHashMap;
import javolution.text.TypeFormat;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.util.Crontab;
import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.*;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.Instance;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.DoorTable;
import l2n.game.tables.NpcTable;
import l2n.game.tables.TerritoryTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author L2System Project
 * @date 13.08.2010
 * @time 17:07:40
 */
public class InstanceManager
{
	public static class SpawnInfo
	{
		private final int _locationId;
		private final L2Spawn _spawn;
		private final int _type;

		public SpawnInfo(final int locationId, final L2Spawn spawn, final int type)
		{
			_locationId = locationId;
			_spawn = spawn;
			_type = type;
		}

		public int getLocationId()
		{
			return _locationId;
		}

		public L2Spawn getSpawn()
		{
			return _spawn;
		}

		public int getType()
		{
			return _type;
		}
	}

	public abstract static class InstanceWorld
	{}

	private final static Logger _log = Logger.getLogger(InstanceManager.class.getName());

	private final Lock _worldsReadLock;
	private final Lock _worldsWriteLock;
	private final TLongObjectHashMap<InstanceWorld> _instanceWorlds;

	/** список инстансов <"id инстанса", TIntObjectHashMap<"id локации", "данные инстанса">> */
	private final TIntObjectHashMap<TIntObjectHashMap<Instance>> _instancedZones = new TIntObjectHashMap<TIntObjectHashMap<Instance>>();

	private final static GArray<String> _names = new GArray<String>();

	private int countGood = 0;
	private int countBad = 0;
	private int countGoodDoor = 0;
	private int countBadDoor = 0;

	public static InstanceManager getInstance()
	{
		return SingletonHolder._instance;
	}

	public void addWorld(final long reflectionId, final InstanceWorld instanceWorld)
	{
		_worldsWriteLock.lock();
		try
		{
			_instanceWorlds.put(reflectionId, instanceWorld);
		}
		finally
		{
			_worldsWriteLock.unlock();
		}
	}

	public InstanceWorld removeWorld(final long reflectionId)
	{
		_worldsWriteLock.lock();
		try
		{
			return _instanceWorlds.remove(reflectionId);
		}
		finally
		{
			_worldsWriteLock.unlock();
		}
	}

	@SuppressWarnings("unchecked")
	public <T> T getWorld(final long reflectionId, final Class<?> clazz)
	{
		_worldsReadLock.lock();
		try
		{
			InstanceWorld w;
			if((w = _instanceWorlds.get(reflectionId)) != null)
				if(w.getClass().isAssignableFrom(clazz))
					return (T) w;
		}
		finally
		{
			_worldsReadLock.unlock();
		}
		return null;
	}

	public InstanceManager()
	{
		final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
		_worldsReadLock = lock.readLock();
		_worldsWriteLock = lock.writeLock();
		_instanceWorlds = new TLongObjectHashMap<InstanceWorld>();

		final GArray<File> files = new GArray<File>();
		getFiles("instances", files);
		for(final File f : files)
			load(f);

		printInfo();
	}

	public TIntObjectHashMap<Instance> getById(final int id)
	{
		return _instancedZones.get(id);
	}

	/**
	 * Возвращает сброс реюза в виде Crontab
	 */
	private Crontab getResetReuseByName(final String name)
	{
		for(final TIntObjectHashMap<Instance> ils : _instancedZones.valueCollection())
			if(ils != null)
			{
				final Instance il = ils.get(0);
				if(il.getName().equals(name))
					return il.getResetReuse();
			}
		return null;
	}

	/**
	 * Вычисляет через сколько (минут?) player сможет войти в инстанс зону
	 * 
	 * @param name
	 *            имя инстанса
	 * @param player
	 *            игрок
	 * @return минуты?
	 */
	public int getTimeToNextEnterInstance(final String name, final L2Player player)
	{
		if(player.isGM())
			return 0;

		final Crontab resetReuse = getResetReuseByName(name);
		if(resetReuse == null)
			return 0;
		final String var = player.getVar(name);
		if(var == null)
			return 0;
		return (int) Math.max((resetReuse.timeNextUsage(Long.parseLong(var)) - System.currentTimeMillis()) / 60000, 0);
	}

	// Получаем список файлов
	private void getFiles(final String dirname, final GArray<File> hash)
	{
		final File dir = new File(Config.DATAPACK_ROOT, "data/" + dirname);
		if(!dir.exists())
		{
			_log.config("Dir " + dir.getAbsolutePath() + " not exists");
			return;
		}

		final File[] files = dir.listFiles();
		for(final File f : files)
			if(f.getName().endsWith(".xml"))
				hash.add(f);
			else if(f.isDirectory() && !f.getName().equals(".svn"))
				getFiles("instances/" + f.getName(), hash);
	}

	public void reload()
	{
		for(final int b : _instancedZones.keys())
			_instancedZones.get(b).clear();

		final GArray<File> files = new GArray<File>();
		_instancedZones.clear();
		getFiles("instances", files);
		for(final File f : files)
			load(f);

		printInfo();
	}

	public void printInfo()
	{
		final int locSize = _instancedZones.size();
		int roomSize = 0;

		for(final TIntObjectHashMap<Instance> list : _instancedZones.valueCollection())
			roomSize += list.size();

		_log.info("InstancedManager: Loaded " + locSize + " zones with " + roomSize + " rooms.");
		_log.info("InstancedManager: Loaded " + countGood + " instanced location spawns, " + countBad + " errors.");
		_log.info("InstancedManager: Loaded " + countGoodDoor + " doors, " + countBadDoor + " errors.");
	}

	/**
	 * Возвращает массив униканых имен инстансов
	 */
	public GArray<String> getNames()
	{
		return _names;
	}

	/**
	 * Проверяет возможность создания нового инстанса
	 * 
	 * @param id
	 *            - ID иснанс зоны
	 * @param player
	 *            - кто создаёт
	 * @param checkReuse
	 *            - проверять ли лимиты времени
	 * @param quest_name
	 *            - квест должен быть взят
	 * @param quest_name_complated
	 *            - квест должен быть закончен
	 * @return
	 */
	public static SystemMessage checkCondition(final int id, final L2Player player, final boolean checkReuse, final String quest_name, final String quest_name_complated)
	{
		final InstanceManager izm = InstanceManager.getInstance();
		if(izm == null)
			return Msg.SYSTEM_ERROR;

		final TIntObjectHashMap<Instance> izs = izm.getById(id);
		if(izs == null)
			return Msg.SYSTEM_ERROR;

		final Instance il = izs.get(0);
		assert il != null;

		final String name = il.getName();
		final int min_level = il.getMinLevel();
		final int max_level = il.getMaxLevel();
		final int min_users = il.getMinParty();
		final int max_users = il.getMaxParty();

		final boolean checkChannel = il.isNeedChannel() || (player.getParty() != null && player.getParty().getCommandChannel() != null);
		final boolean checkParty = min_users > 1 && !checkChannel;

		if(checkReuse)
			if(izm.getTimeToNextEnterInstance(name, player) > 0)
				return new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(player);

		// проверяем группу
		if(checkParty)
		{
			// если не в группе
			if(!player.isInParty())
				return Msg.YOU_ARE_NOT_CURRENTLY_IN_A_PARTY_SO_YOU_CANNOT_ENTER;

			final L2Party party = player.getParty();
			// если не лидер группы
			if(!party.isLeader(player))
				return Msg.ONLY_A_PARTY_LEADER_CAN_TRY_TO_ENTER;

			// проверяем лимиты количества человек
			if(party.getMemberCount() < min_users || party.getMemberCount() > max_users)
				return Msg.YOU_CANNOT_ENTER_DUE_TO_THE_PARTY_HAVING_EXCEEDED_THE_LIMIT;

			QuestState qs;
			final GArray<L2Player> members = party.getPartyMembers();
			for(final L2Player member : members)
			{
				// проверяем взял ли этот квест
				if(quest_name != null && !quest_name.isEmpty())
				{
					qs = member.getQuestState(quest_name);
					if(qs == null || qs.getState() != Quest.STARTED)
						return new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member);
				}
				// проверяем закончен ли квест
				if(quest_name_complated != null && !quest_name_complated.isEmpty())
				{
					qs = member.getQuestState(quest_name_complated);
					if(qs == null || !qs.isCompleted())
						return new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member);
				}

				// проверяем лимиты на уровни игроков
				if(member.getLevel() < min_level || member.getLevel() > max_level)
				{
					final SystemMessage sm = new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member);
					member.sendPacket(sm);
					return sm;
				}

				if(member.isCursedWeaponEquipped() || member.isInFlyingTransform() || member.isDead())
					return new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member);

				// проверяем откат
				if(izm.getTimeToNextEnterInstance(name, member) > 0)
					return new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(member);

				// проверяем расстояние
				if(!player.isInRange(member, 500))
				{
					member.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
					return Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK;
				}
			}
		}
		else if(checkChannel)
		{
			if(player.getParty() == null)
				return Msg.YOU_ARE_NOT_CURRENTLY_IN_A_PARTY_SO_YOU_CANNOT_ENTER;
			if(player.getParty().getCommandChannel() == null)
				return Msg.YOU_CANNOT_ENTER_BECAUSE_YOU_ARE_NOT_ASSOCIATED_WITH_THE_CURRENT_COMMAND_CHANNEL;

			final L2CommandChannel channel = player.getParty().getCommandChannel();
			if(channel.getChannelLeader().getObjectId() != player.getObjectId())
				return Msg.ONLY_A_PARTY_LEADER_CAN_TRY_TO_ENTER;

			// проверяем лимиты количества человек
			if(channel.getMemberCount() < min_users)
				return SystemMessage.sendString("The command channel must contains at least " + min_users + " members.");
			if(channel.getMemberCount() > max_users)
				return SystemMessage.sendString("The command channel must contains not more than " + max_users + " members.");

			QuestState qs;
			final GArray<L2Player> members = channel.getMembers();
			for(final L2Player member : members)
			{
				// проверяем взял ли этот квест
				if(quest_name != null && !quest_name.isEmpty())
				{
					qs = member.getQuestState(quest_name);
					if(qs == null || qs.getState() != Quest.STARTED)
						return new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member);
				}
				// проверяем закончен ли квест
				if(quest_name_complated != null && !quest_name_complated.isEmpty())
				{
					qs = member.getQuestState(quest_name_complated);
					if(qs == null || !qs.isCompleted())
						return new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member);
				}

				// проверяем лимиты на уровни игроков
				if(member.getLevel() < min_level || member.getLevel() > max_level)
				{
					final SystemMessage sm = new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member);
					member.sendPacket(sm);
					return sm;
				}

				if(member.isCursedWeaponEquipped() || member.isInFlyingTransform() || member.isDead())
					return new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member);

				// проверяем откат
				if(izm.getTimeToNextEnterInstance(name, member) > 0)
					return new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(member);

				// проверяем расстояние
				if(!player.isInRange(member, 500))
				{
					final SystemMessage sm = new SystemMessage(SystemMessage.C1_IS_IN_A_LOCATION_WHICH_CANNOT_BE_ENTERED_THEREFORE_IT_CANNOT_BE_PROCESSED);
					sm.addName(member);
					member.sendPacket(sm);
					return sm;
				}
			}
		}

		return null;
	}

	public void load(final File file)
	{
		try
		{
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);

			if(!file.exists())
				throw new IOException();

			int spawnType = 0;
			L2Spawn spawnDat = null;

			int min_level = 0;
			int max_level = 0;
			int min_user = 1;
			int max_user = 9;
			int duration = -1; // min
			boolean remove_buff = true;
			boolean needChannel = false;

			Location teleLoc = new Location();
			Location retLoc = new Location();

			/** resetReuse в формате Cron (http://ru.wikipedia.org/wiki/Cron) */
			Crontab resetReuse = new Crontab("0 0 * * *"); // Сброс реюза по умолчанию в каждые сутки в 0:00

			StatsSet set;
			final Document doc = factory.newDocumentBuilder().parse(file);
			for(Node iz = doc.getFirstChild(); iz != null; iz = iz.getNextSibling())
				if("list".equalsIgnoreCase(iz.getNodeName()))
					for(Node area = iz.getFirstChild(); area != null; area = area.getNextSibling())
						if("instance".equalsIgnoreCase(area.getNodeName()))
						{
							NamedNodeMap attrs = area.getAttributes();

							final int instanceId = XMLUtil.getAttributeIntValue(area, "id");
							final String name = XMLUtil.getAttributeValue(area, "name");
							if(!_names.contains(name))
								_names.add(name);

							resetReuse = new Crontab(attrs.getNamedItem("resetReuse").getNodeValue());
							duration = XMLUtil.getAttributeIntValue(area, "timelimit", -1);
							remove_buff = XMLUtil.getAttributeBooleanValue(area, "dispellBuffs", true);

							// check for duplicate instanced location IDs
							if(getById(instanceId) != null)
							{
								_log.warning("InstancedLocationManager load error: File [" + file.getName() + "]: already have instanced location bound to ID " + instanceId + "! Skipping...");
								continue;
							}

							for(Node room = area.getFirstChild(); room != null; room = room.getNextSibling())
								if("level".equalsIgnoreCase(room.getNodeName()))
								{
									min_level = XMLUtil.getAttributeIntValue(room, "min");
									max_level = XMLUtil.getAttributeIntValue(room, "max");
								}
								else if("party".equalsIgnoreCase(room.getNodeName()))
								{
									min_user = XMLUtil.getAttributeIntValue(room, "min");
									max_user = XMLUtil.getAttributeIntValue(room, "max");
								}
								else if("channel".equalsIgnoreCase(room.getNodeName()))
									needChannel = XMLUtil.getAttributeBooleanValue(room, "val", false);
								else if("return".equalsIgnoreCase(room.getNodeName()))
									retLoc = new Location(room.getAttributes().getNamedItem("loc").getNodeValue());

								else if("location".equalsIgnoreCase(room.getNodeName()))
								{
									attrs = room.getAttributes();
									final int roomId = XMLUtil.getAttributeIntValue(room, "id");

									for(Node coord = room.getFirstChild(); coord != null; coord = coord.getNextSibling())
										if("teleport".equalsIgnoreCase(coord.getNodeName()))
											teleLoc = new Location(coord.getAttributes().getNamedItem("loc").getNodeValue());

									// add location to InstancedLocation manager
									if(!_instancedZones.containsKey(instanceId))
										_instancedZones.put(instanceId, new TIntObjectHashMap<Instance>());

									set = new StatsSet();
									set.set("name", name);
									set.set("duration", duration);
									set.set("min_level", min_level);
									set.set("max_level", max_level);
									set.set("min_user", min_user);
									set.set("max_user", max_user);
									set.set("remove_buff", remove_buff);
									set.set("channel", needChannel);
									_instancedZones.get(instanceId).put(roomId, new Instance(set, teleLoc, retLoc, resetReuse));

									// load spawns
									for(Node spawn = room.getFirstChild(); spawn != null; spawn = spawn.getNextSibling())
										if("spawn".equalsIgnoreCase(spawn.getNodeName()))
										{
											attrs = spawn.getAttributes();

											final String[] mobs = attrs.getNamedItem("mobId").getNodeValue().split(" ");

											final int respawn = XMLUtil.getAttributeIntValue(spawn, "respawn", 0);
											final int respawnRnd = XMLUtil.getAttributeIntValue(spawn, "respawnRnd", 0);
											final int count = XMLUtil.getAttributeIntValue(spawn, "count", 1);

											final Node spawnTypeNode = attrs.getNamedItem("type");
											if(spawnTypeNode == null || spawnTypeNode.getNodeValue().equalsIgnoreCase("point"))
												spawnType = 0;
											else if(spawnTypeNode.getNodeValue().equalsIgnoreCase("rnd"))
												spawnType = 1;
											else if(spawnTypeNode.getNodeValue().equalsIgnoreCase("loc"))
												spawnType = 2;
											else
											{
												spawnType = 0;
												_log.warning("Spawn type  '" + spawnTypeNode.getNodeValue() + "' is unknown!");
											}

											final int locId = IdFactory.getInstance().getNextId();
											final L2Territory territory = new L2Territory(locId);
											for(Node location = spawn.getFirstChild(); location != null; location = location.getNextSibling())
												if("coords".equalsIgnoreCase(location.getNodeName()))
													territory.add(new Location(location.getAttributes().getNamedItem("loc").getNodeValue()));
											TerritoryTable.getInstance().getLocations().put(locId, territory);
											L2World.addTerritory(territory);

											for(final String mob : mobs)
											{
												final int mobId = TypeFormat.parseInt(mob);
												final L2NpcTemplate template = NpcTable.getTemplate(mobId);
												if(template == null)
													_log.warning("Template " + mobId + " not found!");
												if(template != null && _instancedZones.containsKey(instanceId) && _instancedZones.get(instanceId).containsKey(roomId))
												{
													spawnDat = new L2Spawn(template);
													spawnDat.setLocation(locId);
													spawnDat.setRespawnDelay(respawn, respawnRnd);
													spawnDat.setAmount(count);
													if(respawn > 0)
														spawnDat.startRespawn();
													_instancedZones.get(instanceId).get(roomId).getSpawnsInfo().add(new SpawnInfo(locId, spawnDat, spawnType));
													countGood++;
												}
												else
													countBad++;
											}
										}
								}
								else // load doors
								if("door".equalsIgnoreCase(room.getNodeName()))
								{
									final int doorId = XMLUtil.getAttributeIntValue(room, "id");
									final boolean opened = XMLUtil.getAttributeBooleanValue(room, "opened", false);
									final boolean invul = XMLUtil.getAttributeBooleanValue(room, "invul", false);

									final L2DoorInstance newDoor = DoorTable.getInstance().getDoor(doorId);
									if(newDoor == null)
									{
										_log.warning("Door " + doorId + " not found!");
										countBadDoor++;
									}
									else
									{
										final L2DoorInstance door = newDoor.clone();
										door.setHPVisible(!invul);
										door.setInvul(invul);
										door.setOpen(opened);
										door.setOpen(opened);
										_instancedZones.get(instanceId).get(0).getDoors().add(door);
										countGoodDoor++;
									}
								}
						}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Error on loading instances: file - " + file.getName() + ": " + e.getMessage(), e);
		}
	}

	private static class SingletonHolder
	{
		private static final InstanceManager _instance = new InstanceManager();
	}
}
