package l2n.game.instancemanager.itemauction;

/**
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 16.09.2011
 * @time 6:38:24
 */
public enum ItemAuctionState
{
	CREATED((byte) 0),
	STARTED((byte) 1),
	FINISHED((byte) 2);

	private final byte _stateId;

	private ItemAuctionState(final byte stateId)
	{
		_stateId = stateId;
	}

	public byte getStateId()
	{
		return _stateId;
	}

	public static final ItemAuctionState stateForStateId(final byte stateId)
	{
		for(final ItemAuctionState state : ItemAuctionState.values())
			if(state.getStateId() == stateId)
				return state;
		return null;
	}
}
