package l2n.game.instancemanager;

import l2n.commons.list.GArray;
import l2n.game.model.L2Spawn;
import l2n.game.model.entity.SevenSigns;

import java.util.logging.Logger;

public class CatacombSpawnManager
{
	private static Logger _log = Logger.getLogger(CatacombSpawnManager.class.getName());

	private static CatacombSpawnManager _instance;
	private static GArray<L2Spawn> _dawnMobs = new GArray<L2Spawn>();
	private static GArray<L2Spawn> _duskMobs = new GArray<L2Spawn>();

	private static int _currentState = 0; // 0 = Undefined, 1 = Dawn, 2 = Dusk

	public static CatacombSpawnManager getInstance()
	{
		if(_instance == null)
			_instance = new CatacombSpawnManager();

		return _instance;
	}

	public void addDawnMob(L2Spawn spawnDat)
	{
		_dawnMobs.add(spawnDat);
	}

	public void addDuskMob(L2Spawn spawnDat)
	{
		_duskMobs.add(spawnDat);
	}

	public void changeMode(int mode)
	{
		if(_currentState == mode)
			return;

		_currentState = mode;

		switch (mode)
		{
			case 0: // all spawns
				deleteMobs(_dawnMobs);
				deleteMobs(_duskMobs);
				spawnMobs(_duskMobs);
				spawnMobs(_dawnMobs);
				break;
			case 1: // dusk spawns
				deleteMobs(_dawnMobs);
				deleteMobs(_duskMobs);
				spawnMobs(_duskMobs);
				break;
			case 2: // dawn spawns
				deleteMobs(_dawnMobs);
				deleteMobs(_duskMobs);
				spawnMobs(_dawnMobs);
				break;
			default:
				_log.warning("DayNightSpawnManager: Wrong mode sent");
				break;
		}
	}

	public void notifyChangeMode()
	{
		if(SevenSigns.getInstance().getCurrentPeriod() == SevenSigns.PERIOD_SEAL_VALIDATION)
			changeMode(SevenSigns.getInstance().getCabalHighestScore());
		else
			changeMode(0);
	}

	public void cleanUp()
	{
		deleteMobs(_duskMobs);
		deleteMobs(_dawnMobs);

		_duskMobs.clear();
		_dawnMobs.clear();
	}

	public void spawnMobs(GArray<L2Spawn> mobsSpawnsList)
	{
		for(L2Spawn spawnDat : mobsSpawnsList)
		{
			if(_currentState == 0)
				spawnDat.restoreAmount();
			else
				spawnDat.setAmount(spawnDat.getAmount() * 2);

			spawnDat.init();
		}
	}

	public static void deleteMobs(GArray<L2Spawn> mobsSpawnsList)
	{
		for(L2Spawn spawnDat : mobsSpawnsList)
			spawnDat.despawnAll();
	}
}
