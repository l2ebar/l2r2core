package l2n.game.instancemanager;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Manor;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.items.Warehouse;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ClanTable;
import l2n.util.Log;
import l2n.util.Rnd;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.logging.Logger;

public class CastleManorManager
{
	private final static Logger _log = Logger.getLogger(CastleManorManager.class.getName());

	private static CastleManorManager _instance;

	public static final int PERIOD_CURRENT = 0;
	public static final int PERIOD_NEXT = 1;
	protected static final String var_name = "ManorApproved";

	private static final String CASTLE_MANOR_LOAD_PROCURE = "SELECT * FROM castle_manor_procure WHERE castle_id=?";
	private static final String CASTLE_MANOR_LOAD_PRODUCTION = "SELECT * FROM castle_manor_production WHERE castle_id=?";

	private static final int NEXT_PERIOD_APPROVE = Config.MANOR_APPROVE_TIME; // 6:00
	private static final int NEXT_PERIOD_APPROVE_MIN = Config.MANOR_APPROVE_MIN; //
	private static final int MANOR_REFRESH = Config.MANOR_REFRESH_TIME; // 20:00
	private static final int MANOR_REFRESH_MIN = Config.MANOR_REFRESH_MIN; //
	protected static final long MAINTENANCE_PERIOD = Config.MANOR_MAINTENANCE_PERIOD / 60000; // 6 mins

	private boolean _underMaintenance;
	private boolean _disabled;

	public static CastleManorManager getInstance()
	{
		if(_instance == null)
		{
			_log.info("Manor System: Initializing...");
			_instance = new CastleManorManager();
		}
		return _instance;
	}

	public class CropProcure
	{
		int _cropId;
		long _buyResidual;
		int _rewardType;
		long _buy;
		long _price;

		public CropProcure(final int id)
		{
			_cropId = id;
			_buyResidual = 0;
			_rewardType = 0;
			_buy = 0;
			_price = 0;
		}

		public CropProcure(final int id, final long amount, final int type, final long buy, final long price)
		{
			_cropId = id;
			_buyResidual = amount;
			_rewardType = type;
			_buy = buy;
			_price = price;
		}

		public int getReward()
		{
			return _rewardType;
		}

		public int getId()
		{
			return _cropId;
		}

		public long getAmount()
		{
			return _buyResidual;
		}

		public long getStartAmount()
		{
			return _buy;
		}

		public long getPrice()
		{
			return _price;
		}

		public void setAmount(final long amount)
		{
			_buyResidual = amount;
		}
	}

	public class SeedProduction
	{
		int _seedId;
		long _residual;
		long _price;
		long _sales;

		public SeedProduction(final int id)
		{
			_seedId = id;
			_sales = 0;
			_price = 0;
			_sales = 0;
		}

		public SeedProduction(final int id, final long amount, final long price, final long sales)
		{
			_seedId = id;
			_residual = amount;
			_price = price;
			_sales = sales;
		}

		public int getId()
		{
			return _seedId;
		}

		public long getCanProduce()
		{
			return _residual;
		}

		public long getPrice()
		{
			return _price;
		}

		public long getStartProduce()
		{
			return _sales;
		}

		public void setCanProduce(final long amount)
		{
			_residual = amount;
		}
	}

	private CastleManorManager()
	{
		load(); // load data from database
		init(); // schedule all manor related events
		_underMaintenance = false;
		_disabled = !Config.ALLOW_MANOR;
		for(final Castle c : CastleManager.getInstance().getCastles().values())
			c.setNextPeriodApproved(ServerVariables.getBool(var_name));
	}

	private void load()
	{
		ThreadConnection con = null;
		try
		{
			FiltredPreparedStatement statement = null;
			ResultSet rs = null;

			// Get Connection
			con = L2DatabaseFactory.getInstance().getConnection();
			for(final Castle castle : CastleManager.getInstance().getCastles().values())
			{
				final GArray<SeedProduction> production = new GArray<SeedProduction>();
				final GArray<SeedProduction> productionNext = new GArray<SeedProduction>();
				final GArray<CropProcure> procure = new GArray<CropProcure>();
				final GArray<CropProcure> procureNext = new GArray<CropProcure>();

				// restore seed production info
				statement = con.prepareStatement(CASTLE_MANOR_LOAD_PRODUCTION);
				statement.setInt(1, castle.getId());
				rs = statement.executeQuery();
				while (rs.next())
				{
					final int seedId = rs.getInt("seed_id");
					final int canProduce = rs.getInt("can_produce");
					final int startProduce = rs.getInt("start_produce");
					final int price = rs.getInt("seed_price");
					final int period = rs.getInt("period");
					if(period == PERIOD_CURRENT)
						production.add(new SeedProduction(seedId, canProduce, price, startProduce));
					else
						productionNext.add(new SeedProduction(seedId, canProduce, price, startProduce));
				}
				DbUtils.closeQuietly(statement, rs);

				castle.setSeedProduction(production, PERIOD_CURRENT);
				castle.setSeedProduction(productionNext, PERIOD_NEXT);

				// restore procure info
				statement = con.prepareStatement(CASTLE_MANOR_LOAD_PROCURE);
				statement.setInt(1, castle.getId());
				rs = statement.executeQuery();
				while (rs.next())
				{
					final int cropId = rs.getInt("crop_id");
					final int canBuy = rs.getInt("can_buy");
					final int startBuy = rs.getInt("start_buy");
					final int rewardType = rs.getInt("reward_type");
					final int price = rs.getInt("price");
					final int period = rs.getInt("period");
					if(period == PERIOD_CURRENT)
						procure.add(new CropProcure(cropId, canBuy, rewardType, startBuy, price));
					else
						procureNext.add(new CropProcure(cropId, canBuy, rewardType, startBuy, price));
				}
				DbUtils.closeQuietly(statement, rs);

				castle.setCropProcure(procure, PERIOD_CURRENT);
				castle.setCropProcure(procureNext, PERIOD_NEXT);

				if(!procure.isEmpty() || !procureNext.isEmpty() || !production.isEmpty() || !productionNext.isEmpty())
					_log.info("Manor System: Loaded data for " + castle.getName() + " castle");
			}
		}
		catch(final Exception e)
		{
			_log.info("Manor System: Error restoring manor data: " + e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con);
		}
	}

	protected void init()
	{
		if(ServerVariables.getString(var_name, "").isEmpty())
		{
			final Calendar manorRefresh = Calendar.getInstance();
			manorRefresh.set(Calendar.HOUR_OF_DAY, MANOR_REFRESH);
			manorRefresh.set(Calendar.MINUTE, MANOR_REFRESH_MIN);
			manorRefresh.set(Calendar.SECOND, 0);
			manorRefresh.set(Calendar.MILLISECOND, 0);

			final Calendar periodApprove = Calendar.getInstance();
			periodApprove.set(Calendar.HOUR_OF_DAY, NEXT_PERIOD_APPROVE);
			periodApprove.set(Calendar.MINUTE, NEXT_PERIOD_APPROVE_MIN);
			periodApprove.set(Calendar.SECOND, 0);
			periodApprove.set(Calendar.MILLISECOND, 0);
			final boolean isApproved = periodApprove.getTimeInMillis() < Calendar.getInstance().getTimeInMillis() && manorRefresh.getTimeInMillis() > Calendar.getInstance().getTimeInMillis();
			ServerVariables.set(var_name, isApproved);
		}

		final Calendar FirstDelay = Calendar.getInstance();
		FirstDelay.set(Calendar.SECOND, 0);
		FirstDelay.set(Calendar.MILLISECOND, 0);
		FirstDelay.add(Calendar.MINUTE, 1);
		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new ManorTask(), FirstDelay.getTimeInMillis() - Calendar.getInstance().getTimeInMillis(), 60000);
	}

	public void setNextPeriod()
	{
		for(final Castle c : CastleManager.getInstance().getCastles().values())
		{
			c.setNextPeriodApproved(false);

			if(c.getOwnerId() <= 0)
				continue;

			final L2Clan clan = ClanTable.getInstance().getClan(c.getOwnerId());
			if(clan == null)
				continue;

			final Warehouse cwh = clan.getWarehouse();

			for(final CropProcure crop : c.getCropProcure(PERIOD_CURRENT))
			{
				if(crop.getStartAmount() == 0)
					continue;

				// adding bought crops to clan warehouse
				if(crop.getStartAmount() > crop.getAmount())
				{
					long count = crop.getStartAmount() - crop.getAmount();

					count = count * 90 / 100;
					if(count < 1 && Rnd.get(99) < 90)
						count = 1;

					if(count > 0)
					{
						final int id = L2Manor.getInstance().getMatureCrop(crop.getId());
						cwh.addItem(id, count, "CastleManorManager: setNextPeriod()");
					}
				}

				// reserved and not used money giving back to treasury
				if(crop.getAmount() > 0)
				{
					c.addToTreasuryNoTax(crop.getAmount() * crop.getPrice(), false, false);
					Log.add(c.getName() + "|" + crop.getAmount() * crop.getPrice() + "|ManorManager", "treasury");
				}

				c.setCollectedShops(0);
				c.setCollectedSeed(0);
			}

			c.setSeedProduction(c.getSeedProduction(PERIOD_NEXT), PERIOD_CURRENT);
			c.setCropProcure(c.getCropProcure(PERIOD_NEXT), PERIOD_CURRENT);

			int manor_cost = c.getManorCost(PERIOD_CURRENT);
			if(c.getTreasury() < manor_cost)
			{
				c.setSeedProduction(getNewSeedsList(c.getId()), PERIOD_NEXT);
				c.setCropProcure(getNewCropsList(c.getId()), PERIOD_NEXT);
				manor_cost = c.getManorCost(PERIOD_CURRENT);
				if(manor_cost > 0)
					Log.add(c.getName() + "|" + -manor_cost + "|ManorManager Error@setNextPeriod", "treasury");
			}
			else
			{
				final GArray<SeedProduction> production = new GArray<SeedProduction>();
				final GArray<CropProcure> procure = new GArray<CropProcure>();
				for(final SeedProduction s : c.getSeedProduction(PERIOD_CURRENT))
				{
					s.setCanProduce(s.getStartProduce());
					production.add(s);
				}
				for(final CropProcure cr : c.getCropProcure(PERIOD_CURRENT))
				{
					cr.setAmount(cr.getStartAmount());
					procure.add(cr);
				}
				c.setSeedProduction(production, PERIOD_NEXT);
				c.setCropProcure(procure, PERIOD_NEXT);
			}

			if(Config.MANOR_SAVE_ALL_ACTIONS)
			{
				c.saveCropData();
				c.saveSeedData();
			}

			// Sending notification to a clan leader
			PlayerMessageStack.getInstance().mailto(clan.getLeaderId(), new SystemMessage(SystemMessage.THE_MANOR_INFORMATION_HAS_BEEN_UPDATED));
		}
	}

	public void approveNextPeriod()
	{
		for(final Castle c : CastleManager.getInstance().getCastles().values())
		{
			// Castle has no owner
			if(c.getOwnerId() > 0)
			{
				int manor_cost = c.getManorCost(PERIOD_NEXT);

				if(c.getTreasury() < manor_cost)
				{
					c.setSeedProduction(getNewSeedsList(c.getId()), PERIOD_NEXT);
					c.setCropProcure(getNewCropsList(c.getId()), PERIOD_NEXT);
					manor_cost = c.getManorCost(PERIOD_NEXT);
					if(manor_cost > 0)
						Log.add(c.getName() + "|" + -manor_cost + "|ManorManager Error@approveNextPeriod", "treasury");
					final L2Clan clan = ClanTable.getInstance().getClan(c.getOwnerId());
					PlayerMessageStack.getInstance().mailto(clan.getLeaderId(), new SystemMessage(SystemMessage.THE_AMOUNT_IS_NOT_SUFFICIENT_AND_SO_THE_MANOR_IS_NOT_IN_OPERATION));
				}
				else
				{
					c.addToTreasuryNoTax(-manor_cost, false, false);
					Log.add(c.getName() + "|" + -manor_cost + "|ManorManager", "treasury");
				}
			}
			c.setNextPeriodApproved(true);
		}
	}

	private GArray<SeedProduction> getNewSeedsList(final int castleId)
	{
		final GArray<SeedProduction> seeds = new GArray<SeedProduction>();
		final GArray<Integer> seedsIds = L2Manor.getInstance().getSeedsForCastle(castleId);
		for(final int sd : seedsIds)
			seeds.add(new SeedProduction(sd));
		return seeds;
	}

	private GArray<CropProcure> getNewCropsList(final int castleId)
	{
		final GArray<CropProcure> crops = new GArray<CropProcure>();
		final GArray<Integer> cropsIds = L2Manor.getInstance().getCropsForCastle(castleId);
		for(final int cr : cropsIds)
			crops.add(new CropProcure(cr));
		return crops;
	}

	public boolean isUnderMaintenance()
	{
		return _underMaintenance;
	}

	public void setUnderMaintenance(final boolean mode)
	{
		_underMaintenance = mode;
	}

	public boolean isDisabled()
	{
		return _disabled;
	}

	public void setDisabled(final boolean mode)
	{
		_disabled = mode;
	}

	public SeedProduction getNewSeedProduction(final int id, final long amount, final long price, final long sales)
	{
		return new SeedProduction(id, amount, price, sales);
	}

	public CropProcure getNewCropProcure(final int id, final long amount, final int type, final long price, final long buy)
	{
		return new CropProcure(id, amount, type, buy, price);
	}

	public void save()
	{
		for(final Castle c : CastleManager.getInstance().getCastles().values())
		{
			c.saveSeedData();
			c.saveCropData();
		}
	}

	private class ManorTask implements Runnable
	{
		@Override
		public void run()
		{
			final int H = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
			final int M = Calendar.getInstance().get(Calendar.MINUTE);

			if(ServerVariables.getBool(var_name)) // 06:00 - 20:00
			{
				if(H < NEXT_PERIOD_APPROVE || H > MANOR_REFRESH || H == MANOR_REFRESH && M >= MANOR_REFRESH_MIN)
				{
					ServerVariables.set(var_name, false);
					setUnderMaintenance(true);
					_log.info("Manor System: Under maintenance mode started");
				}
			}
			else if(isUnderMaintenance()) // 20:00 - 20:06
			{
				if(H != MANOR_REFRESH || M >= MANOR_REFRESH_MIN + MAINTENANCE_PERIOD)
				{
					setUnderMaintenance(false);
					_log.info("Manor System: Next period started");
					if(isDisabled())
						return;
					setNextPeriod();
					try
					{
						save();
					}
					catch(final Exception e)
					{
						_log.info("Manor System: Failed to save manor data: " + e);
					}
				}
			}
			else if(H > NEXT_PERIOD_APPROVE && H < MANOR_REFRESH || H == NEXT_PERIOD_APPROVE && M >= NEXT_PERIOD_APPROVE_MIN)
			{
				ServerVariables.set(var_name, true);
				_log.info("Manor System: Next period approved");
				if(isDisabled())
					return;
				approveNextPeriod();
			}
		}
	}
}
