package l2n.game.instancemanager;

import l2n.Config;
import l2n.game.instancemanager.boss.*;
import l2n.game.model.actor.L2Character;

/**
 * @author L2System Project
 * @date 17.09.2010
 * @time 22:53:34
 */
public class EpicBossManager {
    public static final EpicBossManager getInstance() {
        return SingletonHolder._instance;
    }

    public EpicBossManager() {
        if (Config.DONTLOADSPAWN)
            return;

        AntharasManager.init();
        BaiumManager.init();
        BaylorManager.init();
        FrintezzaManager.init();
        SailrenManager.init();
        ValakasManager.init();
        ZakenManager.init();
        EpidosManager.getInstance();
        BelethManager.getInstance();
        Spinozavr.init();
    }

    /**
     * Обрабатываем смерть в менеджерах Эпиков
     */
    public void onDie(L2Character self, L2Character killer) {
        AntharasManager.OnDie(self, killer);
        BaiumManager.OnDie(self, killer);
        BaylorManager.OnDie(self, killer);
        FrintezzaManager.OnDie(self, killer);
        SailrenManager.OnDie(self, killer);
        ValakasManager.OnDie(self, killer);
        ZakenManager.OnDie(self, killer);
        EkimusManager.OnDie(self, killer);
        Spinozavr.OnDie(self, killer);
    }

    @SuppressWarnings("synthetic-access")
    private static class SingletonHolder {
        protected static final EpicBossManager _instance = new EpicBossManager();
    }
}
