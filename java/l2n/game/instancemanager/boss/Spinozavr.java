package l2n.game.instancemanager.boss;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.event.HWIDChecker;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2BossInstance;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.QuestRewardTable;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import l2n.util.Location;
import l2n.util.Log;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

public class Spinozavr {

    private static final Logger log = Logger.getLogger(Spinozavr.class.getName());
    private final static HWIDChecker hwid_check = new HWIDChecker(true);

    private static EpicBossState state;
    private static L2NpcInstance SPINOZAVR;
    private static final int SPINOZAVR_ID = 99009;
    private static boolean Dying = false;
    private static ScheduledFuture<?> monsterSpawnTask = null;
    private static ScheduledFuture<?> sleepCheckTask = null;

    private static int getRespawnInterval() {
        return Config.SPINOZAVR_SPAWN;
    }

    public static void init() {
        state = new EpicBossState(SPINOZAVR_ID);
        log.info("EpicBossManager: Spinozavr state - " + state.getState() + ".");
        //TODO  вставить вместо 0 время через которое он появится после первого запуска

        sleepCheckTask = L2GameThreadPools.getInstance().scheduleGeneral(new CheckLastKillBoss(), 60000);
    }

    public static EpicBossState getState() {
        return state;
    }


    private static void onSpinozavrDie(L2Character killer, L2Character boss) {
        if (Dying) {
            return;
        }

        Log.add("Spinozavr died", "Spinozavr");
        aggrListReward(boss);

        Dying = true;
        state.setRespawnDate(getRespawnInterval() * 60000);
        state.setState(EpicBossState.State.DEAD);
        state.update();

        SPINOZAVR.setCanClean(true);
    }

    private static void aggrListReward(L2Character boss) {

        final TreeSet<L2NpcInstance.AggroInfo> aggroList = new TreeSet<L2NpcInstance.AggroInfo>(new Comparator<L2NpcInstance.AggroInfo>() {
            @Override
            public int compare(final L2NpcInstance.AggroInfo o1, final L2NpcInstance.AggroInfo o2) {
                final int hateDiff = o1.hate - o2.hate;
                if (hateDiff != 0)
                    return hateDiff;
                return o1.damage - o2.damage;
            }
        });
        Log.add("reward reviece :", "Spinozavr");
        aggroList.addAll(((L2BossInstance) boss).getAggroList());

        for (L2NpcInstance.AggroInfo player : aggroList.descendingSet()) {
            if (hwid_check.canParticipate((L2Player) player.attacker)) {
                reward((L2Player) player.attacker);
            }
        }

    }

    private static void reward(L2Player player) {

        L2ItemInstance item;
        for (final QuestRewardTable.Reward reward : getRewardList()) {
            if (reward.isStackable()) {
                item = ItemTable.getInstance().createItem(reward.getItemId(), player.getObjectId(), 0, "Spinozavr");
                item.setCount(reward.getCount());
                player.getInventory().addItem(item);
                Log.add("player " + player.getName() + " item = " + item, "Spinozavr");
                Log.LogItem(player, SPINOZAVR, Log.GetQuestItem, item);
            } else
                for (int i = 0; i < reward.getCount(); i++) {
                    item = ItemTable.getInstance().createItem(reward.getItemId(), player.getObjectId(), 0, "Spinozavr");
                    item.setCount(1);
                    player.getInventory().addItem(item);
                    Log.add("player " + player.getName() + " item = " + item, "Spinozavr");
                    Log.LogItem(player, SPINOZAVR, Log.GetQuestItem, item);
                }
            player.sendPacket(SystemMessage.obtainItems(reward.getItemId(), reward.getCount(), 0));
        }

    }

    private static List<QuestRewardTable.Reward> getRewardList() {
        List<QuestRewardTable.Reward> rewardList = new ArrayList<QuestRewardTable.Reward>();
        for (String rew : Config.SPINOZAVR_REWARD) {
            String[] check = rew.split(":");
            if (check.length > 0) {
                L2Item items = ItemTable.getInstance().getTemplate(Integer.parseInt(check[0]));
                long count = Long.parseLong(check[1]);
                QuestRewardTable.Reward reward = new QuestRewardTable.Reward(items, count);
                rewardList.add(reward);
            } else {
                log.warning("PLS CONFIGURATION SpinozavrReward in config right format item:count");
            }
        }

        return rewardList;
    }

    public static void OnDie(L2Character self, L2Character killer) {
        if (self == null) {
            return;
        }

        if (self == SPINOZAVR) {
            onSpinozavrDie(killer, self);
        }
        sleepCheckTask = L2GameThreadPools.getInstance().scheduleGeneral(new CheckLastKillBoss(), 60000);
    }


    private static class SpinozavrSpawn implements Runnable {
        private int _npcId;
        private final Location _pos = new Location(149150, 47030, -3413, 49229);

        private SpinozavrSpawn(int npcId) {
            _npcId = npcId;
        }

        @Override
        public void run() {

            if (state.getRespawnDate() < System.currentTimeMillis() && !state.getState().equals(EpicBossState.State.ALIVE)) {
                SPINOZAVR = Functions.spawn(new Location(149172, 47008, -3413, 49229), SPINOZAVR_ID);

                state.setRespawnDate(getRespawnInterval());
                state.setState(EpicBossState.State.ALIVE);
                state.update();

                SPINOZAVR.setCanClean(false);
                SPINOZAVR.setRunning();
                SPINOZAVR.getAI().addTaskMove(_pos);
            }
        }
    }


    private static class CheckLastKillBoss implements Runnable {
        @Override
        public void run() {
            if (!state.getState().equals(EpicBossState.State.ALIVE) && state.getRespawnDate() < System.currentTimeMillis()) {
                monsterSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new SpinozavrSpawn(SPINOZAVR_ID), 5000);
            } else {
                sleepCheckTask = L2GameThreadPools.getInstance().scheduleGeneral(new CheckLastKillBoss(), 60000);
            }
        }
    }

}
