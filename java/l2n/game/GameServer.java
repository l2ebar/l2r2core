package l2n.game;

import l2n.Config;
import l2n.Server;
import l2n.commons.Universe;
import l2n.commons.captcha.CaptchaValidator;
import l2n.commons.network.MMOSocket;
import l2n.commons.network.SelectorConfig;
import l2n.commons.network.SelectorThread;
import l2n.commons.text.Strings;
import l2n.commons.threading.ThreadPoolFactory.NextUncaughtExceptionHandler;
import l2n.database.L2DatabaseFactory;
import l2n.database.utils.DbUtils;
import l2n.extensions.Stat;
import l2n.extensions.scripts.Events;
import l2n.game.ai.model.AIThreadController;
import l2n.game.cache.CrestCache;
import l2n.game.communitybbs.CommunityBoardHandlers;
import l2n.game.communitybbs.Manager.ForumsBBSManager;
import l2n.game.custom.ACP;
import l2n.game.custom.ACP2;
import l2n.game.custom.TheGhostEngine;
import l2n.game.geodata.GeoEngine;
import l2n.game.handler.*;
import l2n.game.handler.interfaces.ICommandHandler;
import l2n.game.idfactory.IdFactory;
import l2n.game.instancemanager.*;
import l2n.game.instancemanager.boss.FourSepulchersManager;
import l2n.game.instancemanager.boss.LastImperialTombManager;
import l2n.game.instancemanager.games.HandysBlockCheckerManager;
import l2n.game.instancemanager.itemauction.ItemAuctionManager;
import l2n.game.loginservercon.LSConnection;
import l2n.game.model.AutoChatHandler;
import l2n.game.model.AutoSpawnHandler;
import l2n.game.model.L2Manor;
import l2n.game.model.L2Multisell;
import l2n.game.model.entity.Hero;
import l2n.game.model.entity.MonsterRace;
import l2n.game.model.entity.SevenSigns;
import l2n.game.model.entity.SevenSignsFestival.SevenSignsFestival;
import l2n.game.model.entity.l2auction.L2AuctionManager;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.model.entity.vehicle.L2VehicleManager;
import l2n.game.model.items.MailParcelController;
import l2n.game.model.quest.QuestRewardTable;
import l2n.game.network.L2GameClient;
import l2n.game.network.L2GamePacketHandler;
import l2n.game.network.clientpackets.L2GameClientPacket;
import l2n.game.network.model.Pinger;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.game.scripts.CharacterAILoader;
import l2n.game.scripts.NPCInstanceLoader;
import l2n.game.scripts.QuestLoader;
import l2n.game.tables.*;
import l2n.game.taskmanager.ItemsAutoDestroy;
import l2n.game.taskmanager.TaskManager;
import l2n.game.taskmanager.deamon.HopZoneTaskManager;
import l2n.game.taskmanager.deamon.L2TopDeamon;
import l2n.game.taskmanager.deamon.MMOTopDeamon;
import l2n.game.taskmanager.tasks.RestoreOfflineTraders;
import l2n.status.Status;
import l2n.util.Colors;
import l2n.util.HWID;
import l2n.util.Log;
import l2n.util.Util;

import java.util.Calendar;
import java.util.logging.Logger;

public class GameServer
{
	private static final Logger _log = Logger.getLogger(GameServer.class.getName());

	protected final SelectorThread<L2GameClient, L2GameClientPacket, L2GameServerPacket> _selectorThread;
	public static GameServer gameServer;

	public static Status statusServer;

	public static Events events;

	private static int _serverStarted;

	public SelectorThread<L2GameClient, L2GameClientPacket, L2GameServerPacket> getSelectorThread()
	{
		return _selectorThread;
	}

	public static int uptime()
	{
		return (int) (System.currentTimeMillis() / 1000) - _serverStarted;
	}

	public GameServer() throws Exception
	{
		Thread.setDefaultUncaughtExceptionHandler(NextUncaughtExceptionHandler.STATIC_INSTANCE);

		Server.gameServer = this;

		_log.info("");
		_log.info("|==================SYSTEM INFORMATION===================|");
		_log.info("| MySQL Server: " + DbUtils.getMySqlServerVersion() + ".");
		_log.info("| " + Util.getOSInfo());
		_log.info("| " + Util.getCpuInfo()[0]);
		_log.info("| " + Util.getCpuInfo()[1]);
		_log.info("| " + Util.getJavaInfo());
		_log.info("|=======================================================|");
		_log.info("");

		Strings.reload();

		final IdFactory _idFactory = IdFactory.getInstance();
		if(!_idFactory.isInitialized())
		{
			_log.severe("Could not read object IDs from DB. Please Check Your Data.");
			throw new Exception("Could not initialize the ID factory");
		}

		L2GameThreadPools.getInstance();

		GameTimeController.getInstance();

		if(Config.DEADLOCK_CHECK_PERIOD > 0)
		{
			_log.info("TDD initialized, interval is " + Config.DEADLOCK_CHECK_PERIOD / 1000d + " seconds.");
			new ThreadDeadlockDetector().start();
		}

		CrestCache.load();

		events = new Events();

		Util.printSection("Skills");
		EnchantChanceSkillsTable.getInstance();
		SkillTargetTypeHandler.getInstance();
		final SkillTable skillTable = SkillTable.getInstance();
		SkillTreeTable.getInstance();
		SupportPetsSkillsTable.getInstance();
		PetSkillsTable.getInstance();
		NobleSkillTable.getInstance();
		HeroSkillTable.getInstance();
		SkillSpellbookTable.getInstance();
		if(!skillTable.isInitialized())
		{
			_log.warning("Could not find the extraced files. Please Check Your Data.");
			throw new Exception("Could not initialize the skill table");
		}

		Util.printSection("Items");
		final ItemTable itemTable = ItemTable.getInstance();
		if(!itemTable.isInitialized())
		{
			_log.warning("Could not find the extraced files. Please Check Your Data.");
			throw new Exception("Could not initialize the item table");
		}
		ExtractableItemsTable.getInstance();
		ArmorSetsTable.getInstance();
		SkillItemsData.getInstance();
		AugmentationData.getInstance();
		L2Multisell.getInstance();
		QuestRewardTable.getInstance();

		Util.printSection("NPCs");
		Colors.loadColors();
		DynamicRateTable.getInstance();
		CharacterAILoader.getInstance();
		NPCInstanceLoader.getInstance();
		final NpcTable npcTable = NpcTable.getInstance();
		if(!npcTable.isInitialized())
		{
			_log.warning("Could not find the extraced files. Please Check Your Data.");
			throw new Exception("Could not initialize the npc table");
		}

		QuestLoader.load();
		PetDataTable.getInstance();
		SoulCrysalAbsorbTable.getInstance();

		Util.printSection("Controller");
		TradeController.getInstance();
		RecipeController.getInstance();

		Util.printSection("Characters");
		CharNameTable.getInstance();
		CharTemplateTable.getInstance();
		LevelUpTable.getInstance();

		final HennaTable _hennaTable = HennaTable.getInstance();
		if(!_hennaTable.isInitialized())
			throw new Exception("Could not initialize the Henna Table");
		HennaTreeTable.getInstance();
		if(!_hennaTable.isInitialized())
			throw new Exception("Could not initialize the Henna Tree Table");

		if(Config.ALT_AI_ENGINE)
			AIThreadController.getInstance();
		else
			_log.info("AIThreadController: use standart engine.");

		Util.printSection("GeoEngine");
		GeoEngine.loadGeo();

		Util.printSection("Doors");
		DoorTable.getInstance();
		DoorsKeyTable.getInstance();
		StaticObjectsTable.getInstance();

		Util.printSection("Castle");
		CastleManager.getInstance();
		CastleSiegeManager.load();

		Util.printSection("Fortress");
		FortressManager.getInstance();
		FortressSiegeManager.load();

		Util.printSection("Clan Hall");
		ClanHallManager.getInstance();
		ClanHallSiegeManager.load();

		Util.printSection("Territory");
		TerritorySiege.load();

		ClanTable.getInstance();

		Util.printSection("Manor");
		L2Manor.getInstance();
		CastleManorManager.getInstance();
		TownManager.getInstance();

		Util.printSection("Spawn Engine");
		SpawnTable.getInstance();
		RaidBossSpawnManager.getInstance();
		FourSepulchersManager.init();
		LastImperialTombManager.getInstance();

		DimensionalRiftManager.getInstance();
		InstanceManager.getInstance();
		Announcements.getInstance();

		MapRegionTable.getInstance();
		PlayerMessageStack.getInstance();

		if(Config.AUTODESTROY_ITEM_AFTER > 0)
			ItemsAutoDestroy.getInstance();

		MonsterRace.getInstance();

		final SevenSigns _sevenSignsEngine = SevenSigns.getInstance();
		SevenSignsFestival.getInstance();
		_sevenSignsEngine.updateFestivalScore();

		final AutoSpawnHandler _autoSpawnHandler = AutoSpawnHandler.getInstance();
		_log.info("AutoSpawnHandler: Loaded " + _autoSpawnHandler.size() + " handlers in total.");

		final AutoChatHandler _autoChatHandler = AutoChatHandler.getInstance();
		_log.info("AutoChatHandler: Loaded " + _autoChatHandler.size() + " handlers in total.");

		_sevenSignsEngine.spawnSevenSignsNPC();

		if(Config.ENABLE_OLYMPIAD)
		{
			Olympiad.load();
			Hero.getInstance();
		}

		CursedWeaponsManager.getInstance();
		TransformationManager.getInstance().report();

		if(!Config.ALLOW_WEDDING)
		{
			CoupleManager.getInstance();
			_log.info("CoupleManager initialized");
		}

		Util.printSection("Handlers");
		ICommandHandler commandHandlers = ItemHandler.getInstance();
		_log.info("ItemHandler: Loaded " + commandHandlers.size() + " handlers.");
		commandHandlers = AdminCommandHandler.getInstance();
		_log.info("AdminCommand: Loaded " + commandHandlers.size() + " handlers.");
		commandHandlers = UserCommandHandler.getInstance();
		_log.info("UserCommand: Loaded " + commandHandlers.size() + " handlers.");
		commandHandlers = VoicedCommandHandler.getInstance();
		_log.info("VoicedCommand: Loaded " + commandHandlers.size() + " handlers.");
		commandHandlers = CommunityBoardHandlers.getInstance();
		_log.info("CommunityBoard: Loaded " + commandHandlers.size() + " handlers.");
		commandHandlers = BypassHandler.getInstance();
		_log.info("BypassHandler: Loaded " + commandHandlers.size() + " handlers.");

		TaskManager.getInstance();
		ForumsBBSManager.getInstance();
		MercTicketManager.getInstance();

		L2VehicleManager.getInstance();
		AirShipDocksTable.getInstance();

		AuctionManager.getInstance();
		L2AuctionManager.getInstance();
		ItemAuctionManager.getInstance();
		EpicBossManager.getInstance();
		GraciaSeedManager.getInstance();
		SeedOfInfinityManager.init();
		TullyWorkshopManager.getInstance();
		TowerOfNaiaManager.getInstance();
		L2EventManager.getInstance();
		ACP.getInstance().init();
		ACP2.getInstance().init();

		if(Config.ENABLE_BLOCK_CHECKER_EVENT)
		{
			HandysBlockCheckerManager.getInstance().startUpParticipantsQueue();
			_log.info("Handy's Block Checker Event is enabled");
		}

		TeleportTable.getInstance();
		PartyRoomManager.getInstance();

		PcCafePointsManager.getInstance();
		final Shutdown shutdownHandler = Shutdown.getInstance();
		Runtime.getRuntime().addShutdownHook(shutdownHandler);

		try
		{
			DoorTable.getInstance().getDoor(24190001).openMe();
			DoorTable.getInstance().getDoor(24190002).openMe();
			DoorTable.getInstance().getDoor(24190003).openMe();
			DoorTable.getInstance().getDoor(24190004).openMe();

			DoorTable.getInstance().getDoor(23180001).openMe();
			DoorTable.getInstance().getDoor(23180002).openMe();
			DoorTable.getInstance().getDoor(23180003).openMe();
			DoorTable.getInstance().getDoor(23180004).openMe();
			DoorTable.getInstance().getDoor(23180005).openMe();
			DoorTable.getInstance().getDoor(23180006).openMe();

			DoorTable.getInstance().getDoor(23140001).openMe();
			DoorTable.getInstance().getDoor(23140002).openMe();

			DoorTable.getInstance().checkAutoOpen();
		}
		catch(final NullPointerException e)
		{
			_log.warning("Door.csv does not contain the right door info. Update door.csv");
			e.printStackTrace();
		}

		int restartTime = 0;
		int restartAt = 0;

		if(Config.RESTART_AT_TIME > -1)
		{
			final Calendar calendarRestartAt = Calendar.getInstance();
			calendarRestartAt.set(Calendar.HOUR_OF_DAY, Config.RESTART_AT_TIME);
			calendarRestartAt.set(Calendar.MINUTE, 0);

			if(calendarRestartAt.getTimeInMillis() < System.currentTimeMillis())
				calendarRestartAt.add(Calendar.HOUR, 24);

			restartAt = (int) (calendarRestartAt.getTimeInMillis() - System.currentTimeMillis()) / 1000;
		}

		restartTime = Config.RESTART_TIME * 60 * 60;

		if(restartTime < restartAt && restartTime > 0 || restartTime > restartAt && restartAt == 0)
			Shutdown.getInstance().setAutoRestart(restartTime);
		else if(restartAt > 0)
			Shutdown.getInstance().setAutoRestart(restartAt);

		MailParcelController.getInstance();

		_log.info("");
		_log.info("|=======================INFO==========================|");

		if(Config.DEBUG)
			_log.info("GameServer Started in DEBUG mode!");
		else
			_log.info("GameServer Started in NORMAL mode");

		_log.info("Maximum Numbers of Connected Players: " + Config.MAXIMUM_ONLINE_USERS);

		if(Config.ENABLE_VITALITY)
			_log.info("Vitality System: enable.");
		else
			_log.info("Vitality System: disable.");

		FriendsTable.getInstance();

		if(Config.CAPTCHA_ENABLE)
			CaptchaValidator.getInstance();

		if(Config.PROTOCOL_ENABLE_OBFUSCATION)
			_log.info("Opcode Obfuscation: enable.");

		if(Config.L2TOPDEMON_ENABLED)
			L2TopDeamon.getInstance();
		if(Config.MMOTOP_DEMON_ENABLED)
			MMOTopDeamon.getInstance();
		if(Config.L2HOPZONE_SERVICE_ENABLED)
			HopZoneTaskManager.getInstance();

		Stat.init();
		if(Config.PROTECT_ENABLE)
		{
			_log.info("LameGuard support enabled.");
			if(Config.PROTECT_GS_ENABLE_HWID_BANS)
				HWID.reloadBannedHWIDs();
		}

		if(Config.PING_ENABLED)
		{
			_log.info("Pinger starting.");
			final Thread pThread = Pinger.getInstance();
			pThread.setDaemon(true);
			pThread.setPriority(Thread.MIN_PRIORITY);
			pThread.start();
			pThread.setName("Pinger");
		}

		MMOSocket.getInstance();
		LSConnection.getInstance().start();

		final L2GamePacketHandler gph = new L2GamePacketHandler();
		final SelectorConfig<L2GameClient, L2GameClientPacket, L2GameServerPacket> sc = new SelectorConfig<L2GameClient, L2GameClientPacket, L2GameServerPacket>(gph);
		sc.setMaxSendPerPass(30);
		sc.setSelectorSleepTime(1);

		_selectorThread = new SelectorThread<L2GameClient, L2GameClientPacket, L2GameServerPacket>(sc, gph, gph, null);
		_selectorThread.setAntiFlood(Config.ANTIFLOOD_ENABLE);
		if(Config.ANTIFLOOD_ENABLE)
			_selectorThread.setAntiFloodSocketsConf(Config.MAX_UNHANDLED_SOCKETS_PER_IP, Config.UNHANDLED_SOCKET_MIN_TTL);
		_selectorThread.openServerSocket(null, Config.PORT_GAME);
		_selectorThread.start();

		if(Config.ACTIVATE_POSITION_RECORDER)
			Universe.getInstance();

		if(Config.GHOST_ENGINE_ANTIBOT_ALLOW_BAN)
		{
			_log.info("TheGhostEngine: Initialized.");
			TheGhostEngine.onScheduleAbusesPunishments();
		}

		if(Config.SERVICES_OFFLINE_TRADE_RESTORE_AFTER_RESTART)
			L2GameThreadPools.getInstance().scheduleGeneral(new RestoreOfflineTraders(), 30000);
		
	}

	public static void main(final String[] args) throws Exception
	{
		Server.SERVER_MODE = Server.MODE_GAMESERVER;

		_serverStarted = (int) (System.currentTimeMillis() / 1000);

		Config.initLogging();
		Config.load();
		Log.initDevLogs();

		Util.waitForFreePorts(Config.GAMESERVER_HOSTNAME, Config.PORT_GAME);

		L2DatabaseFactory.getInstance().getConnection().close();

		DbUtils.repairTables();
		DbUtils.optimizeTables();

		gameServer = new GameServer();

		if(Config.IS_TELNET_ENABLED)
		{
			statusServer = new Status(Server.MODE_GAMESERVER);
			statusServer.start();
		}
		else
			_log.info("Telnet server is currently disabled.");
			_log.info("-------------------- Game Server --------------------");
	}
}
