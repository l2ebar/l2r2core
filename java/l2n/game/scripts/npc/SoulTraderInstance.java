package l2n.game.scripts.npc;

import l2n.extensions.scripts.Functions;
import l2n.game.model.L2Multisell;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2MerchantInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * @author L2System Project
 * @date 02.03.2011
 * @time 12:02:06
 */
public class SoulTraderInstance extends L2MerchantInstance
{
	public SoulTraderInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.equalsIgnoreCase("buyspecial"))
		{
			// проверяем наличие Mark of Keucereus - Stage 2
			if(Functions.getItemCount(player, 13692) >= 0)
				L2Multisell.getInstance().separateAndSend(698, player, 0);
			else
				L2Multisell.getInstance().separateAndSend(647, player, 0);
		}
		super.onBypassFeedback(player, command);
	}
}
