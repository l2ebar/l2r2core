package l2n.game.scripts.npc;

import l2n.game.instancemanager.SeedOfInfinityManager;
import l2n.game.instancemanager.boss.EkimusManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * @author L2System Project
 * @date 04.03.2011
 * @time 12:53:23
 */
public class GatekeeperOfAbyssInstance extends L2NpcInstance
{
	public GatekeeperOfAbyssInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		int cycle = SeedOfInfinityManager.getCurrentCycle();
		if(command.equalsIgnoreCase("enter"))
		{
			if(cycle > 0)
				showChatWindow(player, 2);
		}
		else if(command.equalsIgnoreCase("toSeed"))
		{
			// сбор камней
			if(cycle == 3 || cycle == 4)
				SeedOfInfinityManager.enterToGathering(player);
			else
				showChatWindow(player, 3);
		}
		else if(command.equalsIgnoreCase("toInfinity"))
		{
			if(cycle > 0) // вход к экимосу
				EkimusManager.enterInstance(player);
		}
		else
			super.onBypassFeedback(player, command);
	}
}
