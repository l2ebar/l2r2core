package l2n.game.scripts.npc;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.templates.L2NpcTemplate;

public class QueenAntLarvaInstance extends L2MonsterInstance
{
	public QueenAntLarvaInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
		setImmobilized(true);
	}

	@Override
	public void reduceCurrentHp(double i, L2Character attacker, L2Skill skill, boolean awake, boolean standUp, boolean directHp, boolean canReflect, boolean isCounteAttack)
	{
		double damage = getCurrentHp() - i > 1 ? i : getCurrentHp() - 1;
		super.reduceCurrentHp(damage, attacker, skill, awake, standUp, directHp, canReflect, isCounteAttack);
	}

	@Override
	public boolean canChampion()
	{
		return false;
	}
}
