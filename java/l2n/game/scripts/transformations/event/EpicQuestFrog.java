package l2n.game.scripts.transformations.event;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class EpicQuestFrog extends L2DefaultTransformation
{
	public EpicQuestFrog()
	{
		// id, colRadius, colHeight
		super(111, 20, 9);
	}

	@Override
	public void onTransform(final L2Player player)
	{
		if(player.getTransformationId() != 111 || player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
		player.getPlayerAccess().UseTeleport = false;
	}

	@Override
	public void transformedSkills(final L2Player player)
	{
		// Dissonance
		addSkill(player, 5437, 1);
		// Frog Jump
		addSkill(player, 959, 1);

	}

	@Override
	public void onUntransform(final L2Player player)
	{
		removeSkills(player);
		player.getPlayerAccess().UseTeleport = true;
	}

	@Override
	public void removeSkills(final L2Player player)
	{
		// Dissonance
		removeSkill(player, 5437);
		// Frog Jump
		removeSkill(player, 959);

	}
}
