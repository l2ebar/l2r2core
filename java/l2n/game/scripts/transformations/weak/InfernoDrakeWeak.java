package l2n.game.scripts.transformations.weak;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class InfernoDrakeWeak extends L2DefaultTransformation
{
	public InfernoDrakeWeak()
	{
		// id, colRadius, colHeight
		super(215, 8.0, 22.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 576, 2); // Paw Strike
		addSkill(player, 577, 2); // Fire Breath
		addSkill(player, 578, 2); // Blaze Quake
		addSkill(player, 579, 2); // Fire Armor
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 576); // Paw Strike
		removeSkill(player, 577); // Fire Breath
		removeSkill(player, 578); // Blaze Quake
		removeSkill(player, 579); // Fire Armor
	}
}
