package l2n.game.scripts.transformations.monster;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Yeti extends L2DefaultTransformation
{
	public Yeti()
	{
		// id, colRadius, colHeight
		super(102, 18.0, 32.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{}

	@Override
	public void removeSkills(L2Player player)
	{}
}
