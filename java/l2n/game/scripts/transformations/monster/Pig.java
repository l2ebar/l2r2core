package l2n.game.scripts.transformations.monster;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Pig extends L2DefaultTransformation
{
	public Pig()
	{
		// id, colRadius, colHeight
		super(104, 15.0, 18.3);
	}

	@Override
	public void transformedSkills(L2Player player)
	{}

	@Override
	public void removeSkills(L2Player player)
	{}
}
