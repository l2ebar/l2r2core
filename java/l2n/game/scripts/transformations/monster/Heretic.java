package l2n.game.scripts.transformations.monster;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Heretic extends L2DefaultTransformation
{
	public Heretic()
	{
		// id, colRadius, colHeight
		super(3, 13.0, 29.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 738, 3); // Heretic Heal
		addSkill(player, 739, 3); // Heretic Battle Heal
		addSkill(player, 740, 3); // Heretic Resurrection
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 738); // Heretic Heal
		removeSkill(player, 739); // Heretic Battle Heal
		removeSkill(player, 740); // Heretic Resurrection
	}
}
