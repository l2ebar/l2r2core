package l2n.game.scripts.transformations.strong;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class GolemGuardianStrong extends L2DefaultTransformation
{
	public GolemGuardianStrong()
	{
		// id, colRadius, colHeight
		super(210, 8.0, 23.5);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 572, 4); // Double Slasher
		addSkill(player, 573, 4); // Earthquake
		addSkill(player, 574, 4); // Bomb Installation
		addSkill(player, 575, 4); // Steel Cutter
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 572); // Double Slasher
		removeSkill(player, 573); // Earthquake
		removeSkill(player, 574); // Bomb Installation
		removeSkill(player, 575); // Steel Cutter
	}
}
