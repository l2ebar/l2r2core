package l2n.game.scripts.ai.npc;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.DefaultAI;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;
import l2n.util.Rnd;

public class Leandro extends DefaultAI
{
	private Location[] points = new Location[18];
	private int current_point = -1;
	private long wait_timeout = 0;
	private boolean wait = false;

	public Leandro(L2Character actor)
	{
		super(actor);
		points[0] = new Location(-82428, 245204, -3720);
		points[1] = new Location(-82422, 245448, -3704);
		points[2] = new Location(-82080, 245401, -3720);
		points[3] = new Location(-82108, 244974, -3720);
		points[4] = new Location(-83595, 244051, -3728);
		points[5] = new Location(-83898, 242776, -3728);
		points[6] = new Location(-85966, 241371, -3728);
		points[7] = new Location(-86079, 240868, -3720);
		points[8] = new Location(-86076, 240392, -3712);
		points[9] = new Location(-86519, 240706, -3712);
		points[10] = new Location(-86343, 241130, -3720);
		points[11] = new Location(-86519, 240706, -3712);
		points[12] = new Location(-86076, 240392, -3712);
		points[13] = new Location(-86079, 240868, -3720);
		points[14] = new Location(-85966, 241371, -3728);
		points[15] = new Location(-83898, 242776, -3728);
		points[16] = new Location(-83595, 244051, -3728);
		points[17] = new Location(-82108, 244974, -3720);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(_def_think)
		{
			doTask();
			return true;
		}

		if(System.currentTimeMillis() > wait_timeout && (current_point > -1 || Rnd.chance(5)))
		{
			if(!wait)
				switch (current_point)
				{
					case 5:
						wait_timeout = System.currentTimeMillis() + 30000;
						Functions.npcShout(actor, "Куда он ушел?");
						wait = true;
						return true;
					case 10:
						wait_timeout = System.currentTimeMillis() + 60000;
						Functions.npcShout(actor, "Видели ли вы Виндвуд?");
						wait = true;
						return true;
				}

			wait_timeout = 0;
			wait = false;

			if(current_point >= points.length - 1)
				current_point = -1;

			current_point++;

			// Добавить новое задание
			addTaskMove(points[current_point]);
			doTask();
			return true;
		}

		if(randomAnimation())
			return true;

		return false;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{}

	@Override
	protected void onEvtAggression(L2Character target, int aggro)
	{}
}
