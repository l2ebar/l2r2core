package l2n.game.scripts.ai.stakato;

import l2n.game.ai.CtrlEvent;
import l2n.game.ai.Fighter;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.NpcTable;
import l2n.util.Location;
import l2n.util.Rnd;

public class FemaleSpikedStakato extends Fighter
{
	private static final int MOB = 22622;
	private static final int MOBS_COUNT = 1;
	private static final int MOB1 = 22619;
	private static final int MOBS1_COUNT = 3;

	public FemaleSpikedStakato(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		L2NpcInstance actor = getActor();
		if(actor != null)
			if(Rnd.chance(80))
			{
				for(int i = 0; i < MOBS1_COUNT; i++)
					try
					{
						Location pos = GeoEngine.findPointToStay(actor.getX(), actor.getY(), actor.getZ(), 100, 120);
						L2Spawn sp = new L2Spawn(NpcTable.getTemplate(MOB1));
						sp.setLoc(pos);
						L2NpcInstance npc = sp.doSpawn(true);
						npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, killer, Rnd.get(1, 100));
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
			}
			else
			{
				for(int i = 0; i < MOBS_COUNT; i++)
					try
					{
						Location pos = GeoEngine.findPointToStay(actor.getX(), actor.getY(), actor.getZ(), 100, 120);
						L2Spawn sp = new L2Spawn(NpcTable.getTemplate(MOB));
						sp.setLoc(pos);
						L2NpcInstance npc = sp.doSpawn(true);
						npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, killer, Rnd.get(1, 100));
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
			}
		super.onEvtDead(killer);
	}
}
