package l2n.game.scripts.ai.soi;

import l2n.game.L2GameThreadPools;
import l2n.game.ai.Fighter;
import l2n.game.model.L2Skill;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.scripts.npc.hellbound.HoSBossInstance;
import l2n.game.tables.SkillTable;

import java.util.Set;
import java.util.concurrent.ScheduledFuture;

/**
 * AI босов близнецов Yehan Klodekus и Yehan Klanikus для Seed of Infinity, инстанс Hall of Suffering:
 * - становится неуявзвимым, если далеко от брата
 * - если убивают и при этом у брата более 10% ХП, то ресаемся и хилим себя на 15%
 * 
 */
public class HallOfSufferingBoss extends Fighter
{
	private static final long INVUL_DISTANCE = 300;
	private static final L2Skill SKILL_DEFEAT = SkillTable.getInstance().getInfo(5823, 1);
	private static final L2Skill SKILL_ARISE = SkillTable.getInstance().getInfo(5824, 1);

	private static final L2Skill BUFF_SKILL = SkillTable.getInstance().getInfo(5934, 1);

	private ScheduledFuture<BufferTask> _buffTask;

	private int _brotherId;
	private HoSBossInstance _brother;
	private long _wait_timeout = 0;

	public HallOfSufferingBoss(L2Character actor)
	{
		super(actor);
		if(actor.getNpcId() == 25665)
			_brotherId = 25666;
		else
			_brotherId = 25665;
	}

	private boolean searchBrother()
	{
		HoSBossInstance actor = getActor();
		if(actor == null)
			return false;

		if(_brother == null)
			// Ищем брата не чаще, чем раз в 15 секунд, если по каким-то причинам его нету
			if(System.currentTimeMillis() > _wait_timeout)
			{
				_wait_timeout = System.currentTimeMillis() + 15000;
				for(L2NpcInstance npc : L2World.getAroundNpc(actor))
					if(npc.getNpcId() == _brotherId)
					{
						_brother = (HoSBossInstance) npc;
						return true;
					}
			}
		return false;
	}

	@Override
	protected boolean thinkActive()
	{
		if(_brother == null)
			searchBrother();

		return super.thinkActive();
	}

	@Override
	protected void thinkAttack()
	{
		HoSBossInstance actor = getActor();
		if(actor == null)
			return;

		if(_brother == null)
			searchBrother();
		else if(!_brother.isDead() && !actor.isInRange(_brother, INVUL_DISTANCE))
			actor.setInvul(true);
		else
			actor.setInvul(false);

		super.thinkAttack();
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		HoSBossInstance actor = getActor();
		if(actor == null)
			return;

		// проверяем на возможность бафа
		checkBuffTask();

		if(_brother == null)
			searchBrother();
		else if(_brother.getCurrentHpPercents() > 20 && actor.getCurrentHp() - damage < actor.getMaxHp() / 10)
		{
			// Если у брата > 20% ХП, то невозможно опустить ХП ниже 10%
			actor.abortAttack();
			actor.abortCast();
			actor.stopMove();
			clearTasks();
			addTaskBuff(actor, SKILL_DEFEAT);
			addTaskBuff(actor, SKILL_ARISE);
			for(L2Playable playable : L2World.getAroundPlayables(actor))
				if(playable.getTargetId() == actor.getObjectId())
				{
					playable.abortAttack();
					playable.abortCast();
					playable.setTarget(null);
				}
			actor.setCurrentHp(actor.getMaxHp() / 3, true);
			return;
		}

		super.onEvtAttacked(attacker, damage);
	}

	private void checkBuffTask()
	{
		HoSBossInstance actor = getActor();
		if(actor == null)
			return;

		if(_brother != null && !_brother.isStartBuffTask() && !actor.isStartBuffTask() && actor.getCurrentHpPercents() <= 60)
		{
			actor.startBuffTask();
			_buffTask = L2GameThreadPools.getInstance().scheduleAi(new BufferTask(), 100, false);
		}
	}

	private class BufferTask implements Runnable
	{
		@Override
		public void run()
		{
			HoSBossInstance actor = getActor();
			if(actor == null)
				return;
			try
			{
				if(!actor.isDead())
				{
					Set<L2Playable> aggroList = actor.getAggroMap().keySet();
					for(L2Playable pl : aggroList)
						if(pl.isPlayer() && !pl.isDead() && pl.getReflectionId() == actor.getReflectionId() && pl.isInRange(actor, 900) && Math.abs(pl.getZ() - actor.getZ()) < 200)
							BUFF_SKILL.getEffects(actor, pl, false, false);

					if(_buffTask != null)
					{
						_buffTask.cancel(false);
						_buffTask = null;
					}
					_buffTask = L2GameThreadPools.getInstance().scheduleAi(new BufferTask(), 60 * 1000, false);
				}
			}
			catch(Throwable e)
			{}
		}
	}

	@Override
	public HoSBossInstance getActor()
	{
		return (HoSBossInstance) super.getActor();
	}
}
