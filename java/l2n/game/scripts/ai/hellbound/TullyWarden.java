package l2n.game.scripts.ai.hellbound;

import l2n.game.ai.Fighter;
import l2n.game.ai.model.AdditionalAIParams.AiOptionsType;
import l2n.game.instancemanager.TullyWorkshopManager;
import l2n.game.model.actor.L2Character;

public class TullyWarden extends Fighter
{
	public TullyWarden(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		TullyWorkshopManager.getInstance().trySpawnFaithfulServant(getInt(AiOptionsType.OPTIONS, 0));
		super.onEvtDead(killer);
	}
}
