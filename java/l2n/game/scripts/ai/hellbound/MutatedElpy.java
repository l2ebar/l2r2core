package l2n.game.scripts.ai.hellbound;

import l2n.game.ai.Fighter;
import l2n.game.instancemanager.boss.EpidosManager;
import l2n.game.model.actor.L2Character;

public class MutatedElpy extends Fighter
{
	public MutatedElpy(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		EpidosManager.getInstance().tryStartEpidos(getActor());
		super.onEvtDead(killer);
	}
}
