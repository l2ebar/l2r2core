package l2n.game.tables;

import l2n.game.model.L2Skill;

import java.util.logging.Logger;

/**
 * @author L2System Project
 */
public class NobleSkillTable
{
	private static final L2Skill[] _nobleSkills = new L2Skill[8];
	private static NobleSkillTable _instance;

	protected static final Logger _log = Logger.getLogger(NobleSkillTable.class.getName());

	private NobleSkillTable()
	{
		_nobleSkills[0] = SkillTable.getInstance().getInfo(325, 1);
		_nobleSkills[1] = SkillTable.getInstance().getInfo(326, 1);
		_nobleSkills[2] = SkillTable.getInstance().getInfo(327, 1);
		_nobleSkills[3] = SkillTable.getInstance().getInfo(1323, 1);
		_nobleSkills[4] = SkillTable.getInstance().getInfo(1324, 1);
		_nobleSkills[5] = SkillTable.getInstance().getInfo(1325, 1);
		_nobleSkills[6] = SkillTable.getInstance().getInfo(1326, 1);
		_nobleSkills[7] = SkillTable.getInstance().getInfo(1327, 1);

		_log.info("NobleSkillTable: loaded " + _nobleSkills.length + " nobles skills.");
	}

	public static NobleSkillTable getInstance()
	{
		if(_instance == null)
			_instance = new NobleSkillTable();
		return _instance;
	}

	public static L2Skill[] getNobleSkills()
	{
		return _nobleSkills;
	}
}
