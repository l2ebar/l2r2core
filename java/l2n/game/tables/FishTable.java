package l2n.game.tables;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Player;
import l2n.game.model.drop.FishData;
import l2n.game.model.drop.FishDropData;
import l2n.game.skills.EffectType;
import l2n.util.Rnd;

import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class FishTable
{
	private final static Logger _log = Logger.getLogger(FishTable.class.getName());

	private final TIntObjectHashMap<GArray<FishData>> _fishes;
	private final TIntObjectHashMap<GArray<FishDropData>> _fishRewards;

	public static FishTable getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final FishTable _instance = new FishTable();
	}

	private FishTable()
	{
		_fishes = new TIntObjectHashMap<GArray<FishData>>();
		_fishRewards = new TIntObjectHashMap<GArray<FishDropData>>();

		// Create table that contains all fish datas
		int count = 0;
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet resultSet = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			try
			{
				FishData fish;
				GArray<FishData> fishes;

				statement = con.prepareStatement("SELECT id, level, name, hp, hpregen, fish_type, fish_group, fish_guts, guts_check_time, wait_time, combat_time FROM fish ORDER BY id");
				resultSet = statement.executeQuery();

				while (resultSet.next())
				{
					final int id = resultSet.getInt("id");
					final int lvl = resultSet.getInt("level");
					final String name = resultSet.getString("name");
					final int hp = resultSet.getInt("hp");
					final int hpreg = resultSet.getInt("hpregen");
					final int type = resultSet.getInt("fish_type");
					final int group = resultSet.getInt("fish_group");
					final int fish_guts = resultSet.getInt("fish_guts");
					final int guts_check_time = resultSet.getInt("guts_check_time");
					final int wait_time = resultSet.getInt("wait_time");
					final int combat_time = resultSet.getInt("combat_time");
					fish = new FishData(id, lvl, name, hp, hpreg, type, group, fish_guts, guts_check_time, wait_time, combat_time);
					if((fishes = _fishes.get(group)) == null)
						_fishes.put(group, fishes = new GArray<FishData>());
					fishes.add(fish);
					count++;
				}
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "error while creating fishes table" + e);
			}
			finally
			{
				DbUtils.closeQuietly(statement, resultSet);
			}
			_log.info("FishTable: Loaded " + count + " Fishes.");

			// Create Table that contains all fish rewards (drop of fish)
			count = 0;
			try
			{
				FishDropData fishreward;
				GArray<FishDropData> rewards;
				statement = con.prepareStatement("SELECT fishid, rewardid, min, max, chance FROM fishreward ORDER BY fishid");
				resultSet = statement.executeQuery();

				while (resultSet.next())
				{
					final short fishid = resultSet.getShort("fishid");
					final short rewardid = resultSet.getShort("rewardid");
					final int mindrop = resultSet.getInt("min");
					final int maxdrop = resultSet.getInt("max");
					final int chance = resultSet.getInt("chance");
					fishreward = new FishDropData(fishid, rewardid, mindrop, maxdrop, chance);
					if((rewards = _fishRewards.get(fishid)) == null)
						_fishRewards.put(fishid, rewards = new GArray<FishDropData>());
					rewards.add(fishreward);
					count++;
				}
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "error while creating fish rewards table" + e);
			}
			finally
			{
				DbUtils.closeQuietly(statement, resultSet);
			}
			_log.config("FishRewardsTable: Loaded " + count + " FishRewards.");
		}
		catch(final Exception e)
		{
			_log.severe(e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, resultSet);
		}
	}

	/**
	 * @param Fish
	 *            - lvl
	 * @param Fish
	 *            - type
	 * @param Fish
	 *            - group
	 * @return List of Fish that can be fished
	 */
	public GArray<FishData> getfish(final int lvl, final int type, final int group)
	{

		final GArray<FishData> fishs = _fishes.get(group);
		if(fishs == null)
		{
			_log.warning("No fishes defined for group: " + group + "!");
			return null;
		}

		final GArray<FishData> result = new GArray<FishData>();
		for(final FishData f : fishs)
		{
			if(f.getLevel() != lvl)
				continue;
			if(f.getType() != type)
				continue;

			result.add(f);
		}
		if(result.isEmpty())
			_log.warning("Cant Find Any Fish!? - Lvl: " + lvl + " Type: " + type);
		return result;
	}

	public GArray<FishDropData> getFishReward(final int fishid)
	{
		final GArray<FishDropData> result = _fishRewards.get(fishid);
		if(_fishRewards == null)
		{
			_log.warning("No fish rewards defined for fish id: " + fishid + "!");
			return null;
		}

		if(result.isEmpty())
			_log.warning("No fish rewards for fish id: " + fishid + "!");

		return result;
	}

	public int[] getFishIds()
	{
		return _fishRewards.keys();
	}

	public int getRandomFishType(final int lureId)
	{
		int check = Rnd.get(100);
		int type;

		switch (lureId)
		{
			case 7807: // Green Colored Lure - For Beginners, preferred by fast-moving (nimble) fish (type 5)
				if(check <= 54)
					type = 5;
				else if(check <= 77)
					type = 4;
				else
					type = 6;
				break;
			case 7808: // Purple Colored Lure - For Beginners, preferred by fat fish (type 4)
				if(check <= 54)
					type = 4;
				else if(check <= 77)
					type = 6;
				else
					type = 5;
				break;
			case 7809: // Yellow Colored Lure - For Beginners, preferred by ugly fish (type 6)
				if(check <= 54)
					type = 6;
				else if(check <= 77)
					type = 5;
				else
					type = 4;
				break;
			case 8486: // Prize-Winning Novice Fishing Lure
				if(check <= 33)
					type = 4;
				else if(check <= 66)
					type = 5;
				else
					type = 6;
				break;
			case 7610: // Wind Fishing Lure
			case 7611: // Icy Air Fishing Lure
			case 7612: // Earth Fishing Lure
			case 7613: // Flaming Fishing Lure

			case 8496: // Gludio's Luminous Lure
			case 8497: // Dion's Luminous Lure
			case 8498: // Giran's Luminous Lure
			case 8499: // Oren's Luminous Lure
			case 8500: // Aden's Luminous Lure
			case 8501: // Innadril's Luminous Lure
			case 8502: // Goddard's Luminous Lure
			case 8503: // Rune's Luminous Lure
			case 8504: // Schuttgart's Luminous Lure
			case 8548: // Hot Springs Lure
				type = 3;
				break;
			// all theese lures (green) are prefered by fast-moving (nimble) fish (type 1)
			case 6519: // Green Colored Lure - Low Grade
			case 8505: // Green Luminous Lure - Low Grade
			case 6520: // Green Colored Lure
			case 6521: // Green Colored Lure - High Grade
			case 8507: // Green Colored Lure - High Grade
				if(check <= 54)
					type = 1;
				else if(check <= 74)
					type = 0;
				else if(check <= 94)
					type = 2;
				else
					type = 3;
				break;
			// all theese lures (purple) are prefered by fat fish (type 0)
			case 6522: // Purple Colored Lure - Low Grade
			case 6523: // Purple Colored Lure
			case 6524: // Purple Colored Lure - High Grade
			case 8508: // Purple Luminous Lure - Low Grade
			case 8510: // Purple Luminous Lure - High Grade
				if(check <= 54)
					type = 0;
				else if(check <= 74)
					type = 1;
				else if(check <= 94)
					type = 2;
				else
					type = 3;
				break;
			// all theese lures (yellow) are prefered by ugly fish (type 2)
			case 6525: // Yellow Colored Lure - Low Grade
			case 6526: // Yellow Colored Lure
			case 6527: // Yellow Colored Lure - High Grade
			case 8511: // Yellow Luminous Lure - Low Grade
			case 8513: // Yellow Luminous Lure
				if(check <= 55)
					type = 2;
				else if(check <= 74)
					type = 1;
				else if(check <= 94)
					type = 0;
				else
					type = 3;
				break;
			case 8484: // Prize-Winning Fishing Lure
				if(check <= 33)
					type = 0;
				else if(check <= 66)
					type = 1;
				else
					type = 2;
				break;
			case 8506: // Green Luminous Lure, preferred by fast-moving (nimble) fish (type 8)
				if(check <= 54)
					type = 8;
				else if(check <= 77)
					type = 7;
				else
					type = 9;
				break;
			case 8509: // Purple Luminous Lure, preferred by fat fish (type 7)
				if(check <= 54)
					type = 7;
				else if(check <= 77)
					type = 9;
				else
					type = 8;
				break;
			case 8512: // Yellow Luminous Lure, preferred by ugly fish (type 9)
				if(check <= 54)
					type = 9;
				else if(check <= 77)
					type = 8;
				else
					type = 7;
				break;
			case 8485: // Prize-Winning Night Fishing Lure, prize-winning fishing lure
				if(check <= 33)
					type = 7;
				else if(check <= 66)
					type = 8;
				else
					type = 9;
				break;
			default:
				type = 1;
				break;
		}

		return type;
	}

	public int getRandomFishLvl(final L2Player player)
	{
		int skilllvl = 0;

		// Проверка на Fisherman's Potion
		final L2Effect effect = player.getEffectList().getEffectByType(EffectType.FishPotion);
		if(effect != null)
			skilllvl = (int) effect.getSkill().getPower();
		else
			skilllvl = player.getSkillLevel(1315);

		if(skilllvl <= 0)
			return 1;

		int randomlvl;
		final int check = Rnd.get(100);

		if(check < 50)
			randomlvl = skilllvl;
		else if(check <= 85)
		{
			randomlvl = skilllvl - 1;
			if(randomlvl <= 0)
				randomlvl = 1;
		}
		else
			randomlvl = skilllvl + 1;

		return randomlvl;
	}

	public int getFishGroup(final int lureId)
	{
		switch (lureId)
		{
			case 7807: // Green Colored Lure - For Beginners
			case 7808: // Purple Colored Lure - For Beginners
			case 7809: // Yellow Colored Lure - For Beginners
			case 8486: // Prize-Winning Novice Fishing Lure
				return 0;
			case 8506: // Green Luminous Lure
			case 8509: // Purple Luminous Lure
			case 8512: // Yellow Luminous Lure
			case 8485: // Prize-Winning Night Fishing Lure
				return 2;
			default:
				return 1;
		}
	}

	public void reload()
	{

	}
}
