package l2n.game.tables;

import javolution.util.FastMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.cache.Msg;
import l2n.game.idfactory.IdFactory;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Alliance;
import l2n.game.model.L2Clan;
import l2n.game.model.L2ClanMember;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.SiegeClan;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.network.serverpackets.SystemMessage;

import java.sql.ResultSet;
import java.util.AbstractMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClanTable
{
	private static final Logger _log = Logger.getLogger(ClanTable.class.getName());

	private static ClanTable _instance;

	private final FastMap<Integer, L2Clan> _clans = new FastMap<Integer, L2Clan>().shared();
	private final FastMap<Integer, L2Alliance> _alliances = new FastMap<Integer, L2Alliance>().shared();

	public static ClanTable getInstance()
	{
		if(_instance == null)
			new ClanTable();
		return _instance;
	}

	public L2Clan[] getClans()
	{
		return _clans.values().toArray(new L2Clan[_clans.size()]);
	}

	public L2Alliance[] getAlliances()
	{
		return _alliances.values().toArray(new L2Alliance[_alliances.size()]);
	}

	private ClanTable()
	{
		_instance = this;

		restoreClans();
		restoreAllies();
		restoreWars();
		_log.info("ClanTable: loaded " + _clans.size() + " clans.");
		_log.info("ClanTable: loaded " + _alliances.size() + " alliances.");
	}


	public L2Clan getClan(final int clanId)
	{
		if(clanId <= 0)
			return null;

		final L2Clan clan = _clans.get(clanId);
		if(clan == null)
			if(Config.DEBUG_OTHER)
				_log.log(Level.WARNING, "", new NullPointerException());

		return clan;
	}

	public boolean ifExist(final int clanId)
	{
		if(clanId <= 0)
			return false;

		return _clans.get(clanId) != null;
	}

	public L2Alliance getAlliance(final int allyId)
	{
		if(allyId <= 0)
			return null;

		final L2Alliance ally = _alliances.get(allyId);

		if(ally == null)
			_log.warning("ClanTable[92]: error while restoring allyId: " + allyId);

		return ally;
	}

	public Entry<L2Clan, L2Alliance> getClanAndAllianceByCharId(final int charId)
	{
		final L2Player player = L2ObjectsStorage.getPlayer(charId);
		final L2Clan charClan = player != null ? player.getClan() : getClanByCharId(charId);
		return new AbstractMap.SimpleEntry<L2Clan, L2Alliance>(charClan, charClan == null ? null : charClan.getAlliance());
	}

	public L2Clan getClanByName(final String clanName)
	{
		if(!Config.CLAN_NAME_TEMPLATE.matcher(clanName).matches())
			return null;

		for(final L2Clan clan : getClans())
			if(clan.getName().equalsIgnoreCase(clanName))
				return clan;

		return null;
	}

	public L2Clan getClanByCharId(final int charId)
	{
		if(charId <= 0)
			return null;
		for(final L2Clan clan : getClans())
			if(clan != null && clan.isMember(charId))
				return clan;

		return null;
	}

	public L2Alliance getAllyByName(final String allyName)
	{
		if(!Config.ALLY_NAME_TEMPLATE.matcher(allyName).matches())
			return null;

		for(final L2Alliance ally : getAlliances())
			if(ally.getAllyName().equalsIgnoreCase(allyName))
				return ally;

		return null;
	}

	public L2Clan createClan(final L2Player player, final String clanName)
	{
		L2Clan clan = null;

		if(getClanByName(clanName) == null)
		{
			final L2ClanMember leader = new L2ClanMember(player);
			leader.setPlayerInstance(player);
			clan = new L2Clan(IdFactory.getInstance().getNextId(), clanName, leader);
			clan.store();
			player.setClan(clan);
			player.setPowerGrade(6);
			_clans.put(clan.getClanId(), clan);
		}

		return clan;
	}

	public void dissolveClan(final L2Player player)
	{
		final L2Clan clan = player.getClan();
		final long curtime = System.currentTimeMillis();
		SiegeManager.removeSiegeSkills(player);
		for(final L2Player clanMember : clan.getOnlineMembers(null))
		{
			clanMember.setClan(null);
			clanMember.setTitle(null);
			clanMember.sendPacket(Msg.PledgeShowMemberListDeleteAll);
			clanMember.broadcastUserInfo(true);
			clanMember.sendPacket(Msg.YOU_HAVE_RECENTLY_BEEN_DISMISSED_FROM_A_CLAN_YOU_ARE_NOT_ALLOWED_TO_JOIN_ANOTHER_CLAN_FOR_24_HOURS);
			clanMember.setLeaveClanTime(curtime);
		}

		final SiegeClan siegeClan = TerritorySiege.getSiegeClan(clan);
		if(siegeClan != null)
			TerritorySiege.removeClan(siegeClan);

		SiegeManager.clearCastleRegistrations(clan);
		SiegeManager.clearFortressRegistrations(clan);

		clan.flush();
		deleteClanFromDb(clan.getClanId());
		_clans.remove(clan.getClanId());
		player.sendPacket(Msg.CLAN_HAS_DISPERSED);
		player.setDeleteClanTime(curtime);
	}

	public void deleteClanFromDb(final int clanId)
	{
		final long curtime = System.currentTimeMillis();
		ThreadConnection con = null;
		FiltredStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			statement.executeUpdate("UPDATE characters SET clanid=0, title='', pledge_type=0, pledge_rank=0, lvl_joined_academy=0, apprentice=0, leaveclan=" + curtime / 1000 + " WHERE clanid=" + clanId);
			statement.executeUpdate("DELETE FROM clan_data WHERE clan_id=" + clanId);
			statement.executeUpdate("DELETE FROM clan_subpledges WHERE clan_id=" + clanId);
			statement.executeUpdate("DELETE FROM clan_privs WHERE clan_id=" + clanId);
			statement.executeUpdate("DELETE FROM clan_skills WHERE clan_id=" + clanId);
			statement.executeUpdate("DELETE FROM clan_notices WHERE clanID=" + clanId);
			statement.executeUpdate("DELETE FROM clan_wars WHERE clan1=" + clanId + " OR clan2=" + clanId);
		}
		catch(final Exception e)
		{
			_log.warning("could not dissolve clan:" + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public L2Alliance createAlliance(final L2Player player, final String allyName)
	{
		L2Alliance alliance = null;

		if(getAllyByName(allyName) == null)
		{
			final L2Clan leader = player.getClan();
			alliance = new L2Alliance(IdFactory.getInstance().getNextId(), allyName, leader);
			alliance.store();
			_alliances.put(alliance.getAllyId(), alliance);

			player.getClan().setAllyId(alliance.getAllyId());
			for(final L2Player temp : player.getClan().getOnlineMembers(null))
				temp.broadcastUserInfo(true);
		}

		return alliance;
	}

	public void dissolveAlly(final L2Player player)
	{
		final int allyId = player.getAllyId();
		for(final L2Clan member : player.getAlliance().getMembers())
		{
			member.setAllyId(0);
			member.broadcastClanStatus(false, true, true);
			member.broadcastToOnlineMembers(Msg.YOU_HAVE_WITHDRAWN_FROM_THE_ALLIANCE);
			member.setLeavedAlly();
		}
		deleteAllyFromDb(allyId);
		_alliances.remove(allyId);
		player.sendPacket(Msg.THE_ALLIANCE_HAS_BEEN_DISSOLVED);
		player.getClan().setDissolvedAlly();
	}

	public void deleteAllyFromDb(final int allyId)
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			final int res = statement.executeUpdate("UPDATE clan_data SET ally_id=0 WHERE ally_id=" + allyId);
			if(res > 0)
				statement.executeUpdate("DELETE FROM ally_data WHERE ally_id=" + allyId);
		}
		catch(final Exception e)
		{
			_log.warning("could not dissolve clan:" + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void startClanWar(final L2Clan clan1, final L2Clan clan2)
	{
		if(clan1 == null || clan2 == null)
			return;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		boolean started = false;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement("REPLACE INTO clan_wars (clan1, clan2) VALUES(?,?)");
			statement.setInt(1, clan1.getClanId());
			statement.setInt(2, clan2.getClanId());
			final int res = statement.executeUpdate();
			if(res != 1)
				_log.warning("could not store clan war data between " + clan1.getName() + " and " + clan2.getName());
			else
				started = true;

		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "could not store clan war data:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
		if(started)
		{
			clan1.setEnemyClan(clan2);
			clan2.setAttackerClan(clan1);
			clan1.broadcastClanStatus(false, true, true);
			clan2.broadcastClanStatus(false, true, true);

			clan1.broadcastToOnlineMembers(new SystemMessage(SystemMessage.CLAN_WAR_HAS_BEEN_DECLARED_AGAINST_S1_CLAN_IF_YOU_ARE_KILLED_DURING_THE_CLAN_WAR_BY_MEMBERS_OF_THE_OPPOSING_CLAN_THE_EXPERIENCE_PENALTY_WILL_BE_REDUCED_TO_1_4_OF_NORMAL).addString(clan2.getName()));
			clan2.broadcastToOnlineMembers(new SystemMessage(SystemMessage.S1_CLAN_HAS_DECLARED_CLAN_WAR).addString(clan1.getName()));
		}
	}

	public void stopClanWar(final L2Clan clan1, final L2Clan clan2)
	{
		if(clan1 == null || clan2 == null)
			return;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		boolean stoped = false;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM clan_wars WHERE clan1=? AND clan2=?");
			statement.setInt(1, clan1.getClanId());
			statement.setInt(2, clan2.getClanId());
			final int res = statement.executeUpdate();
			if(res != 1)
				_log.warning("Could not stop clan war between " + clan1.getName() + " and " + clan2.getName());
			else
				stoped = true;

		}
		catch(final Exception e)
		{
			_log.warning("could not delete war data:" + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}

		if(stoped)
		{
			clan1.deleteEnemyClan(clan2);
			clan2.deleteAttackerClan(clan1);

			clan1.broadcastClanStatus(false, true, true);
			clan2.broadcastClanStatus(false, true, true);

			clan1.broadcastToOnlineMembers(new SystemMessage(SystemMessage.THE_WAR_AGAINST_S1_CLAN_HAS_BEEN_STOPPED).addString(clan2.getName()));
			clan2.broadcastToOnlineMembers(new SystemMessage(SystemMessage.S1_CLAN_HAS_STOPPED_THE_WAR).addString(clan1.getName()));
		}
	}

	private void restoreWars()
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.createStatement();
			rset = statement.executeQuery("SELECT clan1, clan2 FROM clan_wars");
			L2Clan clan1;
			L2Clan clan2;
			while (rset.next())
			{
				clan1 = getClan(rset.getInt("clan1"));
				clan2 = getClan(rset.getInt("clan2"));
				if(clan1 != null && clan2 != null)
				{
					clan1.setEnemyClan(clan2);
					clan2.setAttackerClan(clan1);
				}
			}
		}
		catch(final Exception e)
		{
			_log.warning("could not restore clan wars data:");
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	private void restoreClans()
	{
		final GArray<Integer> clanIds = new GArray<Integer>();
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet result = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			result = statement.executeQuery("SELECT clan_id FROM clan_data");

			while (result.next())
				clanIds.add(result.getInt("clan_id"));
		}
		catch(final Exception e)
		{
			_log.warning("ClanTable: error while restoring clans!!! " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, result);
		}

		for(final int clanId : clanIds)
		{
			final L2Clan clan = L2Clan.restore(clanId);
			if(clan == null)
			{
				_log.warning("ClanTable[511]: error while restoring clanId: " + clanId);
				continue;
			}

			if(clan.getMembersCount() <= 0)
			{
				_log.warning("ClanTable: membersCount = 0 for clanId: " + clanId);
				continue;
			}

			if(clan.getLeader() == null)
			{
				_log.warning("ClanTable: not found leader for clanId: " + clanId);
				continue;
			}

			_clans.put(clan.getClanId(), clan);
		}
	}

	private void restoreAllies()
	{
		final GArray<Integer> allyIds = new GArray<Integer>();
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet result = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			result = statement.executeQuery("SELECT ally_id FROM ally_data");

			while (result.next())
				allyIds.add(result.getInt("ally_id"));
		}
		catch(final Exception e)
		{
			_log.warning("ClanTable: error while restoring allies!!! " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, result);
		}

		for(final int allyId : allyIds)
		{
			final L2Alliance ally = new L2Alliance(allyId);

			if(ally.getMembersCount() <= 0)
			{
				_log.warning("ClanTable: membersCount = 0 for allyId: " + allyId);
				continue;
			}

			if(ally.getLeader() == null)
			{
				_log.warning("ClanTable: not found leader for allyId: " + allyId);
				continue;
			}

			_alliances.put(ally.getAllyId(), ally);
		}
	}
}
