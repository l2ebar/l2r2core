package l2n.game.communitybbs.Manager;

import javolution.util.FastList;
import javolution.util.FastMap;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.communitybbs.BB.Forum;
import l2n.game.model.actor.L2Player;

import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class ForumsBBSManager extends AbstractBBSManager
{
	private static final Logger _log = Logger.getLogger(ForumsBBSManager.class.getName());
	private final static Map<Integer, Forum> _root = new FastMap<Integer, Forum>();
	private final static List<Forum> _table = new FastList<Forum>();
	private static int lastid = 1;

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final ForumsBBSManager _instance = new ForumsBBSManager();
	}

	public static ForumsBBSManager getInstance()
	{
		return SingletonHolder._instance;
	}

	public ForumsBBSManager()
	{
		load();
	}

	public static void addForum(Forum ff)
	{
		_table.add(ff);
		if(ff.getID() > lastid)
			lastid = ff.getID();
	}

	/**
	 *
	 */
	private void load()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT forum_id FROM forums WHERE forum_type=0");
			rset = statement.executeQuery();
			while (rset.next())
			{
				Forum f = new Forum(Integer.parseInt(rset.getString("forum_id")), null);
				_root.put(Integer.parseInt(rset.getString("forum_id")), f);
			}
		}
		catch(Exception e)
		{
			_log.warning("data error on Forum (root): " + e);
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see l2n.game.communitybbs.Manager.BaseBBSManager#parsecmd(java.lang.String, l2n.game.model.actor.instance.L2Player)
	 */
	@Override
	public void parsecmd(String command, L2Player activeChar)
	{}

	/**
	 * @param string
	 * @return
	 */
	public Forum getForumByName(String Name)
	{
		for(Forum f : _table)
			if(f.getName().equals(Name))
				return f;

		return null;
	}

	/**
	 * @param name
	 * @param forumByName
	 * @return
	 */
	public Forum CreateNewForum(String name, Forum parent, int type, int perm, int oid)
	{
		Forum forum;
		forum = new Forum(name, parent, type, perm, oid);
		forum.insertindb();
		return forum;
	}

	/**
	 * @return
	 */
	public int GetANewID()
	{
		lastid++;
		return lastid;
	}

	/**
	 * @param idf
	 * @return
	 */
	public Forum getForumByID(int idf)
	{
		for(Forum f : _table)
			if(f.getID() == idf)
				return f;
		return null;
	}

	@Override
	public void parsewrite(String ar1, String ar2, String ar3, String ar4, String ar5, L2Player activeChar)
	{}
}
