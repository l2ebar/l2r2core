package l2n.game.communitybbs.Manager;

import gnu.trove.iterator.TIntIntIterator;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.procedure.TIntIntProcedure;

import java.io.File;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilderFactory;

import l2n.Config;
import l2n.commons.threading.RunnableImpl;
import l2n.database.FiltredPreparedStatement;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.listeners.collections.MethodCollection;
import l2n.extensions.listeners.engine.MethodInvokeListener;
import l2n.extensions.listeners.events.MethodEvent;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.event.L2EventType;
import l2n.game.instancemanager.TownManager;
import l2n.game.model.EffectList;
import l2n.game.model.L2Effect;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.entity.residence.Residence;
import l2n.game.model.entity.siege.Siege;
import l2n.game.network.CustomSystemMessageId;
import l2n.game.network.serverpackets.ShowBoard;
import l2n.game.tables.SkillTable;
import l2n.game.taskmanager.EffectTaskManager;
import l2n.util.Files;
import l2n.util.IllegalPlayerAction;
import l2n.util.StringUtil;
import l2n.util.Util;
import l2n.util.XMLUtil;

import org.w3c.dom.Document;
import org.w3c.dom.Node;


public class BuffBBSManager extends AbstractBBSManager
{
	private static final Logger _log = Logger.getLogger(AbstractBBSManager.class.getName());

	private static final class BuffGroupTask extends RunnableImpl
	{
		private final L2Playable target;
		private final List<L2Skill> skills;

		private BuffGroupTask(final L2Playable target, final List<L2Skill> skills)
		{
			this.target = target;
			this.skills = skills;
		}

		@Override
		public void runImpl() throws Exception
		{
			target.setMassUpdating(true);
			for(final L2Skill skill : skills)
				if(skill != null)
				{
					final int level = _allBuffs.get(skill.getId()); // скил для бафа должен быть в списке созданных администратором
					if(level > 0 && level == skill.getLevel())
						skill.getEffectsSelf(target, Config.COMMUNITY_BOARD_BUFFER_ALT_TIME);
				}
			target.setMassUpdating(false);
			target.updateStats();
			target.updateEffectIcons();
		}

		@Override
		protected Logger getLogger()
		{
			return _log;
		}
	}

	/**
	 * Делает автобафф после смерти
	 */
	private final static class AutoBuffListener implements MethodInvokeListener
	{
		@Override
		public boolean accept(final MethodEvent event)
		{
			if(!Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF || !Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF_ATTACK_STOP || event.getOwner() == null)
				return false;
			return event.getMethodName().equalsIgnoreCase(MethodCollection.AutoBuff);
		}

		@Override
		public final void methodInvoked(final MethodEvent e)
		{
			autoBuff((L2Player) e.getOwner(), e);
		}

		public void autoBuff(final L2Player player, final MethodEvent e)
		{
			if(chekConditionListener(player))
			{
				player.setCommunityLastTime();

				final int type = (Integer) e.getArgs()[0];
				switch (type)
				{
					case 1: // Окончание действия эффекта
					{
						if(!Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF_EFFECT_EXIT)
							return;

						final ArrayList<L2Skill> buffs = getSkillsForBuff(player);

						// считаем стоимость бафа исходя из количества баффов
						final long price = buffs.size() * Config.COMMUNITY_BOARD_BUFFER_PRICE_ONE * Config.COMMUNITY_BOARD_BUFFER_PRICE_MOD_GRP * Config.COMMUNITY_BOARD_BUFFER_LVL_MOD[player.getLevel() - 1];
						// если денег на баф не хватате)
						if(price > 0 && player.getAdena() < price)
							return;

						if(price > 0)
							player.reduceAdena(price, false);

						startBuffGroup(player, buffs, " Player");
						break;
					}
					case 2: // Окончание атаки
					{
						if(!Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF_ATTACK_STOP)
							return;

						final ArrayList<L2Skill> buffs = getSkillsForBuff(player);

						// считаем стоимость бафа исходя из количества баффов
						final long price = buffs.size() * Config.COMMUNITY_BOARD_BUFFER_PRICE_ONE * Config.COMMUNITY_BOARD_BUFFER_PRICE_MOD_GRP * Config.COMMUNITY_BOARD_BUFFER_LVL_MOD[player.getLevel() - 1];
						// если денег на баф не хватате)
						if(price > 0 && player.getAdena() < price)
							return;

						if(price > 0)
							player.reduceAdena(price, false);

						startBuffGroup(player, buffs, " Player");
						break;
					}
					case 3: // Делает автобафф после смерти
					{
						if(!Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF_AFTER_DEATH)
							return;

						final ArrayList<L2Skill> buffs = getSkillsForBuff(player);

						// считаем стоимость бафа исходя из количества баффов
						final long price = buffs.size() * Config.COMMUNITY_BOARD_BUFFER_PRICE_ONE * Config.COMMUNITY_BOARD_BUFFER_PRICE_MOD_GRP * Config.COMMUNITY_BOARD_BUFFER_LVL_MOD[player.getLevel() - 1];
						// если денег на баф не хватате)
						if(price > 0 && player.getAdena() < price)
							return;

						if(price > 0)
							player.reduceAdena(price, false);

						startBuffGroup(player, buffs, " Player");
						break;
					}
				}
			}
		}
	}

	private static final ArrayList<L2Skill> getSkillsForBuff(final L2Player player)
	{
		final ArrayList<L2Skill> buffs = new ArrayList<L2Skill>();
		// ищем скилы баффов которых нету
		outer: for(final L2Skill skill : player.getCommunityBuffs())
		{
			for(final L2Effect ef : player.getEffectList().getEffects())
				if(EffectList.checkEffect(ef, skill))
					continue outer;

			buffs.add(skill);
		}
		return buffs;
	}

	private static final int MAX_SKILLS_ON_PAGE = 27;
	private static final int MAX_SKILLS_IN_SCHEME = Config.COMMUNITY_BOARD_BUFFER_MAX_SKILLS_IN_SCHEME; // макс количество бафов в схеме

	/** количество колонок */
	private static final int cols_number = 3;
	/** баф */
	private static final int TYPE_BUFF = 1;
	/** удаление из группы */
	private static final int TYPE_DELETE = 2;
	/** добавление в группу */
	private static final int TYPE_ADD = 3;

	private static final TIntObjectHashMap<ArrayList<L2Skill>> _buffs = new TIntObjectHashMap<ArrayList<L2Skill>>();
	private static final TIntIntHashMap _allBuffs = new TIntIntHashMap();
	private static final TIntArrayList available_skills = new TIntArrayList();

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final BuffBBSManager _instance = new BuffBBSManager();
	}

	public static BuffBBSManager getInstance()
	{
		return SingletonHolder._instance;
	}

	public BuffBBSManager()
	{
		load();
		_log.info("CommunityBuffer: load " + _buffs.size() + " groups, " + _allBuffs.size() + " skills");
	}

	private void load()
	{
		final File localFile = new File("./config/CommunityBufferConfig.xml");
		if(!localFile.exists())
		{
			System.out.println("File CommunityBufferConfig.xml not found!");
			return;
		}
		Document localDocument = null;
		try
		{
			final DocumentBuilderFactory localDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
			localDocumentBuilderFactory.setValidating(false);
			localDocumentBuilderFactory.setIgnoringComments(true);
			localDocument = localDocumentBuilderFactory.newDocumentBuilder().parse(localFile);
		}
		catch(final Exception e1)
		{
			e1.printStackTrace();
		}
		try
		{
			parseFile(localDocument);
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}

	public final int getSkillHashCode(final int skillId, final int skillLevel)
	{
		return skillId * 1021 + skillLevel;
	}

	public final boolean isAvailable(final int skillId, final int skillLevel)
	{
		return available_skills.binarySearch(getSkillHashCode(skillId, skillLevel)) >= 0;
	}

	private void parseFile(final Document doc)
	{
		for(Node il = doc.getFirstChild(); il != null; il = il.getNextSibling())
			if("list".equalsIgnoreCase(il.getNodeName()))
				for(Node area = il.getFirstChild(); area != null; area = area.getNextSibling())
					if("buffPackage".equalsIgnoreCase(area.getNodeName()))
					{
						final int id = XMLUtil.getAttributeIntValue(area, "id", 0);
						final ArrayList<L2Skill> skills = new ArrayList<L2Skill>();
						for(Node buff = area.getFirstChild(); buff != null; buff = buff.getNextSibling())
							if("buff".equalsIgnoreCase(buff.getNodeName()))
							{
								final int skillId = XMLUtil.getAttributeIntValue(buff, "skillID", 0);
								final int skillLevel = XMLUtil.getAttributeIntValue(buff, "skillLevel", 0);

								final L2Skill skill = SkillTable.getInstance().getInfo(skillId, skillLevel);
								if(skill == null)
									continue;

								skills.add(skill);
								_allBuffs.put(skillId, skillLevel);
								if(!available_skills.contains(getSkillHashCode(skillId, skillLevel)))
									available_skills.add(getSkillHashCode(skillId, skillLevel));

								if(SkillTable.getInstance().getBaseLevel(skillId) < skillLevel)
									_log.info("skillId: " + skillId + " maxlevel = " + SkillTable.getInstance().getBaseLevel(skillId) + " current = " + skillLevel);
							}
						_buffs.put(id, skills);
					}

		final ArrayList<L2Skill> skills = new ArrayList<L2Skill>(_allBuffs.size());
		_allBuffs.forEachEntry(new TIntIntProcedure()
		{
			@Override
			public boolean execute(final int id, final int level)
			{
				skills.add(SkillTable.getInstance().getInfo(id, level));
				return true;
			}
		});

		_buffs.put(0, skills);

		// Sorting for binarySearch
		available_skills.sort();
	}

	@Override
	public void parsecmd(final String command, final L2Player activeChar)
	{
		final String[] commands = command.split(";");
		if(commands.length == 0)
			return;

		if(commands[0].equals("_bbsbuff") && commands.length == 1)
			showBuffIndexPage(activeChar);
		else if(commands[1].equalsIgnoreCase("buff"))
		{
			int skill_id = 0, skill_level = 0;
			try
			{
				skill_id = Integer.parseInt(commands[2]);
				skill_level = Integer.parseInt(commands[3]);
			}
			catch(final Exception e)
			{}

			if(skill_id > 0 && skill_level > 0)
				buffOne(activeChar, skill_id, skill_level);
		}
		else if(commands[1].equalsIgnoreCase("buff_scheme"))
		{
			int scheme_id = -1;
			String target = null;
			try
			{
				scheme_id = Integer.parseInt(commands[2]);
				target = commands[3];
			}
			catch(final Exception e)
			{}

			if(scheme_id >= 0 && target != null)
				buffGroup(activeChar, scheme_id, target);
		}
		else if(commands[1].equalsIgnoreCase("bufffixedgrp"))
		{
			final int scheme_id = Integer.parseInt(commands[2]);
			final String target = commands[3];
			buffFixedGroup(activeChar, scheme_id, target);
		}
		else if(commands[1].equalsIgnoreCase("create_scheme"))
		{
			if(!Config.COMMUNITY_BOARD_BUFFER_ALLOW_SAVE_RESTOR)
			{
				ShowBoard.disabled(activeChar);
				return;
			}

			String group_name_add = null;
			try
			{
				group_name_add = commands[2];
			}
			catch(final Exception e)
			{
				showBuffIndexPage(activeChar);
				return;
			}

			createScheme(activeChar, group_name_add.trim());
			showBuffIndexPage(activeChar);
		}
		else if(commands[1].equalsIgnoreCase("delete_scheme"))
		{
			final int scheme_id = Integer.parseInt(commands[2]);
			deleteScheme(activeChar, scheme_id);
			showBuffIndexPage(activeChar);
		}
		else if(commands[1].equalsIgnoreCase("use"))
		{
			if(!Config.COMMUNITY_BOARD_BUFFER_ALLOW_SAVE_RESTOR)
			{
				ShowBoard.disabled(activeChar);
				return;
			}

			final int scheme_id = Integer.parseInt(commands[2]);
			final int page = Integer.parseInt(commands[3]);
			showBuffGroup(activeChar, scheme_id, page);
		}
		else if(commands[1].equalsIgnoreCase("usefixed"))
		{
			final int scheme_id = Integer.parseInt(commands[2]);
			final int page = Integer.parseInt(commands[3]);
			showFixedBuffGroup(activeChar, scheme_id, page);
		}
		else if(commands[1].equalsIgnoreCase("edit_scheme"))
		{
			final int scheme_id = Integer.parseInt(commands[2]);
			final int page = Integer.parseInt(commands[3]);
			editScheme(activeChar, scheme_id, page);
		}
		else if(commands[1].equalsIgnoreCase("show_available_skills"))
		{
			final int scheme_id = Integer.parseInt(commands[2]);
			final int page = Integer.parseInt(commands[3]);
			showAvailableSkill(activeChar, scheme_id, page);
		}
		else if(commands[1].equalsIgnoreCase("add_skill"))
		{
			final int scheme_id = Integer.parseInt(commands[2]);
			final int skill_id = Integer.parseInt(commands[3]);
			final int skill_level = Integer.parseInt(commands[4]);
			final int page = Integer.parseInt(commands[5]);
			addSkill(activeChar, scheme_id, skill_id, skill_level);
			// показываем группу из которой добавляли
			showAvailableSkill(activeChar, scheme_id, page);
		}
		else if(commands[1].equalsIgnoreCase("delete_skill"))
		{
			final int scheme_id = Integer.parseInt(commands[2]);
			final int skill_id = Integer.parseInt(commands[3]);
			final int page = Integer.parseInt(commands[4]);
			deleteSkill(activeChar, scheme_id, skill_id);
			// показываем скилы которорые уже есть, для удаления
			editScheme(activeChar, scheme_id, page);
		}
		else if(commands[1].equalsIgnoreCase("start_autobuff"))
		{
			final int scheme_id = Integer.parseInt(commands[2]);
			Integer fix_scheme_id = null;
			try
			{
				fix_scheme_id = Integer.parseInt(commands[3]);
			}
			catch(final NumberFormatException e)
			{}
			final int price = Integer.parseInt(commands[4]);
			startAutoBuff(activeChar, scheme_id, fix_scheme_id, price);
		}
		else if(commands[1].equalsIgnoreCase("stop_autobuff"))
		{
			final int scheme_id = Integer.parseInt(commands[2]);
			Integer fix_scheme_id = null;
			try
			{
				fix_scheme_id = Integer.parseInt(commands[3]);
			}
			catch(final NumberFormatException e)
			{}
			stopAutoBuff(activeChar, scheme_id, fix_scheme_id);
		}
		else if(commands[1].equalsIgnoreCase("restoreCP"))
		{
			restoreCP(activeChar);
			return;
		}
		else if(commands[1].equalsIgnoreCase("restoreHP"))
		{
			restoreHP(activeChar);
			return;
		}
		else if(commands[1].equalsIgnoreCase("restoreMP"))
		{
			restoreMP(activeChar);
			return;
		}
		else if(commands[1].equalsIgnoreCase("cancel"))
		{
			cancel(activeChar);
			return;
		}
	}

	/**
	 * Бафаем гриппу баффов составленную администратором сервера.<br>
	 * Формируется переменная со списком всех баффов из группы и передается в startBuffGroup
	 */
	private void buffFixedGroup(final L2Player activeChar, final int group_id, final String target)
	{
		try
		{
			if(chekCondition(activeChar))
			{
				// получаем список доступных скилов для данной группы
				final ArrayList<L2Skill> skills = _buffs.get(group_id);
				if(skills != null)
				{
					final int price = skills.size() * Config.COMMUNITY_BOARD_BUFFER_PRICE_ONE * Config.COMMUNITY_BOARD_BUFFER_PRICE_MOD_GRP * Config.COMMUNITY_BOARD_BUFFER_LVL_MOD[activeChar.getLevel() - 1];
					if(price > 0 && activeChar.getAdena() < price)
					{
						activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
						return;
					}

					if(price > 0)
						activeChar.reduceAdena(price, false);
					startBuffGroup(activeChar, skills, target);
				}
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "BuffBBSManager: buffFixedGroup() error: ", e);
		}
	}

	/**
	 * Бафаем группу баффов сохранённую игроком.<br>
	 * Формируется переменная со списком всех баффов из группы и передается в startBuffGroup
	 *
	 * @param activeChar
	 *            - тот кто запускает
	 * @param groupId
	 *            - номер группы
	 * @param target
	 *            - цель для баффа
	 */
	private void buffGroup(final L2Player activeChar, final int groupId, final String target)
	{
		try
		{
			if(chekCondition(activeChar))
			{
				// получаем список доступных скилов для данной схемы
				final ArrayList<L2Skill> skillIds = getUsersBuffs(activeChar.getObjectId(), groupId);

				// считаем цену
				final int price = skillIds.size() * Config.COMMUNITY_BOARD_BUFFER_PRICE_ONE * Config.COMMUNITY_BOARD_BUFFER_PRICE_MOD_GRP * Config.COMMUNITY_BOARD_BUFFER_LVL_MOD[activeChar.getLevel() - 1];
				if(price > 0 && activeChar.getAdena() < price)
				{
					activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
					return;
				}

				if(price > 0)
					activeChar.reduceAdena(price, true);

				startBuffGroup(activeChar, skillIds, target);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "BuffBBSManager: buffGroup() error: ", e);
		}
	}

	/**
	 * Запуск баффа для группы (общий метод)
	 *
	 * @param activeChar
	 *            - тот кто бафает
	 * @param target
	 *            - цель для бафа
	 * @param allbuff
	 *            - список всех баффов из группы. Значение устанавливается в методах buffGroup и buffFixedGroup
	 */
	private static final void startBuffGroup(final L2Player activeChar, final List<L2Skill> skills, final String target)
	{
		if(activeChar == null || skills == null || skills.isEmpty())
			return;

		if(target.startsWith(" Player"))
			applyEffect(activeChar, skills);
		else if(target.startsWith(" Pet") && activeChar.getPet() != null)
			applyEffect(activeChar.getPet(), skills);
	}

	/**
	 * Бафаем один бафф
	 *
	 * @param activeChar
	 *            - кого бафаем
	 * @param skillId
	 *            - ID скила
	 * @param skillLvl
	 *            - уровень скила
	 */
	private void buffOne(final L2Player activeChar, final int skillId, final int skillLvl)
	{
		if(!chekCondition(activeChar))
			return;

		if(!isAvailable(skillId, skillLvl))
		{
			Util.handleIllegalPlayerAction(activeChar, "BuffBBSManager[639]", "buff illegal skill", IllegalPlayerAction.CRITICAL);
			return;
		}

		final L2Skill skill = SkillTable.getInstance().getInfo(skillId, skillLvl);
		if(skill == null)
			return;

		final int price = Config.COMMUNITY_BOARD_BUFFER_PRICE_ONE * Config.COMMUNITY_BOARD_BUFFER_LVL_MOD[activeChar.getLevel() - 1];
		if(price > 0 && activeChar.getAdena() < price)
		{
			activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			return;
		}

		if(price > 0)
			activeChar.reduceAdena(price, true);

		activeChar.getPlayer().setCommunityBuffTime(250);
		L2GameThreadPools.getInstance().scheduleGeneral(new Buff(skill, activeChar), 250);
	}

	/** Удаляем скилл из набора. */
	private void deleteSkill(final L2Player activeChar, final int group_id, final int skill_id)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM community_buffs_group_skills WHERE group_id=? AND skill_id=? AND object_id=?");
			statement.setInt(1, group_id);
			statement.setInt(2, skill_id);
			statement.setInt(3, activeChar.getObjectId());
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "BuffBBSManager: deleteBuffFromGroup() error: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/** Добавляем бафф в набор. */
	private void addSkill(final L2Player activeChar, final int group_id, final int skill_id, final int skill_lvl)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			// получем сколько сейчас в группе у игрока баффов
			statement = con.prepareStatement("SELECT COUNT(*) FROM community_buffs_group_skills WHERE object_id=? AND group_id=?");
			statement.setInt(1, activeChar.getObjectId());
			statement.setInt(2, group_id);
			rs = statement.executeQuery();

			rs.next();
			if(rs.getInt(1) < MAX_SKILLS_IN_SCHEME)
			{
				statement = con.prepareStatement("REPLACE INTO community_buffs_group_skills (`group_id`,`object_id`,`skill_id`,`skill_level`) VALUES(?,?,?,?)");
				statement.setInt(1, group_id);
				statement.setInt(2, activeChar.getObjectId());
				statement.setInt(3, skill_id);
				statement.setInt(4, skill_lvl);
				statement.execute();
			}
			else
				activeChar.sendMessage("Набор не может содержать более " + MAX_SKILLS_IN_SCHEME + " баффов");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "BuffBBSManager: addBuffInGroup() error: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	/** Отображаем список скилов которые можно добавить в схему, с учётом уже имеющихся в схеме скилов */
	private void showAvailableSkill(final L2Player activeChar, final int group_id, final int page)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			// получаем данные схемы игрока (скилы игрока)
			statement = con.prepareStatement("SELECT `s`.`skill_id`, `g`.`group_name` FROM `community_buffs_group_skills` `s` LEFT JOIN `community_buffs_group` `g` ON (`s`.`object_id` = `g`.`objectId` AND `s`.`group_id` = `g`.`groupId`) WHERE `object_id` = ? AND `group_id` = ?");
			statement.setInt(1, activeChar.getObjectId());
			statement.setInt(2, group_id);
			rs = statement.executeQuery();

			String group_name = null;
			// создаём список, в котором будут храниться id скилов которые есть у игрока
			final ArrayList<Integer> exist_skillIds = new ArrayList<Integer>();
			while (rs.next())
			{
				if(group_name == null)
					group_name = rs.getString("group_name");
				exist_skillIds.add(rs.getInt("skill_id"));
			}

			List<int[]> skillIds = new ArrayList<int[]>();
			// ищем скилы которых ещё нет в группе, которые можно добавить
			for(final TIntIntIterator iter = _allBuffs.iterator(); iter.hasNext();)
			{
				iter.advance();
				// если такого скила нету, то добавляем
				if(!exist_skillIds.contains(iter.key()))
					skillIds.add(new int[] { iter.key(), iter.value() });
			}

			final int numpages = getNumPages(skillIds.size());
			// обрезаем под размеры (макс количество на страницу)
			skillIds = getSkills(skillIds, page);

			final StringBuilder html = StringUtil.startAppend(600, buildTable(skillIds, TYPE_ADD, group_id, page, "level"));
			final StringBuilder htmltoppanel = StringUtil.startAppend(600, "<table width=600><tr><td><font color=30D249>", activeChar.isLangRus() ? "Редактирование" : "Edit", ": ", group_name, "</font></td><td></td>");
			StringUtil.append(htmltoppanel, "<td><button value=\"", activeChar.isLangRus() ? "Назад" : "Back", "\" action=\"bypass -h _bbsbuff;use;", group_id, ";1\" width=50 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td></tr></table>");
			StringUtil.append(htmltoppanel, page_list(numpages, page, group_id, "show_available_skills"));

			String content = Files.read("data/html/CommunityBoard/buffer-list.htm", activeChar);
			content = content.replace("%buffgrptoppanel%", htmltoppanel.toString());
			content = content.replace("%buffgrp%", html.toString());
			content = content.replace("%price%", "" + Config.COMMUNITY_BOARD_BUFFER_PRICE_ONE * Config.COMMUNITY_BOARD_BUFFER_LVL_MOD[activeChar.getLevel() - 1]);

			separateAndSend(content, activeChar);
			StringUtil.recycle(html);
			StringUtil.recycle(htmltoppanel);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "BuffBBSManager: editAddBuffInGroup() error: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	/**
	 * Открывает страницу редактора набора. Список уже имеющихся баффов в наборе. Которые можно удалить!
	 */
	private void editScheme(final L2Player activeChar, final int group_id, final int page)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT `s`.`skill_id`, `s`.`skill_level`, `g`.`group_name` FROM `community_buffs_group_skills` `s` LEFT JOIN `community_buffs_group` `g` ON (`s`.`object_id` = `g`.`objectId` AND `s`.`group_id` = `g`.`groupId`) WHERE `object_id`=? AND `group_id` = ?");
			statement.setInt(1, activeChar.getObjectId());
			statement.setInt(2, group_id);
			rs = statement.executeQuery();

			String group_name = null;
			List<int[]> skillIds = new ArrayList<int[]>();
			while (rs.next())
			{
				if(group_name == null)
					group_name = rs.getString("group_name");
				skillIds.add(new int[] { rs.getInt("skill_id"), rs.getInt("skill_level") });
			}

			final int numpages = getNumPages(skillIds.size());
			// обрезаем под размеры (макс количество на страницу)
			skillIds = getSkills(skillIds, page);

			final StringBuilder html_skills = StringUtil.startAppend(500, buildTable(skillIds, TYPE_DELETE, group_id, page, "level"));
			final StringBuilder htmltoppanel = StringUtil.startAppend(500, "<table width=600><tr>");
			StringUtil.append(htmltoppanel, "<td><font color=30D249>", activeChar.isLangRus() ? "Редактирование" : "Edit", ": ", group_name, "</font></td>");
			StringUtil.append(htmltoppanel, "<td></td>");
			StringUtil.append(htmltoppanel, "<td><button value=\"", activeChar.isLangRus() ? "Добавить бафф" : "Add buff", "\" action=\"bypass -h _bbsbuff;show_available_skills;", group_id, ";1\" width=140 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
			StringUtil.append(htmltoppanel, "<td><button value=\"", activeChar.isLangRus() ? "Удалить набор" : "Delete scheme", "\" action=\"bypass -h _bbsbuff;delete_scheme;", group_id, "\" width=140 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
			StringUtil.append(htmltoppanel, "<td><button value=\"", activeChar.isLangRus() ? "Назад" : "Back", "\" action=\"bypass -h _bbsbuff;use;", group_id, ";1\" width=100 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
			StringUtil.append(htmltoppanel, "</tr>");
			StringUtil.append(htmltoppanel, "</table>");
			StringUtil.append(htmltoppanel, page_list(numpages, page, group_id, "edit_scheme"));

			String content = Files.read("data/html/CommunityBoard/buffer-list.htm", activeChar);
			content = content.replace("%buffgrptoppanel%", htmltoppanel.toString());
			content = content.replace("%buffgrp%", html_skills.toString());
			content = content.replace("%price%", "" + Config.COMMUNITY_BOARD_BUFFER_PRICE_ONE * Config.COMMUNITY_BOARD_BUFFER_LVL_MOD[activeChar.getLevel() - 1]);

			separateAndSend(content, activeChar);
			StringUtil.recycle(html_skills);
			StringUtil.recycle(htmltoppanel);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "BuffBBSManager: editBuffGroup() error: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	/**
	 * Показываем страницу фиксированной группы.<br>
	 * Группа составляется администратором и не может быть отредактированна игроком.
	 */
	private void showFixedBuffGroup(final L2Player activeChar, final int groupId, final int page)
	{
		try
		{
			List<L2Skill> buffs = new ArrayList<L2Skill>(_buffs.get(groupId));
			final long price = buffs.size() * Config.COMMUNITY_BOARD_BUFFER_PRICE_ONE * Config.COMMUNITY_BOARD_BUFFER_PRICE_MOD_GRP * Config.COMMUNITY_BOARD_BUFFER_LVL_MOD[activeChar.getLevel() - 1];

			final int numpages = getNumPages(buffs.size());
			// обрезаем под размеры (макс количество на страницу)
			buffs = getSkills(buffs, page);

			final StringBuilder html = StringUtil.startAppend(600, buildTable2(buffs, TYPE_BUFF, 0, page, "level"));
			final StringBuilder htmltoppanel = StringUtil.startAppend(600, "<table width=600><tr>", "<td><font color=30D249>[Все: ", price, " aden]</font>", "</td>");
			StringUtil.append(htmltoppanel, "<td>Себе:</td>");
			StringUtil.append(htmltoppanel, "<td><button value=\"", activeChar.isLangRus() ? "Все" : "All", "\" action=\"bypass -h _bbsbuff;bufffixedgrp;", groupId, "; Player\" width=40 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");

			if(Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF)
				if(!activeChar.isCommunityAutoBuff())
					StringUtil.append(htmltoppanel, "<td><button value=\"", activeChar.isLangRus() ? "Включить авто-бафф" : "Enable auto-buff", "\" action=\"bypass -h _bbsbuff;start_autobuff;0;", groupId, ";", price, "\" width=140 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
				else
					StringUtil.append(htmltoppanel, "<td><button value=\"", activeChar.isLangRus() ? "Отключить авто-бафф" : "Disable auto-buff", "\" action=\"bypass -h _bbsbuff;stop_autobuff;0;", groupId, "\" width=140 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
			StringUtil.append(htmltoppanel, "</tr>");

			if(activeChar.getPet() != null)
			{
				StringUtil.append(htmltoppanel, "<tr><td></td>");
				if(numpages <= 1)
				{
					StringUtil.append(htmltoppanel, "<td>Питомец:</td>");
					StringUtil.append(htmltoppanel, "<td><button value=\"", activeChar.isLangRus() ? "Все" : "All", "\" action=\"bypass -h _bbsbuff;bufffixedgrp;" + groupId + "; Pet\" width=40 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
				}
				StringUtil.append(htmltoppanel, "<td></td></tr>");
			}

			StringUtil.append(htmltoppanel, "</table>");
			StringUtil.append(htmltoppanel, page_list(numpages, page, groupId, "usefixed"));

			String content = Files.read("data/html/CommunityBoard/buffer-list.htm", activeChar);
			content = content.replace("%buffgrptoppanel%", htmltoppanel.toString());
			content = content.replace("%buffgrp%", html.toString());
			content = content.replace("%price%", "" + Config.COMMUNITY_BOARD_BUFFER_PRICE_ONE * Config.COMMUNITY_BOARD_BUFFER_LVL_MOD[activeChar.getLevel() - 1]);

			separateAndSend(content, activeChar);

			StringUtil.recycle(html);
			StringUtil.recycle(htmltoppanel);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "BuffBBSManager: showFixedBuffGroup() error: ", e);
		}
		finally
		{}
	}

	/**
	 * Показываем страницу группы
	 */
	private void showBuffGroup(final L2Player activeChar, final int group_id, final int page)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT `s`.`skill_id`, `s`.`skill_level`, `g`.`group_name` FROM `community_buffs_group_skills` `s` LEFT JOIN `community_buffs_group` `g` ON (`s`.`object_id` = `g`.`objectId` AND `s`.`group_id` = `g`.`groupId`) WHERE `object_id`=? AND `group_id` = ?");
			statement.setInt(1, activeChar.getObjectId());
			statement.setInt(2, group_id);
			rs = statement.executeQuery();

			String group_name = null;
			List<int[]> buffIds = new ArrayList<int[]>();
			while (rs.next())
			{
				if(group_name == null)
					group_name = rs.getString("group_name");
				buffIds.add(new int[] { rs.getInt("skill_id"), rs.getInt("skill_level") });
			}

			final int price = buffIds.size() * Config.COMMUNITY_BOARD_BUFFER_PRICE_ONE * Config.COMMUNITY_BOARD_BUFFER_PRICE_MOD_GRP * Config.COMMUNITY_BOARD_BUFFER_LVL_MOD[activeChar.getLevel() - 1];

			final int numpages = getNumPages(buffIds.size());
			// обрезаем под размеры (макс количество на страницу)
			buffIds = getSkills(buffIds, page);

			final StringBuilder html = StringUtil.startAppend(500, buildTable(buffIds, TYPE_BUFF, group_id, page, "level"));

			final StringBuilder htmltoppanel = StringUtil.startAppend(300, "<table width=600><tr><td><font color=30D249>", group_name, " [Все: ", price, " aden]</font></td><td>Себе:</td>");
			StringUtil.append(htmltoppanel, "<td><button value=\"", activeChar.isLangRus() ? "Все" : "All", "\" action=\"bypass -h _bbsbuff;buff_scheme;", group_id, "; Player\" width=40 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");

			if(Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF)
				if(!activeChar.isCommunityAutoBuff())
					StringUtil.append(htmltoppanel, "<td><button value=\"", activeChar.isLangRus() ? "Включить авто-бафф" : "Enable auto-buff", "\" action=\"bypass -h _bbsbuff;start_autobuff;", group_id, ";null;", price, "\" width=140 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
				else
					StringUtil.append(htmltoppanel, "<td><button value=\"", activeChar.isLangRus() ? "Отключить авто-бафф" : "Disable auto-buff", "\" action=\"bypass -h _bbsbuff;stop_autobuff;", group_id, ";null\" width=140 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");

			StringUtil.append(htmltoppanel, "<td><button value=\"", activeChar.isLangRus() ? "Редактирование" : "Edit", "\" action=\"bypass -h _bbsbuff;edit_scheme;", group_id, ";1\" width=100 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
			StringUtil.append(htmltoppanel, "</tr>");

			if(activeChar.getPet() != null)
			{
				StringUtil.append(htmltoppanel, "<tr><td></td><td>Питомец:</td>");
				StringUtil.append(htmltoppanel, "<td><button value=\"", activeChar.isLangRus() ? "Все" : "All", "\" action=\"bypass -h _bbsbuff;buff_scheme;", group_id, "; Pet\" width=40 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td><td></td></tr>");
			}
			StringUtil.append(htmltoppanel, "</table>");
			StringUtil.append(htmltoppanel, page_list(numpages, page, group_id, "use"));

			String content = Files.read("data/html/CommunityBoard/buffer-list.htm", activeChar);
			content = content.replace("%buffgrptoppanel%", htmltoppanel.toString());
			content = content.replace("%buffgrp%", html.toString());
			content = content.replace("%price%", "" + Config.COMMUNITY_BOARD_BUFFER_PRICE_ONE * Config.COMMUNITY_BOARD_BUFFER_LVL_MOD[activeChar.getLevel() - 1]);

			separateAndSend(content, activeChar);
			StringUtil.recycle(html);
			StringUtil.recycle(htmltoppanel);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "BuffBBSManager: showBuffGroup() error: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	/**
	 * Удаляем группу баффов.
	 */
	private void deleteScheme(final L2Player activeChar, final int group_id)
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			// Удаляем группу бафов.
			statement = con.createStatement();
			statement.executeUpdate("DELETE FROM community_buffs_group WHERE objectId=" + activeChar.getObjectId() + " AND groupId=" + group_id);
			DbUtils.closeQuietly(statement);

			// Удаляем баффы которые были в удаляемой группе.
			statement = con.createStatement();
			statement.executeUpdate("DELETE FROM community_buffs_group_skills WHERE object_id=" + activeChar.getObjectId() + " AND group_id=" + group_id);
			DbUtils.closeQuietly(statement);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "BuffBBSManager: deleteBuffGroup() error: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * Создаем группу баффов.
	 *
	 * @param
	 */
	private void createScheme(final L2Player activeChar, String group_name)
	{
		if(group_name.equals("") || group_name.equals(null))
		{
			activeChar.sendPacket(CustomSystemMessageId.L2CB_BUFFER_NOT_ENTER_SCHEME_NAME.getPacket());
			return;
		}

		// обрезаем пробелы)
		group_name = group_name.trim();
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			// Получаем кол-во групп текущего чара из таблицы и если кол-во их не превышает установленное добавляем новую.
			statement = con.prepareStatement("SELECT COUNT(*) FROM community_buffs_group WHERE objectId=?");
			statement.setLong(1, activeChar.getObjectId());
			rs = statement.executeQuery();

			rs.next();
			if(rs.getInt(1) <= 4)
			{
				// Проверяем существует ли группа с именем которое передано в параметре
				statement = con.prepareStatement("SELECT COUNT(*) FROM community_buffs_group WHERE objectId=? AND group_name=?");
				statement.setLong(1, activeChar.getObjectId());
				statement.setString(2, group_name);
				rs = statement.executeQuery();

				rs.next();
				if(rs.getInt(1) == 0)
				{
					// Если группы нет, создаем.
					statement = con.prepareStatement("INSERT INTO community_buffs_group (objectId,group_name) VALUES(?,?)");
					statement.setInt(1, activeChar.getObjectId());
					statement.setString(2, group_name);
					statement.execute();
					statement.close();
				}
				else
				// надо ли это?
				{}
			}
			else
				activeChar.sendPacket(CustomSystemMessageId.L2CB_BUFFER_CANT_SAVE_MORE_THAN_4_SCHEME.getPacket());
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "BuffBBSManager: addBuffGroup() error: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	/**
	 * Показываем чару первую страницу баффера со списком его персональных групп.
	 */
	private void showBuffIndexPage(final L2Player activeChar)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			st = con.prepareStatement("SELECT * FROM community_buffs_group WHERE objectId=?");
			st.setLong(1, activeChar.getObjectId());
			rs = st.executeQuery();

			final StringBuilder html = StringUtil.startAppend(400, "<table width=220>");
			int group_id;
			String group_name;
			while (rs.next())
			{
				group_id = rs.getInt("groupId");
				group_name = rs.getString("group_name");
				StringUtil.append(html, "<tr><td>");
				StringUtil.append(html, "<button value=\"", group_name, "\" action=\"bypass -h _bbsbuff;use;", group_id, ";1\" width=190 height=20 back=\"L2UI_ct1.Button_DF_Down\" fore=\"L2UI_ct1.button_df\">");
				StringUtil.append(html, "</td></tr>");
			}
			StringUtil.append(html, "</table>");

			String content = Files.read("data/html/CommunityBoard/buffer.htm", activeChar);
			content = content.replace("%buffgrps%", html.toString());
			separateAndSend(content, activeChar);

			StringUtil.recycle(html);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "BuffBBSManager: showBuffIndexPage() error: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, st, rs);
		}
	}

	/**
	 * Строит страничку для отображения скилов: для бафа, для удаления, для добавления
	 *
	 * @param skill_ids
	 *            - список бафов
	 * @param type
	 *            - тип отображаемой таблицы: 1 - баф, 2 - удаление из группы, 3 - добавление в группу.
	 * @param scheme_id
	 * @param levelString
	 *            TODO
	 * @param cols_number
	 *            - количество колонок
	 * @return
	 */
	private String buildTable(final List<int[]> skill_ids, final int type, final int scheme_id, final int page, final String levelString)
	{
		L2Skill skill;
		String bottom = null;
		final StringBuilder res = StringUtil.startAppend(400, "<table width=600>");
		final int rows = (int) Math.ceil((double) skill_ids.size() / cols_number);
		int c = 0;
		for(int i = 0; i < rows; i++)
		{
			StringUtil.append(res, "<tr>");
			for(int j = 0; j < cols_number; j++)
			{
				if(skill_ids.size() > c)
				{
					final int skill_id = skill_ids.get(c)[0];
					final int skill_level = skill_ids.get(c)[1];
					skill = SkillTable.getInstance().getInfo(skill_id, skill_level);
					if(skill == null)
					{
						_log.warning("BuffBBSManager: buildTable - skill[" + skill_id + "], group " + scheme_id + " is NULL!");
						c++;
						continue;
					}

					switch (type)
					{
						case TYPE_BUFF:// баф
						{
							bottom = StringUtil.concat("<td width=25><button value=\"$\" action=\"bypass -h _bbsbuff;buff;", skill.getId(), ";", skill.getLevel(), ";", page, "\" width=25 height=32 back=\"L2UI_ct1.Button_DF_Down\" fore=\"L2UI_ct1.button_df\"></td>");
							break;
						}
						case TYPE_DELETE:// удаление из группы
						{
							bottom = StringUtil.concat("<td width=25><button value=\"-\" action=\"bypass -h _bbsbuff;delete_skill;", scheme_id, ";", skill.getId(), ";", page, "\" width=25 height=32 back=\"L2UI_ct1.Button_DF_Down\" fore=\"L2UI_ct1.button_df\"></td>");
							break;
						}
						case TYPE_ADD:// добавление в группу
						{
							bottom = StringUtil.concat("<td width=25><button value=\"+\" action=\"bypass -h _bbsbuff;add_skill;", scheme_id, ";", skill.getId(), ";", skill.getLevel(), ";", page, "\" width=25 height=32 back=\"L2UI_ct1.Button_DF_Down\" fore=\"L2UI_ct1.button_df\"></td>");
							break;
						}
					}

					StringUtil.append(res, "<td width=185><center><table width=185 height=32><tr><td width=32><center><img src=icon.", skill.getIcon(), " width=32 height=32></center></td>", bottom, "<td width=128><table width=128><tr><td><font color=3293F3>", skill.getName(), "</font></td></tr><tr><td><font color=F2C202>", levelString, ": ", skill_level, "</font></td></tr></table></td></tr></table></center></td>");
				}
				else
					StringUtil.append(res, "<td width=150><center></center></td>");
				c++;
			}

			StringUtil.append(res, "</tr>");
		}
		StringUtil.append(res, "</table><br>");
		return res.toString();
	}

	/**
	 * Строит страничку для отображения скилов: для бафа, для удаления, для добавления
	 *
	 * @param skills
	 *            - список бафов
	 * @param type
	 *            - тип отображаемой таблицы: 1 - баф, 2 - удаление из группы, 3 - добавление в группу.
	 * @param scheme_id
	 * @param levelString
	 *            TODO
	 * @param cols_number
	 *            - количество колонок
	 * @return
	 */
	private String buildTable2(final List<L2Skill> skills, final int type, final int scheme_id, final int page, final String levelString)
	{
		String bottom = null;
		final StringBuilder res = StringUtil.startAppend(400, "<table width=600>");
		final int rows = (int) Math.ceil((double) skills.size() / cols_number);
		int c = 0;
		for(int i = 0; i < rows; i++)
		{
			StringUtil.append(res, "<tr>");
			for(int j = 0; j < cols_number; j++)
			{
				if(skills.size() > c)
				{
					final L2Skill skill = skills.get(c);
					if(skill == null)
					{
						_log.warning("BuffBBSManager: skill in group " + scheme_id + " is NULL!");
						c++;
						continue;
					}

					switch (type)
					{
						case TYPE_BUFF:// баф
						{
							bottom = StringUtil.concat("<td width=25><button value=\"+\" action=\"bypass -h _bbsbuff;buff;", skill.getId(), ";", skill.getLevel(), ";", page, "\" width=25 height=32 back=\"L2UI_ct1.Button_DF_Down\" fore=\"L2UI_ct1.button_df\"></td>");
							break;
						}
						case TYPE_DELETE:// удаление из группы
						{
							bottom = StringUtil.concat("<td width=25><button value=\"-\" action=\"bypass -h _bbsbuff;delete_skill;", scheme_id, ";", skill.getId(), ";", page, "\" width=25 height=32 back=\"L2UI_ct1.Button_DF_Down\" fore=\"L2UI_ct1.button_df\"></td>");
							break;
						}
						case TYPE_ADD:// добавление в группу
						{
							bottom = StringUtil.concat("<td width=25><button value=\"+\" action=\"bypass -h _bbsbuff;add_skill;", scheme_id, ";", skill.getId(), ";", skill.getLevel(), ";", page, "\" width=25 height=32 back=\"L2UI_ct1.Button_DF_Down\" fore=\"L2UI_ct1.button_df\"></td>");
							break;
						}
					}

					StringUtil.append(res, "<td width=185><center><table width=185 height=32><tr><td width=32><center><img src=icon.", skill.getIcon(), " width=32 height=32></center></td>", bottom, "<td width=128><table width=128><tr><td><font color=3293F3>", skill.getName(), "</font></td></tr><tr><td><font color=F2C202>", levelString, ": ", skill.getLevel(), "</font></td></tr></table></td></tr></table></center></td>");
				}
				else
					StringUtil.append(res, "<td width=150><center></center></td>");
				c++;
			}

			StringUtil.append(res, "</tr>");
		}
		StringUtil.append(res, "</table><br>");
		return res.toString();
	}

	private String page_list(final double numpages, final int cur_page, final int scheme_id, final String bypass)
	{
		if(numpages == 1)
			return null;

		final StringBuilder navpage = StringUtil.startAppend(200, "<table width=", numpages * 25, "><tr>");
		for(int page = 1; page <= numpages; page++)
			if(page == cur_page)
				StringUtil.append(navpage, "<td align=center width=25>", page, "</td>");
			else
				StringUtil.append(navpage, "<td width=25><button value=\"", page, "\" action=\"bypass -h _bbsbuff;", bypass, ";", scheme_id, ";", page, "\" width=25 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");

		StringUtil.append(navpage, "</tr></table>");
		return navpage.toString();
	}

	private final static <T> List<T> getSkills(List<T> scheme_skills, final int page)
	{
		final int offset = MAX_SKILLS_ON_PAGE * (page - 1);
		// обрезаем под размеры (макс количество на страницу)
		if(scheme_skills.size() > MAX_SKILLS_ON_PAGE)
		{
			final int maxIndex = Math.min(scheme_skills.size(), MAX_SKILLS_ON_PAGE * page);
			scheme_skills = scheme_skills.subList(offset, maxIndex);
		}
		return scheme_skills;
	}

	private final static int getNumPages(final int size)
	{
		return (int) Math.ceil((double) size / MAX_SKILLS_ON_PAGE);
	}

	private static final boolean chekCondition(final L2Player activeChar)
	{
		if(activeChar == null || !activeChar.isConnected() || activeChar.isInOfflineMode() || activeChar.isLogoutStarted() || activeChar.getCommunityBuffTime() > System.currentTimeMillis())
			return false;

		if(activeChar.isDead() || activeChar.isAlikeDead() || activeChar.isCastingNow() || activeChar.isInCombat() || activeChar.isAttackingNow() || activeChar.isFlying() || activeChar.isCombatFlagEquipped() || activeChar.isInZone(ZoneType.no_escape))
		{
			activeChar.sendMessage(new CustomMessage("common.notAvailable", activeChar));
			return false;
		}

		if(Config.COMMUNITY_BOARD_BUFFER_ALLOW_PK && activeChar.getKarma() > 0)
		{
			activeChar.sendMessage(new CustomMessage("common.notAvailable", activeChar));
			return false;
		}

		if((activeChar.isInOlympiadMode() || activeChar.getOlympiadGameId() != -1 || Olympiad.isRegisteredInComp(activeChar)) && !Config.COMMUNITY_BOARD_BUFFER_ALLOW_OLYMPIAD)
		{
			activeChar.sendMessage(new CustomMessage("common.notAvailable", activeChar));
			return false;
		}

		if(activeChar.isTransformed() && !activeChar.getTransform().isDefaultActionListTransform() && !Config.COMMUNITY_BOARD_BUFFER_ALLOW_TRANSFORM)
		{
			activeChar.sendMessage(new CustomMessage("common.notAvailable", activeChar));
			return false;
		}

		// Проверяем по уровню
		if(activeChar.getLevel() > Config.COMMUNITY_BOARD_BUFFER_MAX_LVL || activeChar.getLevel() < Config.COMMUNITY_BOARD_BUFFER_MIN_LVL)
		{
			activeChar.sendPacket(CustomSystemMessageId.L2CB_BUFFER_YOUR_LEVEL_NOT_ALLOWED.getPacket());
			return false;
		}

		// нельзя использовать в отражениях
		if(!Config.COMMUNITY_BOARD_BUFFER_ALLOW_INSTANCE && activeChar.getReflectionId() > 0)
		{
			activeChar.sendMessage(new CustomMessage("common.notAvailable", activeChar));
			return false;
		}

		if(!Config.COMMUNITY_BOARD_BUFFER_ALLOW_EVENT && activeChar.isInEvent(L2EventType.NONE))
		{
			activeChar.sendPacket(CustomSystemMessageId.L2CB_BUFFER_DISABLE_IN_EVENT.getPacket());
			return false;
		}

		// Можно ли юзать бафера во время осады?
		if(!Config.COMMUNITY_BOARD_BUFFER_ALLOW_SIEGE)
		{
			final Residence residence = TownManager.getInstance().getClosestTown(activeChar).getCastle();
			final Siege siege = residence.getSiege();
			if(siege != null && siege.isInProgress())
			{
				activeChar.sendPacket(CustomSystemMessageId.L2CB_BUFFER_DISABLE_IN_SIEGE.getPacket());
				return false;
			}
		}

		return true;
	}

	/**
	 * Метод для проверки условий для слушателей
	 *
	 * @param activeChar
	 * @return
	 */
	private static final boolean chekConditionListener(final L2Player activeChar)
	{
		if(activeChar == null || !activeChar.isConnected() || activeChar.isInOfflineMode() || activeChar.isLogoutStarted() || !Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF)
			return false;

		if(!activeChar.isCommunityAutoBuff() || activeChar.getCommunityLastTime() + 1000 > System.currentTimeMillis())
			return false;

		// не работает когда игрок мертв/притворятся мертвым/кастует/в бою/атакует/на олимпиаде/зарегистрирован на олимпиаду
		if(activeChar.isDead() || activeChar.isAlikeDead() || activeChar.isCastingNow() || activeChar.isAttackingNow() || activeChar.isInOlympiadMode() || activeChar.getOlympiadGameId() != -1 || Olympiad.isRegisteredInComp(activeChar))
			return false;

		// Проверяем по уровню
		if(activeChar.getLevel() > Config.COMMUNITY_BOARD_BUFFER_MAX_LVL || activeChar.getLevel() < Config.COMMUNITY_BOARD_BUFFER_MIN_LVL)
			return false;

		// не работает когда у игрока флаг ТВ/флаг Фортов/Проклятое оружие
		if(!Config.COMMUNITY_BOARD_BUFFER_ALLOW_OTHER && (activeChar.isCombatFlagEquipped() || activeChar.isTerritoryFlagEquipped() || activeChar.isCursedWeaponEquipped()))
			return false;

		// Можно ли использовать в отражениях
		if(!Config.COMMUNITY_BOARD_BUFFER_ALLOW_INSTANCE && activeChar.getReflectionId() > 0)
			return false;

		// Можно ли использовать на ивентах
		if(!Config.COMMUNITY_BOARD_BUFFER_ALLOW_EVENT && activeChar.isInEvent(L2EventType.NONE))
			return false;

		// Можно ли использовать во время осады
		if(!Config.COMMUNITY_BOARD_BUFFER_ALLOW_SIEGE)
		{
			final Residence residence = TownManager.getInstance().getClosestTown(activeChar).getCastle();
			final Siege siege = residence.getSiege();
			if(siege != null && siege.isInProgress())
				return false;
		}

		return true;
	}

	private void restoreCP(final L2Player activeChar)
	{
		if(activeChar.getCurrentCp() == activeChar.getMaxCp())
			return;
		if(chekCondition(activeChar))
			if(Functions.getItemCount(activeChar, Config.COMMUNITY_BOARD_RESTORE_CP_ITEMID) >= Config.COMMUNITY_BOARD_RESTORE_CP_ITEMCOUNT)
			{
				Functions.removeItem(activeChar, Config.COMMUNITY_BOARD_RESTORE_CP_ITEMID, Config.COMMUNITY_BOARD_RESTORE_CP_ITEMCOUNT);
				activeChar.setCurrentCp(activeChar.getMaxCp());
			}
			else
				activeChar.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	private void restoreHP(final L2Player activeChar)
	{
		if(activeChar.getCurrentHp() == activeChar.getMaxHp())
			return;
		if(chekCondition(activeChar))
			if(Functions.getItemCount(activeChar, Config.COMMUNITY_BOARD_RESTORE_HP_ITEMID) >= Config.COMMUNITY_BOARD_RESTORE_HP_ITEMCOUNT)
			{
				Functions.removeItem(activeChar, Config.COMMUNITY_BOARD_RESTORE_HP_ITEMID, Config.COMMUNITY_BOARD_RESTORE_HP_ITEMCOUNT);
				activeChar.setCurrentHp(activeChar.getMaxHp(), false);
			}
			else
				activeChar.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	private void restoreMP(final L2Player activeChar)
	{
		if(activeChar.getCurrentMp() == activeChar.getMaxMp())
			return;

		if(chekCondition(activeChar))
			if(Functions.getItemCount(activeChar, Config.COMMUNITY_BOARD_RESTORE_MP_ITEMID) >= Config.COMMUNITY_BOARD_RESTORE_MP_ITEMCOUNT)
			{
				Functions.removeItem(activeChar, Config.COMMUNITY_BOARD_RESTORE_MP_ITEMID, Config.COMMUNITY_BOARD_RESTORE_MP_ITEMCOUNT);
				activeChar.setCurrentMp(activeChar.getMaxMp());
			}
			else
				activeChar.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	private void cancel(final L2Player activeChar)
	{
		if(activeChar.getEffectList().isEmpty())
			return;

		if(chekCondition(activeChar))
			if(Functions.getItemCount(activeChar, Config.COMMUNITY_BOARD_RESTORE_CANCEL_ITEMID) >= Config.COMMUNITY_BOARD_RESTORE_CANCEL_ITEMCOUNT)
			{
				Functions.removeItem(activeChar, Config.COMMUNITY_BOARD_RESTORE_CANCEL_ITEMID, Config.COMMUNITY_BOARD_RESTORE_CANCEL_ITEMCOUNT);
				if(Config.COMMUNITY_BOARD_RESTORE_CANCEL_POSITIVE)
				{
					activeChar.setMassUpdating(true);
					L2Skill s;
					for(final L2Effect e : activeChar.getEffectList().getEffects())
					{
						s = e.getSkill();
						if(e.isOffensive() || s.isDebuff())
							continue;

						e.exit();
					}
					activeChar.setMassUpdating(false);
					activeChar.updateStats();
					activeChar.updateEffectIcons();
				}
				else
					activeChar.stopAllEffects();
			}
			else
				activeChar.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	/** Запускаем задачу автобафа */
	private void startAutoBuff(final L2Player activeChar, final int user_groupId, final Integer fix_groupId, final int price)
	{
		if(chekCondition(activeChar))
		{
			if(price > 0 && activeChar.getAdena() < price)
			{
				activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
				return;
			}

			// устанавливаем что включен автобафф
			activeChar.setCommunityAutoBuff(true);
			// добавляем слушателей
			if(Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF)
				activeChar.addMethodInvokeListener(new AutoBuffListener());

			// если выбран автобафф фиксированной группы
			if(user_groupId == 0 && fix_groupId != null)
			{
				activeChar.sendMessage("Включен авто-бафф набора #" + fix_groupId.intValue());
				activeChar.setCommunityFixedBuff(true);
				activeChar.setCommunityBuffs(new ArrayList<L2Skill>(_buffs.get(fix_groupId.intValue())));

				// бафаем выбранную группу
				buffFixedGroup(activeChar, fix_groupId, " Player");
				showFixedBuffGroup(activeChar, fix_groupId, 1);
			}
			// если выбран автобафф схемы созданной пользователем
			else
			{
				activeChar.sendMessage("Включен авто-бафф набора #" + user_groupId);
				activeChar.setCommunityFixedBuff(false);
				activeChar.setCommunityBuffs(getUsersBuffs(activeChar.getObjectId(), user_groupId));

				// бафаем выбранную группу
				buffGroup(activeChar, user_groupId, " Player");
				showBuffGroup(activeChar, user_groupId, 1);
			}
		}
	}

	/** Останавливаем задачу автобафа */
	private void stopAutoBuff(final L2Player activeChar, final int user_groupId, final Integer fix_groupId)
	{
		activeChar.sendPacket(CustomSystemMessageId.L2CB_BUFFER_AUTOBUFF_DISABLED.getPacket());
		activeChar.setCommunityFixedBuff(false);
		activeChar.setCommunityAutoBuff(false);
		activeChar.setCommunityBuffs(null);

		// удаляем слушатели
		activeChar.removeMethodInvokeListener(MethodCollection.AutoBuff);

		if(user_groupId == 0 && fix_groupId != null)
			showFixedBuffGroup(activeChar, fix_groupId, 1);
		else
			showBuffGroup(activeChar, user_groupId, 1);
	}

	/**
	 * Получаем список доступных скилов для данной схемы
	 *
	 * @param objectId
	 * @param groupId
	 * @return
	 */
	private final static ArrayList<L2Skill> getUsersBuffs(final int objectId, final int groupId)
	{
		final ArrayList<L2Skill> skillIds = new ArrayList<L2Skill>();
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT skill_id, skill_level FROM community_buffs_group_skills WHERE group_id=? AND object_id=?");
			statement.setInt(1, groupId);
			statement.setInt(2, objectId);
			rs = statement.executeQuery();

			while (rs.next())
				skillIds.add(SkillTable.getInstance().getInfo(rs.getInt("skill_id"), rs.getInt("skill_level")));

		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "BuffBBSManager: getUsersBuffs error: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}

		return skillIds;
	}

	private static final void applyEffect(final L2Playable effected, final List<L2Skill> skills)
	{
		EffectTaskManager.getInstance().schedule(new BuffGroupTask(effected, skills), 250);
		effected.getPlayer().setCommunityBuffTime(600);
	}

	private final static class Buff implements Runnable
	{
		private final L2Skill _skill;
		private final L2Playable _target;

		public Buff(final L2Skill skill, final L2Playable target)
		{
			_skill = skill;
			_target = target;
		}

		@Override
		public void run()
		{
			_skill.getEffectsSelf(_target, Config.COMMUNITY_BOARD_BUFFER_ALT_TIME);
		}
	}

	@Override
	public void parsewrite(final String ar1, final String ar2, final String ar3, final String ar4, final String ar5, final L2Player player)
	{}
}
