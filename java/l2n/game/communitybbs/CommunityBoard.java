package l2n.game.communitybbs;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.communitybbs.Manager.*;
import l2n.game.handler.VoicedCommandHandler;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.network.L2GameClient;
import l2n.game.network.serverpackets.ShowBoard;
import l2n.util.Log;
import l2n.util.PremiumAccess;
import l2n.util.PremiumAccess.CommunityBoardType;

public class CommunityBoard
{
	public static void handleCommands(final L2GameClient client, final String command)
	{
		final L2Player activeChar = client.getActiveChar();
		if(activeChar == null)
			return;

			if(Config.ALLOW_COMMUNITYBOARD && activeChar.getPassCheck())
		{
			if(CommunityBoardHandlers.getInstance().useHandler(activeChar, command))
				return;

			if(command.startsWith("_bbsclan"))
				ClanBBSManager.getInstance().parsecmd(command, activeChar);
			else if(command.startsWith("_bbsmemo"))
								{
				if(Config.MEMO_PAGE_DISABLE)
				
					ShowBoard.disabled(activeChar);
									else
				TopicBBSManager.getInstance().parsecmd(command, activeChar);
								}
			else if(command.startsWith("_bbstopics"))
				TopicBBSManager.getInstance().parsecmd(command, activeChar);
			else if(command.startsWith("_bbsposts"))
				PostBBSManager.getInstance().parsecmd(command, activeChar);
			else if(command.startsWith("_bbstop"))
				TopBBSManager.getInstance().parsecmd(command, activeChar);
			else if(command.startsWith("_bbshome"))
				TopBBSManager.getInstance().parsecmd(command, activeChar);
			else if(command.startsWith("_bbsloc"))
			{
				if(Config.REGION_PAGE_DISABLE)
					ShowBoard.disabled(activeChar);
				else
					RegionBBSManager.getInstance().parsecmd(command, activeChar);
			}
			else if(command.startsWith("_friend") || command.startsWith("_block"))
				FriendsBBSManager.getInstance().parsecmd(command, activeChar);
			else if(command.startsWith("_bbscuston"))
				CustonManager.getInstance().parsecmd(command, activeChar);
			else if(command.startsWith("_bbsobtc"))
				BonusColorManager.getInstance().parsecmd(command, activeChar);
			else if(command.startsWith("_maillist"))
				MailBBSManager.getInstance().parsecmd(command, activeChar);
			else if(command.startsWith("_bbsms;"))
				msBBSManager.getInstance().parsecmd(command, activeChar);
			else if(command.startsWith("_bbsteleport;"))
			
			{
				if(!Config.COMMUNITY_BOARD_ALLOW_TELEPORT)
				{
					ShowBoard.disabled(activeChar);
					return;
				}

				if(PremiumAccess.checkAccessCommunity(activeChar, CommunityBoardType.TELEPORT))
					TeleportBBSManager.getInstance().parsecmd(command, activeChar);
			}
			else if(command.startsWith("_bbsechant"))
			{
				if(!Config.COMMUNITY_BOARD_ALLOW_ECHANT)
				{
					ShowBoard.disabled(activeChar);
					return;
				}

				if(PremiumAccess.checkAccessCommunity(activeChar, CommunityBoardType.ENCHANT))
					EnchantBBSManager.getInstance().parsecmd(command, activeChar);
			}
			else if(command.startsWith("_bbssms;"))
				SmsBBSManager.getInstance().parsecmd(command, activeChar);
			else if(command.startsWith("_bbsbuff;"))
			{
				if(!Config.COMMUNITY_BOARD_ALLOW_BUFFER)
				{
					ShowBoard.disabled(activeChar);
					return;
				}

				if(PremiumAccess.checkAccessCommunity(activeChar, CommunityBoardType.BUFFER))
					BuffBBSManager.getInstance().parsecmd(command, activeChar);
			}
			else if(command.startsWith("user_"))
			{
				final String com = command.substring(5).trim();
				final String word = com.split("\\s+")[0];
				final String args = com.substring(word.length()).trim();
				final IVoicedCommandHandler vch = VoicedCommandHandler.getInstance().getVoicedCommandHandler(word);
				if(vch != null)
					vch.useVoicedCommand(word, activeChar, args);
			}
			else
			{
				if(Config.COMMUNITYBOARDLOGGIN)
					Log.add("CommunityBoard|Main|AccountName=" + activeChar.getAccountName() + "|CharName=" + activeChar.getName() + "|IP=" + activeChar.getIP(), "CommunityBoard");
				ShowBoard.notImplementedYet(activeChar, command);
			}
		}
		else
			activeChar.sendPacket(Msg.THE_COMMUNITY_SERVER_IS_CURRENTLY_OFFLINE);
	}

	public static void handleWriteCommands(final L2GameClient client, final String url, final String arg1, final String arg2, final String arg3, final String arg4, final String arg5)
	{
		final L2Player activeChar = client.getActiveChar();
		if(activeChar == null)
			return;

		if(Config.ALLOW_COMMUNITYBOARD)
		{
			if(Config.COMMUNITYBOARDLOGGIN)
				Log.add("CommunityBoard|Main|AccountName=" + activeChar.getAccountName() + "|CharName=" + activeChar.getName() + "|IP=" + activeChar.getIP() + "|url=" + url, "CommunityBoard");
			if(url.equals("Topic"))
				TopicBBSManager.getInstance().parsewrite(arg1, arg2, arg3, arg4, arg5, activeChar);
			else if(url.equals("Post"))
				PostBBSManager.getInstance().parsewrite(arg1, arg2, arg3, arg4, arg5, activeChar);
			else if(url.equals("Region"))
				RegionBBSManager.getInstance().parsewrite(arg1, arg2, arg3, arg4, arg5, activeChar);
			else if(url.equals("Notice"))
				ClanBBSManager.getInstance().parsewrite(arg1, arg2, arg3, arg4, arg5, activeChar);
			else if(url.equals("Mail"))
				MailBBSManager.getInstance().parsewrite(arg1, arg2, arg3, arg4, arg5, activeChar);
			else
				ShowBoard.notImplementedYet(activeChar, url);
		}
		else
			activeChar.sendPacket(Msg.THE_COMMUNITY_SERVER_IS_CURRENTLY_OFFLINE);
	}
}
