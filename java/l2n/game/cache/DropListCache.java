package l2n.game.cache;

import gnu.trove.map.hash.TIntObjectHashMap;

public final class DropListCache
{
	private static final TIntObjectHashMap<String> _droplistCache = new TIntObjectHashMap<String>();

	public static void add(final int id, final String list)
	{
		_droplistCache.put(id, list);
	}

	public static String get(final int id)
	{
		return _droplistCache.get(id);
	}
}
