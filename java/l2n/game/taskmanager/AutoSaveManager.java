package l2n.game.taskmanager;

import l2n.Config;
import l2n.commons.lang.reference.IHardReference;
import l2n.commons.threading.SteppingRunnableQueueManager;
import l2n.game.L2GameThreadPools;
import l2n.game.model.actor.L2Player;
import l2n.util.Rnd;

import java.util.concurrent.Future;
import java.util.logging.Logger;

public class AutoSaveManager extends SteppingRunnableQueueManager
{
	private static final Logger _log = Logger.getLogger(AutoSaveManager.class.getName());

	public static final class AutoSaveTask implements Runnable
	{
		private final IHardReference<L2Player> _playerRef;

		private AutoSaveTask(final L2Player player)
		{
			_playerRef = player.getRef();
		}

		@Override
		public void run()
		{
			final L2Player player = _playerRef.get();
			if(player == null || !player.isConnected() || player.isLogoutStarted() || player.getNetConnection() == null)
				return;
			if(Config.AUTOSAVE)
				player.store(true);
		}
	}

	public AutoSaveManager()
	{
		super(10000L);
		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(this, 10000L, 10000L);
		
		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new Runnable()
		{
			@Override
			public void run()
			{
				AutoSaveManager.this.purge();
			}

		}, 60000L, 60000L);
	}

	public static AutoSaveManager getInstance()
	{
		return SingletonHolder._instance;
	}

	public final Future<AutoSaveTask> addPlayerTask(final L2Player p)
	{
		final long delay = Rnd.get(Config.AUTOSAVE_MIN_TIME, Config.AUTOSAVE_MIN_TIME * 2);
		return scheduleAtFixedRate(new AutoSaveTask(p), delay, delay);
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final AutoSaveManager _instance = new AutoSaveManager();
	}

	@Override
	protected String getName()
	{
		return "AutoSaveManager";
	}

	@Override
	protected Logger getLogger()
	{
		return _log;
	}
}
