package l2n.game.taskmanager;

import l2n.commons.lang.reference.IHardReference;
import l2n.commons.threading.SteppingRunnableQueueManager;
import l2n.game.L2GameThreadPools;
import l2n.game.model.actor.L2Character;

import java.util.concurrent.Future;
import java.util.logging.Logger;

public class DecayTaskManager extends SteppingRunnableQueueManager
{
	private static final Logger _log = Logger.getLogger(DecayTaskManager.class.getName());

	private static final DecayTaskManager _instance = new DecayTaskManager();

	public static final DecayTaskManager getInstance()
	{
		return _instance;
	}

	private DecayTaskManager()
	{
		super(500L);

		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(this, 500L, 500L);

		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new Runnable()
		{
			@Override
			public void run()
			{
				DecayTaskManager.this.purge();
			}

		}, 60000L, 60000L);
	}

	public Future<DecayTask> addDecayTask(final L2Character actor, final long delay)
	{
		return schedule(new DecayTask(actor), delay);
	}

	public static final class DecayTask implements Runnable
	{
		private final IHardReference<? extends L2Character> cha;

		private DecayTask(final L2Character actor)
		{
			cha = actor.getRef();
		}

		@Override
		public void run()
		{
			final L2Character actor = cha.get();
			if(actor != null)
				actor.onDecay();
		}
	}

	@Override
	protected String getName()
	{
		return "DecayTaskManager";
	}

	@Override
	protected Logger getLogger()
	{
		return _log;
	}
}
