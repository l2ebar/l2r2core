package l2n.game.skills.conditions;

import l2n.game.skills.Env;

public class ConditionMaxPlayerPercentCp extends Condition
{
	private final float _cp;

	public ConditionMaxPlayerPercentCp(final int cp)
	{
		_cp = cp / 100f;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		return env.character.getCurrentCp() >= _cp * env.character.getMaxCp();
	}
}
