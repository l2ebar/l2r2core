package l2n.game.skills.conditions;

import l2n.game.skills.Env;

public abstract class ConditionInventory extends Condition
{
	protected final short _slot;

	public ConditionInventory(final short slot)
	{
		_slot = slot;
	}

	@Override
	public abstract boolean testImpl(Env env);
}
