package l2n.game.skills.conditions;

import l2n.game.model.actor.L2Player;
import l2n.game.skills.Env;

public class ConditionPlayerState extends Condition
{
	public enum PlayerState
	{
		RESTING,
		MOVING,
		RUNNING,
		STANDING,
		FLYING,
		FLYING_TRANSFORM,
		BEHIND,
		FRONT
	}

	private final PlayerState _check;
	private final boolean _required;

	public ConditionPlayerState(final PlayerState check, final boolean required)
	{
		_check = check;
		_required = required;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		switch (_check)
		{
			case RESTING:
				if(env.character.isPlayer())
					return ((L2Player) env.character).isSitting() == _required;
				return !_required;
			case MOVING:
				return env.character.isMoving == _required;
			case RUNNING:
				return (env.character.isMoving && env.character.isRunning()) == _required;
			case STANDING:
				if(env.character.isPlayer())
					return ((L2Player) env.character).isSitting() != _required && env.character.isMoving != _required;
				return env.character.isMoving != _required;
			case FLYING:
				if(env.character.isPlayer())
					return env.character.isFlying() == _required;
				return !_required;
			case FLYING_TRANSFORM:
				if(env.character.isPlayer())
					return ((L2Player) env.character).isInFlyingTransform() == _required;
				return !_required;
			case BEHIND:
				return env.character.isBehindTarget() == _required;
			case FRONT:
				return env.character.isInFrontOfTarget() == _required;
		}
		return !_required;
	}
}
