package l2n.game.skills.skillclasses;

import l2n.commons.list.GArray;
import l2n.commons.util.StatsSet;
import l2n.game.model.L2Effect;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Formulas;
import l2n.game.skills.Stats;
import l2n.game.templates.L2Weapon;
import l2n.util.ArrayUtil;
import l2n.util.Rnd;

public class NegateStats extends L2Skill
{
	private Object[][] _negateStats = null; // <Stats, Integer>
	private final boolean _negateOffensive;
	private final int _negateCount;

	public NegateStats(final StatsSet set)
	{
		super(set);

		final String[] negateStats = set.getString("negateStats", "").split(";");
		for(int i = 0; i < negateStats.length; i++)
			if(!negateStats[i].isEmpty())
			{
				final String[] entry = negateStats[i].split(":");
				_negateStats = ArrayUtil.arrayAdd(_negateStats, new Object[] { Stats.valueOfXml(entry[0]), entry.length > 1 ? Integer.decode(entry[1]) : 100 }, Object[].class);
			}

		_negateOffensive = set.getBool("negateDebuffs");
		_negateCount = set.getInteger("negateCount", 0);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
		final L2Weapon weapon = activeChar.getActiveWeaponItem();

		for(final L2Character target : targets)
		{
			if(!_negateOffensive && !Formulas.calcSkillSuccess(activeChar, target, this))
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.S1_HAS_RESISTED_YOUR_S2).addString(target.getName()).addSkillName(getId(), getLevel()));
				continue;
			}

			int count = 0;
			final L2Effect[] effects = target.getEffectList().getAllEffects();
			for(final Object[] element : _negateStats)
				for(final L2Effect e : effects)
				{
					final L2Skill skill = e.getSkill();
					if(skill.isOffensive() == _negateOffensive && e.containsStat((Stats) element[0]) && skill.isCancelable() && Rnd.chance((Integer) element[1]))
					{
						target.sendPacket(new SystemMessage(SystemMessage.THE_EFFECT_OF_S1_HAS_BEEN_REMOVED).addSkillName(e.getSkill().getId(), e.getSkill().getLevel()));
						e.exit();
						count++;
					}
					if(_negateCount > 0 && count >= _negateCount)
						break;
				}

			getEffects(activeChar, target, getActivateRate() > 0, false);
			if(weapon != null)
				weapon.getEffect(false, activeChar, target, isOffensive());
		}
	}

	@Override
	public boolean isOffensive()
	{
		return !_negateOffensive;
	}

	public GArray<Stats> getNegateStats()
	{
		final GArray<Stats> list = new GArray<Stats>(_negateStats.length);
		for(final Object[] element : _negateStats)
			list.add((Stats) element[0]);
		return list;
	}
}
