package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2PetInstance;
import l2n.game.model.items.Inventory;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.util.Rnd;

public class SummonItem extends L2Skill
{
	private final short _itemId;
	private final int _minId;
	private final int _maxId;
	private final int _minCount;
	private final int _maxCount;

	public SummonItem(final StatsSet set)
	{
		super(set);

		_itemId = set.getShort("SummonItemId", (short) 0);
		_minId = set.getInteger("SummonMinId", 0);
		_maxId = set.getInteger("SummonMaxId", _minId);
		_minCount = set.getInteger("SummonMinCount");
		_maxCount = set.getInteger("SummonMaxCount", _minCount);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		for(final L2Character target : targets)
		{

			Inventory inventory;
			if(target.isPlayer())
				inventory = ((L2Player) target).getInventory();
			else if(target.isPet())
				inventory = ((L2PetInstance) target).getInventory();
			else
				continue;

			L2ItemInstance item;
			if(_minId > 0)
				item = ItemTable.getInstance().createItem(Rnd.get(_minId, _maxId), activeChar.getObjectId(), getId(), "SkillSummonItem");
			else
				item = ItemTable.getInstance().createItem(_itemId, activeChar.getObjectId(), getId(), "SkillSummonItem");

			final int count = Rnd.get(_minCount, _maxCount);
			item.setCount(count);
			activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_EARNED_S2_S1S).addItemName(item.getItemId()).addNumber(count));
			item = inventory.addItem(item);
			activeChar.sendChanges();
			getEffects(activeChar, target, getActivateRate() > 0, false);
		}
	}
}
