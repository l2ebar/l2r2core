package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.instances.L2SiegeHeadquarterInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.templates.L2Weapon;

public class HealCombatPointPercent extends L2Skill
{
	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(target instanceof L2DoorInstance || target instanceof L2SiegeHeadquarterInstance)
			return false;

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	public HealCombatPointPercent(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
		final L2Weapon weapon = activeChar.getActiveWeaponItem();

		for(final L2Character target : targets)
		{

			if(target.isDead())
				continue;

			final double cp = target.getMaxCp() * _power / 100;

			getEffects(activeChar, target, getActivateRate() > 0, false);

			target.setCurrentCp(cp + target.getCurrentCp());

			if(target.isPlayer() && activeChar == target)
				target.sendPacket(new SystemMessage(SystemMessage.S1_CPS_WILL_BE_RESTORED).addNumber((int) cp));
			if(target.isPlayer() && activeChar != target)
				target.sendPacket(new SystemMessage(SystemMessage.S1_WILL_RESTORE_S2S_CP).addString(activeChar.getName()).addNumber((int) cp));

			if(weapon != null)
				weapon.getEffect(false, activeChar, target, isOffensive());
		}
	}
}
