package l2n.game.skills.skillclasses;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.quest.Quest;
import l2n.game.network.serverpackets.StatusUpdate;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Rnd;

import java.util.StringTokenizer;

/**
* fix by Rasiel
 */
public class ExtractStone extends L2Skill
{
	private static final int ExtractScrollSkill = 2630;

	private static final int ExtractedCoarseRedStarStone = 13858;
	private static final int ExtractedCoarseBlueStarStone = 13859;
	private static final int ExtractedCoarseGreenStarStone = 13860;

	private static final int ExtractedRedStarStone = 14009;
	private static final int ExtractedBlueStarStone = 14010;
	private static final int ExtractedGreenStarStone = 14011;

	private static final int FireEnergyCompressionStone = 14015; // 14015
	private static final int WaterEnergyCompressionStone = 14016; // 14016
	private static final int WindEnergyCompressionStone = 14017; // 14017
	private static final int EarthEnergyCompressionStone = 14018; // 14018
	private static final int DarknessEnergyCompressionStone = 14019; // 14019
	private static final int SacredEnergyCompressionStone = 14020; // 14020

	private static final int SeedFire = 18679;
	private static final int SeedWater = 18678;
	private static final int SeedWind = 18680;
	private static final int SeedEarth = 18681;
	private static final int SeedDivinity = 18682;
	private static final int SeedDarkness = 18683;

	private static final int RedStarStone1 = 18684;
	private static final int RedStarStone2 = 18685;
	private static final int RedStarStone3 = 18686;
	private static final int BlueStarStone1 = 18687;
	private static final int BlueStarStone2 = 18688;
	private static final int BlueStarStone3 = 18689;
	private static final int GreenStarStone1 = 18690;
	private static final int GreenStarStone2 = 18691;
	private static final int GreenStarStone3 = 18692;

	private final GArray<Integer> _npcIds = new GArray<Integer>();

	public ExtractStone(final StatsSet set)
	{
		super(set);
		final StringTokenizer st = new StringTokenizer(set.getString("npcIds", ""), ";");
		while (st.hasMoreTokens())
			_npcIds.add(Integer.valueOf(st.nextToken()));
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(target == null || !target.isNpc() || getItemId(target.getNpcId()) == 0)
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return false;
		}

		if(!_npcIds.isEmpty() && !_npcIds.contains(new Integer(target.getNpcId())))
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return false;
		}

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	private int getItemId(final int npcId)
	{
		switch (npcId)
		{
			case RedStarStone1:
			case RedStarStone2:
			case RedStarStone3:
				if(_id == ExtractScrollSkill)
					return ExtractedCoarseRedStarStone;
				return ExtractedRedStarStone;
			case BlueStarStone1:
			case BlueStarStone2:
			case BlueStarStone3:
				if(_id == ExtractScrollSkill)
					return ExtractedCoarseBlueStarStone;
				return ExtractedBlueStarStone;
			case GreenStarStone1:
			case GreenStarStone2:
			case GreenStarStone3:
				if(_id == ExtractScrollSkill)
					return ExtractedCoarseGreenStarStone;
				return ExtractedGreenStarStone;
			case SeedWater:
				return WaterEnergyCompressionStone;
			case SeedFire:
				return FireEnergyCompressionStone;
			case SeedWind:
				return WindEnergyCompressionStone;
			case SeedEarth:
				return EarthEnergyCompressionStone;
			case SeedDarkness:
				return DarknessEnergyCompressionStone;
			case SeedDivinity:
				return SacredEnergyCompressionStone;
		}
		return 0;
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final L2Player player = activeChar.getPlayer();
		if(player == null)
			return;

		for(final L2Character target : targets)
			if(target != null && getItemId(target.getNpcId()) != 0)
			{
				final double rate = Config.RATE_DROP_ITEMS * player.getBonus().RATE_DROP_ITEMS;
				final int itemId = getItemId(target.getNpcId());

                int maxRandom;

                switch (itemId)
                {
                    case FireEnergyCompressionStone: //14015
                        maxRandom = Config.INSTANCE_FIRE_ENERGY_COMPRESSION_STONE;
                        break;
                    case WaterEnergyCompressionStone: //14016
                        maxRandom = Config.INSTANCE_WATER_ENERGY_COMPRESSION_STONE;
                        break;
                    case WindEnergyCompressionStone: //14017
                        maxRandom = Config.INSTANCE_WIND_ENERGY_COMPRESSION_STONE;
                        break;
                    case EarthEnergyCompressionStone: //14018
                        maxRandom = Config.INSTANCE_EARTH_ENERGY_COMPRESSION_STONE;
                        break;
                    case DarknessEnergyCompressionStone: //14019
                        maxRandom = Config.INSTANCE_DARKNESS_ENERGY_COMPRESSION_STONE;
                        break;
                    case SacredEnergyCompressionStone: //14020
                        maxRandom = Config.INSTANCE_SACRED_ENERGY_COMPRESSION_STONE;
                        break;
                    default:
                        maxRandom = 10;
                }
                final long count = _id == ExtractScrollSkill ? 1 : Math.min(maxRandom, Rnd.get((int) (getLevel() * rate + 1)));

				if(count > 0)
				{
					player.getInventory().addItem(itemId, count, 0, null);
					player.sendPacket(Quest.SOUND_ITEMGET);
					player.sendPacket(SystemMessage.obtainItems(itemId, count, 0));
					player.sendStatusUpdate(false, StatusUpdate.CUR_LOAD);
				}
				else
					player.sendPacket(Msg.THE_COLLECTION_HAS_FAILED);
				target.doDie(player);
			}
		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}
}
