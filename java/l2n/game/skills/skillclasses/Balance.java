package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.skills.Stats;

public class Balance extends L2Skill
{
	public Balance(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		double summaryCurrentHp = 0;
		int summaryMaximumHp = 0;

		for(final L2Character target : targets)
		{
			if(target.isDead())
				continue;
			summaryCurrentHp += target.getCurrentHp();
			summaryMaximumHp += target.getMaxHp();
		}

		final double percent = summaryCurrentHp / summaryMaximumHp;

		for(final L2Character target : targets)
			if(!target.isDead())
			{
				final double newHp = Math.max(0, Math.min(target.getMaxHp() * percent, target.calcStat(Stats.HP_LIMIT, 100, null, null) * target.getMaxHp() / 100.));
				target.setCurrentHp(newHp, false);
				getEffects(activeChar, target, getActivateRate() > 0, false);
			}

		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}
}
