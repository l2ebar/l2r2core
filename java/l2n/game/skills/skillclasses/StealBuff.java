package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.model.L2Effect;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.EffectType;
import l2n.game.skills.Env;
import l2n.game.skills.Formulas;
import l2n.game.skills.effects.EffectTemplate;
import l2n.game.templates.L2Weapon;

public class StealBuff extends L2Skill
{
	private final int _stealCount;

	public StealBuff(final StatsSet set)
	{
		super(set);
		_stealCount = set.getInteger("stealCount", 1);
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(target == null || !target.isPlayer())
		{
			activeChar.sendPacket(Msg.TARGET_IS_INCORRECT);
			return false;
		}

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final L2Weapon weapon = activeChar.getActiveWeaponItem();
		for(L2Character target : targets)
			if(target != null)
			{
				if(getActivateRate() > 0 && !Formulas.calcSkillSuccess(activeChar, target, this, getActivateRate()))
				{
					activeChar.sendPacket(new SystemMessage(SystemMessage.S1_HAS_RESISTED_YOUR_S2).addString(target.getName()).addSkillName(getId(), getLevel()));
					continue;
				}

				if(!target.isPlayer())
					continue;

				if(target.checkReflectSkill(activeChar, this))
					target = activeChar;

				final L2Effect[] a = target.getEffectList().getAllEffects();
				if(a.length == 0)
					continue;

				int counter = 0;
				int maxCount = _stealCount;
				boolean update = false;

				if(activeChar.getSkillMastery(getId()) == 3 && getId() == L2Skill.SKILL_STEAL_DIVINITY)
				{
					activeChar.removeSkillMastery(getId());
					maxCount *= 2;
				}

				// Сначало крадем песни/танцы
				for(int i = 0; counter < maxCount && i < a.length; i++)
				{
					final L2Effect e = a[a.length - i - 1];
					if(e != null && e.getSkill().isDanceSong() && e.getSkill().isCancelable() && !e.getSkill().isToggle() && !e.getSkill().isPassive() && !e.getSkill().isOffensive() && e.getEffectType() != EffectType.Vitality && !e._template._applyOnCaster)
					{
						final L2Effect stealedEffect = cloneEffect(activeChar, e);
						e.exit();
						if(stealedEffect != null)
						{
							activeChar.getEffectList().addEffect(stealedEffect);
							update = true;
						}
						counter++;
					}
				}

				// Потом остальное
				for(int i = 0; counter < maxCount && i < a.length; i++)
				{
					final L2Effect e = a[a.length - i - 1];
					if(e != null && !e.getSkill().isDanceSong() && e.getSkill().isCancelable() && !e.getSkill().isToggle() && !e.getSkill().isPassive() && !e.getSkill().isOffensive() && e.getEffectType() != EffectType.Vitality && !e._template._applyOnCaster)
					{
						final L2Effect stealedEffect = cloneEffect(activeChar, e);
						e.exit();
						if(stealedEffect != null)
						{
							activeChar.getEffectList().addEffect(stealedEffect);
							update = true;
						}
						counter++;
					}
				}

				if(update)
				{
					activeChar.sendMessage(new CustomMessage("l2n.game.skills.skillclasses.StealBuff.Success", activeChar).addNumber(counter));
					activeChar.sendChanges();
					activeChar.updateEffectIcons();
				}

				if(weapon != null)
					weapon.getEffect(false, activeChar, target, isOffensive());

				getEffects(activeChar, target, getActivateRate() > 0, false);
			}

		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}

	private L2Effect cloneEffect(final L2Character cha, final L2Effect eff)
	{
		final L2Skill skill = eff.getSkill();

		for(final EffectTemplate et : skill.getEffectTemplates())
		{
			final L2Effect effect = et.getEffect(new Env(cha, cha, skill));
			if(effect != null)
			{
				effect.setCount(eff.getCount());
				if(eff.getCount() == 1)
					effect.setPeriod(eff.getPeriod() - eff.getTime());
				else
					effect.setPeriod(eff.getPeriod());
				return effect;
			}
		}
		return null;
	}
}
