package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.skills.Formulas;

public class CPDam extends L2Skill
{
	public CPDam(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void  useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final boolean ss = activeChar.getChargedSoulShot() && isSSPossible();
		if(ss)
			activeChar.unChargeShots(false);

		for(L2Character target : targets)
		{
			if(target.isDead())
				continue;

			Formulas.doCounterAttack(this, activeChar, target, false);

			if(target.checkReflectSkill(activeChar, this))
				target = activeChar;

			if(target.getCurrentCp() < 1)
				continue;

			double damage = _power * target.getCurrentCp();

			if(damage < 1)
				damage = 1;

			target.reduceCurrentHp(damage, activeChar, this, true, true, false, true, false);

			getEffects(activeChar, target, getActivateRate() > 0, false);
		}
	}
}
