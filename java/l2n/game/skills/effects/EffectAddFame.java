package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;
public class EffectAddFame extends L2Effect
{
    public EffectAddFame(final Env env, final EffectTemplate template)
    {
        super(env, template);
    }

    @Override
    public void onStart()
    {
        if(getEffected().isPlayer())
            getEffected().getPlayer().addFame((int)calc());
    }

    @Override
    public void onExit()
    {
        //
    }

    @Override
    public boolean onActionTime()
    {
        return false;
    }
}