package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Env;
import l2n.game.skills.Stats;
import l2n.util.Rnd;
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @email l2-scripts@mail.ru   ICQ 9-013-612
 * @date 18.11.2011
 * @time 11:43:37
 */
public class EffectDispelEffects extends L2Effect
{
	private final String _dispelType;
	private final int _cancelRate;
	private final String[] _stackTypes;
	private final int _negateCount;

	private final int minChance;
	private final int maxChance;

	/*
	 * cancelRate is skill dependant constant:
	 * Cancel - 25
	 * Touch of Death/Insane Crusher - 25
	 * Mage/Warrior Bane - 80
	 * Mass Mage/Warrior Bane - 40
	 * Infinity Spear - 10
	 */

	public EffectDispelEffects(final Env env, final EffectTemplate template)
	{
		super(env, template);
		_dispelType = template.getParam().getString("dispelType", "");
		_cancelRate = template.getParam().getInteger("cancelRate", 0);
		_negateCount = template.getParam().getInteger("negateCount", 5);
		_stackTypes = template.getParam().getString("negateStackTypes", "").split(";");

		minChance = template.getParam().getInteger("minChance", _dispelType.equals("bane") ? 40 : 25);
		maxChance = template.getParam().getInteger("maxChance", _dispelType.equals("bane") ? 90 : 75);
	}

	@Override
	public void onStart()
	{
		final List<L2Effect> _musicList = new ArrayList<L2Effect>();
		final List<L2Effect> _buffList = new ArrayList<L2Effect>();

		// H5 - triggerable skills go first

		// Getting effect lists
		for(final L2Effect e : _effected.getEffectList().getAllEffects())
		{
			if(e == null)
				continue;

			if(_dispelType.equals("cancellation"))
			{
				if(!e.isOffensive() && !e.getSkill().isToggle() && e.getSkill().isCancelable())
					if(e.getSkill().isDanceSong())
						_musicList.add(e);
					else
						_buffList.add(e);
			}
			else if(_dispelType.equals("bane"))
			{
				if(!e.isOffensive() && (ArrayUtils.contains(_stackTypes, e.getStackType()) || ArrayUtils.contains(_stackTypes, e.getStackType2())) && e.getSkill().isCancelable())
					_buffList.add(e);
			}
			else if(_dispelType.equals("cleanse"))
				if(e.isOffensive() && e.getSkill().isCancelable())
					_buffList.add(e);
		}

		// Reversing lists and adding to a new list
		final List<L2Effect> _effectList = new ArrayList<L2Effect>();
		Collections.reverse(_musicList);
		Collections.reverse(_buffList);
		_effectList.addAll(_musicList);
		_effectList.addAll(_buffList);

		if(_effectList.isEmpty())
			return;

		double prelimChance, eml, dml, cancel_res_multiplier = _effected.calcStat(Stats.CANCEL_RECEPTIVE, 0, null, null); // constant resistance is applied for whole cycle of cancellation
		int buffTime, negated = 0;

		for(final L2Effect e : _effectList)
			if(negated < _negateCount)
			{
				eml = e.getSkill().getMagicLevel();
				dml = getSkill().getMagicLevel() - (eml == 0 ? _effected.getLevel() : eml); // FIXME: no effect can have mLevel == 0. Tofix in skilldata
				buffTime = (int) (e.getTimeLeft() / 1000);
				cancel_res_multiplier = 1 - cancel_res_multiplier * .01;
				prelimChance = (2. * dml + _cancelRate + buffTime / 120) * cancel_res_multiplier; // retail formula

				if(Rnd.chance(calcSkillChanceLimits(prelimChance, _effector.isPlayable())))
				{
					negated++;
					_effected.sendPacket(new SystemMessage(SystemMessage.THE_EFFECT_OF_S1_HAS_BEEN_REMOVED).addSkillName(e.getSkill().getId(), e.getSkill().getLevel()));
					e.exit();
				}
			}
	}

	private double calcSkillChanceLimits(final double prelimChance, final boolean isPlayable)
	{
		if(_dispelType.equals("bane") || _dispelType.equals("cancellation"))
		{
			if(prelimChance < minChance)
				return minChance;
			else if(prelimChance > maxChance)
				return maxChance;
		}
		else if(_dispelType.equals("cleanse"))
			return _cancelRate;
		return prelimChance;
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
