package l2n.game.skills.effects;

import l2n.Config;
import l2n.game.ai.CtrlIntention;
import l2n.game.model.L2Effect;
import l2n.game.model.L2World;
import l2n.game.model.L2WorldRegion;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.skills.Env;

public final class EffectInvisible extends L2Effect
{
	public EffectInvisible(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean checkCondition()
	{
		if(!_effected.isPlayer())
			return false;
		final L2Player player = _effected.getPlayer();
		if(player.isInvisible())
			return false;
		if(player.isCombatFlagEquipped() || player.isTerritoryFlagEquipped())
			return false;
		return super.checkCondition();
	}

	@Override
	public void onStart()
	{
		super.onStart();

		final L2Player player = _effected.getPlayer();

		player.setInvisible(true);

		if(player.getCurrentRegion() != null)
			for(final L2WorldRegion neighbor : player.getCurrentRegion().getNeighbors())
				if(!neighbor.areNeighborsEmpty())
					neighbor.removePlayerFromOtherPlayers(player);

		// Отменяем атаку у нпс вокруг
		for(final L2NpcInstance npc : L2World.getAroundNpc(player, Config.MAX_PURSUE_RANGE, 200))
			if(npc != null)
				if(npc.getAI().getAttackTarget() == player)
				{
					npc.setTarget(null);
					npc.abortAttack();
					npc.abortCast();
					npc.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);
				}
	}

	@Override
	public void onExit()
	{
		super.onExit();

		if(!_effected.isPlayer())
			return;

		final L2Player player = (L2Player) _effected;
		if(!player.isInvisible())
			return;

		player.setInvisible(false);
		player.broadcastUserInfo(true);
		if(player.getPet() != null)
			player.getPet().broadcastCharInfo();
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
