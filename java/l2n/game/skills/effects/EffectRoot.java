package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

public final class EffectRoot extends L2Effect
{
	public EffectRoot(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean checkCondition()
	{
		if(_effected.isRootImmune())
			return false;
		return super.checkCondition();
	}

	/** Notify started */
	@Override
	public void onStart()
	{
		super.onStart();
		_effected.startRooted();
	}

	/** Notify exited */
	@Override
	public void onExit()
	{
		super.onExit();
		_effected.stopRooting();
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
