package l2n.game.skills;

import java.util.NoSuchElementException;

public enum Stats
{
	MAX_HP("maxHp"),
	MAX_MP("maxMp"),
	MAX_CP("maxCp"),
	REGENERATE_HP_RATE("regHp"),
	REGENERATE_CP_RATE("regCp"),
	REGENERATE_MP_RATE("regMp"),

	RUN_SPEED("runSpd"),

	POWER_DEFENCE("pDef"),
	MAGIC_DEFENCE("mDef"),
	POWER_ATTACK("pAtk"),
	MAGIC_ATTACK("mAtk"),
	POWER_ATTACK_SPEED("pAtkSpd"),
	MAGIC_ATTACK_SPEED("mAtkSpd"),

	MAGIC_REUSE_RATE("mReuse"), // how fast spells becomes ready to reuse
	PHYSIC_REUSE_RATE("pReuse"),
	ATK_REUSE("atkReuse"),
	ATK_BASE("atkBaseSpeed"),

	// Atk & Def rates
	/** увеличивает урон от крита в процентах */
	CRITICAL_DAMAGE("cAtk"),
	CRITICAL_DAMAGE_STATIC("cAtkStatic"), // this is another type for special critical damage mods - vicious stance, crit power and crit damage SA
	CRITICAL_BASE("baseCrit"),
	CRITICAL_RATE("rCrit"),

	/**
	 * резист к дамагу от крита
	 * для увелечения резиста надо либо делать sub, либо mul (mul val 0.85 - увеличивает резист на 15%)
	 */
	CRIT_DAMAGE_RECEPTIVE("critDamRcpt"),
	/** резист получить крит, указывается в процентах */
	CRIT_CHANCE_RECEPTIVE("critChanceRcpt"), // резист получить крит

	EVASION_RATE("rEvas"),
	ACCURACY_COMBAT("accCombat"),
	MCRITICAL_RATE("mCritRate"),
	MCRITICAL_DAMAGE("mCritDamage"),
	BLOW_RATE("blowRate"), // blow land rate

	PHYSICAL_DAMAGE("physDamage"),
	MAGIC_DAMAGE("magicDamage"),

	CAST_INTERRUPT("concentration"),

	SHIELD_DEFENCE("sDef"),
	SHIELD_RATE("rShld"),
	SHIELD_ANGLE("shldAngle"),

	POWER_ATTACK_RANGE("pAtkRange"),
	MAGIC_ATTACK_RANGE("mAtkRange"),
	POLE_ATTACK_ANGLE("poleAngle"),
	POLE_TARGERT_COUNT("poleTargetCount"), // atkCountMax - SF

	STAT_STR("STR"),
	STAT_CON("CON"),
	STAT_DEX("DEX"),
	STAT_INT("INT"),
	STAT_WIT("WIT"),
	STAT_MEN("MEN"),

	BREATH("breath"),
	FALL("fall"),
	EXP_LOST("expLost"),

	// resistance to * attacks
	BLEED_RECEPTIVE("bleedRcpt"),
	POISON_RECEPTIVE("poisonRcpt"),
	STUN_RECEPTIVE("stunRcpt"),
	ROOT_RECEPTIVE("rootRcpt"),
	MENTAL_RECEPTIVE("mentalRcpt"), // trait_derangement
	SLEEP_RECEPTIVE("sleepRcpt"),
	PARALYZE_RECEPTIVE("paralyzeRcpt"),
	CANCEL_RECEPTIVE("cancelRcpt"),
	DEBUFF_RECEPTIVE("debuffRcpt"),
	MAGIC_RECEPTIVE("magicRcpt"),
	DEATH_RECEPTIVE("deathRcpt"),

	// * attack bonus
	BLEED_POWER("bleedPower"),
	POISON_POWER("poisonPower"),
	STUN_POWER("stunPower"),
	ROOT_POWER("rootPower"),
	MENTAL_POWER("mentalPower"),
	SLEEP_POWER("sleepPower"),
	PARALYZE_POWER("paralyzePower"),
	CANCEL_POWER("cancelPower"),
	DEBUFF_POWER("debuffPower"),
	MAGIC_POWER("magicPower"),

	// бонусы к элементальным
	ATTACK_ELEMENT_FIRE("attackFire"),
	ATTACK_ELEMENT_WATER("attackWater"),
	ATTACK_ELEMENT_WIND("attackWind"),
	ATTACK_ELEMENT_EARTH("attackEarth"),
	ATTACK_ELEMENT_SACRED("attackSacred"), // Divine attack (holy)
	ATTACK_ELEMENT_UNHOLY("attackUnholy"), // Dark Attack (dark)

	// сопротивляемости к эементальным атакам
	FIRE_RECEPTIVE("fireRcpt"),
	WIND_RECEPTIVE("windRcpt"),
	WATER_RECEPTIVE("waterRcpt"),
	EARTH_RECEPTIVE("earthRcpt"),
	SACRED_RECEPTIVE("sacredRcpt"),
	UNHOLY_RECEPTIVE("unholyRcpt"),

	// сопротивляемости к различным типам оружия
	// Надо уменьшать для увеличения резиста sub
	NONE_WPN_RECEPTIVE("noneWpnRcpt"), // Shields!!!
	SWORD_WPN_RECEPTIVE("swordWpnRcpt"),
	DUAL_WPN_RECEPTIVE("dualWpnRcpt"),
	BLUNT_WPN_RECEPTIVE("bluntWpnRcpt"),
	DAGGER_WPN_RECEPTIVE("daggerWpnRcpt"),
	BOW_WPN_RECEPTIVE("bowWpnRcpt"),
	CROSSBOW_WPN_RECEPTIVE("crossbowWpnRcpt"),
	POLE_WPN_RECEPTIVE("poleWpnRcpt"),
	FIST_WPN_RECEPTIVE("fistWpnRcpt"),
	RAPIER_WPN_RECEPTIVE("rapierWpnRcpt"),
	ANCIENT_WPN_RECEPTIVE("ancientWpnRcpt"),

	// Reflect Damage
	// Отражение урона с шансом. Урон получает только атакующий.
	REFLECT_AND_BLOCK_DAMAGE_CHANCE("reflectAndBlockDam"), // Ближний урон без скиллов
	REFLECT_AND_BLOCK_PSKILL_DAMAGE_CHANCE("reflectAndBlockPSkillDam"), // Ближний урон скиллами
	REFLECT_AND_BLOCK_MSKILL_DAMAGE_CHANCE("reflectAndBlockMSkillDam"), // Любой урон магией

	// Отражение урона в процентах. Урон получает и атакующий и цель
	REFLECT_DAMAGE_PERCENT("reflectDam"), // Ближний урон без скиллов
	REFLECT_PSKILL_DAMAGE_PERCENT("reflectPSkillDam"), // Ближний урон скиллами
	REFLECT_MSKILL_DAMAGE_PERCENT("reflectMSkillDam"), // Любой урон магией

	REFLECT_PHYSIC_SKILL("reflectPhysicSkill"),
	REFLECT_MAGIC_SKILL("reflectMagicSkill"),

	REFLECT_PHYSIC_DEBUFF("reflectPhysicDebuff"),
	REFLECT_MAGIC_DEBUFF("reflectMagicDebuff"),

	// Absorb HP
	ABSORB_DAMAGE_PERCENT("absorbDam"),
	// Absorb MP
	ABSORB_DAMAGEMP_PERCENT("absorbDamMp"),

	TRANSFER_PET_DAMAGE_PERCENT("transferPetDam"),
	EVADE_MAGIC_SKILL("evadeMagicSkill"),
	EVADE_PHYSIC_SKILL("evadePhysicSkill"),
	// Counter Attack
	COUNTER_ATTACK("counterAttack"),

	SKILL_POWER("skillPower"),

	// PvP stats
	PVP_PHYSICAL_DMG("pvpPhysDmg"),
	PVP_MAGICAL_DMG("pvpMagicalDmg"),
	PVP_PHYS_SKILL_DMG("pvpPhysSkillsDmg"),
	PVP_PHYSICAL_DEF("pvpPhysDef"),
	PVP_MAGICAL_DEF("pvpMagicalDef"),
	PVP_PHYS_SKILL_DEF("pvpPhysSkillsDef"),

	CANCEL_TARGET("cancelTarget"),

	CPHEAL_EFFECTIVNESS("cpEff"),
	HEAL_EFFECTIVNESS("hpEff"),
	MANAHEAL_EFFECTIVNESS("mpEff"),

	// Для эффектов типа Seal of Limit
	HP_LIMIT("hpLimit"),
	MP_LIMIT("mpLimit"),
	CP_LIMIT("cpLimit"),

	HEAL_POWER("healPower"),
	MP_MAGIC_SKILL_CONSUME("mpConsum"),
	MP_PHYSICAL_SKILL_CONSUME("mpConsumePhysical"),
	MP_DANCE_SKILL_CONSUME("mpDanceConsume"),

	MP_USE_BOW("cheapShot"),
	MP_USE_BOW_CHANCE("cheapShotChance"),
	SS_USE_BOW("miser"),
	SS_USE_BOW_CHANCE("soulShotChance"),

	ACTIVATE_RATE("activateRate"),
	SKILL_MASTERY("skillMastery"),

	MAX_LOAD("maxLoad"),
	MAX_NO_PENALTY_LOAD("maxNoPenaltyLoad"),
	INVENTORY_LIMIT("inventoryLimit"),
	STORAGE_LIMIT("storageLimit"),
	TRADE_LIMIT("tradeLimit"),
	COMMON_RECIPE_LIMIT("CommonRecipeLimit"),
	DWARVEN_RECIPE_LIMIT("DwarvenRecipeLimit"),
	BUFF_LIMIT("buffLimit"),
	DANCE_SONG_LIMIT("DanceSongLimit"),
	SOULS_LIMIT("soulsLimit"),
	SOULS_CONSUME_EXP("soulsExp"),
	TALISMANS_LIMIT("talismansLimit"),

	GRADE_EXPERTISE_LEVEL("expertiseLevel"),
	EXP("ExpMultiplier"),
	SP("SpMultiplier"),
	DROP("DropMultiplier"),

	INVULNERABLE("Invulnerable"), // FIXME для бессмертия, убрать потом
	CLOAK_SLOT("cloak"),

	// vitality
	VITALITY_CONSUME_RATE("vitalityConsumeRate");

	public static final int NUM_STATS = values().length;

	private String _value;

	public String getValue()
	{
		return _value;
	}

	private Stats(String s)
	{
		_value = s;
	}

	public static Stats valueOfXml(String name)
	{
		for(Stats s : values())
			if(s.getValue().equals(name))
				return s;

		throw new NoSuchElementException("Unknown name '" + name + "' for enum BaseStats");
	}

	@Override
	public String toString()
	{
		return _value;
	}
}
