package l2n.game.loginservercon.receive;

import l2n.game.loginservercon.AttLS;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

public class IpAction extends LoginServerBasePacket
{
	public IpAction(byte[] decrypt, AttLS loginServer)
	{
		super(decrypt, loginServer);
	}

	@Override
	public void read()
	{
		String ip = readS();
		boolean isBan = readC() == 1;

		String gm = null;
		if(isBan)
			gm = readS();

		String message = isBan ? "IP: " + ip + " has been banned by " + gm : "IP: " + ip + " has been unbanned";
		for(L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
			if(player.isGM())
				player.sendMessage(message);
	}
}
