package l2n.game.loginservercon.receive;

import l2n.game.loginservercon.AttLS;
import l2n.game.loginservercon.LSConnection;

import java.util.logging.Logger;

public class RSAKey extends LoginServerBasePacket
{
	private static final Logger log = Logger.getLogger(RSAKey.class.getName());

	public RSAKey(byte[] decrypt, AttLS loginServer)
	{
		super(decrypt, loginServer);
	}

	@Override
	public void read()
	{
		getLoginServer().initRSA(readB(256));
		getLoginServer().initCrypt();

		if(LSConnection.DEBUG_GS_LS)
			log.info("GS Debug: RSAKey packet readed.");
	}
}
