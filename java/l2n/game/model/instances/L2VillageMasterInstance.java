package l2n.game.model.instances;

import javolution.text.TextBuilder;
import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.CastleSiegeManager;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.*;
import l2n.game.model.L2Clan.SubPledge;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.*;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.entity.siege.SiegeDatabase;
import l2n.game.network.serverpackets.*;
import l2n.game.tables.CharTemplateTable;
import l2n.game.tables.ClanTable;
import l2n.game.tables.SkillTable;
import l2n.game.tables.SkillTreeTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.StringUtil;

import java.util.HashMap;
import java.util.Set;
import java.util.logging.Logger;

public final class L2VillageMasterInstance extends L2NpcInstance
{
	private static final Logger _log = Logger.getLogger(L2VillageMasterInstance.class.getName());

	private final static int STAR_OF_DESTINY = 5011;
	private final static int MIMIRS_ELIXIR = 6319;

	/**
	 * @param template
	 */
	public L2VillageMasterInstance(final int objectId, final L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(final L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.startsWith("create_clan") && command.length() > 12)
		{
			final String val = command.substring(12);
			createClan(player, val);
		}
		else if(command.startsWith("create_academy") && command.length() > 15)
		{
			final String sub = command.substring(15, command.length());
			createSubPledge(player, sub, L2Clan.SUBUNIT_ACADEMY, 5, "");
		}
		else if(command.startsWith("create_royal") && command.length() > 15)
		{
			final String[] sub = command.substring(13, command.length()).split(" ", 2);
			if(sub.length == 2)
				createSubPledge(player, sub[1], L2Clan.SUBUNIT_ROYAL1, 6, sub[0]);
		}
		else if(command.startsWith("create_knight") && command.length() > 16)
		{
			final String[] sub = command.substring(14, command.length()).split(" ", 2);
			if(sub.length == 2)
				createSubPledge(player, sub[1], L2Clan.SUBUNIT_KNIGHT1, 7, sub[0]);
		}
		else if(command.startsWith("assign_subpl_leader") && command.length() > 22)
		{
			final String[] sub = command.substring(20, command.length()).split(" ", 2);
			if(sub.length == 2)
				assignSubPledgeLeader(player, sub[1], sub[0]);
		}
		else if(command.startsWith("assign_new_clan_leader") && command.length() > 23)
		{
			final String val = command.substring(23);
			setLeader(player, val);
		}
		if(command.startsWith("create_ally") && command.length() > 12)
		{
			final String val = command.substring(12);
			createAlly(player, val);
		}
		else if(command.startsWith("dissolve_ally"))
			dissolveAlly(player);
		else if(command.startsWith("dissolve_clan"))
			dissolveClan(player);
		else if(command.startsWith("increase_clan_level"))
			levelUpClan(player);
		else if(command.startsWith("learn_clan_skills"))
			showClanSkillWindow(player);
		else if(command.startsWith("ShowCouponExchange"))
		{
			if(Functions.getItemCount(player, 8869) > 0 || Functions.getItemCount(player, 8870) > 0)
				command = "Multisell 800";
			else
				command = "Link villagemaster/reflect_weapon_master_noticket.htm";
			super.onBypassFeedback(player, command);
		}
		else if(command.startsWith("Subclass"))
		{
			if(player.getPet() != null)
			{
				player.sendPacket(Msg.A_SUB_CLASS_MAY_NOT_BE_CREATED_OR_CHANGED_WHILE_A_SERVITOR_OR_PET_IS_SUMMONED);
				return;
			}

			// Саб класс нельзя получить или поменять, пока используется скилл или персонаж находится в режиме трансформации
			if(player.isActionsDisabled() || player.getTransformationId() != 0 || player.isCastingNow())
			{
				player.sendPacket(Msg.SUB_CLASSES_MAY_NOT_BE_CREATED_OR_CHANGED_WHILE_A_SKILL_IS_IN_USE);
				return;
			}

			final StringBuilder content = new StringBuilder("<html><body>");
			final NpcHtmlMessage html = new NpcHtmlMessage(_objectId);

			final HashMap<Integer, L2SubClass> playerClassList = player.getSubClasses();
			Set<PlayerClass> subsAvailable;

			int cmdChoice = 0;
			int classId = 0;
			int newClassId = 0;
			try
			{
				cmdChoice = Integer.parseInt(command.substring(9, 10).trim());

				int endIndex = command.indexOf(' ', 11);
				if(endIndex == -1)
					endIndex = command.length();

				classId = Integer.parseInt(command.substring(11, endIndex).trim());
				if(command.length() > endIndex)
					newClassId = Integer.parseInt(command.substring(endIndex).trim());
			}
			catch(final Exception NumberFormatException)
			{}

			switch (cmdChoice)
			{
				case 1: // Возвращает список сабов, которые можно взять (см case 4)
					subsAvailable = getAvailableSubClasses(player);
					if(subsAvailable != null && !subsAvailable.isEmpty())
					{
						content.append("Add Subclass:<br>Which subclass do you wish to add?<br>");
						for(final PlayerClass subClass : subsAvailable)
							StringUtil.append(content, "<a action=\"bypass -h npc_", String.valueOf(getObjectId()), "_Subclass 4 ", String.valueOf(subClass.ordinal()), "\" msg=\"1268;", "\">", formatClassForDisplay(subClass), "</a><br>");
					}
					else
					{
						player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.NoSubAtThisTime", player));
						return;
					}
					break;
				case 2: // Установка уже взятого саба (см case 5)
					content.append("Change Subclass:<br>");

					final int baseClassId = player.getBaseClassId();

					if(playerClassList.size() < 2)
						content.append("You can't change subclasses when you don't have a subclass to begin with.<br><a action=\"bypass -h npc_" + getObjectId() + "_Subclass 1\">Add subclass.</a>");
					else
					{
						content.append("Which class would you like to switch to?<br>");

						if(baseClassId == player.getActiveClassId())
							content.append(CharTemplateTable.getClassNameById(baseClassId) + " <font color=\"LEVEL\">(Base Class)</font><br>");
						else
							content.append("<a action=\"bypass -h npc_" + getObjectId() + "_Subclass 5 " + baseClassId + "\">" + CharTemplateTable.getClassNameById(baseClassId) + "</a> " + "<font color=\"LEVEL\">(Base Class)</font><br>");

						for(final L2SubClass subClass : playerClassList.values())
						{
							if(subClass.isBase())
								continue;
							final int subClassId = subClass.getClassId();

							if(subClassId == player.getActiveClassId())
								content.append(CharTemplateTable.getClassNameById(subClassId) + "<br>");
							else
								StringUtil.append(content, "<a action=\"bypass -h npc_", String.valueOf(getObjectId()), "_Subclass 5 ", String.valueOf(subClassId), "\">", CharTemplateTable.getClassNameById(subClassId), "</a><br>");
						}
					}
					break;
				case 3: // Отмена сабкласса - список имеющихся (см case 6)
					content.append("Change Subclass:<br>Which of the following sub-classes would you like to change?<br>");

					for(final L2SubClass sub : playerClassList.values())
						if(!sub.isBase())
							StringUtil.append(content, "<a action=\"bypass -h npc_", String.valueOf(getObjectId()), "_Subclass 6 ", String.valueOf(sub.getClassId()), "\">", CharTemplateTable.getClassNameById(sub.getClassId()), "</a><br>");

					content.append("<br>If you change a sub-class, you'll start at level 40 after the 2nd class transfer.");
					break;
				case 4: // Добавление сабкласса - обработка выбора из case 1
					boolean allowAddition = true;
					boolean haveItems = Config.ALT_GAME_SUBCLASS_FOR_ITEMS;

					// Проверка хватает ли уровня
					if(player.getLevel() < Config.ALT_GAME_LEVEL_TO_GET_SUBCLASS)
					{
						player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.NoSubBeforeLevel", player).addNumber(Config.ALT_GAME_LEVEL_TO_GET_SUBCLASS));
						allowAddition = false;
					}

					if(!playerClassList.isEmpty())
						for(final L2SubClass subClass : playerClassList.values())
							if(subClass.getLevel() < Config.ALT_GAME_LEVEL_TO_GET_SUBCLASS)
							{
								player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.NoSubBeforeLevel", player).addNumber(Config.ALT_GAME_LEVEL_TO_GET_SUBCLASS));
								allowAddition = false;
								break;
							}

					if(Config.ENABLE_OLYMPIAD && (Olympiad.isRegisteredInComp(player) || player.getOlympiadGameId() > -1))
					{
						player.sendPacket(Msg.SINCE_YOU_HAVE_CHANGED_YOUR_CLASS_INTO_A_SUB_JOB_YOU_CANNOT_PARTICIPATE_IN_THE_OLYMPIAD);
						_log.warning("Unregister due tj subclass change.");
						Olympiad.unRegisterNoble(player);
					}
					/*
					 * Если требуется квест - проверка прохождения Mimir's Elixir (Path to Subclass)
					 * Для камаэлей квест 236_SeedsOfChaos
					 * Если саб первый, то проверить начилие предмета, если не первый, то даём сабкласс.
					 * Если сабов нету, то проверяем наличие предмета.
					 */
					if(!Config.ALT_GAME_SUBCLASS_WITHOUT_QUESTS && !playerClassList.isEmpty() && playerClassList.size() < 2 + Config.ALT_GAME_SUB_ADD)
					{
						if(haveItems)
							haveItems = player.getInventory().getCountOf(STAR_OF_DESTINY) > 0 && player.getInventory().getCountOf(MIMIRS_ELIXIR) > 0;

						if(!haveItems)
						{
							if(!Config.ALT_GAME_SUBCLASS_WITHOUT_FATES_WHISPER)
							{
								allowAddition = player.isQuestCompleted("_234_FatesWhisper");
								if(!allowAddition)
								{
									player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.QuestFatesWhisper", player));
									return;
								}
							}

							if(player.getRace() == Race.kamael)
							{
								allowAddition = player.isQuestCompleted("_236_SeedsOfChaos");
								if(!allowAddition)
									player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.QuestSeedsOfChaos", player));
							}
							else
							{
								allowAddition = player.isQuestCompleted("_235_MimirsElixir");
								if(!allowAddition)
									player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.QuestMimirsElixir", player));
							}
						}
					}

					if(allowAddition)
					{
						if(haveItems)
						{
							player.getInventory().destroyItemByItemId(STAR_OF_DESTINY, 1, false);
							player.getInventory().destroyItemByItemId(MIMIRS_ELIXIR, 1, false);
						}

						final String className = CharTemplateTable.getClassNameById(classId);
						if(!player.addSubClass(classId, -1, true))
						{
							player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.SubclassCouldNotBeAdded", player));
							return;
						}

						content.append("Add Subclass:<br>The subclass of <font color=\"LEVEL\">" + className + "</font> has been added.");
						player.sendPacket(Msg.CONGRATULATIONS_YOU_HAVE_TRANSFERRED_TO_A_NEW_CLASS); // Transfer to new class.
					}
					else
						html.setFile("data/html/villagemaster/SubClass_Fail.htm");
					break;
				case 5: // Смена саба на другой из уже взятых - обработка выбора из case 2
					/*
					 * If the character is less than level 75 on any of their
					 * previously chosen classes then disallow them to change to
					 * their most recently added sub-class choice.
					 */
					// Проверка разрешено ли данном уровне игрока получить саб клас.
					if(playerClassList.size() < 2)
						for(final L2SubClass sub : playerClassList.values())
							if(sub.isBase() && sub.getLevel() < Config.ALT_GAME_LEVEL_TO_GET_SUBCLASS)
							{
								player.sendMessage("Вы не можете добавить еще сабкласс пока у вас уровень этого сабкласса " + Config.ALT_GAME_LEVEL_TO_GET_SUBCLASS);
								return;
							}

					if(Config.ENABLE_OLYMPIAD && (Olympiad.isRegisteredInComp(player) || player.getOlympiadGameId() > -1))
					{
						player.sendPacket(Msg.SINCE_YOU_HAVE_CHANGED_YOUR_CLASS_INTO_A_SUB_JOB_YOU_CANNOT_PARTICIPATE_IN_THE_OLYMPIAD);
						Olympiad.unRegisterNoble(player);
					}

					player.setActiveSubClass(classId, true);

					content.append("Change Subclass:<br>Your active subclass is now a <font color=\"LEVEL\">" + CharTemplateTable.getClassNameById(player.getActiveClassId()) + "</font>.");
					player.sendPacket(Msg.THE_TRANSFER_OF_SUB_CLASS_HAS_BEEN_COMPLETED);

					break;
				case 6: // Отмена сабкласса - обработка выбора из case 3
					content.append("Please choose a subclass to change to. If the one you are looking for is not here, " + //
					"please seek out the appropriate master for that class.<br>" + //
					"<font color=\"LEVEL\">Warning!</font> All classes and skills for this class will be removed.<br><br>");

					subsAvailable = getAvailableSubClasses(player);

					if(!subsAvailable.isEmpty())
						for(final PlayerClass subClass : subsAvailable)
							StringUtil.append(content, "<a action=\"bypass -h npc_", getObjectId(), "_Subclass 7 ", classId, " ", subClass.ordinal(), "\" msg=\"1445;", "\">", formatClassForDisplay(subClass), "</a><br>");
					else
					{
						player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.NoSubAtThisTime", player));
						return;
					}
					break;
				case 7: // Отмена сабкласса - обработка выбора из case 6
					if(Config.ENABLE_OLYMPIAD && (Olympiad.isRegisteredInComp(player) || player.getOlympiadGameId() > -1))
					{
						player.sendPacket(Msg.SINCE_YOU_HAVE_CHANGED_YOUR_CLASS_INTO_A_SUB_JOB_YOU_CANNOT_PARTICIPATE_IN_THE_OLYMPIAD);
						Olympiad.unRegisterNoble(player);
						return;
					}

					// Удаляем скиллы трансфера
					int item_id = 0;
					switch (ClassId.values()[classId])
					{
						case cardinal:
							item_id = 15307; // Cardinal (97)
							break;
						case evaSaint:
							item_id = 15308; // Eva's Saint (105)
							break;
						case shillienSaint:
							item_id = 15309; // Shillen Saint (112)
					}

					if(item_id > 0)
					{
						player.unsetVar("TransferSkills" + item_id);
						Functions.removeItem(player, item_id, player.getInventory().getCountOf(item_id));
					}

					if(player.modifySubClass(classId, newClassId))
					{
						content.append("Change Subclass:<br>Your subclass has been changed to <font color=\"LEVEL\">" + CharTemplateTable.getClassNameById(newClassId) + "</font>.");
						player.sendPacket(Msg.THE_NEW_SUB_CLASS_HAS_BEEN_ADDED); // Subclass added.
					}
					else
					{
						player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.SubclassCouldNotBeAdded", player));
						return;
					}
					break;
			}
			content.append("</body></html>");

			// If the content is greater than for a basic blank page,
			// then assume no external HTML file was assigned.
			if(content.length() > 26)
				html.setHtml(content.toString());

			player.sendPacket(html);
		}
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public String getHtmlPath(final int npcId, final int val)
	{
		String pom;
		if(val == 0)
			pom = "" + npcId;
		else
			pom = npcId + "-" + val;

		return "data/html/villagemaster/" + pom + ".htm";
	}

	// Private stuff
	public void createClan(final L2Player player, final String clanName)
	{
		if(Config.DEBUG)
			_log.fine(player.getObjectId() + "(" + player.getName() + ") requested clan creation from " + getObjectId() + "(" + getName() + ")");
		if(player.getLevel() < Config.MIN_LEVEL_TO_CREATE_PLEDGE)
		{
			player.sendPacket(Msg.YOU_ARE_NOT_QUALIFIED_TO_CREATE_A_CLAN);
			return;
		}

		if(player.getClanId() != 0)
		{
			player.sendPacket(Msg.YOU_HAVE_FAILED_TO_CREATE_A_CLAN);
			return;
		}

		if(!player.canCreateClan())
		{
			// you can't create a new clan within 10 days
			player.sendPacket(Msg.YOU_MUST_WAIT_10_DAYS_BEFORE_CREATING_A_NEW_CLAN);
			return;
		}
		if(clanName.length() > 16)
		{
			player.sendPacket(Msg.CLAN_NAMES_LENGTH_IS_INCORRECT);
			return;
		}
		if(!Config.CLAN_NAME_TEMPLATE.matcher(clanName).matches())
		{
			// clan name is not matching template
			player.sendPacket(Msg.CLAN_NAME_IS_INCORRECT);
			return;
		}

		final L2Clan clan = ClanTable.getInstance().createClan(player, clanName);
		if(clan == null)
		{
			// clan name is already taken
			player.sendPacket(Msg.CLAN_NAME_IS_INCORRECT);
			return;
		}

		// should be update packet only
		player.updatePledgeClass();
		player.broadcastCharInfo();
		player.sendPacket(Msg.CLAN_HAS_BEEN_CREATED, new PledgeShowInfoUpdate(clan), new PledgeShowMemberListAll(clan, player));
	}

	public void setLeader(final L2Player player, final String name)
	{
		if(!player.isClanLeader())
		{
			player.sendPacket(Msg.ONLY_THE_CLAN_LEADER_IS_ENABLED);
			return;
		}

		if(player.getSiegeState() != 0 || player.getTerritorySiege() > -1)
			// leader.sendMessage(new CustomMessage("scripts.services.Rename.SiegeNow", leader, new Object[0]));
			return;

		final L2Clan clan = player.getClan();
		if(clan == null)
		{
			player.sendPacket(Msg.YOU_HAVE_FAILED_TO_CREATE_A_CLAN);
			return;
		}

		final L2ClanMember member = clan.getClanMember(name);
		if(member == null)
		{
			player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.S1IsNotMemberOfTheClan", player).addString(name));
			showChatWindow(player, "data/html/villagemaster/clan-20.htm");
			return;
		}

		player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.ClanLeaderWillBeChangedFromS1ToS2", player).addString(player.getName()).addString(name));
		
		L2GameThreadPools.getInstance().scheduleGeneral(new ChangeLeaderClan(clan, name, player), Config.CHANGE_CLAN_LEADER_TIME * 60000);
	}
	
	private static class ChangeLeaderClan implements Runnable
	{
		private L2Clan _clanId;
		private String _name;
		private L2Player _player;

		public ChangeLeaderClan(L2Clan clan, String name, L2Player player)
		{
			_clanId = clan;
			_name = name;
			_player = player;
		}

		@Override
		public void run()
		{
			if(_player.getSiegeState() != 0 || _player.getTerritorySiege() > -1)
			// leader.sendMessage(new CustomMessage("scripts.services.Rename.SiegeNow", leader, new Object[0]));
				return;
			
			final L2ClanMember _member = _clanId.getClanMember(_name);
			if(_member == null)
			{
				_player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.S1IsNotMemberOfTheClan", _player).addString(_name));
				return;
			}
			
			if(_clanId.getLevel() >= CastleSiegeManager.getSiegeClanMinLevel())
			{
				if(_clanId.getLeader() != null)
				{
					final L2Player oldLeaderPlayer = _clanId.getLeader().getPlayer();
					if(oldLeaderPlayer != null)
						SiegeManager.removeSiegeSkills(oldLeaderPlayer);
				}
				final L2Player newLeaderPlayer = _member.getPlayer();
				if(newLeaderPlayer != null)
					SiegeManager.addSiegeSkills(newLeaderPlayer);
			}

			if(_player.getInventory().getItemByItemId(6841) != null)
				_player.getInventory().destroyItemByItemId(6841, 1, false);

			_clanId.setLeader(_member);
			_clanId.broadcastClanStatus(true, true, false);
			if(_member.getPlayer() != null)
				_member.getPlayer().broadcastRelationChanged();

			_clanId.updateClanInDB();
		}
	}
	
	public void createSubPledge(final L2Player player, final String clanName, int pledgeType, final int minClanLvl, final String leaderName)
	{
		int subLeaderId = 0;
		L2ClanMember subLeader = null;

		final L2Clan clan = player.getClan();

		if(clan == null || !player.isClanLeader())
		{
			player.sendPacket(Msg.YOU_HAVE_FAILED_TO_CREATE_A_CLAN);
			return;
		}

		if(!Config.CLAN_NAME_TEMPLATE.matcher(clanName).matches())
		{
			player.sendPacket(Msg.CLAN_NAME_IS_INCORRECT);
			return;
		}

		final SubPledge[] subPledge = clan.getAllSubPledges();
		for(final SubPledge element : subPledge)
			if(element.getName().equals(clanName))
			{
				player.sendPacket(Msg.ANOTHER_MILITARY_UNIT_IS_ALREADY_USING_THAT_NAME_PLEASE_ENTER_A_DIFFERENT_NAME);
				return;
			}

		if(ClanTable.getInstance().getClanByName(clanName) != null)
		{
			player.sendPacket(Msg.ANOTHER_MILITARY_UNIT_IS_ALREADY_USING_THAT_NAME_PLEASE_ENTER_A_DIFFERENT_NAME);
			return;
		}

		if(clan.getLevel() < minClanLvl)
		{
			player.sendPacket(Msg.THE_CONDITIONS_NECESSARY_TO_CREATE_A_MILITARY_UNIT_HAVE_NOT_BEEN_MET);
			return;
		}

		if(pledgeType != L2Clan.SUBUNIT_ACADEMY)
		{
			subLeader = clan.getClanMember(leaderName);
			if(subLeader == null || subLeader.getPledgeType() != L2Clan.SUBUNIT_NONE)
			{
				player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.PlayerCantBeAssignedAsSubUnitLeader", player));
				return;
			}
			else if(subLeader.isClanLeader())
			{
				player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.YouCantBeASubUnitLeader", player));
				return;
			}
			else
				subLeaderId = subLeader.getObjectId();
		}

		pledgeType = clan.createSubPledge(player, pledgeType, subLeaderId, clanName);
		if(pledgeType == L2Clan.SUBUNIT_NONE)
		{
			player.sendActionFailed();
			return;
		}

		clan.broadcastToOnlineMembers(new PledgeReceiveSubPledgeCreated(clan.getSubPledge(pledgeType)));

		SystemMessage sm;
		if(pledgeType == L2Clan.SUBUNIT_ACADEMY)
		{
			sm = new SystemMessage(SystemMessage.CONGRATULATIONS_THE_S1S_CLAN_ACADEMY_HAS_BEEN_CREATED);
			sm.addString(player.getClan().getName());
		}
		else if(pledgeType >= L2Clan.SUBUNIT_KNIGHT1)
		{
			sm = new SystemMessage(SystemMessage.THE_KNIGHTS_OF_S1_HAVE_BEEN_CREATED);
			sm.addString(player.getClan().getName());
		}
		else if(pledgeType >= L2Clan.SUBUNIT_ROYAL1)
		{
			sm = new SystemMessage(SystemMessage.THE_ROYAL_GUARD_OF_S1_HAVE_BEEN_CREATED);
			sm.addString(player.getClan().getName());
		}
		else
			sm = Msg.CLAN_HAS_BEEN_CREATED;

		player.sendPacket(sm);

		if(subLeader != null)
		{
			clan.broadcastToOnlineMembers(new PledgeShowMemberListUpdate(subLeader));
			if(subLeader.isOnline())
			{
				subLeader.getPlayer().updatePledgeClass();
				subLeader.getPlayer().broadcastCharInfo();
			}
		}
	}

	public void assignSubPledgeLeader(final L2Player player, final String clanName, final String leaderName)
	{
		final L2Clan clan = player.getClan();

		if(clan == null)
		{
			player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.ClanDoesntExist", player));
			return;
		}

		if(!player.isClanLeader())
		{
			player.sendPacket(Msg.ONLY_THE_CLAN_LEADER_IS_ENABLED);
			return;
		}

		final SubPledge[] subPledge = clan.getAllSubPledges();
		int match = -1;
		for(int i = 0; i < subPledge.length; i++)
			if(subPledge[i].getName().equals(clanName))
			{
				match = i;
				break;
			}
		if(match < 0)
		{
			player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.SubUnitNotFound", player));
			return;
		}

		final L2ClanMember subLeader = clan.getClanMember(leaderName);
		if(subLeader == null || subLeader.getPledgeType() != L2Clan.SUBUNIT_NONE)
		{
			player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.PlayerCantBeAssignedAsSubUnitLeader", player));
			return;
		}

		if(subLeader.isClanLeader())
		{
			player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.YouCantBeASubUnitLeader", player));
			return;
		}

		subPledge[match].setLeaderId(subLeader.getObjectId());
		clan.broadcastToOnlineMembers(new PledgeReceiveSubPledgeCreated(subPledge[match]));

		clan.broadcastToOnlineMembers(new PledgeShowMemberListUpdate(subLeader));
		if(subLeader.isOnline())
		{
			subLeader.getPlayer().updatePledgeClass();
			subLeader.getPlayer().broadcastCharInfo();
		}

		player.sendMessage(new CustomMessage("l2n.game.model.instances.L2VillageMasterInstance.NewSubUnitLeaderHasBeenAssigned", player));
	}

	/**
	 * Запрос на удаление клана
	 * 
	 * @param player
	 */
	private void dissolveClan(final L2Player player)
	{
		if(player == null || player.getClan() == null)
			return;

		final L2Clan clan = player.getClan();
		if(!player.isClanLeader())
		{
			player.sendPacket(Msg.ONLY_THE_CLAN_LEADER_IS_ENABLED);
			return;
		}
		if(clan.getAllyId() != 0)
		{
			player.sendPacket(Msg.YOU_CANNOT_DISPERSE_THE_CLANS_IN_YOUR_ALLIANCE);
			return;
		}
		if(clan.isAtWar() > 0)
		{
			player.sendPacket(Msg.YOU_CANNOT_DISSOLVE_A_CLAN_WHILE_ENGAGED_IN_A_WAR);
			return;
		}
		if(clan.getHasCastle() != 0 || clan.getHasHideout() != 0 || clan.getHasFortress() != 0)
		{
			player.sendPacket(Msg.UNABLE_TO_DISPERSE_YOUR_CLAN_OWNS_ONE_OR_MORE_CASTLES_OR_HIDEOUTS);
			return;
		}
		if(SiegeDatabase.checkIsRegistered(clan, 0))
		{
			player.sendPacket(Msg.UNABLE_TO_DISPERSE_YOUR_CLAN_HAS_REQUESTED_TO_PARTICIPATE_IN_A_CASTLE_SIEGE);
			return;
		}

		ClanTable.getInstance().dissolveClan(player);
	}

	/**
	 * TODO
	 * Запрос на восстановление клана
	 * 
	 * @param player
	 */
	private void reviveClan(final L2Player player)
	{
		if(player == null || player.getClan() == null)
			return;

		final L2Clan clan = player.getClan();
		if(!player.isClanLeader())
		{
			player.sendPacket(Msg.ONLY_THE_CLAN_LEADER_IS_ENABLED);
			return;
		}
	}

	public void levelUpClan(final L2Player player)
	{
		final L2Clan clan = player.getClan();
		if(clan == null)
			return;
		if(!player.isClanLeader())
		{
			player.sendPacket(Msg.ONLY_THE_CLAN_LEADER_IS_ENABLED);
			return;
		}

		boolean increaseClanLevel = false;

		switch (clan.getLevel())
		{
			case 0:
			{
				// Upgrade to 1
				if(player.getSp() >= Config.CLAN_EXP_TO_LVL_1 && player.getAdena() >= Config.CLAN_ADENA_TO_LVL_1 || player.isGM())
				{
					if(!player.isGM())
					{
						player.setSp(player.getSp() - Config.CLAN_EXP_TO_LVL_1);
						player.reduceAdena(Config.CLAN_ADENA_TO_LVL_1, true);
					}
					increaseClanLevel = true;
				}
				break;
			}
			case 1:
			{
				// Upgrade to 2
				if(player.getSp() >= Config.CLAN_EXP_TO_LVL_2 && player.getAdena() >= Config.CLAN_ADENA_TO_LVL_2 || player.isGM())
				{
					if(!player.isGM())
					{
						player.setSp(player.getSp() - Config.CLAN_EXP_TO_LVL_2);
						player.reduceAdena(Config.CLAN_ADENA_TO_LVL_2, true);
					}
					increaseClanLevel = true;
				}
				break;
			}
			case 2:
			{
				// Upgrade to 3
				// itemid 1419 == Blood Mark
				if(player.getSp() >= Config.CLAN_EXP_TO_LVL_3 && player.getInventory().getCountOf(Config.CLAN_ITEM_TO_LVL_3) >= Config.CLAN_COUNT_ITEM_TO_LVL_3 || player.isGM())
				{
					if(!player.isGM())
					{
						player.setSp(player.getSp() - Config.CLAN_EXP_TO_LVL_3);
						player.getInventory().destroyItemByItemId(Config.CLAN_ITEM_TO_LVL_3, Config.CLAN_COUNT_ITEM_TO_LVL_3, true);
					}
					increaseClanLevel = true;
				}
				break;
			}
			case 3:
			{
				// Upgrade to 4
				// itemid 3874 == Alliance Manifesto
				if(player.getSp() >= Config.CLAN_EXP_TO_LVL_4 && player.getInventory().getCountOf(Config.CLAN_ITEM_TO_LVL_4) >= Config.CLAN_COUNT_ITEM_TO_LVL_4 || player.isGM())
				{
					if(!player.isGM())
					{
						player.setSp(player.getSp() - Config.CLAN_EXP_TO_LVL_4);
						player.getInventory().destroyItemByItemId(Config.CLAN_ITEM_TO_LVL_4, Config.CLAN_COUNT_ITEM_TO_LVL_4, true);
					}
					increaseClanLevel = true;
				}
				break;
			}
			case 4:
			{
				// Upgrade to 5
				// itemid 3870 == Seal of Aspiration
				if(player.getSp() >= Config.CLAN_EXP_TO_LVL_5 && player.getInventory().getCountOf(Config.CLAN_ITEM_TO_LVL_5) >= Config.CLAN_COUNT_ITEM_TO_LVL_5 || player.isGM())
				{
					if(!player.isGM())
					{
						player.setSp(player.getSp() - Config.CLAN_EXP_TO_LVL_5);
						player.getInventory().destroyItemByItemId(Config.CLAN_ITEM_TO_LVL_5, Config.CLAN_COUNT_ITEM_TO_LVL_5, true);
					}
					increaseClanLevel = true;
				}
				break;
			}
			case 5:
			{
				// Upgrade to 6
				if(clan.getReputationScore() >= Config.CLANREP_CLAN_LVL_6 && clan.getMembersCount() >= Config.MEMBERFORLEVEL6 || player.isGM())
				{
					if(!player.isGM())
					{
						clan.incReputation(-Config.CLANREP_CLAN_LVL_6, Config.USE_REDUCE_REPSCORE_RATE, "LvlUpClan");
						player.sendPacket(new SystemMessage(SystemMessage.S1_POINTS_HAVE_BEEN_DEDUCTED_FROM_THE_CLAN_REPUTATION_SCORE).addNumber(Config.CLANREP_CLAN_LVL_6));
					}
					increaseClanLevel = true;
				}
				break;
			}
			case 6:
			{
				// Upgrade to 7
				if(clan.getReputationScore() >= Config.CLANREP_CLAN_LVL_7 && clan.getMembersCount() >= Config.MEMBERFORLEVEL7 || player.isGM())
				{
					if(!player.isGM())
					{
						clan.incReputation(-Config.CLANREP_CLAN_LVL_7, Config.USE_REDUCE_REPSCORE_RATE, "LvlUpClan");
						player.sendPacket(new SystemMessage(SystemMessage.S1_POINTS_HAVE_BEEN_DEDUCTED_FROM_THE_CLAN_REPUTATION_SCORE).addNumber(Config.CLANREP_CLAN_LVL_7));
					}
					increaseClanLevel = true;
				}
				break;
			}
			case 7:
			{
				// Upgrade to 8
				if(clan.getReputationScore() >= Config.CLANREP_CLAN_LVL_8 && clan.getMembersCount() >= Config.MEMBERFORLEVEL8 || player.isGM())
				{
					if(!player.isGM())
					{
						clan.incReputation(-Config.CLANREP_CLAN_LVL_8, Config.USE_REDUCE_REPSCORE_RATE, "LvlUpClan");
						player.sendPacket(new SystemMessage(SystemMessage.S1_POINTS_HAVE_BEEN_DEDUCTED_FROM_THE_CLAN_REPUTATION_SCORE).addNumber(Config.CLANREP_CLAN_LVL_8));
					}
					increaseClanLevel = true;
				}
				break;
			}
			case 8:
			{
				// Upgrade to 9
				// itemId 9910 == Blood Oath
				if(clan.getReputationScore() >= Config.CLANREP_CLAN_LVL_9 && player.getInventory().getCountOf(Config.CLAN_ITEM_TO_LVL_9) >= Config.CLAN_COUNT_ITEM_TO_LVL_9 && clan.getMembersCount() >= Config.MEMBERFORLEVEL9 || player.isGM())
				{
					if(!player.isGM())
					{
						clan.incReputation(-Config.CLANREP_CLAN_LVL_9, Config.USE_REDUCE_REPSCORE_RATE, "LvlUpClan");
						player.sendPacket(new SystemMessage(SystemMessage.S1_POINTS_HAVE_BEEN_DEDUCTED_FROM_THE_CLAN_REPUTATION_SCORE).addNumber(Config.CLANREP_CLAN_LVL_9));
						player.getInventory().destroyItemByItemId(Config.CLAN_ITEM_TO_LVL_9, Config.CLAN_COUNT_ITEM_TO_LVL_9, true);
					}
					increaseClanLevel = true;
				}
				break;
			}
			case 9:
			{
				// Upgrade to 10
				// itemId 9911 == Blood Alliance
				if(clan.getReputationScore() >= Config.CLANREP_CLAN_LVL_10 && player.getInventory().getCountOf(Config.CLAN_ITEM_TO_LVL_10) >= Config.CLAN_COUNT_ITEM_TO_LVL_10 && clan.getMembersCount() >= Config.MEMBERFORLEVEL10 || player.isGM())
				{
					if(!player.isGM())
					{
						clan.incReputation(-Config.CLANREP_CLAN_LVL_10, Config.USE_REDUCE_REPSCORE_RATE, "LvlUpClan");
						player.sendPacket(new SystemMessage(SystemMessage.S1_POINTS_HAVE_BEEN_DEDUCTED_FROM_THE_CLAN_REPUTATION_SCORE).addNumber(Config.CLANREP_CLAN_LVL_10));
						player.getInventory().destroyItemByItemId(Config.CLAN_ITEM_TO_LVL_10, Config.CLAN_COUNT_ITEM_TO_LVL_10, true);
					}
					increaseClanLevel = true;
				}
				break;
			}
			case 10:
			{
				// Upgrade to 11
				// Status владелец поместья
				if(clan.getReputationScore() >= Config.CLANREP_CLAN_LVL_11 && clan.getMembersCount() >= Config.MEMBERFORLEVEL11 || player.isGM())
				{
					if(!player.isGM())
					{
						// TODO Вместо Итема сделать по статусу
						final L2ItemInstance item = player.getInventory().getItemByItemId(9911);
						if(item != null && item.getCount() >= 5)
						{
							clan.incReputation(-Config.CLANREP_CLAN_LVL_11, Config.USE_REDUCE_REPSCORE_RATE, "LvlUpClan");
							player.sendPacket(new SystemMessage(SystemMessage.S1_POINTS_HAVE_BEEN_DEDUCTED_FROM_THE_CLAN_REPUTATION_SCORE).addNumber(Config.CLANREP_CLAN_LVL_11));
							increaseClanLevel = true;
						}
					}
					increaseClanLevel = true;
				}
				break;
			}
			default:
			{
				player.sendPacket(Msg.CLAN_HAS_FAILED_TO_INCREASE_SKILL_LEVEL);
				return;
			}
		}
		if(increaseClanLevel)
		{
			player.sendChanges();

			clan.setLevel((byte) (clan.getLevel() + 1));
			clan.updateClanInDB();
			doCast(SkillTable.getInstance().getInfo(5103, 1), player, true);

			if(clan.getLevel() >= CastleSiegeManager.getSiegeClanMinLevel())
				SiegeManager.addSiegeSkills(player);

			if(clan.getLevel() == 5)
				player.sendPacket(Msg.NOW_THAT_YOUR_CLAN_LEVEL_IS_ABOVE_LEVEL_5_IT_CAN_ACCUMULATE_CLAN_REPUTATION_POINTS);

			// notify all the members about it
			final PledgeShowInfoUpdate pu = new PledgeShowInfoUpdate(clan);
			final PledgeStatusChanged ps = new PledgeStatusChanged(clan);
			for(final L2ClanMember mbr : clan.getMembers())
				if(mbr.isOnline())
				{
					mbr.getPlayer().updatePledgeClass();
					mbr.getPlayer().sendPacket(Msg.CLANS_SKILL_LEVEL_HAS_INCREASED, pu, ps);
					mbr.getPlayer().broadcastCharInfo();
				}
		}
		else
			player.sendPacket(Msg.CLAN_HAS_FAILED_TO_INCREASE_SKILL_LEVEL);
	}

	public void createAlly(final L2Player player, final String allyName)
	{
		// D5 You may not ally with clan you are battle with.
		// D6 Only the clan leader may apply for withdraw from alliance.
		// DD No response. Invitation to join an
		// D7 Alliance leaders cannot withdraw.
		// D9 Different Alliance
		// EB alliance information
		// Ec alliance name $s1
		// ee alliance leader: $s2 of $s1
		// ef affilated clans: total $s1 clan(s)
		// f6 you have already joined an alliance
		// f9 you cannot new alliance 10 days
		// fd cannot accept. clan ally is register as enemy during siege battle.
		// fe you have invited someone to your alliance.
		// 100 do you wish to withdraw from the alliance
		// 102 enter the name of the clan you wish to expel.
		// 202 do you realy wish to dissolve the alliance
		// 502 you have accepted alliance
		// 602 you have failed to invite a clan into the alliance
		// 702 you have withdraw

		if(Config.DEBUG)
			_log.fine(player.getObjectId() + "(" + player.getName() + ") requested ally creation from " + getObjectId() + "(" + getName() + ")");

		if(!player.isClanLeader())
		{
			player.sendPacket(Msg.ONLY_CLAN_LEADERS_MAY_CREATE_ALLIANCES);
			return;
		}
		if(player.getClan().getAllyId() != 0)
		{
			player.sendPacket(Msg.YOU_ALREADY_BELONG_TO_ANOTHER_ALLIANCE);
			return;
		}
		if(allyName.length() > 16)
		{
			player.sendPacket(Msg.INCORRECT_LENGTH_FOR_AN_ALLIANCE_NAME);
			return;
		}
		if(!Config.ALLY_NAME_TEMPLATE.matcher(allyName).matches())
		{
			player.sendPacket(Msg.INCORRECT_ALLIANCE_NAME);
			return;
		}
		if(player.getClan().getLevel() < 5)
		{
			player.sendPacket(Msg.TO_CREATE_AN_ALLIANCE_YOUR_CLAN_MUST_BE_LEVEL_5_OR_HIGHER);
			return;
		}
		if(ClanTable.getInstance().getAllyByName(allyName) != null)
		{
			player.sendPacket(Msg.THIS_ALLIANCE_NAME_ALREADY_EXISTS);
			return;
		}
		if(!player.getClan().canCreateAlly())
		{
			player.sendPacket(Msg.YOU_CANNOT_CREATE_A_NEW_ALLIANCE_WITHIN_10_DAYS_AFTER_DISSOLUTION);
			return;
		}

		final L2Alliance alliance = ClanTable.getInstance().createAlliance(player, allyName);
		if(alliance == null)
			return;

		player.broadcastCharInfo();
		player.sendMessage("Alliance " + allyName + " has been created.");
	}

	private void dissolveAlly(final L2Player player)
	{
		if(player == null || player.getAlliance() == null)
			return;

		if(!player.isAllyLeader())
		{
			player.sendPacket(Msg.FEATURE_AVAILABLE_TO_ALLIANCE_LEADERS_ONLY);
			return;
		}

		if(player.getAlliance().getMembersCount() > 1)
		{
			player.sendPacket(Msg.YOU_HAVE_FAILED_TO_DISSOLVE_THE_ALLIANCE);
			return;
		}

		ClanTable.getInstance().dissolveAlly(player);
	}

	private Set<PlayerClass> getAvailableSubClasses(final L2Player player)
	{
		final int charClassId = player.getBaseClassId();
		final Race npcRace = getVillageMasterRace();
		final ClassType npcTeachType = getVillageMasterTeachType();

		final PlayerClass currClass = PlayerClass.values()[charClassId];

		/**
		 * If the race of your main class is Elf or Dark Elf, you may not select
		 * each class as a subclass to the other class, and you may not select
		 * Overlord and Warsmith class as a subclass.
		 * You may not select a similar class as the subclass. The occupations
		 * classified as similar classes are as follows:
		 * Treasure Hunter, Plainswalker and Abyss Walker Hawkeye, Silver Ranger
		 * and Phantom Ranger Paladin, Dark Avenger, Temple Knight and Shillien
		 * Knight Warlocks, Elemental Summoner and Phantom Summoner Elder and
		 * Shillien Elder Swordsinger and Bladedancer Sorcerer, Spellsinger and
		 * Spellhowler
		 * Kamael могут брать только сабы Kamael
		 * Другие классы не могут брать сабы Kamael
		 */
		final Set<PlayerClass> availSubs = currClass.getAvailableSubclasses();

		if(availSubs == null)
			return null;

		// Из списка сабов удаляем мейн класс игрока
		availSubs.remove(currClass);

		for(final PlayerClass availSub : availSubs)
		{
			// Удаляем из списка возможных сабов, уже взятые сабы и их предков
			for(final L2SubClass subClass : player.getSubClasses().values())
			{
				if(availSub.ordinal() == subClass.getClassId())
				{
					availSubs.remove(availSub);
					continue;
				}

				// Удаляем из возможных сабов их родителей, если таковые есть у чара
				final ClassId parent = ClassId.values()[availSub.ordinal()].getParent(player.getSex());
				if(parent != null && parent.getId() == subClass.getClassId())
				{
					availSubs.remove(availSub);
					continue;
				}

				// Удаляем из возможных сабов родителей текущих сабклассов, иначе если взять саб berserker
				// и довести до 3ей профы - doombringer, игроку будет предложен berserker вновь (дежавю)
				final ClassId subParent = ClassId.values()[subClass.getClassId()].getParent(player.getSex());
				if(subParent != null && subParent.getId() == availSub.ordinal())
					availSubs.remove(availSub);
			}

			if(!availSub.isOfRace(Race.human) && !availSub.isOfRace(Race.elf))
			{
				if(!availSub.isOfRace(npcRace))
					availSubs.remove(availSub);
			}
			else if(!availSub.isOfType(npcTeachType))
				availSubs.remove(availSub);

			// Особенности саб классов камаэль
			if(availSub.isOfRace(Race.kamael))
			{
				// Для Soulbreaker-а и SoulHound не предлагаем Soulbreaker-а другого пола
				if((currClass == PlayerClass.MaleSoulHound || currClass == PlayerClass.FemaleSoulHound || currClass == PlayerClass.FemaleSoulbreaker || currClass == PlayerClass.MaleSoulbreaker) && (availSub == PlayerClass.FemaleSoulbreaker || availSub == PlayerClass.MaleSoulbreaker))
					availSubs.remove(availSub);

				// Для Berserker(doombringer) и Arbalester(trickster) предлагаем Soulbreaker-а только своего пола
				if(currClass == PlayerClass.Berserker || currClass == PlayerClass.Doombringer || currClass == PlayerClass.Arbalester || currClass == PlayerClass.Trickster)
					if(player.getSex() == 1 && availSub == PlayerClass.MaleSoulbreaker || player.getSex() == 0 && availSub == PlayerClass.FemaleSoulbreaker)
						availSubs.remove(availSub);

				// Inspector доступен, только когда вкачаны 2 возможных первых саба камаэль(+ мейн класс):
				// doombringer(berserker), soulhound(maleSoulbreaker, femaleSoulbreaker), trickster(arbalester)
				if(availSub == PlayerClass.Inspector)
					// doombringer(berserker)
					if(!player.getSubClasses().containsKey(131) && !player.getSubClasses().containsKey(127))
						availSubs.remove(availSub);
					// soulhound(maleSoulbreaker, femaleSoulbreaker)
					else if(!player.getSubClasses().containsKey(132) && !player.getSubClasses().containsKey(133) && !player.getSubClasses().containsKey(128) && !player.getSubClasses().containsKey(129))
						availSubs.remove(availSub);
					// trickster(arbalester)
					else if(!player.getSubClasses().containsKey(134) && !player.getSubClasses().containsKey(130))
						availSubs.remove(availSub);
			}
		}
		return availSubs;
	}

	private String formatClassForDisplay(final PlayerClass className)
	{
		String classNameStr = className.toString();
		final char[] charArray = classNameStr.toCharArray();

		for(int i = 1; i < charArray.length; i++)
			if(Character.isUpperCase(charArray[i]))
				classNameStr = classNameStr.substring(0, i) + " " + classNameStr.substring(i);

		return classNameStr;
	}

	private Race getVillageMasterRace()
	{
		final String npcClass = getTemplate().getJClass().toLowerCase();

		if(npcClass.indexOf("human") > -1)
			return Race.human;
		if(npcClass.indexOf("darkelf") > -1)
			return Race.darkelf;
		if(npcClass.indexOf("elf") > -1)
			return Race.elf;
		if(npcClass.indexOf("orc") > -1)
			return Race.orc;
		if(npcClass.indexOf("dwarf") > -1)
			return Race.dwarf;
		return Race.kamael;
	}

	private ClassType getVillageMasterTeachType()
	{
		final String npcClass = getTemplate().getJClass();
		if(npcClass.indexOf("sanctuary") > -1 || npcClass.indexOf("clergyman") > -1)
			return ClassType.Priest;

		if(npcClass.indexOf("mageguild") > -1 || npcClass.indexOf("patriarch") > -1)
			return ClassType.Mystic;

		return ClassType.Fighter;
	}

	/**
	 * this displays PledgeSkillList to the player.
	 * 
	 * @param player
	 */
	public void showClanSkillWindow(final L2Player player)
	{
		if(player == null || player.getClan() == null)
			return;

		if(!Config.ALLOW_CLANSKILLS)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append("Not available now, try later.<br>");
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			player.sendPacket(html);
			return;
		}

		if(player.getTransformationId() != 0)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append(new CustomMessage("l2n.game.model.instances.L2NpcInstance.CantTeachBecauseTransformation", player));
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			player.sendPacket(html);

			return;
		}

		final AcquireSkillList asl = new AcquireSkillList(AcquireSkillList.CLAN);
		int counts = 0;

		final L2PledgeSkillLearn[] skills = SkillTreeTable.getInstance().getAvailablePledgeSkills(player.getClan());
		for(final L2PledgeSkillLearn s : skills)
		{
			if(s == null)
				continue;

			final L2Skill sk = SkillTable.getInstance().getInfo(s.getId(), s.getLevel());
			if(sk == null)
			{
				_log.warning("L2VillageMasterInstance: skill " + s.getName() + " [" + s.getId() + "-" + s.getLevel() + "] is NULL.");
				continue;
			}

			final int cost = s.getRepCost();
			final int itemCount = s.getItemCount();

			asl.addSkill(s.getId(), s.getLevel(), s.getLevel(), cost, itemCount);
			counts++;
		}

		if(counts == 0)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());

			if(player.getClan().getLevel() < 10)
			{
				final SystemMessage sm = new SystemMessage(SystemMessage.YOU_DO_NOT_HAVE_ANY_FURTHER_SKILLS_TO_LEARN__COME_BACK_WHEN_YOU_HAVE_REACHED_LEVEL_S1);
				sm.addNumber(player.getClan().getLevel() + 1);
				player.sendPacket(sm);
			}
			else
			{
				final TextBuilder sb = new TextBuilder();
				sb.append("<html><head><body>");
				sb.append("You've learned all skills available for your Clan.<br>");
				sb.append("</body></html>");
				html.setHtml(sb.toString());
				player.sendPacket(html);
			}
		}
		else
			player.sendPacket(asl);
		player.sendActionFailed();
	}
}
