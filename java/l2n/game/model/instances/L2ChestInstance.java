package l2n.game.model.instances;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.ai.CtrlEvent;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.ItemToDrop;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SocialAction;
import l2n.game.tables.NpcTable;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Rnd;

import java.util.List;

public class L2ChestInstance extends L2MonsterInstance
{
	private boolean _fake;

	public L2ChestInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onSpawn()
	{
		_fake = !Rnd.chance(Config.ALT_TRUE_CHESTS);
		super.onSpawn();
	}

	public void onOpen(L2Player opener)
	{
		if(_fake)
		{
			opener.broadcastPacket(new SocialAction(opener.getObjectId(), 13));
			if(Rnd.get(100) < getChance())
			{
				chestTrap(opener);
				opener.sendMessage(new CustomMessage("l2n.game.model.instances.L2ChestInstance.isTrap", opener));
			}
			else
				opener.sendMessage(new CustomMessage("l2n.game.model.instances.L2ChestInstance.Fake", opener));

			getAI().notifyEvent(CtrlEvent.EVT_ATTACKED, opener, 100);
		}
		else
		{
			setSpoiled(false, null);
			int trueId = getTrueId();
			opener.broadcastPacket(new SocialAction(opener.getObjectId(), 3));
			if(NpcTable.getTemplate(trueId) != null && NpcTable.getTemplate(trueId).getDropData() != null)
			{
				final List<ItemToDrop> drops = NpcTable.getTemplate(trueId).getDropData().rollDrop(0, this, opener, 1);
				for(final ItemToDrop drop : drops)
					dropItem(opener, drop.itemId, drop.count);
			}
			doDie(opener);
		}
	}

	private int getTrueId()
	{
		switch (getNpcId())
		{
			case 21671: // Otherworldly Invader Food
				return Rnd.get(18287, 18288);
			case 21694: // Dimension Invader Food
				return Rnd.get(18289, 18290);
			case 21717: // Purgatory Invader Food
				return Rnd.get(18291, 18292);
			case 21740: // Forbidden Path Invader Food
				return Rnd.get(18293, 18294);
			case 21763: // Dark Omen Invader Food
				return Rnd.get(18295, 18296);
			case 21786: // Messenger Invader Food
				return Rnd.get(18297, 18298);
			default:
				return getNpcId() - 3536;
		}
	}

	// Cast - Trap Chest
	public void chestTrap(L2Player player)
	{
		int trapSkillId = 0;
		int rnd = Rnd.get(120);

		if(getLevel() >= 61)
		{
			if(rnd >= 90)
				trapSkillId = 4139;// explosion
			else if(rnd >= 50)
				trapSkillId = 4118;// area paralysys
			else if(rnd >= 20)
				trapSkillId = 1167;// poison cloud
			else
				trapSkillId = 223;// sting
		}
		else if(getLevel() >= 41)
		{
			if(rnd >= 90)
				trapSkillId = 4139;// explosion
			else if(rnd >= 60)
				trapSkillId = 96;// bleed
			else if(rnd >= 20)
				trapSkillId = 1167;// poison cloud
			else
				trapSkillId = 4118;// area paralysys
		}
		else if(getLevel() >= 21)
		{
			if(rnd >= 80)
				trapSkillId = 4139;// explosion
			else if(rnd >= 50)
				trapSkillId = 96;// bleed
			else if(rnd >= 20)
				trapSkillId = 1167;// poison cloud
			else
				trapSkillId = 129;// poison
		}
		else if(rnd >= 80)
			trapSkillId = 4139;// explosion
		else if(rnd >= 50)
			trapSkillId = 96;// bleed
		else
			trapSkillId = 129;// poison
		handleCast(player, trapSkillId);
	}

	// cast casse
	private void handleCast(L2Player player, int skillId)
	{
		int skillLevel = 1;
		byte lvl = getLevel();

		if(lvl > 20 && lvl <= 40)
			skillLevel = 3;
		else if(lvl > 40 && lvl <= 60)
			skillLevel = 5;
		else if(lvl > 60)
			skillLevel = 6;

		if(player.isDead() || !player.isVisible() || !player.isInRange(player.getTarget(), INTERACTION_DISTANCE))
			return;

		L2Skill skill = SkillTable.getInstance().getInfo(skillId, skillLevel);
		if(skill != null)
		{
			broadcastPacket(new MagicSkillUse(this, player, skill.getId(), skillLevel, skill.getHitTime(), 0));
			skill.useSkill(this, player);
			return;
		}
	}

	/**
	 * Зависит от уровня сундука, чем больше, тем больше шанс
	 * 
	 * @return шанс что сундук станет ловушкой при не удачном открытии.
	 **/
	public int getChance()
	{
		int chestGroup = 0;
		int chestTrapLimit = 0;

		if(getLevel() > 60)
			chestGroup = 4;
		else if(getLevel() > 40)
			chestGroup = 3;
		else if(getLevel() > 30)
			chestGroup = 2;
		else
			chestGroup = 1;

		// TODO Вынести шансы в конфиг
		switch (chestGroup)
		{
			case 1:
				chestTrapLimit = 10;
				break;
			case 2:
				chestTrapLimit = 30;
				break;
			case 3:
				chestTrapLimit = 50;
				break;
			case 4:
				chestTrapLimit = 80;
				break;
		}

		return chestTrapLimit;
	}

	@Override
	public void reduceCurrentHp(final double damage, final L2Character attacker, L2Skill skill, final boolean awake, final boolean standUp, boolean directHp, boolean canReflect, boolean isCounteAttack)
	{
		if(_fake)
			super.reduceCurrentHp(damage, attacker, skill, awake, standUp, directHp, canReflect, isCounteAttack);
		else
			doDie(attacker);
	}

	public boolean isFake()
	{
		return _fake;
	}

	@Override
	public boolean canChampion()
	{
		return false;
	}
}
