package l2n.game.model.instances;

import l2n.Config;
import l2n.commons.captcha.CaptchaValidator;
import l2n.commons.list.GArray;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.model.AdditionalAIParams.AiOptionsType;
import l2n.game.cache.Msg;
import l2n.game.custom.TheGhostEngine;
import l2n.game.instancemanager.CursedWeaponsManager;
import l2n.game.instancemanager.PcCafePointsManager;
import l2n.game.model.L2Manor;
import l2n.game.model.L2ObjectTasks;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Party;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Experience;
import l2n.game.model.base.ItemToDrop;
import l2n.game.model.drop.L2Drop;
import l2n.game.model.drop.L2DropData;
import l2n.game.model.drop.L2DropGroup;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestEventType;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.SocialAction;
import l2n.game.network.serverpackets.SpawnEmitter;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Stats;
import l2n.game.skills.funcs.Func;
import l2n.game.skills.funcs.FuncMul;
import l2n.game.tables.GmListTable;
import l2n.game.tables.ItemTable;
import l2n.game.tables.SoulCrysalAbsorbTable;
import l2n.game.tables.SoulCrysalAbsorbTable.*;
import l2n.game.templates.L2Item;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import static l2n.game.tables.SoulCrysalAbsorbTable.*;

/**
 * This class manages all Monsters. L2MonsterInstance :<BR>
 * <BR>
 * <li>L2MinionInstance</li> <li>L2RaidBossInstance</li>
 */
public class L2MonsterInstance extends L2NpcInstance
{
	protected final static Logger _log = Logger.getLogger(L2MonsterInstance.class.getName());

	protected static final class RewardInfo
	{
		protected L2Character _attacker;
		protected int _dmg = 0;

		public RewardInfo(final L2Character attacker, final int dmg)
		{
			_attacker = attacker;
			_dmg = dmg;
		}

		public void addDamage(int dmg)
		{
			if(dmg < 0)
				dmg = 0;

			_dmg += dmg;
		}

		@Override
		public int hashCode()
		{
			return _attacker.getObjectId();
		}
	}

	private boolean _dead = false;
	private boolean _dying = false;
	private final ReentrantLock dieLock = new ReentrantLock(), dyingLock = new ReentrantLock(), sweepLock = new ReentrantLock(), harvestLock = new ReentrantLock();

	private static final Func[] WEAK_CHAMPION = new Func[] {
			//
			new FuncMul(Stats.MAX_HP, 0x40, null, Config.CHAMPION_WEAK_MODIFIER[0]), // модификатор ХП
			new FuncMul(Stats.MAX_MP, 0x40, null, Config.CHAMPION_WEAK_MODIFIER[1]), // модификатор МП
			new FuncMul(Stats.POWER_ATTACK, 0x40, null, Config.CHAMPION_WEAK_MODIFIER[2]), // модификатор Физ атаки
			new FuncMul(Stats.MAGIC_ATTACK, 0x40, null, Config.CHAMPION_WEAK_MODIFIER[3]), // модификатор Маг атаки
			new FuncMul(Stats.EXP, 0x40, null, Config.CHAMPION_WEAK_MODIFIER[4]), // модификатор получаемого опыта
			new FuncMul(Stats.SP, 0x40, null, Config.CHAMPION_WEAK_MODIFIER[5]), // модификатор SP
			new FuncMul(Stats.DROP, 0x40, null, Config.CHAMPION_WEAK_MODIFIER[6]), // модификатор дропа
	};

	private static final Func[] STRONG_CHAMPION = new Func[] {
			//
			new FuncMul(Stats.MAX_HP, 0x40, null, Config.CHAMPION_STRONG_MODIFIER[0]), // модификатор ХП
			new FuncMul(Stats.MAX_MP, 0x40, null, Config.CHAMPION_STRONG_MODIFIER[1]), // модификатор МП
			new FuncMul(Stats.POWER_ATTACK, 0x40, null, Config.CHAMPION_STRONG_MODIFIER[2]), // модификатор Физ атаки
			new FuncMul(Stats.MAGIC_ATTACK, 0x40, null, Config.CHAMPION_STRONG_MODIFIER[3]), // модификатор Маг атаки
			new FuncMul(Stats.EXP, 0x40, null, Config.CHAMPION_STRONG_MODIFIER[4]), // модификатор получаемого опыта
			new FuncMul(Stats.SP, 0x40, null, Config.CHAMPION_STRONG_MODIFIER[5]), // модификатор SP
			new FuncMul(Stats.DROP, 0x40, null, Config.CHAMPION_STRONG_MODIFIER[6]), // модификатор дропа
	};

	/** Stores the extra (over-hit) damage done to the L2NpcInstance when the attacker uses an over-hit enabled skill */
	private double _overhitDamage;

	/** Stores the attacker who used the over-hit enabled skill on the L2NpcInstance */
	private long overhitAttackerStoreId;

	protected MinionList minionList;
	private ScheduledFuture<MinionMaintainTask> minionMaintainTask;

	private static final int MONSTER_MAINTENANCE_INTERVAL = 1000;

	/** Максимальный уровень мобов */
	private static final int MONSTER_MAX_LEVEL = 200;

	// For ALT_GAME_MATHERIALSDROP
	protected static final L2DropData[] _matdrop = new L2DropData[]
	{
			// Item Price Chance
			new L2DropData(1864, 1, 1, 50000, 1), // Stem 100 5%
			new L2DropData(1865, 1, 1, 25000, 1), // Varnish 200 2.5%
			new L2DropData(1866, 1, 1, 16666, 1), // Suede 300 1.6666%
			new L2DropData(1867, 1, 1, 33333, 1), // Animal Skin 150 3.3333%
			new L2DropData(1868, 1, 1, 50000, 1), // Thread 100 5%
			new L2DropData(1869, 1, 1, 25000, 1), // Iron Ore 200 2.5%
			new L2DropData(1870, 1, 1, 25000, 1), // Coal 200 2.5%
			new L2DropData(1871, 1, 1, 25000, 1), // Charcoal 200 2.5%
			new L2DropData(1872, 1, 1, 50000, 1), // Animal Bone 150 5%
			new L2DropData(1873, 1, 1, 10000, 1), // Silver Nugget 500 1%
			new L2DropData(1874, 1, 1, 1666, 20), // Oriharukon Ore 3000 0.1666%
			new L2DropData(1875, 1, 1, 1666, 20), // Stone of Purity 3000 0.1666%
			new L2DropData(1876, 1, 1, 5000, 20), // Mithril Ore 1000 0.5%
			new L2DropData(1877, 1, 1, 1000, 20), // Adamantite Nugget 5000 0.1%
			new L2DropData(4039, 1, 1, 833, 40), // Mold Glue 6000 0.0833%
			new L2DropData(4040, 1, 1, 500, 40), // Mold Lubricant 10000 0.05%
			new L2DropData(4041, 1, 1, 217, 40), // Mold Hardener 23000 0.0217%
			new L2DropData(4042, 1, 1, 417, 40), // Enria 12000 0.0417%
			new L2DropData(4043, 1, 1, 833, 40), // Asofe 6000 0.0833%
			new L2DropData(4044, 1, 1, 833, 40) // Thons 6000 0.0833%
	};

	protected static final GArray<L2DropGroup> _herbs = new GArray<L2DropGroup>(11);
	static
	{
		L2DropGroup d = new L2DropGroup(0);
		d.addDropItem(new L2DropData(8600, 1, 1, 150000, 1)); // of Life 15%
		d.addDropItem(new L2DropData(8601, 1, 1, 60000, 1)); // Greater of Life 5%
		d.addDropItem(new L2DropData(8602, 1, 1, 16000, 1)); // Superior of Life 1.6%
		_herbs.add(d);
		d = new L2DropGroup(0);
		d.addDropItem(new L2DropData(8603, 1, 1, 120000, 1)); // of Mana 15%
		d.addDropItem(new L2DropData(8604, 1, 1, 40000, 1)); // Greater of Mana 5%
		d.addDropItem(new L2DropData(8605, 1, 1, 12000, 1)); // Superior of Mana 1.6%
		_herbs.add(d);
		d = new L2DropGroup(0);
		d.addDropItem(new L2DropData(8614, 1, 1, 3000, 1)); // of Recovery 0.3%
		_herbs.add(d);
		d = new L2DropGroup(0);
		d.addDropItem(new L2DropData(8611, 1, 1, 50000, 1)); // of Speed 5%
		_herbs.add(d);
		d = new L2DropGroup(0);
		d.addDropItem(new L2DropData(8606, 1, 1, 50000, 1)); // of Power 5%
		d.addDropItem(new L2DropData(8608, 1, 1, 50000, 1)); // of Atk. Spd. 5%
		_herbs.add(d);
		d = new L2DropGroup(0);
		d.addDropItem(new L2DropData(8610, 1, 1, 50000, 1)); // of Critical Attack 5%
		d.addDropItem(new L2DropData(10656, 1, 1, 50000, 1)); // of Critical Attack - Power 5%
		_herbs.add(d);
		d = new L2DropGroup(0);
		d.addDropItem(new L2DropData(10655, 1, 1, 50000, 1)); // of Life Force Absorption 5%
		d.addDropItem(new L2DropData(8612, 1, 1, 10000, 1)); // of Warrior 1%
		_herbs.add(d);
		d = new L2DropGroup(0);
		d.addDropItem(new L2DropData(8607, 1, 1, 50000, 1)); // of Magic 5%
		_herbs.add(d);
		d = new L2DropGroup(0);
		d.addDropItem(new L2DropData(8609, 1, 1, 50000, 1)); // of Casting Speed 5%
		_herbs.add(d);
		d = new L2DropGroup(0);
		d.addDropItem(new L2DropData(8613, 1, 1, 10000, 1)); // of Mystic 1%
		_herbs.add(d);
		d = new L2DropGroup(0);
		d.addDropItem(new L2DropData(10657, 1, 1, 3000, 1)); // of Doubt 0.3%
		d.addDropItem(new L2DropData(13028, 1, 1, 2000, 1)); // of Vitality 0.2%
		_herbs.add(d);
	}

	protected static final GArray<L2DropGroup> _healherbs = new GArray<L2DropGroup>(2);
	static
	{
		L2DropGroup d = new L2DropGroup(0);
		d.addDropItem(new L2DropData(8600, 1, 1, 120000, 1)); // of Life 15%
		d.addDropItem(new L2DropData(8601, 1, 1, 40000, 1)); // Greater of Life 5%
		d.addDropItem(new L2DropData(8602, 1, 1, 12000, 1)); // Superior of Life 1.6%
		_healherbs.add(d);
		d = new L2DropGroup(0);
		d.addDropItem(new L2DropData(8603, 1, 1, 120000, 1)); // of Mana 15%
		d.addDropItem(new L2DropData(8604, 1, 1, 40000, 1)); // Greater of Mana 5%
		d.addDropItem(new L2DropData(8605, 1, 1, 12000, 1)); // Superior of Mana 1.6%
		_healherbs.add(d);
	}

	protected static final L2DropData[] _lifestones = new L2DropData[]
	{
			new L2DropData(8723, 1, 1, 200, 44, 46), // Life Stone: level 46
			new L2DropData(8724, 1, 1, 200, 47, 49), // Life Stone: level 49
			new L2DropData(8725, 1, 1, 200, 50, 52), // Life Stone: level 52
			new L2DropData(8726, 1, 1, 200, 53, 55), // Life Stone: level 55
			new L2DropData(8727, 1, 1, 200, 56, 58), // Life Stone: level 58
			new L2DropData(8728, 1, 1, 200, 59, 61), // Life Stone: level 61
			new L2DropData(8729, 1, 1, 200, 62, 66), // Life Stone: level 64
			new L2DropData(8730, 1, 1, 200, 67, 72), // Life Stone: level 67
			new L2DropData(8731, 1, 1, 200, 73, 75), // Life Stone: level 70
			new L2DropData(8732, 1, 1, 200, 76, 79), // Life Stone: level 76
			new L2DropData(9573, 1, 1, 200, 80, 81), // Life Stone: level 80
			new L2DropData(10483, 1, 1, 200, 82, MONSTER_MAX_LEVEL), // Mid-Grade Life Stone: level 82
			new L2DropData(8733, 1, 1, 100, 44, 46), // Mid-Grade Life Stone: level 46
			new L2DropData(8734, 1, 1, 100, 47, 49), // Mid-Grade Life Stone: level 49
			new L2DropData(8735, 1, 1, 100, 50, 52), // Mid-Grade Life Stone: level 52
			new L2DropData(8736, 1, 1, 100, 53, 55), // Mid-Grade Life Stone: level 55
			new L2DropData(8737, 1, 1, 100, 56, 58), // Mid-Grade Life Stone: level 58
			new L2DropData(8738, 1, 1, 100, 59, 61), // Mid-Grade Life Stone: level 61
			new L2DropData(8739, 1, 1, 100, 62, 66), // Mid-Grade Life Stone: level 64
			new L2DropData(8740, 1, 1, 100, 67, 72), // Mid-Grade Life Stone: level 67
			new L2DropData(8741, 1, 1, 100, 73, 75), // Mid-Grade Life Stone: level 70
			new L2DropData(8742, 1, 1, 100, 76, 79), // Mid-Grade Life Stone: level 76
			new L2DropData(9574, 1, 1, 100, 80, 81), // Mid-Grade Life Stone: level 80
			new L2DropData(10484, 1, 1, 100, 82, MONSTER_MAX_LEVEL), // Mid-Grade Life Stone: level 82
			new L2DropData(8743, 1, 1, 30, 44, 46), // High-Grade Life Stone: level 46
			new L2DropData(8744, 1, 1, 30, 47, 49), // High-Grade Life Stone: level 49
			new L2DropData(8745, 1, 1, 30, 50, 52), // High-Grade Life Stone: level 52
			new L2DropData(8746, 1, 1, 30, 53, 55), // High-Grade Life Stone: level 55
			new L2DropData(8747, 1, 1, 30, 56, 58), // High-Grade Life Stone: level 58
			new L2DropData(8748, 1, 1, 30, 59, 61), // High-Grade Life Stone: level 61
			new L2DropData(8749, 1, 1, 30, 62, 66), // High-Grade Life Stone: level 64
			new L2DropData(8750, 1, 1, 30, 67, 72), // High-Grade Life Stone: level 67
			new L2DropData(8751, 1, 1, 30, 73, 75), // High-Grade Life Stone: level 70
			new L2DropData(8752, 1, 1, 30, 76, 79), // High-Grade Life Stone: level 76
			new L2DropData(9575, 1, 1, 30, 80, 81), // High-Grade Life Stone: level 80
			new L2DropData(10485, 1, 1, 30, 82, MONSTER_MAX_LEVEL) // High-Grade Life Stone: level 82
	};

	protected static final L2DropData[] _toplifestones = new L2DropData[]
	{
			new L2DropData(8753, 1, 1, 100000, 44, 46), // Top-Grade Life Stone: level 46
			new L2DropData(8754, 1, 1, 100000, 47, 49), // Top-Grade Life Stone: level 49
			new L2DropData(8755, 1, 1, 100000, 50, 52), // Top-Grade Life Stone: level 52
			new L2DropData(8756, 1, 1, 100000, 53, 55), // Top-Grade Life Stone: level 55
			new L2DropData(8757, 1, 1, 100000, 56, 58), // Top-Grade Life Stone: level 58
			new L2DropData(8758, 1, 1, 100000, 59, 61), // Top-Grade Life Stone: level 61
			new L2DropData(8759, 1, 1, 100000, 62, 66), // Top-Grade Life Stone: level 64
			new L2DropData(8760, 1, 1, 100000, 67, 72), // Top-Grade Life Stone: level 67
			new L2DropData(8761, 1, 1, 100000, 73, 75), // Top-Grade Life Stone: level 70
			new L2DropData(8762, 1, 1, 100000, 76, 79), // Top-Grade Life Stone: level 76
			new L2DropData(9576, 1, 1, 100000, 80, 81), // Top-Grade Life Stone: level 80
			new L2DropData(10486, 1, 1, 100000, 82, MONSTER_MAX_LEVEL) // Top-Grade Life Stone: level 82
	};

	protected static final L2DropData[] _raiditems = new L2DropData[]
	{
			//
			new L2DropData(9814, 1, 2, 300000, 40, 74), // Memento Mori
			new L2DropData(9815, 1, 2, 300000, 40, 70), // Dragon Heart
			new L2DropData(9816, 1, 2, 300000, 40, 74), // Earth Egg
			new L2DropData(9817, 1, 2, 300000, 40, 74), // Nonliving Nucleus
			new L2DropData(9818, 1, 2, 300000, 40, 70), // Angelic Essence
			new L2DropData(8176, 1, 2, 300000, 40, 74) // Destruction Tombstone
	};

	/** crops */
	private L2ItemInstance _harvestItem;
	private L2Item _seeded;
	private long seederStoreId, spoilerStoreId;

	/** The table containing all L2Player that successfully absorbed the soul of this L2NpcInstance */
	private GArray<Integer> _absorbersList;

	/** Table containing all Items that a Dwarf can Sweep on this L2NpcInstance */
	private L2ItemInstance[] _sweepItems;

	// для чампов
	private int _isChampion;
	private int _team = -1;

	private final boolean _isLethalImmune;

	/**
	 * Constructor of L2MonsterInstance (use L2Character and L2NpcInstance constructor).<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Call the L2Character constructor to set the _template of the L2MonsterInstance (copy skills from template to object and link _calculators to NPC_STD_CALCULATOR)</li> <li>Set the name of the L2MonsterInstance</li> <li>Create a
	 * RandomAnimation Task that will be launched after the calculated delay if the server allow it</li><BR>
	 * <BR>
	 * 
	 * @param objectId
	 *            Identifier of the object to initialized
	 * @param template
	 *            to apply to the NPC
	 */
	public L2MonsterInstance(final int objectId, final L2NpcTemplate template)
	{
		super(objectId, template);
		_isLethalImmune = template.getAIParams().getBool(AiOptionsType.IS_LETHAL_IMMUNE, false);
	}

	@Override
	public boolean isMovementDisabled()
	{
		// Невозможность ходить для этих мобов
		return getNpcId() == 18344 || getNpcId() == 18345 || super.isMovementDisabled();
	}

	@Override
	public boolean isLethalImmune()
	{
		return _isChampion > 0 && Config.CHAMPION_LETHAL_IMMUNE || _isLethalImmune || super.isLethalImmune();
	}

	@Override
	public boolean isFearImmune()
	{
		return _isChampion > 0 && Config.CHAMPION_FEAR_IMMUNE || super.isFearImmune();
	}

	@Override
	public boolean isParalyzeImmune()
	{
		return _isChampion > 0 && Config.CHAMPION_PARALYZE_IMMUNE || super.isParalyzeImmune();
	}

	/**
	 * Return True if the attacker is not another L2MonsterInstance.<BR>
	 * <BR>
	 */
	@Override
	public boolean isAutoAttackable(final L2Character attacker)
	{
		return !attacker.isMonster();
	}

	/**
	 * @return level 1 - слабый чемпиона, 2 - сильный чемпион, 0 - не чемпион вовсе
	 */
	@Override
	public int getChampion()
	{
		return _isChampion;
	}

	/**
	 * @param level
	 *            1 - слабый чемпиона, 2 - сильный чемпион
	 */
	public void setChampion(final int level)
	{
		if(level == 0)
		{
			// удаляем статы чемпиона
			switch (_isChampion)
			{
				case 1:
					removeStatFuncs(WEAK_CHAMPION);
					break;
				case 2:
					removeStatFuncs(STRONG_CHAMPION);
					break;
			}
			_isChampion = 0;
		}
		else
		{
			// добавляем статы чемпиона
			switch (level)
			{
				case 1:
					addStatFuncs(WEAK_CHAMPION);
					break;
				case 2:
					addStatFuncs(STRONG_CHAMPION);
					break;
			}
			_isChampion = level;
			setCurrentHp(getMaxHp(), false);
		}
	}

	/**
	 * @return true если может быть чемпионом
	 */
	public boolean canChampion()
	{
		return getTemplate().revardExp > 0 && getLevel() >= Config.CHAMPION_MIN_LEVEL && getLevel() <= Config.CHAMPION_MAX_LEVEL;
	}

	/**
	 * Определение команды (нужно для подсветки чампов)
	 */
	@Override
	public int getTeam()
	{
		return _team < 0 ? getChampion() : _team;
	}

	@Override
	public void setTeam(final int team, final boolean checksForTeam)
	{
		_team = team;
		broadcastCharInfo();
	}

	/**
	 * Очищает флаги состояний смерти. Дает или очищает статус чемпиона. Восстанавливает HP. Спавнит миньонов. Отключает аггр на 10 секунд.
	 */
	@Override
	public void onSpawn()
	{
		_dead = false;
		_dying = false;
		setChampion(0);

		if(getReflection().canChampions() && canChampion())
		{
			final double random = Rnd.nextDouble();
			if(Config.CHAMPION_STRONG_CHANCE / 100.0 >= random)
				setChampion(2);
			else if(Config.CHAMPION_WEAK_CHANCE / 100. >= random)
				setChampion(1);
		}

		setCurrentHpMp(getMaxHp(), getMaxMp(), true);
		super.onSpawn();
		spawnMinions();
		getAI().setGlobalAggro(System.currentTimeMillis() + 10000);

		// Clear mob spoil, absorbs, seed
		setSpoiled(false, null);
		_sweepItems = null;
		_absorbersList = null;
		_seeded = null;
		seederStoreId = 0;
		spoilerStoreId = 0;

		// судя по всему, УД мобы юзают только в соло локах, точного списка не, поэтому разрешим только мобам, которые дропают хербы
		if(Config.ALT_ALLOW_MOBS_USE_UD)
			if(!isRaid() && !isChest() && ((L2NpcTemplate) _template).drop_herbs != 0)
				getAI().setCanUseUD(true);
	}

	@Override
	protected void onDespawn()
	{
		setOverhitDamage(0);
		setOverhitAttacker(null);
		clearSweep();
		clearHarvest();

		if(_absorbersList != null)
			_absorbersList.clear();

		super.onDespawn();
	}

	protected int getMaintenanceInterval()
	{
		return MONSTER_MAINTENANCE_INTERVAL;
	}

	public void spawnMinions()
	{
		if(getTemplate().getMinionData().size() > 0)
		{
			if(minionMaintainTask != null)
			{
				minionMaintainTask.cancel(true);
				minionMaintainTask = null;
			}
			minionMaintainTask = L2GameThreadPools.getInstance().scheduleAi(new MinionMaintainTask(), getMaintenanceInterval(), false);
		}
	}

	public Location getMinionPosition()
	{
		return Location.getAroundPosition(this, this, 100, 150, 10);
	}

	@Override
	public void callMinionsToAssist(final L2Character attacker)
	{
		if(hasMinions())
			for(final L2MinionInstance minion : minionList.getSpawnedMinions())
				if(minion != null && minion.getAI().getIntention() != CtrlIntention.AI_INTENTION_ATTACK && !minion.isDead())
					minion.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, Rnd.get(1, 100));
	}

	public void setDead(final boolean dead)
	{
		_dead = dead;
	}

	public void removeMinions()
	{
		if(minionMaintainTask != null)
		{
			minionMaintainTask.cancel(true);
			minionMaintainTask = null;
		}

		if(minionList != null)
			minionList.maintainLonelyMinions();
		minionList = null;
	}

	public int getTotalSpawnedMinionsInstances()
	{
		return minionList == null ? 0 : minionList.countSpawnedMinions();
	}

	public int getTotalSpawnedMinionsGroups()
	{
		return minionList.lazyCountSpawnedMinionsGroups();
	}

	public void notifyMinionDied(final L2MinionInstance minion)
	{
		if(minionList != null)
			minionList.removeSpawnedMinion(minion);
	}

	public void notifyMinionSpawned(final L2MinionInstance minion)
	{
		minionList.addSpawnedMinion(minion);
	}

	@Override
	public boolean hasMinions()
	{
		return minionList != null && minionList.hasMinions();
	}

	public MinionList getMinionList()
	{
		return minionList;
	}

	public void setNewMinionList()
	{
		minionList = new MinionList(this);
	}

	@Override
	public void setReflection(final long i)
	{
		super.setReflection(i);

		if(hasMinions())
			for(final L2MinionInstance m : minionList.getSpawnedMinions())
				m.setReflection(i);
	}

	@Override
	public void deleteMe()
	{
		if(hasMinions())
			removeMinions();
		super.deleteMe();
	}

	@Override
	public void doDie(final L2Character killer)
	{
		if(minionMaintainTask != null)
		{
			minionMaintainTask.cancel(true);
			minionMaintainTask = null;
		}

		if(_dead)
			return;

		dieLock.lock();
		try
		{
			if(_dead)
				return;

			_dieTime = System.currentTimeMillis();
			_dead = true;

			if(isChest() && !((L2ChestInstance) this).isFake())
			{
				super.doDie(killer);
				return;
			}

			// Distribute Exp and SP rewards to L2Player (including Summon owner) that hit the L2NpcInstance and to their Party members
			try
			{
				dyingLock.lock();
				_dying = true;
				calculateRewards(killer);
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "", e);
			}
			finally
			{
				_dying = false;
				dyingLock.unlock();
			}
		}
		finally
		{
			dieLock.unlock();
		}

		if(killer != null && killer.isPlayer())
		{
			final L2Player activeChar = killer.getPlayer();
			 if(Config.CAPTCHA_ANTIBOT_ENABLE && !activeChar.isFakePlayer())
	            {
					for(final int id : Config.CAPTCHA_ANTIBOT_LIST)
						if(this.getNpcId() == id)
						{
							activeChar.setCaptchaInt(activeChar.getCaptchaInt() + 1);
							int lvlCaptcha = 0;
							int yourLvl = activeChar.getLevel();
							if(yourLvl <= 40)
							{
								lvlCaptcha = Config.CAPTCHA_ANTIBOT_COUNT_40;
							}
							else if(yourLvl > 40 && yourLvl <= 60)
							{
								lvlCaptcha = Config.CAPTCHA_ANTIBOT_COUNT_60;
							}
							else if(yourLvl > 60 && yourLvl <= 75)
							{
								lvlCaptcha = Config.CAPTCHA_ANTIBOT_COUNT_75;
							}
							else if(yourLvl > 75 && yourLvl <= 79)
							{
								lvlCaptcha = Config.CAPTCHA_ANTIBOT_COUNT_79;
							}
							else if(yourLvl > 79 && yourLvl <= 83)
							{
								lvlCaptcha = Config.CAPTCHA_ANTIBOT_COUNT_79;
							}
							else if(yourLvl > 83)
							{
								lvlCaptcha = Config.CAPTCHA_ANTIBOT_COUNT_85;
							}


							if(activeChar.getCaptchaInt() >= lvlCaptcha)
							{
								CaptchaValidator.getInstance().sendCaptcha(activeChar);
								activeChar.setCaptchaInt(0);
							}
							break;
						}
	            }
			if(Config.GHOST_ENGINE_ANTIBOT_ALLOW_BAN)
			{
				final int chancesForPacketsRain = 5; // 5%
				// If they kill a monster and didn't kill one over the past 2minutes, send them AntiH4X Packets...
				// It'll put them on wrong trace if they attempt to find out how we get bots...
				// It has no effects.
				if(activeChar.getLastMonsterKilledTime() == 0 || Rnd.chance(chancesForPacketsRain))
					TheGhostEngine.onFloodSuperUberAntiHacksPackets(activeChar, Rnd.get(30000, 60000));

				if(!isFlying() && !activeChar.isFlying() && !activeChar.isCaughtByRabbits())
				{
					final long lastCheck = activeChar.getLastBotTrackerCheck();
					// уже проверяли не давно
					if(lastCheck != 0 && lastCheck > System.currentTimeMillis() - Config.GHOST_ENGINE_MINIMUM_INTERVAL_BETWEEN_FAKE_CHECKS)
					{
						if(Config.GHOST_ENGINE_DEVELOPMENT_MODE)
							GmListTable.broadcastMessageToGMs("TheGhostEngine: " + activeChar.getName() + " has already been checked recently. Skipping.");
					}
					else
					{
						// 0.1% Chance to spawn a monster if level < MINIMUM_LEVEL_TO_CHECK_OFTEN. 5% Otherwise.
						int CHANCES_TO_SPAWN_FAKE_MONSTER = activeChar.getLevel();

						if(!isMonster())
						{
							CHANCES_TO_SPAWN_FAKE_MONSTER = 0;
							if(Config.GHOST_ENGINE_DEVELOPMENT_MODE)
								GmListTable.broadcastMessageToGMs("TheGhostEngine: " + activeChar.getName() + " killed monster type " + getTemplate().type + ". We dont spawn rabbits for this type");
						}
						else if(CHANCES_TO_SPAWN_FAKE_MONSTER < Config.GHOST_ENGINE_MINIMUM_LEVEL_TO_CHECK_OFTEN)
						{
							CHANCES_TO_SPAWN_FAKE_MONSTER = 1;

							if(Config.GHOST_ENGINE_DEVELOPMENT_MODE)
								GmListTable.broadcastMessageToGMs("TheGhostEngine: " + activeChar.getName() + " is low level, low chances to clone.");
						}
						else
						{
							CHANCES_TO_SPAWN_FAKE_MONSTER = 50;

							if(Config.GHOST_ENGINE_DEVELOPMENT_MODE)
								GmListTable.broadcastMessageToGMs("TheGhostEngine: " + activeChar.getName() + " is high level, higher chances to clone.");
						}

						if(CHANCES_TO_SPAWN_FAKE_MONSTER > Rnd.get(1000))
							TheGhostEngine.onSpawnFakeMonsterForPlayer(activeChar, getTemplate());
					}
				}
				else if(Config.GHOST_ENGINE_DEVELOPMENT_MODE)
					GmListTable.broadcastMessageToGMs("TheGhostEngine: " + activeChar.getName() + " is flying or already banned, no clone for him.");
			}

			activeChar.setLastMonsterKilledTime(System.currentTimeMillis());
		}

		super.doDie(killer);
	}

	protected void calculateRewards(L2Character lastAttacker)
	{
		final HashMap<L2Playable, AggroInfo> aggroList = getAggroMap();
		L2Character topDamager = getTopDamager(aggroList.values());
		if(lastAttacker == null || !lastAttacker.isPlayable())
			lastAttacker = topDamager;

		if(lastAttacker == null || !lastAttacker.isPlayable())
			return;

		// Get the L2Player that killed the L2NpcInstance
		final L2Player killer = lastAttacker.getPlayer();
		if(killer == null)
			return;

		if(topDamager == null)
			topDamager = lastAttacker;

		if(Config.KILL_COUNTER)
		{
			killer.incrementKillsCounter(getNpcId());
			getTemplate().killscount++;
		}

		// Notify the Quest Engine of the L2NpcInstance death if necessary
		try
		{
			final Quest[] list = getTemplate().getEventQuests(QuestEventType.MOBKILLED);
			if(list != null && list.length != 0)
			{
				// массив с игроками, которые могут быть заинтересованы в квестах
				GArray<L2Player> players = null;
				if(isRaid() && Config.ALT_NO_LASTHIT) // обработка при убийстве рб, берутся игроки из хейт листа
				{
					players = new GArray<L2Player>();
					for(final L2Playable pl : aggroList.keySet())
						if(pl.isPlayer() && !pl.isDead() && pl.getReflectionId() == getReflectionId() && (pl.isInRange(this, Config.ALT_PARTY_DISTRIBUTION_RANGE) || pl.isInRange(killer, Config.ALT_PARTY_DISTRIBUTION_RANGE)) && Math.abs(pl.getZ() - getZ()) < 400)
							players.add((L2Player) pl);
				}
				else if(killer.getParty() != null) // обробатываем пати, берутся члены пати
				{
					players = new GArray<L2Player>(killer.getParty().getMemberCount());
					for(final L2Player pl : killer.getParty().getPartyMembers())
						if(!pl.isDead() && pl.getReflectionId() == getReflectionId() && (pl.isInRange(this, Config.ALT_PARTY_DISTRIBUTION_RANGE) || pl.isInRange(killer, Config.ALT_PARTY_DISTRIBUTION_RANGE)) && Math.abs(pl.getZ() - getZ()) < 400)
							players.add(pl);
				}

				for(final Quest quest : list)
				{
					/** тот кому даём награду при смерте в квестах, по умолчанию равен тому кто убил */
					L2Player toReward = killer;

					QuestState qs = null;
					final String quest_name = quest.getName();
					// начём обработку
					switch (quest.getParty())
					{
					// награда только тому кто убил
						case Quest.PARTY_NONE:
							qs = toReward.getQuestState(quest.getName());
							if(qs != null && !qs.isCompleted())
								quest.notifyKill(this, qs); // уведомляем о смерте в квесте
							toReward = null;
							break;
						// награда выдаётся рандомному члену пати
						case Quest.PARTY_ONE:
							if(players != null)
							{
								final GArray<L2Player> interested = new GArray<L2Player>(players.size());
								for(final L2Player pl : players)
								{
									qs = pl.getQuestState(quest_name);
									if(qs != null && !qs.isCompleted())
										interested.add(pl);
								}

								if(interested.isEmpty())
									continue;

								toReward = interested.get(Rnd.get(interested.size())); // берём рандомного игрока из списка
								if(toReward == null) // если такого нет, назначем того кто убил моба
									toReward = killer;
							}
							break;
						// награда выдаётся всем членам пати
						case Quest.PARTY_ALL:
							int count = 0;
							if(players != null)
								for(final L2Player pl : players)
								{
									qs = pl.getQuestState(quest_name);
									if(qs != null && !qs.isCompleted())
									{
										quest.notifyKill(this, qs);
										count++;
									}
								}
							if(count > 0)
								toReward = null;
							break;

					}

					if(toReward == null)
						continue;

					qs = toReward.getQuestState(quest_name);
					if(qs != null && !qs.isCompleted())
						quest.notifyKill(this, qs); // уведомляем о смерте в квесте
				}

			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "L2MonsterInstance: notify the Quest Engine: ", e);
		}

		// Distribute Exp and SP rewards to L2Player (including Summon owner) that hit the L2NpcInstance and to their Party members
		final Map<L2Player, RewardInfo> rewards = new HashMap<L2Player, RewardInfo>();
		for(final AggroInfo info : aggroList.values())
		{
			if(info.damage <= 1)
				continue;

			L2Character attacker = info.attacker;
			if(attacker == null)
				continue;

			L2Player player = attacker.getPlayer();
			if(player != null)
			{
				final RewardInfo reward = rewards.get(player);
				if(reward == null)
					rewards.put(player, new RewardInfo(player, info.damage));
				else
					reward.addDamage(info.damage);
			}
		}

		if(topDamager == null || !topDamager.isPlayable())
			return;

		// Manage Base, Quests and Special Events drops of the L2NpcInstance
		doItemDrop(topDamager);

		// Manage Sweep drops of the L2NpcInstance
		if(isSpoiled())
			doSweepDrop(lastAttacker);

		// С рейдов падают только топовые лайфстоны, с миньонов вообще ничего дополнительно не падает
		if(!isRaid() && !isMinion())
		{
			final double chancemod = ((L2NpcTemplate) _template).rateHp * Experience.penaltyModifier(calculateLevelDiffForDrop(topDamager.getLevel(), false), 9.);

			// Дополнительный дроп материалов
			if(Config.ALT_GAME_MATHERIALSDROP && chancemod > 0 && (!isSeeded() || _seeded.isAltSeed()))
				for(final L2DropData d : _matdrop)
					if(getLevel() >= d.getMinLevel())
					{
						final long count = Util.rollDrop(d.getMinDrop(), d.getMaxDrop(), d.getChance() * chancemod * Config.RATE_DROP_ITEMS * killer.getRateItems(), true);
						if(count > 0)
							dropItem(killer, d.getItemId(), count);
					}

			// Хербы
			if(chancemod > 0)
				if(((L2NpcTemplate) _template).drop_herbs == 1)
					for(final L2DropGroup h : _herbs)
					{
						final List<ItemToDrop> itdl = h.rollFixedQty(0, this, killer, chancemod);
						if(itdl != null)
							for(final ItemToDrop itd : itdl)
								dropItem(killer, itd.itemId, 1);
					}
				else if(((L2NpcTemplate) _template).drop_herbs == 2)
					for(final L2DropGroup h : _healherbs)
					{
						final List<ItemToDrop> itdl = h.rollFixedQty(0, this, killer, chancemod);
						if(itdl != null)
							for(final ItemToDrop itd : itdl)
								dropItem(killer, itd.itemId, 1);
					}

			// Лайфстоуны
			if(chancemod > 0)
				for(final L2DropData l : _lifestones)
					if(getLevel() >= l.getMinLevel() && getLevel() <= l.getMaxLevel() && Rnd.get(1, L2Drop.MAX_CHANCE) <= l.getChance() * Config.RATE_DROP_ITEMS * killer.getRateItems() * chancemod)
						dropItem(killer, l.getItemId(), 1);
		}

		else if(isRaid())
		{
			// Лайфстоуны с рейдов
			for(final L2DropData l : _toplifestones)
				if(getLevel() >= l.getMinLevel() && getLevel() <= l.getMaxLevel())
				{
					final List<ItemToDrop> itd = l.roll(killer, isBoss() ? Config.ALT_DROP_LS_BOSS : Config.ALT_DROP_LS_RAID, true);
					for(final ItemToDrop t : itd)
						for(int i = 0; i < t.count; i++)
							dropItem(killer, t.itemId, 1);
				}
			// предметы для изучения клановых скилов (не падают с эпиков)
			if(!isBoss())
				for(final L2DropData l : _raiditems)
					if(getLevel() >= l.getMinLevel() && getLevel() <= l.getMaxLevel() && Rnd.get(1, L2Drop.MAX_CHANCE) <= l.getChance())
					{
						final int mod = (int) (getLevel() * Config.RATE_DROP_RAIDBOSS / 20);
						dropItem(killer, l.getItemId(), Rnd.get(l.getMinDrop() * mod, l.getMaxDrop() * mod));
					}
		}		
		L2Party attackerParty;

		final L2Player[] attackers = rewards.keySet().toArray(new L2Player[rewards.size()]);
		for(final L2Player attacker : attackers)
		{
			if(attacker == null || attacker.isDead())
				continue;

			final RewardInfo reward = rewards.get(attacker);
			if(reward == null)
				continue;

			attackerParty = attacker.getParty();
			final int maxHp = getMaxHp();

			// Если без пати (соло)
			if(attackerParty == null)
			{
				final int damage = Math.min(reward._dmg, maxHp);
				if(damage > 0)
				{
					final double[] xpsp = calculateExpAndSp(attacker, attacker.getLevel(), damage);

					// Начисление душ камаэлянам
					final double neededExp = attacker.calcStat(Stats.SOULS_CONSUME_EXP, 0, this, null);
					if(neededExp > 0 && xpsp[0] > neededExp)
					{
						broadcastPacket(new SpawnEmitter(this, attacker));
						L2GameThreadPools.getInstance().scheduleGeneral(new L2ObjectTasks.SoulConsumeTask(attacker), 1000);
					}

					// Расчитываем урон при ОверХите
					xpsp[0] = applyOverhit(killer, xpsp[0]);
					if(Config.ENABLE_VITALITY)
					{
						attacker.addVitExpAndSp((long) xpsp[0], (long) xpsp[1], this, true, true);
						attacker.calculateVitalityPoints(this, (long) xpsp[0]);
					}
					else
						attacker.addExpAndSp((long) xpsp[0], (long) xpsp[1], true, true);

					if(attacker.isPlayer()) // Выдаём PCBang очки
						PcCafePointsManager.getInstance().givePcCafePoint(attacker, (long) xpsp[0]);
				}
				rewards.remove(attacker);
			}
			else
			{
				int partyDmg = 0;
				int partylevel = 1;
				final GArray<L2Player> rewardedMembers = new GArray<L2Player>();
				for(final L2Player partyMember : attackerParty.getPartyMembers())
				{
					if(partyMember == null || partyMember.isDead())
						continue;

					final RewardInfo ai = rewards.remove(partyMember);
					if(partyMember.isDead() || !isInRangeZ(partyMember, Config.ALT_PARTY_DISTRIBUTION_RANGE))
						continue;

					if(ai != null)
						partyDmg += ai._dmg;

					rewardedMembers.add(partyMember);
					if(partyMember.getLevel() > partylevel)
						partylevel = partyMember.getLevel();
				}

				partyDmg = Math.min(partyDmg, maxHp);
				if(partyDmg > 0)
				{
					final double[] xpsp = calculateExpAndSp(attacker, partylevel, partyDmg);
					final float partyMul = (float) partyDmg / maxHp;

					xpsp[0] *= partyMul;
					xpsp[1] *= partyMul;

					// Check for an over-hit enabled strike
					// (When in party, the over-hit exp bonus is given to the whole party and splitted proportionally through the party members)
					xpsp[0] = applyOverhit(killer, xpsp[0]);
					attackerParty.distributeXpAndSp(xpsp[0], xpsp[1], rewardedMembers, lastAttacker, this, partyDmg);
				}
			}
		}

		// Check the drop of a cursed weapon
		CursedWeaponsManager.getInstance().dropAttackable(this, killer);

		try
		{
			levelSoulCrystals(killer, aggroList);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "L2MonsterInstance:levelSoulCrystals(L2Character):", e);
		}
	}

	/**
	 * Моб уже формально мертв, но его труп еще нельзя использовать поскольку не закончен подсчет наград
	 */
	public boolean isDying()
	{
		return _dying;
	}

	@Override
	public void onRandomAnimation()
	{
		// Action id для живности 1-3
		broadcastPacket(new SocialAction(getObjectId(), Rnd.get(1, 3)));
	}

	@Override
	public int getKarma()
	{
		return 0;
	}

	/**
	 * Adds an attacker that successfully absorbed the soul of this L2NpcInstance into the _absorbersList.<BR>
	 * <BR>
	 * params: attacker - a valid L2Player condition - an integer indicating the event when mob dies. This should be:<BR>
	 * = 0 - "the crystal scatters";<BR>
	 * = 1 - "the crystal failed to absorb. nothing happens";<BR>
	 * = 2 - "the crystal resonates because you got more than 1 crystal on you";<BR>
	 * = 3 - "the crystal cannot absorb the soul because the mob level is too low";<BR>
	 * = 4 - "the crystal successfuly absorbed the soul";
	 */
	public void addAbsorber(final L2Player attacker)
	{
		if(attacker == null)
			return;

		if(SoulCrysalAbsorbTable.getMaxLevelCrystal(getNpcId()) == 0 || getCurrentHpPercents() > 50)
			return;

		if(_absorbersList == null)
			_absorbersList = new GArray<Integer>();
		if(!_absorbersList.contains(attacker.getObjectId()))
			_absorbersList.add(attacker.getObjectId());
	}

	/**
	 * Calculate the leveling chance of Soul Crystals based on the attacker that killed this L2NpcInstance
	 * 
	 * @param attacker
	 *            - игрок который последним ударил (убил) монстра
	 * @param aggroList
	 *            - агролист
	 */
	private void levelSoulCrystals(final L2Character attacker, final HashMap<L2Playable, AggroInfo> aggroList)
	{
		// Only L2Player can absorb a soul
		if(attacker == null || !attacker.isPlayable())
		{
			_absorbersList = null;
			return;
		}

		final SoulCrystal soulCrystal = SoulCrysalAbsorbTable.get(getNpcId());
		if(soulCrystal == null)
		{
			_absorbersList = null;
			return;
		}

		boolean levelPartyCrystals = false;
		final L2Player killer = attacker.getPlayer();

		// минимальный необходимый уровень кристала для повышения
		final int minCrystalLevel = soulCrystal.getMinLevelCrystal();
		// максимальный уровень до которого можно повысить
		final int maxCrystalLevel = soulCrystal.getMaxLevelCrystal();
		// тип поглащение душ)
		final AbsorbCrystalType absorbType = soulCrystal.getAbsorbType();
		// шанс
		final double chance = LEVEL_CHANCE > 0 ? LEVEL_CHANCE : soulCrystal.getChance();

		if(absorbType == AbsorbCrystalType.FULL_PARTY || absorbType == AbsorbCrystalType.PARTY_ONE_RANDOM)
			levelPartyCrystals = true;
		// иначе проверяем юзал ли игрок сам кристал, у пати типов юзать не обязательно
		else if(maxCrystalLevel == 0 || _absorbersList == null || !_absorbersList.contains(killer.getObjectId()))
		{
			_absorbersList = null;
			return;
		}

		_absorbersList = null;

		GArray<L2Player> players = null;
		switch (absorbType)
		{
			case LAST_HIT:
			{
				players = new GArray<L2Player>(1);
				players.add(killer);
				break;
			}
			case PARTY_ONE_RANDOM:
			{
				players = new GArray<L2Player>(1);
				if(killer.isInParty())
					players.add(killer.getParty().getRandomMember());
				else
					players.add(killer);
				break;
			}
			case FULL_PARTY:
			{
				if(killer.isInParty())
				{
					if(getNpcId() != 25338)
						players = killer.getParty().getPartyMembers();
					else
					{
						// between 1 and at least 4 party members can get their Soul Crystal leveled
						int count = 0;
						players = new GArray<L2Player>(4);
						for(final L2Player pm : killer.getParty().getPartyMembers())
						{
							if(count >= 4)
								break;
							players.add(pm);
							count++;
						}
					}
				}
				else
				{
					players = new GArray<L2Player>(1);
					players.add(killer);
				}
				break;
			}
			default:
			{
				_absorbersList = null;
				return;
			}
		}

		for(final L2Player player : players)
		{
			// Для прокачки кристалла обязательно должен быть взят квест _350_EnhanceYourWeapon
			if(player.getQuestState("_350_EnhanceYourWeapon") == null)
				continue;

			if(!player.isInRange(this, Config.ALT_PARTY_DISTRIBUTION_RANGE) && !player.isInRange(killer, Config.ALT_PARTY_DISTRIBUTION_RANGE))
				continue;

			int oldCrystalId = 0;
			int newCrystalId = 0;
			int crystalsCount = 0;
			int crystalLevel = 0;
			boolean canIncreaseCrystal = false;
			boolean resonated = false;

			// Check how many soul crystals the player has in his inventory and which soul crystal he has
			for(final L2ItemInstance item : player.getInventory().getItems())
			{
				final int itemId = item.getItemId();
				// Find any of the 39 possible crystals.
				if(!isSoulCrystal(itemId))
					continue;

				// Проверяем чтоб у игрока был только один кристал в инвентаре
				if(++crystalsCount > 1)
				{
					// нашли больше 1, фейл!
					resonated = true;
					break;
				}

				if(crystalsCount < 1)
					continue;
				if((newCrystalId = getNextLevelCrystalId(itemId)) != 0)
				{
					crystalLevel = getCrystalLevel(itemId);
					// если уровень кристала больше или равен минимальному требуемому и меньше максимально возможного
					canIncreaseCrystal = crystalLevel >= minCrystalLevel && crystalLevel < maxCrystalLevel;
					oldCrystalId = itemId;
				}
			}

			// The player has more than one soul crystal with him, the crystal resonates and we skip this player
			if(resonated)
			{
				if(!levelPartyCrystals)
					player.sendPacket(Msg.THE_SOUL_CRYSTALS_CAUSED_RESONATION_AND_FAILED_AT_ABSORBING_A_SOUL);
				continue;
			}

			// The soul crystal stage of the player is way too high, refuse to increase it
			if(!canIncreaseCrystal)
			{
				if(!levelPartyCrystals)
					player.sendPacket(Msg.THE_SOUL_CRYSTAL_IS_REFUSING_TO_ABSORB_A_SOUL);
				continue;
			}

			// If the killer succeeds or it is a boss mob, level up the crystal
			if(levelPartyCrystals && canIncreaseCrystal || Rnd.chance(chance))
			{
				final L2ItemInstance oldCrystal = player.getInventory().getItemByItemId(oldCrystalId);
				if(oldCrystal == null)
					continue;

				// на рейд-боссе Baylor (из Crystal Caverns), то есть шанс, что Soul Crystal превратятся в Cursed Soul Crystal
				if((getNpcId() == 29099 || getNpcId() == 29103) && Rnd.chance(50))
					newCrystalId = getCursedCrystal(newCrystalId);

				player.sendPacket(SystemMessage.removeItems(oldCrystal.getItemId(), 1), Msg.THE_SOUL_CRYSTAL_SUCCEEDED_IN_ABSORBING_A_SOUL, SystemMessage.obtainItems(newCrystalId, 1, 0));
				player.getInventory().destroyItem(oldCrystal, 1, true);
				player.getInventory().addItem(ItemTable.getInstance().createItem(newCrystalId, player.getObjectId(), getNpcId(), "L2NpcInstance.levelSoulCrystals"));

				final String newCrystalColor = getCrystalColor(newCrystalId);
				if(newCrystalColor != null)
				{
					final int newCrystalLvl = getCrystalLevel(newCrystalId);
					final CustomMessage cm = new CustomMessage("l2n.game.model.instances.L2MonsterInstance.levelSoulCrystals", player);
					cm.addCharName(player).addString(newCrystalColor).addNumber(newCrystalLvl);
					player.broadcastPacketToOthers(new SystemMessage(cm));
				}

				continue;
			}

			player.sendPacket(Msg.THE_SOUL_CRYSTAL_IS_REFUSING_TO_ABSORB_A_SOUL);
		}
	}

	/**
	 * @param crystalId
	 *            - itemId кристала
	 * @return цвет кристала
	 */
	private static String getCrystalColor(final int crystalId)
	{
		if(ArrayUtil.arrayContains(REDCRYSTALS, crystalId))
			return "red";
		if(ArrayUtil.arrayContains(BLUECRYSTALS, crystalId))
			return "blue";
		if(ArrayUtil.arrayContains(GREENCRYSTALS, crystalId))
			return "green";
		switch (crystalId)
		{
			case REDCURSEDCRYSTAL_LVL14:
				return "red";
			case BLUECURSEDCRYSTAL_LVL14:
				return "blue";
			case GREENCURSEDCRYSTAL_LVL14:
				return "green";
		}
		return null;
	}

	/**
	 * @param crystalId
	 *            - itemId кристала
	 * @return уровень кристала
	 */
	private static int getCrystalLevel(final int crystalId)
	{
		for(int i = 0; i < REDCRYSTALS.length; i++)
			if(REDCRYSTALS[i] == crystalId)
				return i;
		for(int i = 0; i < BLUECRYSTALS.length; i++)
			if(BLUECRYSTALS[i] == crystalId)
				return i;
		for(int i = 0; i < GREENCRYSTALS.length; i++)
			if(GREENCRYSTALS[i] == crystalId)
				return i;
		switch (crystalId)
		{
			case REDCURSEDCRYSTAL_LVL14:
			case BLUECURSEDCRYSTAL_LVL14:
			case GREENCURSEDCRYSTAL_LVL14:
				return 14;
		}
		return 0;
	}

	/**
	 * @param crystalId
	 *            - itemId кристала
	 * @return следующий уровень кристала
	 */
	private static int getNextLevelCrystalId(final int crystalId)
	{
		for(int i = 0; i < REDCRYSTALS.length; i++)
			if(REDCRYSTALS[i] == crystalId)
				return i >= MAX_CRYSTALS_LEVEL ? REDCRYSTALS[MAX_CRYSTALS_LEVEL] : REDCRYSTALS[i + 1];

		for(int i = 0; i < GREENCRYSTALS.length; i++)
			if(GREENCRYSTALS[i] == crystalId)
				return i >= MAX_CRYSTALS_LEVEL ? GREENCRYSTALS[MAX_CRYSTALS_LEVEL] : GREENCRYSTALS[i + 1];

		for(int i = 0; i < BLUECRYSTALS.length; i++)
			if(BLUECRYSTALS[i] == crystalId)
				return i >= MAX_CRYSTALS_LEVEL ? BLUECRYSTALS[MAX_CRYSTALS_LEVEL] : BLUECRYSTALS[i + 1];
		return 0;
	}

	/**
	 * @param crystalId
	 *            - itemId кристала
	 * @return itemId Cursed Soul Crystal
	 */
	private static int getCursedCrystal(final int crystalId)
	{
		if(crystalId == REDCRYSTALS[14])
			return REDCURSEDCRYSTAL_LVL14;
		if(crystalId == GREENCRYSTALS[14])
			return GREENCURSEDCRYSTAL_LVL14;
		if(crystalId == BLUECRYSTALS[14])
			return BLUECURSEDCRYSTAL_LVL14;
		return crystalId;
	}

	/**
	 * @param crystalId
	 *            - itemId кристала
	 * @return Возвращает true если предмет с указаным айди является кристалом души, иначе вернёт false
	 */
	private static boolean isSoulCrystal(final int crystalId)
	{
		if(ArrayUtil.arrayContains(REDCRYSTALS, crystalId))
			return true;
		if(ArrayUtil.arrayContains(BLUECRYSTALS, crystalId))
			return true;
		if(ArrayUtil.arrayContains(GREENCRYSTALS, crystalId))
			return true;
		return false;
	}

	public L2ItemInstance takeHarvest()
	{
		harvestLock.lock();
		final L2ItemInstance harvest = _harvestItem;
		_harvestItem = null;
		_seeded = null;
		seederStoreId = 0;
		harvestLock.unlock();
		return harvest;
	}

	public void setSeeded(final L2Item seed, final L2Player player)
	{
		if(player == null)
			return;

		harvestLock.lock();
		try
		{
			_seeded = seed;
			seederStoreId = player.getStoredId();
			// Количество всходов зависит от множителя HP конкретного моба

			_harvestItem = ItemTable.getInstance().createItem(L2Manor.getInstance().getCropType(seed.getItemId()), 0, getNpcId(), "L2NpcInstance.setSeeded");
			// Количество всходов от xHP до (xHP + xHP/2)
			if(getTemplate().rateHp <= 1)
				_harvestItem.setCount(Config.RATE_MANOR);
			else
				_harvestItem.setCount(Rnd.get(Math.round(getTemplate().rateHp), Math.round(1.5 * getTemplate().rateHp)) * Config.RATE_MANOR);
		}
		finally
		{
			harvestLock.unlock();
		}
	}

	public boolean isSeeded(final L2Player seeder)
	{
		if(seederStoreId == 0)
			return false;
		final L2Player _seeder = L2ObjectsStorage.getAsPlayer(seederStoreId);
		return seeder != null && seeder == _seeder || _dieTime + 10000 < System.currentTimeMillis();
	}

	public boolean isSeeded()
	{
		return _seeded != null;
	}

	/** True if a Dwarf has used Spoil on this L2NpcInstance */
	private boolean _isSpoiled;

	/**
	 * Return True if this L2NpcInstance has drops that can be sweeped.
	 */
	public boolean isSpoiled()
	{
		return _isSpoiled; // FIXME возможно нужно делать sweepLock.lock() ... но тут же только чтение одного поля...
	}

	public boolean isSpoiled(final L2Player spoiler)
	{
		L2Player this_spoiler;
		sweepLock.lock();
		try
		{
			if(!_isSpoiled) // если не заспойлен то false
				return false;
			this_spoiler = L2ObjectsStorage.getAsPlayer(spoilerStoreId);
		}
		finally
		{
			sweepLock.unlock();
		}
		if(this_spoiler == null || spoiler.getObjectId() == this_spoiler.getObjectId() || _dieTime + 10000 < System.currentTimeMillis())
			return true;
		if(getDistance(this_spoiler) > Config.ALT_PARTY_DISTRIBUTION_RANGE) // если спойлер слишком далеко разрешать
			return true;
		if(spoiler.getParty() != null && spoiler.getParty().containsMember(this_spoiler)) // сопартийцам тоже можно
			return true;
		return false;
	}

	/**
	 * Set the spoil state of this L2NpcInstance.<BR>
	 * <BR>
	 */
	public void setSpoiled(final boolean isSpoiled, final L2Player spoiler)
	{
		sweepLock.lock();
		try
		{
			_isSpoiled = isSpoiled;
			spoilerStoreId = spoiler != null ? spoiler.getStoredId() : 0;
		}
		finally
		{
			sweepLock.unlock();
		}
	}

	public void doItemDrop(final L2Character lastAttacker)
	{
		final L2Player player = lastAttacker.getPlayer();
		if(player == null)
			return;

		if(getTemplate().getDropData() != null)
		{
			final double mod = calcStat(Stats.DROP, 1., player, null);

			final List<ItemToDrop> drops = getTemplate().getDropData().rollDrop(calculateLevelDiffForDrop(player.getLevel(), false), this, player, mod);
			for(final ItemToDrop drop : drops)
			{
				// Если в моба посеяно семя, причем не альтернативное - не давать никакого дропа, кроме адены.
				if(_seeded != null && !_seeded.isAltSeed() && drop.itemId != 57 && drop.itemId != 6360 && drop.itemId != 6361 && drop.itemId != 6362)
					continue;
				dropItem(player, drop.itemId, drop.count);
			}
		}
	}

	private void doSweepDrop(final L2Character lastAttacker)
	{
		final L2Player player = lastAttacker.getPlayer();
		if(player == null)
			return;

		final GArray<L2ItemInstance> spoiled = new GArray<L2ItemInstance>();
		if(getTemplate().getDropData() != null)
		{
			final int levelDiff = calculateLevelDiffForDrop(lastAttacker.getLevel(), true);
			final double mod = calcStat(Stats.DROP, 1., lastAttacker, null);

			final List<ItemToDrop> spoils = getTemplate().getDropData().rollSpoil(levelDiff, this, player, mod);
			for(final ItemToDrop spoil : spoils)
			{
				final L2ItemInstance dropit = ItemTable.getInstance().createItem(spoil.itemId, lastAttacker.getObjectId(), getNpcId(), "L2NpcInstance.doSweepDrop");
				dropit.setCount(spoil.count);
				spoiled.add(dropit);
			}
		}

		if(spoiled.size() > 0)
			_sweepItems = spoiled.toArray(new L2ItemInstance[spoiled.size()]);
	}

	private double[] calculateExpAndSp(final L2Character attacker, final int level, final long damage)
	{
		if(!isInRange(attacker, Config.ALT_PARTY_DISTRIBUTION_RANGE) && Math.abs(attacker.getZ() - getZ()) < 400)
			return new double[] { 0, 0 };

		// kamael exp penalty
		int diff = level - getLevel();
		if(level > 77 && diff > 3 && diff <= 5)
			diff += 3;

		double xp = getExpReward() * damage / getMaxHp();
		double sp = getSpReward() * damage / getMaxHp();

		if(diff > 5)
		{
			final double mod = Math.pow(.83, diff - 5);
			xp *= mod;
			sp *= mod;
		}

		xp = Math.max(0, xp);
		sp = Math.max(0, sp);

		return new double[] { xp, sp };
	}

	private double applyOverhit(final L2Player killer, double xp)
	{
		if(xp > 0 && getOverhitAttacker() != null && killer == getOverhitAttacker())
		{
			final int overHitExp = calculateOverhitExp(xp);
			killer.sendPacket(Msg.OVER_HIT, new SystemMessage(SystemMessage.ACQUIRED_S1_BONUS_EXPERIENCE_THROUGH_OVER_HIT).addNumber((long) (overHitExp * killer.getRateExp())));
			xp += overHitExp;
		}
		return xp;
	}

	public L2Character getOverhitAttacker()
	{
		return overhitAttackerStoreId == 0 ? null : L2ObjectsStorage.getAsCharacter(overhitAttackerStoreId);
	}

	@Override
	public void setOverhitAttacker(final L2Character overhitAttacker)
	{
		overhitAttackerStoreId = overhitAttacker == null ? 0 : overhitAttacker.getStoredId();
	}

	public double getOverhitDamage()
	{
		return _overhitDamage;
	}

	@Override
	public void setOverhitDamage(final double damage)
	{
		_overhitDamage = damage;
	}

	private final int calculateOverhitExp(final double normalExp)
	{
		// Get the percentage based on the total of extra (over-hit) damage done relative to the total (maximum) ammount of HP on the L2MonsterInstance
		double overhitPercentage = getOverhitDamage() * 100 / getMaxHp();

		// Over-hit damage percentages are limited to 25% max
		if(overhitPercentage > 25)
			overhitPercentage = 25;

		// Get the overhit exp bonus according to the above over-hit damage percentage
		// (1/1 basis - 13% of over-hit damage, 13% of extra exp is given, and so on...)
		final double overhitExp = overhitPercentage / 100 * normalExp;
		overhitAttackerStoreId = 0;
		setOverhitDamage(0);

		// Return the rounded ammount of exp points to be added to the player's normal exp reward
		return (int) Math.round(overhitExp);
	}

	/**
	 * Return True if a Dwarf use Sweep on the L2NpcInstance and if item can be spoiled.<BR>
	 * <BR>
	 */
	public boolean isSweepActive()
	{
		dyingLock.lock();
		try
		{
			return _sweepItems != null && _sweepItems.length > 0;
		}
		finally
		{
			dyingLock.unlock();
		}
	}

	/**
	 * Return table containing all L2ItemInstance that can be spoiled.<BR>
	 * <BR>
	 */
	public L2ItemInstance[] takeSweep()
	{
		sweepLock.lock();
		final L2ItemInstance[] sweep;
		try
		{
			sweep = _sweepItems == null || _sweepItems.length == 0 ? null : _sweepItems.clone();
			_sweepItems = null;
		}
		finally
		{
			sweepLock.unlock();
		}
		return sweep;
	}

	public void clearSweep()
	{
		sweepLock.lock();
		try
		{
			_isSpoiled = false;
			spoilerStoreId = 0;
			_sweepItems = null;
		}
		finally
		{
			sweepLock.unlock();
		}
	}

	public void clearHarvest()
	{
		harvestLock.lock();
		try
		{
			_harvestItem = null;
			_seeded = null;
			seederStoreId = 0;
		}
		finally
		{
			harvestLock.unlock();
		}
	}

	@Override
	public boolean isInvul()
	{
		return _isInvul || calcStat(Stats.INVULNERABLE, 0, null, null) > 0;
	}

	@Override
	public boolean isMonster()
	{
		return true;
	}

	public class MinionMaintainTask implements Runnable
	{
		@Override
		public void run()
		{
			if(L2MonsterInstance.this == null || isDead())
				return;
			try
			{
				if(minionList == null)
					setNewMinionList();
				minionList.maintainMinions();
			}
			catch(final Throwable e)
			{
				e.printStackTrace();
			}
		}
	}
}
