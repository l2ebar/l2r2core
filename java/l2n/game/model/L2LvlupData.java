package l2n.game.model;

public class L2LvlupData
{
	private int _classid;
	private int _classLvl;
	private float _classHpAdd;
	private float _classHpBase;
	private float _classHpModifier;
	private float _classCpAdd;
	private float _classCpBase;
	private float _classCpModifier;
	private float _classMpAdd;
	private float _classMpBase;
	private float _classMpModifier;

	public float get_classHpAdd()
	{
		return _classHpAdd;
	}

	public void set_classHpAdd(float hpAdd)
	{
		_classHpAdd = hpAdd;
	}

	public float get_classHpBase()
	{
		return _classHpBase;
	}

	public void set_classHpBase(float hpBase)
	{
		_classHpBase = hpBase;
	}

	public float get_classHpModifier()
	{
		return _classHpModifier;
	}

	public void set_classHpModifier(float hpModifier)
	{
		_classHpModifier = hpModifier;
	}

	public float get_classCpAdd()
	{
		return _classCpAdd;
	}

	public void set_classCpAdd(float cpAdd)
	{
		_classCpAdd = cpAdd;
	}

	public float get_classCpBase()
	{
		return _classCpBase;
	}

	public void set_classCpBase(float cpBase)
	{
		_classCpBase = cpBase;
	}

	public float get_classCpModifier()
	{
		return _classCpModifier;
	}

	public void set_classCpModifier(float cpModifier)
	{
		_classCpModifier = cpModifier;
	}

	public int get_classid()
	{
		return _classid;
	}

	public void set_classid(int _classid)
	{
		this._classid = _classid;
	}

	public int get_classLvl()
	{
		return _classLvl;
	}

	public void set_classLvl(int lvl)
	{
		_classLvl = lvl;
	}

	public float get_classMpAdd()
	{
		return _classMpAdd;
	}

	public void set_classMpAdd(float mpAdd)
	{
		_classMpAdd = mpAdd;
	}

	public float get_classMpBase()
	{
		return _classMpBase;
	}

	public void set_classMpBase(float mpBase)
	{
		_classMpBase = mpBase;
	}

	public float get_classMpModifier()
	{
		return _classMpModifier;
	}

	public void set_classMpModifier(float mpModifier)
	{
		_classMpModifier = mpModifier;
	}
}
