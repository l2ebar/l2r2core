package l2n.game.model.quest;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.instancemanager.QuestManager;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.QuestRewardTable.QuestReward;
import l2n.game.model.quest.QuestRewardTable.Reward;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.network.serverpackets.PlaySound;
import l2n.game.network.serverpackets.StatusUpdate;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2Item;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Files;
import l2n.util.Location;
import l2n.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Quest extends EventScript {
    public static final PlaySound SOUND_ITEMGET = new PlaySound("ItemSound.quest_itemget");
    public static final PlaySound SOUND_ACCEPT = new PlaySound("ItemSound.quest_accept");
    public static final PlaySound SOUND_MIDDLE = new PlaySound("ItemSound.quest_middle");
    public static final PlaySound SOUND_FINISH = new PlaySound("ItemSound.quest_finish");
    public static final PlaySound SOUND_GIVEUP = new PlaySound("ItemSound.quest_giveup");
    public static final PlaySound SOUND_TUTORIAL = new PlaySound("ItemSound.quest_tutorial");
    public static final PlaySound SOUND_JACKPOT = new PlaySound("ItemSound.quest_jackpot");
    public static final PlaySound SOUND_HORROR2 = new PlaySound("SkillSound5.horror_02");
    public static final PlaySound SOUND_BEFORE_BATTLE = new PlaySound("Itemsound.quest_before_battle");
    public static final PlaySound SOUND_FANFARE_MIDDLE = new PlaySound("ItemSound.quest_fanfare_middle");
    public static final PlaySound SOUND_FANFARE2 = new PlaySound("ItemSound.quest_fanfare_2");
    public static final PlaySound SOUND_BROKEN_KEY = new PlaySound("ItemSound2.broken_key");
    public static final PlaySound SOUND_ENCHANT_SUCESS = new PlaySound("ItemSound3.sys_enchant_sucess");
    public static final PlaySound SOUND_ENCHANT_FAILED = new PlaySound("ItemSound3.sys_enchant_failed");
    public static final PlaySound SOUND_ED_CHIMES05 = new PlaySound("AmdSound.ed_chimes_05");
    public static final PlaySound SOUND_ARMOR_WOOD_3 = new PlaySound("ItemSound.armor_wood_3");
    public static final PlaySound SOUND_ITEM_DROP_EQUIP_ARMOR_CLOTH = new PlaySound("ItemSound.item_drop_equip_armor_cloth");

    public static final int ADENA_ID = 57;

    public static final int PARTY_NONE = -1;
    public static final int PARTY_ONE = 0;
    public static final int PARTY_ALL = 1;

    protected static final Logger _log = Logger.getLogger(Quest.class.getName());

    /**
     * HashMap containing events from String value of the event
     */
    private static final HashMap<String, Quest> _allEventsS = new HashMap<>();

    /**
     * HashMap containing lists of timers from the name of the timer
     */
    private static final HashMap<String, GArray<QuestTimer>> _allEventTimers = new HashMap<>();

    protected String _descr;
    protected String _name;
    protected int _party;
    protected int _questId;

    public static final int CREATED = 1;
    public static final int STARTED = 2;
    public static final int COMPLETED = 3;

    private final GArray<Integer> _questitems = new GArray<>();

    /**
     * награда за квест
     */
    private QuestReward _reward = null;

    /**
     * Этот метод для регистрации квестовых вещей, которые будут удалены при прекращении квеста, независимо от того, был он закончен или прерван. <strong>Добавлять сюда награды нельзя</strong>.
     */
    public void addQuestItem(final int... ids) {
        for (final int id : ids)
            if (id != 0)
                addQuestItem(id);
    }

    /**
     * Этот метод для регистрации квестовых вещей, которые будут удалены при прекращении квеста, независимо от того, был он закончен или прерван. <strong>Добавлять сюда награды нельзя</strong>.
     */
    public void addQuestItem(final int id) {
        L2Item i = null;
        try {
            i = ItemTable.getInstance().getTemplate(id);
        } catch (final Exception e) {
            if (Config.DEBUG)
                _log.warning("Quest: Warning - unknown item " + i + " (" + id + ") in quest drop in " + getName());
        }

        if (Config.DEBUG && (i == null || i.getType2() != L2Item.TYPE2_QUEST))
            _log.warning("Quest: Warning - non-quest item " + i + " (" + id + ") in quest drop in " + getName());

        if (_questitems.contains(id) && Config.DEBUG)
            _log.warning("Quest: Warning - " + i + " (" + id + ") multiple times in quest drop in " + getName());

        _questitems.add(id);
    }

    public GArray<Integer> getItems() {
        return _questitems;
    }

    /**
     * Для проверки квестовый итем или награда.
     *
     * @param itemId - id предмета
     * @return вернёт true если предмет квестовый
     */
    public boolean isQuestItem(final int itemId) {
        return _questitems.contains(itemId);
    }

    /**
     * Update informations regarding quest in database.<BR>
     * <U><I>Actions :</I></U><BR>
     * <LI>Get ID state of the quest recorded in object qs</LI> <LI>Save in database the ID state (with or without the star) for the variable called "&lt;state&gt;" of the quest</LI>
     *
     * @param qs : QuestState
     */
    public static void updateQuestInDb(final QuestState qs) {
        updateQuestVarInDb(qs, "<state>", qs.getStateName());
    }

    /**
     * Insert in the database the quest for the player.
     *
     * @param qs    : QuestState pointing out the state of the quest
     * @param var   : String designating the name of the variable for the quest
     * @param value : String designating the value of the variable for the quest
     */
    public static void updateQuestVarInDb(final QuestState qs, final String var, final String value) {
        final L2Player player = qs.getPlayer();
        if (player == null)
            return;

        ThreadConnection con = null;
        FiltredPreparedStatement statement = null;
        try {
            con = L2DatabaseFactory.getInstance().getConnection();
            statement = con.prepareStatement("REPLACE INTO character_quests (char_id,name,var,value) VALUES (?,?,?,?)");
            statement.setInt(1, qs.getPlayer().getObjectId());
            statement.setString(2, qs.getQuest().getName());
            statement.setString(3, var);
            statement.setString(4, value);
            statement.executeUpdate();
        } catch (final Exception e) {
            _log.log(Level.WARNING, "could not insert char quest:", e);
        } finally {
            DbUtils.closeQuietly(con, statement);
        }
    }

    /**
     * Delete the player's quest from database.
     *
     * @param qs : QuestState pointing out the player's quest
     */
    public static void deleteQuestInDb(final QuestState qs) {
        final L2Player player = qs.getPlayer();
        if (player == null)
            return;

        ThreadConnection con = null;
        FiltredPreparedStatement statement = null;
        try {
            con = L2DatabaseFactory.getInstance().getConnection();
            statement = con.prepareStatement("DELETE FROM character_quests WHERE char_id=? AND name=?");
            statement.setInt(1, qs.getPlayer().getObjectId());
            statement.setString(2, qs.getQuest().getName());
            statement.executeUpdate();
        } catch (final Exception e) {
            _log.log(Level.WARNING, "could not delete char quest:", e);
        } finally {
            DbUtils.closeQuietly(con, statement);
        }
    }

    /**
     * Delete a variable of player's quest from the database.
     *
     * @param qs  : object QuestState pointing out the player's quest
     * @param var : String designating the variable characterizing the quest
     */
    public static void deleteQuestVarInDb(final QuestState qs, final String var) {
        final L2Player player = qs.getPlayer();
        if (player == null)
            return;

        ThreadConnection con = null;
        FiltredPreparedStatement statement = null;
        try {
            con = L2DatabaseFactory.getInstance().getConnection();
            statement = con.prepareStatement("DELETE FROM character_quests WHERE char_id=? AND name=? AND var=?");
            statement.setInt(1, qs.getPlayer().getObjectId());
            statement.setString(2, qs.getQuest().getName());
            statement.setString(3, var);
            statement.executeUpdate();
        } catch (final Exception e) {
            _log.log(Level.WARNING, "could not delete char quest:", e);
        } finally {
            DbUtils.closeQuietly(con, statement);
        }
    }

    /**
     * Return collection view of the values contains in the allEventS
     *
     * @return Collection<Quest>
     */
    public static Collection<Quest> findAllEvents() {
        return _allEventsS.values();
    }

    /**
     * Add quests to the L2PCInstance of the player.<BR>
     * <BR>
     * <U><I>Action : </U></I><BR>
     * Add state of quests, drops and variables for quests in the HashMap _quest of L2Player
     *
     * @param player : Player who is entering the world
     */
    public static void playerEnter(final L2Player player) {
        ThreadConnection con = null;
        FiltredPreparedStatement statement = null;
        FiltredPreparedStatement invalidQuestData = null;
        ResultSet rset = null;
        try {
            // Get list of quests owned by the player from database
            con = L2DatabaseFactory.getInstance().getConnection();

            invalidQuestData = con.prepareStatement("DELETE FROM character_quests WHERE char_id=? and name=?");
            statement = con.prepareStatement("SELECT name,value FROM character_quests WHERE char_id=? AND var=?");
            statement.setInt(1, player.getObjectId());
            statement.setString(2, "<state>");
            rset = statement.executeQuery();
            while (rset.next()) {
                // Get ID of the quest and ID of its state
                final String questId = rset.getString("name");
                final String state = rset.getString("value");

                if (state.equalsIgnoreCase("Start")) // невзятый квест
                {
                    invalidQuestData.setInt(1, player.getObjectId());
                    invalidQuestData.setString(2, questId);
                    invalidQuestData.executeUpdate();
                    continue;
                }

                // Search quest associated with the ID
                final Quest q = QuestManager.getQuest(questId);
                if (q == null) {
                    if (!Config.DONTLOADQUEST)
                        _log.warning("Unknown quest " + questId + " for player " + player.getName());
                    continue;
                }

                // Create a new QuestState for the player that will be added to the player's list of quests
                new QuestState(q, player, getStateId(state));
            }
            DbUtils.closeQuietly(invalidQuestData);
            DbUtils.closeQuietly(statement, rset);

            // Get list of quests owned by the player from the DB in order to add variables used in the quest.
            statement = con.prepareStatement("SELECT name,var,value FROM character_quests WHERE char_id=?");
            statement.setInt(1, player.getObjectId());
            rset = statement.executeQuery();
            while (rset.next()) {
                final String questId = rset.getString("name");
                final String var = rset.getString("var");
                String value = rset.getString("value");
                // Get the QuestState saved in the loop before
                final QuestState qs = player.getQuestState(questId);
                if (qs == null)
                    continue;

                // затычка на пропущенный первый конд
                if (var.equals("cond") && Integer.parseInt(value) < 0)
                    value = String.valueOf(Integer.parseInt(value) | 1);
                // Add parameter to the quest
                qs.set(var, value, false);
            }
        } catch (final Exception e) {
            _log.log(Level.WARNING, "could not insert char quest:", e);
        } finally {
            DbUtils.close(invalidQuestData);
            DbUtils.closeQuietly(con, statement, rset);
        }

        // events
        for (final String name : _allEventsS.keySet())
            player.processQuestEvent(name, "enter", null);
    }

    /**
     * @param questIdint pointing out the ID of the quest
     * @param party      -1 - killer; 0 - party one random with quest; 1 - to all party with quest
     */
    public Quest(final int questId, final int party) {
        this(questId, null, null, party);
    }

    /**
     * (Constructor)Add values to class variables and put the quest in HashMaps.
     *
     * @param questId : int pointing out the ID of the quest
     * @param name    : String corresponding to the name of the quest
     * @param descr   : String for the description of the quest
     * @param party   : -1 - killer; 0 - party one random with quest; 1 - to all party with quest
     */
    public Quest(final int questId, final String name, final String descr, final int party) {
        _questId = questId;
        _name = name == null ? getClass().getSimpleName() : name;
        _descr = descr;
        if (Config.ENG_QUEST_NAMES && _descr == null && questId > 0)
            _descr = getDescr();
        _party = party;

        if (questId != 0)
            QuestManager.addQuest(this);
        else
            _allEventsS.put(_name, this);

        if (questId > 0)
            _reward = QuestRewardTable.getInstance().getReward(questId);
    }

    public static String getStateName(final int state) {
        switch (state) {
            case 1:
                return "Start";
            case 2:
                return "Started";
            case 3:
                return "Completed";
        }
        return "Start";
    }

    public static int getStateId(final String state) {
        if (state.equalsIgnoreCase("Start"))
            return 1;
        if (state.equalsIgnoreCase("Started"))
            return 2;
        if (state.equalsIgnoreCase("Completed"))
            return 3;
        return 1;
    }

    /**
     * Add this quest to the list of quests that the passed mob will respond to for Attack Events.<BR>
     * <BR>
     *
     * @param attackId
     * @return int : attackId
     */
    public L2NpcTemplate addAttackId(final int attackId) {
        return addEventId(attackId, QuestEventType.MOBGOTATTACKED);
    }

    public void addAttackId(final int... attackIds) {
        for (final int attackId : attackIds)
            addEventId(attackId, QuestEventType.MOBGOTATTACKED);
    }

    /**
     * Add this quest to the list of quests that the passed mob will respond to for the specified Event type.<BR>
     * <BR>
     *
     * @param npcId     : id of the NPC to register
     * @param eventType : type of event being registered
     * @return int : npcId
     */
    private final L2NpcTemplate addEventId(final int npcId, final QuestEventType eventType) {
        try {
            final L2NpcTemplate t = NpcTable.getTemplate(npcId);
            if (t != null)
                t.addQuestEvent(eventType, this);
            else
                _log.warning("Quest: Q[" + _questId + "] for npcId " + npcId + " template is NULL.");
            return t;
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            return null;
        }
    }

    public void addKillId(final int... killIds) {
        for (final int killid : killIds)
            addEventId(killid, QuestEventType.MOBKILLED);
    }

    public void addKillId(final Collection<Integer> killIds) {
        for (final int killid : killIds)
            addKillId(killid);
    }

    /**
     * Add this quest to the list of quests that the passed mob will respond to for Kill Events.<BR>
     * <BR>
     *
     * @param killId
     * @return int : killId
     */
    public L2NpcTemplate addKillId(final int killId) {
        return addEventId(killId, QuestEventType.MOBKILLED);
    }

    /**
     * Add this quest to the list of quests that the passed npc will respond to for Skill-Use Events.<BR>
     * <BR>
     *
     * @param npcId : ID of the NPC
     * @return int : ID of the NPC
     */
    public L2NpcTemplate addSkillUseId(final int npcId) {
        return addEventId(npcId, QuestEventType.MOB_TARGETED_BY_SKILL);
    }

    /**
     * Add the quest to the NPC's startQuest Вызывает addTalkId
     *
     * @param npcId
     * @return L2NpcTemplate : Start NPC
     */
    public L2NpcTemplate addStartNpc(final int npcId) {
        addTalkId(npcId);
        return addEventId(npcId, QuestEventType.QUEST_START);
    }

    public void addStartNpc(final int... npcIds) {
        for (final int npcId : npcIds)
            addStartNpc(npcId);
    }

    /**
     * Add the quest to the NPC's first-talk (default action dialog)
     *
     * @param npcId
     * @return L2NpcTemplate : Start NPC
     */
    public L2NpcTemplate addFirstTalkId(final int npcId) {
        return addEventId(npcId, QuestEventType.NPC_FIRST_TALK);
    }

    public void addFirstTalkId(final int... npcIds) {
        for (final int npcId : npcIds)
            addFirstTalkId(npcId);
    }

    /**
     * Add this quest to the list of quests that the passed npc will respond to for Talk Events.<BR>
     * <BR>
     *
     * @param talkId : ID of the NPC
     * @return int : ID of the NPC
     */
    public L2NpcTemplate addTalkId(final int talkId) {
        return addEventId(talkId, QuestEventType.QUEST_TALK);
    }

    public void addTalkId(final int... talkIds) {
        for (final int talkId : talkIds)
            addTalkId(talkId);
    }

    /**
     * This is used to register all monsters contained in mobs for a particular script
     *
     * @param mobs
     */
    public void registerMobs(final int... mobs) {
        for (final int id : mobs) {
            addEventId(id, QuestEventType.MOB_TARGETED_BY_SKILL);
            addEventId(id, QuestEventType.MOBGOTATTACKED);
            addEventId(id, QuestEventType.MOBKILLED);
        }
    }

    public void cancelQuestTimer(final String name, final L2NpcInstance npc, final L2Player player) {
        final QuestTimer timer = getQuestTimer(name, npc, player);
        if (timer != null)
            timer.cancel();
    }

    /**
     * Возвращает название квеста на языке игрока, если возможно
     */
    public String getDescr(final L2Player player) {
        if (_descr == null)
            return new CustomMessage("q." + _questId, player).toString();
        return _descr;
    }

    /**
     * Возвращает английское название квеста
     */
    public String getDescr() {
        if (_descr == null)
            return new CustomMessage("q." + _questId, "en").toString();
        return _descr;
    }

    /**
     * Return name of the quest
     *
     * @return String
     */
    @Override
    public String getName() {
        return _name;
    }

    /**
     * Return ID of the quest
     *
     * @return int
     */
    public int getQuestIntId() {
        return _questId;
    }

    public QuestTimer getQuestTimer(final String name, final L2NpcInstance npc, final L2Player player) {
        if (_allEventTimers.get(name) == null)
            return null;
        for (final QuestTimer timer : _allEventTimers.get(name))
            if (timer.isMatch(this, name, npc, player))
                return timer;
        return null;
    }

    public GArray<QuestTimer> getQuestTimers(final String name) {
        return _allEventTimers.get(name);
    }

    /**
     * Return party state of quest
     *
     * @return <B>-1</B> - награда только тому кто убил<BR>
     * <B>0</B> - награда выдаётся рандомно члену пати<BR>
     * <B>1</B> - награда выдаётся всем членам пати<BR>
     */
    public int getParty() {
        return _party;
    }

    /**
     * Add a new QuestState to the database and return it.
     *
     * @param player
     * @return QuestState : QuestState created
     */
    public QuestState newQuestState(final L2Player player, final int st) {
        final QuestState qs = new QuestState(this, player, st);
        updateQuestInDb(qs);
        return qs;
    }

    public void notifyAttack(final L2NpcInstance npc, final QuestState qs) {
        String res = null;
        try {
            res = onAttack(npc, qs);
        } catch (final Exception e) {
            showError(qs.getPlayer(), e);
            return;
        }
        showResult(npc, qs.getPlayer(), res);
    }

    public void notifyDeath(final L2Character killer, final L2Playable victim, final QuestState qs) {
        String res = null;
        try {
            res = onDeath(killer, victim, qs);
        } catch (final Exception e) {
            showError(qs.getPlayer(), e);
            return;
        }
        showResult(null, qs.getPlayer(), res);
    }

    public void notifyEvent(final String event, final QuestState qs, final L2NpcInstance npc) {
        String res = null;
        try {
            res = onEvent(event, qs, npc);
        } catch (final Exception e) {
            showError(qs.getPlayer(), e);
            return;
        }
        showResult(npc, qs.getPlayer(), res);
    }

    public void notifyKill(final L2NpcInstance npc, final QuestState qs) {
        String res = null;
        try {
            res = onKill(npc, qs);
        } catch (final Exception e) {
            showError(qs.getPlayer(), e);
            return;
        }
        showResult(npc, qs.getPlayer(), res);
    }

    public void notifyPlayerKill(final L2Player player, final QuestState qs) {
        String res = null;
        try {
            res = onPlayerKill(player, qs);
        } catch (final Exception e) {
            showError(qs.getPlayer(), e);
            return;
        }
        showResult(null, qs.getPlayer(), res);
    }

    /**
     * Override the default NPC dialogs when a quest defines this for the given NPC
     */
    public final boolean notifyFirstTalk(final L2NpcInstance npc, final L2Player player) {
        String res = null;
        try {
            res = onFirstTalk(npc, player);
        } catch (final Exception e) {
            showError(player, e);
            return true;
        }
        // if the quest returns text to display, display it. Otherwise, use the default npc text.
        return showResult(npc, player, res);
    }

    public boolean notifyTalk(final L2NpcInstance npc, final QuestState qs) {
        String res = null;
        try {
            res = onTalk(npc, qs);
        } catch (final Exception e) {
            showError(qs.getPlayer(), e);
            return true;
        }
        return showResult(npc, qs.getPlayer(), res);
    }

    public boolean notifySkillUse(final L2NpcInstance npc, final L2Skill skill, final QuestState qs) {
        String res = null;
        try {
            res = onSkillUse(npc, skill, qs);
        } catch (final Exception e) {
            showError(qs.getPlayer(), e);
            return true;
        }
        return showResult(npc, qs.getPlayer(), res);
    }

    public String onAttack(final L2NpcInstance npc, final QuestState qs) {
        return null;
    }

    public String onDeath(final L2Character killer, final L2Playable victim, final QuestState qs) {
        return null;
    }

    public String onEvent(final String event, final QuestState qs, final L2NpcInstance npc) {
        return null;
    }

    public String onKill(final L2NpcInstance npc, final QuestState qs) {
        return null;
    }

    public String onPlayerKill(final L2Player killed, final QuestState st) {
        return null;
    }

    public String onFirstTalk(final L2NpcInstance npc, final L2Player player) {
        return null;
    }

    public String onTalk(final L2NpcInstance npc, final QuestState qs) {
        return null;
    }

    public String onSkillUse(final L2NpcInstance npc, final L2Skill skill, final QuestState qs) {
        return null;
    }

    public void onAbort(final QuestState qs) {
    }

    public void onChange(final boolean end) {
    }

    public void removeQuestTimer(final QuestTimer timer) {
        if (timer == null)
            return;
        final GArray<QuestTimer> timers = getQuestTimers(timer.getName());
        if (timers == null)
            return;
        timers.remove(timer);
    }

    /**
     * Show message error to player who has an access level greater than 0
     *
     * @param player : L2Player
     * @param t      : Throwable
     */
    private void showError(final L2Player player, final Throwable t) {
        _log.log(Level.WARNING, "", t);
        if (player.getPlayerAccess().IsGM) {
            final StringWriter sw = new StringWriter();
            final PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            final String res = "<html><body><title>Script error</title>" + sw.toString() + "</body></html>";
            showResult(null, player, res);
        }
    }

    /**
     * Show HTML file to client
     *
     * @param fileName
     * @return String : message sent to client
     */
    public String showHtmlFile(final L2Player player, final String fileName) {
        return showHtmlFile(player, fileName, null, (String[]) null); // (String[]) - затык для вызова корректного метода.
    }

    public String showHtmlFile(final L2Player player, final String fileName, final String toReplace, final String replaceWith) {
        return showHtmlFile(player, fileName, new String[]{toReplace}, new String[]{replaceWith});
    }

    public String showHtmlFile(final L2Player player, final String fileName, final String[] toReplace, final String[] replaceWith) {
        String content = null;

        if (fileName.contains("/"))
            content = Files.read(fileName, player);
        else if (!isCoreQuest()) {
            String _path = getClass().toString();
            _path = _path.substring(6, _path.lastIndexOf(".")) + ".";
            if (_path.contains("data.scripts")){
                content = Files.read(  _path.replace(".", "/") + fileName, player);
            }else {
                content = Files.read("data/scripts/" + _path.replace(".", "/") + fileName, player);
            }
            if (player.isGM() && content == null)
                content = "Can't find file '" + "data/scripts/" + _path.replace(".", "/") + fileName + "'";
        }
        // для квестов которые в ядре
        else if (isCoreQuest()) {
            content = Files.read("data/scripts/quests/" + _name + "/" + fileName, player);
            if (player.isGM() && content == null)
                content = "Can't find file '" + "data/scripts/quests/" + _name + "/" + fileName + "'";
        }
        if (content == null)
            content = "Can't find file '" + fileName + "'";

        if (player != null && player.getTarget() != null)
            content = content.replaceAll("%objectId%", String.valueOf(player.getTarget().getObjectId()));

        // Make a replacement inside before sending html to client
        if (toReplace != null && replaceWith != null && toReplace.length == replaceWith.length)
            for (int i = 0; i < toReplace.length; i++)
                content = content.replaceAll(toReplace[i], replaceWith[i]);

        // Send message to client if message not empty
        if (content != null && player != null) {
            final NpcHtmlMessage npcReply = new NpcHtmlMessage(5);
            npcReply.setHtml(content);
            if (getQuestIntId() > 0 && getQuestIntId() < 20000)
                npcReply.setQuest(getQuestIntId());
            player.sendPacket(npcReply);
        }

        return content;
    }

    protected boolean isCoreQuest() {
        return false;
    }

    /**
     * Show a message to player.<BR>
     * <BR>
     * <U><I>Concept : </I></U><BR>
     * 3 cases are managed according to the value of the parameter "res" :<BR>
     * <LI><U>"res" ends with string ".html" :</U> an HTML is opened in order to be shown in a dialog box</LI> <LI><U>"res" starts with "<html>" :</U> the message hold in "res" is shown in a dialog box</LI> <LI><U>otherwise :</U> the message hold in
     * "res" is shown in chat box</LI>
     *
     * @param qs  : QuestState
     * @param res : String pointing out the message to show at the player
     * @return boolean
     */
    private boolean showResult(final L2NpcInstance npc, final L2Player player, final String res) {
        if (res == null)
            return true;
        if (res.isEmpty())
            return false;
        if (res.startsWith("no_quest") || res.equalsIgnoreCase("noquest") || res.equalsIgnoreCase("no-quest"))
            showHtmlFile(player, "data/html/no-quest.htm");
        else if (res.equalsIgnoreCase("completed"))
            showHtmlFile(player, "data/html/completed-quest.htm");
        else if (res.endsWith(".htm"))
            showHtmlFile(player, res);
        else {
            final NpcHtmlMessage npcReply = npc == null ? new NpcHtmlMessage(5) : new NpcHtmlMessage(player, npc);
            npcReply.setHtml(res);
            if (getQuestIntId() > 0 && getQuestIntId() < 20000)
                npcReply.setQuest(getQuestIntId());
            player.sendPacket(npcReply);
        }
        return true;
    }

    /**
     * Add a timer to the quest, if it doesn't exist already
     *
     * @param name   : name of the timer (also passed back as "event" in onAdvEvent)
     * @param time   : time in ms for when to fire the timer
     * @param npc    : npc associated with this timer (can be null)
     * @param player : player associated with this timer (can be null)
     */
    public void startQuestTimer(final String name, final long time, final L2NpcInstance npc, final L2Player player) {
        // Add quest timer if timer doesn't already exist
        GArray<QuestTimer> timers = getQuestTimers(name);
        if (timers == null) {
            timers = new GArray<QuestTimer>();
            timers.add(new QuestTimer(this, name, time, npc, player));
            _allEventTimers.put(name, timers);
        }
        // a timer with this name exists, but may not be for the same set of npc and player
        else // if there exists a timer with this name, allow the timer only if the [npc, player] set is unique
            // nulls act as wildcards
            if (getQuestTimer(name, npc, player) == null)
                timers.add(new QuestTimer(this, name, time, npc, player));
    }

    protected String str(final long i) {
        return String.valueOf(i);
    }

    public static boolean isdigit(final String s) {
        try {
            Integer.parseInt(s);
        } catch (final NumberFormatException e) {
            return false;
        }
        return true;
    }

    public L2NpcInstance addSpawn(final int npcId, final Location loc, final int randomOffset, final int despawnDelay) {
        final L2NpcInstance result = Functions.spawn(randomOffset > 50 ? loc.rnd(50, randomOffset, false) : loc, npcId);
        if (despawnDelay > 0 && result != null)
            L2GameThreadPools.getInstance().scheduleGeneral(new DeSpawnScheduleTimerTask(result), despawnDelay);
        return result;
    }

    public L2NpcInstance addSpawn(final int npcId, final int x, final int y, final int z, final int heading, final boolean randomOffset, final int despawnDelay) {
        return addSpawn(npcId, new Location(x, y, z, heading), randomOffset ? 100 : 0, despawnDelay);
    }

    /**
     * Добавляет спаун с числовым значением разброса - от 50 до randomOffset.
     * Если randomOffset указан мене 50, то координаты не меняются.
     */
    public static L2NpcInstance addSpawnToInstance(final int npcId, final Location loc, final boolean randomOffset, final long refId) {
        try {
            final L2NpcTemplate template = NpcTable.getTemplate(npcId);
            if (template != null) {
                final L2NpcInstance npc = NpcTable.getTemplate(npcId).getNewInstance();
                npc.setReflection(refId);
                npc.setSpawnedLoc(randomOffset ? loc.rnd(50, 100, false) : loc);
                npc.onSpawn();
                npc.spawnMe(npc.getSpawnedLoc());
                return npc;
            }
        } catch (final Exception e1) {
            _log.log(Level.WARNING, "Could not spawn Npc " + npcId, e1);
        }
        return null;
    }

    public static L2NpcInstance addSpawnToInstance(final int npcId, final int x, final int y, final int z, final int heading, final int randomOffset, final long refId) {
        return addSpawnToInstance(npcId, new Location(x, y, z, heading), randomOffset, refId);
    }

    public static L2NpcInstance addSpawnToInstance(final int npcId, final Location loc, final int randomOffset, final long refId) {
        try {
            final L2NpcTemplate template = NpcTable.getTemplate(npcId);
            if (template != null) {
                final L2NpcInstance npc = NpcTable.getTemplate(npcId).getNewInstance();
                npc.setReflection(refId);
                npc.setSpawnedLoc(randomOffset > 50 ? loc.rnd(50, randomOffset, false) : loc);
                npc.onSpawn();
                npc.spawnMe(npc.getSpawnedLoc());
                return npc;
            }
        } catch (final Exception e1) {
            _log.log(Level.WARNING, "Could not spawn Npc " + npcId, e1);
        }
        return null;
    }

    public class DeSpawnScheduleTimerTask implements Runnable {
        private L2NpcInstance _npc = null;

        public DeSpawnScheduleTimerTask(final L2NpcInstance npc) {
            _npc = npc;
        }

        @Override
        public void run() {
            if (_npc != null)
                if (_npc.getSpawn() != null)
                    _npc.getSpawn().despawnAll();
                else
                    _npc.deleteMe();
        }
    }

    private static final NumberFormat formatter = new DecimalFormat("000");

    /**
     * Выводит информацию о квесте в консоль
     */
    public void printInfo() {
        System.out.println("Loaded Quest: " + formatter.format(getQuestIntId()) + " - " + getDescr());
    }

    /**
     * перекреваем метод в квестах
     */
    public void onLoad() {
        printInfo();
    }

    protected void giveReward(final L2Player player) {
        if (player == null)
            return;

        if (_reward.exp > 0 || _reward.sp > 0) {
            final double bonus = player == null ? 1 : player.getBonus().RATE_QUESTS_REWARD;
            final long exp = (long) (_reward.exp * Config.RATE_QUESTS_REWARD_XP * bonus);
            final long sp = (long) (_reward.sp * Config.RATE_QUESTS_REWARD_SP * bonus);
            player.addExpAndSp(exp, sp, false, false);
        }

        L2ItemInstance item;
        for (final Reward r : _reward.getItems()) {
            if (r.isStackable()) {
                item = ItemTable.getInstance().createItem(r.getItemId(), player.getObjectId(), 0, "Quest[ID:" + _questId + "]");
                // Set quantity of item
                item.setCount(r.getCount());
                // Add items to player's inventory
                player.getInventory().addItem(item);
                Log.LogItem(player, Log.GetQuestItem, item);
            } else
                for (int i = 0; i < r.getCount(); i++) {
                    item = ItemTable.getInstance().createItem(r.getItemId(), player.getObjectId(), 0, "Quest[ID:" + _questId + "]");
                    // Set quantity of item
                    item.setCount(1);
                    // Add items to player's inventory
                    player.getInventory().addItem(item);
                    Log.LogItem(player, Log.GetQuestItem, item);
                }

            // If item for reward is gold, send message of gold reward to client
            player.sendPacket(SystemMessage.obtainItems(r.getItemId(), r.getCount(), 0));
        }

        player.sendStatusUpdate(false, StatusUpdate.CUR_LOAD);
    }
}
