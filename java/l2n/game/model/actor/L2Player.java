package l2n.game.model.actor;

import gnu.trove.iterator.TLongIterator;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.list.array.TLongArrayList;
import gnu.trove.map.hash.TIntLongHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.procedure.TIntLongProcedure;
import gnu.trove.procedure.TObjectProcedure;
import l2n.Config;
import l2n.commons.lang.reference.HardReferences;
import l2n.commons.lang.reference.IHardReference;
import l2n.commons.list.GArray;
import l2n.commons.list.GCSArray;
import l2n.commons.math.SafeMath;
import l2n.commons.sql.SqlBatch;
import l2n.commons.text.Strings;
import l2n.commons.util.CollectionUtils;
import l2n.database.FiltredPreparedStatement;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.database.utils.mysql;
import l2n.extensions.Bonus;
import l2n.extensions.Stat;
import l2n.extensions.listeners.list.CharacterListenerList;
import l2n.extensions.listeners.list.PlayerListenerList;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.*;
import l2n.game.GameTimeController;
import l2n.game.L2GameThreadPools;
import l2n.game.RecipeController;
import l2n.game.ai.*;
import l2n.game.ai.L2PlayableAI.nextAction;
import l2n.game.cache.Msg;
import l2n.game.communitybbs.BB.Forum;
import l2n.game.communitybbs.Manager.ForumsBBSManager;
import l2n.game.event.L2EventType;
import l2n.game.geodata.GeoEngine;
import l2n.game.handler.ItemHandler;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.idfactory.IdFactory;
import l2n.game.instancemanager.*;
import l2n.game.instancemanager.games.HandysBlockCheckerManager;
import l2n.game.instancemanager.games.HandysBlockCheckerManager.ArenaParticipantsHolder;
import l2n.game.loginservercon.LSConnection;
import l2n.game.loginservercon.send.ChangeAccessLevel;
import l2n.game.model.*;
import l2n.game.model.BypassManager.BypassType;
import l2n.game.model.BypassManager.DecodedBypass;
import l2n.game.model.L2Clan.RankPrivs;
import l2n.game.model.L2Multisell.MultiSellListContainer;
import l2n.game.model.L2ObjectTasks.*;
import l2n.game.model.L2Skill.Element;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.recorder.PlayerStatsChangeRecorder;
import l2n.game.model.base.*;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.model.drop.FishData;
import l2n.game.model.entity.DimensionalRift;
import l2n.game.model.entity.Duel;
import l2n.game.model.entity.Duel.DuelState;
import l2n.game.model.entity.Hero;
import l2n.game.model.entity.olympiad.CompType;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.entity.olympiad.OlympiadGame;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.residence.ClanHall;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.model.entity.residence.*;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.entity.siege.fortress.FortressSiege;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.model.entity.vehicle.L2Ship;
import l2n.game.model.entity.vehicle.L2Vehicle;
import l2n.game.model.instances.*;
import l2n.game.model.instances.L2CubicInstance.CubicType;
import l2n.game.model.items.Inventory;
import l2n.game.model.items.PcInventory;
import l2n.game.model.items.PcWarehouse;
import l2n.game.model.items.Warehouse;
import l2n.game.model.items.Warehouse.WarehouseType;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestEventType;
import l2n.game.model.quest.QuestState;
import l2n.game.model.restrictions.PlayerRestrictions;
import l2n.game.network.L2GameClient;
import l2n.game.network.clientpackets.ConfirmDlg;
import l2n.game.network.clientpackets.EnterWorld;
import l2n.game.network.serverpackets.*;
import l2n.game.skills.*;
import l2n.game.skills.effects.EffectTemplate;
import l2n.game.skills.skillclasses.Call;
import l2n.game.tables.*;
import l2n.game.taskmanager.*;
import l2n.game.taskmanager.AutoSaveManager.AutoSaveTask;
import l2n.game.taskmanager.tasks.PlayerTaskType;
import l2n.game.templates.*;
import l2n.game.templates.L2Armor.ArmorType;
import l2n.game.templates.L2Weapon.WeaponType;
import l2n.util.*;
import l2n.util.HWID.HardwareID;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.awt.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import static l2n.game.model.L2Zone.ZoneType.*;

public final class L2Player extends L2Playable
{
	private static final GCSArray<L2CubicInstance> EMPTY_CUBICS = new GCSArray<L2CubicInstance>(0);

	private static final String UPDATE_CHARACTER = "UPDATE characters SET face=?, hairStyle=?, hairColor=?, heading=?, x=?, y=?, z=? ,karma=?, pvpkills=?, pkkills=?, rec_have=?, rec_left=?, clanid=?, deletetime=?,  title=?, accesslevel=?, online=?, leaveclan=?, deleteclan=?, nochannel=?, onlinetime=?, ram=?, vitPoints=?, pledge_type=?, pledge_rank=?, lvl_joined_academy=?, apprentice=?, pcBangPoints=?, char_name=?, fame=?, bookmarkslot=?, key_bindings=? WHERE obj_Id=?";

	private static final String REMOVE_SKILL = "DELETE FROM character_skills WHERE skill_id=? AND char_obj_id=? AND class_index=?";
	private static final String UPDATE_CHARACTER_SKILL_LEVEL = "UPDATE character_skills SET skill_level=? WHERE skill_id=? AND char_obj_id=? AND class_index=?";
	private static final String ADD_SKILL_SAVE = "REPLACE INTO character_skills (char_obj_id,skill_id,skill_level,skill_name,class_index) values(?,?,?,?,?)";
	private static final String RESTORE_SKILL = "SELECT skill_id,skill_level FROM character_skills WHERE char_obj_id=? AND class_index=?";

	private static final String NEW_CHAR = "INSERT INTO `characters` (account_name, obj_Id, char_name, face, hairStyle, hairColor, sex, karma, pvpkills, pkkills, clanid, createtime, deletetime, title, accesslevel, online, leaveclan, deleteclan, nochannel, pledge_type, pledge_rank, lvl_joined_academy, apprentice) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String NEW_SUB = "INSERT INTO character_subclasses (char_obj_id, class_id, exp, sp, curHp, curMp, curCp, maxHp, maxMp, maxCp, level, active, isBase, death_penalty, num_sub) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static final String RESTORE_EFFECTS = "SELECT `skill_id`,`skill_level`,`effect_count`,`effect_cur_time`,`duration` FROM `character_effects_save` WHERE `char_obj_id`=? AND `class_index`=? ORDER BY `order` ASC";

	public static final short STORE_PRIVATE_NONE = 0;
	public static final short STORE_PRIVATE_SELL = 1;
	public static final short STORE_PRIVATE_BUY = 3;
	public static final short STORE_PRIVATE_MANUFACTURE = 5;
	public static final short STORE_OBSERVING_GAMES = 7;
	public static final short STORE_PRIVATE_SELL_PACKAGE = 8;

	public static final int RANK_VAGABOND = 0;
	public static final int RANK_VASSAL = 1;
	public static final int RANK_HEIR = 2;
	public static final int RANK_KNIGHT = 3;
	public static final int RANK_WISEMAN = 4;
	public static final int RANK_BARON = 5;
	public static final int RANK_VISCOUNT = 6;
	public static final int RANK_COUNT = 7;
	public static final int RANK_MARQUIS = 8;
	public static final int RANK_DUKE = 9;
	public static final int RANK_GRAND_DUKE = 10;
	public static final int RANK_DISTINGUISHED_KING = 11;
	public static final int RANK_EMPEROR = 12;

	public static final int RELATION_PARTY1 = 0x00001;
	public static final int RELATION_PARTY2 = 0x00002;
	public static final int RELATION_PARTY3 = 0x00004;
	public static final int RELATION_PARTY4 = 0x00008;
	public static final int RELATION_PARTYLEADER = 0x00010;
	public static final int RELATION_HAS_PARTY = 0x00020;
	public static final int RELATION_CLAN_MEMBER = 0x00040;
	public static final int RELATION_LEADER = 0x00080;
	public static final int RELATION_CLAN_MATE = 0x00100;
	public static final int RELATION_INSIEGE = 0x00200;
	public static final int RELATION_ATTACKER = 0x00400;
	public static final int RELATION_ALLY = 0x00800;
	public static final int RELATION_ENEMY = 0x01000;
	public static final int RELATION_MUTUAL_WAR = 0x04000;
	public static final int RELATION_1SIDED_WAR = 0x08000;
	public static final int RELATION_ALLY_MEMBER = 0x10000;
	public static final int RELATION_TERRITORY_WAR = 0x80000;

	public static final int[] EXPERTISE_LEVELS = { 0, 20, 40, 52, 61, 76, 80, 84, Integer.MAX_VALUE };

	private static final Logger _log = Logger.getLogger("l2n.game.model.actor.L2Player");

	private L2GameClient _connection;

	private byte _level;

	private int _charId = 0x00030b7a;

	private long _exp = 0;
	private int _sp = 0;

	private double _rateEXP;
	private double _rateSP;

	private int _karma;

	private int _pvpKills;

	private int _nameColor;

	private int _title_color;

	private int _pkKills;

	private int _recomHave;
	private int _recomLeft;
	private final TIntArrayList _recomChars = new TIntArrayList();

	private int _curWeightPenalty = 0;

	private int _deleteTimer;
	private final PcInventory _inventory = new PcInventory(this);
	private PcWarehouse _warehouse = new PcWarehouse(this);

	public boolean isSitting = false;
	public boolean sittingTaskLaunched;
	private int _waitTimeWhenSit;

	private boolean autoLoot = Config.AUTO_LOOT;

	private boolean _relax;

	private int _face;

	private int _hairStyle;

	private int _hairColor;

	private final HashMap<String, QuestState> _quests = new HashMap<String, QuestState>();

	private final ShortCuts _shortCuts = new ShortCuts(this);

	private final MacroList _macroses = new MacroList(this);

	private L2TradeList _tradeList;
	private L2ManufactureList _createList;
	private ConcurrentLinkedQueue<TradeItem> _sellList;
	private ConcurrentLinkedQueue<TradeItem> _buyList;


	private short _privatestore;

	private final L2HennaInstance[] _henna = new L2HennaInstance[3];
	private short _hennaSTR;
	private short _hennaINT;
	private short _hennaDEX;
	private short _hennaMEN;
	private short _hennaWIT;
	private short _hennaCON;

	private L2Summon _summon = null;
	private L2GolemTraderInstance _merchant = null;

	private L2DecoyInstance _decoy = null;

	public L2Radar radar;

	private L2Party _party;

	private L2Clan _clan;

	private int _pledgeClass = 0;
	private int _pledgeType = 0;
	private int _powerGrade = 0;
	private int _lvlJoinedAcademy = 0;
	private int _apprentice = 0;

	private long _leaveClanTime;
	private long _deleteClanTime;

	private long _onlineTime;
	private long _onlineBeginTime;

	private long _NoChannel;
	private long _NoChannelBegin;

	private int _accessLevel;
	private PlayerAccess _playerAccess = new PlayerAccess();

	private boolean _messageRefusal = false;
	private boolean _tradeRefusal = false;
	private boolean _exchangeRefusal = false;

	private float _vitalityPoints = 1.0f;
	private int _vitalityLevel = 0;

	public static final int VITALITY_LEVELS[] = { 240, 1800, 14600, 18200, 20000 };
	public static final int MAX_VITALITY_POINTS = VITALITY_LEVELS[4];
	public static final int MIN_VITALITY_POINTS = 1;

	private L2ItemInstance _arrowItem;

	private L2Weapon _fistsWeaponItem;

	private long _uptime;
	private String _accountName;

	private TIntObjectHashMap<String> _chars = new TIntObjectHashMap<String>(8);

	private final Map<Integer, L2RecipeList> _recipebook = new TreeMap<Integer, L2RecipeList>();
	private final Map<Integer, L2RecipeList> _commonrecipebook = new TreeMap<Integer, L2RecipeList>();

	private boolean _logoutStarted = false;

	public int expertiseIndex = 0;
	public int weaponExpertisePenalty = 0;
	public int armorExpertisePenalty = 0;

	private boolean _isEnchanting = false;
	private L2ItemInstance _activeEnchantItem;
	private L2ItemInstance _activeEnchantSupportItem;
	private L2ItemInstance _activeEnchantAttrItem;
	private long _activeEnchantTimestamp = 0;

	private WarehouseType _usingWHType;
	private boolean _isOnline = false;
	private boolean _isDeleting = false;

	protected boolean _inventoryDisable = false;

	private GCSArray<L2CubicInstance> _cubics;

	private L2AgathionInstance _agathion;

	private Transaction _transaction;

	private IHardReference<L2NpcInstance> _lastNpc = HardReferences.emptyRef();

	private MultiSellListContainer _multisell = null;

	protected ConcurrentSkipListSet<Integer> _activeSoulShots = new ConcurrentSkipListSet<Integer>();

	private boolean _invisible = false;

	private L2WorldRegion _observerRegion;
	private int _observerMode = 0;

	public int _telemode = 0;
	public int _unstuck = 0;

	public Location _stablePoint = null;

	public int _loto[] = new int[5];
	public int _race[] = new int[2];

	private final TIntObjectHashMap<String> _blockList = new TIntObjectHashMap<String>(); // characters

	private boolean _blockAll = false;

	private boolean _isConnected = true;

	private boolean _hero = false;
	private int _team = 0;
	private boolean _checksForTeam = false;

	public int olyBuff = 0;

	private long _lastAccess;

	private L2Vehicle _vehicle;
	private Location _inVehiclePosition;

	private final ReentrantLock _storeLock = new ReentrantLock(), _subclassLock = new ReentrantLock();
	private final HashMap<Integer, L2SubClass> _classlist = new HashMap<Integer, L2SubClass>();
	protected int _baseClass;
	protected byte _baseLevel;
	protected long _baseExp;
	protected long _baseSp;
	protected L2SubClass _activeClass;

	private long _enterWorldTime;
	private long _charSelectTime;
	private long _charCaptchaTime = System.currentTimeMillis();

	private boolean _noble = false;
	private boolean _inOlympiadMode = false;
	private int _olympiadGameId = -1;
	private int _olympiadSide = -1;
	private int _olympiadObserveId = -1;

	private int _varka = 0;
	private int _ketra = 0;
	private int _ram = 0;

	private int _siegeState = 0;
	private int _siegeSide = 0;

	private byte[] _keyBindings;

	protected TIntLongHashMap statKills;
	protected TIntLongHashMap statDrop;
	protected TIntLongHashMap statCraft;

	private Forum _forumMemo;

	private int _cursedWeaponEquippedId = 0;

	private L2Fishing _fishCombat;
	private boolean _fishing = false;
	private Location _fishLoc = new Location();
	private L2ItemInstance _lure = null;

	private Bonus _bonus;
	private Future<ReturnTerritoryFlagTask> _returnTerritoryFlagTask = null;
	private Future<PvPFlagTask> _pvpRegTask;
	public Future<UserInfoTask> _userInfoTask;
	public Future<BroadcastCharInfoTask> _broadcastCharInfoTask;

	private Future<AutoSaveTask> _autoSaveTask;
	private ScheduledFuture<WaterTask> _taskWater;
	public ScheduledFuture<LookingForFishTask> _taskforfish;
	public ScheduledFuture<UnJailTask> _unjailTask;

	private boolean _isInCombatZone;
	private boolean _isOnSiegeField;
	private boolean _isInPeaceZone;
	private boolean _isInSSZone;

	private ResidenceType _inResidence = ResidenceType.None;

	private boolean _offline = false;
	private boolean _isFakePlayer = false;
	public boolean entering = true;

	private int _transformationId;
	private L2Transformation _transformation;

	private int pcBangPoints;

	private int _expandInventory = 0;
	private int _expandWarehouse = 0;

	private boolean _notShowBuffAnim = false;

	private int _buyListId;
	private int _incorrectValidateCount = 0;

	// The Fame of this L2Player
	private int _fame;

	protected final TIntObjectHashMap<L2Skill> _transformationSkills = new TIntObjectHashMap<L2Skill>();

	private TIntLongHashMap _talismanSkills;

	private boolean _isVipChar;

	private final TIntObjectHashMap<CharVariables> _variables = new TIntObjectHashMap<CharVariables>();

	private int _partyMatchingLevels;
	private int _partyMatchingRegion;
	private int _partyRoom = 0;

	private long _createTime;

	private int _movieId = 0;

	private int _territorySide = -1;
	public long _deathtime;
	private String _captcha = "";
	private int _captchaCount = 0;

	private GArray<String> bypasses = null, bypasses_bbs = null;
	private static final String NOT_CONNECTED = "<not connected>";

	private final PlayerRestrictions restrictions = new PlayerRestrictions();

	private boolean _isCaughtByRabbits = false;
	private long _lastRabbitsOkay = 0;

	public long _lastCheckByBotTracker = 0;
	public long _lastRabbitSpawnTime = 0;
	public long _lastMonsterKilled = 0;
	private int _captchaKill = 0;
	private boolean _passCheck = false;
    private boolean _isParalyzed = false;

	private boolean _community_auto_buff = false;
	private boolean _community_fixed_buff = false;
	private ArrayList<L2Skill> _community_buffs;
	private volatile long _community_last_time;
	private volatile long _communityBuffTime;

	public boolean isCommunityAutoBuff()
	{
		return _community_auto_buff && Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF;
	}

	public long getCommunityBuffTime()
	{
		return _communityBuffTime;
	}

	public void setCommunityBuffTime(final long time)
	{
		_communityBuffTime = System.currentTimeMillis() + time;
	}

	public long getCommunityLastTime()
	{
		return _community_last_time;
	}

	public void setCommunityLastTime()
	{
		_community_last_time = System.currentTimeMillis();
	}

	public boolean isCommunityFixedBuff()
	{
		return _community_fixed_buff;
	}

	public ArrayList<L2Skill> getCommunityBuffs()
	{
		return _community_buffs;
	}

	public void setCommunityAutoBuff(final boolean value)
	{
		_community_auto_buff = value;
	}

	public void setCommunityFixedBuff(final boolean value)
	{
		_community_fixed_buff = value;
	}

	public void setCommunityBuffs(final ArrayList<L2Skill> list)
	{
		_community_buffs = list;
	}

	public boolean isAllowEventQuestion()
	{
		return _reflection != ReflectionTable.JAIL && _reflection <= 0 && !isInOfflineMode() && !isInEvent(L2EventType.NONE) && !Olympiad.isRegisteredInComp(this) && !isDead();
	}

	public final boolean isInEvent(final L2EventType... types)
	{
		final Iterable<EventScript> scripts = Scripts.getInstance().getEventScripts(ScriptEventType.ON_IS_IN_EVENT_CHECK);
		for(final EventScript script : scripts)
			for(final L2EventType t : types)
				if(script.notifyIsInEventCheck(this, t))
					return true;
		return false;
	}
	protected volatile CharacterListenerList _listeners = CharacterListenerList.emptyList();
	public PlayerListenerList getListeners() {
		return _listeners.getPlayer();
	}
	public String getCaptcha()
	{
		return _captcha;
	}

	public void setCaptcha(final String captcha)
	{
		_captcha = captcha;
	}
    public void setCaptchaCountError(int captchaCount)
    {
        _captchaCount = captchaCount;
    }

    public int getCaptchaCountError()
    {
        return _captchaCount;
    }
	private L2Player(final int objectId, final L2PlayerTemplate template, final String accountName)
	{
		super(objectId, template, null);

		_accountName = accountName;
		_level = 1;
		_nameColor = 0xFFFFFF;
		_title_color = 0xFFFF77;
		_baseClass = getClassId().getId();

		_baseLevel = _level;
		_baseExp = _exp;
		_baseSp = _sp;
	}

	private L2Player(final int objectId, final L2PlayerTemplate template)
	{
		this(objectId, template, null);

		getInventory().restore();

		setAI(new L2PlayerAI(this));

		radar = new L2Radar(this);

		if(!Config.EVERYBODY_HAS_ADMIN_RIGHTS)
			setPlayerAccess(Config.gmlist.get(objectId));
		else
			setPlayerAccess(Config.gmlist.get(Integer.valueOf(0)));

		_macroses.restore();
	}

	@SuppressWarnings("unchecked")
	@Override
	public IHardReference<L2Player> getRef()
	{
		return (IHardReference<L2Player>) super.getRef();
	}

	public static L2Player create(final int classId, final byte sex, final String accountName, final String name, final byte hairStyle, final byte hairColor, final byte face)
	{
		final L2PlayerTemplate template = CharTemplateTable.getInstance().getTemplate(classId, sex != 0);

		final L2Player player = new L2Player(IdFactory.getInstance().getNextId(), template, accountName);

		player.setName(name);
		player.setTitle("");
		player.setHairStyle(hairStyle);
		player.setHairColor(hairColor);
		player.setFace(face);
		player.setCreateTime(System.currentTimeMillis());

		if(!player.createDb())
			return null;

		return player;
	}

	public String getAccountName()
	{
		if(_connection == null)
			return NOT_CONNECTED;
		return _connection.getLoginName();
	}

	public String getIP()
	{
		if(_connection == null)
			return NOT_CONNECTED;
		return _connection.getIpAddr();
	}

	public TIntObjectHashMap<String> getAccountChars()
	{
		return _chars;
	}

	@Override
	public final L2PlayerTemplate getTemplate()
	{
		return (L2PlayerTemplate) _template;
	}

	@Override
	public L2PlayerTemplate getBaseTemplate()
	{
		return (L2PlayerTemplate) _baseTemplate;
	}

	public void changeSex()
	{
		boolean male = true;
		if(getSex() == 1)
			male = false;
		_template = CharTemplateTable.getInstance().getTemplate(getClassId(), !male);
	}

	@Override
	public L2PlayableAI getAI()
	{
		if(_ai == null)
			_ai = new L2PlayerAI(this);
		return (L2PlayableAI) _ai;
	}

	public void doCubicsAndAgations(final L2Character target)
	{
		for(final L2CubicInstance cubic : getCubics())
			if(cubic.getType() != CubicType.LIFE_CUBIC)
				cubic.doAction(target);

		if(_agathion != null)
			_agathion.doAction(target);
	}

	@Override
	public void startAttackStanceTask()
	{
		final L2Object target = getTarget();
		if(target != null && target.isCharacter())
			doCubicsAndAgations((L2Character) target);
		super.startAttackStanceTask();
	}

	@Override
	public void updateEffectIcons()
	{
		if(entering || isLogoutStarted() || isMassUpdating())
			return;

		final L2Effect[] effects = getEffectList().getAllFirstEffects();
		Arrays.sort(effects, EffectsComparator.STATIC_INSTANCE);

		final PartySpelled ps = _party != null ? new PartySpelled(this) : null;
		final AbnormalStatusUpdate mi = new AbnormalStatusUpdate();

		for(final L2Effect effect : effects)
			if(effect != null && effect.isInUse())
			{
				if(effect.getStackType().equalsIgnoreCase("HpRecoverCast"))
					sendPacket(new ShortBuffStatusUpdate(effect));
				else
					effect.addIcon(mi);
				if(ps != null && _party != null)
					effect.addPartySpelledIcon(ps);
			}

		sendPacket(mi);
		if(ps != null && _party != null)
			_party.broadcastToPartyMembers(this, ps);

		if(Config.ENABLE_OLYMPIAD && isInOlympiadMode() && isOlympiadCompStart())
		{
			final OlympiadGame olymp_game = Olympiad.getOlympiadGame(getOlympiadGameId());
			if(olymp_game != null)
			{
				final ExOlympiadSpelledInfo OlympiadSpelledInfo = new ExOlympiadSpelledInfo();

				for(final L2Effect effect : effects)
					if(effect != null && effect.isInUse())
						effect.addOlympiadSpelledIcon(this, OlympiadSpelledInfo);

				if(olymp_game.getType() == CompType.CLASSED || olymp_game.getType() == CompType.NON_CLASSED)
					for(final L2Player member : olymp_game.getTeamMembers(this))
						member.sendPacket(OlympiadSpelledInfo);

				for(final L2Player member : olymp_game.getSpectators())
					member.sendPacket(OlympiadSpelledInfo);
			}
		}
	}

	@Override
	public void updateStats()
	{
		if(entering || isLogoutStarted())
			return;

		refreshOverloaded();
		refreshExpertisePenalty();
		super.updateStats();
	}

	@Override
	public void sendChanges()
	{
		if(entering || isLogoutStarted())
			return;
		super.sendChanges();
	}

	@Override
	public final byte getLevel()
	{
		return _level;
	}

	public final void setLevel(final int lvl)
	{
		_level = (byte) lvl;
	}

	public int getSex()
	{
		return getTemplate().isMale ? 0 : 1;
	}

	public int getFace()
	{
		return _face;
	}

	public void setFace(final int face)
	{
		_face = face;
	}

	public int getHairColor()
	{
		return _hairColor;
	}

	public void setHairColor(final int hairColor)
	{
		_hairColor = hairColor;
	}

	public int getHairStyle()
	{
		return _hairStyle;
	}

	public void setHairStyle(final int hairStyle)
	{
		_hairStyle = hairStyle;
	}

	public boolean isInStoreMode()
	{
		return _privatestore != STORE_PRIVATE_NONE && _privatestore != STORE_OBSERVING_GAMES;
	}

	public boolean isInCraftMode()
	{
		return _privatestore == STORE_PRIVATE_MANUFACTURE;
	}

	public void offlineAnswer(final int answer)
	{
		if(answer == 1)
		{
			if(Config.SERVICES_OFFLINE_REQUIRED_ITEM > 0 && Functions.getItemCount(this, Config.SERVICES_OFFLINE_REQUIRED_ITEM) <= 0)
			{
				sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_REQUIRED_ITEMS);
				return;
			}

			if(Config.SERVICES_OFFLINE_TRADE_PRICE > 0 && Config.SERVICES_OFFLINE_TRADE_PRICE_ITEM > 0)
			{
				if(Functions.getItemCount(this, Config.SERVICES_OFFLINE_TRADE_PRICE_ITEM) < Config.SERVICES_OFFLINE_TRADE_PRICE)
				{
					Functions.show(new CustomMessage("scripts.commands.user.offline.NotEnough", this).addItemName(Config.SERVICES_OFFLINE_TRADE_PRICE_ITEM).addNumber(Config.SERVICES_OFFLINE_TRADE_PRICE), this);
					return;
				}
				Functions.removeItem(this, Config.SERVICES_OFFLINE_TRADE_PRICE_ITEM, Config.SERVICES_OFFLINE_TRADE_PRICE);
			}

			offline();
		}
	}

	private void offline()
	{
		setNameColor(Config.SERVICES_OFFLINE_TRADE_NAME_COLOR);
		startAbnormalEffect(Config.SERVICES_OFFLINE_ABNORMAL_EFFECT);
		setOfflineMode(true);
		clearHateList(false);

		setVar("offline", String.valueOf(System.currentTimeMillis() / 1000));
		if(Config.SERVICES_OFFLINE_TRADE_DAYS_TO_KICK > 0)
			startKickTask(Config.SERVICES_OFFLINE_TRADE_DAYS_TO_KICK * 1000, true);

		if(isFestivalParticipant())
		{
			final L2Party playerParty = getParty();
			if(playerParty != null)
				playerParty.broadcastMessageToPartyMembers(getName() + " has been removed from the upcoming festival.");
		}

		if(getParty() != null)
			getParty().oustPartyMember(this);

		if(getPet() != null && getPet().getNpcId() != PetDataTable.IMPROVED_BABY_KOOKABURRA_ID && getPet().getNpcId() != PetDataTable.IMPROVED_BABY_COUGAR_ID)
			getPet().unSummon();

		CursedWeaponsManager.getInstance().doLogout(this);

		if(isInOlympiadMode() || getOlympiadGameId() > -1)
			Olympiad.logoutPlayer(this);

		stopWaterTask();
		stopAutoSaveTask();
		getListeners().onExit();
		sendPacket(Msg.LeaveWorld);
		setConnected(false);
		broadcastUserInfo(true);
		store(false);
	}

	public void logout(final boolean shutdown, final boolean restart, final boolean kicked, final boolean instant)
	{
		if(isLogoutStarted())
			return;


		if(kicked && Config.ALLOW_CURSED_WEAPONS && Config.DROP_CURSED_WEAPONS_ON_KICK)
			if(isCursedWeaponEquipped())
			{
				_pvpFlag = 0;
				CursedWeaponsManager.getInstance().dropPlayer(this);
			}

		if(restart)
		{
			if(instant || Config.PLAYER_LOGOUT_INGAME_TIME == 0)
				deleteMe();
			else
				scheduleDelete(Config.PLAYER_LOGOUT_INGAME_TIME);
			if(_connection != null)
				_connection.setActiveChar(null);
		}
		else
		{
			final L2GameServerPacket sp = !shutdown && !kicked ? Msg.LeaveWorld : Msg.ServerClose;
			sendPacket(sp);
			if(_connection != null && _connection.getConnection() != null)
				_connection.getConnection().close(sp);
			if(instant || Config.PLAYER_LOGOUT_INGAME_TIME == 0)
				deleteMe();
			else
				scheduleDelete(Config.PLAYER_LOGOUT_INGAME_TIME);
		}

		_connection = null;
		setConnected(false);
		broadcastUserInfo(false);
	}

	public void prepareToLogout()
	{
		if(isFlying() && !checkLandingState())
			setLoc(MapRegionTable.getTeleToClosestTown(this));

		if(isCastingNow())
			abortCast();

		if(getDuel() != null)
			getDuel().onPlayerDefeat(this);

		if(isFestivalParticipant())
		{
			final L2Party playerParty = getParty();

			if(playerParty != null)
				playerParty.broadcastMessageToPartyMembers(getName() + " has been removed from the upcoming festival.");
		}

		CursedWeaponsManager.getInstance().doLogout(this);

		if(inObserverMode())
			if(getOlympiadGameId() == -1)
				leaveObserverMode();
			else
				leaveOlympiadObserverMode();

		if(isInOlympiadMode() || getOlympiadGameId() > -1)
			Olympiad.logoutPlayer(this);

		final Iterable<EventScript> scripts = Scripts.getInstance().getEventScripts(ScriptEventType.ON_DISCONNECT);
		for(final EventScript script : scripts)
			script.notifyDisconnect(this);

		if(Config.PETITIONING_ALLOWED)
			PetitionManager.getInstance().onLogoutPlayer(this);

		if(_stablePoint != null)
		{
			teleToLocation(_stablePoint);
			addAdena(_stablePoint.h);
			unsetVar("wyvern_moneyback");
		}

		if(_recomChars.isEmpty())
			unsetVar("recomChars");
		else
		{
			String recomList = Integer.toHexString(_recomChars.get(0));
			for(int i = 1; i < _recomChars.size(); i++)
				recomList += "," + Integer.toHexString(_recomChars.get(i));
			setVar("recomChars", recomList);
		}

		if(isInJail())
		{
			if(_unjailTask != null)
			{
				_unjailTask.cancel(false);
				_unjailTask = null;
			}
			final long secondsInJail = (System.currentTimeMillis() - _jailStartTime) / 1000;
			final long minutesInJail = secondsInJail / 60;
			long srok = Long.parseLong(getVar("jailed"));
			srok -= minutesInJail;
			if(srok < 1)
				srok = 1;
			setVar("jailed", String.valueOf(srok));
		}
		getListeners().onExit();
		if(isInParty())
			try
			{
				leaveParty();
			}
			catch(final Throwable t)
			{
				_log.log(Level.WARNING, "deletedMe(1340)", t);
			}

		if(getPet() != null)
			try
			{
				getPet().unSummon();
			}
			catch(final Throwable t)
			{
				_log.log(Level.WARNING, "deletedMe(1351)", t);
			}
	}

	public boolean isLogoutStarted()
	{
		return _logoutStarted;
	}

	public void setLogoutStarted(final boolean logoutStarted)
	{
		_logoutStarted = logoutStarted;
	}

	public Collection<L2RecipeList> getDwarvenRecipeBook()
	{
		return _recipebook.values();
	}

	public Collection<L2RecipeList> getCommonRecipeBook()
	{
		return _commonrecipebook.values();
	}

	public int recipesCount()
	{
		return _commonrecipebook.size() + _recipebook.size();
	}

	public boolean findRecipe(final L2RecipeList id)
	{
		return _recipebook.containsValue(id) || _commonrecipebook.containsValue(id);
	}

	public boolean findRecipe(final int id)
	{
		return _recipebook.containsKey(id) || _commonrecipebook.containsKey(id);
	}

	public void registerRecipe(final L2RecipeList recipe, final boolean saveDB)
	{
		if(recipe.isDwarvenRecipe())
			_recipebook.put(recipe.getId(), recipe);
		else
			_commonrecipebook.put(recipe.getId(), recipe);
		if(saveDB)
			mysql.set("REPLACE INTO character_recipebook (char_id, id) values(" + getObjectId() + "," + recipe.getId() + ")");
	}

	public void unregisterRecipe(final int recipeID)
	{
		if(_recipebook.containsKey(recipeID))
		{
			mysql.set("DELETE FROM `character_recipebook` WHERE `char_id`=" + getObjectId() + " AND `id`=" + recipeID + " LIMIT 1");
			_recipebook.remove(recipeID);
		}
		else if(_commonrecipebook.containsKey(recipeID))
		{
			mysql.set("DELETE FROM `character_recipebook` WHERE `char_id`=" + getObjectId() + " AND `id`=" + recipeID + " LIMIT 1");
			_commonrecipebook.remove(recipeID);
		}
		else
			_log.warning("Attempted to remove unknown RecipeList" + recipeID);
	}

	public QuestState getQuestState(final String quest)
	{
		return _quests != null ? _quests.get(quest) : null;
	}

	public QuestState getQuestState(final Class<? extends Quest> quest)
	{
		return getQuestState(quest.getSimpleName());
	}

	public boolean isQuestCompleted(final Class<? extends Quest> quest)
	{
		return isQuestCompleted(quest.getSimpleName());
	}

	public boolean isQuestCompleted(final String quest)
	{
		final QuestState q = getQuestState(quest);
		return q != null && q.isCompleted();
	}

	public void setQuestState(final QuestState qs)
	{
		_quests.put(qs.getQuest().getName(), qs);
	}

	public void delQuestState(final String quest)
	{
		_quests.remove(quest);
	}

	public Quest[] getAllActiveQuests()
	{
		final GArray<Quest> quests = new GArray<Quest>();
		for(final QuestState qs : _quests.values())
			if(qs != null && qs.isStarted())
				quests.add(qs.getQuest());
		return quests.toArray(new Quest[quests.size()]);
	}

	public QuestState[] getAllQuestsStates()
	{
		return _quests.values().toArray(new QuestState[_quests.size()]);
	}

	public GArray<QuestState> getQuestsForEvent(final L2NpcInstance npc, final QuestEventType event)
	{
		final Quest quests[] = npc.getTemplate().getEventQuests(event);
		if(quests != null && quests.length > 0)
		{
			final GArray<QuestState> states = new GArray<QuestState>();
			for(final Quest quest : quests)
				if(getQuestState(quest.getName()) != null && !getQuestState(quest.getName()).isCompleted())
					states.add(getQuestState(quest.getName()));
			return states;
		}
		return GArray.emptyCollection();
	}

	public void processQuestEvent(final String quest, String event, final L2NpcInstance npc)
	{
		if(event == null)
			event = "";
		QuestState qs = getQuestState(quest);
		if(qs == null)
		{
			final Quest q = QuestManager.getQuest(quest);
			if(q == null)
			{
				_log.warning("Quest '" + quest + "' event: " + event + " not found! from npc: " + npc.toString());
				return;
			}
			qs = q.newQuestState(this, Quest.CREATED);
		}
		if(qs == null || qs.isCompleted())
			return;
		qs.getQuest().notifyEvent(event, qs, npc);
		sendPacket(new QuestList(this));
	}

	public boolean isQuestContinuationPossible(final boolean msg)
	{
		if(getWeightPenalty() >= 3 || getInventoryLimit() * 0.8 < getInventory().getSize())
		{
			if(msg)
				sendPacket(Msg.INVENTORY_LESS_THAN_80_PERCENT);
			return false;
		}
		return true;
	}

	public Collection<L2ShortCut> getAllShortCuts()
	{
		return _shortCuts.getAllShortCuts();
	}

	public L2ShortCut getShortCut(final int slot, final int page)
	{
		return _shortCuts.getShortCut(slot, page);
	}

	public void registerShortCut(final L2ShortCut shortcut)
	{
		_shortCuts.registerShortCut(shortcut);
	}

	public void deleteShortCut(final int slot, final int page)
	{
		_shortCuts.deleteShortCut(slot, page);
	}

	public void registerMacro(final L2Macro macro)
	{
		_macroses.registerMacro(macro);
	}

	public void deleteMacro(final int id)
	{
		_macroses.deleteMacro(id);
	}

	public MacroList getMacroses()
	{
		return _macroses;
	}

	public int getSiegeState()
	{
		return _siegeState;
	}

	public void setSiegeState(final int siegeState)
	{
		_siegeState = siegeState;
		broadcastRelationChanged();
	}

	public void setSiegeSide(final int val)
	{
		_siegeSide = val;
	}

	public boolean isRegisteredOnThisSiegeField(final int val)
	{
		if(_siegeSide != val && (_siegeSide < 81 || _siegeSide > 89))
			return false;
		return true;
	}

	public int getSiegeSide()
	{
		return _siegeSide;
	}

	public boolean isCastleLord(final int castleId)
	{
		return _clan != null && isClanLeader() && _clan.getHasCastle() == castleId;
	}

	public boolean isFortressLord(final int fortressId)
	{
		return _clan != null && isClanLeader() && _clan.getHasFortress() == fortressId;
	}

	public int getPkKills()
	{
		return _pkKills;
	}

	public void setPkKills(final int pkKills)
	{
		_pkKills = pkKills;
	}

	public int getDeleteTimer()
	{
		return _deleteTimer;
	}

	public void setDeleteTimer(final int deleteTimer)
	{
		_deleteTimer = deleteTimer;
	}

	public int getCurrentLoad()
	{
		return _inventory.getTotalWeight();
	}

	public long getLastAccess()
	{
		return _lastAccess;
	}

	public void setLastAccess(final long value)
	{
		_lastAccess = value;
	}

	public int getRecomHave()
	{
		return _recomHave;
	}

	public void setRecomHave(final int value)
	{
		if(value > 255)
			_recomHave = 255;
		else if(value < 0)
			_recomHave = 0;
		else
			_recomHave = value;
	}

	public int getRecomLeft()
	{
		return _recomLeft;
	}

	public void setRecomLeft(final int value)
	{
		_recomLeft = value;
	}

	public void giveRecom(final L2Player target)
	{
		final int targetRecom = target.getRecomHave();
		if(targetRecom < 255)
			target.setRecomHave(targetRecom + 1);
		if(_recomLeft > 0)
			_recomLeft--;
		_recomChars.add(target.getObjectId());
	}

	public boolean canRecom(final L2Player target)
	{
		return !_recomChars.contains(target.getObjectId());
	}

	@Override
	public int getKarma()
	{
		return _karma;
	}

	public void setKarma(int karma)
	{
		if(karma < 0)
			karma = 0;

		if(_karma == karma)
			return;

		_karma = karma;

		if(karma > 0)
			for(final L2Character object : L2World.getAroundCharacters(this))
				if(object instanceof L2GuardInstance && object.getAI().getIntention() == CtrlIntention.AI_INTENTION_IDLE)
					object.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE, null, null);

		sendChanges();

		if(getPet() != null)
			getPet().broadcastCharInfo();
	}

	/**
	 * @return the max weight that the L2Player can load.
	 */
	public int getMaxLoad()
	{
		final int con = getCON();
		if(con < 1)
			return (int) (31000 * Config.MAXLOAD_MODIFIER);
		else if(con > 59)
			return (int) (176000 * Config.MAXLOAD_MODIFIER);
		else
			return (int) calcStat(Stats.MAX_LOAD, Math.pow(1.029993928, con) * 30495.627366 * Config.MAXLOAD_MODIFIER, this, null);
	}

	public int getArmorExpertisePenalty()
	{
		return getSkillLevel(6213, 0);
	}

	public int getWeaponsExpertisePenalty()
	{
		return getSkillLevel(6209, 0);
	}

	public int getWeightPenalty()
	{
		return _curWeightPenalty;
	}

	public void refreshOverloaded()
	{
		if(isMassUpdating() || getMaxLoad() <= 0)
			return;

		setOverloaded(getCurrentLoad() > getMaxLoad());
		final double weightproc = 100. * (getCurrentLoad() - calcStat(Stats.MAX_NO_PENALTY_LOAD, 0, this, null)) / getMaxLoad();
		int newWeightPenalty;

		if(weightproc < 50)
			newWeightPenalty = 0;
		else if(weightproc < 66.6)
			newWeightPenalty = 1;
		else if(weightproc < 80)
			newWeightPenalty = 2;
		else if(weightproc < 100)
			newWeightPenalty = 3;
		else
			newWeightPenalty = 4;

		if(_curWeightPenalty == newWeightPenalty)
			return;

		_curWeightPenalty = newWeightPenalty;
		if(_curWeightPenalty > 0)
			super.addSkill(SkillTable.getInstance().getInfo(4270, _curWeightPenalty));
		else
			super.removeSkill(getKnownSkill(4270));

		sendPacket(new EtcStatusUpdate(this));
	}

	public void refreshExpertisePenalty()
	{
		if(isLogoutStarted())
			return;

		final int level = (int) calcStat(Stats.GRADE_EXPERTISE_LEVEL, getLevel(), null, null);
		int i = 0;
		for(i = 0; i < EXPERTISE_LEVELS.length; i++)
			if(level < EXPERTISE_LEVELS[i + 1])
				break;

		boolean skillUpdate = false;
		if(expertiseIndex != i)
		{
			expertiseIndex = i;
			if(expertiseIndex > 0)
			{
				addSkill(SkillTable.getInstance().getInfo(239, expertiseIndex), false);
				skillUpdate = true;
			}
		}

		checkItemsPenalty(skillUpdate);
	}

	private void checkItemsPenalty(boolean skillUpdate)
	{
		if(Config.DISABLE_GRADE_PENALTY)
			return;

		int newArmorPenalty = 0;
		int newWeaponPenalty = 0;
		GArray<L2ItemInstance> items = getInventory().getPaperdollItems();
		for(L2ItemInstance f : items)
		{
			if(f == null || !f.isEquipped() || f.getCrystalType().ordinal() <= expertiseIndex)
				continue;

			final int crystaltype = f.getCrystalType().ordinal();
			if(f.isAccessory() || f.isArmor())
			{
				if(crystaltype > newArmorPenalty)
					newArmorPenalty = crystaltype;
			}
			else if(f.isWeapon())
				if(crystaltype > newWeaponPenalty)
					newWeaponPenalty = crystaltype;
		}

		newWeaponPenalty = newWeaponPenalty - expertiseIndex;
		if(newWeaponPenalty <= 0)
			newWeaponPenalty = 0;
		else if(newWeaponPenalty >= 4)
			newWeaponPenalty = 4;
		
		newArmorPenalty = newArmorPenalty - expertiseIndex;
		if(newArmorPenalty <= 0)
			newArmorPenalty = 0;
		else if(newArmorPenalty >= 4)
			newArmorPenalty = 4;
		
		int weaponExpertise = getWeaponsExpertisePenalty();
		int armorExpertise = getArmorExpertisePenalty();

		if(weaponExpertise != newWeaponPenalty)

			{
				weaponExpertise = newWeaponPenalty;
				if(weaponExpertise > 0)
					super.addSkill(SkillTable.getInstance().getInfo(6209, weaponExpertise));
				else
					super.removeSkill(getKnownSkill(6209));

				skillUpdate = true;
			}
		if(armorExpertise != newArmorPenalty)
		{
			armorExpertise = newArmorPenalty;
			if(armorExpertise > 0)
				super.addSkill(SkillTable.getInstance().getInfo(6213, armorExpertise));
			else
				super.removeSkill(getKnownSkill(6213));
			skillUpdate = true;
		}

		if(skillUpdate)
		{
			sendPacket(new SkillList(this));
			sendPacket(new EtcStatusUpdate(this));
			updateStats();
		}
	}

	public int getPvpKills()
	{
		return _pvpKills;
	}

	public void setPvpKills(final int pvpKills)
	{
		_pvpKills = pvpKills;
	}

	public ClassId getClassId()
	{
		return getTemplate().classId;
	}

	public boolean isSummoner()
	{
		return getClassId().isSummoner();
	}

	public void addClanPointsOnProfession(final int id)
	{
		if(getLvlJoinedAcademy() != 0 && _clan != null && _clan.getLevel() >= 5 && ClassId.values()[id].getLevel() > 2)
		{
			int earnedPoints = 0;
			if(getLvlJoinedAcademy() <= 16)
				earnedPoints = Config.JOIN_ACADEMY_MAX_REP_SCORE;
			else if(getLvlJoinedAcademy() >= 39)
				earnedPoints = Config.JOIN_ACADEMY_MIN_REP_SCORE;
			else
				earnedPoints = Config.JOIN_ACADEMY_MAX_REP_SCORE - (getLvlJoinedAcademy() - 16) * 20;

			_clan.removeClanMember(getObjectId());
			final SystemMessage sm = new SystemMessage(SystemMessage.CLAN_ACADEMY_MEMBER_S1_HAS_SUCCESSFULLY_COMPLETED_THE_2ND_CLASS_TRANSFER_AND_OBTAINED_S2_CLAN_REPUTATION_POINTS);
			sm.addString(getName());
			sm.addNumber(_clan.incReputation(earnedPoints, true, "Academy"));
			_clan.broadcastToOnlineMembers(sm);
			_clan.broadcastToOtherOnlineMembers(new PledgeShowMemberListDelete(getName()), this);

			setLvlJoinedAcademy(0);
			setClan(null);
			setTitle("");
			sendPacket(Msg.CONGRATULATIONS_YOU_WILL_NOW_GRADUATE_FROM_THE_CLAN_ACADEMY_AND_LEAVE_YOUR_CURRENT_CLAN_AS_A_GRADUATE_OF_THE_ACADEMY_YOU_CAN_IMMEDIATELY_JOIN_A_CLAN_AS_A_REGULAR_MEMBER_WITHOUT_BEING_SUBJECT_TO_ANY_PENALTIES);
			setLeaveClanTime(0);

			broadcastCharInfo();

			sendPacket(Msg.PledgeShowMemberListDeleteAll);

			// Give academy circlet
			final L2ItemInstance academyCirclet = ItemTable.getInstance().createItem(8181, getObjectId(), 0, "L2Clan.completeAcademy");
			getInventory().addItem(academyCirclet);
			sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_EARNED_S1).addString("Academy Reward").addNumber(1));
		}
	}

	public synchronized void setClassId(final int id, final boolean noban)
	{
		if(!noban && !(ClassId.values()[id].equalsOrChildOf(ClassId.values()[getActiveClassId()]) || getPlayerAccess().CanChangeClass || Config.EVERYBODY_HAS_ADMIN_RIGHTS))
		{
			Thread.dumpStack();
			Util.handleIllegalPlayerAction(this, "L2Player[1989]", "tried to change class " + getActiveClass() + " to " + id, IllegalPlayerAction.CRITICAL);
			return;
		}

		// Если новый ID не принадлежит имеющимся классам значит это новая профа
		if(!_classlist.containsKey(id))
		{
			final L2SubClass cclass = getActiveClass();
			if(cclass == null) // не знаю как это возможно но тикет был
				return;
			_classlist.remove(getActiveClassId());
			changeClassInDb(cclass.getClassId(), id);
			if(cclass.isBase())
			{
				setBaseClass(id);
				addClanPointsOnProfession(id);
				L2ItemInstance coupons = null;
				L2ItemInstance newAdena = null;
				if(ClassId.values()[id].getLevel() == 2)
				{
					coupons = ItemTable.getInstance().createItem(8869, getObjectId(), 0, "ClassMaster");
					// пока нет не какой  инфы что за 1 профу после ее покупки даеться адена 
					//newAdena = ItemTable.getInstance().createItem(57, getObjectId(), 0, "ClassMaster");
					//newAdena.setCount(163800);
					unsetVar("newbieweapon");
					unsetVar("p1q2");
					unsetVar("p1q3");
					unsetVar("p1q4");
					unsetVar("prof1");
					unsetVar("ng1");
					unsetVar("ng2");
					unsetVar("ng3");
					unsetVar("ng4");
				}
				else if(ClassId.values()[id].getLevel() == 3)
				{
					coupons = ItemTable.getInstance().createItem(8870, getObjectId(), 0, "ClassMaster");
					unsetVar("dd");
					unsetVar("dd1"); // удаляем отметки о выдаче дименшен даймондов
					unsetVar("dd2");
					unsetVar("dd3");
					unsetVar("prof2.1");
					unsetVar("prof2.2");
					unsetVar("prof2.3");
					unsetVar("newbiearmor");
				}
				if(coupons != null)
				{
					// тоже относиться к выдачи Адены при покупке 1 профы
				//	if(newAdena != null)
				//	{
						//getInventory().addItem(newAdena);
					//	sendPacket(SystemMessage.obtainItems(newAdena));
				//	}
					coupons.setCount(15);
					getInventory().addItem(coupons);
					sendPacket(SystemMessage.obtainItems(coupons));
				}

				// Выдаём нублес если включено в конфигах
				if(ClassId.values()[id].getLevel() == 4 && Config.AUTO_CHARACTER_NOBLESSE)
				{
					setNoble(true, 1);
					Olympiad.addNoble(this);
				}
			}

			// Выдача итемов для трансфера скилов
			switch (ClassId.values()[id])
			{
				case cardinal:
					Functions.addItem(this, 15307, 1); // Cardinal (97)
					break;
				case evaSaint:
					Functions.addItem(this, 15308, 1); // Eva's Saint (105)
					break;
				case shillienSaint:
					Functions.addItem(this, 15309, 4); // Shillen Saint (112)
			}

			cclass.setClassId(id);
			_classlist.put(id, cclass);

			rewardSkills();

			storeCharSubClasses();

			// Социалка при получении профы
			broadcastPacket(new MagicSkillUse(this, this, 5103, 1, 1000, 0));

			if(Config.ALT_PROF_SOCIAL)
				broadcastPacket(new SocialAction(getObjectId(), Config.ALT_PROF_SOCIAL_ID));

			sendPacket(Quest.SOUND_FANFARE2);
			broadcastUserInfo(true);
		}

		final L2PlayerTemplate t = CharTemplateTable.getInstance().getTemplate(id, getSex() == 1);
		if(t == null)
		{
			_log.severe("Missing template for classId: " + id);
			// do not throw error - only print error
			return;
		}
		// Set the template of the L2Player
		setTemplate(t);

		// Update class icon in party and clan
		if(isInParty())
			getParty().broadcastToPartyMembers(new PartySmallWindowUpdate(this));
		if(getClan() != null)
			getClan().broadcastToOnlineMembers(new PledgeShowMemberListUpdate(this));
	}

	public void setClassId(final int id)
	{
		setClassId(id, false);
	}

	/**
	 * @return the Experience of the L2Player.<BR>
	 * <BR>
	 */
	public long getExp()
	{
		return Math.min(_exp, Long.MAX_VALUE);
	}

	/**
	 * @return the SP amount of the L2Player.<BR>
	 * <BR>
	 */
	public int getSp()
	{
		return Math.min(_sp, Integer.MAX_VALUE);
	}

	/**
	 * Set the SP amount of the L2Player.<BR>
	 * <BR>
	 */
	public void setSp(long spValue)
	{
		spValue = Math.max(spValue, 0);
		spValue = Math.min(spValue, Integer.MAX_VALUE);
		_sp = (int) spValue;
	}

	/**
	 * Добавлеят игроку SP
	 * 
	 * @param spValue
	 *            - сколько нужно добавить
	 */
	public void addSp(final long spValue)
	{
		setSp(getSp() + spValue);
	}

	/**
	 * Set the EXP amount of the L2Player.<BR>
	 * <BR>
	 */
	public void setExp(long expValue)
	{
		expValue = Math.max(expValue, 0);
		expValue = Math.min(expValue, Long.MAX_VALUE);
		_exp = expValue;
	}

	public void setActiveEnchantAttrItem(final L2ItemInstance stone)
	{
		_activeEnchantAttrItem = stone;
	}

	public L2ItemInstance getActiveEnchantAttrItem()
	{
		return _activeEnchantAttrItem;
	}

	public void setActiveEnchantItem(final L2ItemInstance scroll)
	{
		// If we dont have a Enchant Item, we are not enchanting.
		if(scroll == null)
		{
			setActiveEnchantSupportItem(null);
			setActiveEnchantTimestamp(0);
			setIsEnchanting(false);
		}
		_activeEnchantItem = scroll;
	}

	public L2ItemInstance getActiveEnchantItem()
	{
		return _activeEnchantItem;
	}

	public void setActiveEnchantSupportItem(final L2ItemInstance item)
	{
		_activeEnchantSupportItem = item;
	}

	public L2ItemInstance getActiveEnchantSupportItem()
	{
		return _activeEnchantSupportItem;
	}

	public long getActiveEnchantTimestamp()
	{
		return _activeEnchantTimestamp;
	}

	public void setActiveEnchantTimestamp(final long val)
	{
		_activeEnchantTimestamp = val;
	}

	public void setIsEnchanting(final boolean val)
	{
		_isEnchanting = val;
	}

	public boolean isEnchanting()
	{
		return _isEnchanting;
	}

	public void setFistsWeaponItem(final L2Weapon weaponItem)
	{
		_fistsWeaponItem = weaponItem;
	}

	public L2Weapon getFistsWeaponItem()
	{
		return _fistsWeaponItem;
	}

	public L2Weapon findFistsWeaponItem(final int classId)
	{
		// human fighter fists
		if(classId >= 0x00 && classId <= 0x09)
			return (L2Weapon) ItemTable.getInstance().getTemplate(246);

		// human mage fists
		if(classId >= 0x0a && classId <= 0x11)
			return (L2Weapon) ItemTable.getInstance().getTemplate(251);

		// elven fighter fists
		if(classId >= 0x12 && classId <= 0x18)
			return (L2Weapon) ItemTable.getInstance().getTemplate(244);

		// elven mage fists
		if(classId >= 0x19 && classId <= 0x1e)
			return (L2Weapon) ItemTable.getInstance().getTemplate(249);

		// dark elven fighter fists
		if(classId >= 0x1f && classId <= 0x25)
			return (L2Weapon) ItemTable.getInstance().getTemplate(245);

		// dark elven mage fists
		if(classId >= 0x26 && classId <= 0x2b)
			return (L2Weapon) ItemTable.getInstance().getTemplate(250);

		// orc fighter fists
		if(classId >= 0x2c && classId <= 0x30)
			return (L2Weapon) ItemTable.getInstance().getTemplate(248);

		// orc mage fists
		if(classId >= 0x31 && classId <= 0x34)
			return (L2Weapon) ItemTable.getInstance().getTemplate(252);

		// dwarven fists
		if(classId >= 0x35 && classId <= 0x39)
			return (L2Weapon) ItemTable.getInstance().getTemplate(247);

		return null;
	}

	public void addVitExpAndSp(long addToExp, long addToSp, final L2NpcInstance target, final boolean applyBonus, final boolean appyToPet)
	{
		if(target == null)
			return;

		if(!target.isRaid() && !target.isBoss() && Config.ENABLE_VITALITY)
			switch (getVitalityLevel())
			{
				case 0:
					break;
				case 1:
					addToExp *= Config.RATE_VITALITY_LEVEL_1;
					addToSp *= Config.RATE_VITALITY_LEVEL_1;
					break;
				case 2:
					addToExp *= Config.RATE_VITALITY_LEVEL_2;
					addToSp *= Config.RATE_VITALITY_LEVEL_2;
					break;
				case 3:
					addToExp *= Config.RATE_VITALITY_LEVEL_3;
					addToSp *= Config.RATE_VITALITY_LEVEL_3;
					break;
				case 4:
					addToExp *= Config.RATE_VITALITY_LEVEL_4;
					addToSp *= Config.RATE_VITALITY_LEVEL_4;
					break;
			}

		addExpAndSp(addToExp, addToSp, applyBonus, appyToPet);
	}

	@Override
	public void addExpAndSp(long addToExp, long addToSp, final boolean applyBonus, final boolean appyToPet)
	{
		if(_exp > Experience.LEVEL[getMaxLevel() + 1])
			_exp = Experience.LEVEL[getMaxLevel() + 1];

		if(applyBonus)
		{
			addToExp *= getRateExp();
			addToSp *= getRateSp();


		}

		if(addToExp > 0)
		{
			if(appyToPet)
			{
				final L2Summon pet = getPet();
				if(pet != null && !pet.isDead())
					// Sin Eater забирает всю экспу у персонажа
					if(pet.getNpcId() == PetDataTable.SIN_EATER_ID)
					{
						pet.addExpAndSp(addToExp, 0);
						addToExp = 0;
					}
					// if this player has a pet that takes from the owner's Exp, give the pet Exp now
					else if(pet.isPet() && pet.getExpPenalty() > 0)
					{
						float ratioTakenByPet = pet.getExpPenalty();
						final int delta = getLevel() - pet.getLevel();
						if(delta > 5)
						{
							ratioTakenByPet -= (delta - 5) * (ratioTakenByPet / 20);
							if(ratioTakenByPet < 0.02)
								ratioTakenByPet = 0.02f;
						}
						if(delta < -10)
						{
							ratioTakenByPet -= (delta + 10) * (ratioTakenByPet / 20);
							if(delta < -11)
								ratioTakenByPet += 0.1;
							if(ratioTakenByPet > 0.98)
								ratioTakenByPet = 0.98f;
						}

						if(ratioTakenByPet > 0)
						{
							// now adjust the max ratio to avoid the owner earning negative exp/sp
							if(ratioTakenByPet > 1)
								ratioTakenByPet = 1;
							pet.addExpAndSp((long) (addToExp * ratioTakenByPet), 0);
							addToExp *= 1f - ratioTakenByPet;
						}
					}
					else if(pet.isSummon())
						addToExp *= 1f - pet.getExpPenalty();
			}

			// Remove Karma when the player kills L2MonsterInstance
			if(!isCursedWeaponEquipped() && addToSp > 0 && _karma > 0)
				_karma -= addToSp / (Config.KARMA_SP_DIVIDER * Config.RATE_SP);

			if(_karma < 0)
				_karma = 0;

			// Проверка на отключение получения опыта
			final long max_xp = getVarB("NoExp") ? Experience.LEVEL[getLevel() + 1] - 1 : Experience.LEVEL[getMaxLevel() + 1];
			addToExp = Math.min(addToExp, max_xp - getExp());
		}

		// На всякий случай
		addToSp = Math.min(addToSp, Integer.MAX_VALUE);

		if(addToSp > 0 && addToExp == 0)
			sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_ACQUIRED_S1_SP).addNumber(addToSp));
		else if(addToSp > 0 || addToExp > 0)
			sendPacket(new SystemMessage(SystemMessage.YOU_EARNED_S1_EXP_AND_S2_SP).addNumber(addToExp).addNumber(addToSp));

		setExp(_exp + addToExp);
		setSp(_sp + addToSp);

		// запоминаем как уровень был, даьше нужен будет)
		final int level = getLevel();
		if(_exp >= Experience.LEVEL[_level + 1] && _level < getMaxLevel())
			increaseLevelAction();

		while (_exp >= Experience.LEVEL[_level + 1] && _level < getMaxLevel())
			increaseLevel();

		while (_exp < Experience.LEVEL[_level] && _level > 1)
			decreaseLevel();

		// Если уровень изменился
		if(level != getLevel())
		{
			// после установки уровня обновляем данные пати/клана и другое
			updatePlayerInfo(level > getLevel());
			// После установки уровня обновляем рейты, если уровень изменился
			updateRate();

			// Обновление скилов трансформации при изменении уровня игрока
			if(isTransformed())
				getTransform().onLevelUp(this);
		}

		sendChanges();
	}

	public void rewardSkills()
	{
		boolean update = false;
		if(Config.AUTO_LEARN_SKILLS)
		{
			if(isTransformed() || isCursedWeaponEquipped())
				return;

			int unLearnable = 0;
			L2SkillLearn[] skills = SkillTreeTable.getInstance().getAvailableSkills(this, getClassId());
			while (skills.length > unLearnable)
			{
				unLearnable = 0;
				for(final L2SkillLearn s : skills)
				{
					final L2Skill sk = SkillTable.getInstance().getInfo(s.id, s.skillLevel);
					if(sk == null || !sk.getCanLearn(getClassId()) || s.getMinLevel() > Config.AUTO_LEARN_SKILLS_MAX_LEVEL)
					{
						unLearnable++;
						continue;
					}
					addSkill(sk, true);
				}
				skills = SkillTreeTable.getInstance().getAvailableSkills(this, getClassId());
			}
			update = true;
		}
		else
			// Скиллы дающиеся бесплатно не требуют изучения
			for(final L2SkillLearn skill : SkillTreeTable.getInstance().getAvailableSkills(this, getClassId()))
				if(skill._repCost == 0 && skill._spCost == 0 && skill.itemCount == 0)
				{
					final L2Skill sk = SkillTable.getInstance().getInfo(skill.getId(), skill.getLevel());
					addSkill(sk, true);
					if(getAllShortCuts().size() > 0 && sk.getLevel() > 1)
						for(final L2ShortCut sc : getAllShortCuts())
							if(sc.id == sk.getId() && sc.type == L2ShortCut.TYPE_SKILL)
							{
								final L2ShortCut newsc = new L2ShortCut(sc.slot, sc.page, sc.type, sc.id, sk.getLevel());
								sendPacket(new ShortCutRegister(newsc));
								registerShortCut(newsc);
							}
					update = true;
				}

		if(update)
			sendPacket(new SkillList(this));

		// This function gets called on login, so not such a bad place to check weight
		// Update the overloaded status of the L2Player
		refreshOverloaded();
		refreshExpertisePenalty();
	}

	public Race getRace()
	{
		return getBaseTemplate().race;
	}

	/**
	 * @return the Clan Identifier of the L2Player.<BR>
	 * <BR>
	 */
	public int getClanId()
	{
		return _clan == null ? 0 : _clan.getClanId();
	}

	/**
	 * @return the Clan Crest Identifier (= Clan Identifier) of the L2Player or 0.<BR>
	 * <BR>
	 */
	@Override
	public int getClanCrestId()
	{
		return _clan == null ? 0 : _clan.getCrestId();
	}

	@Override
	public int getClanCrestLargeId()
	{
		return _clan == null ? 0 : _clan.getCrestLargeId();
	}

	public long getLeaveClanTime()
	{
		return _leaveClanTime;
	}

	public long getDeleteClanTime()
	{
		return _deleteClanTime;
	}

	public void setLeaveClanTime(final long time)
	{
		_leaveClanTime = time;
	}

	public void setDeleteClanTime(final long time)
	{
		_deleteClanTime = time;
	}

	public void setOnlineTime(final long time)
	{
		_onlineTime = time;
		_onlineBeginTime = System.currentTimeMillis();
	}

	public void setNoChannel(final long time)
	{
		_NoChannel = time;
		if(_NoChannel > 2145909600000L || _NoChannel < 0)
			_NoChannel = -1;

		if(_NoChannel > 0)
			_NoChannelBegin = System.currentTimeMillis();
		else
			_NoChannelBegin = 0;

		sendPacket(new EtcStatusUpdate(this));
	}

	public long getNoChannel()
	{
		return _NoChannel;
	}

	public long getNoChannelRemained()
	{
		if(_NoChannel == 0)
			return 0;
		else if(_NoChannel < 0)
			return -1;
		else
		{
			final long remained = _NoChannel - System.currentTimeMillis() + _NoChannelBegin;
			if(remained < 0)
				return 0;

			return remained;
		}
	}

	public void setLeaveClanCurTime()
	{
		_leaveClanTime = System.currentTimeMillis();
	}

	public void setDeleteClanCurTime()
	{
		_deleteClanTime = System.currentTimeMillis();
	}

	public boolean canJoinClan()
	{
		if(_leaveClanTime == 0)
			return true;
		if(System.currentTimeMillis() - _leaveClanTime >= Config.HOURS_BEFORE_JOIN_A_CLAN * 60 * 60 * 1000)
		{
			_leaveClanTime = 0;
			return true;
		}
		return false;
	}

	public boolean canCreateClan()
	{
		if(_deleteClanTime == 0)
			return true;
		if(System.currentTimeMillis() - _deleteClanTime >= Config.DAYS_BEFORE_CREATE_A_CLAN * 60 * 60 * 1000)
		{
			_deleteClanTime = 0;
			return true;
		}
		return false;
	}

	/**
	 * @return the PcInventory Inventory of the L2Player contained in _inventory.<BR>
	 * <BR>
	 */
	@Override
	public PcInventory getInventory()
	{
		return _inventory;
	}

	public L2ItemInstance addItem(final L2ItemInstance newItem)
	{
		return getInventory().addItem(newItem);
	}

	public L2ItemInstance addItem(final int itemId, final long count, final int source, final String create_type)
	{
		return getInventory().addItem(itemId, count, source, create_type);
	}

	public L2ItemInstance destroyItemByItemId(final int itemId, final long count, final boolean toLog)
	{
		return getInventory().destroyItemByItemId(itemId, count, toLog);
	}

	public void removeItemFromShortCut(final int objectId)
	{
		_shortCuts.deleteShortCutByObjectId(objectId);
	}

	public void removeSkillFromShortCut(final int skillId)
	{
		_shortCuts.deleteShortCutBySkillId(skillId);
	}

	@Override
	public boolean isSitting()
	{
		return inObserverMode() || isSitting;
	}

	public void setSitting(final boolean val)
	{
		isSitting = val;
	}

	public boolean getSittingTask()
	{
		return sittingTaskLaunched;
	}

	@Override
	public void sitDown()
	{
		if(isSitting() || sittingTaskLaunched || isAlikeDead())
			return;

		if(isStunned() || isSleeping() || isParalyzed() || isAttackingNow() || isCastingNow() || isMoving)
		{
			getAI().setNextAction(nextAction.REST, null, null, false, false);
			return;
		}

		resetWaitSitTime();
		getAI().setIntention(CtrlIntention.AI_INTENTION_REST, null, null);
		broadcastPacket(new ChangeWaitType(this, ChangeWaitType.WT_SITTING));
		sittingTaskLaunched = true;
		isSitting = true;
		L2GameThreadPools.getInstance().scheduleAi(new EndSitDownTask(this), 2500, true);
	}

	@Override
	public void standUp()
	{
		if(isSitting && !sittingTaskLaunched && !isInStoreMode() && !isAlikeDead())
		{
			if(_relax)
			{
				setRelax(false);
				getEffectList().stopEffects(EffectType.Relax);
			}
			getAI().clearNextAction();
			broadcastPacket(new ChangeWaitType(this, ChangeWaitType.WT_STANDING));
			sittingTaskLaunched = true;
			isSitting = true;
			L2GameThreadPools.getInstance().scheduleAi(new EndStandUpTask(this), 2500, true);
		}
	}

	/**
	 * Set the value of the _relax value. Must be True if using skill Relax and False if not.
	 */
	public void setRelax(final boolean val)
	{
		_relax = val;
	}

	/** update time counter when L2Player is sitting. */
	public void updateWaitSitTime()
	{
		if(_waitTimeWhenSit < 200)
			_waitTimeWhenSit += 2;
	}

	/** getting how long L2Player is sitting. */
	public int getWaitSitTime()
	{
		return _waitTimeWhenSit;
	}

	/** reset time counter */
	public void resetWaitSitTime()
	{
		_waitTimeWhenSit = 0;
	}

	public Warehouse getWarehouse()
	{
		return _warehouse;
	}

	public int getCharId()
	{
		return _charId;
	}

	public void setCharId(final int charId)
	{
		_charId = charId;
	}

	public long getAdena()
	{
		return _inventory.getAdena();
	}

	public L2ItemInstance reduceAdena(final long adena, final boolean notify)
	{
		if(adena > 0)
			sendPacket(new SystemMessage(SystemMessage.S1_ADENA_DISAPPEARED).addNumber(adena));
		return _inventory.reduceAdena(adena);
	}

	public L2ItemInstance addAdena(final long adena)
	{
		return _inventory.addAdena(adena);
	}

	public L2GameClient getNetConnection()
	{
		return _connection;
	}

	/**
	 * Set the active connection with the client.
	 */
	public void setNetConnection(final L2GameClient connection)
	{
		_connection = connection;
		_isConnected = connection != null && connection.isConnected();
	}

	/**
	 * Close the active connection with the client.
	 */
	public void closeNetConnection()
	{
		if(_connection != null)
			_connection.closeNow(false);
	}


	@Override
	public void onAction(final L2Player player, final boolean shift)
	{
		if(Events.onAction(player, this, shift))
		{
			player.sendActionFailed();
			return;
		}

		// Check if the other player already target this L2Player
		if(player.getTarget() != this)
		{
			player.setTarget(this);
			if(player.getTarget() == this)
				player.sendPacket(new MyTargetSelected(getObjectId(), 0)); // The color to display in the select window is White
			else
				sendActionFailed();
		}
		// Check if this L2Player has a Private Store
		else if(getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
			if(getDistance(player) > INTERACTION_DISTANCE && getAI().getIntention() != CtrlIntention.AI_INTENTION_INTERACT)
			{
				if(!shift)
					player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this, null);
			}
			else
				player.doInteract(this);
		else if(isAutoAttackable(player))
		{
			// Player with lvl < 21 can't attack a cursed weapon holder
			// And a cursed weapon holder can't attack players with lvl < 21
			if(isCursedWeaponEquipped() && player.getLevel() < 21 || player.isCursedWeaponEquipped() && getLevel() < 21)
				player.sendActionFailed();
			else
				player.getAI().Attack(this, false, shift);
		}
		else if(player != this && !shift)
		{
			// This Action Failed packet avoids activeChar getting stuck when clicking three or more times
			player.sendActionFailed();
			player.getAI().setIntention(CtrlIntention.AI_INTENTION_FOLLOW, this, Config.FOLLOW_RANGE);
		}
		else
			sendActionFailed();

		if(shift)
			// This Action Failed packet avoids activeChar getting stuck when clicking three or more times
			player.sendActionFailed();
	}

	public class UserInfoTask implements Runnable
	{
		@Override
		public void run()
		{
			sendUserInfoImpl();
			_userInfoTask = null;
		}
	}

	public class BroadcastCharInfoTask implements Runnable
	{
		@Override
		public void run()
		{
			broadcastCharInfoImpl();
			_broadcastCharInfoTask = null;
		}
	}

	@Override
	public void broadcastStatusUpdate()
	{
		// По идее еше должно срезать траффик. Будут глюки с отображением - убрать это условие.
		if(!needStatusUpdate())
			return;

		// Send the Server->Client packet StatusUpdate with current HP, MP and CP to this L2Player
		sendStatusUpdate(false, StatusUpdate.MAX_HP, StatusUpdate.MAX_MP, StatusUpdate.MAX_CP, StatusUpdate.CUR_HP, StatusUpdate.CUR_MP, StatusUpdate.CUR_CP);

		// Check if a party is in progress
		if(isInParty())
			getParty().broadcastToPartyMembers(this, new PartySmallWindowUpdate(this)); // Send the Server->Client packet PartySmallWindowUpdate with current HP, MP and Level to all other L2Player of the Party

		if(getDuel() != null)
			getDuel().broadcastToOppositTeam(this, new ExDuelUpdateUserInfo(this));

		if(!isInOlympiadMode() || !isOlympiadCompStart())
			return;
		final OlympiadGame game = Olympiad.getOlympiadGame(getOlympiadGameId());
		if(game != null)
			game.broadcastInfo(this, null, false);
	}

	@Override
	public void broadcastCharInfo()
	{
		broadcastUserInfo(false);
	}

	public void broadcastUserInfo(boolean force)
	{
		sendUserInfo(force);

		if(isInvisible() || isInOfflineMode())
			return;

		if(Config.BROADCAST_CHAR_INFO_INTERVAL == 0)
			force = true;

		if(force)
		{
			if(_broadcastCharInfoTask != null)
			{
				_broadcastCharInfoTask.cancel(false);
				_broadcastCharInfoTask = null;
			}
			broadcastCharInfoImpl();
			return;
		}

		if(_broadcastCharInfoTask != null)
			return;

		_broadcastCharInfoTask = L2GameThreadPools.getInstance().scheduleAi(new BroadcastCharInfoTask(), Config.BROADCAST_CHAR_INFO_INTERVAL, true);
	}

	public final L2GameServerPacket newCharInfo()
	{
		if(!isPolymorphed())
			return new CharInfo(this);
		if(getPolytype().equals("npc"))
			return new NpcInfoPoly(this);
		return new SpawnItemPoly(this);
	}

	private void broadcastCharInfoImpl()
	{
		if(isInvisible() || isInOfflineMode())
			return;

		final L2GameServerPacket ci = newCharInfo();
		for(final L2Player player : L2World.getAroundPlayers(this))
			if(player != null && player != this)
			{
				player.sendPacket(ci);
				player.sendPackets(RelationChanged.update(this, player));
			}
	}

	public void broadcastRelationChanged()
	{
		if(isInvisible() || isInOfflineMode())
			return;

		for(final L2Player target : L2World.getAroundPlayers(this))
			if(target != null && _objectId != target.getObjectId())
				target.sendPackets(RelationChanged.update(this, target));
	}

	public void sendUserInfo()
	{
		sendUserInfo(false);
	}

	public void sendUserInfo(final boolean force)
	{
		if(entering || isLogoutStarted() || isInOfflineMode())
			return;

		if(Config.USER_INFO_INTERVAL == 0 || force)
		{
			if(_userInfoTask != null)
			{
				_userInfoTask.cancel(false);
				_userInfoTask = null;
			}
			sendUserInfoImpl();
			return;
		}

		if(_userInfoTask != null)
			return;
		_userInfoTask = L2GameThreadPools.getInstance().scheduleAi(new UserInfoTask(), Config.USER_INFO_INTERVAL, true);
	}

	private void sendUserInfoImpl()
	{
		sendPacket(new UserInfo(this), new ExBrExtraUserInfo(this));
	}

	@Override
	public StatusUpdate makeStatusUpdate(final int... fields)
	{
		final StatusUpdate su = new StatusUpdate(getObjectId(), fields.length);
		for(final int field : fields)
			switch (field)
			{
				case StatusUpdate.CUR_HP:
					su.addAttribute(field, (int) getCurrentHp());
					break;
				case StatusUpdate.MAX_HP:
					su.addAttribute(field, getMaxHp());
					break;
				case StatusUpdate.CUR_MP:
					su.addAttribute(field, (int) getCurrentMp());
					break;
				case StatusUpdate.MAX_MP:
					su.addAttribute(field, getMaxMp());
					break;
				case StatusUpdate.CUR_LOAD:
					su.addAttribute(field, getCurrentLoad());
					break;
				case StatusUpdate.MAX_LOAD:
					su.addAttribute(field, getMaxLoad());
					break;
				case StatusUpdate.PVP_FLAG:
					su.addAttribute(field, _pvpFlag);
					break;
				case StatusUpdate.KARMA:
					su.addAttribute(field, getKarma());
					break;
				case StatusUpdate.CUR_CP:
					su.addAttribute(field, (int) getCurrentCp());
					break;
				case StatusUpdate.MAX_CP:
					su.addAttribute(field, getMaxCp());
					break;
				default:
					System.out.println("unknown StatusUpdate field: " + field);
					Thread.dumpStack();
			}
		return su;
	}

	public void sendStatusUpdate(final boolean broadCast, final int... fields)
	{
		if(fields.length == 0 || entering && !broadCast)
			return;

		final StatusUpdate su = makeStatusUpdate(fields);
		if(!su.hasAttributes())
			return;

		if(!broadCast)
			sendPacket(su);
		else if(entering)
			broadcastPacketToOthers(su);
		else
			broadcastPacket(su);
	}

	/**
	 * @return the Alliance Identifier of the L2Player.<BR>
	 * <BR>
	 */
	public int getAllyId()
	{
		return _clan == null ? 0 : _clan.getAllyId();
	}

	@Override
	public int getAllyCrestId()
	{
		return getAlliance() == null ? 0 : getAlliance().getAllyCrestId();
	}

	/** Send SystemMessage to player */
	public void sendPacket(final int messageId)
	{
		sendPacket(new SystemMessage(messageId));
	}

	/**
	 * Send a Server->Client packet StatusUpdate to the L2Player
	 */
	@Override
	public void sendPacket(final L2GameServerPacket... packets)
	{
		if(_isConnected && packets.length > 0)
			try
			{
				if(_connection != null)
					_connection.sendPacket(packets);
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "", e);
			}
	}

	/**
	 * Send a Server->Client packet StatusUpdate to the L2Player
	 */
	public void sendPackets(final Collection<L2GameServerPacket> packets)
	{
		if(_isConnected && packets != null && packets.size() > 0)
			try
			{
				if(_connection != null)
					_connection.sendPackets(packets);
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "", e);
			}
	}

	/**
	 * Send a Server->Client packet StatusUpdate to the L2Player
	 */
	public void sendPackets2(final Deque<L2GameServerPacket> packets)
	{
		if(_isConnected && packets != null && packets.size() > 0)
			try
			{
				if(_connection != null)
					_connection.sendPackets(packets);
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "", e);
			}
	}

	public void doInteract(final L2Object target)
	{
		if(target == null || isOutOfControl())
		{
			sendActionFailed();
			return;
		}

		if(target.isPlayer())
		{
			if(target.getDistance(this) <= INTERACTION_DISTANCE)
			{
				final L2Player temp = (L2Player) target;

				if(temp.getPrivateStoreType() == STORE_PRIVATE_SELL || temp.getPrivateStoreType() == STORE_PRIVATE_SELL_PACKAGE)
				{
					sendPacket(new PrivateStoreListSell(this, temp));
					sendActionFailed();
				}
				else if(temp.getPrivateStoreType() == STORE_PRIVATE_BUY)
				{
					sendPacket(new PrivateStoreListBuy(this, temp));
					sendActionFailed();
				}
				else if(temp.getPrivateStoreType() == STORE_PRIVATE_MANUFACTURE)
				{
					sendPacket(new RecipeShopSellList(this, temp));
					sendActionFailed();
				}
				sendActionFailed();
			}
			else if(getAI().getIntention() != CtrlIntention.AI_INTENTION_INTERACT)
				getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this, null);
		}
		else
			target.onAction(this, false);
	}

	public void doAutoLootOrDrop(final L2ItemInstance item, final L2NpcInstance fromNpc)
	{
		// Принудительный автолут: с летающих монстров, в отражении с автолутом, адены и если включён автолут адены
		final boolean forceAutoloot = fromNpc.isFlying() || getReflection().isAutoLootForced() || item.isAdena() && Config.AUTO_LOOT_ADENA;

		// Запрещаем автолут Проклятого оружия
		if(item.isCursed())
		{
			sendActionFailed();
			return;
		}

		if((fromNpc.isRaid() || fromNpc instanceof L2BossInstance || fromNpc instanceof L2ReflectionBossInstance) && !Config.AUTO_LOOT_RAIDBOSS && !item.isHerb() && !forceAutoloot)
		{
			item.dropToTheGround(this, fromNpc);
			return;
		}

		// Herbs
		if(item.isHerb())
		{
			if(!Config.AUTO_LOOT_HERBS && !forceAutoloot)
			{
				item.dropToTheGround(this, fromNpc);
				return;
			}

			final L2Skill[] skills = item.getItem().getAttachedSkills();
			if(skills != null && skills.length > 0)
				for(final L2Skill skill : skills)
				{
					altUseSkill(skill, this);
					if(getPet() != null && getPet().isSummon() && !getPet().isDead() && (item.getItemId() <= 8605 || item.getItemId() == 8614))
						getPet().altUseSkill(skill, getPet());
				}

			item.deleteMe();
			return;
		}

		if(!autoLoot && !forceAutoloot || item.isAdena() && !Config.AUTO_LOOT_ADENA)
		{
			item.dropToTheGround(this, fromNpc);
			return;
		}

		// Check if the L2Player is in a Party
		if(!isInParty())
		{
			if(!getInventory().validateWeight(item))
			{
				sendActionFailed();
				sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
				item.dropToTheGround(this, fromNpc);
				return;
			}

			if(!getInventory().validateCapacity(item))
			{
				sendActionFailed();
				sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
				item.dropToTheGround(this, fromNpc);
				return;
			}

			// Send a System Message to the L2Player
			sendPacket(SystemMessage.obtainItems(item));

			// Add the Item to the L2Player inventory
			final L2ItemInstance target2 = getInventory().addItem(item);
			Log.LogItem(this, fromNpc, Log.GetItemByAutoLoot, target2);

			sendChanges();
		}
		else if(item.getItemId() == L2Item.ITEM_ID_ADENA)
			// Distribute Adena between Party members
			getParty().distributeAdena(item, fromNpc, this);
		else
			// Distribute Item between Party members
			getParty().distributeItem(this, item, fromNpc);

		broadcastPickUpMsg(item);
	}

	@Override
	public void doPickupItem(final L2Object object)
	{
		getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE, null, null);

		// Check if the L2Object to pick up is a L2ItemInstance
		if(!object.isItem())
		{
			// dont try to pickup anything that is not an item :)
			_log.warning("trying to pickup wrong target." + getTarget());
			sendActionFailed();
			return;
		}

		sendActionFailed();
		stopMove();

		final L2ItemInstance item = (L2ItemInstance) object;
		if(item.getItem().isCombatFlag() && !FortressSiegeManager.checkIfCanPickup(this))
		{
			sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_FAILED_TO_PICK_UP_S1).addItemName(item.getItemId()));
			sendActionFailed();
			return;
		}

		synchronized (item)
		{
			// Check if me not owner of item and, if in party, not in owner party and nonowner pickup delay still active
			if(item.getDropTimeOwner() != 0 && item.getItemDropOwner() != null && item.getDropTimeOwner() > System.currentTimeMillis() && this != item.getItemDropOwner() && (!isInParty() || isInParty() && item.getItemDropOwner().isInParty() && getParty() != item.getItemDropOwner().getParty()))
			{
				SystemMessage sm;
				if(item.getItemId() == 57)
				{
					sm = new SystemMessage(SystemMessage.YOU_HAVE_FAILED_TO_PICK_UP_S1_ADENA);
					sm.addNumber(item.getCount());
				}
				else
				{
					sm = new SystemMessage(SystemMessage.YOU_HAVE_FAILED_TO_PICK_UP_S1);
					sm.addItemName(item.getItemId());
				}
				sendPacket(sm);
				return;
			}

			if(!item.isVisible())
				return;

			// Herbs
			if(item.isHerb())
			{
				final L2Skill[] skills = item.getItem().getAttachedSkills();
				if(skills != null && skills.length > 0)
					for(final L2Skill skill : skills)
					{
						altUseSkill(skill, this);
						if(getPet() != null && getPet().isSummon() && !getPet().isDead() && (item.getItemId() <= 8605 || item.getItemId() == 8614))
							getPet().altUseSkill(skill, getPet());
					}

				item.deleteMe();
				broadcastPacket(new GetItem(item, getObjectId()));
				return;
			}

			final boolean equip = (item.getCustomFlags() & L2ItemInstance.FLAG_EQUIP_ON_PICKUP) == L2ItemInstance.FLAG_EQUIP_ON_PICKUP;
			if(!isInParty() || equip)
			{
				if(!_inventory.validateWeight(item))
				{
					sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
					return;
				}

				if(!_inventory.validateCapacity(item))
				{
					sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
					return;
				}

				if(!item.pickupMe(this))
					return;

				sendPacket(SystemMessage.obtainItems(item));
				Log.LogItem(this, Log.PickupItem, getInventory().addItem(item));

				if(equip)
					getInventory().equipItem(item, true);

				sendChanges();
			}
			else if(item.getItemId() == 57)
			{
				if(!item.pickupMe(this))
					return;
				getParty().distributeAdena(item, this);
			}
			else
			{
				// Нужно обязательно сначало удалить предмет с земли.
				if(!item.pickupMe(null))
					return;
				getParty().distributeItem(this, item);
			}

			broadcastPacket(new GetItem(item, getObjectId()));
			broadcastPickUpMsg(item);
		}
	}

	@Override
	public void setTarget(L2Object newTarget)
	{
		// Check if the new target is visible
		if(newTarget != null && !newTarget.isVisible())
			newTarget = null;

		if(newTarget instanceof L2FestivalMonsterInstance && !isFestivalParticipant())
			newTarget = null;

		final L2Party party = getParty();
		if(party != null && party.isInDimensionalRift())
		{
			final int riftType = party.getDimensionalRift().getType();
			final int riftRoom = party.getDimensionalRift().getCurrentRoom();
			if(newTarget != null && !DimensionalRiftManager.getInstance().getRoom(riftType, riftRoom).checkIfInZone(newTarget.getX(), newTarget.getY(), newTarget.getZ()))
				newTarget = null;
		}

		// Get the current target
		final L2Object oldTarget = getTarget();
		if(oldTarget != null)
		{
			if(oldTarget.equals(newTarget))
				return; // no target change

			// Remove the L2Player from the _statusListener of the old target if it was a L2Character
			if(oldTarget.isCharacter())
				((L2Character) oldTarget).removeStatusListener(this);
			broadcastPacket(new TargetUnselected(this));
		}

		// Add the L2Player to the _statusListener of the new target if it's a L2Character
		if(newTarget != null)
		{
			if(newTarget.isCharacter())
				((L2Character) newTarget).addStatusListener(this);
			broadcastPacket(new TargetSelected(getObjectId(), newTarget.getObjectId(), getLoc()));
		}

		// Target the new L2Object (add the target to the L2Player _target, _knownObject and L2Player to _KnownObject of the L2Object)
		super.setTarget(newTarget);
	}

	/**
	 * @return the active weapon instance (always equipped in the right hand).<BR>
	 * <BR>
	 */
	@Override
	public L2ItemInstance getActiveWeaponInstance()
	{
		return getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);
	}

	/**
	 * @return the active weapon item (always equipped in the right hand).<BR>
	 * <BR>
	 */
	@Override
	public L2Weapon getActiveWeaponItem()
	{
		final L2ItemInstance weapon = getActiveWeaponInstance();
		if(weapon == null)
			return getFistsWeaponItem();
		return (L2Weapon) weapon.getItem();
	}

	/**
	 * @return the secondary weapon instance (always equipped in the left hand).<BR>
	 * <BR>
	 */
	@Override
	public L2ItemInstance getSecondaryWeaponInstance()
	{
		return getInventory().getPaperdollItem(Inventory.PAPERDOLL_LHAND);
	}

	/**
	 * @return the secondary weapon item (always equipped in the left hand) or the fists weapon.<BR>
	 * <BR>
	 */
	@Override
	public L2Weapon getSecondaryWeaponItem()
	{
		final L2ItemInstance weapon = getSecondaryWeaponInstance();

		if(weapon == null)
			return getFistsWeaponItem();

		final L2Item item = weapon.getItem();

		if(item instanceof L2Weapon)
			return (L2Weapon) item;

		return null;
	}

	public boolean isWearingArmor(final ArmorType armorType)
	{
		final L2ItemInstance chest = getInventory().getPaperdollItem(Inventory.PAPERDOLL_CHEST);
		if(chest == null)
			return armorType == ArmorType.NONE;

		if(chest.getItemType() != armorType)
			return false;

		if(chest.getBodyPart() == L2Item.SLOT_FULL_ARMOR)
			return true;

		final L2ItemInstance legs = getInventory().getPaperdollItem(Inventory.PAPERDOLL_LEGS);

		return legs == null ? armorType == ArmorType.NONE : legs.getItemType() == armorType;
	}

	/** Является ли чар ВИП чаром (имеет особые скиллы, рейт) (see vipchar.ini) */
	public boolean isVipChar()
	{
		return _isVipChar;
	}

	/** истанавливает статус VIP чару */
	public void setIsVipChar(final boolean b)
	{
		_isVipChar = b;
	}

	@Override
	public void reduceCurrentHp(double damage, final L2Character attacker, final L2Skill skill, final boolean awake, final boolean standUp, final boolean directHp, final boolean canReflect, final boolean isCounteAttack)
	{
		if(attacker == null || isDead() || attacker.isDead())
			return;

		// Check if the L2Player isn't Invulnerable
		if(isInvul() && attacker != this)
		{
			attacker.sendPacket(Msg.ATTACK_WAS_BLOCKED);
			return;
		}

		if(attacker.isPlayable() && isInZoneBattle() != attacker.isInZoneBattle())
		{
			attacker.getPlayer().sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		if(this != attacker && isInOlympiadMode() && !isOlympiadCompStart())
		{
			attacker.getPlayer().sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		if(this != attacker)
		{
			// Check and calculate transfered damage
			double trans = calcStat(Stats.TRANSFER_PET_DAMAGE_PERCENT, 0, attacker, skill);
			if(trans >= 1)
				if(_summon == null || _summon.isDead())
				{
					getEffectList().stopEffect(L2Skill.SKILL_TRANSFER_PAIN);
					getEffectList().stopEffect(711); // Divine Summoner Transfer Pain
					getEffectList().stopEffect(3667); // Yellow Talisman - Damage Transition
				}
				else if(_summon.isSummon() && _summon.isInRange(this, 1200))
				{
					trans *= damage * .01;
					damage -= trans;
					_summon.reduceCurrentHp(trans, attacker, null, false, false, false, false, false);
				}

			if(canReflect)
			{
				final L2Effect transferDam = getEffectList().getEffectByType(EffectType.TransferDam);
				if(transferDam != null)
				{
					final L2Character effector = transferDam.getEffector();
					if(effector == null || effector.isDead())
						getEffectList().stopEffects(EffectType.TransferDam);
					else if(effector.isInRange(this, 1200))
					{
						trans = transferDam.calc();
						trans *= damage * .01;
						damage -= trans;
						effector.reduceCurrentHp(trans, attacker, null, false, false, false, false, false);
					}
				}
			}
		}

		if(attacker != this)
			sendPacket(new SystemMessage(SystemMessage.S1_HAS_RECEIVED_DAMAGE_OF_S3_FROM_S2).addName(this).addName(attacker).addNumber((int) damage));

		final double hp = directHp ? getCurrentHp() : getCurrentHp() + getCurrentCp();
		if(getDuel() != null)
			if(getDuel() != attacker.getDuel())
				getDuel().setDuelState(getStoredId(), DuelState.Interrupted);
			else
			{
				if(getDuel().getDuelState(getStoredId()) == DuelState.Interrupted)
				{
					attacker.getPlayer().sendPacket(Msg.INCORRECT_TARGET);
					return;
				}
				if(damage >= hp)
				{
					setCurrentHp(1.0, false);
					getDuel().onPlayerDefeat(this);
					getDuel().stopFighting(attacker.getPlayer());
					return;
				}
			}

		if(isInOlympiadMode())
		{
			final OlympiadGame olymp_game = Olympiad.getOlympiadGame(getOlympiadGameId());
			if(olymp_game != null)
			{
				if(olymp_game.getState() <= 0)
				{
					attacker.getPlayer().sendPacket(Msg.INCORRECT_TARGET);
					return;
				}

				if(this != attacker)
					olymp_game.addDamage(this, Math.min(hp, damage));
				if(damage >= hp)
				{
					if(olymp_game.getType() != CompType.TEAM && olymp_game.getType() != CompType.TEAM_RANDOM)
					{
						olymp_game.setWinner(getOlympiadSide() == 1 ? 2 : 1);
						olymp_game.endGame(20000, false, true);
						setCurrentHp(1.0, false);
						attacker.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
						attacker.sendActionFailed();
						return;
					}
					if(olymp_game.doDie(this)) // Все умерли
					{
						olymp_game.setWinner(getOlympiadSide() == 1 ? 2 : 1);
						olymp_game.endGame(20000, false, true);
					}
				}
			}
			else
			{
				_log.warning("OlympiadGame id = " + getOlympiadGameId() + " is null");
				Thread.dumpStack();
			}
		}

		// Reduce the current HP of the L2Player
		super.reduceCurrentHp(damage, attacker, skill, awake, standUp, directHp, canReflect, isCounteAttack);

		// TODO: переделать на листенер
		if(getLevel() < 6 && getCurrentHpPercents() < 25)
		{
			final Quest q = QuestManager.getQuest(255);
			if(q != null)
				processQuestEvent(q.getName(), "CE45", null);
		}
	}

	private void altDeathPenalty(final L2Character killer)
	{
		// Reduce the Experience of the L2Player in function of the calculated Death Penalty
		if(!Config.ALT_GAME_DELEVEL)
			return;
		if(isInZoneBattle() && killer.isPlayable())
			return;
		deathPenalty(killer);
	}

	public final void doPurePk(final L2Player killer)
	{
		// Check if the attacker has a PK counter greater than 0
		final int pkCountMulti = Math.max(killer.getPkKills() / 2, 1);

		// Calculate the level difference Multiplier between attacker and killed L2Player
		// final int lvlDiffMulti = Math.max(killer.getLevel() / _level, 1);

		// Calculate the new Karma of the attacker : newKarma = baseKarma*pkCountMulti*lvlDiffMulti
		// Add karma to attacker and increase its PK counter
		killer.increaseKarma(Config.KARMA_MIN_KARMA * pkCountMulti); // * lvlDiffMulti);
		killer.setPkKills(killer.getPkKills() + 1);
	}

	public final void doKillInPeace(final L2Player killer)
	{
		// Check if the L2Player killed haven't Karma
		if(_karma <= 0)
			doPurePk(killer);
		else
			// Dead player have Karma
			// Increase the PvP Kills counter (Number of player killed during a PvP)
			killer.setPvpKills(killer.getPvpKills() + 1);
	}

	public static final GArray<L2ItemInstance> checkAddItemToDrop(final GArray<L2ItemInstance> array, final L2ItemInstance item, final double rate)
	{
		if(Rnd.get() >= rate)
			return array;

		if(Config.KARMA_LIST_NONDROPPABLE_ITEMS.contains(item.getItemId()))
			return array;

		array.add(item);

		return array;
	}

	private static final void manageHonorSystem(final L2Player killer, final L2Player victim, final boolean clanWar, final boolean pvp_zone)
	{
		if(!Config.ALT_HONOR_SYSTEM || killer == null || victim == null)
			return;

		final int diff = killer.getLevel() - victim.getLevel();
		if(diff > 20)
			return;

		if(clanWar)
		{
			victim.addItem(Config.ALT_HONOR_SYSTEM_LOSE_ITEMID, Config.ALT_HONOR_SYSTEM_LOSE_ITEM_COUNT, killer.getObjectId(), "PKPVPManage");
			killer.addItem(Config.ALT_HONOR_SYSTEM_WON_ITEMID, Config.ALT_HONOR_SYSTEM_WON_ITEM_COUNT, victim.getObjectId(), "PKPVPManage");
		}
		else if(pvp_zone && Config.ALT_HONOR_SYSTEM_IN_PVP_ZONE || !pvp_zone)
			killer.addItem(Config.ALT_HONOR_SYSTEM_PVP_ITEMID, Config.ALT_HONOR_SYSTEM_PVP_ITEM_COUNT, victim.getObjectId(), "PKPVPManage");
	}

	protected void doPKPVPManage(L2Character killer)
	{
		if(isCombatFlagEquipped())
		{
			L2ItemInstance flag = getActiveWeaponInstance();
			if(flag != null)
			{
				final int customFlags = flag.getCustomFlags();
				flag.setCustomFlags(0, false);
				flag = getInventory().dropItem(flag, flag.getCount(), true);
				flag.setCustomFlags(customFlags, false);

				if(isInZone(Siege))
					flag.dropMe(this, getLoc().rnd(0, 100, false));
				else if(!isInZone(Siege) && _clan != null && _clan.getSiege() != null && _clan.getSiege().isForterss())
				{
					final FortressSiege siege = (FortressSiege) _clan.getSiege();
					siege.returnFlag(this, flag);
				}
				sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_DROPPED_S1).addItemName(flag.getItemId()));
			}
		}

		if(isTerritoryFlagEquipped())
		{
			final L2ItemInstance flag = getActiveWeaponInstance();
			if(flag != null && flag.getCustomType1() != 77) // 77 это эвентовый флаг
			{
				final L2TerritoryFlagInstance flagNpc = TerritorySiege.getNpcFlagByItemId(flag.getItemId());
				flagNpc.drop(this);

				sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_DROPPED_S1).addItemName(flag.getItemId()));
				final String terrName = CastleManager.getInstance().getCastleByIndex(flagNpc.getBaseTerritoryId()).getName();
				TerritorySiege.announceToPlayer(new SystemMessage(2751).addString(terrName), true);
			}
		}

		for(L2ItemInstance item : getInventory().getItemsList())
			if((item.getCustomFlags() & L2ItemInstance.FLAG_ALWAYS_DROP_ON_DIE) == L2ItemInstance.FLAG_ALWAYS_DROP_ON_DIE)
			{
				item = getInventory().dropItem(item, item.getCount(), false);
				item.dropMe(this, getLoc().rnd(0, 100, false));
				sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_DROPPED_S1).addItemName(item.getItemId()));
			}

		if(killer == null || killer == _summon)
			return;

		if(killer.getObjectId() == _objectId)
			return;

		// если оба в PvP зоне
		if(killer.isPlayable() && killer.isInZone(ZoneType.pvp_zone) && isInZone(ZoneType.pvp_zone))
		{
			final L2Player pk = killer.getPlayer();
			pk.setPvpKills(pk.getPvpKills() + 1);
			manageHonorSystem(pk, this, false, true);
		}

		if(isInZoneBattle() || killer.isInZoneBattle())
			return;

		if(killer.isSummon0())
			killer = killer.getPlayer();

		// Processing Karma/PKCount/PvPCount for killer
		if(killer.isPlayer())
		{
			final L2Player playerKiller = killer.getPlayer();
			final boolean war = atMutualWarWith(playerKiller);

			manageHonorSystem(playerKiller, this, false, false);

			if(getLevel() > 4 && _clan != null && playerKiller.getClan() != null)
				if(war || _clan.getSiege() != null && _clan.getSiege() == playerKiller.getClan().getSiege() && (_clan.isDefender() && playerKiller.getClan().isAttacker() || _clan.isAttacker() && playerKiller.getClan().isDefender()))
				{
					manageHonorSystem(playerKiller, this, true, false);

					if(playerKiller.getClan().getReputationScore() > 0 && _clan.getLevel() >= 5 && _clan.getReputationScore() > 0 && playerKiller.getClan().getLevel() >= 5)
					{
						_clan.broadcastToOtherOnlineMembers(new SystemMessage(SystemMessage.YOUR_CLAN_MEMBER_S1_WAS_KILLED_S2_POINTS_HAVE_BEEN_DEDUCTED_FROM_YOUR_CLAN_REPUTATION_SCORE_AND_ADDED_TO_YOUR_OPPONENT_CLAN_REPUTATION_SCORE).addString(getName()).addNumber(-_clan.incReputation(-Config.REPUTATION_SCORE_PER_KILL, Config.USE_REDUCE_REPSCORE_RATE, "ClanWar")), this);
						playerKiller.getClan().broadcastToOtherOnlineMembers(new SystemMessage(SystemMessage.FOR_KILLING_AN_OPPOSING_CLAN_MEMBER_S1_POINTS_HAVE_BEEN_DEDUCTED_FROM_YOUR_OPPONENTS_CLAN_REPUTATION_SCORE).addNumber(playerKiller.getClan().incReputation(Config.REPUTATION_SCORE_PER_KILL, Config.USE_REDUCE_REPSCORE_RATE, "ClanWar")), playerKiller);
					}
				}

			if(isInZone(ZoneType.Siege))
			{
				if(playerKiller.getTerritorySiege() > -1 && getTerritorySiege() > -1 && playerKiller.getTerritorySiege() != getTerritorySiege() && playerKiller.getLevel() - getLevel() < 10 && playerKiller.getLevel() > 61 && getLevel() > 61)
				{
					if(getClanId() > 0 && getClanId() != playerKiller.getClanId() || getAllyId() > 0 && getAllyId() != playerKiller.getAllyId())
						return;
					final String var = playerKiller.getVar("badges" + playerKiller.getTerritorySiege());
					int badges = var == null ? 0 : Integer.parseInt(var);
					if(!isInParty())
						badges += Rnd.get(0, 1);
					else if(getParty() != playerKiller.getParty())
						badges += Rnd.get(0, 2);
					playerKiller.setVar("badges" + playerKiller.getTerritorySiege(), "" + badges);
				}
				return;
			}

			if(_pvpFlag > 0 || war)
				playerKiller.setPvpKills(playerKiller.getPvpKills() + 1);
			else
				doKillInPeace(playerKiller);

			// Send a Server->Client UserInfo packet to attacker with its PvP Kills Counter
			playerKiller.sendChanges();
		}

		final int karma = _karma;
		decreaseKarma(Config.KARMA_LOST_BASE);

		// TODO в нормальных условиях вещи теряются только при смерти от гварда или игрока
		final boolean isPvP = killer.isPlayable() || killer instanceof L2GuardInstance;

		if(!isPvP || _pkKills < Config.MIN_PK_TO_ITEMS_DROP || karma == 0 && Config.KARMA_NEEDED_TO_DROP || isFestivalParticipant())
			return;

		// No drop from GM's
		if(!Config.KARMA_DROP_GM && isGM())
			return;

		// нечего терять
		if(getInventory().getItemsList().isEmpty())
			return;

		final int max_drop_count = Config.KARMA_DROP_ITEM_LIMIT;

		double dropRate = _pkKills * Config.KARMA_DROPCHANCE_MULTIPLIER;

		if(dropRate < Config.KARMA_DROPCHANCE_MINIMUM)
			dropRate = Config.KARMA_DROPCHANCE_MINIMUM;

		dropRate /= 100.0;

		final double dropEquipRate = Config.KARMA_DROPCHANCE_EQUIPMENT / 100.0;
		final double dropWeaponRate = Config.KARMA_DROPCHANCE_EQUIPPED_WEAPON / 100.0;
		final double dropItemRate = Math.max(0, 1.0 - (dropEquipRate + dropWeaponRate));

		// _log.info("dropEquipRate=" + dropEquipRate + "; dropWeaponRate=" + dropWeaponRate + "; dropItemRate=" + dropItemRate);

		GArray<L2ItemInstance> dropped_items = new GArray<L2ItemInstance>();
		// Items to check = max_drop_count * 3; (Inventory + Weapon + Equipment)
		if(_inventory.getSize() > 0)
			for(byte i = 0, cycles = 100; i < max_drop_count * 3 && cycles > 0 && dropped_items.size() < max_drop_count; cycles--)
			{
				final L2ItemInstance random_item = _inventory.getItems()[Rnd.get(_inventory.getSize())];

				if(!random_item.canBeDropped(this))
					continue;

				if(random_item.getItem().getType1() == L2Item.TYPE1_ITEM_QUESTITEM_ADENA)
					continue;

				if(dropped_items.contains(random_item))
					continue;

				i++;

				if(random_item.isEquipped())
				{
					if(random_item.getItem().getType2() == L2Item.TYPE2_WEAPON)
						dropped_items = checkAddItemToDrop(dropped_items, random_item, dropWeaponRate * dropRate);
					else
						dropped_items = checkAddItemToDrop(dropped_items, random_item, dropEquipRate * dropRate);
				}
				else
					dropped_items = checkAddItemToDrop(dropped_items, random_item, dropItemRate * dropRate);
			}

		// Dropping items, if present
		if(dropped_items.isEmpty())
			return;

		for(L2ItemInstance item : dropped_items)
		{
			final long count = item.getCount();
			if(item.isEquipped())
			{
				getInventory().unEquipItemInBodySlotAndNotify(item.getBodyPart(), item);
				item.setBodyPart(item.getItem().getBodyPart());
			}

			item = _inventory.dropItem(item, count);

			if(killer.isPlayer() && Config.AUTO_LOOT && Config.AUTO_LOOT_PK)
				((L2Player) killer).getInventory().addItem(item);
			else if(killer.isSummon() && Config.AUTO_LOOT && Config.AUTO_LOOT_PK)
				killer.getPlayer().getInventory().addItem(item);
			else
			{
				Location pos = Rnd.coordsRandomize(this, Config.KARMA_RANDOM_DROP_LOCATION_LIMIT);
				for(byte i = 0; i < 20 && !GeoEngine.canMoveWithCollision(getX(), getY(), getZ(), pos.x, pos.y, pos.z); i++)
					pos = Rnd.coordsRandomize(this, Config.KARMA_RANDOM_DROP_LOCATION_LIMIT);
				if(!GeoEngine.canMoveWithCollision(getX(), getY(), getZ(), pos.x, pos.y, pos.z))
				{
					pos.x = killer.getX();
					pos.y = killer.getY();
					pos.z = killer.getZ();
				}
				item.dropMe(this, pos);
			}

			sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_DROPPED_S1).addItemName(item.getItemId()));
		}
		refreshOverloaded();
	}

	@Override
	public void doDie(final L2Character killer)
	{
		dieLock.lock();
		try
		{
			if(_killedAlreadyPlayer)
				return;
			setIsKilledAlreadyPlayer(true);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player.doDie(L2Character): ReentrantLock error", e);
		}
		finally
		{
			dieLock.unlock();
		}

		// Check for active charm of luck for death penalty
		getDeathPenalty().checkCharmOfLuck();

		if(isInTransaction())
		{
			if(getTransaction().isTypeOf(TransactionType.TRADE))
				sendPacket(SendTradeDone.Fail);
			getTransaction().cancel();
		}

		setPrivateStoreType(L2Player.STORE_PRIVATE_NONE);

		// Kill the L2Player
		super.doDie(killer);

		// Dont unsummon a summon, it can kill few enemies. But pet must returned back into its item
		// Unsummon siege summons
		if(_summon != null && (_summon.isPet() || _summon.isSiegeWeapon()))
			_summon.unSummon();

		if(_merchant != null)
			_merchant.unSummon();

		// Unsummon Cubics and agathion
		if(!isBlessedByNoblesse() && !isSalvation() && _cubics != null)
		{
			for(final L2CubicInstance cubic : _cubics)
				cubic.deleteMe();

			_cubics.clear();
			_cubics = null;
		}

		setAgathion(0);

		if(Config.LOG_KILLS)
		{
			final String coords = " at (" + getX() + "," + getY() + "," + getZ() + ")";
			if(killer.isNpc())
				Log.add("" + this + " karma " + _karma + " killed by mob " + killer.getNpcId() + coords, "kills");
			else if(killer.isSummon0() && killer.getPlayer() != null)
				Log.add("" + this + " karma " + _karma + " killed by summon of " + killer.getPlayer() + coords, "kills");
			else
				Log.add("" + this + " karma " + _karma + " killed by " + killer + coords, "kills");
		}

		if(Config.ALLOW_CURSED_WEAPONS)
			if(isCursedWeaponEquipped())
			{
				_pvpFlag = 0;
				CursedWeaponsManager.getInstance().dropPlayer(this);
				return;
			}
			else if(killer.isPlayer() && killer.isCursedWeaponEquipped())
			{
				_pvpFlag = 0;
				// noinspection ConstantConditions
				CursedWeaponsManager.getInstance().increaseKills(((L2Player) killer).getCursedWeaponEquippedId());
				return;
			}

		doPKPVPManage(killer);

		if(!killer.isPlayer())
			altDeathPenalty(killer);
		else if(killer.getPlayer() != this)
			altDeathPenalty(killer);

		// отправляем диалог для восскрешения
		if(isSalvation() && !isInOlympiadMode())
			reviveRequest(getPlayer(), _salvationPower, false);

		// And in the end of process notify death penalty that owner died :)
		getDeathPenalty().notifyDead(killer);

		setIncreasedForce(0);

		if(isInParty() && getParty().isInReflection() && getParty().getReflection().isDimensionalRift())
			((DimensionalRift) getParty().getReflection()).memberDead(this);

		stopWaterTask();

		// форсируем тут отключение инвула после смерти
		if(!isGM() && !getPlayerAccess().GodMode)
			setInvul(false);

		if(!isSalvation() && isInZone(Siege) && isCharmOfCourage())
		{
			_reviveRequested = 1;
			_revivePower = 100;
			sendPacket(new ConfirmDlgPacket(2306, 60000, ConfirmDlg.REVIVE_ANSWER));
			setCharmOfCourage(false);
		}

		if(getLevel() < 6)
		{
			final Quest q = QuestManager.getQuest(255);
			if(q != null)
				processQuestEvent(q.getName(), "CE30", null);
		}
	}

	/**
	 * Restore the Experience of the L2Player and send it a Server->Client StatusUpdate packet.<BR>
	 * <BR>
	 */
	public void restoreExp()
	{
		restoreExp(100.);
	}

	/**
	 * Restore the percentes of losted Experience of the L2Player.<BR>
	 * <BR>
	 */
	public void restoreExp(final double percent)
	{
		if(percent == 0)
			return;

		int lostexp = 0;
		final String lostexps = getVar("lostexp");
		if(lostexps != null)
		{
			lostexp = Integer.parseInt(lostexps);
			unsetVar("lostexp");
		}

		if(lostexp != 0)
			addExpAndSp((long) (lostexp * percent / 100), 0, false, false);
	}

	/**
	 * Reduce the Experience (and level if necessary) of the L2Player in function of the calculated Death Penalty.<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Calculate the Experience loss</li> <li>Set the value of _lostExp</li> <li>Set the new Experience value of the L2Player and Decrease its level if necessary</li> <li>Send a Server->Client StatusUpdate packet with its new Experience</li><BR>
	 * <BR>
	 */
	public void deathPenalty(final L2Character killer)
	{
		final boolean atwar = killer.getPlayer() != null ? atWarWith(killer.getPlayer()) : false;

		double deathPenaltyBonus = getDeathPenalty().getLevel() * Config.ALT_DEATH_PENALTY_C5_EXPERIENCE_PENALTY;
		if(deathPenaltyBonus < 2)
			deathPenaltyBonus = 1;
		else
			deathPenaltyBonus = deathPenaltyBonus / 2;

		// The death steal you some Exp
		double percentLost = 0.0;
		if(_level >= 76)
			percentLost = 2.0;
		else if(_level >= 40)
			percentLost = 4.0;
		else if(_level >= 20)
			percentLost = 7.0;
		else if(_level >= 10)
			percentLost = 8.87;

		if(Config.ALT_DEATH_PENALTY)
			percentLost = percentLost * Config.RATE_XP + _pkKills * Config.ALT_PK_DEATH_RATE;

		if(isFestivalParticipant() || atwar)
			percentLost = percentLost / 4.0;

		// Calculate the Experience loss
		int lostexp = (int) Math.round((Experience.LEVEL[_level + 1] - Experience.LEVEL[_level]) * percentLost / 100);
		lostexp *= deathPenaltyBonus;

		lostexp = (int) calcStat(Stats.EXP_LOST, lostexp, killer, null);

		// Потеря опыта на зарегистрированной осаде 1/4 от обычной смерти, с Charm of Courage нет потери
		// На чужой осаде - как при обычной смерти от *моба*
		if(isInZone(Siege))
		{
			final Siege siege = SiegeManager.getSiege(this, true);
			if(siege != null && siege.isParticipant(this))
				lostexp = 0;

			if(getTerritorySiege() > -1 && TerritorySiege.checkIfInZone(this))
				lostexp = 0;

			// После смерти на поле боя боевая мощь снижается (Battlefield Death Syndrome)
			final L2Effect effect = getEffectList().getFirstEffect(L2Skill.SKILL_BATTLEFIELD_DEATH_SYNDROME);
			if(effect != null)
			{
				final int syndromeLvl = effect.getSkill().getLevel();
				if(syndromeLvl < 5)
				{
					getEffectList().stopEffect(L2Skill.SKILL_BATTLEFIELD_DEATH_SYNDROME);
					final L2Skill skill = SkillTable.getInstance().getInfo(L2Skill.SKILL_BATTLEFIELD_DEATH_SYNDROME, syndromeLvl + 1);
					skill.getEffects(this, this, false, false);
				}
				else if(syndromeLvl == 5)
				{
					getEffectList().stopEffect(L2Skill.SKILL_BATTLEFIELD_DEATH_SYNDROME);
					final L2Skill skill = SkillTable.getInstance().getInfo(L2Skill.SKILL_BATTLEFIELD_DEATH_SYNDROME, 5);
					skill.getEffects(this, this, false, false);
				}
			}
			else
			{
				final L2Skill skill = SkillTable.getInstance().getInfo(L2Skill.SKILL_BATTLEFIELD_DEATH_SYNDROME, 1);
				if(skill != null)
					skill.getEffects(this, this, false, false);
			}
		}

		final long before = getExp();
		addExpAndSp(-lostexp, 0, false, false);
		final long lost = before - getExp();

		if(lost > 0)
			setVar("lostexp", String.valueOf(lost));
	}

	public void setPartyMatchingLevels(final int levels)
	{
		_partyMatchingLevels = levels;
	}

	public int getPartyMatchingLevels()
	{
		return _partyMatchingLevels;
	}

	public void setPartyMatchingRegion(final int region)
	{
		_partyMatchingRegion = region;
	}

	public int getPartyMatchingRegion()
	{
		return _partyMatchingRegion;
	}

	public int getPartyRoom()
	{
		return _partyRoom;
	}

	public void setPartyRoom(final int partyRoom)
	{
		_partyRoom = partyRoom;
	}

	/** истанавливаем игроку операцию */
	public void setTransaction(final Transaction transaction)
	{
		_transaction = transaction;
	}

	/** Возвращает текущую операцию пользователя */
	public Transaction getTransaction()
	{
		return _transaction;
	}

	/**
	 * @return True если операция ещё не закончилась.<BR>
	 * <BR>
	 */
	public boolean isInTransaction()
	{
		if(_transaction == null)
			return false;
		return _transaction.isInProgress();
	}

	public GArray<L2GameServerPacket> addVisibleObject(final L2Object object, final L2Character dropper)
	{
		if(isLogoutStarted() || object == null || object.getObjectId() == getObjectId() || !object.isVisible())
			return GArray.emptyCollection();

		if(object.isTrap() && !((L2TrapInstance) object).isDetected() && ((L2TrapInstance) object).getOwner() != this)
			return GArray.emptyCollection();

		final GArray<L2GameServerPacket> result = new GArray<L2GameServerPacket>();
		if(object.isPolymorphed() && object.getPolytype().equals("item"))
		{
			result.add(new SpawnItemPoly(object));
			showMoves(result, object);
			return result;
		}

		if(object.isPolymorphed() && object.getPolytype().equals("npc"))
		{
			result.add(new NpcInfoPoly(object));
			showMoves(result, object);
			return result;
		}

		if(object.isItem())
		{
			if(dropper != null)
				result.add(new DropItem((L2ItemInstance) object, dropper.getObjectId()));
			else
				result.add(new SpawnItem((L2ItemInstance) object));
			return result;
		}

		if(object.isDoor())
		{
			result.add(new StaticObject((L2DoorInstance) object));
			return result;
		}

		if(object.isStaticObject())
		{
			result.add(new StaticObject((L2StaticObjectInstance) object));
			return result;
		}

		if(object instanceof L2FenceInstance)
		{
			result.add(new ExColosseumFenceInfo((L2FenceInstance) object));
			return result;
		}

		if(object instanceof L2ClanHallManagerInstance)
			((L2ClanHallManagerInstance) object).sendDecoInfo(this);

		if(object.isNpc())
		{
			result.add(new NpcInfo((L2NpcInstance) object, this));
			showMoves(result, object);

			if(object.getAI() instanceof DefaultAI && !object.getAI().isActive())
				object.getAI().startAITask();

			// Т.к. у нас не создаются инстансы при каждом спавне, то все ок.
			if(((L2NpcInstance) object).hasRandomAnimation() && !object.isMonster() && object.getAI() instanceof L2CharacterAI) // для монстров через AI
				((L2NpcInstance) object).startRandomAnimation();

			return result;
		}

		if(object.isSummon0())
		{
			final L2Summon summon = (L2Summon) object;
			final L2Player owner = summon.getPlayer();

			if(owner != null && owner == this)
			{
				result.add(new PetInfo(summon, summon.getShowSpawnAnimation()));
				result.add(new PartySpelled(summon, true));

				if(summon.isPet())
					result.add(new PetItemList((L2PetInstance) summon));
			}
			else if(owner != null && getParty() != null && getParty().containsMember(owner))
			{
				result.add(new NpcInfo(summon, this, summon.getShowSpawnAnimation()));
				result.add(new PartySpelled(summon, true));
			}
			else
			{
				result.add(new NpcInfo(summon, this, summon.getShowSpawnAnimation()));
				if(owner != null)
				{
					final L2Party party = getParty();
					if(party != null && party == owner.getParty())
						result.add(new PartySpelled(summon, true));
					result.addAll(RelationChanged.update(owner, this));
				}
			}

			showMoves(result, object);
			return result;
		}

		if(object.isPlayer())
		{
			final L2Player otherPlayer = (L2Player) object;
			if(otherPlayer.isInvisible() && getObjectId() != otherPlayer.getObjectId())
				return result;

			// Не отображаем торговцев
			if(otherPlayer.getPrivateStoreType() != STORE_PRIVATE_NONE && getVarB("notraders"))
				return result;

			if(getObjectId() != otherPlayer.getObjectId())
				result.add(otherPlayer.newCharInfo());

			if(otherPlayer.getPrivateStoreType() != STORE_PRIVATE_NONE)
			{
				if(otherPlayer.getPrivateStoreType() == STORE_PRIVATE_BUY)
					result.add(new PrivateStoreMsgBuy(otherPlayer));
				else if(otherPlayer.getPrivateStoreType() == STORE_PRIVATE_SELL || otherPlayer.getPrivateStoreType() == STORE_PRIVATE_SELL_PACKAGE)
					result.add(new PrivateStoreMsgSell(otherPlayer));
				else if(otherPlayer.getPrivateStoreType() == STORE_PRIVATE_MANUFACTURE)
					result.add(new RecipeShopMsg(otherPlayer));

				if(isInZonePeace()) // Мирным торговцам не нужно посылать больше пакетов, для экономии траффика
					return result;
			}

			if(otherPlayer.isCastingNow())
			{
				final L2Character castingTarget = otherPlayer.getCastingTarget();
				final L2Skill castingSkill = otherPlayer.getCastingSkill();
				final long animationEndTime = otherPlayer.getAnimationEndTime();
				if(castingSkill != null && castingTarget != null && castingTarget.isCharacter() && otherPlayer.getAnimationEndTime() > 0)
					result.add(new MagicSkillUse(otherPlayer, castingTarget, castingSkill.getId(), castingSkill.getLevel(), (int) (animationEndTime - System.currentTimeMillis()), 0));
			}

			result.addAll(RelationChanged.update(otherPlayer, this));

			if(otherPlayer.isInVehicle())
				if(otherPlayer.getVehicle().isAirShip())
					result.add(new ExGetOnAirShip(otherPlayer, (L2AirShip) otherPlayer.getVehicle(), otherPlayer.getInVehiclePosition()));
				else
					result.add(new GetOnVehicle(otherPlayer, (L2Ship) otherPlayer.getVehicle(), otherPlayer.getInVehiclePosition()));
			else
				showMoves(result, object);
			return result;
		}

		if(object.isAirShip())
		{
			final L2AirShip boat = (L2AirShip) object;
			result.add(new ExAirShipInfo(boat));
			if(isInVehicle() && getVehicle() == boat)
				result.add(new ExGetOnAirShip(this, boat, getInVehiclePosition()));
			if(boat.isMoving)
				result.add(new ExMoveToLocationAirShip(boat, boat.getLoc(), boat.getDestination()));
		}
		else if(object.isShip())
		{
			final L2Ship boat = (L2Ship) object;
			result.add(new VehicleInfo(boat));
			if(isInVehicle() && getVehicle() == boat)
				result.add(new GetOnVehicle(this, boat, getInVehiclePosition()));
			if(boat.isMoving)
				result.add(new VehicleDeparture(boat));
		}

		return result;
	}

	public L2GameServerPacket removeVisibleObject(final L2Object object, final DeleteObject packet, final boolean deactivateAI)
	{
		if(isLogoutStarted() || object == null || object.getObjectId() == getObjectId())
			return null;
		if(isInVehicle() && getVehicle() == object)
			return null;

		if(deactivateAI && object.isNpc())
		{
			final L2NpcInstance npc = (L2NpcInstance) object;
			final L2WorldRegion region = npc.getCurrentRegion();
			final L2CharacterAI ai = npc.getAI();
			if(ai instanceof DefaultAI && ai.isActive() && !ai.isGlobalAI() && (region == null || region.areNeighborsEmpty()))
			{
				npc.setTarget(null);
				npc.stopMove();
				npc.getAI().stopAITask();
			}
		}

		final L2GameServerPacket result = packet == null ? new DeleteObject(object) : packet;

		getAI().notifyEvent(CtrlEvent.EVT_FORGET_OBJECT, object);
		return result;
	}

	public boolean isTransformed()
	{
		return _transformationId != 0;
	}

	private void showMoves(final GArray<L2GameServerPacket> result, final L2Object object)
	{
		if(object != null && object.isCharacter())
		{
			final L2Character obj = (L2Character) object;
			if(obj.isMoving || obj.isFollow)
				result.add(new CharMoveToLocation(obj));
		}
	}

	/**
	 * Manage the increase level task of a L2Player (Max MP, Max MP, Recommendation, Expertise).<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Send a Server->Client System Message to the L2Player : YOU_INCREASED_YOUR_LEVEL</li> <li>Send a Server->Client packet StatusUpdate to the L2Player with new LEVEL, MAX_HP and MAX_MP</li> <li>Set the current HP and MP of the L2Player,
	 * Launch/Stop a HP/MP/CP Regeneration Task and send StatusUpdate packet to all other L2Player to inform (exclusive broadcast)</li> <li>Recalculate the party level</li> <li>Give Expertise skill of this level</li><BR>
	 * <BR>
	 */
	private void increaseLevel()
	{
		if(_level >= Experience.getMaxLevel())
			return;
		_level++;

		final Quest q = QuestManager.getQuest(255);
		if(q != null)
			processQuestEvent(q.getName(), "CE40", null);
		if(Config.LEVEL_UP_NOTIFICATION)
		{
			final NpcHtmlMessage Reply = new NpcHtmlMessage(5);
			String content = "";		
			switch(_level)
			{
				case 20:
					Reply.setFile("data/html/addons/lvl/20.htm");
					break;
				case 40:
					Reply.setFile("data/html/addons/lvl/40.htm");
					break;
				case 76:
					Reply.setFile("data/html/addons/lvl/76.htm");
					break;
				case 85:
					Reply.setFile("data/html/addons/lvl/85.htm");
					break;
				default:
					return;
			}
			sendPacket(Reply);
		
		}
	}

	/**
	 * Manage the decrease level task of a L2Player (Max MP, Max MP, Recommendation, Expertise and beginner skills...).<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Send a Server->Client packet StatusUpdate to the L2Player with new LEVEL, MAX_HP and MAX_MP</li> <li>Recalculate the party level</li> <li>Recalculate the number of Recommendation that the L2Player can give</li> <li>Give Expertise skill of
	 * this level</li><BR>
	 * <BR>
	 */
	private void decreaseLevel()
	{
		if(_level == 1)
			return;
		_level--;
	}

	private void increaseLevelAction()
	{
		if(_level >= Experience.getMaxLevel())
			return;

		sendPacket(Msg.YOU_HAVE_INCREASED_YOUR_LEVEL);
		broadcastPacket(new SocialAction(getObjectId(), SocialAction.LEVEL_UP));
	}

	/**
	 * Обновляем значение рейтов
	 */
	private void updateRate()
	{
		_rateEXP = DynamicRateTable.getRateXP(_level);
		_rateSP = DynamicRateTable.getRateSP(_level);
	}

	/**
	 * Обвновляем информации о игроки выполняем необходимые действия при повышении/уменьшении уровня игрока
	 */
	private void updatePlayerInfo(final boolean decreaseLevel)
	{
		if(!decreaseLevel)
		{
			setCurrentHpMp(getMaxHp(), getMaxMp());
			setCurrentCp(getMaxCp());

			final Iterable<EventScript> scripts = Scripts.getInstance().getEventScripts(ScriptEventType.ON_LEVEL_INCREASE);
			for(final EventScript script : scripts)
				script.notifyLevelIncreased(this);
		}

		// Recalculate the party level
		if(isInParty())
			getParty().recalculatePartyData();

		if(_clan != null)
		{
			final PledgeShowMemberListUpdate memberUpdate = new PledgeShowMemberListUpdate(this);
			for(final L2Player clanMember : _clan.getOnlineMembers(null))
				clanMember.sendPacket(memberUpdate);
		}

		// Give Expertise skill of this level
		if(!isCursedWeaponEquipped() || !isTransformed())
			rewardSkills();

		if(decreaseLevel && Config.ALT_REMOVE_SKILLS_ON_DELEVEL)
			checkSkills(10);
	}

	/**
	 * Проверяет скилы, удаляет если уровень скила больше чем maxDiff уровня игрока, <BR>
	 * если уровен скила больше или равен 1, то скил добавляется снова, с предыдущим уровнем -1.
	 * 
	 * @param maxDiff
	 *            максимальная разница в уровнях
	 */
	public void checkSkills(final int maxDiff)
	{
		boolean changed = false;
		for(final L2Skill sk : getAllSkillsArray())
			if(SkillTreeTable.getInstance().checkSkill(this, sk, maxDiff))
				changed = true;
		if(changed)
			sendPacket(new SkillList(this));
	}

	/**
	 * Stops timers that are related to current instance of L2Player<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Set the RegenActive flag to False</li> <li>Stop the HP/MP/CP Regeneration task</li> <li>Stop cubics</li>
	 */
	public void stopAllTimers()
	{
		if(_cubics != null && !_cubics.isEmpty())
			for(final L2CubicInstance cubic : _cubics)
				cubic.deleteMe();

		setAgathion(0);
		stopWaterTask();
		stopLookingForFishTask();
		stopUnimportantTimers();
		cancelSkillTask(true);
		stopAutoSaveTask();
		PlayerTaskManager.getInstance().cancelPlayerTask(PlayerTaskType.KICK, _objectId);
		PlayerTaskManager.getInstance().cancelPlayerTask(PlayerTaskType.BONUS, _objectId);

		if(_returnTerritoryFlagTask != null)
		{
			_returnTerritoryFlagTask.cancel(false);
			_returnTerritoryFlagTask = null;
		}
	}

	public void stopUnimportantTimers()
	{
		if(_userInfoTask != null)
			_userInfoTask.cancel(false);
		_userInfoTask = null;
		if(_broadcastCharInfoTask != null)
			_broadcastCharInfoTask.cancel(false);
		_broadcastCharInfoTask = null;
	}

	/**
	 * @return the L2Summon of the L2Player or null.
	 */
	@Override
	public L2Summon getPet()
	{
		return _summon;
	}

	/**
	 * Set the L2Summon of the L2Player.<BR>
	 * <BR>
	 * 
	 * @param summon
	 *            L2Summon to set
	 */
	public void setPet(final L2Summon summon)
	{
		_summon = summon;
		rechargeAutoSoulShot();
		if(summon == null)
			getEffectList().stopEffect(4140);
	}

	/**
	 * идалит персонажа из мира через указанное время, если на момент истечения времени он не будет присоединен.
	 * TODO: через минуту делать его неуязвимым.
	 * TODO: сделать привязку времени к контексту, для зон с лимитом времени оставлять в игре на все время в зоне.
	 */
	public void scheduleDelete(final long time)
	{
		if(time == 0)
		{
			deleteMe();
			return;
		}

		// если игрок даже не загрузился до конца, не сохраняем
		if(!entering && _storeLock.tryLock())
			try
			{
				// получаем лишнее сохранение при логауте, но так надежнее
				L2GameClient.saveCharToDisk(this);
			}
			finally
			{
				_storeLock.unlock();
			}

		L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
		{
			@Override
			public void run()
			{
				if(getNetConnection() == null || !getNetConnection().isConnected())
					deleteMe();
			}
		}, time);
	}

	/**
	 * Manage the delete task of a L2Player (Leave Party, Unsummon pet, Save its inventory in the database, Remove it from the world...).<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>If the L2Player is in observer mode, set its position to its position before entering in observer mode</li> <li>Set the online Flag to True or False and update the characters table of the database with online status and lastAccess</li> <li>
	 * Stop the HP/MP/CP Regeneration task</li> <li>Cancel Crafting, Attak or Cast</li> <li>Remove the L2Player from the world</li> <li>Stop Party and Unsummon Pet</li> <li>Update database with items in its inventory and remove them from the world</li>
	 * <li>Remove all L2Object from _knownObjects and _knownPlayer of the L2Character then cancel Attak or Cast and notify AI</li> <li>Close the connection with the client</li><BR>
	 * <BR>
	 */
	@Override
	public void deleteMe()
	{
		if(isLogoutStarted())
			return;

		setLogoutStarted(true);
		prepareToLogout();

		// если игрок даже не загрузился до конца, не сохраняем
		if(!entering && _storeLock.tryLock())
			try
			{
				L2GameClient.saveCharToDisk(this);
			}
			finally
			{
				_storeLock.unlock();
			}

		// Останавливаем и запоминаем все квестовые таймеры
		// Quest.pauseQuestTimes(this);

		// удаляем до super.deleteMe(), так как потом его не будет в хранилище
		L2Player player;
		for(final TLongIterator iter = _snoopedPlayer.iterator(); iter.hasNext();)
		{
			player = L2ObjectsStorage.getAsPlayer(iter.next());
			if(player != null)
				player.removeSnooper(getStoredId());
		}
		for(final TLongIterator iter = _snoopListener.iterator(); iter.hasNext();)
		{
			player = L2ObjectsStorage.getAsPlayer(iter.next());
			if(player != null)
				player.removeSnooped(getStoredId());
		}

		// remove player from instance and set spawn location if any
		final Reflection r = getReflection();
		if(r != null)
			r.removeObject(this);

		super.deleteMe();

		_isDeleting = true;

		stopAllEffects();

		setMassUpdating(true);

		// Send friendlists to friends that this player has logged off
		EnterWorld.notifyFriends(this, false);

		if(isInTransaction())
			getTransaction().cancel();

		// Set the online Flag to True or False and update the characters table of the database with online status and lastAccess (called when login and logout)
		try
		{
			setOnlineStatus(false);
		}
		catch(final Throwable t)
		{
			_log.log(Level.WARNING, "deletedMe: setOnlineStatus", t);
		}

		// Stop the HP/MP/CP Regeneration task (scheduled tasks)
		try
		{
			stopAllTimers();
		}
		catch(final Throwable t)
		{
			_log.log(Level.WARNING, "deletedMe: stopAllTimers", t);
		}

		// Cancel Attak or Cast
		try
		{
			setTarget(null);
		}
		catch(final Throwable t)
		{
			_log.log(Level.WARNING, "deletedMe: setTarget", t);
		}

		if(_decoy != null)
			_decoy.unSummon();
		if(_merchant != null)
			_merchant.unSummon();

		setPartyRoom(0);

		try
		{
			if(getClanId() > 0 && _clan != null && _clan.getClanMember(getObjectId()) != null)
			{
				final int sponsor = _clan.getClanMember(getObjectId()).getSponsor();
				final int apprentice = getApprentice();
				final PledgeShowMemberListUpdate memberUpdate = new PledgeShowMemberListUpdate(this);
				for(final L2Player clanMember : _clan.getOnlineMembers(getName()))
				{
					if(clanMember.getObjectId() == getObjectId())
						continue;
					clanMember.sendPacket(memberUpdate);
					if(clanMember.getObjectId() == sponsor)
						clanMember.sendPacket(new SystemMessage(SystemMessage.S1_YOUR_CLAN_ACADEMYS_APPRENTICE_HAS_LOGGED_OUT).addString(getName()));
					else if(clanMember.getObjectId() == apprentice)
						clanMember.sendPacket(new SystemMessage(SystemMessage.S1_YOUR_CLAN_ACADEMYS_SPONSOR_HAS_LOGGED_OUT).addString(getName()));
				}
				_clan.getClanMember(getObjectId()).setPlayerInstance(null);
			}
		}
		catch(final Throwable t)
		{
			_log.log(Level.WARNING, "deletedMe(4860)", t);
		}

		try
		{
			if(isCombatFlagEquipped())
			{
				L2ItemInstance flag = getActiveWeaponInstance();
				if(flag != null)
				{
					final int customFlags = flag.getCustomFlags();
					flag.setCustomFlags(0, false);
					flag = getInventory().dropItem(flag, flag.getCount(), true);
					flag.setCustomFlags(customFlags, false);

					if(isInZone(Siege))
						flag.dropMe(this, getLoc().rnd(0, 100, false));
					else if(!isInZone(Siege) && _clan != null && _clan.getSiege() != null && _clan.getSiege().isForterss())
					{
						final FortressSiege siege = (FortressSiege) _clan.getSiege();
						siege.returnFlag(this, flag);
					}
				}
			}

			// territory ward on relog fix
			if(isTerritoryFlagEquipped())
			{
				final L2ItemInstance flag = getActiveWeaponInstance();
				if(flag != null && flag.getCustomType1() != 77) // 77 это эвентовый флаг
				{
					final L2TerritoryFlagInstance flagNpc = TerritorySiege.getNpcFlagByItemId(flag.getItemId());
					flagNpc.drop(this);
				}
			}

		}
		catch(final Throwable t)
		{
			_log.log(Level.WARNING, "deletedMe(4971)", t);
		}

		if(CursedWeaponsManager.getInstance().getCursedWeapon(getCursedWeaponEquippedId()) != null)
			CursedWeaponsManager.getInstance().getCursedWeapon(getCursedWeaponEquippedId()).setPlayer(null);

		if(getPartyRoom() > 0)
		{
			final PartyRoom room = PartyRoomManager.getInstance().getRooms().get(getPartyRoom());
			if(room != null)
				if(room.getLeader() == null || room.getLeader().equals(this))
					PartyRoomManager.getInstance().removeRoom(room.getId());
				else
					room.removeMember(this, false);
		}

		setPartyRoom(0);
		setEffectList(null);

		// Update database with items in its inventory and remove them from the world
		try
		{
			getInventory().deleteMe();
		}
		catch(final Throwable t)
		{
			_log.log(Level.WARNING, "deletedMe: remove inventory", t);
		}

		destroyAllTraps();

		_warehouse = null;
		_ai = null;
		if(_summon != null)
			_summon.unSummon();
		_summon = null;
		if(_merchant != null)
			_merchant.unSummon();
		_merchant = null;
		_arrowItem = null;
		_fistsWeaponItem = null;
		_chars = null;
		_activeEnchantItem = null;
		_activeEnchantSupportItem = null;
		_activeEnchantAttrItem = null;
		if(_agathion != null)
			_agathion.deleteMe();
		_agathion = null;
		_lastNpc = HardReferences.emptyRef();
		_observerRegion = null;
		bypasses_bbs = null;
		bypasses = null;
	}

	/**
	 * Set the _tradeList object of the L2Player.
	 */
	public void setTradeList(final L2TradeList x)
	{
		_tradeList = x;
	}

	/**
	 * @return the _tradeList object of the L2Player.
	 */
	public L2TradeList getTradeList()
	{
		return _tradeList;
	}

	/**
	 * Set the _sellList object of the L2Player.
	 */
	public void setSellList(final ConcurrentLinkedQueue<TradeItem> x)
	{
		_sellList = x;
		saveTradeList();
	}

	/**
	 * @return the _sellList object of the L2Player.
	 */
	public ConcurrentLinkedQueue<TradeItem> getSellList()
	{
		if(_sellList != null)
			return _sellList;
		return CollectionUtils.emptyConcurrentQueue();
	}

	/**
	 * @return the _createList object of the L2Player.
	 */
	public L2ManufactureList getCreateList()
	{
		return _createList;
	}

	/**
	 * Set the _createList object of the L2Player.
	 */
	public void setCreateList(final L2ManufactureList x)
	{
		_createList = x;
		saveTradeList();
	}

	/**
	 * Set the _buyList object of the L2Player.
	 */
	public void setBuyList(final ConcurrentLinkedQueue<TradeItem> x)
	{
		_buyList = x;
		saveTradeList();
	}

	/**
	 * @return the _buyList object of the L2Player.
	 */
	public ConcurrentLinkedQueue<TradeItem> getBuyList()
	{
		if(_buyList != null)
			return _buyList;
		return CollectionUtils.emptyConcurrentQueue();
	}

	/**
	 * Set the Private Store type of the L2Player.<BR>
	 * <BR>
	 * <B><U> Values </U> :</B><BR>
	 * <BR>
	 * <li>0 : STORE_PRIVATE_NONE</li><BR>
	 * <li>1 : STORE_PRIVATE_SELL</li><BR>
	 * <li>2 : sellmanage</li><BR>
	 * <li>3 : STORE_PRIVATE_BUY</li><BR>
	 * <li>4 : buymanage</li><BR>
	 * <li>5 : STORE_PRIVATE_MANUFACTURE</li><BR>
	 * <li>8 : STORE_PRIVATE_SELL_PACKAGE</li><BR>
	 */
	public void setPrivateStoreType(final short type)
	{
		_privatestore = type;
		if(type != STORE_PRIVATE_NONE && type != STORE_OBSERVING_GAMES)
			setVar("storemode", String.valueOf(type));
		else
			unsetVar("storemode");
	}

	/**
	 * @return the Private Store type of the L2Player.<BR>
	 * <BR>
	 *         <B><U> Values </U> :</B><BR>
	 * <BR>
	 *         <li>0 : STORE_PRIVATE_NONE</li> <li>1 : STORE_PRIVATE_SELL</li> <li>2 : sellmanage</li><BR>
	 *         <li>3 : STORE_PRIVATE_BUY</li><BR>
	 *         <li>4 : buymanage</li><BR>
	 *         <li>5 : STORE_PRIVATE_MANUFACTURE</li><BR>
	 *         <li>7 : STORE_OBSERVING_GAMES</li><BR>
	 *         <li>8 : STORE_PRIVATE_SELL_PACKAGE</li><BR>
	 */
	public short getPrivateStoreType()
	{
		if(inObserverMode())
			return STORE_OBSERVING_GAMES;
		return _privatestore;
	}

	/**
	 * Set the _clan object, _clanId, _clanLeader Flag and title of the L2Player.<BR>
	 * <BR>
	 * 
	 * @param clan
	 *            the clat to set
	 */
	public void setClan(final L2Clan clan)
	{
		if(_clan != clan && _clan != null)
			unsetVar("canWhWithdraw");

		final L2Clan oldClan = _clan;
		if(oldClan != null && clan == null)
		{
			// удаляем клановые скилы, фортов, территорий
			for(final L2Skill skill : oldClan.getAllSkills())
				removeSkill(skill, false);
			// удаляем скилы отрядов
			final TIntObjectHashMap<L2Skill> skills = oldClan.getSquadSkills().get(_pledgeType);
			if(skills != null && !skills.isEmpty())
				skills.forEachValue(new TObjectProcedure<L2Skill>()
				{
					@Override
					public boolean execute(final L2Skill skill)
					{
						removeSkill(skill, false);
						return true;
					}
				});
		}

		_clan = clan;

		if(clan == null)
		{
			_pledgeType = 0;
			_pledgeClass = 0;
			_powerGrade = 0;
			_lvlJoinedAcademy = 0;
			_apprentice = 0;
			if(_territorySide > -1)
			{
				_territorySide = -1;
				TerritorySiege.removePlayer(this);
			}
			getInventory().checkAllConditions();
			return;
		}

		if(!clan.isMember(getObjectId()))
		{
			// char has been kicked from clan
			_log.fine("Char " + getName() + " is kicked from clan: " + clan.getName());
			setClan(null);
			setTitle("");
			return;
		}

		TerritorySiege.removePlayer(this);
		setTerritorySiege(_clan.getTerritorySiege());
		setTitle("");
	}

	/** @return the _clan object of the L2Player. */
	public L2Clan getClan()
	{
		return _clan;
	}

	/** @return замок */
	public Castle getCastle()
	{
		return CastleManager.getInstance().getCastleByOwner(_clan);
	}

	/** @return кланхолл */
	public ClanHall getClanHall()
	{
		return ClanHallManager.getInstance().getClanHallByOwner(_clan);
	}

	/** @return форт */
	public Fortress getFortress()
	{
		return FortressManager.getInstance().getFortressByOwner(_clan);
	}

	public L2Alliance getAlliance()
	{
		return _clan == null ? null : _clan.getAlliance();
	}

	/**
	 * @return True if the L2Player is the leader of its clan.
	 */
	public boolean isClanLeader()
	{
		return _clan != null && _objectId == _clan.getLeaderId();
	}

	public boolean isAllyLeader()
	{
		return getAlliance() != null && getAlliance().getLeader().getLeaderId() == getObjectId();
	}

	/**
	 * Reduce the number of arrows owned by the L2Player.<BR>
	 * <BR>
	 */
	@Override
	public void reduceArrowCount()
	{
		final L2ItemInstance arrows = getInventory().destroyItem(getInventory().getPaperdollObjectId(Inventory.PAPERDOLL_LHAND), 1, false);
		if(arrows == null || arrows.getCount() == 0)
		{
			getInventory().unEquipItemInSlot(Inventory.PAPERDOLL_LHAND);
			_arrowItem = null;
		}
	}

	/**
	 * Equip arrows needed in left hand and send a Server->Client packet ItemList to the L2Player then return True.<BR>
	 * <BR>
	 */
	protected boolean checkAndEquipArrows()
	{
		// Check if nothing is equipped in left hand
		if(getInventory().getPaperdollItem(Inventory.PAPERDOLL_LHAND) == null)
		{
			final L2ItemInstance activeWeapon = getActiveWeaponInstance();
			if(activeWeapon != null)
				if(activeWeapon.getItemType() == WeaponType.BOW)
					_arrowItem = getInventory().findArrowForBow(activeWeapon.getItem());
				else if(activeWeapon.getItemType() == WeaponType.CROSSBOW)
					getInventory().findArrowForCrossbow(activeWeapon.getItem());

			// Equip arrows needed in left hand
			if(_arrowItem != null)
				getInventory().setPaperdollItem(Inventory.PAPERDOLL_LHAND, _arrowItem);
		}
		else
			// Get the L2ItemInstance of arrows equipped in left hand
			_arrowItem = getInventory().getPaperdollItem(Inventory.PAPERDOLL_LHAND);

		return _arrowItem != null;
	}

	public void setUptime(final long time)
	{
		_uptime = time;
	}

	public long getUptime()
	{
		return System.currentTimeMillis() - _uptime;
	}

	/**
	 * @return True if the L2Player has a Party in progress.<BR>
	 */
	public boolean isInParty()
	{
		return _party != null;
	}

	/**
	 * @return True if the player is party leader.<BR>
	 */
	public boolean isPartyLeader()
	{
		if(_party != null)
			return _party.getPartyLeaderOID() == getObjectId();
		return false;
	}

	/**
	 * Set the _party object of the L2Player (without joining it).<BR>
	 */
	public void setParty(final L2Party party)
	{
		_party = party;
	}

	/**
	 * Set the _party object of the L2Player AND join it.<BR>
	 * <BR>
	 */
	public void joinParty(final L2Party party)
	{
		if(party != null)
		{
			_party = party;
			party.addPartyMember(this);
		}
	}

	/**
	 * Manage the Leave Party task of the L2Player.<BR>
	 * <BR>
	 */
	public void leaveParty()
	{
		if(isInParty())
		{
			_party.oustPartyMember(this);
			_party = null;
		}
	}

	/**
	 * @return the _party object of the L2Player.<BR>
	 * <BR>
	 */
	public L2Party getParty()
	{
		return _party;
	}

	/**
	 * @return True if the L2Player is a GM.<BR>
	 * <BR>
	 */
	public boolean isGM()
	{
		return _playerAccess.IsGM;
	}

	/**
	 * @return True if the L2Player can see invisible player.<BR>
	 * <BR>
	 */
	public boolean isCanSeeInvisible()
	{
		return _playerAccess.CanSeeInvisible;
	}

	/**
	 * Нигде не используется, но может пригодиться для БД
	 */
	public void setAccessLevel(final int level)
	{
		_accessLevel = level;
	}

	/**
	 * Нигде не используется, но может пригодиться для БД
	 */
	@Override
	public int getAccessLevel()
	{
		return _accessLevel;
	}

	public void setPlayerAccess(final PlayerAccess pa)
	{
		if(pa != null)
			_playerAccess = pa;
		else
			_playerAccess = new PlayerAccess();

		setAccessLevel(_playerAccess.IsGM || _playerAccess.Admin ? 100 : 0);
	}

	public PlayerAccess getPlayerAccess()
	{
		return _playerAccess;
	}

	public void setAccountAccesslevel(final int level, final String comments, final int banTime)
	{
		LSConnection.getInstance().sendPacket(new ChangeAccessLevel(getAccountName(), level, comments, banTime));
	}

	@Override
	public double getLevelMod()
	{
		return (89. + getLevel()) / 100.0;
	}

	/**
	 * Send a Server->Client StatusUpdate packet with Karma to the L2Player and all L2Player to inform (broadcast).<BR>
	 * <BR>
	 */
	public void updateKarma(final boolean flagChanged)
	{
		sendStatusUpdate(true, StatusUpdate.KARMA);
		if(flagChanged)
			broadcastRelationChanged();
	}

	/**
	 * Set the online Flag to True or False and update the characters table of the database with online status and lastAccess (called when login and logout).<BR>
	 * <BR>
	 */
	public void setOnlineStatus(final boolean isOnline)
	{
		if(_isOnline != isOnline)
			_isOnline = isOnline;

		// Update the characters table of the database with online status and lastAccess (called when login and logout)
		updateOnlineStatus();
	}
	/**
	 * Update the characters table of the database with online status and lastAccess of this L2Player (called when login and logout).<BR>
	 * <BR>
	 */
	public void storeHWID(final String HWID)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE characters SET LastHWID=? WHERE obj_id=?");
			statement.setString(1, HWID);
			statement.setInt(2, getObjectId());
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.warning("could not store characters HWID: " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
	public void updateOnlineStatus()
	{
		if(isInOfflineMode())
			return;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE characters SET online=?, lastAccess=? WHERE obj_id=?");
			statement.setInt(1, isOnline() ? 1 : 0);
			statement.setLong(2, System.currentTimeMillis() / 1000);
			statement.setInt(3, getObjectId());
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.warning("could not set char online status:" + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * Decrease Karma of the L2Player and Send it StatusUpdate packet with Karma and PvP Flag (broadcast).<BR>
	 * <BR>
	 */
	public void increaseKarma(final long add_karma)
	{
		final boolean flagChanged = _karma == 0;
		long new_karma = _karma + add_karma;

		if(new_karma > Integer.MAX_VALUE)
			new_karma = Integer.MAX_VALUE;

		if(_karma == 0 && new_karma > 0)
		{
			_karma = (int) new_karma;
			for(final L2Character cha : L2World.getAroundCharacters(this))
				if(cha.isGuard() && cha.getAI().getIntention() == CtrlIntention.AI_INTENTION_IDLE)
					cha.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE, null, null);
		}
		else
			_karma = (int) new_karma;

		// Send a Server->Client StatusUpdate packet with Karma to the L2Player and all L2Player to inform (broadcast)
		updateKarma(flagChanged);
	}

	/**
	 * Decrease Karma of the L2Player and Send it StatusUpdate packet with Karma and PvP Flag (broadcast).<BR>
	 * <BR>
	 * 
	 * @param i
	 *            : The loss Karma value
	 */
	public void decreaseKarma(final int i)
	{
		final boolean flagChanged = _karma > 0;
		_karma -= i;
		if(_karma <= 0)
		{
			_karma = 0;
			// Send a Server->Client StatusUpdate packet with Karma and PvP Flag to the L2Player and all L2Player to inform (broadcast)
			updateKarma(flagChanged);
		}
		else
			updateKarma(false);

	}

	/**
	 * Create a new player in the characters table of the database.<BR>
	 * <BR>
	 */
	private boolean createDb()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(NEW_CHAR);
			statement.setString(1, _accountName);
			statement.setInt(2, getObjectId());
			statement.setString(3, getName());
			statement.setInt(4, getFace());
			statement.setInt(5, getHairStyle());
			statement.setInt(6, getHairColor());
			statement.setInt(7, getSex());
			statement.setInt(8, getKarma());
			statement.setInt(9, getPvpKills());
			statement.setInt(10, getPkKills());
			statement.setInt(11, getClanId());
			statement.setLong(12, getCreateTime() / 1000);
			statement.setInt(13, getDeleteTimer());
			statement.setString(14, getTitle());
			statement.setInt(15, _accessLevel);
			statement.setInt(16, isOnline() ? 1 : 0);
			statement.setLong(17, getLeaveClanTime() / 1000);
			statement.setLong(18, getDeleteClanTime() / 1000);
			statement.setLong(19, _NoChannel > 0 ? _NoChannel / 1000 : _NoChannel);
			// statement.setInt(20, Config.NEW_CHARACTER_NOBLESSE ? 1 : 0); //FIXME статус Дворянина при создании персонажа
			statement.setInt(20, getPledgeType());
			statement.setInt(21, getPowerGrade());
			statement.setInt(22, getLvlJoinedAcademy());
			statement.setInt(23, getApprentice());
			statement.executeUpdate();
			DbUtils.close(statement);

			statement = con.prepareStatement(NEW_SUB);
			statement.setInt(1, getObjectId());
			statement.setInt(2, getTemplate().classId.getId());
			statement.setLong(3, Experience.LEVEL[Config.STARTING_LEVEL]); // exp
			statement.setInt(4, Config.STARTING_SP); // Стартовое значение SP при создании персонажа
			statement.setDouble(5, getTemplate().baseHpMax + getTemplate().lvlHpAdd + getTemplate().lvlHpMod);
			statement.setDouble(6, getTemplate().baseMpMax + getTemplate().lvlMpAdd + getTemplate().lvlMpMod);
			statement.setDouble(7, getTemplate().baseCpMax + getTemplate().lvlCpAdd + getTemplate().lvlCpMod);
			statement.setDouble(8, getTemplate().baseHpMax + getTemplate().lvlHpAdd + getTemplate().lvlHpMod);
			statement.setDouble(9, getTemplate().baseMpMax + getTemplate().lvlMpAdd + getTemplate().lvlMpMod);
			statement.setDouble(10, getTemplate().baseCpMax + getTemplate().lvlCpAdd + getTemplate().lvlCpMod);
			statement.setInt(11, Config.STARTING_LEVEL); // Стартовый уровень персонажа
			statement.setInt(12, 1);
			statement.setInt(13, 1);
			statement.setInt(14, 0);
			statement.setInt(15, 1);
			statement.executeUpdate();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "could not insert char data:", e);
			return false;
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
		return true;
	}

	/**
	 * Retrieve a L2Player from the characters table of the database and add it in _allObjects of the L2World.<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Retrieve the L2Player from the characters table of the database</li> <li>Add the L2Player object in _allObjects</li> <li>Set the x,y,z position of the L2Player and make it invisible</li> <li>Update the overloaded status of the L2Player</li>
	 * <BR>
	 * <BR>
	 * 
	 * @param objectId
	 *            Identifier of the object to initialized
	 * @return The L2Player loaded from the database
	 */
	public static L2Player restore(final int objectId)
	{
		L2Player player = null;
		ThreadConnection con = null;
		FiltredStatement statement = null;
		FiltredStatement statement2 = null;
		ResultSet pl_rset = null;
		ResultSet ps_rset = null;
		try
		{
			// Retrieve the L2Player from the characters table of the database
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.createStatement();
			statement2 = con.createStatement();
			pl_rset = statement.executeQuery("SELECT * FROM `characters` WHERE `obj_Id`='" + objectId + "' LIMIT 1");
			ps_rset = statement2.executeQuery("SELECT `class_id` FROM `character_subclasses` WHERE `char_obj_id`='" + objectId + "' AND `isBase`='1' LIMIT 1");

			if(pl_rset.next() && ps_rset.next())
			{
				final short classId = ps_rset.getShort("class_id");
				final boolean female = pl_rset.getInt("sex") == 1;
				final L2PlayerTemplate template = CharTemplateTable.getInstance().getTemplate(classId, female);

				player = new L2Player(objectId, template);

				// получаем все переменные
				player.loadVariables();
				// применяем временные переменные
				player.applyTimeVariables(true);

				player.setBaseClass(classId);
				player._accountName = pl_rset.getString("account_name");
				player.setName(pl_rset.getString("char_name"));

				player.setFace(pl_rset.getByte("face"));
				player.setHairStyle(pl_rset.getByte("hairStyle"));
				player.setHairColor(pl_rset.getByte("hairColor"));
				player.setHeading(pl_rset.getInt("heading"));

				player.setKarma(pl_rset.getInt("karma"));
				player.setPvpKills(pl_rset.getInt("pvpkills"));
				player.setPkKills(pl_rset.getInt("pkkills"));

				player.setLeaveClanTime(pl_rset.getLong("leaveclan") * 1000);
				if(player.getLeaveClanTime() > 0 && player.canJoinClan())
					player.setLeaveClanTime(0);

				player.setDeleteClanTime(pl_rset.getLong("deleteclan") * 1000);
				if(player.getDeleteClanTime() > 0 && player.canCreateClan())
					player.setDeleteClanTime(0);

				player.setNoChannel(pl_rset.getLong("nochannel") * 1000);
				if(player.getNoChannel() > 0 && player.getNoChannelRemained() < 0)
					player.updateNoChannel(0);

				player.setOnlineTime(pl_rset.getLong("onlinetime") * 1000);

				final int clanId = pl_rset.getInt("clanid");
				if(clanId > 0 && ClanTable.getInstance().ifExist(clanId))
				{
					player.setClan(ClanTable.getInstance().getClan(clanId));
					player.setPledgeType(pl_rset.getInt("pledge_type"));
					player.setPowerGrade(pl_rset.getInt("pledge_rank"));
					player.setLvlJoinedAcademy(pl_rset.getInt("lvl_joined_academy"));
					player.setApprentice(pl_rset.getInt("apprentice"));
				}

				player.setCreateTime(pl_rset.getLong("createtime") * 1000);
				player.setDeleteTimer(pl_rset.getInt("deletetime"));

				player.setTitle(pl_rset.getString("title"));
				// устанавливаем цвет титула
				if(player.getVar("titleColor") != null)
					player.setTitleColor(Integer.decode("0x" + player.getVar("titleColor")));

				// устанавливаем цвет ника
				if(player.getVar("namecolor") == null)
					if(player.getPlayerAccess().IsGM)
						player.setNameColor(Config.GM_NAME_COLOUR);
					else if(player.getClan() != null && player.getClan().getLeaderId() == player.getObjectId())
						player.setNameColor(Config.CLANLEADER_NAME_COLOUR);
					else
						player.setNameColor(Config.NORMAL_NAME_COLOUR);
				else
					player.setNameColor(Integer.decode("0x" + player.getVar("namecolor")));

				if(Config.COMMAND_ALLOW_AUTOLOOT_COMMAND)
					player.autoLoot = player.getVarB("@autoloot", Config.AUTO_LOOT);

				final String recomList = player.getVar("recomChars");
				if(recomList != null && !recomList.isEmpty())
					for(final String recom : recomList.split(","))
						if(!recom.isEmpty())
							player._recomChars.add(Integer.decode("0x" + recom));

				player.setFistsWeaponItem(player.findFistsWeaponItem(classId));
				player.setUptime(System.currentTimeMillis());
				player.setLastAccess(pl_rset.getLong("lastAccess"));

				player.setRecomHave(pl_rset.getInt("rec_have"));
				player.setRecomLeft(pl_rset.getInt("rec_left"));

				player.setKeyBindings(pl_rset.getBytes("key_bindings"));
				player.setPcBangPoints(pl_rset.getInt("pcBangPoints"));

				player.setFame(pl_rset.getInt("fame"));
				player.setBookMarkSlot(pl_rset.getInt("bookmarkslot"));

				player.restoreRecipeBook();

				if(Config.ENABLE_OLYMPIAD)
				{
					if(Hero.getInstance().isHero(player.getObjectId()))
						player.setHero(true, 0);
					if(Olympiad.isNoble(player.getObjectId()))
						player.setNoble(true, 0);
				}

				// player.setVarka(pl_rset.getInt("varka"));
				// player.setKetra(pl_rset.getInt("ketra"));

				player.updateKetraVarka();

				player.setRam(pl_rset.getInt("ram"));

				player.updatePledgeClass();

				// для сервиса виверн - возврат денег если сервер упал во время полета
				String wm = player.getVar("wyvern_moneyback");
				if(wm != null && Integer.parseInt(wm) > 0)
					player.addAdena(Integer.parseInt(wm));
				player.unsetVar("wyvern_moneyback");

				long reflection = 0;
				// Set the x,y,z position of the L2Player and make it invisible
				// Игрок в тюрьме ли? А то может быть надо посадить досиживать срок
				if(player.getVar("jailed") != null)
				{
					player.setXYZInvisible(-114648, -249384, -2984);
					final String[] re = player.getVar("jailedFrom").split(";");
					final Location loc = new Location(Integer.parseInt(re[0]), Integer.parseInt(re[1]), Integer.parseInt(re[2]));
					// srok - значение в минутах (см. scripts/commands/admin/AdminBan.java)

					reflection = ReflectionTable.JAIL;
					player._unjailTask = L2GameThreadPools.getInstance().scheduleGeneral(new UnJailTask(player, loc), Integer.parseInt(player.getVar("jailed")) * 60000);
					player._isInJail = true;
					player._jailStartTime = System.currentTimeMillis();

					if(Config.DEBUG)
						_log.fine("L2Player.restore(): jailed player " + player.getName() + " for " + player.getVar("jailed") + " minutes");
				}
				else
				{
					player.setXYZInvisible(pl_rset.getInt("x"), pl_rset.getInt("y"), pl_rset.getInt("z"));
					wm = player.getVar("reflection");
					if(wm != null)
					{
						reflection = Long.parseLong(wm);
						if(reflection > 0 || reflection <= -5 && reflection >= -9)
						{
							final String back = player.getVar("backCoords");
							if(back != null)
							{
								player.setXYZInvisible(new Location(back));
								player.unsetVar("backCoords");
							}
							reflection = 0;
						}
					}
				}

				player.setReflection(reflection);

				player.restoreTradeList();
				if(player.getVar("storemode") != null && player.getVar("offline") != null)
				{
					player.setPrivateStoreType(Short.parseShort(player.getVar("storemode")));
					player.setSitting(true);
				}

				if(TerritorySiege.isInProgress())
					if(player.getClan() != null && player.getClan().getTerritorySiege() > -1)
						player.setTerritorySiege(player.getClan().getTerritorySiege());
					else
						player.setTerritorySiege(TerritorySiege.getTerritoryForPlayer(objectId));

				Quest.playerEnter(player);

				player._hidden = true;
				restoreCharSubClasses(player);
				player._hidden = false;

				player.setVitalityPoints(pl_rset.getFloat("vitPoints"), false);

				// 15 секунд после входа в игру на персонажа не агрятся мобы
				player.setNonAggroTime(System.currentTimeMillis() + 15000);

				// Серивис расширения инвенторя
				try
				{
					final String var = player.getVar("ExpandInventory");
					if(var != null)
						player.setExpandInventory(Integer.parseInt(var));
				}
				catch(final Exception e)
				{
					_log.log(Level.INFO, "", e);
				}

				// Серивис расширения хранилища
				try
				{
					final String var = player.getVar("ExpandWarehouse");
					if(var != null)
						player.setExpandWarehouse(Integer.parseInt(var));
				}
				catch(final Exception e)
				{
					_log.log(Level.INFO, "", e);
				}

				FiltredPreparedStatement stmt = null;
				ResultSet chars = null;
				try
				{
					stmt = con.prepareStatement("SELECT obj_Id, char_name FROM characters WHERE account_name=? AND obj_Id != ?");
					stmt.setString(1, player._accountName);
					stmt.setInt(2, objectId);
					chars = stmt.executeQuery();
					while (chars.next())
					{
						final Integer charId = chars.getInt("obj_Id");
						final String charName = chars.getString("char_name");
						player._chars.put(charId, charName);
					}
				}
				catch(final Exception e)
				{
					_log.log(Level.WARNING, e.getMessage(), e);
				}
				finally
				{
					DbUtils.closeQuietly(stmt, chars);
				}

				if(Config.KILL_COUNTER)
				{
					// Restore kills stat
					FiltredStatement stt = null;
					ResultSet rstkills = null;
					try
					{
						stt = con.createStatement();
						rstkills = stt.executeQuery("SELECT `npc_id`, `count` FROM `killcount` WHERE `char_id`=" + objectId);
						player.statKills = new TIntLongHashMap(128);
						while (rstkills.next())
							player.statKills.put(rstkills.getInt("npc_id"), rstkills.getLong("count"));
					}
					catch(final Exception e)
					{
						_log.log(Level.SEVERE, "", e);
					}
					finally
					{
						DbUtils.closeQuietly(stt, rstkills);
					}
				}

				// Restore craft stat
				if(Config.CRAFT_COUNTER)
				{
					FiltredStatement stcraft = null;
					ResultSet rstcraft = null;
					try
					{
						stcraft = con.createStatement();
						rstcraft = stcraft.executeQuery("SELECT `item_id`, `count` FROM `craftcount` WHERE `char_id`=" + objectId);
						player.statCraft = new TIntLongHashMap(32);
						while (rstcraft.next())
							player.statCraft.put(rstcraft.getInt("item_id"), rstcraft.getLong("count"));
					}
					catch(final Exception e)
					{
						_log.log(Level.SEVERE, "", e);
					}
					finally
					{
						DbUtils.closeQuietly(stcraft, rstcraft);
					}
				}

				if(Config.DROP_COUNTER)
				{
					// Restore drop stat
					FiltredStatement stdrop = null;
					ResultSet rstdrop = null;
					try
					{
						stdrop = con.createStatement();
						rstdrop = stdrop.executeQuery("SELECT `item_id`, `count` FROM `dropcount` WHERE `char_id`=" + objectId);
						player.statDrop = new TIntLongHashMap(128);
						while (rstdrop.next())
							player.statDrop.put(rstdrop.getInt("item_id"), rstdrop.getLong("count"));
					}
					catch(final Exception e)
					{
						_log.log(Level.SEVERE, "", e);
					}
					finally
					{
						DbUtils.closeQuietly(stdrop, rstdrop);
					}
				}

				// При не правильных координатах портует в ближайший город
				if(!L2World.validCoords(player.getX(), player.getY()) || player.getX() == 0 && player.getY() == 0)
					player.setXYZInvisible(MapRegionTable.getTeleToClosestTown(player));

				// Перед началом работы с территориями, выполним их обновление
				player.updateTerritories();

				if(!player.isGM())
				{
					// Если персонаж появиться на стадионе олимпиады его портует в ближайший город
					if(Config.ENABLE_OLYMPIAD && ZoneManager.getInstance().checkIfInZone(ZoneType.OlympiadStadia, player))
					{
						player.sendMessage(new CustomMessage("l2n.game.clientpackets.EnterWorld.TeleportedReasonOlympiad", player));
						player.setXYZInvisible(MapRegionTable.getTeleToClosestTown(player));
					}

					final L2Zone noRestartZone = ZoneManager.getInstance().getZoneByTypeAndObject(ZoneType.no_restart, player);
					if(noRestartZone != null)
					{
						final long allowed_time = noRestartZone.getRestartTime();
						final long last_time = player.getLastAccess();
						final long curr_time = System.currentTimeMillis() / 1000;

						if(curr_time - last_time > allowed_time)
						{
							player.sendMessage(new CustomMessage("l2n.game.clientpackets.EnterWorld.TeleportedReasonNoRestart", player));
							player.setXYZInvisible(MapRegionTable.getTeleToClosestTown(player));
						}
					}

					if(player.isInZone(ZoneType.Siege))
					{
						final Siege siege = SiegeManager.getSiege(player, true);
						if(siege != null && !siege.checkIsDefender(player.getClan(), false))
							if(siege.getHeadquarter(player.getClan()) == null)
								player.setXYZInvisible(MapRegionTable.getTeleToClosestTown(player));
							else
								player.setXYZInvisible(MapRegionTable.getTeleToHeadquarter(player));

						if(TerritorySiege.checkIfInZone(player))
							if(TerritorySiege.getHeadquarter(player.getClan()) == null)
								player.setXYZInvisible(MapRegionTable.getTeleToClosestTown(player));
							else
								player.setXYZInvisible(MapRegionTable.getTeleToHeadquarter(player));
					}

					// Проверка если игрок находится на рифте.
					if(DimensionalRiftManager.getInstance().checkIfInRiftZone(player.getLoc(), false))
						player.setXYZInvisible(DimensionalRiftManager.getInstance().getRoom(0, 0).getTeleportCoords());
				}

				player.getInventory().validateItems();
				player.refreshOverloaded();
				player.refreshExpertisePenalty();
				player.restoreBlockList();

				// Стартуем автосохранение
				player.startAutoSaveTask();
				// Уведомление что долго играем)
				BreakWarnManager.getInstance().addWarnTask(player);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player.restore(int): could not restore char data:", e);
		}
		finally
		{
			DbUtils.closeQuietly(statement2, ps_rset);
			DbUtils.closeQuietly(con, statement, pl_rset);
		}
		return player;
	}

	public boolean _isInJail; // true, если чар в тюрьме
	public long _jailStartTime; // время (currentTimeMillis()) помещения чара в тюрьму

	/** @return true, если игрок на зоне */
	public boolean isInJail()
	{
		return _isInJail;
	}

	public void incrementKillsCounter(final int Id)
	{
		final long tmp = statKills.containsKey(Id) ? statKills.get(Id) + 1 : 1;
		statKills.put(Id, tmp);
		sendMessage(new CustomMessage("l2n.game.model.L2Player.KillsCounter", this).addNumber(tmp));
	}

	public void incrementDropCounter(final int Id, final long qty)
	{
		statDrop.put(Id, statDrop.containsKey(Id) ? statDrop.get(Id) + qty : qty);
	}

	public void incrementCraftCounter(final int Id, final int qty)
	{
		final long tmp = statCraft.containsKey(Id) ? statCraft.get(Id) + qty : qty;
		statCraft.put(Id, tmp);
		sendMessage(new CustomMessage("l2n.game.model.L2Player.CraftCounter", this).addNumber(tmp));
	}

	/**
	 * Update L2Player stats in the characters table of the database.<BR>
	 * <BR>
	 * 
	 * @param fast
	 *            TODO
	 */
	public void store(final boolean fast)
	{
		if(!_storeLock.tryLock())
			return;
		try
		{
			// При логауте автоматом проигрывается дуэль.
			if(getDuel() != null)
				getDuel().onPlayerDefeat(this);

			ThreadConnection con = null;
			FiltredPreparedStatement statement = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				statement = con.prepareStatement(UPDATE_CHARACTER);
				statement.setInt(1, getFace());
				statement.setInt(2, getHairStyle());
				statement.setInt(3, getHairColor());
				statement.setInt(4, getHeading());
				if(_stablePoint == null) // если игрок находится в точке в которой его сохранять не стоит (например на виверне) то сохраняются последние координаты
				{
					statement.setInt(5, getX());
					statement.setInt(6, getY());
					statement.setInt(7, getZ());
				}
				else
				{
					statement.setInt(5, _stablePoint.x);
					statement.setInt(6, _stablePoint.y);
					statement.setInt(7, _stablePoint.z);
				}
				statement.setInt(8, getKarma());
				statement.setInt(9, getPvpKills());
				statement.setInt(10, getPkKills());
				statement.setInt(11, getRecomHave());
				statement.setInt(12, getRecomLeft());
				statement.setInt(13, getClanId());
				statement.setInt(14, getDeleteTimer());
				statement.setString(15, getTitle());
				statement.setInt(16, _accessLevel);
				statement.setInt(17, isOnline() ? 1 : 0);
				statement.setLong(18, getLeaveClanTime() / 1000);
				statement.setLong(19, getDeleteClanTime() / 1000);
				statement.setLong(20, _NoChannel > 0 ? getNoChannelRemained() / 1000 : _NoChannel);
				statement.setLong(21, _onlineBeginTime > 0 ? (_onlineTime + System.currentTimeMillis() - _onlineBeginTime) / 1000 : _onlineTime / 1000);
				statement.setInt(22, getRam());
				statement.setDouble(23, getVitalityPoints());
				statement.setInt(24, getPledgeType());
				statement.setInt(25, getPowerGrade());
				statement.setInt(26, getLvlJoinedAcademy());
				statement.setInt(27, getApprentice());
				statement.setInt(28, getPcBangPoints());
				statement.setString(29, getName());
				statement.setInt(30, Math.min(Config.MAX_PERSONAL_FAME_POINTS, getFame())); // FIXME заипали ошибки тут(
				statement.setInt(31, getBookMarkSlot());
				statement.setBytes(32, getKeyBindings());
				statement.setInt(33, getObjectId());

				statement.executeUpdate();
				Stat.increaseUpdatePlayerBase();

				try
				{
					if(!fast && Config.KILL_COUNTER && statKills != null)
					{
						final FiltredStatement fs = con.createStatement();
						statKills.forEachEntry(new TIntLongProcedure()
						{
							@Override
							public boolean execute(final int npcId, final long count)
							{
								try
								{
									fs.addBatch(StringUtil.concat("REPLACE DELAYED INTO `killcount` SET `npc_id`=", npcId, ", `count`=", count, ", `char_id`=", _objectId, "\""));
								}
								catch(final SQLException e)
								{}
								return true;
							}
						});

						fs.executeBatch();
						DbUtils.close(fs);
					}

					if(!fast && Config.CRAFT_COUNTER && statCraft != null)
					{
						final FiltredStatement fs = con.createStatement();
						statCraft.forEachEntry(new TIntLongProcedure()
						{
							@Override
							public boolean execute(final int item_id, final long count)
							{
								try
								{
									fs.addBatch(StringUtil.concat("REPLACE DELAYED INTO `craftcount` SET `item_id`=", item_id, ", `count`=", count, ", `char_id`=", _objectId, "\""));
								}
								catch(final SQLException e)
								{}
								return true;
							}
						});

						fs.executeBatch();
						DbUtils.close(fs);
					}

					if(!fast && Config.DROP_COUNTER && statDrop != null)
					{
						final FiltredStatement fs = con.createStatement();
						statDrop.forEachEntry(new TIntLongProcedure()
						{
							@Override
							public boolean execute(final int item_id, final long count)
							{
								try
								{
									fs.addBatch(StringUtil.concat("REPLACE DELAYED INTO `dropcount` SET `item_id`=", item_id, ", `count`=", count, ", `char_id`=", _objectId, "\""));
								}
								catch(final SQLException e)
								{}
								return true;
							}
						});
						fs.executeBatch();
						DbUtils.close(fs);
					}
				}
				catch(final Exception e)
				{}

				if(!fast)
				{
					storeEffects();
					storeDisableSkills();
					storeBlockList();
				}

				storeCharSubClasses();
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "L2Player.store(): couldn't store char data: " + toFullString(), e);
			}
			finally
			{
				DbUtils.closeQuietly(con, statement);
			}
		}
		finally
		{
			_storeLock.unlock();
		}
	}

	/**
	 * @return True if the L2Player is on line.<BR>
	 * <BR>
	 */
	public boolean isOnline()
	{
		return _isOnline;
	}

	/**
	 * Add a skill to the L2Player _skills and its Func objects to the calculator set of the L2Player and save update in the character_skills table of the database.<BR>
	 * <BR>
	 * <B><U> Concept</U> :</B><BR>
	 * <BR>
	 * All skills own by a L2Player are identified in <B>_skills</B><BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Replace oldSkill by newSkill or Add the newSkill</li> <li>If an old skill has been replaced, remove all its Func objects of L2Character calculator set</li> <li>Add Func objects of newSkill to the calculator set of the L2Character</li><BR>
	 * <BR>
	 * 
	 * @param newSkill
	 *            The L2Skill to add to the L2Character
	 * @return The L2Skill replaced or null if just added a new L2Skill
	 */
	public L2Skill addSkill(final L2Skill newSkill, final boolean store)
	{
		if(newSkill == null)
			return null;

		// Add a skill to the L2Player _skills and its Func objects to the calculator set of the L2Player
		final L2Skill oldSkill = super.addSkill(newSkill);

		if(newSkill.equals(oldSkill))
			return oldSkill;

		// Add or update a L2Player skill in the character_skills table of the database
		if(store)
			storeSkill(newSkill, oldSkill);

		// sendUserInfo(true);

		return oldSkill;
	}

	@Override
	public L2Skill addSkill(final L2Skill newSkill)
	{
		return addSkill(newSkill, false);
	}

	/**
	 * Remove a skill from the L2Character and its Func objects from calculator set of the L2Character and save update in the character_skills table of the database.<BR>
	 * <BR>
	 * <B><U> Concept</U> :</B><BR>
	 * <BR>
	 * All skills own by a L2Character are identified in <B>_skills</B><BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Remove the skill from the L2Character _skills</li> <li>Remove all its Func objects from the L2Character calculator set</li><BR>
	 * <BR>
	 * <B><U> Overriden in </U> :</B><BR>
	 * <BR>
	 * <li>L2Player : Save update in the character_skills table of the database</li> <BR>
	 * <BR>
	 * 
	 * @param skill
	 *            The L2Skill to remove from the L2Character
	 * @return The L2Skill removed
	 */
	public final L2Skill removeSkill(final L2Skill skill, final boolean fromDB)
	{
		// Remove a skill from the L2Character and its Func objects from calculator set of the L2Character
		L2Skill oldSkill = super.removeSkill(skill);

		// удаляем из списка скил Талисмана
		if(skill != null && skill.isTalismanSkill())
			removeTalismanSkill(skill);

		if(!fromDB)
			return oldSkill;

		if(oldSkill == null)
			oldSkill = skill;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			// Remove or update a L2Player skill from the character_skills table of the database
			con = L2DatabaseFactory.getInstance().getConnection();
			if(oldSkill != null)
			{
				statement = con.prepareStatement(REMOVE_SKILL);
				statement.setInt(1, oldSkill.getId());
				statement.setInt(2, getObjectId());
				statement.setInt(3, getActiveClassId());
				statement.execute();
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player.removeSkill(L2Skill, boolean): error could not delete skill:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
		return oldSkill;
	}

	public final L2Skill removeSkill(final int id, final boolean fromDB)
	{
		final L2Skill oldSkill = super.removeSkillById(id);

		if(!fromDB)
			return oldSkill;

		if(oldSkill != null)
		{
			ThreadConnection con = null;
			FiltredPreparedStatement statement = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				statement = con.prepareStatement(REMOVE_SKILL);
				statement.setInt(1, oldSkill.getId());
				statement.setInt(2, getObjectId());
				statement.setInt(3, getActiveClassId());
				statement.execute();
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "L2Player.removeSkill(int, boolean): error could not delete Skill:", e);
			}
			finally
			{
				DbUtils.closeQuietly(con, statement);
			}
		}
		return oldSkill;
	}

	/**
	 * Add or update a L2Player skill in the character_skills table of the database.<BR>
	 * <BR>
	 */
	private void storeSkill(final L2Skill newSkill, final L2Skill oldSkill)
	{
		if(newSkill == null || newSkill.getId() > 369 && newSkill.getId() < 392)
			return;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			if(oldSkill != null && newSkill != null)
			{
				statement = con.prepareStatement(UPDATE_CHARACTER_SKILL_LEVEL);
				statement.setInt(1, newSkill.getLevel());
				statement.setInt(2, oldSkill.getId());
				statement.setInt(3, getObjectId());
				statement.setInt(4, getActiveClassId());
				statement.execute();
			}
			else if(newSkill != null)
			{
				statement = con.prepareStatement(ADD_SKILL_SAVE);
				statement.setInt(1, getObjectId());
				statement.setInt(2, newSkill.getId());
				statement.setInt(3, newSkill.getLevel());
				statement.setString(4, newSkill.getName());
				statement.setInt(5, getActiveClassId());
				statement.execute();
			}
			else
				_log.warning("could not store new skill. its NULL");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player.storeSkill(L2Skill, L2Skill): error could not store skills:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * Retrieve from the database all skills of this L2Player and add them to * _skills.
	 */
	public void restoreSkills()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			// Retrieve all skills of this L2Player from the database
			// Send the SQL query : SELECT skill_id,skill_level FROM character_skills WHERE char_obj_id=? to the database
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(RESTORE_SKILL);
			statement.setInt(1, getObjectId());
			statement.setInt(2, getActiveClassId());
			rset = statement.executeQuery();

			// Go though the recordset of this SQL query
			while (rset.next())
			{
				final int id = rset.getInt("skill_id");
				final int level = rset.getInt("skill_level");
				boolean allow = false;

				if(id > 9000)
					continue; // fake skills for base stats

				// Create a L2Skill object for each record
				final L2Skill skill = SkillTable.getInstance().getInfo(id, level);
				if(skill == null)
					continue;

				// Remove skill if not possible
				if(!isGM() && !skill.isCommon() && !SkillTreeTable.getInstance().isSkillPossible(this, skill.getId(), skill.getLevel()) && !allow)
				{
					removeSkill(skill, true);
					removeSkillFromShortCut(skill.getId());
					Util.handleIllegalPlayerAction(this, "L2Player[6537]", "has illegal skills: " + skill, IllegalPlayerAction.WARNING);
					continue;
				}

				// Add the L2Skill object to the L2Character _skills and its Func objects to the calculator set of the L2Character
				super.addSkill(skill);
			}

			// Restore noble skills
			if(isNoble())
				for(final L2Skill sk : NobleSkillTable.getNobleSkills())
					super.addSkill(sk);

			// Restore Hero skills at main class only
			if(isHero() && getBaseClassId() == getActiveClassId())
				for(final L2Skill sk : HeroSkillTable.getHeroSkills())
					super.addSkill(sk);

			if(_clan != null)
			{
				// Restore clan leader siege skills
				if(_clan.getLeaderId() == getObjectId() && _clan.getLevel() >= CastleSiegeManager.getSiegeClanMinLevel())
					SiegeManager.addSiegeSkills(this);

				// Restore clan skills
				_clan.addAndShowSkillsToPlayer(this);
			}

			// Give dwarven craft skill
			if(getActiveClassId() >= 53 && getActiveClassId() <= 57 || getActiveClassId() == 117 || getActiveClassId() == 118)
				super.addSkill(SkillTable.getInstance().getInfo(L2Skill.SKILL_DWARVEN_CRAFT, 1));
			super.addSkill(SkillTable.getInstance().getInfo(L2Skill.SKILL_COMMON_CRAFT, 1));

			if(Config.UNSTUCK_SKILL && getSkillLevel(1050) < 0)
				addSkill(SkillTable.getInstance().getInfo(2099, 1), false);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player.restoreSkills(): couldn't restore skills:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void storeDisableSkills()
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			statement.executeUpdate("DELETE FROM character_skills_save WHERE char_obj_id = " + getObjectId() + " AND class_index=" + getActiveClassId());
			DbUtils.close(statement);

			if(_skillReuses == null || _skillReuses.isEmpty())
				return;

			final SqlBatch b = new SqlBatch("REPLACE INTO `character_skills_save` (`char_obj_id`,`skill_id`,`class_index`,`end_time`,`reuse_delay_org`) VALUES");
			synchronized (_skillReuses)
			{
				StringBuilder sb;
				for(final SkillTimeStamp t : _skillReuses.values())
					if(t.hasNotPassed())
					{
						sb = StringUtil.startAppend(200, "(");
						StringUtil.append(sb, getObjectId(), ",", t.getId(), ",", getActiveClassId(), ",", t.getEndTime(), ",", t.getReuseBasic(), ")");
						b.write(sb.toString());
					}
			}

			if(!b.isEmpty())
			{
				statement = con.createStatement();
				statement.executeUpdate(b.close());
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player.storeDisableSkills(): couldn't store disable skills data: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void storeEffects()
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.createStatement();
			statement.executeUpdate("DELETE FROM character_effects_save WHERE char_obj_id = " + getObjectId() + " AND class_index=" + getActiveClassId());
			DbUtils.close(statement);

			if(_effectList == null || _effectList.isEmpty())
				return;

			int order = 0;
			final SqlBatch b = new SqlBatch("INSERT IGNORE INTO `character_effects_save` (`char_obj_id`,`skill_id`,`skill_level`,`effect_count`,`effect_cur_time`,`duration`,`order`,`class_index`) VALUES");

			synchronized (getEffectList())
			{
				StringBuilder sb;
				for(L2Effect effect : getEffectList().getAllEffects())
					if(effect != null && effect.isInUse() && !effect.getSkill().isToggle())
					{
						switch (effect.getEffectType())
						{
							case HealOverTime:
							case CombatPointHealOverTime:
							case Invisible:
								continue;
						}

						if(effect.isSaveable())
						{
							sb = StringUtil.startAppend(200, "(");
							StringUtil.append(sb, getObjectId(), ",", effect.getSkill().getId(), ",", effect.getSkill().getLevel(), ",", effect.getCount(), ",", effect.getTime(), ",", effect.getPeriod(), ",", order, ",", getActiveClassId(), ")");
							b.write(sb.toString());
						}
						while ((effect = effect.getNext()) != null && effect.isSaveable())
						{
							sb = StringUtil.startAppend(200, "(");
							StringUtil.append(sb, getObjectId(), ",", effect.getSkill().getId(), ",", effect.getSkill().getLevel(), ",", effect.getCount(), ",", effect.getTime(), ",", effect.getPeriod(), ",", order, ",", getActiveClassId(), ")");
							b.write(sb.toString());
						}
						order++;
					}
				if(Config.ALT_SAVE_UNSAVEABLE)
					for(final L2CubicInstance cubic : getCubics())
					{
						sb = StringUtil.startAppend(200, "(");
						StringUtil.append(sb, getObjectId(), ",", cubic.getId() + L2CubicInstance.CUBIC_STORE_OFFSET, ",", cubic.getLevel(), ",1,", cubic.lifeLeft(), ",1,", order++, ",", getActiveClassId(), ")");
						b.write(sb.toString());
					}
			}

			if(!b.isEmpty())
			{
				statement = con.createStatement();
				statement.executeUpdate(b.close());
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player.storeEffects(): couldn't store active effects data: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void restoreEffects()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement(RESTORE_EFFECTS);
			statement.setInt(1, getObjectId());
			statement.setInt(2, getActiveClassId());

			rset = statement.executeQuery();
			while (rset.next())
			{
				int skillId = rset.getInt("skill_id");
				final int skillLvl = rset.getInt("skill_level");
				final int effectCount = rset.getInt("effect_count");
				final long effectCurTime = rset.getLong("effect_cur_time");
				final long duration = rset.getLong("duration");

				if(skillId >= L2CubicInstance.CUBIC_STORE_OFFSET) // cubic
				{
					skillId -= L2CubicInstance.CUBIC_STORE_OFFSET;
					addCubic(skillId, skillLvl, (int) effectCurTime, 0, true);
					continue;
				}

				final L2Skill skill = SkillTable.getInstance().getInfo(skillId, skillLvl);
				if(skill == null)
				{
					_log.warning("Can't restore Effect\tskill: " + skillId + ":" + skillLvl + " " + toFullString());
					Thread.dumpStack();
					continue;
				}

				if(skill.getEffectTemplates() == null)
				{
					_log.warning("Can't restore Effect, EffectTemplates is NULL\tskill: " + skillId + ":" + skillLvl + " " + toFullString());
					Thread.dumpStack();
					continue;
				}

				for(final EffectTemplate et : skill.getEffectTemplates())
				{
					if(et == null || et._effectType == EffectType.Symbol)
						continue;

					final Env env = new Env(this, this, skill);
					final L2Effect effect = et.getEffect(env);
					if(effect == null || effect.isOneTime())
						continue;

					effect.setCount(effectCount);
					effect.setPeriod(effectCount == 1 ? duration - effectCurTime : duration);

					// Добавляем все эффекты игроку
					getEffectList().addEffect(effect);

				}
			}

			DbUtils.closeQuietly(statement, rset);

			statement = con.prepareStatement("DELETE FROM `character_effects_save` WHERE `char_obj_id` = ? AND `class_index` = ?");
			statement.setInt(1, getObjectId());
			statement.setInt(2, getActiveClassId());
			statement.executeUpdate();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player.restoreEffects(): couldn't restore active effects data: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void restoreDisableSkills()
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.createStatement();
			rset = statement.executeQuery("SELECT skill_id,end_time,reuse_delay_org FROM character_skills_save WHERE char_obj_id = " + getObjectId() + " AND class_index = " + getActiveClassId());
			L2Skill skill;
			int skillId, skillLevel;
			long endTime, rDelayOrg, curTime;
			while (rset.next())
			{
				skillId = rset.getInt("skill_id");
				skillLevel = Math.max(getSkillLevel(skillId), 1);
				endTime = rset.getLong("end_time");
				rDelayOrg = rset.getLong("reuse_delay_org");
				curTime = System.currentTimeMillis();

				skill = SkillTable.getInstance().getInfo(skillId, skillLevel);
				if(skill != null && endTime - curTime > 500)
				{
					disableSkill(skill, new SkillTimeStamp(skill, endTime, rDelayOrg));
					disableItem(skill, rDelayOrg, endTime - curTime);
				}
			}
			DbUtils.closeQuietly(statement, rset);

			statement = con.createStatement();
			statement.executeUpdate("DELETE FROM character_skills_save WHERE char_obj_id = " + getObjectId() + " AND class_index=" + getActiveClassId() + " AND `end_time` < " + System.currentTimeMillis());
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player.restoreDisableSkills(): couldn't restore active skills data for " + getObjectId() + "/" + getActiveClassId(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		updateEffectIcons();
	}

	@Override
	public void disableItem(final L2Skill handler, final long timeTotal, final long timeLeft)
	{
		if(handler.isHandler() && timeLeft > 1000)
			if(handler.getReuseGroupId() > 0)
			{
				final GArray<Integer> disabled = new GArray<Integer>();
				for(final Integer skill_id : handler.getReuseGroup())
					// TODO: хранить отключенные группы отдельно от скиллов во избежание коллизий и для отсылки при изменении ярлыков
					for(final L2Skill sk : SkillTable.getInstance().getAllLevels(skill_id))
						if(sk != null && sk.getItemConsumeIDs().length > 0 && !disabled.contains(sk.getItemConsumeId()))
						{
							sendPacket(new ExUseSharedGroupItem(sk.getItemConsumeId(), sk.getLevel(), (int) timeLeft, (int) timeTotal));
							disabled.add(sk.getItemConsumeId());

							if(!isSkillDisabled(sk))
								disableSkill(sk, timeLeft);
						}
			}
			else if(handler.getItemConsumeIDs().length > 0)
				sendPacket(new ExUseSharedGroupItem(handler.getItemConsumeId(), handler.getLevel(), (int) timeTotal, (int) timeLeft));
	}

	/**
	 * Retrieve from the database all Henna of this L2Player, add them to _henna and calculate stats of the L2Player.<BR>
	 * <BR>
	 */
	private void restoreHenna()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT slot, symbol_id FROM character_hennas WHERE char_obj_id = ? AND class_index = ?");
			statement.setInt(1, getObjectId());
			statement.setInt(2, getActiveClassId());
			rset = statement.executeQuery();

			for(byte i = 0; i < 3; i++)
				_henna[i] = null;

			L2HennaInstance sym;
			L2Henna tpl;
			while (rset.next())
			{
				final int slot = rset.getInt("slot");
				if(slot < 1 || slot > 3)
					continue;

				final int symbol_id = rset.getInt("symbol_id");
				if(symbol_id != 0)
				{
					tpl = HennaTable.getInstance().getTemplate(symbol_id);
					if(tpl != null)
					{
						sym = new L2HennaInstance(tpl);
						_henna[slot - 1] = sym;
					}
				}
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player.restoreHenna(): couldn't restore henna: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		// Calculate Henna modifiers of this L2Player
		recalcHennaStats();

	}

	/**
	 * @return the number of Henna empty slot of the L2Player.<BR>
	 * <BR>
	 */
	public int getHennaEmptySlots()
	{
		int totalSlots = 1 + getClassId().level();
		for(byte i = 0; i < 3; i++)
			if(_henna[i] != null)
				totalSlots--;

		if(totalSlots <= 0)
			return 0;

		return totalSlots;
	}

	/**
	 * Remove a Henna of the L2Player, save update in the character_hennas table of the database and send Server->Client HennaInfo/UserInfo packet to this L2Player.<BR>
	 * <BR>
	 */
	public boolean removeHenna(int slot)
	{
		if(slot < 1 || slot > 3)
			return false;

		slot--;

		if(_henna[slot] == null)
			return false;

		final L2HennaInstance henna = _henna[slot];
		final short dyeID = henna.getItemIdDye();

		// Added by Tempy - 10 Aug 05
		// Gives amount equal to half of the dyes needed for the henna back.
		final L2ItemInstance hennaDyes = ItemTable.getInstance().createItem(dyeID, getObjectId(), 0, "L2Player.removeHenna");
		hennaDyes.setCount(henna.getAmountDyeRequire() / 2);

		_henna[slot] = null;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM character_hennas where char_obj_id=? and slot=? and class_index=?");
			statement.setInt(1, getObjectId());
			statement.setInt(2, slot + 1);
			statement.setInt(3, getActiveClassId());
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player.removeHenna(int): couldn't remove char henna: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}

		// Calculate Henna modifiers of this L2Player
		recalcHennaStats();

		// Send Server->Client HennaInfo packet to this L2Player
		sendPacket(new HennaInfo(this));

		// Send Server->Client UserInfo packet to this L2Player
		sendUserInfo(false);

		// Add the recovered dyes to the player's inventory and notify them.
		getInventory().addItem(hennaDyes);
		sendPacket(SystemMessage.obtainItems(henna.getItemIdDye(), henna.getAmountDyeRequire() / 2, 0));

		return true;
	}

	/**
	 * Add a Henna to the L2Player, save update in the character_hennas table of the database and send Server->Client HennaInfo/UserInfo packet to this L2Player.<BR>
	 * <BR>
	 * 
	 * @param henna
	 *            L2HennaInstance для добавления
	 */
	public boolean addHenna(final L2HennaInstance henna)
	{
		if(getHennaEmptySlots() == 0)
		{
			sendPacket(Msg.NO_SLOT_EXISTS_TO_DRAW_THE_SYMBOL);
			return false;
		}

		// int slot = 0;
		for(byte i = 0; i < 3; i++)
			if(_henna[i] == null)
			{
				_henna[i] = henna;

				// Calculate Henna modifiers of this L2Player
				recalcHennaStats();

				ThreadConnection con = null;
				FiltredPreparedStatement statement = null;
				try
				{
					con = L2DatabaseFactory.getInstance().getConnection();
					statement = con.prepareStatement("INSERT INTO `character_hennas` (char_obj_id, symbol_id, slot, class_index) VALUES (?,?,?,?)");
					statement.setInt(1, getObjectId());
					statement.setInt(2, henna.getSymbolId());
					statement.setInt(3, i + 1);
					statement.setInt(4, getActiveClassId());
					statement.execute();
				}
				catch(final Exception e)
				{
					_log.log(Level.WARNING, "L2Player.addHenna(L2HennaInstance): couldn't save char henna: ", e);
				}
				finally
				{
					DbUtils.closeQuietly(con, statement);
				}

				// Send Server->Client HennaInfo packet to this L2Player
				final HennaInfo hi = new HennaInfo(this);
				sendPacket(hi);

				// Send Server->Client UserInfo packet to this L2Player
				sendUserInfo(true);

				return true;
			}

		return false;
	}

	/**
	 * Calculate Henna modifiers of this L2Player.<BR>
	 * <BR>
	 */
	private void recalcHennaStats()
	{
		_hennaINT = 0;
		_hennaSTR = 0;
		_hennaCON = 0;
		_hennaMEN = 0;
		_hennaWIT = 0;
		_hennaDEX = 0;

		for(byte i = 0; i < 3; i++)
		{
			if(_henna[i] == null)
				continue;
			_hennaINT += _henna[i].getStatINT();
			_hennaSTR += _henna[i].getStatSTR();
			_hennaMEN += _henna[i].getStatMEM();
			_hennaCON += _henna[i].getStatCON();
			_hennaWIT += _henna[i].getStatWIT();
			_hennaDEX += _henna[i].getStatDEX();
		}

		if(_hennaINT > 5)
			_hennaINT = 5;
		if(_hennaSTR > 5)
			_hennaSTR = 5;
		if(_hennaMEN > 5)
			_hennaMEN = 5;
		if(_hennaCON > 5)
			_hennaCON = 5;
		if(_hennaWIT > 5)
			_hennaWIT = 5;
		if(_hennaDEX > 5)
			_hennaDEX = 5;
	}

	/**
	 * @param slot
	 *            id слота у перса
	 * @return the Henna of this L2Player corresponding to the selected slot.<BR>
	 * <BR>
	 */
	public L2HennaInstance getHenna(final int slot)
	{
		if(slot < 1 || slot > 3)
			return null;
		return _henna[slot - 1];
	}

	/**
	 * @return the INT Henna modifier of this L2Player.<BR>
	 * <BR>
	 */
	public int getHennaStatINT()
	{
		return _hennaINT;
	}

	/**
	 * @return the STR Henna modifier of this L2Player.<BR>
	 * <BR>
	 */
	public int getHennaStatSTR()
	{
		return _hennaSTR;
	}

	/**
	 * @return the CON Henna modifier of this L2Player.<BR>
	 * <BR>
	 */
	public int getHennaStatCON()
	{
		return _hennaCON;
	}

	/**
	 * @return the MEN Henna modifier of this L2Player.<BR>
	 * <BR>
	 */
	public int getHennaStatMEN()
	{
		return _hennaMEN;
	}

	/**
	 * @return the WIT Henna modifier of this L2Player.<BR>
	 * <BR>
	 */
	public int getHennaStatWIT()
	{
		return _hennaWIT;
	}

	/**
	 * @return the DEX Henna modifier of this L2Player.<BR>
	 * <BR>
	 */
	public int getHennaStatDEX()
	{
		return _hennaDEX;
	}

	/**
	 * Reduce Item quantity of the L2Player Inventory.<BR>
	 * <BR>
	 */
	@Override
	public boolean consumeItem(final int itemConsumeId, final int itemCount)
	{
		final L2ItemInstance item = getInventory().findItemByItemId(itemConsumeId);
		if(item == null || item.getCount() < itemCount)
			return false;
		return getInventory().destroyItem(item, itemCount, false) != null;
	}

	/**
	 * @return True if the L2Player is a Mage.<BR>
	 * <BR>
	 */
	@Override
	public boolean isMageClass()
	{
		return _template.baseMAtk > 3;
	}

	public boolean isMounted()
	{
		return _mountNpcId > 0;
	}

	/**
	 * Проверяет, можно ли приземлиться в этой зоне.
	 * 
	 * @return можно ли приземлится
	 */
	public boolean checkLandingState()
	{
		if(isInZone(no_landing))
			return false;
		final Siege siege = SiegeManager.getSiege(this, false);
		if(siege != null)
		{
			final Residence unit = siege.getSiegeUnit();
			if(unit != null && _clan != null && isClanLeader() && (_clan.getHasCastle() == unit.getId() || _clan.getHasFortress() == unit.getId()))
				return true;
			return false;
		}
		return true;
	}

	public void setMount(final int npcId, final int obj_id)
	{
		if(isCursedWeaponEquipped() || isCombatFlagEquipped() || isTerritoryFlagEquipped())
		{
			sendActionFailed();
			return;
		}

		switch (npcId)
		{
			case 0: // Dismount

				if(isFlying() && Config.GEODATA_ENABLED)
				{
					final int dist = Math.abs(GeoEngine.getHeight(getLoc()) - getLoc().getZ());
					// если слишком высоко, слезать с виверны не даём
					if(dist > 600)
					{
						sendPacket(Msg.YOU_ARE_NOT_ALLOWED_TO_DISMOUNT_AT_THIS_LOCATION);
						if(isGM())
							sendMessage("GeoEngine: geo_z=" + GeoEngine.getHeight(getLoc()) + ", loc_z=" + getZ() + ", height=" + dist);
						sendActionFailed();
						return;
					}
				}

				setFlying(false);
				setRiding(false);
				if(getTransformationId() > 0)
					stopTransformation();
				removeSkillById(L2Skill.SKILL_STRIDER_ASSAULT);
				removeSkillById(L2Skill.SKILL_WYVERN_BREATH);
				getEffectList().stopEffect(L2Skill.SKILL_HINDER_STRIDER);
				break;
			case PetDataTable.STRIDER_WIND_ID:
			case PetDataTable.STRIDER_STAR_ID:
			case PetDataTable.STRIDER_TWILIGHT_ID:
			case PetDataTable.RED_STRIDER_WIND_ID:
			case PetDataTable.RED_STRIDER_STAR_ID:
			case PetDataTable.RED_STRIDER_TWILIGHT_ID:
			case PetDataTable.GUARDIANS_STRIDER_ID:
				setRiding(true);
				if(isNoble())
					addSkill(SkillTable.getInstance().getInfo(L2Skill.SKILL_STRIDER_ASSAULT, 1), false);
				break;
			case PetDataTable.WYVERN_ID:
				if(isInZone(no_landing))
				{
					sendPacket(Msg.YOU_CANNOT_MOUNT_BECAUSE_YOU_DO_NOT_MEET_THE_REQUIREMENTS);
					return;
				}
				setFlying(true);
				setLoc(getLoc().changeZ(32));
				addSkill(SkillTable.getInstance().getInfo(L2Skill.SKILL_WYVERN_BREATH, 1), false);
				break;
			case PetDataTable.WGREAT_WOLF_ID:
			case PetDataTable.FENRIR_WOLF_ID:
			case PetDataTable.WFENRIR_WOLF_ID:
			case PetDataTable.LIGHT_PURPLE_MANED_HORSE_ID:
			case PetDataTable.TAWNY_MANED_LION_ID:
			case PetDataTable.STEAM_BEATLE_ID:
				setRiding(true);
				break;
			case PetDataTable.AURA_BIRD_FALCON_ID:
				if(isInZone(no_landing))
				{
					sendPacket(Msg.YOU_CANNOT_MOUNT_BECAUSE_YOU_DO_NOT_MEET_THE_REQUIREMENTS);
					return;
				}
				setLoc(getLoc().changeZ(32));
				setFlying(true);
				setTransformationId(8);
				break;
			case PetDataTable.AURA_BIRD_OWL_ID:
				if(isInZone(no_landing))
				{
					sendPacket(Msg.YOU_CANNOT_MOUNT_BECAUSE_YOU_DO_NOT_MEET_THE_REQUIREMENTS);
					return;
				}
				setLoc(getLoc().changeZ(32));
				setFlying(true);
				setTransformationId(9);
				break;
		}

		if(npcId > 0 && !isCursedWeaponEquipped())
			unEquipWeapon();

		_mountNpcId = npcId;
		_mountObjId = obj_id;

		broadcastUserInfo(true); // нужно послать пакет перед Ride для корректного снятия оружия с заточкой
		broadcastPacket(new Ride(this));
		broadcastUserInfo(true); // нужно послать пакет после Ride для корректного отображения скорости

		sendPacket(new SkillList(this));
	}

	public void unEquipWeapon()
	{
		L2ItemInstance wpn = getInventory().getPaperdollItem(Inventory.PAPERDOLL_LHAND);
		if(wpn != null)
			sendDisarmMessage(wpn);
		getInventory().unEquipItemInSlot(Inventory.PAPERDOLL_LHAND);

		wpn = getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);
		if(wpn != null)
			sendDisarmMessage(wpn);
		getInventory().unEquipItemInSlot(Inventory.PAPERDOLL_RHAND);

		refreshExpertisePenalty();
		abortAttack();
		abortCast();
	}

	@Override
	public int getSpeed(int baseSpeed)
	{
		if(isFlying())
			return Config.FLYING_SPEED;
		if(isRiding())
			baseSpeed = Config.RIDING_SPEED;

		final int val = super.getSpeed(baseSpeed);
		// Apply max run speed cap.
		if(Config.LIM_RUNSPEED > 0 && val > Config.LIM_RUNSPEED)
			return Config.LIM_RUNSPEED;

		return val;
	}

	public void sendDisarmMessage(final L2ItemInstance wpn)
	{
		if(wpn.getEnchantLevel() > 0)
		{
			final SystemMessage sm = new SystemMessage(SystemMessage.EQUIPMENT_OF__S1_S2_HAS_BEEN_REMOVED);
			sm.addNumber(wpn.getEnchantLevel());
			sm.addItemName(wpn.getItemId());
			sendPacket(sm);
		}
		else
		{
			final SystemMessage sm = new SystemMessage(SystemMessage.S1__HAS_BEEN_DISARMED);
			sm.addItemName(wpn.getItemId());
			sendPacket(sm);
		}
	}

	private int _mountNpcId;
	private int _mountObjId;

	public void setMountNpcId(final int id)
	{
		_mountNpcId = id;
	}

	public int getMountNpcId()
	{
		return _mountNpcId;
	}

	public void setMountObjId(final int id)
	{
		_mountObjId = id;
	}

	public int getMountObjId()
	{
		return _mountObjId;
	}

	/**
	 * Disable the Inventory and create a new task to enable it after 1.5s.<BR>
	 * <BR>
	 */
	public void tempInventoryDisable()
	{
		_inventoryDisable = true;
		L2GameThreadPools.getInstance().scheduleAi(new InventoryEnable(), 1500, true);
	}

	/**
	 * @return True if the Inventory is disabled.<BR>
	 * <BR>
	 */
	public boolean isInventoryDisabled()
	{
		return _inventoryDisable;
	}

	private class InventoryEnable implements Runnable
	{
		@Override
		public void run()
		{
			_inventoryDisable = false;
		}
	}

	/**
	 * истанавливает тип используемого склада.
	 * 
	 * @param type
	 *            тип склада:<BR>
	 *            <ul>
	 *            <li>WarehouseType.PRIVATE
	 *            <li>WarehouseType.CLAN
	 *            <li>WarehouseType.CASTLE
	 *            <li>WarehouseType.FREIGHT
	 *            </ul>
	 */
	public void setUsingWarehouseType(final WarehouseType type)
	{
		_usingWHType = type;
	}

	/**
	 * Возвращает тип используемого склада.
	 * 
	 * @return null или тип склада:<br>
	 *         <ul>
	 *         <li>WarehouseType.PRIVATE
	 *         <li>WarehouseType.CLAN
	 *         <li>WarehouseType.CASTLE
	 *         <li>WarehouseType.FREIGHT
	 *         </ul>
	 */
	public WarehouseType getUsingWarehouseType()
	{
		return _usingWHType;
	}

	public GCSArray<L2CubicInstance> getCubics()
	{
		return _cubics == null ? EMPTY_CUBICS : _cubics;
	}

	public void addCubic(final int id, final int level, final int lifetime, final int chance, final boolean givenByOther)
	{
		// удаляем старый кубик и отменяем его действия
		if(_cubics != null)
			for(final L2CubicInstance old : _cubics)
				if(old.getId() == id)
					old.deleteMe();

		if(_cubics == null)
			_cubics = new GCSArray<L2CubicInstance>(4);
		final int mastery = Math.max(0, getSkillLevel(L2Skill.SKILL_CUBIC_MASTERY));
		if(_cubics.size() > mastery)
		{
			sendPacket(Msg.CUBIC_SUMMONING_FAILED);
			return;
		}
		_cubics.add(new L2CubicInstance(this, id, level, lifetime, chance, givenByOther));
	}

	public void delCubic(final L2CubicInstance cubic)
	{
		if(_cubics != null)
			_cubics.remove(cubic);
	}

	public L2CubicInstance getCubic(final int id)
	{
		if(_cubics != null)
			for(final L2CubicInstance cubic : _cubics)
				if(cubic.getId() == id)
					return cubic;
		return null;
	}

	@Override
	public String toString()
	{
		return "player '" + getName() + "'-" + getObjectId();
	}

	/**
	 * @return the modifier corresponding to the Enchant Effect of the Active Weapon (Min : 127).<BR>
	 * <BR>
	 */
	public int getEnchantEffect()
	{
		final L2ItemInstance wpn = getActiveWeaponInstance();

		if(wpn == null)
			return 0;

		return Math.min(127, wpn.getEnchantLevel());
	}

	/**
	 * Set the _lastNpc of the L2Player corresponding to the last Folk witch one the player talked.<BR>
	 * <BR>
	 */
	public void setLastNpc(final L2NpcInstance npc)
	{
		if(npc == null)
			_lastNpc = HardReferences.emptyRef();
		else
			_lastNpc = npc.getRef();
	}

	/**
	 * @return the _lastNpc of the L2Player corresponding to the last Folk witch one the player talked.<BR>
	 * <BR>
	 */
	public L2NpcInstance getLastNpc()
	{
		return _lastNpc.get();
	}

	public void setMultisell(final MultiSellListContainer multisell)
	{
		_multisell = multisell;
	}

	public MultiSellListContainer getMultisell()
	{
		return _multisell;
	}

	/**
	 * @return True if L2Player is a participant in the Festival of Darkness.<BR>
	 * <BR>
	 */
	public boolean isFestivalParticipant()
	{
		return getReflection().isDarknessFestival();
	}

	@Override
	public boolean unChargeShots(final boolean spirit)
	{
		final L2ItemInstance weapon = getActiveWeaponInstance();
		if(weapon == null)
			return false;

		if(spirit)
			weapon.setChargedSpiritshot(L2ItemInstance.CHARGED_NONE);
		else
			weapon.setChargedSoulshot(L2ItemInstance.CHARGED_NONE);

		rechargeAutoSoulShot();
		return true;
	}

	public boolean unChargeFishShot()
	{
		final L2ItemInstance weapon = getActiveWeaponInstance();
		if(weapon == null)
			return false;
		weapon.setChargedFishshot(false);
		rechargeAutoSoulShot();
		return true;
	}

	public void rechargeAutoSoulShot()
	{
		synchronized (_activeSoulShots)
		{
			L2ItemInstance item;
			IItemHandler handler;
			for(final Integer e : _activeSoulShots)
			{
				if(e == null)
					continue;
				item = getInventory().findItemByItemId(e);
				if(item == null)
				{
					_activeSoulShots.remove(e);
					continue;
				}
				handler = ItemHandler.getInstance().getItemHandler(e);
				if(handler != null)
					handler.useItem(this, item);
			}
		}
	}

	public boolean getChargedFishShot()
	{
		final L2ItemInstance weapon = getActiveWeaponInstance();
		return weapon != null && weapon.getChargedFishshot();
	}

	@Override
	public boolean getChargedSoulShot()
	{
		final L2ItemInstance weapon = getActiveWeaponInstance();
		return weapon != null && weapon.getChargedSoulshot() == L2ItemInstance.CHARGED_SOULSHOT;
	}

	@Override
	public int getChargedSpiritShot()
	{
		final L2ItemInstance weapon = getActiveWeaponInstance();
		if(weapon == null)
			return 0;
		return weapon.getChargedSpiritshot();
	}

	public void addAutoSoulShot(final Integer itemId)
	{
		_activeSoulShots.add(itemId);
	}

	public void removeAutoSoulShot(final Integer itemId)
	{
		_activeSoulShots.remove(itemId);
	}

	public ConcurrentSkipListSet<Integer> getAutoSoulShot()
	{
		return _activeSoulShots;
	}

	public void setInvisible(final boolean vis)
	{
		setInvisible(vis, false);
	}

	public void setInvisible(final boolean vis, final boolean effect)
	{
		if(effect)
			getEffectList().stopEffects(EffectType.Invisible);
		_invisible = vis;
	}

	@Override
	public boolean isInvisible()
	{
		return _invisible;
	}

	public int getClanPrivileges()
	{
		if(_clan == null)
			return 0;
		if(isClanLeader())
			return L2Clan.CP_ALL;
		if(_powerGrade < 1 || _powerGrade > 9)
			return 0;
		final RankPrivs privs = _clan.getRankPrivs(_powerGrade);
		if(privs != null)
			return privs.getPrivs();
		return 0;
	}

	public synchronized boolean enterObserverMode(final Location loc)
	{
		_observerRegion = L2World.getRegion(loc, 0);
		if(_observerRegion == null)
			return false;

		setTarget(null);
		stopMove();
		sitDown();
		block();

		// "Телепортируемся"
		setObserverMode(1);

		// Отображаем надпись над головой
		broadcastCharInfo();

		sendPacket(new ObserverStart(loc));

		return true;
	}

	public void appearObserverMode()
	{
		final L2WorldRegion observNeighbor = _observerRegion;
		final L2WorldRegion currentRegion = getCurrentRegion();
		if(observNeighbor == null || currentRegion == null)
		{
			if(getOlympiadObserveId() == -1)
				leaveObserverMode();
			else
				leaveOlympiadObserverMode();
			return;
		}

		setObserverMode(3);

		// Очищаем все видимые обьекты
		for(final L2WorldRegion neighbor : currentRegion.getNeighbors())
			neighbor.removeObjectsFromPlayer(this);

		// Добавляем фэйк в точку наблюдения
		if(!_observerRegion.equals(currentRegion))
			_observerRegion.addObject(this);

		// Показываем чару все обьекты, что находятся в точке наблюдения и соседних регионах
		for(final L2WorldRegion neighbor : _observerRegion.getNeighbors())
			neighbor.showObjectsToPlayer(this);

		if(getOlympiadObserveId() > -1)
		{
			final OlympiadGame game = Olympiad.getOlympiadGame(getOlympiadObserveId());
			if(game != null)
				game.broadcastInfo(null, this, true);
		}
	}

	public void returnFromObserverMode()
	{
		setObserverMode(0);
		_observerRegion = null;
		_olympiadObserveId = -1;

		final L2WorldRegion currentRegion = getCurrentRegion();

		// Показываем чару все обьекты, что находятся в точке воврата и соседних регионах
		if(currentRegion != null)
			for(final L2WorldRegion neighbor : currentRegion.getNeighbors())
				neighbor.showObjectsToPlayer(this);

		broadcastUserInfo(true);
	}

	public void leaveObserverMode()
	{
		// идаляем фэйк из точки наблюдения и удаляем у чара все обьекты, что там находятся
		final L2WorldRegion observNeighbor = _observerRegion;

		// идаляем фэйк из точки наблюдения и удаляем у чара все обьекты, что там находятся
		if(observNeighbor != null)
			for(final L2WorldRegion neighbor : observNeighbor.getNeighbors())
			{
				neighbor.removeObjectsFromPlayer(this);
				neighbor.removeObject(this, false);
			}

		// Нужно при телепорте с более высокой точки на более низкую, иначе наносится вред от "падения"
		setLastClientPosition(null);
		setLastServerPosition(null);

		_observerRegion = null;
		setObserverMode(2);

		setTarget(null);
		unblock();
		standUp();

		// Выходим из режима обсервинга
		sendPacket(new ObserverEnd(this));
	}

	public void enterOlympiadObserverMode(final Location loc, final int id)
	{
		_observerRegion = L2World.getRegion(loc, 0);
		if(_observerRegion == null)
			return;

		setTarget(null);
		setInvul(true);
		setInvisible(true);

		// Очищаем все видимые обьекты
		if(getCurrentRegion() != null)
			for(final L2WorldRegion neighbor : getCurrentRegion().getNeighbors())
				neighbor.removePlayerFromOtherPlayers(this);

		stopMove();
		// block();

		_olympiadObserveId = id;
		setObserverMode(1);

		// Отображаем надпись над головой
		// broadcastCharInfo();

		// Меняем интерфейс
		sendPacket(ExOlympiadMode.SPECTATE);

		// Нужно при телепорте с более высокой точки на более низкую, иначе наносится вред от "падения"
		setLastClientPosition(null);
		setLastServerPosition(null);

		if(getPet() != null)
			getPet().unSummon();

		if(!getCubics().isEmpty())
		{
			for(final L2CubicInstance cubic : getCubics())
				cubic.deleteMe();
			getCubics().clear();
		}
		if(getParty() != null)
			getParty().oustPartyMember(this);
		if(isSitting())
			standUp();

		// Телепортируемся
		sendPacket(new TeleportToLocation(this, loc));
	}

	public void leaveOlympiadObserverMode()
	{
		// Удаляем фэйк из точки наблюдения и удаляем у чара все обьекты, что там находятся
		final L2WorldRegion observNeighbor = _observerRegion;
		if(observNeighbor != null)
			for(final L2WorldRegion neighbor : observNeighbor.getNeighbors())
			{
				neighbor.removeObjectsFromPlayer(this);
				neighbor.removeObject(this, false);
			}

		_observerRegion = null;
		setObserverMode(2);

		setTarget(null);
		setInvul(false);
		setInvisible(false);

		standUp();
		// unblock();

		Olympiad.removeSpectator(_olympiadObserveId, this);
		_olympiadObserveId = -1;

		// Меняем интерфейс
		sendPacket(ExOlympiadMode.RETURN, new ExOlympiadMatchEnd());

		// Нужно при телепорте с более высокой точки на более низкую, иначе наносится вред от "падения"
		setLastClientPosition(null);
		setLastServerPosition(null);

		// ибираем надпись над головой
		broadcastUserInfo(true);

		// "Телепортируемся"
		sendPacket(new TeleportToLocation(this, getLoc()));
	}

	public void setOlympiadSide(final int i)
	{
		_olympiadSide = i;
	}

	public int getOlympiadSide()
	{
		return _olympiadSide;
	}

	public void setOlympiadGameId(final int id)
	{
		_olympiadGameId = id;
	}

	public int getOlympiadGameId()
	{
		return _olympiadGameId;
	}

	public int getOlympiadObserveId()
	{
		return _olympiadObserveId;
	}

	@Override
	public boolean inObserverMode()
	{
		return _observerMode > 0;
	}

	public int getObserverMode()
	{
		return _observerMode;
	}

	public void setObserverMode(final int mode)
	{
		_observerMode = mode;
	}

	public L2WorldRegion getObserverRegion()
	{
		return _observerRegion;
	}

	public int getTeleMode()
	{
		return _telemode;
	}

	public void setTeleMode(final int mode)
	{
		_telemode = mode;
	}

	public int getUnstuck()
	{
		return _unstuck;
	}

	public void setUnstuck(final int mode)
	{
		_unstuck = mode;
	}

	public void setLoto(final int i, final int val)
	{
		_loto[i] = val;
	}

	public int getLoto(final int i)
	{
		return _loto[i];
	}

	public void setRace(final int i, final int val)
	{
		_race[i] = val;
	}

	public int getRace(final int i)
	{
		return _race[i];
	}

	public boolean getMessageRefusal()
	{
		return _messageRefusal;
	}

	public void setMessageRefusal(final boolean mode)
	{
		_messageRefusal = mode;
		sendPacket(new EtcStatusUpdate(this));
	}

	public void setTradeRefusal(final boolean mode)
	{
		_tradeRefusal = mode;
	}

	public boolean getTradeRefusal()
	{
		return _tradeRefusal;
	}

	public void setExchangeRefusal(final boolean mode)
	{
		_exchangeRefusal = mode;
	}

	public boolean getExchangeRefusal()
	{
		return _exchangeRefusal;
	}

	public void addToBlockList(final String charName)
	{
		if(charName == null || charName.equalsIgnoreCase(getName()) || isInBlockList(charName))
		{
			// уже в списке
			sendPacket(Msg.YOU_HAVE_FAILED_TO_REGISTER_THE_USER_TO_YOUR_IGNORE_LIST);
			return;
		}

		final L2Player block_target = L2ObjectsStorage.getPlayer(charName);

		if(block_target != null)
		{
			if(block_target.isGM())
			{
				sendPacket(Msg.YOU_MAY_NOT_IMPOSE_A_BLOCK_ON_A_GM);
				return;
			}
			_blockList.put(block_target.getObjectId(), block_target.getName());
			sendPacket(new SystemMessage(SystemMessage.S1_HAS_BEEN_ADDED_TO_YOUR_IGNORE_LIST).addString(block_target.getName()));
			block_target.sendPacket(new SystemMessage(SystemMessage.S1__HAS_PLACED_YOU_ON_HIS_HER_IGNORE_LIST).addString(getName()));
			return;
		}

		// чар не в игре
		final int charId = Util.GetCharIDbyName(charName);

		if(charId == 0)
		{
			// чар не существует
			sendPacket(Msg.YOU_HAVE_FAILED_TO_REGISTER_THE_USER_TO_YOUR_IGNORE_LIST);
			return;
		}

		if(Config.gmlist.containsKey(charId) && Config.gmlist.get(charId).IsGM)
		{
			sendPacket(Msg.YOU_MAY_NOT_IMPOSE_A_BLOCK_ON_A_GM);
			return;
		}
		_blockList.put(charId, charName);
		sendPacket(new SystemMessage(SystemMessage.S1_HAS_BEEN_ADDED_TO_YOUR_IGNORE_LIST).addString(charName));
	}

	public void removeFromBlockList(final String charName)
	{
		int charId = 0;
		for(final int blockId : _blockList.keys())
			if(charName.equalsIgnoreCase(_blockList.get(blockId)))
			{
				charId = blockId;
				break;
			}
		if(charId == 0)
		{
			sendPacket(Msg.YOU_HAVE_FAILED_TO_DELETE_THE_CHARACTER_FROM_IGNORE_LIST);
			return;
		}
		sendPacket(new SystemMessage(SystemMessage.S1_HAS_BEEN_REMOVED_FROM_YOUR_IGNORE_LIST).addString(_blockList.remove(charId)));
		final L2Player block_target = L2ObjectsStorage.getPlayer(charId);
		if(block_target != null)
			block_target.sendMessage(getName() + " has removed you from his/her Ignore List."); // В системных(619 == 620) мессагах ошибка ;)
	}

	public boolean isInBlockList(final L2Player player)
	{
		return isInBlockList(player.getObjectId());
	}

	public boolean isInBlockList(final int charId)
	{
		return _blockList != null && _blockList.containsKey(charId);
	}

	public boolean isInBlockList(final String charName)
	{
		for(final int blockId : _blockList.keys())
			if(charName.equalsIgnoreCase(_blockList.get(blockId)))
				return true;
		return false;
	}

	private void restoreBlockList()
	{
		_blockList.clear();

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT target_Id, char_name FROM character_blocklist LEFT JOIN characters ON ( character_blocklist.target_Id = characters.obj_Id ) WHERE character_blocklist.obj_Id = ?");
			statement.setInt(1, getObjectId());
			rs = statement.executeQuery();
			while (rs.next())
				_blockList.put(rs.getInt("target_Id"), rs.getString("char_name"));
		}
		catch(final SQLException e)
		{
			_log.warning("Can't restore player blocklist " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	private void storeBlockList()
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			statement.executeUpdate("DELETE FROM character_blocklist WHERE obj_Id=" + getObjectId());

			if(_blockList.isEmpty())
				return;

			final SqlBatch b = new SqlBatch("INSERT IGNORE INTO `character_blocklist` (`obj_Id`,`target_Id`) VALUES");
			synchronized (_blockList)
			{
				StringBuilder sb;
				for(final int objId : _blockList.keys())
				{
					sb = StringUtil.startAppend(50, "(", getObjectId(), ",", objId, ")");
					b.write(sb.toString());
				}
			}
			if(!b.isEmpty())
				statement.executeUpdate(b.close());
		}
		catch(final Exception e)
		{
			_log.warning("Can't store player blocklist " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public boolean isBlockAll()
	{
		return _blockAll;
	}

	public void setBlockAll(final boolean state)
	{
		_blockAll = state;
	}

	public Collection<String> getBlockList()
	{
		return _blockList.valueCollection();
	}

	public void setConnected(final boolean connected)
	{
		_isConnected = connected;
	}

	public boolean isConnected()
	{
		return _isConnected;
	}

	/**
	 * истанавливаем статус героя
	 * 
	 * @param hero
	 *            true - герой, false - не герой))
	 * @param type
	 *            0 - ничего не делать, 1 - добавить скилы, 2 удалить скилы
	 */
	public void setHero(final boolean hero, final int type)
	{
		_hero = hero;

		switch (type)
		{
			case 0: // ничего не делаем)
				break;
			case 1:// Добавляем скилы
				if(getBaseClassId() != getActiveClassId())
					return;

				for(final L2Skill sk : HeroSkillTable.getHeroSkills())
					super.addSkill(sk);

				sendPacket(new SkillList(this));
				break;
			case 2:// удаляем скилы
				for(final L2Skill sk : HeroSkillTable.getHeroSkills())
					super.removeSkill(sk);

				sendPacket(new SkillList(this));
				break;
			default:
				_log.warning("L2Player: unknow type " + type + " in setHero()");
		}
	}

	@Override
	public boolean isHero()
	{
		return _hero;
	}

	public void setIsInOlympiadMode(final boolean b)
	{
		_inOlympiadMode = b;
	}

	/** Игрок находиться на олимпиаде (на арене) */
	@Override
	public boolean isInOlympiadMode()
	{
		return _inOlympiadMode;
	}

	/** Игрок находиться на арене, бой <font color=red><b>ещё не начался</b></font> */
	public boolean isOlympiadGameStart()
	{
		final int id = _olympiadGameId;
		if(id < 0)
			return false;

		final OlympiadGame og = Olympiad.getOlympiadGame(id);
		return og != null && og.getState() == 1;
	}

	/** Игрок находиться на арене, бой <font color=red><b>начался</b></font> */
	public boolean isOlympiadCompStart()
	{
		final int id = _olympiadGameId;
		if(id < 0)
			return false;

		final OlympiadGame og = Olympiad.getOlympiadGame(id);
		return og != null && og.getState() == 2;
	}

	/**
	 * истанавливаем статус дворянина
	 * 
	 * @param noble
	 *            true - дворянин, false - не дворянин))
	 * @param type
	 *            0 - ничего не делать, 1 - добавить скилы, 2 - удалить скилы.
	 */
	public void setNoble(final boolean noble, final int type)
	{
		_noble = noble;

		switch (type)
		{
			case 0: // ничего не делаем)
				break;
			case 1:// Добавляем скилы
				for(final L2Skill sk : NobleSkillTable.getNobleSkills())
					super.addSkill(sk);
				sendPacket(new SkillList(this));
				break;
			case 2:// идаляем скилы
				for(final L2Skill sk : NobleSkillTable.getNobleSkills())
					super.removeSkill(sk);
				sendPacket(new SkillList(this));
				break;
			default:
				_log.warning("L2Player: unknow type " + type + " in setNoble()");
		}
	}

	public boolean isNoble()
	{
		return _noble;
	}

	public int getSubLevel()
	{
		return isSubClassActive() ? _level : 0;
	}

	/* varka silenos and ketra orc quests related functions */
	public void updateKetraVarka()
	{
		if(Functions.getItemCount(this, 7215) > 0)
			_ketra = 5;
		else if(Functions.getItemCount(this, 7214) > 0)
			_ketra = 4;
		else if(Functions.getItemCount(this, 7213) > 0)
			_ketra = 3;
		else if(Functions.getItemCount(this, 7212) > 0)
			_ketra = 2;
		else if(Functions.getItemCount(this, 7211) > 0)
			_ketra = 1;
		else if(Functions.getItemCount(this, 7225) > 0)
			_varka = 5;
		else if(Functions.getItemCount(this, 7224) > 0)
			_varka = 4;
		else if(Functions.getItemCount(this, 7223) > 0)
			_varka = 3;
		else if(Functions.getItemCount(this, 7222) > 0)
			_varka = 2;
		else if(Functions.getItemCount(this, 7221) > 0)
			_varka = 1;
		else
		{
			_varka = 0;
			_ketra = 0;
		}
	}

	public int getVarka()
	{
		return _varka;
	}

	public int getKetra()
	{
		return _ketra;
	}

	public void setRam(final int faction)
	{
		_ram = faction;
	}

	public int getRam()
	{
		return _ram;
	}

	public void setPledgeType(final int typeId)
	{
		_pledgeType = typeId;
	}

	public int getPledgeType()
	{
		return _pledgeType;
	}

	public void setLvlJoinedAcademy(final int lvl)
	{
		_lvlJoinedAcademy = lvl;
	}

	public int getLvlJoinedAcademy()
	{
		return _lvlJoinedAcademy;
	}

	public void setPledgeClass(final int classId)
	{
		_pledgeClass = classId;
	}

	public int getPledgeClass()
	{
		return _pledgeClass;
	}

	public void updatePledgeClass()
	{
		final byte CLAN_LEVEL = _clan == null ? -1 : _clan.getLevel();
		final boolean CLAN_LEADER = _clan != null && _clan.getLeaderId() == _objectId;
		final boolean IN_ACADEMY = _clan != null && _clan.isAcademy(_pledgeType);
		final boolean IS_GUARD = _clan != null && _clan.isRoyalGuard(_pledgeType);
		final boolean IS_KNIGHT = _clan != null && _clan.isOrderOfKnights(_pledgeType);
		boolean IS_GUARD_CAPTAIN = false;
		boolean IS_KNIGHT_COMMANDER = false;
		if(_clan != null && _pledgeType == 0)
		{
			final int leaderOf = _clan.getClanMember(_objectId) == null ? 0 : _clan.getClanMember(_objectId).isSubLeader();
			if(_clan.isRoyalGuard(leaderOf))
				IS_GUARD_CAPTAIN = true;
			else if(_clan.isOrderOfKnights(leaderOf))
				IS_KNIGHT_COMMANDER = true;
		}

		switch (CLAN_LEVEL)
		{
			case -1:
				_pledgeClass = RANK_VAGABOND;
				break;
			case 0:
			case 1:
			case 2:
			case 3:
				if(CLAN_LEADER)
					_pledgeClass = RANK_HEIR;
				else
					_pledgeClass = RANK_VASSAL;
				break;
			case 4:
				if(CLAN_LEADER)
					_pledgeClass = RANK_KNIGHT;
				else
					_pledgeClass = RANK_VASSAL;
				break;
			case 5:
				if(CLAN_LEADER)
					_pledgeClass = RANK_WISEMAN;
				else if(IN_ACADEMY)
					_pledgeClass = RANK_VASSAL;
				else
					_pledgeClass = RANK_HEIR;
				break;
			case 6:
				if(CLAN_LEADER)
					_pledgeClass = RANK_BARON;
				else if(IN_ACADEMY)
					_pledgeClass = RANK_VASSAL;
				else if(IS_GUARD_CAPTAIN)
					_pledgeClass = RANK_WISEMAN;
				else if(IS_GUARD)
					_pledgeClass = RANK_HEIR;
				else
					_pledgeClass = RANK_KNIGHT;
				break;
			case 7:
				if(CLAN_LEADER)
					_pledgeClass = RANK_COUNT;
				else if(IN_ACADEMY)
					_pledgeClass = RANK_VASSAL;
				else if(IS_GUARD_CAPTAIN)
					_pledgeClass = RANK_VISCOUNT;
				else if(IS_GUARD)
					_pledgeClass = RANK_KNIGHT;
				else if(IS_KNIGHT_COMMANDER)
					_pledgeClass = RANK_BARON;
				else if(IS_KNIGHT)
					_pledgeClass = RANK_HEIR;
				else
					_pledgeClass = RANK_WISEMAN;
				break;
			case 8:
				if(CLAN_LEADER)
					_pledgeClass = RANK_MARQUIS;
				else if(IN_ACADEMY)
					_pledgeClass = RANK_VASSAL;
				else if(IS_GUARD_CAPTAIN)
					_pledgeClass = RANK_COUNT;
				else if(IS_GUARD)
					_pledgeClass = RANK_WISEMAN;
				else if(IS_KNIGHT_COMMANDER)
					_pledgeClass = RANK_VISCOUNT;
				else if(IS_KNIGHT)
					_pledgeClass = RANK_KNIGHT;
				else
					_pledgeClass = RANK_BARON;
				break;
			case 9:
				if(CLAN_LEADER)
					_pledgeClass = RANK_DUKE;
				else if(IN_ACADEMY)
					_pledgeClass = RANK_VASSAL;
				else if(IS_GUARD)
					_pledgeClass = RANK_BARON;
				else if(IS_KNIGHT)
					_pledgeClass = RANK_WISEMAN;
				else if(IS_GUARD_CAPTAIN)
					_pledgeClass = RANK_MARQUIS;
				else if(IS_KNIGHT_COMMANDER)
					_pledgeClass = RANK_COUNT;
				else
					_pledgeClass = RANK_VISCOUNT;
				break;
			case 10:
				if(CLAN_LEADER)
					_pledgeClass = RANK_GRAND_DUKE;
				else if(IN_ACADEMY)
					_pledgeClass = RANK_VASSAL;
				else if(IS_GUARD)
					_pledgeClass = RANK_VISCOUNT;
				else if(IS_KNIGHT)
					_pledgeClass = RANK_BARON;
				else if(IS_GUARD_CAPTAIN)
					_pledgeClass = RANK_DUKE;
				else if(IS_KNIGHT_COMMANDER)
					_pledgeClass = RANK_MARQUIS;
				else
					_pledgeClass = RANK_COUNT;
				break;
			case 11:
				if(CLAN_LEADER)
					_pledgeClass = RANK_DISTINGUISHED_KING;
				else if(IN_ACADEMY)
					_pledgeClass = RANK_VASSAL;
				else if(IS_GUARD)
					_pledgeClass = RANK_COUNT;
				else if(IS_KNIGHT)
					_pledgeClass = RANK_VISCOUNT;
				else if(IS_GUARD_CAPTAIN)
					_pledgeClass = RANK_GRAND_DUKE;
				else if(IS_KNIGHT_COMMANDER)
					_pledgeClass = RANK_DUKE;
				else
					_pledgeClass = RANK_MARQUIS;
		}

		if(_hero && _pledgeClass < RANK_MARQUIS)
			_pledgeClass = RANK_MARQUIS;
		else if(_noble && _pledgeClass < RANK_BARON)
			_pledgeClass = RANK_BARON;
	}

	public void setPowerGrade(final int grade)
	{
		_powerGrade = grade;
	}

	public int getPowerGrade()
	{
		return _powerGrade;
	}

	public void setApprentice(final int apprentice)
	{
		_apprentice = apprentice;
	}

	public int getApprentice()
	{
		return _apprentice;
	}

	public int getSponsor()
	{
		if(_clan == null)
			return 0;
		return _clan.getClanMember(getObjectId()).getSponsor();
	}

	/**
	 * @param team
	 *            - <font color=blue>1 = Blue</font>, <font color=red>2 = red</font>
	 * @param checksForTeam
	 */
	@Override
	public void setTeam(final int team, final boolean checksForTeam)
	{
		_checksForTeam = checksForTeam;
		if(_team == team)
			return;

		_team = team;

		sendChanges();
		if(getPet() != null)
			getPet().sendChanges();
	}

	@Override
	public int getTeam()
	{
		return _team;
	}

	public boolean isChecksForTeam()
	{
		return _checksForTeam;
	}

	public int getTitleColor()
	{
		return _title_color;
	}

	public void setTitleColor(final int color)
	{
		if(color != 0xFFFF77)
			setVar("titleColor", Integer.toHexString(color));
		else if(color == 0xFFFF77)
			unsetVar("titleColor");
		_title_color = color;
	}

	public void setTitleColor(final int red, final int green, final int blue)
	{
		_title_color = (red & 0xFF) + ((green & 0xFF) << 8) + ((blue & 0xFF) << 16);
		if(_title_color != 0xFFFF77)
			setVar("titleColor", Integer.toHexString(_title_color));
		else
			unsetVar("titleColor");
	}

	public int getNameColor()
	{
		if(inObserverMode())
			return Color.black.getRGB();

		return _nameColor;
	}

	public void setNameColor(final int nameColor)
	{
		if(nameColor != Config.NORMAL_NAME_COLOUR && nameColor != Config.CLANLEADER_NAME_COLOUR && nameColor != Config.GM_NAME_COLOUR && nameColor != Config.SERVICES_OFFLINE_TRADE_NAME_COLOR)
			setVar("namecolor", Integer.toHexString(nameColor));
		else if(nameColor == Config.NORMAL_NAME_COLOUR)
			unsetVar("namecolor");
		_nameColor = nameColor;
	}

	public void setNameColor(final int red, final int green, final int blue)
	{
		_nameColor = (red & 0xFF) + ((green & 0xFF) << 8) + ((blue & 0xFF) << 16);
		if(_nameColor != Config.NORMAL_NAME_COLOUR && _nameColor != Config.CLANLEADER_NAME_COLOUR && _nameColor != Config.GM_NAME_COLOUR && _nameColor != Config.SERVICES_OFFLINE_TRADE_NAME_COLOR)
			setVar("namecolor", Integer.toHexString(_nameColor));
		else
			unsetVar("namecolor");
	}

	public final String toFullString()
	{
		final StringBuilder sb = new StringBuilder(160);
		sb.append("Player '").append(getName()).append("' [oid=").append(_objectId).append(", account='").append(getAccountName()).append(", ip=").append(_connection != null ? _connection.getIpAddr() : "0.0.0.0").append("']");
		return sb.toString();
	}

	/**
	 * истановка переменной без ограничения по времени
	 * 
	 * @param name
	 *            имя переменной
	 * @param value
	 *            значение переменной
	 */
	public void setVar(final String name, final String value)
	{
		if(name == null || value == null)
			return;

		_variables.put(name.hashCode(), new CharVariables("user-var", name, value, -1));

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("REPLACE INTO character_variables (obj_id, type, name, value, expire_time) VALUES (?,'user-var',?,?, -1)");
			statement.setInt(1, _objectId);
			statement.setString(2, name);
			statement.setString(3, value);
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player setVar: name (" + name + "), value (" + value + ")", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * Установка переменной с ограничением по времени действия
	 * 
	 * @param name
	 *            имя переменной
	 * @param value
	 *            значение переменной
	 * @param time
	 *            время действия (в минутах) <br>
	 *            <font color="red">ВНИМАНИЕ: время в базу сохранять в секундах UNIX TIME</font>
	 */
	public void setVar(final String name, final String value, final int time)
	{
		if(name == null || value == null)
			return;

		final long expire_time = System.currentTimeMillis() / 1000 + time * 60;
		_variables.put(name.hashCode(), new CharVariables("user-var", name, value, expire_time));

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("REPLACE INTO character_variables (obj_id, type, name, value, expire_time) VALUES (?,'user-var',?,?,?)");
			statement.setInt(1, _objectId);
			statement.setString(2, name);
			statement.setString(3, value);
			statement.setLong(4, expire_time);
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player setVar: name (" + name + "), value (" + value + "), time (" + time + ")", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * идаление переменной
	 */
	public void unsetVar(final String name)
	{
		if(name == null)
			return;
		if(_variables.remove(name.hashCode()) != null)
			mysql.set("DELETE FROM `character_variables` WHERE `obj_id`='" + _objectId + "' AND `type`='user-var' AND `name`='" + name + "' LIMIT 1");
	}

	/**
	 * @param name
	 *            имя переменной
	 * @return значение переменной name
	 */
	public String getVar(final String name)
	{
		if(_variables.get(name.hashCode()) != null)
			return _variables.get(name.hashCode()).getValue();
		return null;
	}

	public CharVariables getVariable(final String name)
	{
		if(_variables.get(name.hashCode()) != null)
			return _variables.get(name.hashCode());
		return null;
	}

	public boolean getVarB(final String name, final boolean defaultVal)
	{
		final CharVariables var = _variables.get(name.hashCode());
		if(var == null)
			return defaultVal;
		return !(var.getValue().equals("0") || var.getValue().equalsIgnoreCase("false"));
	}

	public boolean getVarB(final String name)
	{
		final CharVariables var = _variables.get(name.hashCode());
		return !(var == null || var.getValue().equals("0") || var.getValue().equalsIgnoreCase("false"));
	}
	public int getVarInt(String name, int defaultVal)
	{
		int result = defaultVal;
		String var = getVar(name);
		if(var != null)
			result = Integer.parseInt(var);
		return result;
	}

	/**
	 * Загрузка всех переменных.<BR>
	 * Тут первая проверка на время.
	 */
	private void loadVariables()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement offline = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			offline = con.prepareStatement("SELECT * FROM character_variables WHERE obj_id = ?");
			offline.setInt(1, _objectId);
			rs = offline.executeQuery();
			while (rs.next())
			{
				final String type = rs.getString("type");
				final String name = rs.getString("name");
				final String value = rs.getString("value");
				final long expire_time = rs.getLong("expire_time");

				// проверяем время действия переменной (текущее время +15 сек)
				if(expire_time > 0 && expire_time <= System.currentTimeMillis() / 1000 + 15)
				{
					unsetVar(name);
					continue;
				}

				_variables.put(name.hashCode(), new CharVariables(type, name, value, expire_time));
			}

			// TODO Здесь обазятельно выставлять все стандартные параметры, иначе будут NPE
			if(getVar("lang@") == null)
				setVar("lang@", Config.DEFAULT_LANG);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player.loadVariables(): error couldn't load vars:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, offline, rs);
		}
	}

	public String getLang()
	{
		return getVar("lang@");
	}

	public int getLangId()
	{
		final String lang = getLang();
		if(lang.equalsIgnoreCase("en") || lang.equalsIgnoreCase("e") || lang.equalsIgnoreCase("eng"))
			return 0;
		if(lang.equalsIgnoreCase("ru") || lang.equalsIgnoreCase("r") || lang.equalsIgnoreCase("rus"))
			return 1;
		return -1;
	}

	public boolean isLangRus()
	{
		return getLangId() == 1;
	}

	/**
	 * Применение временных переменных.
	 */
	public void applyTimeVariables(final boolean onLoad)
	{
		boolean find = false;
		for(final CharVariables var : getTimeLimitedVars(false))
			if(var.getName().equalsIgnoreCase("hero_time"))
			{
				setHero(true, onLoad ? 0 : 1);
				find = true;
			}
			else if(var.getName().equalsIgnoreCase("noble_time"))
			{
				setNoble(true, onLoad ? 0 : 1);
				find = true;
			}
		// Добавляем в менеджер для управления переменными игрока
		if(find)
			CharVariablesManager.getInstance().addPlayer(this);
	}

	/**
	 * @return список переменных c ограничением по времени
	 */
	public CharVariables[] getTimeLimitedVars(final boolean all)
	{
		final GArray<CharVariables> timeVars = new GArray<CharVariables>(5);
		_variables.forEachValue(new TObjectProcedure<CharVariables>()
		{
			@Override
			public boolean execute(final CharVariables var)
			{
				if(var.isTimeLimited() && (all || !var.isExpired() && !all))
					timeVars.addLastUnsafe(var);
				return true;
			}
		});

		return timeVars.toArray(new CharVariables[timeVars.size()]);
	}

	public void stopWaterTask()
	{
		if(_taskWater != null)
		{
			_taskWater.cancel(false);
			_taskWater = null;
			sendPacket(new SetupGauge(2, 0));
			sendChanges();
		}
	}

	public void checkWaterState()
	{
		if(isInZoneWater())
			startWaterTask();
		else
			stopWaterTask();
	}

	private int _reviveRequested = 0;
	private double _revivePower = 0;
	private boolean _isRevivePet = false;

	public void doRevive(final double percent)
	{
		restoreExp(percent);
		doRevive();
	}

	@Override
	public void doRevive()
	{
		super.doRevive();
		unsetVar("lostexp");
		updateEffectIcons();
		rechargeAutoSoulShot();
		_reviveRequested = 0;
		_revivePower = 0;
		 _deathtime = System.currentTimeMillis();

		final Iterable<EventScript> scripts = Scripts.getInstance().getEventScripts(ScriptEventType.ON_REVIVE);
		for(final EventScript script : scripts)
			script.notifyRevive(this);
	}

	public void reviveRequest(final L2Player reviver, final double percent, final boolean isPet)
	{
		if(_reviveRequested == 1)
		{
			if(_isRevivePet == isPet && _revivePower >= percent)
				reviver.sendPacket(Msg.BETTER_RESURRECTION_HAS_BEEN_ALREADY_PROPOSED);
			else if(isPet && !_isRevivePet)
				reviver.sendPacket(Msg.SINCE_THE_MASTER_WAS_IN_THE_PROCESS_OF_BEING_RESURRECTED_THE_ATTEMPT_TO_RESURRECT_THE_PET_HAS_BEEN_CANCELLED);
			else if(isPet && isDead())
				reviver.sendPacket(Msg.WHILE_A_PET_IS_ATTEMPTING_TO_RESURRECT_IT_CANNOT_HELP_IN_RESURRECTING_ITS_MASTER);
			return;
		}
		if(isPet && getPet() != null && getPet().isDead() || !isPet && isDead())
		{
			_reviveRequested = 1;
			_revivePower = percent;
			_isRevivePet = isPet;
			final ConfirmDlgPacket pkt = new ConfirmDlgPacket(SystemMessage.S1_IS_MAKING_AN_ATTEMPT_AT_RESURRECTION_WITH_$S2_EXPERIENCE_POINTS_DO_YOU_WANT_TO_CONTINUE_WITH_THIS_RESURRECTION, 0, ConfirmDlg.REVIVE_ANSWER);
			final String lostexps = getVar("lostexp");
			int lostexp = 0;
			if(lostexps != null)
				lostexp = Integer.parseInt(lostexps);
			pkt.addString(reviver.getName()).addNumber((int) (lostexp * _revivePower / 100));
			sendPacket(pkt);
		}
	}

	public void reviveAnswer(final int answer)
	{
		if(_reviveRequested != 1 || !isDead() && !_isRevivePet || _isRevivePet && getPet() != null && !getPet().isDead())
			return;
		if(answer == 1)
			if(!_isRevivePet)
			{
				if(_revivePower != 0)
					doRevive(_revivePower);
				else
					doRevive();
			}
			else if(getPet() != null)
				if(_revivePower != 0)
					((L2PetInstance) getPet()).doRevive(_revivePower);
				else
					getPet().doRevive();
		_reviveRequested = 0;
		_revivePower = 0;
	}

	/** Координаты точки призыва персонажа */
	private Location _summonCharacterLocation;
	/** Флаг необходимости потребления Summoning Cystall-а при призыве персонажа */
	private int _summonConsumeCrystall = 0;

	/**
	 * Обработчик ответа клиента на призыв персонажа.
	 * 
	 * @param answer
	 *            Идентификатор запроса
	 */
	public void summonCharacterAnswer(final int answer)
	{
		if(answer == 1 && _summonCharacterLocation != null && Call.canBeSummoned(this) == null)
		{
			abortAttack();
			abortCast();
			sendActionFailed();
			broadcastPacket(new StopMove(this));
			if(_summonConsumeCrystall > 0)
			{
				final L2ItemInstance item = getInventory().getItemByItemId(Call.SUMMONING_CRYSTAL);
				if(item != null && item.getCount() >= _summonConsumeCrystall)
				{
					getInventory().destroyItemByItemId(Call.SUMMONING_CRYSTAL, _summonConsumeCrystall, false);
					sendPacket(SystemMessage.removeItems(Call.SUMMONING_CRYSTAL, _summonConsumeCrystall));
					teleToLocation(_summonCharacterLocation);
				}
				else
					sendPacket(Msg.INCORRECT_ITEM_COUNT);
			}
			else
				teleToLocation(_summonCharacterLocation);
		}
		_summonCharacterLocation = null;
		_summonConsumeCrystall = 0;
	}

	/**
	 * Отправляет запрос клиенту на призыв персонажа.
	 * 
	 * @param SummonerName
	 *            Имя призывающего персонажа
	 * @param crystallCount
	 *            количество кристалов которое будет удалено при телепортации
	 * @param loc
	 *            Координаты точки призыва персонажа
	 */
	public void summonCharacterRequest(final String SummonerName, final Location loc, final int crystallCount)
	{
		if(_summonCharacterLocation == null)
		{
			_summonCharacterLocation = loc;
			_summonConsumeCrystall = crystallCount;
			final ConfirmDlgPacket cd = new ConfirmDlgPacket(SystemMessage.S1_WISHES_TO_SUMMON_YOU_FROM_S2_DO_YOU_ACCEPT, 30000, ConfirmDlg.SUMMON_CHARACTER_ANSWER);
			cd.addString(SummonerName).addZoneName(_summonCharacterLocation.x, _summonCharacterLocation.y, _summonCharacterLocation.z);
			sendPacket(cd);
		}
	}

	private String _scriptName = StringUtils.EMPTY;
	private Object[] _scriptArgs = ArrayUtils.EMPTY_OBJECT_ARRAY;

	public void scriptAnswer(final int answer)
	{
		if(answer == 1 && !_scriptName.equals(""))
			callScripts(_scriptName.split(":")[0], _scriptName.split(":")[1], _scriptArgs);
		_scriptName = StringUtils.EMPTY;
		_scriptArgs = ArrayUtils.EMPTY_OBJECT_ARRAY;
	}

	public void scriptRequest(final String text, final String scriptName, final Object[] args)
	{
		if(_scriptName.equals(""))
		{
			_scriptName = scriptName;
			_scriptArgs = args;
			sendPacket(new ConfirmDlgPacket(SystemMessage.S1_S2, 30000, ConfirmDlg.SCRIPT_ANSWER).addString(text));
		}
	}

	public boolean isReviveRequested()
	{
		return _reviveRequested == 1;
	}

	public boolean isRevivingPet()
	{
		return _isRevivePet;
	}

	public void updateNoChannel(final long time)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;

		setNoChannel(time);

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			final String stmt = "UPDATE characters SET nochannel = ? WHERE obj_Id=?";
			statement = con.prepareStatement(stmt);
			statement.setLong(1, _NoChannel > 0 ? _NoChannel / 1000 : _NoChannel);
			statement.setInt(2, getObjectId());
			statement.executeUpdate();
		}
		catch(final Exception e)
		{
			_log.warning("Could not activate nochannel:" + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	private void checkRecom()
	{
		final Calendar temp = Calendar.getInstance();
		temp.set(Calendar.HOUR_OF_DAY, 13);
		temp.set(Calendar.MINUTE, 0);
		temp.set(Calendar.SECOND, 0);
		long count = Math.round((System.currentTimeMillis() / 1000 - _lastAccess) / 86400);
		if(count == 0 && _lastAccess < temp.getTimeInMillis() / 1000 && System.currentTimeMillis() > temp.getTimeInMillis())
			count++;

		for(int i = 1; i < count; i++)
			if(_recomHave < 200)
				_recomHave -= 2;
			else
				_recomHave -= 3;

		if(_recomHave < 0)
			_recomHave = 0;

		if(getLevel() < 10)
			return;

		if(count > 0)
			restartRecom();
	}

	public void restartRecom()
	{
		try
		{
			_recomChars.clear();

			if(getLevel() < 20)
				_recomLeft = 3;
			else if(getLevel() < 40)
				_recomLeft = 6;
			else
				_recomLeft = 9;

			if(_recomHave < 200)
				_recomHave -= 2;
			else
				_recomHave -= 3;

			if(_recomHave < 0)
				_recomHave = 0;
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "", e);
		}
	}

	/** @return true если игрок находиться на транспорте */
	@Override
	public boolean isInVehicle()
	{
		return _vehicle != null;
	}

	/** @return L2Vehicle транспорт */
	public L2Vehicle getVehicle()
	{
		return _vehicle;
	}

	/**
	 * истанавлиевает транспорт
	 * 
	 * @param boat
	 *            - L2Vehicle транспорт
	 */
	public void setVehicle(final L2Vehicle boat)
	{
		_vehicle = boat;
	}

	/** @return позицию на транспорте */
	public Location getInVehiclePosition()
	{
		return _inVehiclePosition;
	}

	/** Устанавливаем позицию на транспорте */
	public void setInVehiclePosition(final Location loc)
	{
		_inVehiclePosition = loc;
	}

	public HashMap<Integer, L2SubClass> getSubClasses()
	{
		return _classlist;
	}

	/**
	 * Возрващает номер для следующего сабкласса.
	 */
	public int getNextNumSub()
	{
		int num = 1;
		for(final L2SubClass sub : _classlist.values())
			if(sub.getNumSub() > num)
				num = sub.getNumSub();
		return num + 1;
	}

	public void setBaseClass(final int baseClass)
	{
		_baseClass = baseClass;
	}

	public int getBaseClassId()
	{
		return _baseClass;
	}

	public int getBaseLevel()
	{
		return _baseLevel;
	}

	public long getBaseExp()
	{
		return _baseExp;
	}

	public long getBaseSp()
	{
		return _baseSp;
	}

	private final void setActiveClass(final L2SubClass activeClass)
	{
		if(activeClass == null)
			_log.log(Level.WARNING, "WARNING! setActiveClass(null)", new Exception("setActiveClass"));
		_activeClass = activeClass;
	}

	public L2SubClass getActiveClass()
	{
		return _activeClass;
	}

	public int getActiveClassId()
	{
		return getActiveClass().getClassId();
	}

	public void setActiveClassId(final int id)
	{
		getActiveClass().setClassId(id);
	}

	public final int getMaxLevel()
	{
		if(getClassId() != null)
			if(getBaseClassId() == getClassId().getId())
				return Experience.getMaxLevel();
		return Experience.getMaxSubLevel(); // на всяк случай, чтоб всегда было значение
	}

	/**
	 * Changing index of class in DB, used for changing class when finished professional quests
	 * 
	 * @param oldclass
	 * @param newclass
	 */
	public void changeClassInDb(final int oldclass, final int newclass)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement("UPDATE character_subclasses SET class_id=? WHERE char_obj_id=? AND class_id=?");
			statement.setInt(1, newclass);
			statement.setInt(2, getObjectId());
			statement.setInt(3, oldclass);
			statement.executeUpdate();
			DbUtils.close(statement);

			statement = con.prepareStatement("UPDATE character_hennas SET class_index=? WHERE char_obj_id=? AND class_index=?");
			statement.setInt(1, newclass);
			statement.setInt(2, getObjectId());
			statement.setInt(3, oldclass);
			statement.executeUpdate();
			DbUtils.close(statement);

			statement = con.prepareStatement("UPDATE character_shortcuts SET class_index=? WHERE char_obj_id=? AND class_index=?");
			statement.setInt(1, newclass);
			statement.setInt(2, getObjectId());
			statement.setInt(3, oldclass);
			statement.executeUpdate();
			DbUtils.close(statement);

			statement = con.prepareStatement("UPDATE character_skills SET class_index=? WHERE char_obj_id=? AND class_index=?");
			statement.setInt(1, newclass);
			statement.setInt(2, getObjectId());
			statement.setInt(3, oldclass);
			statement.executeUpdate();
			DbUtils.close(statement);

			statement = con.prepareStatement("UPDATE character_effects_save SET class_index=? WHERE char_obj_id=? AND class_index=?");
			statement.setInt(1, newclass);
			statement.setInt(2, getObjectId());
			statement.setInt(3, oldclass);
			statement.executeUpdate();
			DbUtils.close(statement);

			statement = con.prepareStatement("UPDATE character_skills_save SET class_index=? WHERE char_obj_id=? AND class_index=?");
			statement.setInt(1, newclass);
			statement.setInt(2, getObjectId());
			statement.setInt(3, oldclass);
			statement.executeUpdate();
			DbUtils.close(statement);
		}
		catch(final SQLException e)
		{
			_log.log(Level.WARNING, "Error while changing index of class in DB for " + this, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * Сохраняет информацию о классах в БД
	 */
	public void storeCharSubClasses()
	{
		final L2SubClass currentSub = _classlist.get(getActiveClassId());
		if(currentSub != null)
		{
			currentSub.setCp(getCurrentCp());
			currentSub.setExp(getExp());
			currentSub.setHp(getCurrentHp());
			currentSub.setLevel(getLevel());
			currentSub.setMp(getCurrentMp());
			currentSub.setSp(getSp());
			currentSub.setActive(true);
			_classlist.put(getActiveClassId(), currentSub);
		}
		else
			_log.warning("Could not store char sub data, main class " + getActiveClassId() + " not found for " + this);

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE character_subclasses SET exp=?,sp=?,curHp=?,curMp=?,curCp=?,level=?,active=?,isBase=?,death_penalty=?,num_sub=? WHERE char_obj_id=? AND class_id=? LIMIT 1");
			for(final L2SubClass subClass : _classlist.values())
			{
				statement.setLong(1, subClass.getExp());
				statement.setLong(2, subClass.getSp());
				statement.setDouble(3, subClass.getHp());
				statement.setDouble(4, subClass.getMp());
				statement.setDouble(5, subClass.getCp());
				statement.setInt(6, subClass.getLevel());
				statement.setInt(7, subClass.isActive() ? 1 : 0);
				statement.setInt(8, subClass.isBase() ? 1 : 0);
				statement.setInt(9, subClass.getDeathPenalty().getLevelOnSaveDB());
				statement.setInt(10, subClass.getNumSub());
				statement.setInt(11, getObjectId());
				statement.setInt(12, subClass.getClassId());
				statement.execute();
			}
			DbUtils.close(statement);

			statement = con.prepareStatement("UPDATE LOW_PRIORITY character_subclasses SET maxHp=?,maxMp=?,maxCp=? WHERE char_obj_id=? AND active=1 LIMIT 1");
			statement.setDouble(1, getMaxHp());
			statement.setDouble(2, getMaxMp());
			statement.setDouble(3, getMaxCp());
			statement.setInt(4, getObjectId());
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Could not store char sub data: char_obj_id = " + getObjectId(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * Restore list of character professions and set up active proof Used when character is loading
	 */
	public static void restoreCharSubClasses(final L2Player player)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT class_id,exp,sp,level,curHp,curCp,curMp,active,isBase,death_penalty,num_sub FROM character_subclasses WHERE char_obj_id=?");
			statement.setInt(1, player.getObjectId());
			rset = statement.executeQuery();
			while (rset.next())
			{
				final L2SubClass subClass = new L2SubClass();
				subClass.setBase(rset.getInt("isBase") != 0);
				subClass.setClassId(rset.getInt("class_id"));
				subClass.setLevel(rset.getByte("level"));
				subClass.setExp(rset.getLong("exp"));
				subClass.setSp(rset.getInt("sp"));
				subClass.setHp(rset.getDouble("curHp"));
				subClass.setMp(rset.getDouble("curMp"));
				subClass.setCp(rset.getDouble("curCp"));
				subClass.setActive(rset.getInt("active") != 0);
				subClass.setDeathPenalty(new DeathPenalty(player, rset.getByte("death_penalty")));

				final int num = rset.getInt("num_sub");
				if(num == -1)
					subClass.setNumSub(player.getNextNumSub());
				else if(subClass.isBase())
					subClass.setNumSub(1);
				else
					subClass.setNumSub(num);

				subClass.setPlayer(player);
				player.getSubClasses().put(subClass.getClassId(), subClass);
			}

			if(player.getSubClasses().size() == 0)
				throw new Exception("There are no one subclass for " + player.toFullString());

			final int BaseClassId = player.getBaseClassId();
			if(BaseClassId == -1)
				throw new Exception("There are no base subclass for " + player.toFullString());

			// истанока активного класса
			for(final L2SubClass subClass : player.getSubClasses().values())
				if(subClass.isActive())
				{
					player.setActiveSubClass(subClass.getClassId(), false);
					break;
				}

			// Если активного класса нет, то ставим основной.
			if(player.getActiveClass() == null)
			{
				// если из-за какого-либо сбоя ни один из сабкласов не отмечен как активный помечаем базовый как активный
				final L2SubClass subClass = player.getSubClasses().get(BaseClassId);
				subClass.setActive(true);
				player.setActiveSubClass(subClass.getClassId(), false);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Could not restore char sub-classes: char_objectId = " + player.getObjectId(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	/**
	 * Добавить класс, используется только для сабклассов
	 * 
	 * @param classId
	 *            - Id класса
	 * @param num
	 *            - номер субкласса
	 * @param isNew
	 *            - true если создание нового сабкласса
	 */
	public boolean addSubClass(final int classId, final int num, final boolean isNew)
	{
		if(!_subclassLock.tryLock())
			return false;

		try
		{
			if(_classlist.size() >= 4 + Config.ALT_GAME_SUB_ADD)
				return false;

			if(_classlist.containsKey(classId))
				return false;

			final ClassId newId = ClassId.values()[classId];

			final L2SubClass newClass = new L2SubClass();
			if(newId.getRace() == null)
				return false;

			int numSub = num;
			// Если это добавление нового субкласса
			if(isNew)
				numSub = getNextNumSub();

			newClass.setClassId(classId);
			newClass.setPlayer(this);
			newClass.setNumSub(numSub);

			_classlist.put(classId, newClass);

			ThreadConnection con = null;
			FiltredPreparedStatement statement = null;
			try
			{
				// Store the basic info about this new sub-class.
				con = L2DatabaseFactory.getInstance().getConnection();
				statement = con.prepareStatement("INSERT INTO character_subclasses (char_obj_id, class_id, exp, sp, curHp, curMp, curCp, maxHp, maxMp, maxCp, level, active, isBase, death_penalty, num_sub) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				statement.setInt(1, getObjectId());
				statement.setInt(2, newClass.getClassId());
				statement.setLong(3, Experience.LEVEL[40]);
				statement.setInt(4, 0);
				statement.setDouble(5, getCurrentHp());
				statement.setDouble(6, getCurrentMp());
				statement.setDouble(7, getCurrentCp());
				statement.setDouble(8, getCurrentHp());
				statement.setDouble(9, getCurrentMp());
				statement.setDouble(10, getCurrentCp());
				statement.setInt(11, 40);
				statement.setInt(12, 0);
				statement.setInt(13, 0);
				statement.setInt(14, 0);
				statement.setInt(15, numSub);
				statement.execute();
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "Could not add character sub-class: ", e);
				return false;
			}
			finally
			{
				DbUtils.closeQuietly(con, statement);
			}

			setActiveSubClass(classId, isNew);

			// Add all the necessary skills up to level 40 for this new class.
			boolean countUnlearnable = true;
			int unLearnable = 0;
			int numSkillsAdded = 0;
			L2SkillLearn[] skills = SkillTreeTable.getInstance().getAvailableSkills(this, newId);
			while (skills.length > unLearnable)
			{
				for(final L2SkillLearn s : skills)
				{
					final L2Skill sk = SkillTable.getInstance().getInfo(s.getId(), s.getLevel());
					if(sk == null || !sk.getCanLearn(newId))
					{
						if(countUnlearnable)
							unLearnable++;
						continue;
					}
					addSkill(sk, true);
					numSkillsAdded++;
				}
				countUnlearnable = false;
				skills = SkillTreeTable.getInstance().getAvailableSkills(this, newId);
			}

			restoreSkills();
			rewardSkills();
			sendPacket(new SkillList(this));
			setCurrentHpMp(getMaxHp(), getMaxMp());
			setCurrentCp(getMaxCp());

			if(Config.DEBUG)
				_log.info(numSkillsAdded + " skills added for " + getName() + "'s sub-class.");

			return true;
		}
		finally
		{
			_subclassLock.unlock();
		}
	}

	/**
	 * Удаляет всю информацию о классе и добавляет новую, только для сабклассов
	 */
	public boolean modifySubClass(final int oldClassId, final int newClassId)
	{
		if(!_subclassLock.tryLock())
			return false;

		try
		{
			final L2SubClass originalClass = _classlist.get(oldClassId);
			if(originalClass.isBase())
				return false;

			final int numSub = originalClass.getNumSub();

			ThreadConnection con = null;
			FiltredPreparedStatement statement = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();

				// Remove all basic info stored about this sub-class.
				statement = con.prepareStatement("DELETE FROM character_subclasses WHERE char_obj_id=? AND class_id=? AND isBase = 0");
				statement.setInt(1, getObjectId());
				statement.setInt(2, oldClassId);
				statement.execute();
				DbUtils.close(statement);

				// Remove all skill info stored for this sub-class.
				statement = con.prepareStatement("DELETE FROM character_skills WHERE char_obj_id=? AND class_index=? ");
				statement.setInt(1, getObjectId());
				statement.setInt(2, oldClassId);
				statement.execute();
				DbUtils.close(statement);

				// Remove all saved skills info stored for this sub-class.
				statement = con.prepareStatement("DELETE FROM character_skills_save WHERE char_obj_id=? AND class_index=? ");
				statement.setInt(1, getObjectId());
				statement.setInt(2, oldClassId);
				statement.execute();
				DbUtils.close(statement);

				// Remove all saved effects stored for this sub-class.
				statement = con.prepareStatement("DELETE FROM character_effects_save WHERE char_obj_id=? AND class_index=? ");
				statement.setInt(1, getObjectId());
				statement.setInt(2, oldClassId);
				statement.execute();
				DbUtils.close(statement);

				// Remove all henna info stored for this sub-class.
				statement = con.prepareStatement("DELETE FROM character_hennas WHERE char_obj_id=? AND class_index=? ");
				statement.setInt(1, getObjectId());
				statement.setInt(2, oldClassId);
				statement.execute();
				DbUtils.close(statement);

				// Remove all shortcuts info stored for this sub-class.
				statement = con.prepareStatement("DELETE FROM character_shortcuts WHERE char_obj_id=? AND class_index=? ");
				statement.setInt(1, getObjectId());
				statement.setInt(2, oldClassId);
				statement.execute();
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "Could not delete char sub-class: ", e);
			}
			finally
			{
				DbUtils.closeQuietly(con, statement);
			}
			_classlist.remove(oldClassId);
			return newClassId > 0 ? addSubClass(newClassId, numSub, false) : true;
		}
		finally
		{
			_subclassLock.unlock();
		}
	}

	/**
	 * Устанавливает активный сабкласс<BR>
	 * <li>Retrieve from the database all skills of this L2Player and add them to _skills</li><BR>
	 * <li>Retrieve from the database all macroses of this L2Player and add them to _macroses</li><BR>
	 * <li>Retrieve from the database all shortCuts of this L2Player and add them to _shortCuts</li><BR>
	 */
	public void setActiveSubClass(final int subId, final boolean store)
	{
		if(!_subclassLock.tryLock())
			return;

		try
		{
			final L2SubClass sub = _classlist.get(subId);
			if(sub == null)
			{
				_log.warning("WARNING! setActiveSubClass :: sub == null :: subId == " + subId);
				Thread.dumpStack();
				return;
			}

			if(getActiveClass() != null)
			{
				storeEffects();
				storeDisableSkills();

				if(QuestManager.getQuest(422) != null)
				{
					final String qn = QuestManager.getQuest(422).getName();
					if(qn != null)
					{
						final QuestState qs = getQuestState(qn);
						if(qs != null)
							qs.exitCurrentQuest(true);
					}
				}
			}

			if(store)
			{
				final L2SubClass oldsub = getActiveClass();
				oldsub.setCp(getCurrentCp());
				oldsub.setHp(getCurrentHp());
				oldsub.setMp(getCurrentMp());
				oldsub.setExp(getExp());
				oldsub.setSp(getSp());
				oldsub.setLevel(getLevel());
				oldsub.setActive(false);
				_classlist.put(getActiveClassId(), oldsub);
			}

			sub.setActive(true);
			setActiveClass(sub);
			_classlist.put(getActiveClassId(), sub);
			_level = sub.getLevel();

			setExp(sub.getExp());
			setSp(sub.getSp());

			// первый раз устанавливаем рейты
			updateRate();

			setClassId(subId, false);

			removeAllSkills();
			getEffectList().stopAllEffects();

			if(getPet() != null && (getPet().isSummon() || getPet().getNpcId() == 16035 && !isMageClass() || getPet().getNpcId() == 16034 && isMageClass()))
				getPet().unSummon();

			if(_cubics != null && !_cubics.isEmpty())
			{
				for(final L2CubicInstance cubic : _cubics)
					cubic.deleteMe();
				_cubics.clear();
				_cubics = null;
			}
			setAgathion(0);

			checkRecom();
			restoreSkills();
			rewardSkills();
			refreshExpertisePenalty();
			sendPacket(new ExStorageMaxCount(this), new SkillList(this));

			getInventory().refreshListeners();
			getInventory().checkAllConditions();

			for(byte i = 0; i < 3; i++)
				_henna[i] = null;

			restoreHenna();
			sendPacket(new HennaInfo(this));

			restoreTeleportBookmark();

			restoreEffects();
			if(isVisible()) // костыль для загрузки чара
				restoreDisableSkills();

			setCurrentHpMp(sub.getHp(), sub.getMp());
			setCurrentCp(sub.getCp());

			_shortCuts.restore();
			sendPacket(new ShortCutInit(this));
			for(final int shotId : getAutoSoulShot())
				sendPacket(new ExAutoSoulShot(shotId, true));
			sendPacket(new SkillCoolTime(this));

			broadcastPacket(new SocialAction(getObjectId(), SocialAction.LEVEL_UP));

			getDeathPenalty().restore();
			setIncreasedForce(0);

			if(isGM() && Config.SAVE_GM_EFFECTS && getPlayerAccess().UseGMComand)
			{
				// silence
				if(getVarB("gm_silence"))
				{
					setMessageRefusal(true);
					sendPacket(Msg.MESSAGE_REFUSAL_MODE);
				}
				// invul
				if(getVarB("gm_invul"))
				{
					setInvul(true);
					startAbnormalEffect(AbnormalEffect.S_INVULNERABLE);
					sendMessage("You enter in invulnerable mode.");
				}
				// gmspeed
				try
				{
					final int var_gmspeed = Integer.parseInt(getVar("gm_gmspeed"));
					if(var_gmspeed >= 1 && var_gmspeed <= 4)
						SkillTable.getInstance().getInfo(7029, var_gmspeed).getEffects(this, this, false, false);
				}
				catch(final Exception E)
				{}
			}

			broadcastCharInfo();
			updateEffectIcons();
			updateStats();
		}
		finally
		{
			_subclassLock.unlock();
		}
	}

	public boolean isLocked()
	{
		return _subclassLock.isLocked();
	}

	/**
	 * Стартуте таск для кика игрока
	 * 
	 * @param time
	 *            - время через которое должен запуститься таск <font color=red>(в миллисикундах)</font>.
	 * @param interval
	 *            TODO
	 */
	public void startKickTask(final long time, final boolean interval)
	{
		PlayerTaskManager.getInstance().addPlayerTask(PlayerTaskType.KICK, this, time, interval);
	}

	/**
	 * Стартуте таск для окончания ПА
	 * 
	 * @param time
	 *            - время через которое должен запуститься таск <font color=red>(в миллисикундах)</font>.
	 */
	public void startBonusTask(final long time)
	{
		PlayerTaskManager.getInstance().addPlayerTask(PlayerTaskType.BONUS, this, time, false);
	}

	public int getInventoryLimit()
	{
		return (int) calcStat(Stats.INVENTORY_LIMIT, 0, null, null);
	}

	public int getWarehouseLimit()
	{
		return (int) calcStat(Stats.STORAGE_LIMIT, 0, null, null);
	}

	public int getTradeLimit()
	{
		return (int) calcStat(Stats.TRADE_LIMIT, 0, null, null);
	}

	public int getDwarvenRecipeLimit()
	{
		return (int) calcStat(Stats.DWARVEN_RECIPE_LIMIT, 50, null, null) + Config.ALT_ADD_RECIPES;
	}

	public int getCommonRecipeLimit()
	{
		return (int) calcStat(Stats.COMMON_RECIPE_LIMIT, 50, null, null) + Config.ALT_ADD_RECIPES;
	}

	@Override
	public int getNpcId()
	{
		return -2;
	}

	public L2Object getVisibleObject(final int id)
	{
		if(getObjectId() == id)
			return this;

		if(getTargetId() == id)
			return getTarget();

		if(_party != null)
			for(final L2Player p : _party.getPartyMembers())
				if(p != null && p.getObjectId() == id)
					return p;

		L2Object obj = L2World.getAroundObjectById(this, id);

		// Руль кланового летающего корабля
		if(obj == null && isInVehicle() && getVehicle().isClanAirShip() && ClanTable.getInstance().getClan(id) != null)
			obj = ((L2AirShip) getVehicle()).getControlKey();

		return obj == null || obj.isInvisible() ? null : obj;
	}

	@Override
	public int getCriticalHit(final L2Character target, final L2Skill skill)
	{
		int val = (int) calcStat(Stats.CRITICAL_BASE, _template.baseCritRate, target, skill);
		if(Config.LIM_PCRIT > 0 && val > Config.LIM_PCRIT)
			val = Config.LIM_PCRIT;
		return val;
	}

	@Override
	public int getPAtk(final L2Character target)
	{
		final double init = getActiveWeaponInstance() == null ? isMageClass() ? 3 : 4 : 0;
		int val = (int) calcStat(Stats.POWER_ATTACK, init, target, null);
		if(Config.LIM_PATK > 0 && val > Config.LIM_PATK)
			val = Config.LIM_PATK;
		return val;
	}

	@Override
	public int getPDef(final L2Character target)
	{
		double init = 4; // empty cloak and underwear slots

		final L2ItemInstance chest = _inventory.getPaperdollItem(Inventory.PAPERDOLL_CHEST);
		if(chest == null)
			init += isMageClass() ? L2Armor.EMPTY_BODY_MYSTIC : L2Armor.EMPTY_BODY_FIGHTER;
		if(_inventory.getPaperdollItem(Inventory.PAPERDOLL_LEGS) == null && (chest == null || chest.getBodyPart() != L2Item.SLOT_FULL_ARMOR))
			init += isMageClass() ? L2Armor.EMPTY_LEGS_MYSTIC : L2Armor.EMPTY_LEGS_FIGHTER;

		if(_inventory.getPaperdollItem(Inventory.PAPERDOLL_HEAD) == null)
			init += L2Armor.EMPTY_HELMET;
		if(_inventory.getPaperdollItem(Inventory.PAPERDOLL_GLOVES) == null)
			init += L2Armor.EMPTY_GLOVES;
		if(_inventory.getPaperdollItem(Inventory.PAPERDOLL_FEET) == null)
			init += L2Armor.EMPTY_BOOTS;

		int val = (int) calcStat(Stats.POWER_DEFENCE, init, target, null);
		if(Config.LIM_PDEF > 0 && val > Config.LIM_PDEF)
			val = Config.LIM_PDEF;
		return val;
	}

	@Override
	public int getMDef(final L2Character target, final L2Skill skill)
	{
		double init = 0;

		if(_inventory.getPaperdollItem(Inventory.PAPERDOLL_LEAR) == null)
			init += L2Armor.EMPTY_EARRING;
		if(_inventory.getPaperdollItem(Inventory.PAPERDOLL_REAR) == null)
			init += L2Armor.EMPTY_EARRING;
		if(_inventory.getPaperdollItem(Inventory.PAPERDOLL_NECK) == null)
			init += L2Armor.EMPTY_NECKLACE;
		if(_inventory.getPaperdollItem(Inventory.PAPERDOLL_LFINGER) == null)
			init += L2Armor.EMPTY_RING;
		if(_inventory.getPaperdollItem(Inventory.PAPERDOLL_RFINGER) == null)
			init += L2Armor.EMPTY_RING;

		int val = (int) calcStat(Stats.MAGIC_DEFENCE, init, null, skill);
		if(Config.LIM_MDEF > 0 && val > Config.LIM_MDEF)
			val = Config.LIM_MDEF;
		return val;
	}

	public boolean isSubClassActive()
	{
		return getBaseClassId() != getActiveClassId();
	}

	public Forum getMemo()
	{
		if(_forumMemo == null)
		{
			if(ForumsBBSManager.getInstance().getForumByName("MemoRoot") == null)
				return null;
			if(ForumsBBSManager.getInstance().getForumByName("MemoRoot").GetChildByName(_accountName) == null)
				ForumsBBSManager.getInstance().CreateNewForum(_accountName, ForumsBBSManager.getInstance().getForumByName("MemoRoot"), Forum.MEMO, Forum.OWNERONLY, getObjectId());
			setMemo(ForumsBBSManager.getInstance().getForumByName("MemoRoot").GetChildByName(_accountName));
		}
		return _forumMemo;
	}

	/**
	 * @param forum
	 */
	public void setMemo(final Forum forum)
	{
		_forumMemo = forum;
	}

	@Override
	public boolean isCursedWeaponEquipped()
	{
		return _cursedWeaponEquippedId != 0;
	}

	public void setCursedWeaponEquippedId(final int value)
	{
		_cursedWeaponEquippedId = value;
	}

	public int getCursedWeaponEquippedId()
	{
		return _cursedWeaponEquippedId;
	}

	private FishData _fish;

	public void setFish(final FishData fish)
	{
		_fish = fish;
	}

	public void stopLookingForFishTask()
	{
		if(_taskforfish != null)
		{
			_taskforfish.cancel(false);
			_taskforfish = null;
		}
	}

	public void startLookingForFishTask()
	{
		if(!isDead() && _taskforfish == null)
		{
			int checkDelay = 0;
			boolean isNoob = false;
			boolean isUpperGrade = false;

			if(_lure != null)
			{
				final int lureid = _lure.getItemId();
				isNoob = _fish.getGroup() == 0;
				isUpperGrade = _fish.getGroup() == 2;
				if(lureid == 6519 || lureid == 6522 || lureid == 6525 || lureid == 8505 || lureid == 8508 || lureid == 8511) // low grade
					checkDelay = Math.round((float) (_fish.getGutsCheckTime() * 1.33));
				else if(lureid == 6520 || lureid == 6523 || lureid == 6526 || lureid >= 8505 && lureid <= 8513 || lureid >= 7610 && lureid <= 7613 || lureid >= 7807 && lureid <= 7809 || lureid >= 8484 && lureid <= 8486) // medium grade, beginner,
					// prize-winning & quest
					// special bait
					checkDelay = Math.round((float) (_fish.getGutsCheckTime() * 1.00));
				else if(lureid == 6521 || lureid == 6524 || lureid == 6527 || lureid == 8507 || lureid == 8510 || lureid == 8513) // high grade
					checkDelay = Math.round((float) (_fish.getGutsCheckTime() * 0.66));
			}
			_taskforfish = L2GameThreadPools.getInstance().scheduleAiAtFixedRate(new LookingForFishTask(this, _fish.getWaitTime(), _fish.getFishGuts(), _fish.getType(), isNoob, isUpperGrade), 10000, checkDelay, false);
		}
	}

	public void startFishCombat(final boolean isNoob, final boolean isUpperGrade)
	{
		_fishCombat = new L2Fishing(this, _fish, isNoob, isUpperGrade);
	}

	public void endFishing(final boolean win)
	{
		final ExFishingEnd efe = new ExFishingEnd(win, this);
		broadcastPacket(efe);
		_fishing = false;
		_fishLoc = new Location(0, 0, 0);
		broadcastUserInfo(true);
		if(_fishCombat == null)
			sendPacket(Msg.BAITS_HAVE_BEEN_LOST_BECAUSE_THE_FISH_GOT_AWAY);
		_fishCombat = null;
		_lure = null;
		// Ends fishing
		sendPacket(Msg.ENDS_FISHING);
		setImmobilized(false);
		stopLookingForFishTask();
	}

	public L2Fishing getFishCombat()
	{
		return _fishCombat;
	}

	public void setFishLoc(final Location loc)
	{
		_fishLoc = loc;
	}

	public Location getFishLoc()
	{
		return _fishLoc;
	}

	public void setLure(final L2ItemInstance lure)
	{
		_lure = lure;
	}

	public L2ItemInstance getLure()
	{
		return _lure;
	}

	public boolean isFishing()
	{
		return _fishing;
	}

	public void setFishing(final boolean fishing)
	{
		_fishing = fishing;
	}

	public Bonus getBonus()
	{
		return _bonus;
	}

	public void restoreBonus()
	{
		_bonus = new Bonus(this);
	}

	@Override
	public double getRateAdena()
	{
		return _party == null ? _bonus.RATE_DROP_ADENA : _party._rateAdena;
	}

	@Override
	public double getRateItems()
	{
		return _party == null ? _bonus.RATE_DROP_ITEMS : _party._rateDrop;
	}

	@Override
	public double getRateSpoil()
	{
		return _party == null ? _bonus.RATE_DROP_SPOIL : _party._rateSpoil;
	}

	/**
	 * Возвращает рейты на EXP для игрока<BR>
	 * <b><font color=red>если в пати</font> - вернёт рейты пати (среднеарифметическое из рейтов бонусов членов пати помноженного на Config.RATE_XP_PARTY)<BR>
	 * <font color=red>если один</font> - вернёт рейты (статические или динамические помноженные на рейты бонуса)</b>
	 */
	@Override
	public double getRateExp()
	{
		final double multiplier = calcStat(Stats.EXP, 1, null, null);
		return (_party == null ? _bonus.RATE_XP * _rateEXP : _party._rateExp) * multiplier;
	}

	/**
	 * Возвращает рейты на SP для игрока<BR>
	 * <b><font color=red>если в пати</font> - вернёт рейты пати (среднеарифметическое из рейтов бонусов членов пати помноженного на Config.RATE_SP_PARTY)<BR>
	 * <font color=red>если один</font> - вернёт рейты (статические или динамические помноженные на рейты бонуса)</b>
	 */
	@Override
	public double getRateSp()
	{
		final double multiplier = calcStat(Stats.SP, 1, null, null);
		return (_party == null ? _bonus.RATE_SP * _rateSP : _party._rateSp) * multiplier;
	}

	private boolean _maried = false;
	private int _partnerId = 0;
	private int _coupleId = 0;
	private boolean _engagerequest = false;
	private int _engageid = 0;
	private boolean _maryrequest = false;
	private boolean _maryaccepted = false;
	private boolean _IsWearingFormalWear = false;

	public boolean isWearingFormalWear()
	{
		return _IsWearingFormalWear;
	}

	public void setIsWearingFormalWear(final boolean value)
	{
		_IsWearingFormalWear = value;
	}

	public boolean isMaried()
	{
		return _maried;
	}

	public void setMaried(final boolean state)
	{
		_maried = state;
	}

	public boolean isEngageRequest()
	{
		return _engagerequest;
	}

	public void setEngageRequest(final boolean state, final int playerid)
	{
		_engagerequest = state;
		_engageid = playerid;
	}

	public void setMaryRequest(final boolean state)
	{
		_maryrequest = state;
	}

	public boolean isMaryRequest()
	{
		return _maryrequest;
	}

	public void setMaryAccepted(final boolean state)
	{
		_maryaccepted = state;
	}

	public boolean isMaryAccepted()
	{
		return _maryaccepted;
	}

	public int getEngageId()
	{
		return _engageid;
	}

	public int getPartnerId()
	{
		return _partnerId;
	}

	/**
	 * Return the Fame of this L2Plater <BR>
	 * <BR>
	 * 
	 * @return
	 */
	public int getFame()
	{
		return _fame;
	}

	/**
	 * Set the Fame of this L2Player <BR>
	 * <BR>
	 * 
	 * @param fame
	 */
	public void setFame(int fame)
	{
		if(fame > Config.MAX_PERSONAL_FAME_POINTS)
			fame = Config.MAX_PERSONAL_FAME_POINTS;
		if(fame > _fame)
			sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_ACQUIRED_S1_REPUTATION_SCORE).addNumber(fame - _fame));
		_fame = fame;
		sendChanges();
	}

	/**
	 * Добавляет очки репутации игроку <BR>
	 * <BR>
	 * 
	 * @param fame
	 */
	public void addFame(final int fame)
	{
		L2Player player = (L2Player) this;
		sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_ACQUIRED_S1_REPUTATION_SCORE).addNumber(fame));
		_fame = SafeMath.safeAddInt(_fame, fame);
		player.broadcastUserInfo(true);
		if(_fame > Config.MAX_PERSONAL_FAME_POINTS)
			_fame = Config.MAX_PERSONAL_FAME_POINTS;
	}

	public void setAutoLoot(final boolean enable)
	{
		if(Config.COMMAND_ALLOW_AUTOLOOT_COMMAND)
		{
			autoLoot = enable;
			setVar("@autoloot", String.valueOf(enable));
		}
	}

	public boolean isAutoLootEnable()
	{
		return autoLoot;
	}

	public void setPartnerId(final int partnerid)
	{
		_partnerId = partnerid;
	}

	public int getCoupleId()
	{
		return _coupleId;
	}

	public void setCoupleId(final int coupleId)
	{
		_coupleId = coupleId;
	}

	public void engageAnswer(final int answer)
	{
		if(!_engagerequest || _engageid == 0)
			return;

		final L2Player ptarget = L2ObjectsStorage.getPlayer(_engageid);
		setEngageRequest(false, 0);
		if(ptarget != null)
			if(answer == 1)
			{
				CoupleManager.getInstance().createCouple(ptarget, this);
				ptarget.sendMessage(new CustomMessage("l2n.game.model.L2Player.EngageAnswerYes", this));
			}
			else
				ptarget.sendMessage(new CustomMessage("l2n.game.model.L2Player.EngageAnswerNo", this));
	}

	@Override
	public PlayerStatsChangeRecorder getStatsRecorder()
	{
		if(_statsRecorder == null)
			synchronized (this)
			{
				if(_statsRecorder == null)
					_statsRecorder = new PlayerStatsChangeRecorder(this);
			}
		return (PlayerStatsChangeRecorder) _statsRecorder;
	}

	/** те кто прослушивает этого игрока */
	private final TLongArrayList _snoopListener = new TLongArrayList(0);
	/** те кого прослушивает этот игрок */
	private final TLongArrayList _snoopedPlayer = new TLongArrayList(0);

	@Deprecated
	public void broadcastSnoop(final int type, final String name, final String text)
	{
		if(_snoopListener.size() > 0)
		{
			final Snoop sn = new Snoop(getObjectId(), getName(), type, name, text);
			L2Player player;
			for(final TLongIterator iter = _snoopListener.iterator(); iter.hasNext();)
			{
				player = L2ObjectsStorage.getAsPlayer(iter.next());
				if(player != null)
					player.sendPacket(sn);
			}
		}
	}

	public void addSnooper(final long storedId)
	{
		if(!_snoopListener.contains(storedId))
			_snoopListener.add(storedId);
	}

	public void removeSnooper(final long storedId)
	{
		_snoopListener.remove(storedId);
	}

	public void addSnooped(final long storedId)
	{
		if(!_snoopedPlayer.contains(storedId))
			_snoopedPlayer.add(storedId);
	}

	public void removeSnooped(final long storedId)
	{
		_snoopedPlayer.remove(storedId);
	}

	/**
	 * Сброс реюза всех скилов персонажа.
	 */
	public void resetReuse()
	{
		if(_skillReuses != null)
			_skillReuses.clear();
	}

	@Override
	public void disableSkill(final int skill_hash, final SkillTimeStamp ts)
	{
		if(ts.getReuseBasic() < 10 || isGM() && getPlayerAccess().DisableReuseSkill)
			return;
		super.disableSkill(skill_hash, ts);
	}

	/**
	 * Send a reuse message to the player.<BR>
	 * 
	 * @param skill
	 *            - скил для отправки сообщения о времени отката
	 */
	@Override
	public void sendReuseMessage(final L2Skill skill)
	{
		if(isCastingNow())
			return;

		// Check if this skill is enabled (ex : reuse time)
		final SkillTimeStamp sts = getSkillReuse(skill);
		if(sts == null || !sts.hasNotPassed())
		{
			sendPacket(new SystemMessage(SystemMessage.S1_IS_NOT_AVAILABLE_AT_THIS_TIME_BEING_PREPARED_FOR_REUSE).addSkillName(skill.getId(), skill.getDisplayLevel()));
			return;
		}

		final float remainingTime = sts.getReuseCurrent() / 1000;
		if(!Config.ALT_SHOW_REUSE_MSG && remainingTime < 10 || remainingTime < 0.5)
			return;

		final int hours = (int) (remainingTime / 3600);
		final int minutes = (int) (remainingTime % 3600 / 60);
		final int seconds = (int) (remainingTime % 60);

		if(hours > 0)
			sendPacket(new SystemMessage(SystemMessage.THERE_ARE_S2_HOURS_S3_MINUTES_AND_S4_SECONDS_REMAINING_IN_S1S_REUSE_TIME).addSkillName(skill.getId(), skill.getDisplayLevel()).addNumber(hours).addNumber(minutes).addNumber(seconds));
		else if(minutes > 0)
			sendPacket(new SystemMessage(SystemMessage.THERE_ARE_S2_MINUTES_S3_SECONDS_REMAINING_IN_S1S_REUSE_TIME).addSkillName(skill.getId(), skill.getDisplayLevel()).addNumber(minutes).addNumber(seconds));
		else
			sendPacket(new SystemMessage(SystemMessage.THERE_ARE_S2_SECONDS_REMAINING_IN_S1S_REUSE_TIME).addSkillName(skill.getId(), skill.getDisplayLevel()).addNumber(seconds));
	}

	public ScheduledFuture<WaterTask> getWaterTask()
	{
		return _taskWater;
	}

	public DeathPenalty getDeathPenalty()
	{
		if(getActiveClass() != null)
			return getActiveClass().getDeathPenalty();
		return null;
	}

	public void setDeathPeanalty(final DeathPenalty dp)
	{
		if(_classlist.get(getActiveClassId()) != null)
			_classlist.get(getActiveClassId()).setDeathPenalty(dp);
	}

	// fast fix for dice spam
	public long lastDiceThrown = 0;

	private boolean _charmOfCourage = false;

	public boolean isCharmOfCourage()
	{
		return _charmOfCourage;
	}

	public void setCharmOfCourage(final boolean val)
	{
		_charmOfCourage = val;

		if(!val)
			getEffectList().stopEffect(L2Skill.SKILL_CHARM_OF_COURAGE);
		sendPacket(new EtcStatusUpdate(this));
	}

	private void revalidatePenalties()
	{
		_curWeightPenalty = 0;
		weaponExpertisePenalty = 0;
		armorExpertisePenalty = 0;
		refreshOverloaded();
		refreshExpertisePenalty();
	}

	private int _increasedForce = 0;
	private int _consumedSouls = 0;

	@Override
	public int getIncreasedForce()
	{
		return _increasedForce;
	}

	@Override
	public int getConsumedSouls()
	{
		return _consumedSouls;
	}

	@Override
	public void setConsumedSouls(int i, final L2NpcInstance monster)
	{
		if(i == _consumedSouls)
			return;

		final int max = (int) calcStat(Stats.SOULS_LIMIT, 0, monster, null);

		if(i > max)
			i = max;

		if(i <= 0)
		{
			_consumedSouls = 0;
			sendPacket(new EtcStatusUpdate(this));
			return;
		}

		if(_consumedSouls != i)
		{
			final int diff = i - _consumedSouls;
			if(diff > 0)
			{
				final SystemMessage sm = new SystemMessage(SystemMessage.YOUR_SOUL_HAS_INCREASED_BY_S1_SO_IT_IS_NOW_AT_S2);
				sm.addNumber(diff);
				sm.addNumber(i);
				sendPacket(sm);
			}
		}
		else if(max == i)
		{
			sendPacket(Msg.SOUL_CANNOT_BE_ABSORBED_ANY_MORE);
			return;
		}

		_consumedSouls = i;
		sendPacket(new EtcStatusUpdate(this));
	}

	@Override
	public void setIncreasedForce(int i)
	{
		i = Math.min(i, 8);
		i = Math.max(i, 0);

		if(i != 0 && i > _increasedForce)
			sendPacket(new SystemMessage(SystemMessage.YOUR_FORCE_HAS_INCREASED_TO_S1_LEVEL).addNumber(i));

		_increasedForce = i;
		sendPacket(new EtcStatusUpdate(this));
	}

	private long _lastFalling;

	public boolean isFalling()
	{
		return System.currentTimeMillis() - _lastFalling < 5000;
	}

	public void falling(final int height)
	{
		if(isDead() || isFlying() || isSwimming() || isInVehicle() || !Config.DAMAGE_FROM_FALLING)
			return;

		_lastFalling = System.currentTimeMillis();

		final int maxHp = getMaxHp();
		final int curHp = (int) getCurrentHp();
		final int damage = (int) calcStat(Stats.FALL, maxHp / 1000 * height, null, null);
		if(curHp - damage < 1)
			setCurrentHp(1, false);
		else
			setCurrentHp(curHp - damage, false);
		sendPacket(new SystemMessage(SystemMessage.YOU_RECEIVED_S1_DAMAGE_FROM_TAKING_A_HIGH_FALL).addNumber(damage));
	}

	// сюда пасивные скиллы
	private static final byte[] HP_MESSAGES_HP = { 30, 30 };
	private static final int[] HP_MESSAGES_SKILLS = { 290, 291 };

	// сюда активные эффекты
	private static final int[] HP_MESSAGES_EFFECTS_SKILLS_ID = { 139, 176, 292, 292, 420 };
	private static final byte[] HP_MESSAGES_EFFECTS_HP = { 30, 30, 30, 60, 30 };

	/**
	 * Системные сообщения о текущем состоянии хп
	 */
	@Override
	public void checkHpMessages(final double curHp, final double newHp)
	{
		final double percent = getMaxHp() / 100;
		final double _curHpPercent = curHp / percent;
		final double _newHpPercent = newHp / percent;
		boolean needsUpdate = false;

		// check for passive skills
		for(int i = 0; i < HP_MESSAGES_SKILLS.length; i++)
		{
			final int level = getSkillLevel(HP_MESSAGES_SKILLS[i]);
			if(level > 0)
				if(_curHpPercent > HP_MESSAGES_HP[i] && _newHpPercent <= HP_MESSAGES_HP[i])
				{
					sendPacket(new SystemMessage(SystemMessage.SINCE_HP_HAS_DECREASED_THE_EFFECT_OF_S1_CAN_BE_FELT).addSkillName(HP_MESSAGES_SKILLS[i], level));
					needsUpdate = true;
				}
				else if(_curHpPercent <= HP_MESSAGES_HP[i] && _newHpPercent > HP_MESSAGES_HP[i])
				{
					sendPacket(new SystemMessage(SystemMessage.SINCE_HP_HAS_INCREASED_THE_EFFECT_OF_S1_WILL_DISAPPEAR).addSkillName(HP_MESSAGES_SKILLS[i], level));
					needsUpdate = true;
				}
		}

		// check for active effects
		for(int i = 0; i < HP_MESSAGES_EFFECTS_SKILLS_ID.length; i++)
			if(getEffectList().getFirstEffect(HP_MESSAGES_EFFECTS_SKILLS_ID[i]) != null)
				if(_curHpPercent > HP_MESSAGES_EFFECTS_HP[i] && _newHpPercent <= HP_MESSAGES_EFFECTS_HP[i])
				{
					sendPacket(new SystemMessage(SystemMessage.SINCE_HP_HAS_DECREASED_THE_EFFECT_OF_S1_CAN_BE_FELT).addSkillName(HP_MESSAGES_EFFECTS_SKILLS_ID[i], (short) 1));
					needsUpdate = true;
				}
				else if(_curHpPercent <= HP_MESSAGES_EFFECTS_HP[i] && _newHpPercent > HP_MESSAGES_EFFECTS_HP[i])
				{
					sendPacket(new SystemMessage(SystemMessage.SINCE_HP_HAS_INCREASED_THE_EFFECT_OF_S1_WILL_DISAPPEAR).addSkillName(HP_MESSAGES_EFFECTS_SKILLS_ID[i], (short) 1));
					needsUpdate = true;
				}

		if(needsUpdate)
			sendChanges();
	}

	/**
	 * Системные сообщения для темных эльфов о вкл/выкл ShadowSence (skill id = 294)
	 */
	public void checkDayNightMessages()
	{
		final int level = getSkillLevel(294);
		if(level > 0)
			if(GameTimeController.getInstance().isNowNight())
				sendPacket(new SystemMessage(SystemMessage.IT_IS_NOW_MIDNIGHT_AND_THE_EFFECT_OF_S1_CAN_BE_FELT).addSkillName(294, level));
			else
				sendPacket(new SystemMessage(SystemMessage.IT_IS_DAWN_AND_THE_EFFECT_OF_S1_WILL_NOW_DISAPPEAR).addSkillName(294, level));
		sendChanges();
	}

	private boolean _isInDangerArea;

	public boolean isInDangerArea()
	{
		return _isInDangerArea;
	}

	public void setInDangerArea(final boolean value)
	{
		_isInDangerArea = value;
	}

	public void setInCombatZone(final boolean flag)
	{
		_isInCombatZone = flag;
	}

	public void setOnSiegeField(final boolean flag)
	{
		_isOnSiegeField = flag;
	}

	public boolean isInPeaceZone()
	{
		return _isInPeaceZone;
	}

	public void setInPeaceZone(final boolean b)
	{
		_isInPeaceZone = b;
		if(b)
			VitalityManager.getInstance().addVitalityTask(this);
	}

	public boolean isInSSZone()
	{
		return _isInSSZone;
	}

	public void setInSSZone(final boolean b)
	{
		_isInSSZone = b;
	}

	public boolean isInCombatZone()
	{
		return _isInCombatZone;
	}

	public boolean isOnSiegeField()
	{
		return _isOnSiegeField;
	}

	@Override
	public void doZoneCheck(final int messageNumber)
	{
		final boolean oldIsInDangerArea = isInDangerArea();
		final boolean oldIsInCombatZone = isInCombatZone();
		final boolean oldIsOnSiegeField = isOnSiegeField();
		final boolean oldIsInPeaceZone = isInPeaceZone();
		final boolean oldSSQZone = isInSSZone();

		setInDangerArea(isInZoneDanger());
		setInCombatZone(isInZoneBattle());
		setOnSiegeField(isInZone(Siege));
		setInPeaceZone(isInZonePeace());
		setInSSZone(isInZone(ssq_zone));

		if(oldIsInDangerArea != isInDangerArea() || oldIsInCombatZone != isInCombatZone() || oldIsOnSiegeField != isOnSiegeField() || oldIsInPeaceZone != isInPeaceZone() || oldSSQZone != isInSSZone())
		{
			sendPacket(new ExSetCompassZoneCode(this));
			if(messageNumber != 0)
				sendPacket(new SystemMessage(messageNumber));
		}

		if(oldIsInDangerArea != isInDangerArea())
			sendPacket(new EtcStatusUpdate(this));

		if(oldIsOnSiegeField != isOnSiegeField())
		{
			broadcastRelationChanged();
			if(isOnSiegeField())
				sendPacket(Msg.YOU_HAVE_ENTERED_A_COMBAT_ZONE);
			else
			{
				sendPacket(Msg.YOU_HAVE_LEFT_A_COMBAT_ZONE);
				if(!isTeleporting() && getPvpFlag() == 0)
					startPvPFlag(null);
			}
		}

		revalidateInResidence();
	}

	public void checkTerritoryFlag()
	{
		if(isTerritoryFlagEquipped() && getActiveWeaponInstance().getCustomType1() != 77) // 77 это эвентовый флаг
		{
			final L2Zone siegeZone = ZoneManager.getInstance().getZoneByType(ZoneType.Siege, getX(), getY(), true);
			if(siegeZone == null && (_returnTerritoryFlagTask == null || _returnTerritoryFlagTask.isDone()))
			{
				_returnTerritoryFlagTask = L2GameThreadPools.getInstance().scheduleGeneral(new ReturnTerritoryFlagTask(this), Config.TERRETORY_ALT_TIME_FLAG);
				sendMessage("Вам нужно, вернуться в осадную зону, иначе флаг вернется в замок. Вы можете использовать форты как помежуточные точки, для сброса таймера.");
			}

			if(siegeZone != null && _returnTerritoryFlagTask != null)
			{
				_returnTerritoryFlagTask.cancel(true);
				_returnTerritoryFlagTask = null;
			}
		}
	}

	public void revalidateInResidence()
	{
		final L2Clan clan = _clan;
		if(clan == null)
			return;

		final int clanHallIndex = clan.getHasHideout();
		if(clanHallIndex != 0)
		{
			final ClanHall clansHall = ClanHallManager.getInstance().getClanHall(clanHallIndex);
			if(clansHall != null && clansHall.checkIfInZone(getX(), getY()))
			{
				setInResidence(ResidenceType.Clanhall);
				return;
			}
		}

		final int castleIndex = clan.getHasCastle();
		if(castleIndex != 0)
		{
			final Castle castle = CastleManager.getInstance().getCastleByIndex(castleIndex);
			if(castle != null && castle.checkIfInZone(getX(), getY()))
			{
				setInResidence(ResidenceType.Castle);
				return;
			}
		}

		final int fortressIndex = clan.getHasFortress();
		if(fortressIndex != 0)
		{
			final Fortress fort = FortressManager.getInstance().getFortressByIndex(fortressIndex);
			if(fort != null && fort.checkIfInZone(getX(), getY()))
			{
				setInResidence(ResidenceType.Fortress);
				return;
			}
		}
		setInResidence(ResidenceType.None);
	}

	@Override
	public boolean isInWater()
	{
		return getWaterTask() != null;
	}

	public ResidenceType getInResidence()
	{
		return _inResidence;
	}

	public void setInResidence(final ResidenceType inResidence)
	{
		_inResidence = inResidence;
	}

	public void startAutoSaveTask()
	{
		if(!Config.AUTOSAVE)
			return;
		if(_autoSaveTask == null)
			_autoSaveTask = AutoSaveManager.getInstance().addPlayerTask(this);
	}

	public void stopAutoSaveTask()
	{
		if(_autoSaveTask != null)
			_autoSaveTask.cancel(false);
		_autoSaveTask = null;
	}

	// TODO переделать основное на @see l2n.game.network.CustomSystemMessageId
	@Override
	public void sendMessage(final String message)
	{
		sendPacket(SystemMessage.sendString(message));
	}

	private Location _lastClientPosition;
	private Location _lastServerPosition;

	@Override
	public void setLastClientPosition(final Location position)
	{
		_lastClientPosition = position;
	}

	public Location getLastClientPosition()
	{
		return _lastClientPosition;
	}

	@Override
	public void setLastServerPosition(final Location position)
	{
		_lastServerPosition = position;
	}

	public Location getLastServerPosition()
	{
		return _lastServerPosition;
	}

	private int _useSeed = 0;

	public void setUseSeed(final int id)
	{
		_useSeed = id;
	}

	public int getUseSeed()
	{
		return _useSeed;
	}

	public int getRelation(final L2Player target)
	{
		int result = 0;

		if(getClan() != null)
		{
			result |= RELATION_CLAN_MEMBER;
			if(getClan() == target.getClan())
				result |= RELATION_CLAN_MATE;
			if(getAllyId() != 0)
				result |= RELATION_ALLY_MEMBER;
		}
		if(isClanLeader())
			result |= RELATION_LEADER;

		final L2Party party = getParty();
		if(party != null && party == target.getParty())
		{
			result |= RELATION_HAS_PARTY;

			switch (party.indexOfPartyMember(this))
			{
				case 0:
					result |= RELATION_PARTYLEADER; // 0x10
					break;
				case 1:
					result |= RELATION_PARTY4; // 0x8
					break;
				case 2:
					result |= RELATION_PARTY3 + RELATION_PARTY2 + RELATION_PARTY1; // 0x7
					break;
				case 3:
					result |= RELATION_PARTY3 + RELATION_PARTY2; // 0x6
					break;
				case 4:
					result |= RELATION_PARTY3 + RELATION_PARTY1; // 0x5
					break;
				case 5:
					result |= RELATION_PARTY3; // 0x4
					break;
				case 6:
					result |= RELATION_PARTY2 + RELATION_PARTY1; // 0x3
					break;
				case 7:
					result |= RELATION_PARTY2; // 0x2
					break;
				case 8:
					result |= RELATION_PARTY1; // 0x1
					break;
			}
		}

		final L2Clan clan1 = getClan();
		final L2Clan clan2 = target.getClan();
		if(clan1 != null && clan2 != null)
		{
			final Siege siege1 = clan1.getSiege();
			final Siege siege2 = clan2.getSiege();

			final int state1 = getSiegeState();
			final int state2 = target.getSiegeState();

			if(siege1 != null && siege2 != null && siege1 == siege2 && siege1.isInProgress() && state1 != 0 && state2 != 0)
			{
				result |= RELATION_INSIEGE;
				if(state1 != state2 || siege1.isMidVictory() && state1 == 1 && state2 == 1)
					result |= RELATION_ENEMY;
				else
					result |= RELATION_ALLY;
				if(state1 == 1)
					result |= RELATION_ATTACKER;
			}
			if(target.getPledgeType() != L2Clan.SUBUNIT_ACADEMY && getPledgeType() != L2Clan.SUBUNIT_ACADEMY && clan2.isAtWarWith(clan1.getClanId()))
			{
				result |= RELATION_1SIDED_WAR;
				if(clan1.isAtWarWith(clan2.getClanId()))
					result |= RELATION_MUTUAL_WAR;
			}
		}

		final int territorySiege1 = getTerritorySiege();
		final int territorySiege2 = target.getTerritorySiege();

		if(territorySiege1 > -1 && territorySiege2 > -1)
			result |= RELATION_TERRITORY_WAR;

		if(getHandysBlockCheckerEventArena() != -1)
		{
			result |= RELATION_INSIEGE;
			final ArenaParticipantsHolder holder = HandysBlockCheckerManager.getInstance().getHolder(getHandysBlockCheckerEventArena());
			if(holder.getPlayerTeam(this) == 0)
				result |= RELATION_ENEMY;
			else
				result |= RELATION_ALLY;
			result |= RELATION_ATTACKER;
		}

		return result;
	}

	/** The PvP Flag state of the L2Player (0=White, 1=Purple, 2=PurpleBlink) */
	protected int _pvpFlag;
	private long _lastPvpAttack;

	public void setlastPvpAttack(final long time)
	{
		_lastPvpAttack = time;
	}

	public long getlastPvpAttack()
	{
		return _lastPvpAttack;
	}

	@Override
	public void startPvPFlag(final L2Character target)
	{
		long startTime = System.currentTimeMillis();
		if(target != null && target.getPvpFlag() != 0)
			startTime -= Config.PVP_TIME / 2;
		if(_pvpFlag != 0 && _lastPvpAttack > startTime)
			return;
		_lastPvpAttack = startTime;

		updatePvPFlag(1);

		if(_pvpRegTask == null)
			_pvpRegTask = L2GameThreadPools.getInstance().scheduleAiAtFixedRate(new PvPFlagTask(this), 1000, 1000, true);
	}

	public void stopPvPFlag()
	{
		final Future<?> future = _pvpRegTask;
		if(future != null)
			future.cancel(false);
		_pvpRegTask = null;
		updatePvPFlag(0);
	}

	public void updatePvPFlag(final int value)
	{
		if(_pvpFlag == value)
			return;
		setPvpFlag(value);

		if(_karma < 1)
		{
			sendStatusUpdate(true, StatusUpdate.PVP_FLAG);
			if(getPet() != null)
				getPet().broadcastCharInfo();
		}
		broadcastRelationChanged();
	}

	/**
	 * Set the PvP Flag of the L2Player.<BR>
	 * <BR>
	 * 
	 * @param pvpFlag
	 *            new value of pvpFlag to set
	 */
	public void setPvpFlag(final int pvpFlag)
	{
		_pvpFlag = pvpFlag;
	}

	/**
	 * @return pvpFlag of this L2Player
	 */
	@Override
	public int getPvpFlag()
	{
		return _pvpFlag;
	}

	private Duel _duel;

	public void setDuel(final Duel duel)
	{
		_duel = duel;
		broadcastCharInfo();
	}

	@Override
	public Duel getDuel()
	{
		return _duel;
	}

	private L2TamedBeastInstance _tamedBeast = null;

	/**
	 * @return the L2Summon of the L2Player or null.<BR>
	 * <BR>
	 */
	public L2TamedBeastInstance getTrainedBeast()
	{
		return _tamedBeast;
	}

	/**
	 * Set the L2Summon of the L2Player.<BR>
	 * <BR>
	 */
	public void setTrainedBeast(final L2TamedBeastInstance tamedBeast)
	{
		_tamedBeast = tamedBeast;
	}

	private long _lastAttackPacket;
	private long _lastMovePacket;
	private long _lastItemAuctionInfoPacket;

	public long getLastAttackPacket()
	{
		return _lastAttackPacket;
	}

	public void setLastAttackPacket()
	{
		_lastAttackPacket = System.currentTimeMillis();
	}

	public long getLastMovePacket()
	{
		return _lastMovePacket;
	}

	public void setLastMovePacket()
	{
		_lastMovePacket = System.currentTimeMillis();
	}

	public long getLastItemAuctionPacket()
	{
		return _lastItemAuctionInfoPacket;
	}

	public void setLastItemAuctionPacket()
	{
		_lastItemAuctionInfoPacket = System.currentTimeMillis();
	}

	public byte[] getKeyBindings()
	{
		return _keyBindings;
	}

	public void setKeyBindings(byte[] keyBindings)
	{
		if(keyBindings == null)
			keyBindings = new byte[0];
		_keyBindings = keyBindings;
	}

	/**
	 * Возвращает тип атакующего элемента и его силу.
	 * 
	 * @return массив, в котором: <li>[0]: тип элемента, <li>[1]: его сила
	 */
	public int[] getAttackElement()
	{
		return Formulas.calcAttackElement(this);
	}

	/**
	 * Возвращает силу атаки элемента
	 * 
	 * @return значение атаки
	 */
	public int getAttack(final Element element)
	{
		if(element == Element.NONE)
			return 0;
		return (int) calcStat(element.getAttack(), 0., null, null);
	}

	/**
	 * Возвращает защиту от элемента
	 * 
	 * @return значение защиты
	 */
	public int getDefence(final Element element)
	{
		if(element == Element.NONE)
			return 0;
		return (int) calcStat(element.getDefence(), 0., null, null);
	}

	/**
	 * истанавливает режим трансформаии, новый метод<BR>
	 * 
	 * @param transformationId
	 *            идентификатор трансформации
	 * @param transformation
	 *            трансформация
	 */
	public void setTransformation(final int transformationId, final L2Transformation transformation)
	{
		if(isTransformed())
		{
			// You already polymorphed and cannot polymorph again.
			sendPacket(Msg.YOU_ALREADY_POLYMORPHED_AND_CANNOT_POLYMORPH_AGAIN);
			return;
		}

		// Get off the strider or something else if character is mounted
		if(isMounted())
			setMount(0, 0);
		// Unsummon pets
		if(getPet() != null && !transformation.isDefaultActionListTransform())
			getPet().unSummon();

		if(transformationId == _transformationId || _transformationId != 0 && transformationId != 0)
			return;

		setTransformationId(transformationId);
		_transformation = transformation;

		// Останавливаем все тоггле скилы
		getEffectList().stopEffects(EffectList.PROC_STOP_ALL_TOGGLES);
		transformation.onTransform(this);

		final Collection<L2Skill> skills = getTransformationSkills();
		for(final L2Skill s : skills)
			addSkill(s, false);

		// выслать список действий у трансформированного чара
		// у некоторых трансоформов он ограничен
		if(!transformation.isDefaultActionListTransform())
			sendPacket(ExBasicActionList.TRANSFORMED_ACTION_LIST);

		sendPacket(new SkillList(this));
		broadcastUserInfo(true);
	}

	/**
	 * Остонавливает эффект трансформации <BR>
	 * <B><U> Действия</U> :</B> <BR>
	 * <li>Отменяет эффект трансформации</li><BR>
	 * <li>Notify the L2Character AI</li><BR>
	 * <li>Send Server->Client UserInfo/CharInfo packet</li> <BR>
	 */
	public final void stopTransformation()
	{
		getEffectList().stopEffects(EffectType.Transformation);
		// if this is a player instance, then untransform, also set the transform_id column equal to 0 if not cursed.
		if(getTransform() != null)
			untransform();

		getAI().notifyEvent(CtrlEvent.EVT_THINK, null);
		broadcastUserInfo(true);
	}

	/**
	 * <B><U> Действия</U> :</B><BR>
	 * <li>Удаляет скилы выданные при трансформации</li><BR>
	 * <li>Очищает список скилов трансформации</li><BR>
	 * <li>Востонавливает список доступных действий</li><BR>
	 * <li>Востонавливает список доступных скилов</li><BR>
	 * <li>Send Server->Client UserInfo/CharInfo packet</li> <BR>
	 */
	private void untransform()
	{
		if(isTransformed())
		{
			// restoreSkills();

			setTransformationId(0);
			_transformation.onUntransform(this);
			_transformation = null;

			// удаляем скилы трансформации
			if(!_transformationSkills.isEmpty())
			{
				final Collection<L2Skill> skills = getTransformationSkills();
				for(final L2Skill s : skills)
					if(!s.isCommon() && !SkillTreeTable.getInstance().isSkillPossible(this, s.getId(), s.getLevel()))
						super.removeSkill(s);
				super.removeSkillById(619);
				_transformationSkills.clear();
			}

			sendPacket(ExBasicActionList.DEFAULT_ACTION_LIST, new SkillCoolTime(this), new SkillList(this));
		}
	}

	/**
	 * @return true если в трансформе:<br>
	 *         <li>Final Flying Form (skill 840)</li><br>
	 *         <li>Aura Bird - Falcon (skill 841)</li><br>
	 *         <li>Aura Bird - Owl (skill 842)</li>
	 */
	public boolean isInFlyingTransform()
	{
		return _transformationId == 8 || _transformationId == 9 || _transformationId == 260;
	}

	/**
	 * Возвращает режим трансформации
	 * 
	 * @return ID режима трансформации
	 */
	public int getTransformationId()
	{
		return _transformationId;
	}

	/** истанавливает ID трансформации */
	public void setTransformationId(final int id)
	{
		_transformationId = id;
	}

	/**
	 * Возвращает трансформацию
	 * 
	 * @return L2Transformation трансформации
	 */
	public L2Transformation getTransform()
	{
		return _transformation;
	}

	/**
	 * Возвращает коллекцию скиллов, с учетом текущей трансформации
	 */
	@Override
	public final Collection<L2Skill> getAllSkills()
	{
		// Трансформация неактивна
		if(_transformationId == 0)
			return super.getAllSkills();

		// Трансформация активна
		final TIntObjectHashMap<L2Skill> tempSkills = new TIntObjectHashMap<L2Skill>();
		for(final L2Skill s : super.getAllSkills())
			if(s != null && !s.isActive() && !s.isToggle())
				tempSkills.put(s.getId(), s);
		tempSkills.putAll(_transformationSkills); // Добавляем к пассивкам скилы текущей трансформации
		return tempSkills.valueCollection();
	}

	/**
	 * Добавляет в список скил и талисман
	 * 
	 * @param skill
	 * @param talisman
	 */
	public final void addTalismanSkill(final L2Skill skill, final L2ItemInstance talisman)
	{
		if(_talismanSkills == null)
			_talismanSkills = new TIntLongHashMap(6);
		_talismanSkills.put(skill.getId(), talisman.getStoredId());
	}

	public final void removeTalismanSkill(final L2Skill skill)
	{
		if(_talismanSkills != null)
			_talismanSkills.remove(skill.getId());
	}

	@Override
	public final L2ItemInstance getActiveTalisman(final L2Skill skill)
	{
		if(_talismanSkills != null)
		{
			final long storedId = _talismanSkills.get(skill.getId());
			if(storedId > 0)
				return L2ObjectsStorage.getAsItem(storedId);
		}
		return null;
	}

	public Collection<L2Skill> getTransformationSkills()
	{
		return _transformationSkills.valueCollection();
	}

	public void addTransformationSkill(final L2Skill skill)
	{
		_transformationSkills.put(skill.getId(), skill);
	}

	/**
	 * истанавливает агнишена
	 * 
	 * @param template
	 *            ID шаблона NPC агнишена<BR>
	 */
	public void setAgathion(final int id)
	{
		if(id == 0)
		{
			if(_agathion != null)
				_agathion.deleteMe();
			_agathion = null;
		}
		else
			_agathion = new L2AgathionInstance(this, id);

		broadcastCharInfo();
		sendPacket(new SkillList(this));
	}

	/**
	 * Возвращает агнишена
	 * 
	 * @return L2AgathionInstance
	 */
	public L2AgathionInstance getAgathion()
	{
		return _agathion;
	}

	/**
	 * Возвращает количество PcBangPoint'ов даного игрока
	 * 
	 * @return количество PcCafe Bang Points
	 */
	public int getPcBangPoints()
	{
		return pcBangPoints;
	}

	/**
	 * истанавливает количество Pc Cafe Bang Points для даного игрока
	 * 
	 * @param pcBangPoints
	 *            новое количество PcCafeBangPoints
	 */
	public void setPcBangPoints(final int pcBangPoints)
	{
		this.pcBangPoints = pcBangPoints;
	}

	private Location _groundSkillLoc;

	public void setGroundSkillLoc(final Location location)
	{
		_groundSkillLoc = location;
	}

	public Location getGroundSkillLoc()
	{
		return _groundSkillLoc;
	}

	public boolean isDeleting()
	{
		return _isDeleting;
	}

	public void setOfflineMode(final boolean val)
	{
		if(!val)
			unsetVar("offline");
		_offline = val;
	}

	/**
	 * @return true если игрок находиться в режиме оффлайн торговли
	 */
	public boolean isInOfflineMode()
	{
		return _offline;
	}

	public void saveTradeList()
	{
		String val = "";

		if(_sellList == null || _sellList.isEmpty())
			unsetVar("selllist");
		else
		{
			for(final TradeItem i : _sellList)
				val += i.getObjectId() + ";" + i.getCount() + ";" + i.getOwnersPrice() + ":";
			setVar("selllist", val);
			val = "";
			if(_tradeList != null && _tradeList.getSellStoreName() != null)
				setVar("sellstorename", _tradeList.getSellStoreName());
		}

		if(_buyList == null || _buyList.isEmpty())
			unsetVar("buylist");
		else
		{
			for(final TradeItem i : _buyList)
				val += i.getItemId() + ";" + i.getCount() + ";" + i.getOwnersPrice() + ":";
			setVar("buylist", val);
			val = "";
			if(_tradeList != null && _tradeList.getBuyStoreName() != null)
				setVar("buystorename", _tradeList.getBuyStoreName());
		}

		if(_createList == null || _createList.getList().isEmpty())
			unsetVar("createlist");
		else
		{
			for(final L2ManufactureItem i : _createList.getList())
				val += i.getRecipeId() + ";" + i.getCost() + ":";
			setVar("createlist", val);
			if(_createList.getStoreName() != null)
				setVar("manufacturename", _createList.getStoreName());
		}
	}

	public void restoreTradeList()
	{
		if(getVar("selllist") != null)
		{
			_sellList = new ConcurrentLinkedQueue<TradeItem>();
			final String[] items = getVar("selllist").split(":");
			for(final String item : items)
			{
				if(item.equals(""))
					continue;
				final String[] values = item.split(";");
				if(values.length < 3)
					continue;
				final TradeItem i = new TradeItem();
				final int oId = Integer.parseInt(values[0]);
				long count = Long.parseLong(values[1]);
				final long price = Long.parseLong(values[2]);
				i.setObjectId(oId);

				final L2ItemInstance itemToSell = getInventory().getItemByObjectId(oId);

				if(count < 1 || itemToSell == null)
					continue;

				if(count > itemToSell.getCount())
					count = itemToSell.getCount();

				i.setCount(count);
				i.setOwnersPrice(price);
				i.setItemId(itemToSell.getItemId());
				i.setEnchantLevel(itemToSell.getEnchantLevel());
				i.setCustomType2(itemToSell.getItem().getType2ForPackets());
				i.setAttackElement(itemToSell.getAttackElement());
				i.setDefenceFire(itemToSell.getDefenceFire());
				i.setDefenceWater(itemToSell.getDefenceWater());
				i.setDefenceWind(itemToSell.getDefenceWind());
				i.setDefenceEarth(itemToSell.getDefenceEarth());
				i.setDefenceHoly(itemToSell.getDefenceHoly());
				i.setDefenceUnholy(itemToSell.getDefenceUnholy());

				_sellList.add(i);
			}
			if(_tradeList == null)
				_tradeList = new L2TradeList();
			if(getVar("sellstorename") != null)
				_tradeList.setSellStoreName(getVar("sellstorename"));
		}
		if(getVar("buylist") != null)
		{
			_buyList = new ConcurrentLinkedQueue<TradeItem>();
			final String[] items = getVar("buylist").split(":");
			for(final String item : items)
			{
				if(item.equals(""))
					continue;
				final String[] values = item.split(";");
				if(values.length < 3)
					continue;
				final TradeItem i = new TradeItem();
				i.setItemId(Integer.parseInt(values[0]));
				i.setCount(Long.parseLong(values[1]));
				i.setOwnersPrice(Long.parseLong(values[2]));
				_buyList.add(i);
			}
			if(_tradeList == null)
				_tradeList = new L2TradeList();
			if(getVar("buystorename") != null)
				_tradeList.setBuyStoreName(getVar("buystorename"));
		}
		if(getVar("createlist") != null)
		{
			_createList = new L2ManufactureList();
			final String[] items = getVar("createlist").split(":");
			for(final String item : items)
			{
				if(item.equals(""))
					continue;
				final String[] values = item.split(";");
				if(values.length < 2)
					continue;
				_createList.add(new L2ManufactureItem(Integer.parseInt(values[0]), Long.parseLong(values[1])));
			}
			if(getVar("manufacturename") != null)
				_createList.setStoreName(getVar("manufacturename"));
		}
	}

	public void restoreRecipeBook()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT id FROM character_recipebook WHERE char_id=?");
			statement.setInt(1, getObjectId());
			rset = statement.executeQuery();

			while (rset.next())
			{
				final int id = rset.getInt("id");
				final L2RecipeList recipe = RecipeController.getInstance().getRecipeList(id);
				registerRecipe(recipe, false);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Count not recipe skills:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public L2DecoyInstance getDecoy()
	{
		return _decoy;
	}

	public void setDecoy(final L2DecoyInstance decoy)
	{
		_decoy = decoy;
	}

	/**
	 * Return the type of Pet mounted (0 : none, 1 : Strider, 2 : Wyvern, 3: Wolf).<BR>
	 */
	public int getMountType()
	{
		switch (getMountNpcId())
		{
			case PetDataTable.STRIDER_WIND_ID:
			case PetDataTable.STRIDER_STAR_ID:
			case PetDataTable.STRIDER_TWILIGHT_ID:
			case PetDataTable.RED_STRIDER_WIND_ID:
			case PetDataTable.RED_STRIDER_STAR_ID:
			case PetDataTable.RED_STRIDER_TWILIGHT_ID:
			case PetDataTable.GUARDIANS_STRIDER_ID:
				return 1;
			case PetDataTable.WYVERN_ID:
				return 2;
			case PetDataTable.WGREAT_WOLF_ID:
			case PetDataTable.FENRIR_WOLF_ID:
			case PetDataTable.WFENRIR_WOLF_ID:
				return 3;
			case PetDataTable.LIGHT_PURPLE_MANED_HORSE_ID:
			case PetDataTable.TAWNY_MANED_LION_ID:
			case PetDataTable.STEAM_BEATLE_ID:
				return 4;
		}
		return 0;
	}

	@Override
	public float getColRadius()
	{
		if(getTransformationId() != 0)
			return (float) _transformation.getCollisionRadius(this);

		else if(isMounted())
			return NpcTable.getTemplate(getMountNpcId()).collisionRadius;
		else
			return getBaseTemplate().collisionRadius;
	}

	@Override
	public float getColHeight()
	{
		if(getTransformationId() != 0)
			return (float) _transformation.getCollisionHeight(this);

		else if(isMounted())
			return NpcTable.getTemplate(getMountNpcId()).collisionHeight + getBaseTemplate().collisionHeight;
		else
			return getBaseTemplate().collisionHeight;
	}

	@Override
	public void setReflection(final long i)
	{
		if(_reflection == i)
			return;

		super.setReflection(i);
		if(_summon != null && !_summon.isDead())
			_summon.setReflection(i);
		if(i != 0)
		{
			final String var = getVar("reflection");
			if(var == null || !var.equals(String.valueOf(i)))
				setVar("reflection", String.valueOf(i));
		}
		else
			unsetVar("reflection");

		if(getActiveClass() != null)
		{
			getInventory().checkAllConditions();
			// Для квеста _129_PailakaDevilsLegacy
			if(getPet() != null && (getPet().getNpcId() == 14916 || getPet().getNpcId() == 14917))
				getPet().unSummon();
		}
	}

	public boolean isCombatFlagEquipped()
	{
		final L2ItemInstance weapon = getActiveWeaponInstance();
		return weapon != null && weapon.getItem().isCombatFlag();
	}

	public boolean isTerritoryFlagEquipped()
	{
		final L2ItemInstance weapon = getActiveWeaponInstance();
		return weapon != null && weapon.getItem().isTerritoryFlag();
	}

	public void setBuyListId(final int listId)
	{
		_buyListId = listId;
	}

	public int getBuyListId()
	{
		return _buyListId;
	}

	public boolean checksForShop(final boolean isRequestManufacture)
	{
		if(!getPlayerAccess().UseTrade)
		{
			sendPacket(Msg.THIS_ACCOUNT_CANOT_USE_PRIVATE_STORES);
			return false;
		}

		final String tradeBan = getVar("tradeBan");
		if(tradeBan != null && (tradeBan.equals("-1") || !(Long.parseLong(tradeBan) > System.currentTimeMillis())))
		{
			sendMessage("Your trade is banned! Expires: " + (tradeBan.equals("-1") ? "never" : Util.formatTime((Long.parseLong(tradeBan) - System.currentTimeMillis()) / 1000)) + ".");
			return false;
		}

		if(isActionBlocked(isRequestManufacture ? L2Zone.BLOCKED_ACTION_PRIVATE_WORKSHOP : L2Zone.BLOCKED_ACTION_PRIVATE_STORE) && !isInStoreMode() && (!Config.SERVICES_NO_TRADE_ONLY_OFFLINE || Config.SERVICES_NO_TRADE_ONLY_OFFLINE && isInOfflineMode()))
		{
			sendPacket(Msg.A_PRIVATE_STORE_MAY_NOT_BE_OPENED_IN_THIS_AREA);
			return false;
		}

		if(isCastingNow())
		{
			sendPacket(Msg.A_PRIVATE_STORE_MAY_NOT_BE_OPENED_WHILE_USING_A_SKILL);
			return false;
		}

		if(isInCombat())
		{
			sendPacket(Msg.WHILE_YOU_ARE_ENGAGED_IN_COMBAT_YOU_CANNOT_OPERATE_A_PRIVATE_STORE_OR_PRIVATE_WORKSHOP);
			return false;
		}

		if(isOutOfControl() || isActionsDisabled() || isMounted() || isInOlympiadMode() || getDuel() != null)
			return false;

		if(Config.SERVICES_TRADE_ONLY_FAR && !isInStoreMode())
		{
			boolean tradenear = false;
			for(final L2Player player : L2World.getAroundPlayers(this, Config.SERVICES_TRADE_RADIUS, 200))
				if(player.isInStoreMode())
				{
					tradenear = true;
					break;
				}

			if(L2World.getAroundNpc(this, Config.SERVICES_TRADE_RADIUS + 100, 200).size() > 0)
				tradenear = true;

			if(tradenear)
			{
				sendMessage(new CustomMessage("trade.OtherTradersNear", this));
				return false;
			}
		}
		return true;
	}

	public boolean checksForMount(final boolean checkCast, final boolean action)
	{
		if(isInOlympiadMode())
		{
			sendPacket(Msg.THIS_ITEM_IS_NOT_AVAILABLE_FOR_THE_OLYMPIAD_EVENT);
			return false;
		}
		if(isInVehicle())
		{
			sendPacket(Msg.YOU_CANNOT_MOUNT_BECAUSE_YOU_DO_NOT_MEET_THE_REQUIREMENTS);
			return false;
		}
		if(checkCast && isCastingNow())
		{
			sendPacket(Msg.YOU_CANNOT_MOUNT_BECAUSE_YOU_DO_NOT_MEET_THE_REQUIREMENTS);
			return false;
		}
		if(isParalyzed())
		{
			sendPacket(Msg.YOU_CANNOT_MOUNT_BECAUSE_YOU_DO_NOT_MEET_THE_REQUIREMENTS);
			return false;
		}
		if(!action && (getPet() != null || isMounted()))
		{
			sendPacket(Msg.YOU_ALREADY_HAVE_A_PET);
			return false;
		}
		if(getDuel() != null)
		{
			sendPacket(Msg.YOU_CANNOT_MOUNT_BECAUSE_YOU_DO_NOT_MEET_THE_REQUIREMENTS);
			return false;
		}
		if(isInCombat())
		{
			sendPacket(Msg.A_STRIDER_CANNOT_BE_RIDDEN_WHILE_IN_BATTLE);
			return false;
		}
		if(isFishing())
		{
			sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
			return false;
		}
		if(isSitting() || isMoving)
		{
			sendPacket(Msg.A_STRIDER_CAN_BE_RIDDEN_ONLY_WHEN_STANDING);
			return false;
		}
		if(isCursedWeaponEquipped())
		{
			sendPacket(Msg.YOU_CANNOT_MOUNT_BECAUSE_YOU_DO_NOT_MEET_THE_REQUIREMENTS);
			return false;
		}
		if(getTransformationId() != 0)
		{
			sendPacket(Msg.YOU_CANNOT_MOUNT_BECAUSE_YOU_DO_NOT_MEET_THE_REQUIREMENTS);
			return false;
		}
		if(isCombatFlagEquipped() || isTerritoryFlagEquipped())
		{
			sendPacket(Msg.A_STRIDER_CANNOT_BE_RIDDEN_WHILE_IN_BATTLE);
			return false;
		}

		return true;
	}

	public SystemMessage canJoinParty(final L2Player inviter)
	{
		if(isInTransaction())
			return Msg.WAITING_FOR_ANOTHER_REPLY;
		if(isBlockAll() || getMessageRefusal())
			return Msg.THE_PERSON_IS_IN_A_MESSAGE_REFUSAL_MODE;
		if(isInParty())
			return new SystemMessage(SystemMessage.S1_IS_A_MEMBER_OF_ANOTHER_PARTY_AND_CANNOT_BE_INVITED).addString(getName());
		if(ReflectionTable.getInstance().findSoloKamaloka(getObjectId()) != null)
			return Msg.INCORRECT_TARGET;
		
		if(inviter.getReflection() != getReflection()) // если приглашающий не в рефлекте приглашаемого, а так же не в DEFAULT рефлекте (основной мир)
			if(inviter.getReflection() != ReflectionTable.getInstance().get(0) && getReflection() != ReflectionTable.getInstance().get(0))
				return Msg.INCORRECT_TARGET;
		
		if(isCursedWeaponEquipped() || inviter.isCursedWeaponEquipped())
			return Msg.INCORRECT_TARGET;
		if(inviter.isInOlympiadMode() || isInOlympiadMode())
			return Msg.INCORRECT_TARGET;
		if(!inviter.getPlayerAccess().CanJoinParty || !getPlayerAccess().CanJoinParty)
			return Msg.INCORRECT_TARGET;
		// нельзя взять в пати члена другой команды
		if(getTeam() > 0 && isChecksForTeam() && inviter.getTeam() > 0 && inviter.isChecksForTeam() && getTeam() != inviter.getTeam())
			return Msg.INCORRECT_TARGET;
		SystemMessage msg = null;
		final Iterable<EventScript> scripts = Scripts.getInstance().getEventScripts(ScriptEventType.ON_PARTY_INVITE);
		for(final EventScript script : scripts)
			if((msg = script.notifyPartyInviteTry(inviter, this)) != null)
				return msg;

		return null;
	}

	public boolean canLeaveParty()
	{
		if(isInOlympiadMode())
		{
			sendMessage("Вы не можете сейчас выйти из группы.");
			return false;
		}

		final Iterable<EventScript> scripts = Scripts.getInstance().getEventScripts(ScriptEventType.ON_PARTY_LEAVE);
		for(final EventScript script : scripts)
			if(!script.notifyPartyLeaveTry(this))
			{
				sendPacket(Msg.FAILED_TO_WITHDRAW_FROM_THE_PARTY);
				return false;
			}

		return true;
	}

	/**
	 * Functions for Vitality Level.
	 */

	/** Add/Increase VP of this Player **/
	public synchronized void addVitalityPoints(final float val, final boolean sendMessage)
	{
		if(getVitalityPoints() + val > MAX_VITALITY_POINTS)
			_vitalityPoints = MAX_VITALITY_POINTS;
		else
			_vitalityPoints += val;

		updateVitalityLevel(sendMessage);
	}

	/** Remove/Decrease VP of this Player **/
	public synchronized void removeVitalityPoints(final float val, final boolean sendMessage)
	{
		if(getVitalityPoints() - val < MIN_VITALITY_POINTS)
			_vitalityPoints = MIN_VITALITY_POINTS;
		else
			_vitalityPoints -= val;

		updateVitalityLevel(sendMessage);
	}

	/** Set VP of this Player **/
	public synchronized void setVitalityPoints(float val, final boolean sendMessage)
	{
		val = Math.min(Math.max(val, MIN_VITALITY_POINTS), MAX_VITALITY_POINTS);
		if(val == _vitalityPoints)
			return;

		_vitalityPoints = val;
		updateVitalityLevel(sendMessage);
		sendPacket(new ExVitalityPointInfo((int) getVitalityPoints()));
	}

	/** Returns VL Points **/
	public float getVitalityPoints()
	{
		return _vitalityPoints;
	}

	/** Set VL of this Player **/
	public void setVitalityLevel(int val, final boolean sendMessage)
	{
		val = Math.min(Math.max(val, 0), 4);

		if(sendMessage)
		{
			if(getVitalityLevel() < val)
				sendPacket(Msg.VITALITY_HAS_INCREASED);
			else if(getVitalityLevel() > val)
				sendPacket(Msg.VITALITY_HAS_DECREASED);
			if(val == 4)
				sendPacket(Msg.VITALITY_IS_AT_MAXIMUM);
			else if(val == 0)
				sendPacket(Msg.VITALITY_IS_FULLY_EXHAUSTED);
		}
		_vitalityLevel = val;

		sendUserInfo();
	}

	/** Returns VL of this Player **/
	public int getVitalityLevel()
	{
		return _vitalityLevel;
	}

	/** Update VL of this Player **/
	public void updateVitalityLevel(final boolean sendMessage)
	{
		if(getVitalityPoints() > VITALITY_LEVELS[3] && getVitalityLevel() != 4)
			setVitalityLevel(4, sendMessage);
		else if(getVitalityPoints() > VITALITY_LEVELS[2] && getVitalityPoints() <= VITALITY_LEVELS[3] && getVitalityLevel() != 3)
			setVitalityLevel(3, sendMessage);
		else if(getVitalityPoints() > VITALITY_LEVELS[1] && getVitalityPoints() <= VITALITY_LEVELS[2] && getVitalityLevel() != 2)
			setVitalityLevel(2, sendMessage);
		else if(getVitalityPoints() > VITALITY_LEVELS[0] && getVitalityPoints() <= VITALITY_LEVELS[1] && getVitalityLevel() != 1)
			setVitalityLevel(1, sendMessage);
		else if(getVitalityPoints() < VITALITY_LEVELS[0] && getVitalityLevel() != 0)
			setVitalityLevel(0, sendMessage);
	}

	/** Расчёт добавления/удаления очков виталити **/
	public void calculateVitalityPoints(final L2NpcInstance target, final long finalExp)
	{
		if(target == null || finalExp <= 0 || getLevel() < 10 || target.isMinion())
			return;

		final int targetLevel = target.getLevel();

		// vitality calculation formula
		float points = 25 * target.getExpReward() / (targetLevel * targetLevel * 9);

		// fix for herb exploit when high lvl player is farming blue monsters
		if(targetLevel < getLevel() - 9)
			points = 10;

		final int stat = (int) calcStat(Stats.VITALITY_CONSUME_RATE, 1, this, null);
		// если ноль то виталити не расходуется
		if(stat == 0)
			return;

		if(points > 0)
			if(target.isRaid() || target.isBoss())
				addVitalityPoints(Config.VITALITY_RAID_BONUS, true);
			else if(stat < 0) // если меньше нуля - то добавляем эти очки
			{
				points *= Config.RATE_VITALITY_GAIN;
				sendPacket(Msg.GAINED_VITALITY_POINTS);
				addVitalityPoints(points, true);
			}
			else
			{
				points *= Config.RATE_VITALITY_LOST;
				removeVitalityPoints(points, true);
			}
	}

	/** Add Vitality Points on relog **/
	public void restoreVitality()
	{
		final float points = (float) (Config.RATE_RECOVERY_ON_RECONNECT * (System.currentTimeMillis() / 1000 - getLastAccess()) / 60);
		if(points > 0)
			setVitalityPoints(getVitalityPoints() + points, false);
	}

	public void setEnterWorldTime()
	{
		_enterWorldTime = System.currentTimeMillis();
	}

	public long getEnterWorldTime()
	{
		return _enterWorldTime;
	}

	public void setCharSelectTime()
	{
		_charSelectTime = System.currentTimeMillis();
	}

	public long getCharSelectTime()
	{
		return _charSelectTime;
	}
	
	public void setCharCaptchaTime()
    {
        _charCaptchaTime = System.currentTimeMillis();
    }

    public long getCharCaptchaTime()
    {
        return _charCaptchaTime;
    }

	public int getExpandInventory()
	{
		return _expandInventory;
	}

	public void setExpandInventory(final int inventory)
	{
		_expandInventory = inventory;
	}

	public int getExpandWarehouse()
	{
		return _expandWarehouse;
	}

	public void setExpandWarehouse(final int warehouse)
	{
		_expandWarehouse = warehouse;
	}

	public boolean isNotShowBuffAnim()
	{
		return _notShowBuffAnim;
	}

	public void setNotShowBuffAnim(final boolean value)
	{
		_notShowBuffAnim = value;
	}

	/** Возвращает сколько уже было проверок на валидность координат */
	public int getIncorrectValidateCount()
	{
		return _incorrectValidateCount;
	}

	/** истанавлиевает количество проверок на валидность координат */
	public void setIncorrectValidateCount(final int count)
	{
		_incorrectValidateCount = count;
	}

	public void enterMovieMode()
	{
		setTarget(null);
		stopMove();
		setInvul(true);
		setImmobilized(true);
		sendPacket(new CameraMode(1));
	}

	public void leaveMovieMode()
	{
		if(!isGM())
			setInvul(false);
		setImmobilized(false);
		sendPacket(new CameraMode(0));
		sendPacket(new NormalCamera());
		broadcastUserInfo(true);
	}

	public void specialCamera(final L2Object target, final int dist, final int yaw, final int pitch, final int time, final int duration, final int turn, final int rise)
	{
		sendPacket(new SpecialCamera(target.getObjectId(), dist, yaw, pitch, time, duration, turn, rise));
	}

	public void specialCamera(final L2Object target, final int dist, final int yaw, final int pitch, final int time, final int duration)
	{
		sendPacket(new SpecialCamera(target.getObjectId(), dist, yaw, pitch, time, duration, 0, 0));
	}

	public void specialCamera(final L2Object target, final int dist, final int yaw, final int pitch, final int time, final int duration, final int widescreen)
	{
		sendPacket(new SpecialCamera(target.getObjectId(), dist, yaw, pitch, time, duration, 0, 0, widescreen, 0));
	}

	public void setMovieId(final int id)
	{
		_movieId = id;
	}

	public int getMovieId()
	{
		return _movieId;
	}

	public void showQuestMovie(final int id)
	{
		if(_movieId > 0)// already in movie
			return;

		sendActionFailed();
		setTarget(null);
		stopMove();
		setMovieId(id);
		sendPacket(new ExStartScenePlayer(id));
	}

	public final void reName(final String name, final boolean saveToDB)
	{
		setName(name);
		if(saveToDB)
			saveNameToDB();
		Olympiad.changeNobleName(getObjectId(), name);
		broadcastUserInfo(true);
	}

	public final void reName(final String name)
	{
		reName(name, false);
	}

	public final void saveNameToDB()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement st = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			st = con.prepareStatement("UPDATE characters SET char_name = ? WHERE obj_Id = ?");
			st.setString(1, getName());
			st.setInt(2, getObjectId());
			st.executeUpdate();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player: could not save name to DB: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, st);
		}
	}

	@Override
	public L2Player getPlayer()
	{
		return this;
	}

	/**
	 * @return the L2GolemTrader of the L2Player or null.
	 */
	public L2GolemTraderInstance getMerchant()
	{
		return _merchant;
	}

	/**
	 * Set the L2GolemTrader of the L2Player.<BR>
	 * <BR>
	 * 
	 * @param summon
	 *            L2GolemTrader to set
	 */
	public void setMerchant(final L2GolemTraderInstance merchant)
	{
		_merchant = merchant;
	}

	private int _bookmarkslot = 0; // The Teleport Bookmark Slot

	public int getBookMarkSlot()
	{
		return _bookmarkslot;
	}

	public void setBookMarkSlot(final int slot)
	{
		_bookmarkslot = slot;
		sendPacket(new ExGetBookMarkInfo(this));
	}

	public final GArray<TeleportBookmark> tpbookmark = new GArray<TeleportBookmark>();

	private static final int MY_TELEPORT_SCROLL = 13016;
	// private static final int MY_TELEPORT_SCROLL_EVENT = 13302; //FIXME MY_TELEPORT_SCROLL_EVENT
	private static final int MY_TELEPORT_FLAG = 20033;

	// Character Teleport Bookmark:
	private static final String INSERT_TP_BOOKMARK = "REPLACE INTO character_tpbookmark (char_id, Id, x, y, z, icon, tag, name) values (?,?,?,?,?,?,?,?)";
	private static final String UPDATE_TP_BOOKMARK = "UPDATE character_tpbookmark SET icon=?,tag=?,name=? WHERE char_id=? AND Id=?";

	public static class TeleportBookmark
	{
		public int _id, _x, _y, _z, _icon;
		public String _name, _tag;

		public TeleportBookmark(final int id, final int x, final int y, final int z, final int icon, final String tag, final String name)
		{
			_id = id;
			_x = x;
			_y = y;
			_z = z;
			_icon = icon;
			_name = name;
			_tag = tag;
		}
	}

	public void teleportBookmarkModify(final int Id, final int icon, final String tag, final String name)
	{
		int count = 0;
		final int size = tpbookmark.size();
		while (size > count)
		{
			if(tpbookmark.get(count)._id == Id)
			{
				tpbookmark.get(count)._icon = icon;
				tpbookmark.get(count)._tag = tag;
				tpbookmark.get(count)._name = name;

				ThreadConnection con = null;
				FiltredPreparedStatement statement = null;
				try
				{
					con = L2DatabaseFactory.getInstance().getConnection();
					statement = con.prepareStatement(UPDATE_TP_BOOKMARK);

					statement.setInt(1, icon);
					statement.setString(2, tag);
					statement.setString(3, name);
					statement.setInt(4, getObjectId());
					statement.setInt(5, Id);

					statement.execute();
				}
				catch(final Exception e)
				{
					_log.log(Level.WARNING, "L2Player: could not update character teleport bookmark data: ", e);
				}
				finally
				{
					DbUtils.closeQuietly(con, statement);
				}
			}
			count++;
		}
		sendPacket(new ExGetBookMarkInfo(this));
	}

	public void teleportBookmarkDelete(final int Id)
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			statement.executeUpdate("DELETE FROM character_tpbookmark WHERE char_id=" + getObjectId() + " AND Id=" + Id);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player: could not delete character teleport bookmark data: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}

		int count = 0;
		final int size = tpbookmark.size();

		while (size > count)
		{
			if(tpbookmark.get(count)._id == Id)
			{
				tpbookmark.remove(count);
				break;
			}
			count++;
		}

		sendPacket(new ExGetBookMarkInfo(this));
	}

	public void teleportBookmarkGo(final int Id)
	{
		if(!teleportBookmarkCondition(0))
			return;

		if(getInventory().getCountOf(MY_TELEPORT_SCROLL) == 0/* && getInventory().getCountOf(MY_TELEPORT_SCROLL_EVENT) == 0 */)
		{
			sendPacket(new SystemMessage(SystemMessage.YOU_CANNOT_TELEPORT_BECAUSE_YOU_DO_NOT_HAVE_A_TELEPORT_ITEM));
			return;
		}

		final SystemMessage sm = new SystemMessage(SystemMessage.S1_HAS_DISAPPEARED);
		sm.addItemName(MY_TELEPORT_SCROLL);
		sendPacket(sm);

		int count = 0;
		final int size = tpbookmark.size();
		while (size > count)
		{
			if(tpbookmark.get(count)._id == Id)
			{
				getInventory().destroyItemByItemId(MY_TELEPORT_SCROLL, 1, true);
				teleToLocation(tpbookmark.get(count)._x, tpbookmark.get(count)._y, tpbookmark.get(count)._z);
				break;
			}
			count++;
		}
		sendPacket(new ExGetBookMarkInfo(this));
	}

	public boolean teleportBookmarkCondition(final int type)
	{
		if(isCombatFlagEquipped())
		{
			sendPacket(Msg.YOU_CANNOT_TELEPORT_WHILE_IN_POSSESSION_OF_A_WARD);
			return false;
		}
		if(isTerritoryFlagEquipped())
		{
			sendPacket(Msg.YOU_CANNOT_TELEPORT_WHILE_IN_POSSESSION_OF_A_WARD);
			return false;
		}
		if(getReflection().getId() != 0 || isInJail())
		{
			sendPacket(Msg.YOU_CANNOT_USE_MY_TELEPORTS_IN_AN_INSTANT_ZONE);
			return false;
		}
		if(getDuel() != null)
		{
			sendPacket(Msg.YOU_CANNOT_USE_MY_TELEPORTS_DURING_A_DUEL);
			return false;
		}
		if(isInCombat() || getPvpFlag() != 0)
		{
			sendPacket(Msg.YOU_CANNOT_USE_MY_TELEPORTS_DURING_A_BATTLE);
			return false;
		}
		if(isInOlympiadMode() || isInZoneOlympiad())
		{
			sendPacket(Msg.YOU_CANNOT_USE_MY_TELEPORTS_WHILE_PARTICIPATING_IN_AN_OLYMPIAD_MATCH);
			return false;
		}
		if(isOnSiegeField() || isInZoneBattle() || isInZone(ZoneType.Siege))
		{
			sendPacket(Msg.YOU_CANNOT_USE_MY_TELEPORTS_WHILE_PARTICIPATING_A_LARGE_SCALE_BATTLE_SUCH_AS_A_CASTLE_SIEGE);
			return false;
		}
		if(isFlying())
		{
			sendPacket(Msg.YOU_CANNOT_USE_MY_TELEPORTS_WHILE_FLYING);
			return false;
		}
		if(isSwimming() || isInVehicle())
		{
			sendPacket(Msg.YOU_CANNOT_USE_MY_TELEPORTS_UNDERWATER);
			return false;
		}
		if(isAlikeDead())
		{
			sendPacket(Msg.YOU_CANNOT_USE_MY_TELEPORTS_WHILE_YOU_ARE_DEAD);
			return false;
		}
		if(getPrivateStoreType() != 0 || isInTransaction())
		{
			sendPacket(Msg.YOU_CANNOT_SUMMON_DURING_A_TRADE_OR_WHILE_USING_THE_PRIVATE_SHOPS);
			return false;
		}
		if(isParalyzed() || isStunned() || isSleeping())
		{
			sendPacket(Msg.YOU_CANNOT_USE_MY_TELEPORTS_WHILE_YOU_ARE_IN_A_FLINT_OR_PARALYZED_STATE);
			return false;
		}

		if(isInZone(ZoneInfo.ZONE_NO_SAVE_BOOKMARK))
			return false;

		return true;
	}

	public void teleportBookmarkAdd(final int x, final int y, final int z, final int icon, final String tag, final String name)
	{
		if(!teleportBookmarkCondition(1))
			return;

		if(tpbookmark.size() >= _bookmarkslot)
		{
			sendPacket(Msg.YOU_HAVE_NO_SPACE_TO_SAVE_THE_TELEPORT_LOCATION);
			return;
		}

		if(getInventory().getCountOf(MY_TELEPORT_FLAG) == 0)
		{
			sendPacket(Msg.YOU_CANNOT_BOOKMARK_THIS_LOCATION_BECAUSE_YOU_DO_NOT_HAVE_A_MY_TELEPORT_FLAG);
			return;
		}

		int count = 0;
		int id = 1;
		final GArray<Integer> idlist = new GArray<Integer>();

		final int size = tpbookmark.size();
		while (size > count)
		{
			idlist.add(tpbookmark.get(count)._id);
			count++;
		}

		for(int i = 1; i < 10; i++)
			if(!idlist.contains(i))
			{
				id = i;
				break;
			}

		final TeleportBookmark tpadd = new TeleportBookmark(id, x, y, z, icon, tag, name);
		tpbookmark.add(tpadd);

		getInventory().destroyItemByItemId(MY_TELEPORT_FLAG, 1, true);

		final SystemMessage sm = new SystemMessage(SystemMessage.S1_HAS_DISAPPEARED);
		sm.addItemName(MY_TELEPORT_FLAG);
		sendPacket(sm);

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(INSERT_TP_BOOKMARK);

			statement.setInt(1, getObjectId());
			statement.setInt(2, id);
			statement.setInt(3, x);
			statement.setInt(4, y);
			statement.setInt(5, z);
			statement.setInt(6, icon);
			statement.setString(7, tag);
			statement.setString(8, name);

			statement.execute();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player: could not insert character teleport bookmark data: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}

		sendPacket(new ExGetBookMarkInfo(this));
	}

	public void restoreTeleportBookmark()
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();

			rset = statement.executeQuery("SELECT Id, x, y, z, icon, tag, name FROM character_tpbookmark WHERE char_id=" + getObjectId());

			while (rset.next())
				tpbookmark.add(new TeleportBookmark(rset.getInt("Id"), rset.getInt("x"), rset.getInt("y"), rset.getInt("z"), rset.getInt("icon"), rset.getString("tag"), rset.getString("name")));
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Player: failed restoing character teleport bookmark.", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	/** @return время создание персонажа */
	public long getCreateTime()
	{
		return _createTime;
	}

	/** истанавливает время создание персонажа */
	public void setCreateTime(final long createTime)
	{
		_createTime = createTime;
	}

	/** истанавливает id терретории для осады */
	public void setTerritorySiege(final int side)
	{
		_territorySide = side;
	}

	/** @return id терретории на которую зареган игрок */
	public int getTerritorySiege()
	{
		if(_clan != null && _clan.getTerritorySiege() > -1)
			return _clan.getTerritorySiege();
		return _territorySide;
	}

	public final boolean isInSameChannel(final L2Player target)
	{
		final L2Party activeCharP = getParty();
		if(activeCharP != null)
		{
			final L2CommandChannel chan = activeCharP.getCommandChannel();
			if(chan != null && chan.contains(target))
				return true;
		}
		return false;
	}

	public final boolean isInSameParty(final L2Player target)
	{
		return getParty() != null && getParty() == target.getParty();
	}

	public final boolean isInSameClan(final L2Player target)
	{
		return getClanId() != 0 && getClanId() == target.getClanId();
	}

	public final boolean isInSameAlly(final L2Player target)
	{
		return getAllyId() != 0 && getAllyId() == target.getAllyId();
	}

	public final boolean isInSameDuel(final L2Player target)
	{
		return getDuel() != null && getDuel() == target.getDuel();
	}

	public final boolean isInSameOlympiadGame(final L2Player target)
	{
		return getOlympiadGameId() != -1 && getOlympiadGameId() == target.getOlympiadGameId();
	}

	public int isAtWarWith(final int id)
	{
		if(_clan == null)
			return 0;

		if(_clan.isAtWarWith(id))
			return 1;

		return 0;
	}

	public int isAtWar()
	{
		if(_clan == null)
			return 0;
		if(_clan.isAtWarOrUnderAttack() > 0)
			return 1;
		return 0;
	}

	public final boolean atWarWith(final L2Player player)
	{
		return _clan != null && player.getClan() != null && getPledgeType() != -1 && player.getPledgeType() != -1 && _clan.isAtWarWith(player.getClan().getClanId());
	}

	public boolean atMutualWarWith(final L2Player player)
	{
		return _clan != null && player.getClan() != null && getPledgeType() != -1 && player.getPledgeType() != -1 && _clan.isAtWarWith(player.getClan().getClanId()) && player.getClan().isAtWarWith(_clan.getClanId());
	}

	public void startWaterTask()
	{
		if(isDead())
			stopWaterTask();
		else if(Config.ALLOW_WATER && _taskWater == null)
		{
			final int timeinwater = (int) (calcStat(Stats.BREATH, 86, null, null) * 1000L);
			sendPacket(new SetupGauge(2, timeinwater));
			if(_transformationId != 0 && !L2Transformation.isDefaultActionListTransform(_transformationId) && !isCursedWeaponEquipped())
				stopTransformation();
			_taskWater = L2GameThreadPools.getInstance().scheduleAiAtFixedRate(new WaterTask(this), timeinwater, 1000L, true);
			sendChanges();
		}
	}

	public final boolean isInSameClanWar(final L2Player target)
	{
		final L2Clan aClan = getClan();
		final L2Clan tClan = target.getClan();

		if(aClan != null && tClan != null)
			if(aClan.isAtWarWith(tClan.getClanId()) && tClan.isAtWarWith(aClan.getClanId()))
				return true;
		return false;
	}

	public final boolean isInSameSiegeSide(final L2Player target)
	{
		if(getSiegeState() == 0 || target.getSiegeState() == 0)
			return false;

		final Siege s = SiegeManager.getSiege(this, false);
		if(s != null)
		{
			if(s.checkIsDefender(getClan(), false) && s.checkIsDefender(target.getClan(), false))
				return true;
			if(s.checkIsAttacker(getClan()) && s.checkIsAttacker(target.getClan()))
				return true;
		}

		return false;
	}

	public boolean hasPremiumAccount()
	{
		if(_connection == null || !Config.SERVICES_RATE_BONUS_ENABLED)
			return false;
		return _connection.getBonusExpire() > 0 && _connection.getBonusExpire() > System.currentTimeMillis() / 1000;
	}

	public long getAccountPremiumEndDate()
	{
		return _connection.getBonusExpire();
	}

	private GArray<String> getStoredBypasses(final boolean bbs)
	{
		if(bbs)
		{
			if(bypasses_bbs == null)
				bypasses_bbs = new GArray<String>();
			return bypasses_bbs;
		}
		if(bypasses == null)
			bypasses = new GArray<String>();
		return bypasses;
	}

	public void cleanBypasses(final boolean bbs)
	{
		final GArray<String> bypassStorage = getStoredBypasses(bbs);
		synchronized (bypassStorage)
		{
			bypassStorage.clearSize();
		}
	}

	public String encodeBypasses(final String htmlCode, final boolean bbs)
	{
		final GArray<String> bypassStorage = getStoredBypasses(bbs);
		synchronized (bypassStorage)
		{
			return BypassManager.encode(htmlCode, bypassStorage, bbs);
		}
	}

	public DecodedBypass decodeBypass(final String bypass)
	{
		final BypassType bpType = BypassManager.getBypassType(bypass);
		final boolean bbs = bpType == BypassType.ENCODED_BBS || bpType == BypassType.SIMPLE_BBS;
		final GArray<String> bypassStorage = getStoredBypasses(bbs);
		if(bpType == BypassType.ENCODED || bpType == BypassType.ENCODED_BBS)
			return BypassManager.decode(bypass, bypassStorage, bbs, this);
		if(bpType == BypassType.SIMPLE) // FIXME по идее таких не должно быть
			return new DecodedBypass(bypass, false).trim();
		if(bpType == BypassType.SIMPLE_BBS && !Strings.matches(bypass, Config.COMMUNITYBOARD_BYPASS_PATERN))
			return new DecodedBypass(bypass, true).trim();

		_log.warning("Direct access to bypass: " + bypass + " / Player: " + getName());
		Util.handleIllegalPlayerAction(this, "L2Player[12331]", "Direct access to bypass: " + bypass, IllegalPlayerAction.CRITICAL);
		return null;
	}

	public boolean hasHWID()
	{
		return _connection != null && _connection.hasHWID();
	}

	public HardwareID getHWID()
	{
		return _connection.HWID;
	}

	public HardwareID getAllowHWID()
	{
		return _connection.ALLOW_HWID;
	}
	/** Handy's Block Checker Event */
	private int _handysBlockCheckerEventArena = -1;

	public void setHandysBlockCheckerEventArena(final int handysBlockCheckerEventArena)
	{
		_handysBlockCheckerEventArena = handysBlockCheckerEventArena;
	}

	public int getHandysBlockCheckerEventArena()
	{
		return _handysBlockCheckerEventArena;
	}

	// TODO The Ghost Engine: Bot protection

	public final void setCaughtByRabbits(final boolean m)
	{
		_isCaughtByRabbits = m;
	}

	public final boolean isCaughtByRabbits()
	{
		return _isCaughtByRabbits;
	}

	public final void setLastRabbitsOkay(final long d)
	{
		_lastRabbitsOkay = d;
	}

	public final long getLastRabbitsOkay()
	{
		return _lastRabbitsOkay;
	}

	public final long getLastBotTrackerCheck()
	{
		return _lastCheckByBotTracker;
	}

	public final void setLastBotTrackerCheck(final long i)
	{
		_lastCheckByBotTracker = i;
	}

	public long getLastRabbitSpawnTime()
	{
		return _lastRabbitSpawnTime;
	}

	public void setLastRabbitSpawnTime(final long time)
	{
		_lastRabbitSpawnTime = time;
	}

	public long getLastMonsterKilledTime()
	{
		return _lastMonsterKilled;
	}

	public void setLastMonsterKilledTime(final long time)
	{
		_lastMonsterKilled = time;
	}

	public final PlayerRestrictions getRestrictions()
	{
		return restrictions;
	}
    
    int _selectCommunitySkillLS = 0;
    public void setSelectCommunitySkillLS(int id)
    {
    _selectCommunitySkillLS = id;
    }
    public int getSelectCommunitySkillLS()
    {
    return _selectCommunitySkillLS;
    }

    L2ItemInstance _selectCommunityItemLS = null;
    public void setSelectCommunityItemLS(L2ItemInstance iteminstance)
    {
    _selectCommunityItemLS = iteminstance;
    }
    public L2ItemInstance getSelectCommunityItemLS()
    {
    return _selectCommunityItemLS;
    }

    String filterType = "all";
    public void setCommunityFilterType(String type)
    {
        filterType = type;
    }

    public String getCommunityFilterType()
    {
        return filterType;
    }
	public final void setCaptchaInt(int captchaInt)
	{
		_captchaKill = captchaInt;
	}
	
	public final int getCaptchaInt()
	{
		return _captchaKill;
	}

    public final boolean isFakePlayer()
    {
        return _isFakePlayer;
    }

    public final void setFakePlayer(boolean fakePlayer)
    {
        _isFakePlayer = fakePlayer;
    }
	public final void setPassCheck(boolean passCheck)
	{
		_passCheck = passCheck;
	}
	
	public final boolean getPassCheck()
	{
		return _passCheck;
	}

    public final void setPassParalyzedTrue()
    {
        _isParalyzed = isParalyzed();
        setParalyzed(true);
    }

    public final void setPassParalyzedFalse()
    {
        if(!_isParalyzed){
            setParalyzed(false);
        }
    }
	@Override
	public void onSpawn()
	{
		super.onSpawn();
		final Iterable<EventScript> scripts = Scripts.getInstance().getEventScripts(ScriptEventType.ON_SPAWN);
		for(final EventScript script : scripts)
			script.notifySpawned(this);
	}

	@Override
	public boolean isPlayer()
	{
		return true;
	}
}
