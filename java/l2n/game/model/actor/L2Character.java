package l2n.game.model.actor;

import gnu.trove.map.hash.TIntLongHashMap;
import gnu.trove.procedure.TIntLongProcedure;
import l2n.Config;
import l2n.commons.concurrent.AtomicState;
import l2n.commons.lang.reference.HardReferences;
import l2n.commons.lang.reference.IHardReference;
import l2n.commons.list.GArray;
import l2n.commons.list.procedure.INgObjectProcedure;
import l2n.commons.listener.ListenerList;
import l2n.commons.util.CollectionUtils;
import l2n.extensions.listeners.ICharacterListener;
import l2n.extensions.listeners.collections.MethodCollection;
import l2n.extensions.listeners.collections.PropertyCollection;
import l2n.extensions.listeners.events.L2Object.TerritoryChangeEvent;
import l2n.extensions.listeners.list.CharacterListenerList;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.Scripts;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.DefaultAI;
import l2n.game.ai.L2CharacterAI;
import l2n.game.ai.L2PlayableAI.nextAction;
import l2n.game.ai.model.AdditionalAIParams.AiOptionsType;
import l2n.game.cache.Msg;
import l2n.game.geodata.GeoEngine;
import l2n.game.geodata.GeoMove;
import l2n.game.instancemanager.DimensionalRiftManager;
import l2n.game.instancemanager.EpicBossManager;
import l2n.game.model.*;
import l2n.game.model.EffectList.ForEachEffectStopMeditation;
import l2n.game.model.L2ObjectTasks.*;
import l2n.game.model.L2Skill.SkillTargetType;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.L2Skill.TriggerActionType;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.recorder.CharStatsChangeRecorder;
import l2n.game.model.entity.Duel;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.model.entity.vehicle.L2Ship;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2MinionInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2TrapInstance;
import l2n.game.model.quest.QuestEventType;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.*;
import l2n.game.network.serverpackets.FlyToLocation.FlyType;
import l2n.game.skills.*;
import l2n.game.skills.Formulas.AttackInfo;
import l2n.game.skills.funcs.Func;
import l2n.game.tables.MapRegionTable;
import l2n.game.tables.ReflectionTable;
import l2n.game.tables.SkillTable;
import l2n.game.taskmanager.RegenTaskManager;
import l2n.game.templates.L2CharTemplate;
import l2n.game.templates.L2NpcTemplate;
import l2n.game.templates.L2Weapon;
import l2n.game.templates.L2Weapon.WeaponType;
import l2n.util.Location;
import l2n.util.Rnd;
import l2n.util.SkillUtil;
import l2n.util.Util;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import static l2n.game.ai.CtrlIntention.AI_INTENTION_ACTIVE;

/**
 * Mother class of all character objects of the world (PC, NPC...)<BR>
 * <BR>
 * L2Character :<BR>
 * <BR>
 * <li>L2CastleGuardInstance</li> <li>L2DoorInstance</li> <li>L2NpcInstance</li> <li>L2PlayableInstance</li> <BR>
 * <B><U> Concept of L2CharTemplate</U> :</B><BR>
 * <BR>
 * Each L2Character owns generic and static properties (ex : all Keltir have the same number of HP...).<BR>
 * All of those properties are stored in a different template for each type of L2Character.<BR>
 * Each template is loaded once in the server cache memory (reduce memory use).<BR>
 * This link is stored in <B>_template</B><BR>
 */

public abstract class L2Character extends L2Object
{
	public static enum TargetDirection
	{
		NONE,
		/** в лоб */
		FRONT,
		/** с боку */
		SIDE,
		/** в спину */
		BEHIND
	}

	public static final class HateInfo
	{
		public L2NpcInstance npc;
		public int hate;
		public int damage;

		public HateInfo(final L2NpcInstance attacker)
		{
			npc = attacker;
		}
	}

	protected static final Logger _log = Logger.getLogger(L2Character.class.getName());

	public static final double HEADINGS_IN_PI = 10430.378350470452724949566316381;
	public static final int INTERACTION_DISTANCE = 200;

	public static final EmptyMoveNextTask EMPTY_MOVE_TASK = new EmptyMoveNextTask(null);

	/** Список игроков, которым необходимо отсылать информацию об изменении состояния персонажа */
	private GArray<L2Character> _statusListeners;

	public int _scheduledCastCount;
	public int _scheduledCastInterval;

	public Future<?> _skillTask;
	private Future<?> _skillLaunchedTask;
	private Future<?> _stanceTask;
	private Future<NotifyReadyToAct> _notifyAct;

	private long _stanceEndTime;

	private final static int CLIENT_BAR_SIZE = 352; // 352 - размер полоски CP/HP/MP в клиенте, в пикселях

	private int _lastCpBarUpdate = -1;
	private int _lastHpBarUpdate = -1;
	private int _lastMpBarUpdate = -1;

	protected double _currentCp = 0;
	protected double _currentHp = 1;
	protected double _currentMp = 1;

	private boolean _isAttackAborted;
	private long _attackEndTime;
	private long _attackReuseEndTime;

	/** HashMap(Integer, L2Skill) containing all skills of the L2Character */
	private final ConcurrentHashMap<Integer, L2Skill> _skills = new ConcurrentHashMap<Integer, L2Skill>();
	private ConcurrentLinkedQueue<TriggerSkill>[] _skillsOnAction;

	// Casting Vars
	private L2Skill _castingSkill;
	private long _castInterruptTime;
	private long _animationEndTime;

	private volatile IHardReference<? extends L2Object> target = HardReferences.emptyRef();
	private volatile IHardReference<? extends L2Character> castingTarget = HardReferences.emptyRef();
	private volatile IHardReference<? extends L2Character> followTarget = HardReferences.emptyRef();
	private volatile IHardReference<? extends L2Character> _aggressionTarget = HardReferences.emptyRef();

	/** Table containing all skillId that are disabled */
	protected Map<Integer, SkillTimeStamp> _skillReuses;

	private boolean _massUpdating;

	protected EffectList _effectList;
	protected volatile CharStatsChangeRecorder<? extends L2Character> _statsRecorder;

	private GArray<Stats> _blockedStats;

	/** Map 32 bits (0x00000000) containing all abnormal effect in progress */
	private int _abnormalEffects;
	private int _specialEffects;

	protected TIntLongHashMap _traps;

	private boolean _flying;
	private boolean _riding;

	private boolean _fakeDeath;
	private boolean _fishing;

	protected boolean _isInvul;
	protected boolean _isPendingRevive;
	protected boolean _isTeleporting;
	protected boolean _overloaded;
	protected boolean _killedAlready;
	protected boolean _killedAlreadyPlayer;
	protected boolean _killedAlreadyPet;

	private long _dropDisabled;

	/** Восстанавливает все бафы после смерти */
	private byte _isBlessedByNoblesse;
	/** Восстанавливает все бафы после смерти и полностью CP, MP, HP */
	private byte _isSalvation;
	protected int _salvationPower;
	/** Иммунитет к бафам/дебафам */
	private byte _buffImmunity;
	private final AtomicState _debuffImmunity = new AtomicState(); // Иммунитет к дебафам

	private ConcurrentHashMap<Integer, Byte> _skillMastery;

	private boolean _afraid;
	private boolean _meditated;

	/** Cannot use magic */
	private byte _isMagicMuted;
	/** Cannot use physical skills */
	private byte _isPhysicalMuted;
	/** Cannot use attack */
	private byte _isAttackMuted;

	private boolean _paralyzed;
	private boolean _rooted;
	private boolean _sleeping;
	private boolean _stunned;
	private boolean _imobilised;
	private boolean _confused;
	private boolean _blocked;
	private boolean _healHPBlocked;
	private boolean _healMPBlocked;

	// START ---------- Move vars
	private boolean _running;
	private Future<MoveNextTask> _moveTask;
	protected MoveNextTask _moveTaskRunnable;
	public boolean isMoving;
	public boolean isFollow;
	public List<Location> moveList = new ArrayList<Location>();
	protected Location destination = null;

	/**
	 * при moveToLocation используется для хранения геокоординат в которые мы двигаемся для того что бы избежать повторного построения одного и того же пути
	 * при followToCharacter используется для хранения мировых координат в которых находилась последний раз преследуемая цель для отслеживания необходимости перестраивания пути
	 */
	private final Location movingDestTempPos = new Location();
	private int _offset;

	private boolean _forestalling;
	private long _followTimestamp, _startMoveTime;
	private double _previousSpeed = -1;
	public final List<List<Location>> _targetRecorder = new ArrayList<List<Location>>();
	// STOP ---------- Move vars

	private int _heading;

	/** Table of Calculators containing all used calculator */
	private final Calculator[] _calculators;

	/**
	 * The link on the L2CharTemplate object containing generic and static properties of this L2Character type (ex : Max HP, Speed...)
	 */
	protected L2CharTemplate _template;
	protected L2CharTemplate _baseTemplate;
	protected L2CharacterAI _ai;

	private static final String EMPTY_STRING = new String();
	protected String _name;
	protected String _title;
	protected String _visname;
	protected String _vistitle;

	private boolean _isRegenerating;
	private Future<RegenTask> _regenTask;
	private RegenTask _regenTaskRunnable;

	protected final ReentrantLock dieLock = new ReentrantLock(), statusListenerLock = new ReentrantLock(), regenLock = new ReentrantLock(), territoriesLock = new ReentrantLock();

	private GArray<L2Territory> _territories = null;
	private final ZoneInfo _zoneInfo = new ZoneInfo();

	private Location _flyLoc;
	private boolean onDieTrigger = false;

	protected IHardReference<? extends L2Character> reference;

	public void stopAttackStanceTask()
	{
		broadcastPacket(new AutoAttackStop(getObjectId()));
		try
		{
			if(_stanceTask != null)
				_stanceTask.cancel(false);
		}
		catch(final NullPointerException e)
		{}

		_stanceTask = null;

		if(Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF && Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF_ATTACK_STOP && this == getPlayer())
			fireMethodInvoked(MethodCollection.AutoBuff, new Object[] { 2 });
	}

	/**
	 * Constructor of L2Character.<BR>
	 * <BR>
	 * <B><U> Concept</U> :</B><BR>
	 * <BR>
	 * Each L2Character owns generic and static properties (ex : all Keltir have the same number of HP...). All of those properties are stored in a different template for each type of L2Character. Each template is loaded once in the server cache
	 * memory (reduce memory use). When a new instance of L2Character is spawned, server just create a link between the instance and the template This link is stored in <B>_template</B><BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Set the _template of the L2Character</li> <li>Set _overloaded to false (the charcater can take more items)</li> <BR>
	 * <BR>
	 * <li>If L2Character is a L2NpcInstance, copy skills from template to object</li> <li>If L2Character is a L2NpcInstance, link _calculators to NPC_STD_CALCULATOR</li> <BR>
	 * <BR>
	 * <li>If L2Character is NOT a L2NpcInstance, create an empty _skills slot</li> <li>If L2Character is a L2Player or L2Summon, copy basic Calculator set to object</li> <BR>
	 * <BR>
	 *
	 * @param objectId
	 *            Identifier of the object to initialized
	 * @param template
	 *            The L2CharTemplate to apply to the object
	 */
	public L2Character(final int objectId, final L2CharTemplate template)
	{
		super(objectId);

		// Set its template to the new L2Character
		_template = template;
		_baseTemplate = template;

		_calculators = new Calculator[Stats.NUM_STATS];
		// If L2Character is a L2Player or a L2Summon, create the basic calculator set
		if(isPlayer())
			for(final Stats stat : Stats.values())
				_calculators[stat.ordinal()] = new Calculator(stat, this);

		// Copy the skills of the L2Summon from its template to the L2Character Instance
		if(template != null && (this instanceof L2NpcInstance || this instanceof L2Summon))
			if(((L2NpcTemplate) template).getSkills().size() > 0)
				for(final L2Skill skill : ((L2NpcTemplate) template).getSkills().valueCollection())
					addSkill(skill);

		reference = new L2Reference<L2Character>(this);

		_moveTaskRunnable = EMPTY_MOVE_TASK;
		Formulas.addFuncsToNewCharacter(this);
	}

	@Override
	public IHardReference<? extends L2Character> getRef()
	{
		return reference;
	}

	/**
	 * Abort the attack of the L2Character and send Server->Client ActionFailed packet.<BR>
	 * <BR>
	 */
	public final void abortAttack()
	{
		if(isAttackingNow())
		{
			_attackEndTime = 0;
			_isAttackAborted = true;
			getAI().setIntention(AI_INTENTION_ACTIVE);

			if(isPlayer())
				sendPacket(new SystemMessage(SystemMessage.S1S_ATTACK_FAILED).addName(this), Msg.ActionFail);
		}
	}

	/**
	 * Abort the cast of the L2Character and send Server->Client MagicSkillCanceld/ActionFailed packet.<BR>
	 * <BR>
	 */
	public final void abortCast()
	{
		if(isCastingNow())
		{
			_castInterruptTime = 0;

			final L2Skill castingSkill = _castingSkill;
			if(castingSkill != null)
			{
				if(castingSkill.isUsingWhileCasting())
				{
					final L2Character target = getAI().getAttackTarget();
					if(target != null)
						target.getEffectList().stopEffect(castingSkill.getId());
				}
				final ConcurrentHashMap<Integer, Byte> skillMastery = _skillMastery;
				if(skillMastery != null)
					skillMastery.remove(castingSkill.getId());
			}

			cancelSkillTask(false);
			_castingSkill = null;
			_flyLoc = null;

			broadcastPacket(new MagicSkillCanceled(_objectId)); // broadcast packet to stop animations client-side
			getAI().setIntention(AI_INTENTION_ACTIVE);

			if(isPlayer())
				sendPacket(Msg.CASTING_HAS_BEEN_INTERRUPTED, Msg.ActionFail);// send an "action failed" packet to the caster
		}
	}

	protected void cancelSkillTask(final boolean mayInterruptIfRunning)
	{
		Future<?> future = _skillTask;
		// cancels the skill hit scheduled task
		if(future != null)
			future.cancel(mayInterruptIfRunning);
		_skillTask = null;
		future = _skillLaunchedTask;
		if(future != null)
			future.cancel(mayInterruptIfRunning);
		_skillLaunchedTask = null;
	}

	public boolean absorbAndReflect(final L2Character target, final L2Skill skill, double damage)
	{
		if(target.isDead())
			return false;

		final boolean bow = getActiveWeaponItem() != null && (getActiveWeaponItem().getItemType() == WeaponType.BOW || getActiveWeaponItem().getItemType() == WeaponType.CROSSBOW);

		double value = 0;
		
		final double _finHealth = target.getCurrentCp() + target.getCurrentHp();
		
		if(skill != null && skill.isMagic())
			value = target.calcStat(Stats.REFLECT_AND_BLOCK_MSKILL_DAMAGE_CHANCE, 0, this, skill);
		else if(skill != null && skill.getCastRange() <= 200)
			value = target.calcStat(Stats.REFLECT_AND_BLOCK_PSKILL_DAMAGE_CHANCE, 0, this, skill);
		else if(skill == null && !bow)
			value = target.calcStat(Stats.REFLECT_AND_BLOCK_DAMAGE_CHANCE, 0, this, null);

		// высчитываем шанс
		if(Rnd.chance(value) && _finHealth - damage > 0)
		{
			applyReflectDamage(target, skill, damage, 100.);
			return true;
		}

		if(skill != null && skill.isMagic())
			value = target.calcStat(Stats.REFLECT_MSKILL_DAMAGE_PERCENT, 0, this, skill);
		else if(skill != null && skill.getCastRange() <= 200)
			value = target.calcStat(Stats.REFLECT_PSKILL_DAMAGE_PERCENT, 0, this, skill);
		else if(skill == null && !bow)
			value = target.calcStat(Stats.REFLECT_DAMAGE_PERCENT, 0, this, null);

		if(value > 0 && _finHealth - damage > 0)
			applyReflectDamage(target, skill, damage, value);

		if(skill != null || bow)
			return false;

		// вампирик
		damage = (int) (damage - target.getCurrentCp());

		if(damage <= 0)
			return false;

		double absorb = calcStat(Stats.ABSORB_DAMAGE_PERCENT, 0, target, null);
		if(absorb > 0 && !target.isDoor())
		{
			// считаем количество HP которое вернётся от урона
			absorb *= damage * Config.ALT_ABSORB_DAMAGE_MODIFIER / 100;
			// проверка на лимиты
			absorb = Math.max(0, Math.min(absorb, calcStat(Stats.HP_LIMIT, 100, null, null) * getMaxHp() / 100. - _currentHp));
			if(absorb > 0)
				setCurrentHp(_currentHp + absorb, false);
		}

		absorb = calcStat(Stats.ABSORB_DAMAGEMP_PERCENT, 0, target, null);
		if(absorb > 0 && !target.isHealHPBlocked(true))
		{
			// считаем количество MP которое вернётся от урона
			absorb *= damage * Config.ALT_ABSORB_DAMAGE_MODIFIER / 100;
			// проверка на лимиты
			absorb = Math.max(0, Math.min(absorb, calcStat(Stats.MP_LIMIT, 100, null, null) * getMaxMp() / 100. - _currentMp));
			if(absorb > 0)
				setCurrentMp(_currentMp + absorb);
		}

		return false;
	}

	private void applyReflectDamage(final L2Character target, final L2Skill skill, final double damage, final double reflect)
	{
		double rdmg = damage * reflect / 100.;
		rdmg = Math.min(rdmg, target.getCurrentHp());
		if(isPlayable() && !target.isNpc())
		{
			//to test it
			if(skill == null)
				useActionSkill(null, this, target, TriggerActionType.UNDER_ATTACK, damage);
			else
				useActionSkill(skill, this, target, TriggerActionType.UNDER_OFFENSIVE_SKILL_ATTACK, damage);
			reduceCurrentHp(rdmg, this, null, true, true, false, false, false);
		}
		else
		{
			if(skill == null)
				useActionSkill(null, this, target, TriggerActionType.UNDER_ATTACK, damage);
			else
				useActionSkill(skill, this, target, TriggerActionType.UNDER_OFFENSIVE_SKILL_ATTACK, damage);
			reduceCurrentHp(rdmg, target, null, true, true, false, false, false);
		}

		if(target.isPlayer() && rdmg >= 1.)
			target.sendPacket(new SystemMessage(SystemMessage.S1_HAS_GIVEN_S2_DAMAGE_OF_S3).addName(target).addName(this).addNumber((long) rdmg));
	}

	/**
	 * Add a skill to the L2Character _skills and its Func objects to the calculator set of the L2Character.<BR>
	 * <BR>
	 * <B><U> Concept</U> :</B><BR>
	 * <BR>
	 * All skills own by a L2Character are identified in <B>_skills</B><BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Replace oldSkill by newSkill or Add the newSkill</li> <li>If an old skill has been replaced, remove all its Func objects of L2Character calculator set</li> <li>Add Func objects of newSkill to the calculator set of the L2Character</li> <BR>
	 * <BR>
	 * <B><U> Overriden in </U> :</B><BR>
	 * <BR>
	 * <li>L2Player : Save update in the character_skills table of the database</li> <BR>
	 * <BR>
	 *
	 * @param newSkill
	 *            The L2Skill to add to the L2Character
	 * @return The L2Skill replaced or null if just added a new L2Skill
	 */
	public L2Skill addSkill(final L2Skill newSkill)
	{
		if(newSkill == null)
			return null;

		final L2Skill oldSkill = _skills.get(newSkill.getId());

		if(oldSkill != null && oldSkill.getLevel() == newSkill.getLevel())
			return newSkill;

		// Replace oldSkill by newSkill or Add the newSkill
		_skills.put(newSkill.getId(), newSkill);

		// Добавляем скилы которые срабатывают по действию
		if(newSkill.isOnAction())
			addTriggerableSkill(newSkill);

		// If an old skill has been replaced, remove all its Func objects
		if(oldSkill != null)
			removeStatsOwner(oldSkill);

		// Add Func objects of newSkill to the calculator set of the L2Character
		addStatFuncs(newSkill.getStatFuncs(this));

		return oldSkill;
	}

	public final void addStatFunc(final Func f)
	{
		if(f == null)
			return;

		final int stat = f._stat.ordinal();
		synchronized (_calculators)
		{
			// Select the Calculator of the affected state in the Calculator set
			if(_calculators[stat] == null)
				_calculators[stat] = new Calculator(f._stat, this);

			// Add the Func to the calculator corresponding to the state
			_calculators[stat].addFunc(f);
		}
	}

	public final void addStatFuncs(final Func[] funcs)
	{
		for(final Func f : funcs)
			addStatFunc(f);
	}

	public final boolean isStatusListersEmpty()
	{
		statusListenerLock.lock();
		try
		{
			return _statusListeners == null || _statusListeners.isEmpty();
		}
		finally
		{
			statusListenerLock.unlock();
		}
	}

	public void broadcastToStatusListeners(final L2GameServerPacket... packets)
	{
		if(!isVisible() || packets.length == 0)
			return;

		statusListenerLock.lock();
		try
		{
			if(_statusListeners == null || _statusListeners.isEmpty())
				return;

			L2Character player;
			for(int i = 0; i < _statusListeners.size(); i++)
			{
				player = _statusListeners.getUnsafe(i);
				player.sendPacket(packets);
			}
		}
		finally
		{
			statusListenerLock.unlock();
		}
	}

	public void addStatusListener(final L2Character object)
	{
		if(object == this)
			return;
		statusListenerLock.lock();
		try
		{
			if(_statusListeners == null)
				_statusListeners = new GArray<L2Character>();
			_statusListeners.add(object);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Character.addStatusListener(L2Character): ReentrantLock error", e);
		}
		finally
		{
			statusListenerLock.unlock();
		}
	}

	public void removeStatusListener(final L2Character object)
	{
		statusListenerLock.lock();
		try
		{
			if(_statusListeners == null)
				return;
			_statusListeners.remove(object);
			if(_statusListeners.isEmpty())
				_statusListeners = null;
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Character.removeStatusListener(L2Character): ReentrantLock error", e);
		}
		finally
		{
			statusListenerLock.unlock();
		}
	}

	public void clearStatusListeners()
	{
		statusListenerLock.lock();
		try
		{
			if(_statusListeners == null)
				return;
			_statusListeners.clear();
		}
		finally
		{
			statusListenerLock.unlock();
		}
	}

	public void altOnMagicUseTimer(final L2Character aimingTarget, final L2Skill skill)
	{
		if(isAlikeDead())
			return;

		final int magicId = skill.getDisplayId();
		final int level = Math.max(1, getSkillDisplayLevel(skill.getId()));
		final L2Character[] targets = skill.getTargets(this, aimingTarget, true);
		broadcastPacket(new MagicSkillLaunched(_objectId, magicId, level, skill.isOffensive(), targets));
		final double mpConsume2 = skill.getMpConsumeAll();
		if(mpConsume2 > 0)
		{
			if(_currentMp < mpConsume2)
			{
				sendPacket(Msg.NOT_ENOUGH_MP);
				return;
			}
			if(skill.isMagic())
				reduceCurrentMp(calcStat(Stats.MP_MAGIC_SKILL_CONSUME, mpConsume2, aimingTarget, skill), null);
			else
				reduceCurrentMp(calcStat(Stats.MP_PHYSICAL_SKILL_CONSUME, mpConsume2, aimingTarget, skill), null);
		}
		callSkill(skill, false, targets);
	}

	public void altUseSkill(final L2Skill skill, L2Character target)
	{
		if(skill == null)
			return;

		if(isSkillDisabled(skill))
		{
			sendReuseMessage(skill);
			return;
		}
		if(target == null)
		{
			target = skill.getAimingTarget(this, getTarget());
			if(target == null)
				return;
		}

		final int itemConsume[] = skill.getItemConsumeCounts();
		if(itemConsume.length > 0)
		{
			final int max = itemConsume.length;
			for(int i = max; i-- > 0;)
				if(!consumeItem(skill.getItemConsumeIDs()[i], itemConsume[i]))
				{
					sendPacket(Msg.INCORRECT_ITEM_COUNT);
					sendChanges();
					return;
				}
		}

		if(skill.getSoulsConsume() > getConsumedSouls())
		{
			sendPacket(Msg.THERE_IS_NOT_ENOUGHT_SOUL);
			return;
		}

		fireMethodInvoked(MethodCollection.onStartAltCast, new Object[] { skill, target });

		if(skill.getSoulsConsume() > 0)
			setConsumedSouls(getConsumedSouls() - skill.getSoulsConsume(), null);

		final int magicId = skill.getId();
		final int level = Math.max(1, getSkillDisplayLevel(magicId));

		Formulas.calcSkillMastery(skill, this);
		final int reuseDelay = Formulas.calcSkillReuseDelay(this, skill);

		if(!skill.isToggle())
			broadcastPacket(new MagicSkillUse(this, target, skill.getDisplayId(), level, skill.getHitTime(), reuseDelay));

		// Не показывать сообщение для хербов и кубиков
		if(!(skill.getId() >= 4049 && skill.getId() <= 4055 || skill.getId() >= 4164 && skill.getId() <= 4166 || skill.getId() >= 2278 && skill.getId() <= 2285 || skill.getId() == 5115 || skill.getId() == 5116 && skill.getId() != 2580))
			if(!skill.isHandler())
				sendPacket(new SystemMessage(SystemMessage.USE_S1).addSkillName(magicId, level));
			else
				sendPacket(new SystemMessage(SystemMessage.USE_S1).addItemName(skill.getItemConsumeIDs()[0]));
		// Skill reuse check
		if(reuseDelay > 10)
		{
			disableItem(skill, reuseDelay, reuseDelay);
			disableSkill(skill, reuseDelay);
		}

		// не стартуем отдельный таск если маленькое время
		if(skill.getHitTime() > 200)
			L2GameThreadPools.getInstance().scheduleAi(new AltMagicUseTask(this, target, skill), skill.getHitTime(), isPlayable());
		else if(target != null && skill != null)
			altOnMagicUseTimer(target, skill);
	}

	/** Break a cast and send Server->Client ActionFailed packet and a System Message to the L2Character. */
	public void breakCast(final boolean force)
	{
		if(isCastingNow() && (force || canAbortCast()))
			abortCast();
	}

	/**
	 * Send a reuse message to the player.<BR>
	 *
	 * @param skill
	 *            - скил для отправки сообщения о времени отката
	 */
	public void sendReuseMessage(final L2Skill skill)
	{
		if(isPet() || isSummon())
		{
			final L2Player player = getPlayer();
			if(player != null && isSkillDisabled(skill))
				player.sendPacket(Msg.THAT_PET_SERVITOR_SKILL_CANNOT_BE_USED_BECAUSE_IT_IS_RECHARGING);
		}
	}

	/**
	 * Send a packet to the L2Character AND to all L2Player in the _KnownPlayers of the L2Character.<BR>
	 * <BR>
	 * <B><U> Concept</U> :</B><BR>
	 * <BR>
	 * L2Player in the detection area of the L2Character are identified in <B>_knownPlayers</B>. In order to inform other players of state modification on the L2Character, server just need to go through _knownPlayers to send Server->Client Packet<BR>
	 * <BR>
	 */
	public void broadcastPacket(final L2GameServerPacket... packets)
	{
		sendPacket(packets);
		broadcastPacketToOthers(packets);
	}

	/**
	 * Send a packet to all L2Player in the _KnownPlayers of the L2Character.<BR>
	 * <BR>
	 * <B><U> Concept</U> :</B><BR>
	 * <BR>
	 * L2Player in the detection area of the L2Character are identified in <B>_knownPlayers</B>. In order to inform other players of state modification on the L2Character, server just need to go through _knownPlayers to send Server->Client Packet<BR>
	 * <BR>
	 * <FONT COLOR=#FF0000><B> <U>Caution</U> : This method DOESN'T SEND Server->Client packet to this L2Character (to do this use function broadcastPacket)</B></FONT><BR>
	 * <BR>
	 */
	public final void broadcastPacketToOthers(final L2GameServerPacket... packets)
	{
		if(!isVisible() || packets.length == 0)
			return;

		GArray<L2GameServerPacket> packetsNoBuffs = null;
		for(final L2GameServerPacket packet : packets)
		{
			if(!SkillUtil.notShowPacket(packet))
				continue;
			packetsNoBuffs = new GArray<L2GameServerPacket>(packets.length);
			break;
		}

		if(packetsNoBuffs != null)
			for(final L2GameServerPacket packet : packets)
				if(SkillUtil.notShowPacket(packet))
					packetsNoBuffs.add(packet);

		for(final L2Player target : L2World.getAroundPlayers(this))
			if(target != null && _objectId != target.getObjectId())
				if(packetsNoBuffs != null && target.isNotShowBuffAnim())
					target.sendPackets(packetsNoBuffs);
				else
					target.sendPacket(packets);
	}

	public StatusUpdate makeStatusUpdate(final int... fields)
	{
		final StatusUpdate su = new StatusUpdate(getObjectId(), fields.length);
		for(final int field : fields)
			switch (field)
			{
				case StatusUpdate.CUR_HP:
					su.addAttribute(field, (int) getCurrentHp());
					break;
				case StatusUpdate.MAX_HP:
					su.addAttribute(field, getMaxHp());
					break;
				case StatusUpdate.CUR_MP:
					su.addAttribute(field, (int) getCurrentMp());
					break;
				case StatusUpdate.MAX_MP:
					su.addAttribute(field, getMaxMp());
					break;
				case StatusUpdate.KARMA:
					su.addAttribute(field, getKarma());
					break;
				case StatusUpdate.CUR_CP:
					su.addAttribute(field, (int) getCurrentCp());
					break;
				case StatusUpdate.MAX_CP:
					su.addAttribute(field, getMaxCp());
					break;
				default:
					System.out.println("unknown StatusUpdate field: " + field);
					Thread.dumpStack();
			}
		return su;
	}

	/**
	 * Send the Server->Client packet StatusUpdate with current HP and MP to all other L2Player to inform.<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Create the Server->Client packet StatusUpdate with current HP and MP</li> <li>Send the Server->Client packet StatusUpdate with current HP and MP to all L2Character called _statusListener that must be informed of HP/MP updates of this
	 * L2Character</li> <BR>
	 * <BR>
	 * <FONT COLOR=#FF0000><B> <U>Caution</U> : This method DOESN'T SEND CP information</B></FONT><BR>
	 * <BR>
	 * <B><U> Overriden in </U> :</B><BR>
	 * <BR>
	 * <li>L2Player : Send current HP,MP and CP to the L2Player and only current HP, MP and Level to all other L2Player of the Party</li> <BR>
	 * <BR>
	 */
	public void broadcastStatusUpdate()
	{
		if(isStatusListersEmpty() || !needStatusUpdate())
			return;

		final StatusUpdate su = makeStatusUpdate(StatusUpdate.MAX_HP, StatusUpdate.MAX_MP, StatusUpdate.CUR_HP, StatusUpdate.CUR_MP);
		broadcastToStatusListeners(su);
	}

	public int calcHeading(final Location dest)
	{
		if(dest == null)
			return 0;
		if(Math.abs(getX() - dest.x) == 0 && Math.abs(getY() - dest.y) == 0)
			return _heading;
		return calcHeading(dest.x, dest.y);
	}

	public int calcHeading(final int x_dest, final int y_dest)
	{
		return (int) (Math.atan2(getY() - y_dest, getX() - x_dest) * HEADINGS_IN_PI) + 32768;
	}

	/**
	 * Calculate the new value of the state with modifiers that will be applied on the targeted L2Character.<BR>
	 * <BR>
	 * <B><U> Concept</U> :</B><BR>
	 * <BR>
	 * A L2Character owns a table of Calculators called <B>_calculators</B>. Each Calculator (a calculator per state) own a table of Func object. A Func object is a mathematic function that permit to calculate the modifier of a state (ex :
	 * REGENERATE_HP_RATE...) : <BR>
	 * <BR>
	 * FuncAtkAccuracy -> Math.sqrt(_player.getDEX())*6+_player.getLevel()<BR>
	 * <BR>
	 * When the calc method of a calculator is launched, each mathematic function is called according to its priority <B>_order</B>. Indeed, Func with lowest priority order is executed firsta and Funcs with the same order are executed in unspecified
	 * order. The result of the calculation is stored in the value property of an Env class instance.<BR>
	 * <BR>
	 *
	 * @param stat
	 *            The stat to calculate the new value with modifiers
	 * @param init
	 *            The initial value of the stat before applying modifiers
	 * @param object
	 *            The L2Charcater whose properties will be used in the calculation (ex : CON, INT...)
	 * @param skill
	 *            The L2Skill whose properties will be used in the calculation (ex : Level...)
	 */
	public final double calcStat(final Stats stat, final double init, final L2Character object, final L2Skill skill)
	{
		final int id = stat.ordinal();
		final Calculator c = _calculators[id];
		// If no Func object found, no modifier is applied
		if(c == null || c.size() == 0)
			return init;

		// Create and init an Env object to pass parameters to the Calculator
		final Env env = new Env();
		env.character = this;
		env.target = object;
		env.skill = skill;
		env.value = init;
		// Launch the calculation
		c.calc(env);

		// avoid some troubles with negative stats (some stats should never be negative)
		if(env.value <= 0 && stat != null)
			switch (stat)
			{
				case MAX_HP:
				case MAX_MP:
				case MAX_CP:
				case MAGIC_DEFENCE:
				case POWER_DEFENCE:
				case POWER_ATTACK:
				case MAGIC_ATTACK:
				case POWER_ATTACK_SPEED:
				case MAGIC_ATTACK_SPEED:
				case SHIELD_DEFENCE:
				case STAT_CON:
				case STAT_DEX:
				case STAT_INT:
				case STAT_MEN:
				case STAT_STR:
				case STAT_WIT:
					env.value = 1;
			}
		else if(env.value < 0 && !(stat == Stats.FIRE_RECEPTIVE || stat == Stats.WATER_RECEPTIVE || stat == Stats.WIND_RECEPTIVE || stat == Stats.EARTH_RECEPTIVE || stat == Stats.SACRED_RECEPTIVE || stat == Stats.UNHOLY_RECEPTIVE))
			env.value = 0;

		return env.value;
	}

	/**
	 * Return the Attack Speed of the L2Character (delay (in milliseconds) before next attack).<BR>
	 * <BR>
	 */
	public int calculateAttackDelay()
	{
		return Formulas.calcPAtkSpd(getPAtkSpd());
	}
	protected volatile CharacterListenerList _listeners = CharacterListenerList.emptyList();
	public CharacterListenerList getListeners()
	{
		return _listeners;
	}

	public void createListenerList()
	{
		if(_listeners.isEmptyList())
			synchronized (this)
			{
				if(_listeners.isEmptyList())
					_listeners = new CharacterListenerList(this);
			}
	}


	public final boolean addListener(final ICharacterListener listener)
	{
		createListenerList();
		return getListeners().addListener(listener);
	}


	public final boolean removeListener(final ICharacterListener listener)
	{
		return getListeners().removeListener(listener);
	}

	/**
	 * Launch the magic skill and calculate its effects on each target contained in the targets table.<BR>
	 * <BR>
	 *
	 * @param skill
	 *            The L2Skill to use
	 * @param targets
	 *            The table of L2Object targets
	 */
	public void callSkill(final L2Skill skill, final boolean useActionSkills, final L2Character... targets)
	{
		try
		{
			if(useActionSkills && !skill.isUsingWhileCasting() && !isTriggerSkillsEmpty())
			{
				if(skill.isOffensive())
				{
					if(skill.isMagic())
					{
						final ConcurrentLinkedQueue<TriggerSkill> skillsOnMagicAttack = getTriggerableSkillsByType(TriggerActionType.OFFENSIVE_MAGICAL_SKILL_USE);
						if(skillsOnMagicAttack != null)
							for(final TriggerSkill sk : skillsOnMagicAttack)
								if(Rnd.chance(sk.getChanceForAction(TriggerActionType.OFFENSIVE_MAGICAL_SKILL_USE)) && sk.checkCondition(this, sk.getAimingTarget(this, getTarget()), false, false, true))
								{
									SkillUtil.broadcastUseAnimation(sk.skill, this, targets);
									Formulas.calcSkillMastery(sk.skill, this);
									callSkill(sk.skill, false, targets);
								}
					}
					else
					{
						final ConcurrentLinkedQueue<TriggerSkill> skillsOnSkillAttack = getTriggerableSkillsByType(TriggerActionType.OFFENSIVE_PHYSICAL_SKILL_USE);
						if(skillsOnSkillAttack != null)
							for(final TriggerSkill sk : skillsOnSkillAttack)
								if(Rnd.chance(sk.getChanceForAction(TriggerActionType.OFFENSIVE_PHYSICAL_SKILL_USE)) && sk.checkCondition(this, sk.getAimingTarget(this, getTarget()), false, false, true))
								{
									SkillUtil.broadcastUseAnimation(sk.skill, this, targets);
									Formulas.calcSkillMastery(sk.skill, this);
									callSkill(sk.skill, false, targets);
								}
					}

					for(final L2Character target : targets)
						useActionSkill(skill, target, this, TriggerActionType.UNDER_OFFENSIVE_SKILL_ATTACK, 0);
				}
				else if(skill.isMagic())
				{
					final ConcurrentLinkedQueue<TriggerSkill> skillsOnMagicSupport = getTriggerableSkillsByType(TriggerActionType.SUPPORT_MAGICAL_SKILL_USE);
					if(skillsOnMagicSupport != null)
						for(final TriggerSkill sk : skillsOnMagicSupport)
							if(Rnd.chance(sk.getChanceForAction(TriggerActionType.SUPPORT_MAGICAL_SKILL_USE)) && sk.checkCondition(this, sk.getAimingTarget(this, getTarget()), false, false, true))
							{
								SkillUtil.broadcastUseAnimation(sk.skill, this, targets);
								Formulas.calcSkillMastery(sk.skill, this);
								callSkill(sk.skill, false, targets);
							}
				}

				if(isPlayer())
				{
					final L2Player pl = (L2Player) this;
					L2NpcInstance npc;
					GArray<QuestState> ql;
					for(final L2Character target : targets)
						if(target != null && target.isNpc())
						{
							npc = (L2NpcInstance) target;
							ql = pl.getQuestsForEvent(npc, QuestEventType.MOB_TARGETED_BY_SKILL);
							if(ql != null)
								for(final QuestState qs : ql)
									qs.getQuest().notifySkillUse(npc, skill, qs);
						}
				}
			}

			if(skill.getNegateSkill() > 0)
				for(final L2Character target : targets)
					for(final L2Effect e : target.getEffectList().getAllEffects())
					{
						if(e == null)
							continue;

						final L2Skill efs = e.getSkill();
						if(efs != null && efs.getId() == skill.getNegateSkill() && efs.isCancelable() && efs.getPower() <= skill.getNegatePower())
							e.exit();
					}
			if(skill.getCancelTarget() > 0)
				for(final L2Character target : targets)
					if(Rnd.chance(skill.getCancelTarget()))
					{
						if(target.getCastingSkill() != null && (target.getCastingSkill().getSkillType() == SkillType.TAKECASTLE || target.getCastingSkill().getSkillType() == SkillType.TAKEFORT || target.getCastingSkill().getSkillType() == SkillType.TAKEFLAG))
							continue;

						if(!target.isRaid())
						{
							target.stopMove(true);
							target.abortAttack();
							target.breakCast(true);
							target.setTarget(null);
						}
					}

			if(skill.isSkillInterrupt())
				for(final L2Character target : targets)
					if(!target.isRaid())
					{
						if(target.getCastingSkill() != null && !target.getCastingSkill().isMagic())
							target.abortCast();
						target.abortAttack();
					}

			if(skill.isOffensive())
				startAttackStanceTask();

			// любой активный скил талисмана должен потреблять 6 единиц энергии самого талисмана
			if(useActionSkills && skill.isTalismanSkill())
			{
				final L2ItemInstance talisman = getActiveTalisman(skill);
				if(talisman != null)
					talisman.setLifeTimeRemaining(getPlayer(), talisman.getLifeTimeRemaining() - 6);
			}

			if(Config.ALT_ALLOW_MOBS_USE_UD)
				if(skill.isOffensive() && skill.getCastRange() > 200 && Rnd.chance(Config.ALT_MOBS_UD_USE_CHANCE))
					for(final L2Character target : targets)
						if(target.getAI() != null)
							target.getAI().useNpcUltimateDefense();

			// проверка для скилов, которые не дают эффекта если подходящих целей нету
			if(!skill.isNoEffectWOTargets() || targets.length > 0)
				skill.getEffects(this, this, false, true);
			skill.useSkill(this, targets);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "callSkill error: skill = " + skill + ", cater = " + this, e);
		}
	}

	/**
	 * Return True if the cast of the L2Character can be aborted.<BR>
	 * <BR>
	 */
	public final boolean canAbortCast()
	{
		return _castInterruptTime > System.currentTimeMillis();
	}

	public boolean checkBlockedStat(final Stats stat)
	{
		return _blockedStats != null && _blockedStats.contains(stat);
	}

	public boolean checkReflectSkill(final L2Character attacker, final L2Skill skill)
	{
		if(isInvul() || attacker.isInvul() || !skill.isOffensive()) // Не отражаем, если есть неуязвимость, иначе она может отмениться
			return false;

		// Из магических скилов отражаются только скилы наносящие урон по ХП.
		if(skill.isMagic() && skill.getSkillType() != SkillType.MDAM)
			return false;

		if(Rnd.chance(calcStat(skill.isMagic() ? Stats.REFLECT_MAGIC_SKILL : Stats.REFLECT_PHYSIC_SKILL, 0, attacker, skill)))
		{
			sendPacket(new SystemMessage(SystemMessage.AVOIDED_S1_ATTACK2).addName(attacker));
			attacker.sendPacket(new SystemMessage(SystemMessage.S1_DODGES_ATTACK).addName(this));
			return true;
		}
		return false;
	}

	public void detachAI()
	{
		if(_ai != null)
			_ai.stopAITask();
		_ai = null;
	}

	public final void disableDrop(final int time)
	{
		_dropDisabled = System.currentTimeMillis() + time;
	}

	/**
	 * производит фик атаку
	 *
	 * @param target
	 *            цель для атаки
	 */
	public void doAttack(final L2Character target)
	{
		if(target == null || isAttackingNow() || isAttackMuted() || isAlikeDead() || target.isAlikeDead() || !isInRange(target, 2000))
		{
			sendActionFailed();
			return;
		}

		fireMethodInvoked(MethodCollection.onStartAttack, new Object[] { this, target });

		// Get the Attack Speed of the L2Character (delay (in milliseconds) before next attack)
		final int sAtk = Math.max(calculateAttackDelay(), 333);
		// the hit is calculated to happen halfway to the animation - might need further tuning e.g. in bow case
		final int timeToHit = sAtk / 2;
		int ssGrade = 0;

		int reuse = 0;
		// Get the active weapon item corresponding to the active weapon instance (always equipped in the right hand)
		final L2Weapon weaponItem = getActiveWeaponItem();
		if(weaponItem != null)
		{
			if(isPlayer() && weaponItem.getAttackReuseDelay() > 0)
			{
				// Get the Attack Reuse Delay of the L2Weapon
				reuse = (int) (weaponItem.getAttackReuseDelay() * getReuseModifier(target) * 666 * calcStat(Stats.ATK_BASE, 0, target, null) / 293. / getPAtkSpd());
				if(reuse > 0)
				{
					sendPacket(new SetupGauge(SetupGauge.RED, reuse));
					_attackReuseEndTime = reuse + System.currentTimeMillis() - 75;
					if(reuse > sAtk)
						L2GameThreadPools.getInstance().scheduleAi(new NotifyAITask(this, CtrlEvent.EVT_READY_TO_ACT, null, null), reuse, isPlayable());
				}
			}
			ssGrade = weaponItem.getCrystalType().externalOrdinal;
		}

		_attackEndTime = sAtk + System.currentTimeMillis() + 10000;
		_isAttackAborted = false;

		// Create a Server->Client packet Attack
		final Attack attack = new Attack(this, target, getChargedSoulShot(), ssGrade);

		// Make sure that char is facing selected target
		setHeading(target, true);

		// Select the type of attack to start
		if(weaponItem == null)
			doAttackHitSimple(attack, target, weaponItem, 1., !isPlayer(), timeToHit, false);
		else
			switch (weaponItem.getItemType())
			{
				case BOW:
				case CROSSBOW:
					doAttackHitByBow(attack, target, sAtk, weaponItem);
					break;
				case POLE:
					doAttackHitByPole(attack, target, weaponItem, timeToHit);
					break;
				case DUAL:
				case DUALFIST:
				case DUALDAGGER:
					doAttackHitByDual(attack, target, weaponItem, timeToHit);
					break;
				default:
					doAttackHitSimple(attack, target, weaponItem, 1., true, timeToHit, false);
			}

		// зачем запускать это для луков, если в HitTask'е им всё равно придёт CtrlEvent.EVT_READY_TO_ACT
		if(reuse == 0)
		{
			try
			{
				if(_notifyAct != null)
					_notifyAct.cancel(true);
			}
			catch(final NullPointerException e)
			{}
			_notifyAct = L2GameThreadPools.getInstance().scheduleAi(new NotifyReadyToAct(this), sAtk, isPlayable());
		}

		if(attack.hasHits())
			broadcastPacket(attack);
	}

	/**
	 * Launch a simple attack.<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Calculate if hit is missed or not</li> <li>If hit isn't missed, calculate if shield defense is efficient</li> <li>If hit isn't missed, calculate if hit is critical</li> <li>If hit isn't missed, calculate physical damages</li> <li>Create a
	 * new hit task with Medium priority</li> <li>Add this hit to the Server-Client packet Attack</li> <BR>
	 * <BR>
	 *
	 * @param attack
	 *            Server->Client packet Attack in which the hit will be added
	 * @param target
	 *            The L2Character targeted
	 * @param weapon
	 *            L2Weapon item used by <b>this</b>
	 * @param multiplier
	 *            damage multiplier for polearmers
	 * @return True if the hit isn't missed
	 */
	protected void doAttackHitSimple(final Attack attack, final L2Character target, final L2Weapon weapon, final double multiplier, final boolean unchargeSS, final int timeToHit, final boolean notify)
	{
		int damage1 = 0;
		boolean shld1 = false;
		boolean crit1 = false;

		// Calculate if hit is missed or not
		final boolean miss1 = Formulas.calcHitMiss(this, target);

		// Check if hit isn't missed
		if(!miss1)
		{
			// Calculate physical damages
			final AttackInfo info = Formulas.calcPhysDam(this, target, null, false, false, attack._soulshot, false);
			damage1 = (int) (info.damage * multiplier);
			shld1 = info.shld;
			crit1 = info.crit;

			// FIXME wtf?
			if(Config.ALT_ALLOW_MOBS_USE_UD)
				target.stopEffect(Config.ALT_MOBS_UD_SKILL_ID);

			if(crit1 && weapon != null)
				weapon.getEffect(true, this, target, true);
		}

		// Create a new hit task with Medium priority
		L2GameThreadPools.getInstance().scheduleAi(new HitTask(this, target, damage1, crit1, miss1, attack._soulshot, shld1, unchargeSS, notify), timeToHit, isPlayable());

		// Add this hit to the Server-Client packet Attack
		attack.addHit(target, damage1, miss1, crit1, shld1);
	}

	/**
	 * Launch a Bow attack.<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Calculate if hit is missed or not</li> <li>Consumme arrows</li> <li>If hit isn't missed, calculate if shield defense is efficient</li> <li>If hit isn't missed, calculate if hit is critical</li> <li>If hit isn't missed, calculate physical
	 * damages</li> <li>If the L2Character is a L2Player, Send a Server->Client packet SetupGauge</li> <li>Create a new hit task with Medium priority</li> <li>Calculate and set the disable delay of the bow in function of the Attack Speed</li> <li>Add
	 * this hit to the Server-Client packet Attack</li> <BR>
	 * <BR>
	 *
	 * @param attack
	 *            Server->Client packet Attack in which the hit will be added
	 * @param target
	 *            The L2Character targeted
	 * @param sAtk
	 *            The Attack Speed of the attacker
	 * @param weapon
	 *            L2Weapon item used by this
	 * @return True if the hit isn't missed
	 */
	protected void doAttackHitByBow(final Attack attack, final L2Character target, final int timeToHit, final L2Weapon weapon)
	{
		final L2Weapon activeWeapon = getActiveWeaponItem();
		if(activeWeapon == null)
			return;

		int damage = 0;
		boolean shld1 = false;
		boolean crit1 = false;

		// Calculate if hit is missed or not
		final boolean miss1 = Formulas.calcHitMiss(this, target);
		if(!Config.DONT_DESTROY_ARROWS)
		{
		reduceArrowCount();

		isMoving = false;
		} 
		// в грации дамаг из луков зависит от расстояния до цели
		if(!miss1)
		{
			if(Config.ALT_ALLOW_MOBS_USE_UD)
				if(Rnd.chance(Config.ALT_MOBS_UD_USE_CHANCE) && target.getAI() != null)
					target.getAI().useNpcUltimateDefense();

			final AttackInfo info = Formulas.calcPhysDam(this, target, null, false, false, attack._soulshot, false);
			damage = (int) info.damage;
			shld1 = info.shld;
			crit1 = info.crit;

			final int range = activeWeapon.getAttackRange();
			damage *= Math.min(range, getDistance(target)) / range * .4 + 0.8; // разброс 20% в обе стороны

			if(crit1 && weapon != null)
				weapon.getEffect(true, this, target, true);
		}

		if(isPlayer())
			sendPacket(Msg.GETTING_READY_TO_SHOOT_ARROWS);

		// Create a new hit task with Medium priority
		L2GameThreadPools.getInstance().scheduleAi(new HitTask(this, target, damage, crit1, miss1, attack._soulshot, shld1, true, true), timeToHit, isPlayable());

		attack.addHit(target, damage, miss1, crit1, shld1);
	}

	/**
	 * Launch a Dual attack.<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Calculate if hits are missed or not</li> <li>If hits aren't missed, calculate if shield defense is efficient</li> <li>If hits aren't missed, calculate if hit is critical</li> <li>If hits aren't missed, calculate physical damages</li> <li>
	 * Create 2 new hit tasks with Medium priority</li> <li>Add those hits to the Server-Client packet Attack</li> <BR>
	 * <BR>
	 *
	 * @param attack
	 *            Server->Client packet Attack in which the hit will be added
	 * @param target
	 *            The L2Character targeted
	 * @param weapon
	 *            L2Weapon item used by this
	 * @return True if hit 1 or hit 2 isn't missed
	 */
	protected void doAttackHitByDual(final Attack attack, final L2Character target, final L2Weapon weapon, final int timeToHit)
	{
		int damage1 = 0;
		int damage2 = 0;

		boolean shld1 = false;
		boolean crit1 = false;

		boolean shld2 = false;
		boolean crit2 = false;

		final boolean miss1 = Formulas.calcHitMiss(this, target);
		final boolean miss2 = Formulas.calcHitMiss(this, target);

		if(!miss1)
		{
			final AttackInfo info = Formulas.calcPhysDam(this, target, null, true, false, attack._soulshot, false);
			damage1 = (int) info.damage;
			shld1 = info.shld;
			crit1 = info.crit;

			if(crit1 && weapon != null)
				weapon.getEffect(true, this, target, true);
		}

		if(!miss2)
		{
			final AttackInfo info = Formulas.calcPhysDam(this, target, null, true, false, attack._soulshot, false);
			damage2 = (int) info.damage;
			shld2 = info.shld;
			crit2 = info.crit;

			if(crit2 && weapon != null)
				weapon.getEffect(true, this, target, true);
		}

		// Create a new hit task with Medium priority for hit 1 and for hit 2
		// with a higher delay
		L2GameThreadPools.getInstance().scheduleAi(new HitTask(this, target, damage1, crit1, miss1, attack._soulshot, shld1, true, false), timeToHit / 2, isPlayable());
		L2GameThreadPools.getInstance().scheduleAi(new HitTask(this, target, damage2, crit2, miss2, attack._soulshot, shld2, false, false), timeToHit, isPlayable());

		// Add those hits to the Server-Client packet Attack
		attack.addHit(target, damage1, miss1, crit1, shld1);
		attack.addHit(target, damage2, miss2, crit2, shld2);
	}

	/**
	 * Launch a Pole attack.<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Get all visible objects in a spheric area near the L2Character to obtain possible targets</li> <li>If possible target is the L2Character targeted, launch a simple attack against it</li> <li>If possible target isn't the L2Character targeted
	 * but is attakable, launch a simple attack against it</li> <BR>
	 * <BR>
	 *
	 * @param attack
	 *            Server->Client packet Attack in which the hit will be added
	 * @param weapon
	 *            L2Weapon item used by this
	 * @return True if one hit isn't missed
	 */
	protected void doAttackHitByPole(final Attack attack, final L2Character target, final L2Weapon weapon, final int timeToHit)
	{
		final int angle = (int) calcStat(Stats.POLE_ATTACK_ANGLE, 90, null, null);
		final int range = (int) calcStat(Stats.POWER_ATTACK_RANGE, getTemplate().baseAtkRange, null, null);

		// Используем Math.round т.к. обычный кастинг обрезает к меньшему
		// double d = 2.95. int i = (int)d, выйдет что i = 2
		// если 1% угла или 1 дистанции не играет огромной роли, то для
		// количества целей это критично
		int attackcountmax = (int) Math.round(calcStat(Stats.POLE_TARGERT_COUNT, 3, null, null));

		if(isBoss())
			attackcountmax += 27;
		else if(isRaid())
			attackcountmax += 12;
		else if(isMonster() && getLevel() > 0)
			attackcountmax += getLevel() / 7.5;

		double mult = 0.85;
		int attackcount = 1;

		for(final L2Character t : getAroundCharacters(range, 200))
			if(attackcount <= attackcountmax)
			{
				if(t != null && !t.isDead() && t.isAutoAttackable(this) && !t.isInZonePeace())
				{
					if(t == getAI().getAttackTarget() || !isInFront(t, angle))
						continue;
					doAttackHitSimple(attack, t, weapon, mult, attackcount == 0, timeToHit, false);
					mult *= 0.85;
					attackcount++;
				}
			}
			else
				break;

		doAttackHitSimple(attack, target, weapon, 1., true, timeToHit, false);
	}

	public long getAnimationEndTime()
	{
		return _animationEndTime;
	}

	/**
	 * @param skill
	 *            The L2Skill to use
	 * @param target
	 *            target
	 */
	public void doCast(final L2Skill skill, L2Character target, final boolean forceUse)
	{
		// Прерывать дуэли если цель не дуэлянт
		if(getDuel() != null)
			if(target.getDuel() != getDuel())
				getDuel().setDuelState(getStoredId(), Duel.DuelState.Interrupted);
			else if(isPlayer() && getDuel().getDuelState(getStoredId()) == Duel.DuelState.Interrupted)
			{
				sendPacket(Msg.INCORRECT_TARGET);
				return;
			}

		if(skill == null)
		{
			sendActionFailed();
			return;
		}

		final int itemConsume[] = skill.getItemConsumeCounts();
		if(itemConsume.length > 0)
		{
			final int max = itemConsume.length;
			for(int i = max; i-- > 0;)
				if(!consumeItem(skill.getItemConsumeIDs()[i], itemConsume[i]))
				{
					sendPacket(Msg.INCORRECT_ITEM_COUNT);
					sendChanges();
					return;
				}
		}

		final int magicId = skill.getId();

		if(target == null)
			target = skill.getAimingTarget(this, getTarget());
		if(target == null)
			return;

		fireMethodInvoked(MethodCollection.onStartCast, new Object[] { skill, target, forceUse });

		setHeading(target, true);

		final int level = Math.max(1, getSkillDisplayLevel(magicId));

		int hitTime = skill.isSkillTimePermanent() ? skill.getHitTime() : Formulas.calcMAtkSpd(this, skill, skill.getHitTime());
		int skillInterruptTime = skill.isMagic() ? Formulas.calcMAtkSpd(this, skill, skill.getSkillInterruptTime()) : 0;

		_animationEndTime = System.currentTimeMillis() + hitTime;

		// Only takes 70% of the time to cast a BSpS/SpS cast
		if(skill.isMagic() && !skill.isSkillTimePermanent() && getChargedSpiritShot() > 0)
		{
			hitTime = (int) (0.7 * hitTime);
			skillInterruptTime = (int) (0.7 * skillInterruptTime);
		}

		final int minCastTime = Math.min(Config.SKILLS_CAST_TIME_MIN, skill.getHitTime());
		if(hitTime < minCastTime)
		{
			hitTime = minCastTime;
			skillInterruptTime = 0;
		}

		Formulas.calcSkillMastery(skill, this); // Calculate skill mastery for current cast
		final int reuseDelay = Formulas.calcSkillReuseDelay(this, skill);

		broadcastPacket(new MagicSkillUse(this, target, skill.getDisplayId(), level, hitTime, reuseDelay));

		// Skill reuse check
		disableItem(skill, reuseDelay, reuseDelay);
		disableSkill(skill, reuseDelay);

		// если игрок и скил не Fishing
		if(isPlayer() && magicId != 1312)
			if(!skill.isHandler())
				sendPacket(new SystemMessage(SystemMessage.USE_S1).addSkillName(magicId, level));
			else
				sendPacket(new SystemMessage(SystemMessage.USE_S1).addItemName(skill.getItemConsumeIDs()[0]));

		if(skill.getTargetType() == SkillTargetType.TARGET_HOLY)
			target.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, this, 1);

		double mpConsume1 = skill.isUsingWhileCasting() ? skill.getMpConsumeAll() : skill.getMpInitialConsume();
		final SkillType skillType = skill.getSkillType();

		if(mpConsume1 > 0)
			if(skillType == SkillType.DANCE || skillType == SkillType.SONG)
				mpConsume1 = calcStat(Stats.MP_DANCE_SKILL_CONSUME, mpConsume1, null, skill);
			else if(skill.isMagic())
				reduceCurrentMp(calcStat(Stats.MP_MAGIC_SKILL_CONSUME, mpConsume1, null, skill), null);
			else
				reduceCurrentMp(calcStat(Stats.MP_PHYSICAL_SKILL_CONSUME, mpConsume1, null, skill), null);

		_flyLoc = null;
		if(skill.getFlyType() == FlyType.DUMMY || skill.getFlyType() == FlyType.CHARGE)
		{
			final Location flyLoc = getFlyLocation(target, skill);
			if(flyLoc != null)
			{
				_flyLoc = flyLoc;
				broadcastPacket(new FlyToLocation(this, flyLoc, skill.getFlyType()));
			}
			else
			{
				sendPacket(Msg.CANNOT_SEE_TARGET);
				return;
			}
		}

		_castingSkill = skill;
		_castInterruptTime = System.currentTimeMillis() + skillInterruptTime;
		setCastingTarget(target);

		if(skill.isUsingWhileCasting())
			callSkill(skill, true, skill.getTargets(this, target, forceUse));

		_scheduledCastCount = skill.getHitCounts();
		_scheduledCastInterval = hitTime;

		// Send packet SetupGauge with the color of the gauge and the casting time
		if(isPlayer())
			sendPacket(new SetupGauge(SetupGauge.BLUE, hitTime));

		_skillLaunchedTask = L2GameThreadPools.getInstance().scheduleAi(new MagicLaunchedTask(this, forceUse), skillInterruptTime, isPlayable());
		_skillTask = L2GameThreadPools.getInstance().scheduleAi(new MagicUseTask(this, forceUse), skill.getHitCounts() > 0 ? hitTime * skill.getHitTimings()[0] / 100 : hitTime, isPlayable());
	}

	private Location getFlyLocation(final L2Object target, final L2Skill skill)
	{
		if(target != null && target != this)
		{
			Location loc = target.getLoc();

			final double radian = Util.convertHeadingToRadian(target.getHeading());
			if(skill.isFlyToBack())
				loc = new Location(target.getX() + (int) (Math.sin(radian) * 40.0), target.getY() - (int) (Math.cos(radian) * 40.0), target.getZ());
			else
				loc = new Location(target.getX() - (int) (Math.sin(radian) * 40.0), target.getY() + (int) (Math.cos(radian) * 40.0), target.getZ());
			if(isFlying())
			{
				if(isPlayer() && ((L2Player) this).isInFlyingTransform() && (loc.z <= 0 || loc.z >= 6000))
					return null;
				if(GeoEngine.moveCheckInAir(getX(), getY(), getZ(), loc.x, loc.y, loc.z) == null)
					return null;
			}
			else
			{
				loc.correctGeoZ();
				if(!GeoEngine.canMoveToCoord(getX(), getY(), getZ(), loc.x, loc.y, loc.z))
				{
					loc = target.getLoc();
					if(!GeoEngine.canMoveToCoord(getX(), getY(), getZ(), loc.x, loc.y, loc.z))
						return null;
				}
			}
			return loc;
		}

		final double radian = Util.convertHeadingToRadian(getHeading());
		final int x1 = -(int) (Math.sin(radian) * skill.getFlyRadius());
		final int y1 = (int) (Math.cos(radian) * skill.getFlyRadius());

		if(isFlying())
			return GeoEngine.moveCheckInAir(getX(), getY(), getZ(), getX() + x1, getY() + y1, getZ());
		return GeoEngine.moveCheck(getX(), getY(), getZ(), getX() + x1, getY() + y1);
	}

	public void doDie(final L2Character killer)
	{
		// killing is only possible one time
		dieLock.lock();
		try
		{
			if(_killedAlready)
				return;
			setIsKilledAlready(true);
		}
		finally
		{
			dieLock.unlock();
		}

		fireMethodInvoked(MethodCollection.doDie, new Object[] { killer });

		if(killer != null)
		{
			killer.fireMethodInvoked(MethodCollection.onKill, new Object[] { this });
			if(isPlayer() && killer.isPlayable())
				_currentCp = 0;
		}

		// Set target to null and cancel Attack or Cast
		setTarget(null);
		// Stop movement
		stopMove();

		_currentHp = 0;

		// Stop all active skills effects in progress on the L2Character
		setMassUpdating(true);
		if(isBlessedByNoblesse() || isSalvation())
			getEffectList().stopEffects(EffectList.PROC_STOP_NOBLESSE);
		else
			getEffectList().stopEffects(EffectList.PROC_STOP_AFTER_DEATH);

		setMassUpdating(false);

		sendChanges();
		updateEffectIcons();

		// Send the Server->Client packet StatusUpdate with current HP and MP to
		// all other L2Player to inform
		broadcastStatusUpdate();

		// Notify L2Character AI
		L2GameThreadPools.getInstance().executeGeneral(new NotifyAITask(this, CtrlEvent.EVT_DEAD, killer, null));

		// Обрабатываем смерть в скриптах
		final Iterable<EventScript> scripts = Scripts.getInstance().getEventScripts(ScriptEventType.ON_DIE);
		for(final EventScript script : scripts)
			script.notifyOnDie(this, killer);

		// Обрабатываем смерть в менеджерах Эпиков
		EpicBossManager.getInstance().onDie(this, killer);
	}

	/** Sets HP, MP and CP and revives the L2Character. */
	public void doRevive()
	{
		if(!isTeleporting())
		{
			setIsPendingRevive(false);

			if(isSalvation())
			{
				// Stop Salvation effect
				getEffectList().stopEffects(EffectType.Salvation);
				setCurrentCp(getMaxCp());
				setCurrentHp(getMaxHp(), true);
				setCurrentMp(getMaxMp());
			}
			else
			{
				if(isPlayer() && Config.RESPAWN_RESTORE_CP >= 0)
					setCurrentCp(getMaxCp() * Config.RESPAWN_RESTORE_CP);

				setCurrentHp(getMaxHp() * Config.RESPAWN_RESTORE_HP, true);

				if(Config.RESPAWN_RESTORE_MP >= 0)
					setCurrentMp(getMaxMp() * Config.RESPAWN_RESTORE_MP);
			}

			// Start broadcast status
			broadcastPacket(new Revive(this));
		}
		else
			setIsPendingRevive(true);
	}

	/**
	 * Return a map of 32 bits (0x00000000) containing all abnormal effects
	 */
	public int getAbnormalEffect()
	{
		return _abnormalEffects;
	}

	/**
	 * Return a map of 32 bits (0x00000000) containing all special effects
	 */
	public int getSpecialEffect()
	{
		return _specialEffects;
	}

	/**
	 * Return the Accuracy (base+modifier) of the L2Character in function of the Weapon Expertise Penalty.<BR>
	 * <BR>
	 */
	public int getAccuracy()
	{
		return (int) (calcStat(Stats.ACCURACY_COMBAT, 0, null, null) / getWeaponExpertisePenalty());
	}

	public double getDefenseElementValue(final int defenseAttribute)
	{
		switch (defenseAttribute)
		{
			case Elementals.ATTRIBUTE_FIRE:
				return _template.baseFireDefend;
			case Elementals.ATTRIBUTE_WATER:
				return _template.baseWaterDefend;
			case Elementals.ATTRIBUTE_WIND:
				return _template.baseWindDefend;
			case Elementals.ATTRIBUTE_EARTH:
				return _template.baseEarthDefend;
			case Elementals.ATTRIBUTE_SACRED:
				return _template.baseSacredDefend;
			case Elementals.ATTRIBUTE_UNHOLY:
				return _template.baseUnholyDefend;
			default:
				return 0;
		}
	}

	public int getAttackElementValue(final int attackAttribute)
	{
		switch (attackAttribute)
		{
			case Elementals.ATTRIBUTE_FIRE:
				return _template.baseFireAttack;
			case Elementals.ATTRIBUTE_WATER:
				return _template.baseWaterAttack;
			case Elementals.ATTRIBUTE_WIND:
				return _template.baseWindAttack;
			case Elementals.ATTRIBUTE_EARTH:
				return _template.baseEarthAttack;
			case Elementals.ATTRIBUTE_SACRED:
				return _template.baseSacredAttack;
			case Elementals.ATTRIBUTE_UNHOLY:
				return _template.baseUnholyAttack;
			default:
				return 0;
		}
	}

	/**
	 * Return the L2CharacterAI of the L2Character and if its null create a new one.
	 */
	@Override
	public L2CharacterAI getAI()
	{
		if(_ai == null)
			_ai = new L2CharacterAI(this);
		return _ai;
	}

	/**
	 * Возвращает коллекцию скиллов для быстрого перебора
	 */
	public Collection<L2Skill> getAllSkills()
	{
		return _skills.values();
	}

	/**
	 * Возвращает инстанс талисмана, к которому прикреплён скил
	 */
	public L2ItemInstance getActiveTalisman(final L2Skill skill)
	{
		return null;
	}

	/**
	 * Возвращает массив скиллов для безопасного перебора
	 */
	public final L2Skill[] getAllSkillsArray()
	{
		if(_skills == null)
			return SkillTable.EMPTY_ARRAY;

		final Collection<L2Skill> vals = _skills.values();
		return vals.toArray(new L2Skill[vals.size()]);
	}

	/**
	 * Возвращает отсортированную по id карту скилов
	 */
	public final TreeMap<Integer, L2Skill> getAllSkillsSMap()
	{
		final L2Skill[] skills = getAllSkillsArray();
		final TreeMap<Integer, L2Skill> map = new TreeMap<Integer, L2Skill>();
		for(final L2Skill sk : skills)
			map.put(sk.getId(), sk);
		return map;
	}

	/**
	 * Return the Attack Speed multiplier (base+modifier) of the L2Character to get proper animations.
	 */
	public final float getAttackSpeedMultiplier()
	{
		return (float) (1.1 * getPAtkSpd() / getTemplate().basePAtkSpd);
	}

	public int getBuffLimit()
	{
		return (int) calcStat(Stats.BUFF_LIMIT, Config.ALT_BUFF_LIMIT, null, null);
	}

	public int getDanceSongLimit()
	{
		return (int) calcStat(Stats.DANCE_SONG_LIMIT, Config.ALT_DANCE_SONG_LIMIT, null, null);
	}

	public L2Skill getCastingSkill()
	{
		return _castingSkill;
	}

	/**
	 * If target is L2Character return it, else return null.
	 */
	public final L2Character getCharTarget()
	{
		if(getTarget() == null || !getTarget().isCharacter())
			return null;
		return (L2Character) getTarget();
	}

	/**
	 * Return the COM of the L2Character (base+modifier).
	 */
	public byte getCON()
	{
		return (byte) calcStat(Stats.STAT_CON, _template.baseCON, null, null);
	}

	/**
	 * Возвращает шанс физического крита (1000 == 100%)
	 */
	public int getCriticalHit(final L2Character target, final L2Skill skill)
	{
		return (int) calcStat(Stats.CRITICAL_BASE, _template.baseCritRate, target, skill);
	}

	/**
	 * Возвращает шанс магического крита в процентах (0 - 100)
	 */
	public double getCriticalMagic(final L2Character target, final L2Skill skill)
	{
		double val = calcStat(Stats.MCRITICAL_RATE, 10., target, skill);
		if(Config.LIM_MCRIT > 0 && val > Config.LIM_MCRIT)
			val = Config.LIM_MCRIT;
		return val;
	}

	/**
	 * Return the current CP of the L2Character.
	 */
	public final double getCurrentCp()
	{
		return _currentCp;
	}

	public final double getCurrentCpRatio()
	{
		return getCurrentCp() / getMaxCp();
	}

	public final double getCurrentCpPercents()
	{
		return getCurrentCpRatio() * 100.0;
	}

	public final boolean isCurrentCpFull()
	{
		return !(getCurrentCp() < getMaxCp());
	}

	public final boolean isCurrentCpZero()
	{
		return getCurrentCp() > 1;
	}

	/**
	 * Return the current HP of the L2Character.<BR>
	 * <BR>
	 */
	public final double getCurrentHp()
	{
		return _currentHp;
	}

	public final double getCurrentHpRatio()
	{
		return getCurrentHp() / getMaxHp();
	}

	public final double getCurrentHpPercents()
	{
		return getCurrentHpRatio() * 100.0;
	}

	public final boolean isCurrentHpFull()
	{
		return !(getCurrentHp() < getMaxHp());
	}

	public final boolean isCurrentHpZero()
	{
		return getCurrentHp() > 1;
	}

	/**
	 * Return the current MP of the L2Character.
	 */
	public final double getCurrentMp()
	{
		return _currentMp;
	}

	public final double getCurrentMpRatio()
	{
		return getCurrentMp() / getMaxMp();
	}

	public final double getCurrentMpPercents()
	{
		return getCurrentMpRatio() * 100.0;
	}

	public final boolean isCurrentMpFull()
	{
		return !(getCurrentMp() < getMaxMp());
	}

	public final boolean isCurrentMpZero()
	{
		return getCurrentMp() > 1;
	}

	public Location getDestination()
	{
		return destination;
	}

	/** Return the DEX of the L2Character (base+modifier). */
	public byte getDEX()
	{
		return (byte) calcStat(Stats.STAT_DEX, _template.baseDEX, null, null);
	}

	/** Return the Attack Evasion rate (base+modifier) of the L2Character. */
	public int getEvasionRate(final L2Character target)
	{
		int val = (int) calcStat(Stats.EVASION_RATE, 0, target, null);
		if(Config.LIM_EVASION > 0 && val > Config.LIM_EVASION)
			val = Config.LIM_EVASION;
		return val;
	}

	/**
	 * Return the orientation of the L2Character.
	 */
	@Override
	public final int getHeading()
	{
		if(isAttackingNow() || isCastingNow())
		{
			final L2CharacterAI ai = getAI();
			if(ai != null)
			{
				final L2Character target = ai.getAttackTarget();
				if(target != null)
					setHeading(target, true);
			}
		}
		return _heading;
	}

	/**
	 * Return heading to L2Character<BR>
	 * If <b>boolean toChar</b> is true heading calcs this->target, else target->this.
	 */
	public int getHeadingTo(final L2Object target, final boolean toChar)
	{
		if(target == null || target == this)
			return -1;

		final int dx = target.getX() - getX();
		final int dy = target.getY() - getY();
		int heading = (int) (Math.atan2(-dy, -dx) * 32768. / Math.PI + 32768);

		heading = toChar ? target.getHeading() - heading : _heading - heading;

		if(heading < 0)
			heading += 65536;
		return heading;
	}

	public TargetDirection getDirectionTo(final L2Object target, final boolean toChar)
	{
		final int targeth = getHeadingTo(target, toChar);
		if(targeth == -1)
			return TargetDirection.NONE;
		if(targeth <= 10923 || targeth >= 54613)
			return TargetDirection.BEHIND;
		if(targeth >= 21845 && targeth <= 43691)
			return TargetDirection.FRONT;
		return TargetDirection.SIDE;
	}

	/**
	 * Return the INT of the L2Character (base+modifier).
	 */
	public byte getINT()
	{
		return (byte) calcStat(Stats.STAT_INT, _template.baseINT, null, null);
	}

	public GArray<L2Player> getAroundPlayers(final int radius)
	{
		if(!isVisible())
			return GArray.emptyCollection();
		return L2World.getAroundPlayers(this, radius, 200);
	}

	public GArray<L2Character> getAroundCharacters(final int radius)
	{
		if(!isVisible())
			return GArray.emptyCollection();
		return L2World.getAroundCharacters(this, radius, 200);
	}

	public GArray<L2Character> getAroundCharacters(final int radius, final int height)
	{
		if(!isVisible())
			return GArray.emptyCollection();
		return L2World.getAroundCharacters(this, radius, height);
	}

	public GArray<L2NpcInstance> getAroundNpc(final int range, final int height)
	{
		if(!isVisible())
			return GArray.emptyCollection();
		return L2World.getAroundNpc(this, range, height);
	}

	/**
	 * Return Skill if the skill is known by the L2Character.
	 *
	 * @param skillId
	 *            The identifier of the L2Skill to check the knowledge
	 */
	public final L2Skill getKnownSkill(final int skillId)
	{
		if(_skills == null)
			return null;
		return _skills.get(skillId);
	}

	/**
	 * Return the Magical Attack range (base+modifier) of the L2Character.<BR>
	 * <BR>
	 */
	public final int getMagicalAttackRange(final L2Skill skill)
	{
		if(skill != null)
			return (int) calcStat(Stats.MAGIC_ATTACK_RANGE, skill.getCastRange(), null, skill);
		return getTemplate().baseAtkRange;
	}

	/**
	 * Return the MAtk (base+modifier) of the L2Character for a skill used in function of abnormal effects in progress.<BR>
	 * <BR>
	 * <B><U> Example of use </U> :</B><BR>
	 * <BR>
	 * <li>Calculate Magic damage</li> <BR>
	 * <BR>
	 *
	 * @param target
	 *            The L2Character targeted by the skill
	 * @param skill
	 *            The L2Skill used against the target
	 */
	public int getMAtk(final L2Character target, final L2Skill skill)
	{
		if(skill != null && skill.getMatak() > 0)
			return skill.getMatak();
		return (int) calcStat(Stats.MAGIC_ATTACK, _template.baseMAtk, target, skill);
	}

	/**
	 * Return the MAtk Speed (base+modifier) of the L2Character in function of the Armour Expertise Penalty.<BR>
	 * <BR>
	 */
	public int getMAtkSpd()
	{
		return (int) calcStat(Stats.MAGIC_ATTACK_SPEED, _template.baseMAtkSpd, null, null);
	}

	public double getMAtkSps(final L2Character target, final L2Skill skill)
	{
		final double matk = calcStat(Stats.MAGIC_ATTACK, _template.baseMAtk, target, skill);
		switch (getChargedSpiritShot())
		{
			case 1:
				return matk * 1.41;
			case 2:
				return matk * 2;
			default:
				return matk;
		}
	}

	/**
	 * Return the Max CP (base+modifier) of the L2Character.<BR>
	 * <BR>
	 */
	public int getMaxCp()
	{
		return (int) calcStat(Stats.MAX_CP, _template.baseCpMax, null, null);
	}

	/**
	 * Return the Max HP (base+modifier) of the L2Character.<BR>
	 * <BR>
	 */
	public int getMaxHp()
	{
		return (int) calcStat(Stats.MAX_HP, _template.baseHpMax, null, null);
	}

	/**
	 * Return the Max MP (base+modifier) of the L2Character.<BR>
	 * <BR>
	 */
	public int getMaxMp()
	{
		return (int) calcStat(Stats.MAX_MP, _template.baseMpMax, null, null);
	}

	/**
	 * Return the MDef (base+modifier) of the L2Character against a skill in function of abnormal effects in progress.<BR>
	 * <BR>
	 * <B><U> Example of use </U> :</B><BR>
	 * <BR>
	 * <li>Calculate Magic damage</li> <BR>
	 * <BR>
	 *
	 * @param target
	 *            The L2Character targeted by the skill
	 * @param skill
	 *            The L2Skill used against the target
	 */
	public int getMDef(final L2Character target, final L2Skill skill)
	{
		// Get the base MAtk of the L2Character
		final double defence = _template.baseMDef;

		// Calculate modifiers Magic Attack
		return Math.max((int) calcStat(Stats.MAGIC_DEFENCE, defence, null, skill), 1);
	}

	/**
	 * Return the MEN of the L2Character (base+modifier).<BR>
	 * <BR>
	 */
	public byte getMEN()
	{
		return (byte) calcStat(Stats.STAT_MEN, _template.baseMEN, null, null);
	}

	public float getMinDistance(final L2Object obj)
	{
		float distance = getTemplate().collisionRadius;

		if(obj != null && obj.isCharacter())
			distance += ((L2Character) obj).getTemplate().collisionRadius;

		return distance;
	}

	public float getMovementSpeedMultiplier()
	{
		return getRunSpeed() * 1f / _template.baseRunSpd;
	}

	/**
	 * Return the RunSpeed (base+modifier) or WalkSpeed (base+modifier) of the L2Character in function of the movement type.<BR>
	 * <BR>
	 */
	@Override
	public int getMoveSpeed()
	{
		if(isRunning())
			return getRunSpeed();
		return getWalkSpeed();
	}

	/**
	 * Return the PAtk (base+modifier) of the L2Character.<BR>
	 * <BR>
	 */
	public int getPAtk(final L2Character target)
	{
		return (int) calcStat(Stats.POWER_ATTACK, _template.basePAtk, target, null);
	}

	/**
	 * Return the PAtk Speed (base+modifier) of the L2Character in function of the Armour Expertise Penalty.<BR>
	 * <BR>
	 */
	public int getPAtkSpd()
	{
		return (int) calcStat(Stats.POWER_ATTACK_SPEED, _template.basePAtkSpd, null, null);
	}

	/**
	 * Return the PDef (base+modifier) of the L2Character.<BR>
	 * <BR>
	 */
	public int getPDef(final L2Character target)
	{
		return Math.max((int) calcStat(Stats.POWER_DEFENCE, _template.basePDef, target, null), 1);
	}

	/**
	 * Return the Physical Attack range (base+modifier) of the L2Character.<BR>
	 * <BR>
	 */
	public final int getPhysicalAttackRange()
	{
		return (int) calcStat(Stats.POWER_ATTACK_RANGE, getTemplate().baseAtkRange, null, null);
	}

	/**
	 * Return a Random Damage in function of the weapon.<BR>
	 * <BR>
	 */
	public final int getRandomDamage()
	{
		final L2Weapon weaponItem = getActiveWeaponItem();

		if(weaponItem == null)
			return 5 + (int) Math.sqrt(getLevel());

		return weaponItem.getRandomDamage();
	}

	/**
	 * Return the Skill/Spell reuse modifier.<BR>
	 * <BR>
	 */
	public double getReuseModifier(final L2Character target)
	{
		return calcStat(Stats.ATK_REUSE, 1, target, null);
	}

	/**
	 * @return the RunSpeed of the L2Character.
	 */
	public int getRunSpeed()
	{
		return getSpeed(_template.baseRunSpd);
	}

	/**
	 * @return the ShieldDef rate (base+modifier) of the L2Character.
	 */
	public final int getShldDef()
	{
		if(isPlayer())
			return (int) calcStat(Stats.SHIELD_DEFENCE, 0, null, null);
		return (int) calcStat(Stats.SHIELD_DEFENCE, _template.baseShldDef, null, null);
	}

	/**
	 * Return the level of a skill owned by the L2Character.<BR>
	 * <BR>
	 *
	 * @param skillId
	 *            The identifier of the L2Skill whose level must be returned
	 * @return The level of the L2Skill identified by skillId
	 */
	public final int getSkillLevel(final Integer skillId)
	{
		if(_skills == null)
			return -1;

		final L2Skill skill = _skills.get(skillId);
		if(skill == null)
			return -1;

		return skill.getLevel();
	}

	public final int getSkillLevel(final Integer skillId, final int no_entry_value)
	{
		if(_skills == null)
			return no_entry_value;

		final L2Skill skill = _skills.get(skillId);
		if(skill == null)
			return no_entry_value;

		return skill.getLevel();
	}

	public final int getSkillDisplayLevel(final Integer skillId)
	{
		if(_skills == null)
			return -1;

		final L2Skill skill = _skills.get(skillId);
		if(skill == null)
			return -1;
		return skill.getDisplayLevel();
	}

	public byte getSkillMastery(final int skillId)
	{
		if(_skillMastery == null)
			return 0;
		final Byte mastery = _skillMastery.get(skillId);
		return mastery == null ? 0 : mastery.byteValue();
	}

	public void removeSkillMastery(final int skillId)
	{
		if(_skillMastery != null)
			_skillMastery.remove(skillId);
	}

	/**
	 * Return the Speed (base+modifier) of the L2Character in function of the Armour Expertise and Territories Penalty.<BR>
	 * <BR>
	 */
	public int getSpeed(final int baseSpeed)
	{
		if(isInWater())
			return getSwimSpeed();
		return (int) calcStat(Stats.RUN_SPEED, baseSpeed, null, null);
	}

	/**
	 * Return the STR of the L2Character (base+modifier).<BR>
	 * <BR>
	 */
	public byte getSTR()
	{
		return (byte) calcStat(Stats.STAT_STR, _template.baseSTR, null, null);
	}

	public int getSwimSpeed()
	{
		return (int) calcStat(Stats.RUN_SPEED, Config.SWIMING_SPEED, null, null);
	}

	/**
	 * Return the L2Object targeted or null.<BR>
	 * <BR>
	 */
	public final L2Object getTarget()
	{
		return target.get();
	}

	public L2Character getFollowTarget()
	{
		return followTarget.get();
	}

	public void setFollowTarget(final L2Character target)
	{
		followTarget = target == null ? HardReferences.<L2Character> emptyRef() : target.getRef();
	}

	/**
	 * Return the identifier of the L2Object targeted or -1.<BR>
	 * <BR>
	 */
	public final int getTargetId()
	{
		final L2Object target = getTarget();
		return target == null ? -1 : target.getObjectId();
	}

	/**
	 * Return the template of the L2Character.<BR>
	 * <BR>
	 * <B><U> Concept</U> :</B><BR>
	 * <BR>
	 * Each L2Character owns generic and static properties (ex : all Keltir have the same number of HP...). All of those properties are stored in a different template for each type of L2Character. Each template is loaded once in the server cache
	 * memory (reduce memory use). When a new instance of L2Character is spawned, server just create a link between the instance and the template This link is stored in <B>_template</B><BR>
	 * <BR>
	 */
	public L2CharTemplate getTemplate()
	{
		return _template;
	}

	public L2CharTemplate getBaseTemplate()
	{
		return _baseTemplate;
	}

	/**
	 * Return the WalkSpeed of the L2Character.<BR>
	 * <BR>
	 */
	public final int getWalkSpeed()
	{
		if(isInWater())
			return getSwimSpeed();
		return getSpeed(_template.baseWalkSpd);
	}

	/**
	 * Return the Weapon Expertise Penalty of the L2Character.<BR>
	 * <BR>
	 */
	public float getWeaponExpertisePenalty()
	{
		return 1.f;
	}

	/**
	 * Return the WIT of the L2Character (base+modifier).<BR>
	 * <BR>
	 */
	public byte getWIT()
	{
		return (byte) calcStat(Stats.STAT_WIT, _template.baseWIT, null, null);
	}

	/**
	 * Return True if the L2Character has a L2CharacterAI.<BR>
	 * <BR>
	 */
	@Override
	public boolean hasAI()
	{
		return _ai != null;
	}

	public double headingToRadians(final int heading)
	{
		return (heading - 32768) / HEADINGS_IN_PI;
	}

	/**
	 * Return True if the L2Character is dead or use fake death.<BR>
	 * <BR>
	 */
	public boolean isAlikeDead()
	{
		return _fakeDeath || _currentHp < 1.;
	}

	/**
	 * Return True if the L2Character has aborted its attack.<BR>
	 * <BR>
	 */
	public boolean isAttackAborted()
	{
		return _isAttackAborted;
	}

	/**
	 * Return True if the L2Character is attacking.<BR>
	 * <BR>
	 */
	public final boolean isAttackingNow()
	{
		return _attackEndTime > System.currentTimeMillis();
	}

	public final void setAttackEndTime(final long val)
	{
		_attackEndTime = val;
	}

	/**
	 * Return True if the target is front L2Character and can be seen.<BR>
	 * <BR>
	 * degrees = 0..180, front->sides->back
	 */
	public boolean isInFront(final L2Object target, final int degrees)
	{
		final int head = getHeadingTo(target, false);
		return head <= 32768 * degrees / 180 || head >= 65536 - 32768 * degrees / 180;
	}

	public boolean isToSideOf(final L2Object target)
	{
		if(target != null && target.isCharacter())
		{
			final int head = getHeadingTo(target, true);
			return head != -1 && (head <= 22337 || head >= 43197);
		}

		return false;
	}

	public boolean isToSideOfTarget()
	{
		final L2Object target = getTarget();
		if(target == null)
			return false;

		if(target instanceof L2Character)
			return isToSideOf(target);

		return false;
	}

	/**
	 * Return True if the target is facing the L2Character.
	 */
	public boolean isInFrontOf(final L2Character target)
	{
		if(target == null)
			return false;

		double angleChar, angleTarget, angleDiff;
		final double maxAngleDiff = 45;
		angleTarget = Util.calculateAngleFrom(target, this);
		angleChar = Util.convertHeadingToDegree(target.getHeading());
		angleDiff = angleChar - angleTarget;
		if(angleDiff <= -360 + maxAngleDiff)
			angleDiff += 360;
		if(angleDiff >= 360 - maxAngleDiff)
			angleDiff -= 360;
		if(Math.abs(angleDiff) <= maxAngleDiff)
			return true;
		return false;
	}

	/** Возвращает <b>true</b> если цель стоит лицом к игроку */
	public boolean isInFrontOfTarget()
	{
		final L2Object target = getTarget();
		if(target == null)
			return false;

		if(target.isCharacter())
			return isInFrontOf((L2Character) target);

		return false;
	}

	/**
	 * Return True if the L2Character is behind the target and can't be seen.<BR>
	 * <BR>
	 */
	public boolean isBehindOf(final L2Character target)
	{
		if(target == null)
			return false;

		final int head = getHeadingTo(target, true);
		return head != -1 && (head <= 10430 || head >= 55105);
	}

	/** Возвращает <b>true</b> если L2Character стоит у цели за спиной */
	public boolean isBehindTarget()
	{
		final L2Object target = getTarget();
		if(target == null)
			return false;

		if(target.isCharacter())
			return isBehindOf((L2Character) target);

		return false;
	}

	public final boolean isBlessedByNoblesse()
	{
		return _isBlessedByNoblesse > 0;
	}

	public final boolean isSalvation()
	{
		return _isSalvation > 0;
	}

	public final boolean isBuffImmunity()
	{
		return _buffImmunity > 0;
	}

	public boolean isDebuffImmune()
	{
		return _debuffImmunity.get();
	}

	/**
	 * Return True if the L2Character is dead.<BR>
	 * <BR>
	 */
	public final boolean isDead()
	{
		return _currentHp < 0.5;
	}

	public final boolean isDropDisabled()
	{
		return _dropDisabled > System.currentTimeMillis();
	}

	/**
	 * Return True if the L2Character is flying.<BR>
	 * <BR>
	 */
	@Override
	public final boolean isFlying()
	{
		return _flying;
	}

	/**
	 * Return True if the L2Character is in combat.<BR>
	 * <BR>
	 */
	public boolean isInCombat()
	{
		return _stanceTask != null;
	}

	/**
	 * Check if player is mage
	 */
	public boolean isMageClass()
	{
		return getTemplate().baseMAtk > 3;

	}

	public final boolean isPendingRevive()
	{
		return isDead() && _isPendingRevive;
	}

	/**
	 * Return True if the L2Character is riding.<BR>
	 * <BR>
	 */
	public final boolean isRiding()
	{
		return _riding;
	}

	/**
	 * Return True if the L2Character is running.<BR>
	 * <BR>
	 */
	public final boolean isRunning()
	{
		return _running;
	}

	/**
	 * Disable this skill id for the duration of the delay in milliseconds.
	 *
	 * @param skillId
	 * @param delay
	 *            (seconds * 1000)
	 */
	public void disableSkill(final L2Skill skill, final long delay)
	{
		disableSkill(skill.hashCode(), new SkillTimeStamp(skill, delay));
	}

	public void disableSkill(final int id, final int level, final long delay)
	{
		disableSkill(id * 1023 + level, new SkillTimeStamp(id, level, delay));
	}

	public void disableSkill(final L2Skill skill, final SkillTimeStamp ts)
	{
		disableSkill(skill.hashCode(), ts);
	}

	public void disableSkill(final L2Skill newSkill, final L2Skill oldSkill)
	{
		if(newSkill != null && oldSkill != null && newSkill.getId() == oldSkill.getId())
		{
			if(newSkill.equals(oldSkill))
				return;

			final SkillTimeStamp sts = getSkillReuse(oldSkill);
			if(sts != null && sts.hasNotPassed())
				disableSkill(newSkill, sts);
			enableSkill(oldSkill);
		}
	}

	public void disableSkill(final int skill_hash, final SkillTimeStamp ts)
	{
		if(_skillReuses == null)
			_skillReuses = new ConcurrentHashMap<Integer, SkillTimeStamp>();
		_skillReuses.put(skill_hash, ts);
	}

	public void enableSkill(final L2Skill skill)
	{
		if(_skillReuses == null)
			return;
		_skillReuses.remove(skill.hashCode());
	}

	public boolean isSkillDisabled(final L2Skill skill)
	{
		return isSkillDisabled(skill.hashCode());
	}

	public boolean isSkillDisabled(final int skill_hash)
	{
		if(_skillReuses == null)
			return false;

		final SkillTimeStamp sts = _skillReuses.get(skill_hash);
		if(sts == null)
			return false;
		if(sts.hasNotPassed())
			return true;
		_skillReuses.remove(skill_hash);
		return false;
	}

	public Collection<SkillTimeStamp> getSkillReuses()
	{
		if(_skillReuses == null)
			return CollectionUtils.emptyCollection();
		return _skillReuses.values();
	}

	public SkillTimeStamp getSkillReuse(final L2Skill skill)
	{
		return _skillReuses == null ? null : _skillReuses.get(skill.hashCode());
	}

	public final boolean isTeleporting()
	{
		return _isTeleporting;
	}

	public boolean knowsObject(final L2Object obj)
	{
		return L2World.getAroundObjectById(this, obj.getObjectId()) != null;
	}

	/** ------------------------- Geo Engine Start------------------------------------ */
	// TODO Geo Engine Start 1879

	/**
	 * Возвращает позицию цели, в которой она будет через пол секунды.
	 */
	private final Location getIntersectionPoint(final L2Character target)
	{
		if(!isInFront(target, 90))
			return new Location(target.getX(), target.getY(), target.getZ());
		final double angle = Util.convertHeadingToDegree(target.getHeading()); // угол в градусах
		final double radian = Math.toRadians(angle - 90); // угол в радианах
		final double range = target.getMoveSpeed() / 2; // расстояние, пройденное за 1 секунду, равно скорости. Берем половину.
		return new Location((int) (target.getX() - range * Math.sin(radian)), (int) (target.getY() + range * Math.cos(radian)), target.getZ());
	}

	private final Location applyOffset(final Location point, final int offset)
	{
		if(offset <= 0)
			return point;

		final long dx = point.x - getX();
		final long dy = point.y - getY();
		final long dz = point.z - getZ();

		final double distance = Math.sqrt(dx * dx + dy * dy + dz * dz);
		if(distance <= offset)
		{
			point.set(getX(), getY(), getZ());
			return point;
		}

		if(distance >= 1)
		{
			final double cut = offset / distance;
			point.x -= (int) (dx * cut + 0.5f);
			point.y -= (int) (dy * cut + 0.5f);
			point.z -= (int) (dz * cut + 0.5f);

			if(!isFlying() && !isInVehicle() && !isSwimming() && !isVehicle())
				point.correctGeoZ();
		}

		return point;
	}

	private final List<Location> applyOffset(final List<Location> points, int offset)
	{
		offset = offset >> 4;
		if(offset <= 0)
			return points;

		final long dx = points.get(points.size() - 1).x - points.get(0).x;
		final long dy = points.get(points.size() - 1).y - points.get(0).y;
		final long dz = points.get(points.size() - 1).z - points.get(0).z;

		final double distance = Math.sqrt(dx * dx + dy * dy + dz * dz);
		if(distance <= offset)
		{
			final Location point = points.get(0);
			points.clear();
			points.add(point);
			return points;
		}

		if(distance >= 1)
		{
			final double cut = offset / distance;
			final int num = (int) (points.size() * cut + 0.5f);
			for(int i = 1; i <= num && points.size() > 0; i++)
				points.remove(points.size() - 1);
		}

		return points;
	}

	private final boolean setSimplePath(final Location dest)
	{
		final List<Location> moveList = GeoMove.constructMoveList(getLoc(), dest);
		if(moveList.isEmpty())
			return false;
		_targetRecorder.clear();
		_targetRecorder.add(moveList);
		return true;
	}

	private final boolean buildPathTo(final int x, final int y, final int z, final int offset, final boolean pathFind)
	{
		return buildPathTo(x, y, z, offset, null, false, pathFind);
	}

	public final boolean buildPathTo(final int x, final int y, final int z, final int offset, final L2Character follow, final boolean forestalling, final boolean pathFind)
	{
		Location dest;

		if(forestalling && follow != null && follow.isMoving)
			dest = getIntersectionPoint(follow);
		else
			dest = new Location(x, y, z);

		if(isInVehicle() || isVehicle() || !Config.GEODATA_ENABLED)
		{
			applyOffset(dest, offset);
			return setSimplePath(dest);
		}

		if(isFlying() || isInWater())
		{
			applyOffset(dest, offset);

			Location nextloc;

			if(isFlying())
			{
				if(GeoEngine.canSeeCoord(this, dest.x, dest.y, dest.z, true))
					return setSimplePath(dest);

				nextloc = GeoEngine.moveCheckInAir(getX(), getY(), getZ(), dest.x, dest.y, dest.z);
				if(nextloc != null && !nextloc.equals(getX(), getY(), getZ()))
					return setSimplePath(nextloc);
			}
			else
			{
				final int waterZ = getWaterZ();
				nextloc = GeoEngine.moveInWaterCheck(getX(), getY(), getZ(), dest.x, dest.y, dest.z, waterZ);
				if(nextloc == null)
					return false;

				List<Location> moveList = GeoMove.constructMoveList(getLoc(), nextloc.clone());
				_targetRecorder.clear();
				if(!moveList.isEmpty())
					_targetRecorder.add(moveList);

				final int dz = dest.z - nextloc.z;
				// если пытаемся выбратся на берег, считаем путь с точки выхода до точки назначения
				if(dz > 0 && dz < 128)
				{
					moveList = GeoEngine.MoveList(nextloc.x, nextloc.y, nextloc.z, dest.x, dest.y, false);
					if(moveList != null)
						if(!moveList.isEmpty()) // уже стоим на нужной клетке
							_targetRecorder.add(moveList);
				}

				return !_targetRecorder.isEmpty();
			}
			return false;
		}

		List<Location> moveList = GeoEngine.MoveList(getX(), getY(), getZ(), dest.x, dest.y, true); // onlyFullPath = true - проверяем весь путь до конца
		if(moveList != null) // null - до конца пути дойти нельзя
		{
			if(moveList.isEmpty()) // уже стоим на нужной клетке
				return false;
			applyOffset(moveList, offset);
			if(moveList.isEmpty()) // уже стоим на нужной клетке
				return false;
			_targetRecorder.clear();
			_targetRecorder.add(moveList);
			return true;
		}

		if(pathFind)
		{
			final List<List<Location>> targets = GeoMove.findMovePath(getX(), getY(), getZ(), dest.clone(), this, true);
			if(!targets.isEmpty())
			{
				moveList = targets.remove(targets.size() - 1);
				applyOffset(moveList, offset);
				if(!moveList.isEmpty())
					targets.add(moveList);
				if(!targets.isEmpty())
				{
					_targetRecorder.clear();
					_targetRecorder.addAll(targets);
					return true;
				}
			}
		}

		if(follow != null)
			return false;

		applyOffset(dest, offset);

		moveList = GeoEngine.MoveList(getX(), getY(), getZ(), dest.x, dest.y, false); // onlyFullPath = false - идем до куда можем
		if(moveList != null && !moveList.isEmpty()) // null - нет геодаты, empty - уже стоим на нужной клетке
		{
			_targetRecorder.clear();
			_targetRecorder.add(moveList);
			return true;
		}

		return false;
	}

	public boolean followToCharacter(final L2Character target, int offset, final boolean forestalling)
	{
		synchronized (_targetRecorder)
		{
			if(isMovementDisabled() || target == null || isInVehicle() || isInWater())
				return false;

			offset = Math.max(offset, 10);
			if(isFollow && target == getFollowTarget() && offset == _offset)
				return true;

			if(Math.abs(getZ() - target.getZ()) > 1000 && !isFlying())
			{
				stopMove();
				sendPacket(Msg.CANNOT_SEE_TARGET);
				return false;
			}

			getAI().clearNextAction();

			isFollow = false;
			isMoving = false;
			destination = null;
			moveList = null;
			if(_moveTask != null)
			{
				_moveTask.cancel(false);
				_moveTask = null;
			}
			_targetRecorder.clear();

			if(buildPathTo(target.getX(), target.getY(), target.getZ(), offset, target, forestalling, !target.isDoor()))
				movingDestTempPos.set(target.getX(), target.getY(), target.getZ());
			else
				return false;

			isMoving = true;
			isFollow = true;
			_forestalling = forestalling;
			_offset = offset;
			setFollowTarget(target);

			moveNext(true);

			return true;
		}
	}

	public boolean moveToLocation(final Location loc, final int offset, final boolean pathfinding)
	{
		return moveToLocation(loc.x, loc.y, loc.z, offset, pathfinding);
	}

	/**
	 * Вызывается только для монстров и сервисов
	 *
	 * @param x_dest
	 * @param y_dest
	 * @param z_dest
	 * @param offset
	 * @param pathfinding
	 * @return
	 */
	public boolean moveToLocation(final int x_dest, final int y_dest, final int z_dest, int offset, final boolean pathfinding)
	{
		synchronized (_targetRecorder)
		{
			offset = Math.max(offset, 0);
			final Location dst_geoloc = new Location(x_dest, y_dest, z_dest).world2geo();
			if(isMoving && !isFollow && movingDestTempPos.equals(dst_geoloc))
			{
				sendActionFailed();
				return true;
			}

			if(isMovementDisabled())
			{
				getAI().setNextAction(nextAction.MOVE, new Location(x_dest, y_dest, z_dest), offset, pathfinding, false);
				sendActionFailed();
				return false;
			}

			getAI().clearNextAction();
			if(isPlayer())
				getAI().changeIntention(AI_INTENTION_ACTIVE, null, null);

			isFollow = false;
			isMoving = false;
			destination = null;
			moveList = null;
			if(_moveTask != null)
			{
				_moveTask.cancel(false);
				_moveTask = null;
			}
			_targetRecorder.clear();

			if(buildPathTo(x_dest, y_dest, z_dest, offset, pathfinding))
				movingDestTempPos.set(dst_geoloc);
			else
			{
				sendActionFailed();
				return false;
			}

			isMoving = true;
			moveNext(true);
			return true;
		}
	}

	/**
	 * @param firstMove
	 */
	public final void moveNext(final boolean firstMove)
	{
		if(!isMoving || isMovementDisabled())
		{
			stopMove();
			return;
		}

		_previousSpeed = getMoveSpeed();
		if(_previousSpeed <= 0)
		{
			stopMove();
			return;
		}

		if(!firstMove)
		{
			final Location dest = destination;
			if(dest != null)
				setLoc(dest, true);
		}

		double distance;
		synchronized (_targetRecorder)
		{
			if(_targetRecorder.isEmpty())
			{
				final CtrlEvent ctrlEvent = isFollow ? CtrlEvent.EVT_ARRIVED_TARGET : CtrlEvent.EVT_ARRIVED;
				if(isFollow)
					isFollow = false;
				isMoving = false;
				destination = null;
				moveList = null;
				if(_moveTask != null)
				{
					_moveTask.cancel(false);
					_moveTask = null;
				}
				L2GameThreadPools.getInstance().executeAi(new NotifyAITask(this, ctrlEvent, null, null), isPlayable());
				validateLocation(isPlayer() ? 2 : 1);
				return;
			}

			moveList = _targetRecorder.remove(0);
			final Location begin = moveList.get(0).clone().geo2world();
			final Location end = moveList.get(moveList.size() - 1).clone().geo2world();
			destination = end;
			distance = isFlying() || isInWater() ? begin.distance3D(end) : begin.distance(end); // клиент при передвижении не учитывает поверхность

			if(distance != 0)
				setHeading(calcHeading(destination));
		}

		broadcastMove();
		_startMoveTime = _followTimestamp = System.currentTimeMillis();
		if(_moveTaskRunnable.isEmpty())
			_moveTaskRunnable = new MoveNextTask(this);
		_moveTask = L2GameThreadPools.getInstance().scheduleMove(_moveTaskRunnable.setDist(distance), getMoveTickInterval());
	}

	private final int getMoveTickInterval()
	{
		return (isPlayer() ? 16000 : 32000) / Math.max(getMoveSpeed(), 1);
	}

	private final void broadcastMove()
	{
		if(isAirShip())
			broadcastPacket(new ExMoveToLocationAirShip((L2AirShip) this, getLoc(), getDestination()));
		else if(isShip())
			broadcastPacket(new VehicleDeparture((L2Ship) this));
		else
		{
			validateLocation(isPlayer() ? 2 : 1);
			broadcastPacket(new CharMoveToLocation(this));
		}
	}

	public void runMoveTask()
	{
		_moveTask = L2GameThreadPools.getInstance().scheduleMove(_moveTaskRunnable, getMoveTickInterval());
	}

	public final boolean isForestalling()
	{
		return _forestalling;
	}

	public final long getFollowTimestamp()
	{
		return _followTimestamp;
	}

	public final long getStartMoveTime()
	{
		return _startMoveTime;
	}

	public final void setStartMoveTime(final long startMoveTime)
	{
		_startMoveTime = startMoveTime;
	}

	public final double getPreviousSpeed()
	{
		return _previousSpeed;
	}

	public final void setPreviousSpeed(final double previousSpeed)
	{
		_previousSpeed = previousSpeed;
	}

	public final int getOffset()
	{
		return _offset;
	}

	public final Location getMoveDestPos()
	{
		return movingDestTempPos;
	}

	// TODO Geo Engine End
	// ------------------------- Geo Engine End--------------------------------------

	/**
	 * Останавливает движение и рассылает ValidateLocation
	 */
	public void stopMove()
	{
		stopMove(true, true);
	}

	/**
	 * Останавливает движение и рассылает StopMove
	 *
	 * @param validate
	 *            - рассылать ли ValidateLocation
	 */
	public void stopMove(final boolean validate)
	{
		stopMove(true, validate);
	}

	/**
	 * Останавливает движение
	 *
	 * @param stop
	 *            - рассылать ли StopMove
	 * @param validate
	 *            - рассылать ли ValidateLocation
	 */
	public void stopMove(final boolean stop, final boolean validate)
	{
		if(isMoving)
		{
			synchronized (_targetRecorder)
			{
				isMoving = false;
				destination = null;
				moveList = null;
				if(_moveTask != null)
				{
					_moveTask.cancel(false);
					_moveTask = null;
				}
				_targetRecorder.clear();
			}

			if(validate)
				validateLocation(isPlayer() ? 2 : 1);
			if(stop)
				broadcastPacket(stopMovePacket());
		}
		isFollow = false;
	}

	/**
	 * Returns true if status update should be done, false if not
	 *
	 * @return boolean
	 */
	protected boolean needStatusUpdate()
	{
		if(!isVisible())
			return false;

		boolean result = false;

		int bar;
		bar = (int) (getCurrentHp() * CLIENT_BAR_SIZE / getMaxHp());
		if(bar == 0 || bar != _lastHpBarUpdate)
		{
			_lastHpBarUpdate = bar;
			result = true;
		}

		bar = (int) (getCurrentMp() * CLIENT_BAR_SIZE / getMaxMp());
		if(bar == 0 || bar != _lastMpBarUpdate)
		{
			_lastMpBarUpdate = bar;
			result = true;
		}

		if(isPlayer())
		{
			bar = (int) (getCurrentCp() * CLIENT_BAR_SIZE / getMaxCp());
			if(bar == 0 || bar != _lastCpBarUpdate)
			{
				_lastCpBarUpdate = bar;
				result = true;
			}
		}

		return result;
	}

	/**
	 * Remove the L2Character from the world when the decay task is launched.<BR>
	 * <BR>
	 */
	public void onDecay()
	{
		decayMe();
		fireMethodInvoked(MethodCollection.onDecay, null);
	}

	/**
	 * Manage Forced attack (ctrl + select target).<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>If L2Character or target is in a town area, send a system message TARGET_IN_PEACEZONE a Server->Client packet ActionFailed</li> <li>If target is confused, send a Server->Client packet ActionFailed</li> <li>If L2Character is a
	 * L2ArtefactInstance, send a Server->Client packet ActionFailed</li> <li>Send a Server->Client packet MyTargetSelected to start attack and Notify AI with AI_INTENTION_ATTACK</li> <BR>
	 * <BR>
	 *
	 * @param player
	 *            The L2Player to attack
	 */
	@Override
	public void onForcedAttack(final L2Player player, final boolean shift)
	{
		player.sendPacket(new MyTargetSelected(getObjectId(), player.getLevel() - getLevel()));

		if(!isAttackable(player) || player.isOutOfControl())
		{
			player.sendActionFailed();
			return;
		}

		player.getAI().Attack(this, true, shift);
	}

	public void onHitTimer(final L2Character target, final int damage, final boolean crit, final boolean miss, final boolean soulshot, final boolean shld, final boolean unchargeSS)
	{
		if(isAlikeDead() || target.isDead() || !isInRange(target, 2000))
		{
			sendActionFailed();
			return;
		}

		if(isPlayable() && target.isPlayable() && isInZoneBattle() != target.isInZoneBattle())
		{
			final L2Player player = getPlayer();
			if(player != null)
				player.sendPacket(Msg.INCORRECT_TARGET, Msg.ActionFail);
			return;
		}

		fireMethodInvoked(MethodCollection.onAttacked, new Object[] { this, target, damage, crit, miss, soulshot, shld, unchargeSS });

		final Iterable<EventScript> scripts = Scripts.getInstance().getEventScripts(ScriptEventType.ON_ATTACK);
		for(final EventScript script : scripts)
			script.notifyOnAttack(target, this);

		// If attack isn't aborted, send a message system (critical hit, missed...) to attacker/target if they are L2Player
		if(!isAttackAborted())
		{
			// Ставим ЦП 0 если атакует кто нить с проклятым оружием.
			if(!miss && target.isPlayer() && (isCursedWeaponEquipped() || isHero() && target.isCursedWeaponEquipped()))
				target.setCurrentCp(0);

			if(target.isStunned() && Formulas.calcStunBreak(target, damage, crit))
				target.getEffectList().stopEffects(EffectType.Stun, EffectType.Turner);

			if(isPlayer())
			{
				if(crit)
					sendPacket(new SystemMessage(SystemMessage.S1_HAD_A_CRITICAL_HIT).addName(this));
				if(miss)
					sendPacket(new SystemMessage(SystemMessage.S1S_ATTACK_WENT_ASTRAY).addName(this));
				else
					sendPacket(new SystemMessage(SystemMessage.S1_HAS_GIVEN_S2_DAMAGE_OF_S3).addName(this).addName(target).addNumber(damage));
			}
			else if(this instanceof L2Summon)
				((L2Summon) this).displayHitMessage(target, damage, crit, miss);

			if(target.isPlayer())
			{
				final L2Player enemy = (L2Player) target;

				if(shld && damage > 1)
					enemy.sendPacket(Msg.SHIELD_DEFENSE_HAS_SUCCEEDED);
				else if(shld && damage == 1)
					enemy.sendPacket(Msg.YOUR_EXCELLENT_SHIELD_DEFENSE_WAS_A_SUCCESS);

				if(miss)
					enemy.sendPacket(new SystemMessage(SystemMessage.S1_HAS_EVADED_S2S_ATTACK).addName(target).addName(this));
			}

			// Reduce HP of the target and calculate reflection damage to reduce HP of attacker if necessary
			if(!miss && damage > 0)
			{
				target.reduceCurrentHp(damage, this, null, true, true, false, true, false);
				target.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, this, 1);
				target.getAI().notifyEvent(CtrlEvent.EVT_ATTACKED, this, damage);

				if(!target.isDead())
				{
					// Скиллы, кастуемые при физ атаке
					if(crit)
						useActionSkill(null, this, target, TriggerActionType.CRIT, 0);
					useActionSkill(null, this, target, TriggerActionType.ATTACK, 0);

					// скилы действуюшие только при нанесении урона, не должны срабатвать на цель под инвулом
					if(!target.isInvul())
						useActionSkill(null, target, this, TriggerActionType.UNDER_ATTACK, damage);

					// Проверка на мираж
					if(getTarget() != null && isPlayer() && Rnd.chance(target.calcStat(Stats.CANCEL_TARGET, 0, null, null)))
						setTarget(null);

					// Manage attack or cast break of the target (calculating rate, sending message...)
					if(Formulas.calcCastBreak(target, crit))
						target.breakCast(false);
				}

				if(soulshot && unchargeSS)
					unChargeShots(false);
			}

			if(miss)
				useActionSkill(null, target, this, TriggerActionType.UNDER_MISSED_ATTACK, 0);
		}

		startAttackStanceTask();

		if(checkPvP(target, null))
			startPvPFlag(target);
	}

	private static void useActionSkill(final L2Skill exclude, final L2Character actor, final L2Character target, final TriggerActionType type, final double damage)
	{
		if(actor.isTriggerSkillsEmpty())
			return;

		final ConcurrentLinkedQueue<TriggerSkill> triggerableSkills = actor.getTriggerableSkillsByType(type);
		if(triggerableSkills == null)
			return;

		for(final TriggerSkill skill : triggerableSkills)
			if(skill.skill != exclude && Rnd.chance(skill.getChanceForAction(type)) && (!type.isCheckDamage() || type.isCheckDamage() && damage > skill.getMinDamageForAction(type)))
				useActionSkill(skill, actor, target);
	}

	private static void useActionSkill(final TriggerSkill skill, final L2Character actor, final L2Character target)
	{
		final L2Character aimingTarget = skill.getAimingTarget(actor, target);
		try
		{
			// тут же проверяется реюз скила
			if(skill.checkCondition(actor, aimingTarget, false, false, true))
			{
				final L2Character[] targets = skill.getTargets(actor, aimingTarget, false);
				SkillUtil.broadcastUseAnimation(skill.skill, actor, targets);
				Formulas.calcSkillMastery(skill.skill, actor);

				// FIXME некоторые скилы не имеют триггеров, хотя должны, а просто эффектам прописывается ID скила триггера для отображения. их надо поправить, добавить триггеры, чтоб делать реюз

				// Skill reuse check. Ставим откат скила триггера
				if(skill.isReuse && skill.getReuseDelay() > 0)
					// FIXME ппц затычка, откат должен даваться всем рунам сразу, при срабатывании одной хотя бы
					if(skill.getId() == 755 || skill.getId() == 756 || skill.getId() == 757)
					{
						actor.disableSkill(755, 1, skill.getReuseDelay());
						actor.disableSkill(756, 1, skill.getReuseDelay());
						actor.disableSkill(757, 1, skill.getReuseDelay());
					}
					else
						actor.disableSkill(skill.skill, skill.getReuseDelay());

				actor.callSkill(skill.skill, false, targets);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "useActionSkill: error skill=" + skill + ", actor=" + actor, e);
		}
	}

	public void onMagicUseTimer(final L2Character aimingTarget, final L2Skill skill, boolean forceUse)
	{
		if(skill == null)
		{
			onCastEndTime();
			sendPacket(Msg.ActionFail);
			return;
		}

		_castInterruptTime = 0;

		if(aimingTarget == null)
		{
			onCastEndTime();
			sendPacket(Msg.ActionFail);
			return;
		}

		if(skill.isUsingWhileCasting())
		{
			aimingTarget.getEffectList().stopEffect(skill.getId());
			onCastEndTime();
			return;
		}

		if(!skill.isOffensive() && getAggressionTarget() != null)
			forceUse = true;

		if(!skill.checkCondition(this, aimingTarget, forceUse, false, false))
		{
			onCastEndTime();
			return;
		}

		if(skill.getCastRange() < Short.MAX_VALUE && skill.getSkillType() != SkillType.TAKECASTLE && skill.getSkillType() != SkillType.TAKEFORT && !GeoEngine.canSeeTarget(this, aimingTarget, isFlying()))
		{
			sendPacket(Msg.CANNOT_SEE_TARGET);
			broadcastPacket(new MagicSkillCanceled(_objectId));
			onCastEndTime();
			return;
		}

		int level = getSkillDisplayLevel(skill.getId());
		if(level < 1)
			level = 1;

		final L2Character[] targets = skill.getTargets(this, aimingTarget, forceUse);

		final int hpConsume = skill.getHpConsume();
		if(hpConsume > 0)
			setCurrentHp(Math.max(0, _currentHp - hpConsume), false);

		double mpConsume2 = skill.getMpConsume();
		if(mpConsume2 > 0)
		{
			if(skill.getSkillType() == SkillType.DANCE || skill.getSkillType() == SkillType.SONG)
			{
				int dancecount = getEffectList().getDanceCount();
				dancecount = Math.min(dancecount, 8);
				mpConsume2 += 30 * dancecount;
				mpConsume2 = calcStat(Stats.MP_DANCE_SKILL_CONSUME, mpConsume2, null, skill);
			}
			else if(skill.isMagic())
				mpConsume2 = calcStat(Stats.MP_MAGIC_SKILL_CONSUME, mpConsume2, null, skill);
			else
				mpConsume2 = calcStat(Stats.MP_PHYSICAL_SKILL_CONSUME, mpConsume2, null, skill);

			if(_currentMp < mpConsume2 && isPlayable())
			{
				sendPacket(Msg.NOT_ENOUGH_MP);
				onCastEndTime();
				return;
			}
			reduceCurrentMp(mpConsume2, null);
		}

		callSkill(skill, true, targets);

		if(skill.getNumCharges() > 0)
			setIncreasedForce(getIncreasedForce() - skill.getNumCharges());
		if(skill.isSoulBoost())
			setConsumedSouls(getConsumedSouls() - Math.min(getConsumedSouls(), 5), null);
		else if(skill.getSoulsConsume() > 0)
			setConsumedSouls(getConsumedSouls() - skill.getSoulsConsume(), null);

		Location flyLoc;
		switch (skill.getFlyType())
		{
			case THROW_UP:
			case THROW_HORIZONTAL:
				for(final L2Character target : targets)
				{
					target.setHeading(this, false);
					flyLoc = getFlyLocation(null, skill);
					target.setLoc(flyLoc);
					broadcastPacket(new FlyToLocation(target, flyLoc, skill.getFlyType()));
				}
				break;
			case DUMMY:
			case CHARGE:
				flyLoc = _flyLoc;
				_flyLoc = null;
				if(flyLoc != null)
				{
					broadcastPacket(new FlyToLocation(this, flyLoc, skill.getFlyType()));
					setLoc(flyLoc);
					validateLocation(1);
				}
				break;
		}

		if(isPlayer() && getTarget() != null && skill.isOffensive())
			for(final L2Character target : targets)
				if(Rnd.chance(target.calcStat(Stats.CANCEL_TARGET, 0, aimingTarget, skill)))
				{
					clearCastVars();
					getAI().notifyEvent(CtrlEvent.EVT_FORGET_OBJECT, target);
					return;
				}

		if(_scheduledCastCount > 0)
		{
			final int hitTime = _scheduledCastInterval * skill.getHitTimings()[skill.getHitCounts() - _scheduledCastCount] / 100;
			_skillLaunchedTask = L2GameThreadPools.getInstance().scheduleAi(new MagicLaunchedTask(this, forceUse), hitTime, isPlayable());
			_skillTask = L2GameThreadPools.getInstance().scheduleAi(new MagicUseTask(this, forceUse), hitTime, isPlayable());
			return;
		}

		final int skillCoolTime = Formulas.calcMAtkSpd(this, skill, skill.getCoolTime());
		if(skillCoolTime > 0)
			L2GameThreadPools.getInstance().scheduleAi(new CastEndTimeTask(this), skillCoolTime, isPlayable());
		else
			onCastEndTime();
	}

	public void onCastEndTime()
	{
		final L2Skill skill = _castingSkill;
		clearCastVars();
		getAI().notifyEvent(CtrlEvent.EVT_FINISH_CASTING, skill);
	}

	public void clearCastVars()
	{
		_castingSkill = null;
		_skillTask = null;
		_skillLaunchedTask = null;
		_flyLoc = null;
	}

	/**
	 * Reduce the current HP of the L2Character and launch the doDie Task if necessary.<BR>
	 * <BR>
	 * <B><U> Overriden in </U> :</B><BR>
	 * <BR>
	 * <li>L2NpcInstance : Update the attacker AggroInfo of the L2NpcInstance _aggroList</li> <BR>
	 * <BR>
	 *
	 * @param damage
	 *            The HP decrease value
	 * @param attacker
	 *            The L2Character who attacks
	 * @param skill
	 *            скил
	 * @param awake
	 *            The awake state (If True : stop sleeping)
	 * @param standUp
	 *            If True : stand up
	 * @param directHp
	 *            If True : hit on HP instead of CP first
	 * @param canReflect
	 *            : если true обрабатывать рефлект
	 * @param isCounteAttack
	 *            : если true - значит дамаг идёт с контр атаки
	 */
	public void reduceCurrentHp(double damage, final L2Character attacker, final L2Skill skill, final boolean awake, final boolean standUp, final boolean directHp, final boolean canReflect, final boolean isCounteAttack)
	{
		fireMethodInvoked(MethodCollection.ReduceCurrentHp, new Object[] { damage, attacker, skill, awake, standUp, directHp, });

		if(attacker == null || isDead() || attacker.isDead())
			return;

		if(isInvul() && attacker != this)
		{
			attacker.sendPacket(Msg.ATTACK_WAS_BLOCKED);
			return;
		}

		// 5182 = Blessing of protection, работает если разница уровней больше 10 и не в зоне осады
		if(attacker != null && attacker.isPlayer() && Math.abs(attacker.getLevel() - getLevel()) > 10)
		{
			// ПК не может нанести урон чару с блессингом
			if(attacker.getKarma() > 0 && isProtectionBlessing() && !isInZone(ZoneType.Siege))
				return;
			// чар с блессингом не может нанести урон ПК
			if(getKarma() > 0 && attacker.isProtectionBlessing() && !attacker.isInZone(ZoneType.Siege))
				return;
		}

		if(awake && isSleeping())
			getEffectList().stopEffects(EffectType.Sleep);

		if(isMeditated() && attacker != this)
		{
			final L2Effect effect = getEffectList().getEffectByType(EffectType.Meditation);
			if(effect != null)
				stopEffects(new ForEachEffectStopMeditation(effect.getSkill().getId()));
		}

		if(standUp && isPlayer())
		{
			standUp();
			if(isFakeDeath())
				breakFakeDeath();
		}

		if(attacker != this || skill != null && skill.isOffensive())
		{
			startAttackStanceTask();
			if(isInvisible() && getEffectList().getEffectByType(EffectType.Invisible) != null)
				getEffectList().stopEffects(EffectType.Invisible);
		}

		// вы атакованы физ/маг скилами наносящими урон
		if(canReflect && damage > 0 && skill != null && attacker != null)
			useActionSkill(null, this, attacker, TriggerActionType.UNDER_SKILL_ATTACK, damage);

		if(canReflect && attacker.absorbAndReflect(this, skill, damage))
			return;
		getListeners().onCurrentHpDamage(this,damage, attacker, skill, awake, standUp, directHp);
		if(attacker.isPlayable())
		{
			final L2Playable pAttacker = (L2Playable) attacker;

			// Flag the attacker if it's a L2Player outside a PvP area
			if(!isCounteAttack && !isDead() && pAttacker.checkPvP(this, null))
				pAttacker.startPvPFlag(this);

			if(isMonster() && skill != null && skill.isOverhit())
			{
				final double overhitDmg = (_currentHp - damage) * -1;
				if(overhitDmg <= 0)
				{
					setOverhitDamage(0);
					setOverhitAttacker(null);
				}
				else
				{
					setOverhitDamage(overhitDmg);
					setOverhitAttacker(attacker);
				}
			}

			double ii;
			if(!directHp)
			{
				damage = _currentCp - damage;
				ii = damage;

				if(ii < 0)
					ii *= -1;

				if(damage < 0)
					damage = 0;

				setCurrentCp(damage);
			}
			else
				ii = damage;

			if(_currentCp == 0 || directHp)
			{
				ii = _currentHp - ii;

				if(ii < 0)
					ii = 0;

				if(isNpc())
					pAttacker.addDamage((L2NpcInstance) this, (int) (_currentHp - ii));
				if(!onDieTrigger && ii < 0.5 && !isTriggerSkillsEmpty())
				{
					onDieTrigger = true;
					final ConcurrentLinkedQueue<TriggerSkill> skillsOnDie = getTriggerableSkillsByType(TriggerActionType.DIE);
					if(skillsOnDie != null)
						for(final TriggerSkill sk : skillsOnDie)
							if(Rnd.chance(sk.getChanceForAction(TriggerActionType.DIE)))
								useActionSkill(sk, this, attacker);
					onDieTrigger = false;
				}

				setCurrentHp(ii, false);
			}
		}
		else
		{
			if(_currentHp - damage < 0.5 && !isTriggerSkillsEmpty())
				useActionSkill(null, this, this, TriggerActionType.DIE, 0);

			setCurrentHp(Math.max(_currentHp - damage, 0), false);
		}

		if(isDead())
			doDie(attacker);
	}

	/**
	 * Reduce the current MP of the L2Character.<BR>
	 * <BR>
	 *
	 * @param i
	 *            The MP decrease value
	 * @param attacker
	 *            L2Character
	 */
	public void reduceCurrentMp(double i, final L2Character attacker)
	{
		if(attacker != null && attacker != this)
		{
			if(isSleeping())
				getEffectList().stopEffects(EffectType.Sleep);
			if(isMeditated())
			{
				final L2Effect effect = getEffectList().getEffectByType(EffectType.Meditation);
				if(effect != null)
					stopEffects(new ForEachEffectStopMeditation(effect.getSkill().getId()));
			}
		}

		// 5182 = Blessing of protection, работает если разница уровней больше 10 и не в зоне осады
		if(attacker != null && attacker.isPlayer() && Math.abs(attacker.getLevel() - getLevel()) > 10)
		{
			// ПК не может нанести урон чару с блессингом
			if(attacker.getKarma() > 0 && isProtectionBlessing() && !isInZone(ZoneType.Siege))
				return;
			// чар с блессингом не может нанести урон ПК
			if(getKarma() > 0 && attacker.isProtectionBlessing() && !attacker.isInZone(ZoneType.Siege))
				return;
		}

		i = _currentMp - i;

		if(i < 0)
			i = 0;

		setCurrentMp(i);

		if(attacker != null && attacker != this)
			startAttackStanceTask();
	}

	public double relativeSpeed(final L2Object target)
	{
		return getMoveSpeed() - target.getMoveSpeed() * Math.cos(headingToRadians(getHeading()) - headingToRadians(target.getHeading()));
	}

	public void removeAllSkills()
	{
		for(final L2Skill s : getAllSkillsArray())
			removeSkill(s);
	}

	public void removeBlockStats(final GArray<Stats> stats)
	{
		if(_blockedStats != null)
		{
			_blockedStats.removeAll(stats);
			if(_blockedStats.isEmpty())
				_blockedStats = null;
		}
	}

	public void addBlockStats(final GArray<Stats> stats)
	{
		if(_blockedStats == null)
			_blockedStats = new GArray<Stats>();
		_blockedStats.addAll(stats);
	}

	// ------------------------------ Обработка эффектов --------------------------------------- \\

	public EffectList getEffectList()
	{
		if(_effectList == null)
			_effectList = new EffectList(this);
		return _effectList;
	}

	public void setEffectList(final EffectList el)
	{
		_effectList = el;
	}

	public void stopAllEffects()
	{
		getEffectList().stopAllEffects();
	}

	public void stopEffects(final INgObjectProcedure<L2Effect> proc)
	{
		getEffectList().stopEffects(proc);
	}

	public void stopEffect(final int skillId)
	{
		getEffectList().stopEffect(skillId);
	}

	public void stopEffect(final L2Skill skill)
	{
		getEffectList().stopEffect(skill);
	}

	/**
	 * Remove a skill from the L2Character and its Func objects from calculator set of the L2Character.<BR>
	 * <BR>
	 * <B><U> Concept</U> :</B><BR>
	 * <BR>
	 * All skills own by a L2Character are identified in <B>_skills</B><BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Remove the skill from the L2Character _skills</li> <li>Remove all its Func objects from the L2Character calculator set</li> <BR>
	 * <BR>
	 * <B><U> Overriden in </U> :</B><BR>
	 * <BR>
	 * <li>L2Player : Save update in the character_skills table of the database</li> <BR>
	 * <BR>
	 *
	 * @param skill
	 *            The L2Skill to remove from the L2Character
	 * @return The L2Skill removed
	 */
	public L2Skill removeSkill(final L2Skill skill)
	{
		if(skill == null)
			return null;

		return removeSkillById(skill.getId());
	}

	public L2Skill removeSkillById(final Integer id)
	{
		// Remove the skill from the L2Character _skills
		final L2Skill oldSkill = _skills.remove(id);

		removeTriggerableSkill(id);

		// Remove all its Func objects from the L2Character calculator set
		if(oldSkill != null)
		{
			removeStatsOwner(oldSkill);
			if(oldSkill.isToggle() || Config.ALT_DELETE_SA_BUFFS && (oldSkill.isItemSkill() || oldSkill.isHandler()))
			{
				// Завершаем все эффекты, принадлежащие старому скиллу
				stopEffect(oldSkill);
				// И с петов тоже
				final L2Summon pet = getPet();
				if(pet != null)
					pet.stopEffect(oldSkill);
			}
		}

		return oldSkill;
	}

	/**
	 * @reutrn _skillsOnAction - список шансовых скилов
	 */
	public ConcurrentLinkedQueue<TriggerSkill> getTriggerableSkillsByType(final TriggerActionType type)
	{
		if(_skillsOnAction == null)
			return null;
		return _skillsOnAction[type.ordinal()];
	}

	public final boolean isTriggerSkillsEmpty()
	{
		if(_skillsOnAction == null)
			return true;
		return false;
	}

	public void addTriggerableSkill(final L2Skill skill)
	{
		addTriggerableSkill(skill, false);
	}

	@SuppressWarnings("unchecked")
	public void addTriggerableSkill(final L2Skill skill, final boolean reuse)
	{
		TriggerSkill ts;
		ConcurrentLinkedQueue<TriggerSkill> skills;
		for(final TriggerActionType actionType : skill.getTriggerActions().keySet())
		{
			if(_skillsOnAction == null)
				_skillsOnAction = new ConcurrentLinkedQueue[TriggerActionType.values().length];

			final int ordinal = actionType.ordinal();
			skills = _skillsOnAction[ordinal];
			if(skills == null)
			{
				skills = new ConcurrentLinkedQueue<TriggerSkill>();
				_skillsOnAction[ordinal] = skills;
			}

			ts = new TriggerSkill(skill, reuse);
			skills.add(ts);

			if(actionType == TriggerActionType.ADD)
				ts.isReuse = true;

			// тут вроде используется уже триггерный скил, по этому добавляем откат
			if(actionType == TriggerActionType.ADD && Rnd.chance(ts.getChanceForAction(TriggerActionType.ADD)))
				useActionSkill(ts, this, this);
		}
	}

	public void removeTriggerableSkill(final int id)
	{
		if(_skillsOnAction != null)
			for(final ConcurrentLinkedQueue<TriggerSkill> skills : _skillsOnAction)
			{
				if(skills == null)
					continue;

				for(final TriggerSkill skill : skills)
					if(skill != null && skill.getId() == id)
						skills.remove(skill);
			}
	}

	public final void removeStatFunc(final Func f)
	{
		if(f == null)
			return;

		// Select the Calculator of the affected state in the Calculator set
		final int stat = f._stat.ordinal();

		synchronized (_calculators)
		{
			if(_calculators[stat] == null)
				return;

			// Remove the Func object from the Calculator
			_calculators[stat].removeFunc(f);

			if(_calculators[stat].size() == 0)
				_calculators[stat] = null;
		}
	}

	public final void removeStatFuncs(final Func[] funcs)
	{
		for(final Func f : funcs)
			removeStatFunc(f);
	}

	public final void removeStatsOwner(final Object owner)
	{
		synchronized (_calculators)
		{
			// Go through the Calculator set
			for(int i = 0; i < _calculators.length; i++)
				if(_calculators[i] != null)
					_calculators[i].removeOwner(owner);// Delete all Func objects of the selected owner
		}
	}

	public void sendActionFailed()
	{
		sendPacket(Msg.ActionFail);
	}

	public L2CharacterAI setAI(final L2CharacterAI new_ai)
	{
		final L2CharacterAI oldAI = getAI();
		if(oldAI != null)
			oldAI.stopAITask();
		if(new_ai == null)
			return _ai = null;
		_ai = new_ai;
		return _ai;
	}

	public final void setCurrentCp(final double newCp)
	{
		setCurrentCp(newCp, true);
	}

	public final void setCurrentCp(double newCp, final boolean sendInfo)
	{
		if(!isPlayer())
			return;

		final int maxCp = getMaxCp();
		newCp = Math.min(maxCp, Math.max(0, newCp));

		if(_currentCp == newCp)
			return;

		_currentCp = newCp;

		if(sendInfo)
		{
			broadcastStatusUpdate();
			sendChanges();

			if(_currentCp < maxCp)
				startRegeneration();
		}
	}

	public final void setCurrentHp(final double newHp, final boolean canRessurect)
	{
		setCurrentHp(newHp, canRessurect, true);
	}

	public final void setCurrentHp(double newHp, final boolean canRessurect, final boolean sendInfo)
	{
		final int maxHp = getMaxHp();
		newHp = Math.min(maxHp, Math.max(0, newHp));

		if(_currentHp == newHp)
			return;

		if(newHp >= 0.5 && isDead() && !canRessurect)
			return;

		final double hpStart = _currentHp;

		dieLock.lock();
		try
		{
			_currentHp = newHp;
			if(!isDead())
			{
				setIsKilledAlready(false);
				setIsKilledAlreadyPlayer(false);
				setIsKilledAlreadyPet(false);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Character.setCurrentHp(double): ReentrantLock error", e);
		}
		finally
		{
			dieLock.unlock();
		}

		firePropertyChanged(PropertyCollection.HitPoints, hpStart, _currentHp);

		if(sendInfo)
		{
			checkHpMessages(hpStart, newHp);
			broadcastStatusUpdate();
			sendChanges();

			if(_currentHp < maxHp)
				startRegeneration();
		}
	}

	/**
	 * Set the current HP and MP of the L2Character, Launch/Stop a HP/MP/CP Regeneration Task and send StatusUpdate packet to all other L2Player to inform (exclusive broadcast).<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Get the Max HP and MP of the L2Character</li> <li>Set the RegenActive flag to True/False</li> <li>Launch/Stop the HP/MP/CP Regeneration task with Medium priority</li> <li>Send the Server->Client packet StatusUpdate with current HP and MP
	 * to all other L2Player to inform</li> <BR>
	 * <BR>
	 * <FONT COLOR=#FF0000><B> <U>Caution</U> : This method DOESN'T SEND Server->Client StatusUpdate packet to the L2Character</B></FONT><BR>
	 * <BR>
	 *
	 * @param newHp
	 *            The new HP value of the L2Player
	 * @param newMp
	 *            The new MP value of the L2Player
	 */
	public void setCurrentHpMp(double newHp, double newMp, final boolean canRessurect)
	{
		newHp = Math.min(getMaxHp(), Math.max(0, newHp));
		newMp = Math.min(getMaxMp(), Math.max(0, newMp));

		if(_currentHp == newHp && _currentMp == newMp)
			return;

		if(newHp >= 0.5 && isDead() && !canRessurect)
			return;

		final double hpStart = _currentHp;

		dieLock.lock();
		try
		{
			_currentHp = newHp;
			_currentMp = newMp;

			if(!isDead())
				setIsKilledAlready(false);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Character.setCurrentHpMp(double, double): ReentrantLock error", e);
		}
		finally
		{
			dieLock.unlock();
		}

		startRegeneration();
		firePropertyChanged(PropertyCollection.HitPoints, hpStart, _currentHp);
		checkHpMessages(hpStart, newHp);
		broadcastStatusUpdate();
	}

	public void setCurrentHpMp(final double newHp, final double newMp)
	{
		setCurrentHpMp(newHp, newMp, false);
	}

	public final void setCurrentMp(final double newMp)
	{
		setCurrentMp(newMp, true);
	}

	public final void setCurrentMp(double newMp, final boolean sendInfo)
	{
		final int maxMp = getMaxMp();
		newMp = Math.min(maxMp, Math.max(0, newMp));

		if(_currentMp == newMp)
			return;

		_currentMp = newMp;

		if(sendInfo)
		{
			broadcastStatusUpdate();
			sendChanges();

			if(_currentMp < maxMp)
				startRegeneration();
		}
	}

	/**
	 * Set the L2Character flying mode to True.<BR>
	 * <BR>
	 */
	public final void setFlying(final boolean mode)
	{
		_flying = mode;
	}

	/**
	 * Set the orientation of the L2Character.<BR>
	 * <BR>
	 */
	@Override
	public final void setHeading(int heading)
	{
		if(heading < 0)
			heading = heading + 1 + Integer.MAX_VALUE & 0xFFFF;
		else if(heading > 0xFFFF)
			heading &= 0xFFFF;
		_heading = heading;
	}

	/**
	 * Set the orientation of the L2Character to the target.<BR>
	 * <BR>
	 */
	public final void setHeading(final L2Character target, final boolean toChar)
	{
		if(target == null || target == this)
			return;

		setHeading(new Location(target.getX(), target.getY(), target.getZ()), toChar); // не менять на getLoc() иначе будет цикл из за getHeading() внутри getLoc()
	}

	public final void setHeading(final Location target, final boolean toChar)
	{
		setHeading((int) (Math.atan2(getY() - target.y, getX() - target.x) * HEADINGS_IN_PI) + (toChar ? 32768 : 0));
	}

	public final void setIsBlessedByNoblesse(final boolean value)
	{
		if(value)
			_isBlessedByNoblesse++;
		else
			_isBlessedByNoblesse--;
	}

	public final void setIsSalvation(final boolean value, final int power)
	{
		if(value)
		{
			_isSalvation++;
			_salvationPower = power;
		}
		else
			_isSalvation--;
	}

	public final void setBuffImmunity(final boolean value)
	{
		if(value)
			_buffImmunity++;
		else
			_buffImmunity--;
	}

	/**
	 * @return предыдущее состояние
	 */
	public boolean startDebuffImmunity()
	{
		return _debuffImmunity.getAndSet(true);
	}

	/**
	 * @return текущее состояние
	 */
	public boolean stopDebuffImmunity()
	{
		return _debuffImmunity.setAndGet(false);
	}

	/**
	 * Set the invulnerability Flag of the L2Character.<BR>
	 */
	public void setInvul(final boolean value)
	{
		_isInvul = value;
	}

	/**
	 * Return True if the L2Player is invulnerable.<BR>
	 * <BR>
	 */
	public boolean isInvul()
	{
		return isInvul(true);
	}

	public boolean isInvul(final boolean stat)
	{
		return _isInvul || stat && calcStat(Stats.INVULNERABLE, 0, null, null) > 0;
	}

	public final void setIsPendingRevive(final boolean value)
	{
		_isPendingRevive = value;
	}

	public final void setIsTeleporting(final boolean value)
	{
		_isTeleporting = value;
	}

	protected void setIsKilledAlready(final boolean value)
	{
		_killedAlready = value;
	}

	protected void setIsKilledAlreadyPlayer(final boolean value)
	{
		if(_killedAlreadyPlayer && !value && Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF && Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF_AFTER_DEATH && this == getPlayer())
			fireMethodInvoked(MethodCollection.AutoBuff, new Object[] { 3 });
		_killedAlreadyPlayer = value;
	}

	protected final void setIsKilledAlreadyPet(final boolean value)
	{
		_killedAlreadyPet = value;
	}

	/**
	 * Get the Name of the L2Character.<BR>
	 */
	@Override
	public String getName()
	{
		return _name == null ? EMPTY_STRING : _name;
	}

	/**
	 * Set the Name of the L2Character.<BR>
	 *
	 * @param name
	 *            The text to set as name
	 */
	public final void setName(final String name)
	{
		_name = name;
	}

	/**
	 * Set the Title of the L2Character for Cursed Weapon.<BR>
	 *
	 * @param title
	 *            The text to set as title
	 */
	public void setVisName(final String name)
	{
		_visname = name;
	}

	public String getVisName()
	{
		return _visname == null ? getName() : _visname;
	}

	/**
	 * Return the Title of the L2Character.<BR>
	 * <BR>
	 */
	public String getTitle()
	{
		if(_title != null && _title.length() > 32)
			return _title.substring(0, 32);
		return _title == null ? EMPTY_STRING : _title;
	}

	/**
	 * Set the Title of the L2Character.<BR>
	 *
	 * @param title
	 *            The text to set as title
	 */
	public void setTitle(final String title)
	{
		_title = title;
	}

	/**
	 * Set the Title of the L2Character for Cursed Weapon.<BR>
	 *
	 * @param title
	 *            The text to set as title
	 */
	public void setVisTitle(final String title)
	{
		_vistitle = title;
	}

	public String getVisTitle()
	{
		return _vistitle == null ? getTitle() : _vistitle;
	}

	public L2Character getCastingTarget()
	{
		return castingTarget.get();
	}

	public void setCastingTarget(final L2Character target)
	{
		castingTarget = target == null ? HardReferences.<L2Character> emptyRef() : target.getRef();
	}

	public void setAggressionTarget(final L2Character target)
	{
		_aggressionTarget = target == null ? HardReferences.<L2Character> emptyRef() : target.getRef();
	}

	public L2Character getAggressionTarget()
	{
		return _aggressionTarget.get();
	}

	/**
	 * Set the L2Character riding mode to True.<BR>
	 * <BR>
	 */
	public final void setRiding(final boolean mode)
	{
		_riding = mode;
	}

	/**
	 * Set the L2Character movement type to run and send Server->Client packet ChangeMoveType to all others L2Player.<BR>
	 * <BR>
	 */
	public final void setRunning()
	{
		if(!_running)
		{
			_running = true;
			broadcastPacket(new ChangeMoveType(this));
		}
	}

	public void setSkillMastery(final int skill, final byte mastery)
	{
		if(_skillMastery == null)
			_skillMastery = new ConcurrentHashMap<Integer, Byte>();
		_skillMastery.put(skill, mastery);
	}

	public void setTarget(L2Object object)
	{
		if(object != null && !object.isVisible())
			object = null;
		// If object==null, Cancel Attak or Cast
		if(object == null)
		{
			if(isAttackingNow() && getAI().getAttackTarget() == getTarget())
			{
				abortAttack();
				if(isPlayer())
					sendPacket(new SystemMessage(SystemMessage.S1S_ATTACK_FAILED).addName(this));
			}
			if(isCastingNow() && getAI().getAttackTarget() == getTarget())
			{
				abortCast();
				if(isPlayer())
					sendPacket(Msg.CASTING_HAS_BEEN_INTERRUPTED);
			}
		}
		target = object == null ? HardReferences.<L2Object> emptyRef() : object.getRef();
	}

	/**
	 * Set the template of the L2Character.<BR>
	 * <BR>
	 * <B><U> Concept</U> :</B><BR>
	 * <BR>
	 * Each L2Character owns generic and static properties (ex : all Keltir have the same number of HP...). All of those properties are stored in a different template for each type of L2Character. Each template is loaded once in the server cache
	 * memory (reduce memory use). When a new instance of L2Character is spawned, server just create a link between the instance and the template This link is stored in <B>_template</B><BR>
	 * <BR>
	 * <B><U> Assert </U> :</B><BR>
	 * <BR>
	 * <li>isCharacter</li> <BR>
	 * <BR
	 */
	protected void setTemplate(final L2CharTemplate template)
	{
		_template = template;
	}

	/**
	 * Set the L2Character movement type to walk and send Server->Client packet ChangeMoveType to all others L2Player.<BR>
	 * <BR>
	 */
	public void setWalking()
	{
		if(_running)
		{
			_running = false;
			broadcastPacket(new ChangeMoveType(this));
		}
	}

	/**
	 * Active abnormal effects flags in the binary mask and send Server->Client UserInfo/CharInfo packet.<BR>
	 * <BR>
	 */
	public void startAbnormalEffect(final AbnormalEffect ae)
	{
		if(ae == AbnormalEffect.NULL)
		{
			_abnormalEffects = AbnormalEffect.NULL.getMask();
			_specialEffects = AbnormalEffect.NULL.getMask();
		}
		else if(ae.isSpecial())
			_specialEffects |= ae.getMask();
		else
			_abnormalEffects |= ae.getMask();
		sendChanges();
	}

	@Override
	public void startAttackStanceTask()
	{
		if(System.currentTimeMillis() < _stanceEndTime + 10000)
			return;

		_stanceEndTime = System.currentTimeMillis();

		// Бесконечной рекурсии не будет, потому что выше проверка на _stanceInited
		if(this instanceof L2Summon && getPlayer() != null)
			getPlayer().startAttackStanceTask();
		else if(isPlayer() && getPet() != null)
			getPet().startAttackStanceTask();

		if(_stanceTask != null)
			_stanceTask.cancel(false);
		else
			broadcastPacket(new AutoAttackStart(getObjectId()));

		_stanceTask = L2GameThreadPools.getInstance().scheduleAi(new CancelAttackStanceTask(this), 15000, isPlayable());
	}

	/**
	 * Modify the abnormal effect map according to the mask.
	 */
	public void stopAbnormalEffect(final AbnormalEffect ae)
	{
		if(ae.isSpecial())
			_specialEffects &= ~ae.getMask();
		else
			_abnormalEffects &= ~ae.getMask();
		sendChanges();
	}

	/**
	 * Остановить регенерацию
	 */
	protected final void stopRegeneration()
	{
		regenLock.lock();
		try
		{
			if(_isRegenerating)
			{
				_isRegenerating = false;

				if(_regenTask != null)
				{
					_regenTask.cancel(false);
					_regenTask = null;
				}
			}
		}
		finally
		{
			regenLock.unlock();
		}
	}

	/**
	 * Запустить регенерацию
	 */
	protected final void startRegeneration()
	{
		if(!isVisible() || isDead() || getRegenTick() == 0L)
			return;

		if(_isRegenerating)
			return;

		regenLock.lock();
		try
		{
			if(!_isRegenerating)
			{
				_isRegenerating = true;
				_regenTask = RegenTaskManager.getInstance().scheduleAtFixedRate(_regenTaskRunnable == null ? _regenTaskRunnable = new RegenTask() : _regenTaskRunnable, 0, getRegenTick());
			}
		}
		finally
		{
			regenLock.unlock();
		}
	}

	public long getRegenTick()
	{
		return 3000L;
	}

	/** HP/MP/CP regeneration */
	private class RegenTask implements Runnable
	{
		@Override
		public void run()
		{
			if(isDead() || isAlikeDead() || getRegenTick() == 0L)
				return;

			final double hpStart = _currentHp;

			final int maxHp = getMaxHp();
			final int maxMp = getMaxMp();
			final int maxCp = isPlayer() ? getMaxCp() : 0;

			boolean startRegeneration = false;

			regenLock.lock();
			try
			{
				if(!isHealMPBlocked(false))
				{
					startRegeneration = true;

					double addMp = 0.;

					if(_currentMp < maxMp)
						addMp += Formulas.calcMpRegen(L2Character.this);

					if(isPlayer() && Config.REGEN_SIT_WAIT)
					{
						final L2Player pl = (L2Player) L2Character.this;
						if(pl.isSitting())
						{
							pl.updateWaitSitTime();
							if(pl.getWaitSitTime() > 5)
								addMp += pl.getWaitSitTime();
						}
					}

					_currentMp += Math.max(0, Math.min(addMp, calcStat(Stats.MP_LIMIT, 100, null, null) * maxMp / 100. - _currentMp));
					_currentMp = Math.min(maxMp, _currentMp);

				}

				if(!isHealHPBlocked(false))
				{
					startRegeneration = true;

					double addHp = 0.;

					if(_currentHp < maxHp)
						addHp += Formulas.calcHpRegen(L2Character.this);

					if(isPlayer() && Config.REGEN_SIT_WAIT)
					{
						final L2Player pl = (L2Player) L2Character.this;
						if(pl.isSitting())
						{
							pl.updateWaitSitTime();
							if(pl.getWaitSitTime() > 5)
								addHp += pl.getWaitSitTime();
						}
					}

					_currentHp += Math.max(0, Math.min(addHp, calcStat(Stats.HP_LIMIT, 100, null, null) * maxHp / 100. - _currentHp));
					_currentHp = Math.min(maxHp, _currentHp);

					if(isPlayer())
					{
						_currentCp += Math.max(0, Math.min(Formulas.calcCpRegen(L2Character.this), calcStat(Stats.CP_LIMIT, 100, null, null) * maxCp / 100. - _currentCp));
						_currentCp = Math.min(maxCp, _currentCp);
					}
				}

				// отрегенились, останавливаем задачу
				if(_currentHp == maxHp && _currentMp == maxMp && _currentCp == maxCp)
					stopRegeneration();
			}
			finally
			{
				regenLock.unlock();
			}

			if(startRegeneration)
				broadcastStatusUpdate();

			// FIXME зачем статы то обновлять постоянно? sendChanges();

			firePropertyChanged(PropertyCollection.HitPoints, hpStart, _currentHp);
			checkHpMessages(hpStart, _currentHp);
		}
	}

	public void block()
	{
		_blocked = true;
	}

	public void unblock()
	{
		_blocked = false;
	}

	public void startConfused()
	{
		_confused = true;

	}

	public void stopConfused()
	{
		if(_confused)
		{
			_confused = false;
			abortAttack();
			breakCast(true);
			stopMove();
			getAI().setAttackTarget(null);
		}
	}

	public void setFakeDeath(final boolean value)
	{
		_fakeDeath = value;
	}

	public void breakFakeDeath()
	{
		getEffectList().stopEffects(EffectType.FakeDeath);
	}

	public void startFear()
	{
		if(!_afraid)
		{
			_afraid = true;
			abortAttack();
			breakCast(true);
			stopMove();
		}
	}

	public void stopFear()
	{
		if(_afraid)
			_afraid = false;
	}

	public boolean isMuted(final L2Skill skill)
	{
		if(skill == null || skill.isNotAffectedByMute())
			return false;
		return isMagicMuted() && skill.isMagic() || isPhysicalMuted() && !skill.isMagic();
	}

	public boolean isMagicMuted()
	{
		return _isMagicMuted > 0;
	}

	public boolean isPhysicalMuted()
	{
		return _isPhysicalMuted > 0;
	}

	public boolean isAttackMuted()
	{
		return _isAttackMuted > 0;
	}

	public void startMuted()
	{
		_isMagicMuted++;
		if(isMagicMuted())
		{
			L2Skill skill = getCastingSkill();
			if(skill != null && skill.isMagic())
				breakCast(false);
		}
	}

	public void stopMuted()
	{
		_isMagicMuted--;
		if(!isMagicMuted())
			_isMagicMuted = 0;
	}

	public void startPMuted()
	{
		_isPhysicalMuted++;
		if(isPhysicalMuted())
		{
			L2Skill skill = getCastingSkill();
			if(skill != null && !skill.isMagic())
				breakCast(false);
		}
	}

	public void stopPMuted()
	{
		_isPhysicalMuted--;
		if(!isPhysicalMuted())
			_isPhysicalMuted = 0;
	}

	public void startAMuted()
	{
		_isAttackMuted++;
		if(isAttackMuted())
			abortAttack();
	}

	public void stopAMuted()
	{
		_isAttackMuted--;
		if(!isAttackMuted())
			_isAttackMuted = 0;
	}

	public void startRooted()
	{
		if(!_rooted)
		{
			_rooted = true;
			stopMove();
		}
	}

	public void stopRooting()
	{
		if(_rooted)
			_rooted = false;
	}

	public void startSleeping()
	{
		if(!_sleeping)
		{
			_sleeping = true;
			abortAttack();
			breakCast(true);
			stopMove();
		}
	}

	public void stopSleeping()
	{
		_sleeping = false;
	}

	public void startStunning()
	{
		if(!_stunned)
		{
			_stunned = true;
			abortAttack();
			breakCast(true);
			stopMove();
		}
	}

	public void stopStunning()
	{
		if(_stunned)
			_stunned = false;
	}

	public void setMeditated(final boolean meditated)
	{
		_meditated = meditated;
	}

	public void setParalyzed(final boolean paralyzed)
	{
		if(_paralyzed != paralyzed)
		{
			_paralyzed = paralyzed;
			if(paralyzed)
			{
				abortAttack();
				breakCast(true);
				stopMove();
			}
		}
	}

	public void setImmobilized(final boolean imobilised)
	{
		if(_imobilised != imobilised)
		{
			_imobilised = imobilised;
			if(imobilised)
				stopMove();
		}
	}

	public void setHealHPBlocked(final boolean value)
	{
		_healHPBlocked = value;
	}

	public void setHealMPBlocked(final boolean value)
	{
		_healMPBlocked = value;
	}

	/**
	 * Set the overloaded status of the L2Character is overloaded (if True, the L2Player can't take more item).<BR>
	 * <BR>
	 */
	public void setOverloaded(final boolean overloaded)
	{
		_overloaded = overloaded;
	}

	public boolean isConfused()
	{
		return _confused;
	}

	public boolean isFakeDeath()
	{
		return _fakeDeath;
	}

	public boolean isAfraid()
	{
		return _afraid;
	}

	public boolean isBlocked()
	{
		return _blocked;
	}

	public boolean isRooted()
	{
		return _rooted;
	}

	public boolean isSleeping()
	{
		return _sleeping;
	}

	public boolean isStunned()
	{
		return _stunned;
	}

	public boolean isMeditated()
	{
		return _meditated;
	}

	public boolean isParalyzed()
	{
		return _paralyzed;
	}

	public boolean isImobilised()
	{
		return _imobilised || getRunSpeed() < 1;
	}

	public boolean isHealHPBlocked(final boolean checkInvul)
	{
		return _healHPBlocked;
	}

	public boolean isHealMPBlocked(final boolean checkInvul)
	{
		return _healMPBlocked;
	}

	/**
	 * Return True if the L2Character is casting.<BR>
	 * <BR>
	 */
	public boolean isCastingNow()
	{
		return _skillTask != null;
	}

	/**
	 * Return True if the L2Character can't move (stun, root, sleep, overload, paralyzed).<BR>
	 * <BR>
	 */
	public boolean isMovementDisabled()
	{
		return isSitting() || isStunned() || isRooted() || isSleeping() || isParalyzed() || isImobilised() || isAlikeDead() || isAttackingNow() || isCastingNow() || _overloaded || _fishing || isPlayer() && (isTeleporting() || ((L2Player) this).isLogoutStarted());
	}

	/**
	 * Return True if the L2Character can't use its skills (ex : stun, sleep...).
	 */
	public boolean isActionsDisabled()
	{
		return isStunned() || isSleeping() || isParalyzed() || isAttackingNow() || isCastingNow() || isAlikeDead() || isPlayer() && (isTeleporting() || ((L2Player) this).isLogoutStarted());
	}

	public boolean isPotionsDisabled()
	{
		return isStunned() || isSleeping() || isParalyzed() || isAlikeDead() || isPlayer() && (isTeleporting() || ((L2Player) this).isLogoutStarted());
	}

	public boolean isToggleDisabled()
	{
		return isStunned() || isSleeping() || isParalyzed() || isPlayer() && (isTeleporting() || ((L2Player) this).isLogoutStarted());
	}

	/**
	 * Return True if the L2Character can't attack (stun, sleep, reuse).<BR>
	 * <BR>
	 */
	public boolean isAttackingDisabled()
	{
		return _attackReuseEndTime > System.currentTimeMillis();
	}

	/**
	 * Return True if the L2Character can't be controlled by the player (confused, affraid).<BR>
	 * <BR>
	 */
	public boolean isOutOfControl()
	{
		return isConfused() || isAfraid() || isBlocked() || isPlayer() && (isTeleporting() || ((L2Player) this).isLogoutStarted());
	}

	public void teleToLocation(final Location loc, final long ref)
	{
		teleToLocation(loc.x, loc.y, loc.z, ref);
	}

	public void teleToLocation(final Location loc)
	{
		teleToLocation(loc.x, loc.y, loc.z, getReflectionId());
	}

	public void teleToLocation(final int x, final int y, final int z)
	{
		teleToLocation(x, y, z, getReflectionId());
	}

	public void teleToClosestTown()
	{
		teleToLocation(MapRegionTable.getTeleToClosestTown(this), 0);
	}

	public void teleToSecondClosestTown()
	{
		teleToLocation(MapRegionTable.getTeleToSecondClosestTown(this), 0);
	}

	public void teleToCastle()
	{
		teleToLocation(MapRegionTable.getTeleToCastle(this), 0);
	}

	public void teleToFortress()
	{
		teleToLocation(MapRegionTable.getTeleToFortress(this), 0);
	}

	public void teleToClanhall()
	{
		teleToLocation(MapRegionTable.getTeleToClanHall(this), 0);
	}

	public void teleToHeadquarter()
	{
		teleToLocation(MapRegionTable.getTeleToHeadquarter(this), 0);
	}

	public void teleToLocation(int x, int y, int z, final long ref)
	{
		if(isFakeDeath())
			breakFakeDeath();

		if(isTeleporting())
			return;

		if(isPlayable())
			clearHateList(true);

		if(!isVehicle() && !isFlying() && !L2World.isWater(new Location(x, y, z)))
			z = GeoEngine.getHeight(x, y, z);

		// проверка - телепорт из рифт зоны означает выход из рифта
		if(isPlayer() && DimensionalRiftManager.getInstance().checkIfInRiftZone(getLoc(), true))
		{
			final L2Player player = (L2Player) this;
			if(player.isInParty() && player.getParty().isInDimensionalRift())
			{
				// получим координаты комнаты ожидания - будем телепортировать туда вместо заданных
				final Location newCoords = DimensionalRiftManager.getInstance().getRoom(0, 0).getTeleportCoords();
				x = newCoords.x;
				y = newCoords.y;
				z = newCoords.z;
				player.getParty().getDimensionalRift().usedTeleport(player);
			}
		}

		setTarget(null);

		if(isPlayer())
		{
			final L2Player player = (L2Player) this;

			if(player.isLogoutStarted())
				return;

			setIsTeleporting(true);

			// remove the object from its old location
			decayMe();

			setXYZInvisible(x, y, z);
			if(ref != getReflection().getId())
				setReflection(ref);

			// Нужно при телепорте с более высокой точки на более низкую, иначе наносится вред от "падения"
			setLastClientPosition(null);
			setLastServerPosition(null);

			player.sendPacket(new TeleportToLocation(player, x, y, z));
		}
		else
		{
			setXYZ(x, y, z);
			broadcastPacket(new TeleportToLocation(this, x, y, z));
		}
	}

	public void onTeleported()
	{
		final L2Player activeChar = (L2Player) this;

		if(activeChar.isFakeDeath())
			activeChar.breakFakeDeath();
		if(activeChar.isInVehicle())
			activeChar.setXYZInvisible(activeChar.getVehicle().getLoc());

		// 15 секунд после телепорта на персонажа не агрятся мобы
		activeChar.setNonAggroTime(System.currentTimeMillis() + 15000);

		spawnMe();

		setLastClientPosition(getLoc());
		setLastServerPosition(getLoc());

		setIsTeleporting(false);

		if(_isPendingRevive)
			doRevive();

		if(activeChar.getTrainedBeast() != null)
			activeChar.getTrainedBeast().setXYZ(getX() + Rnd.get(-100, 100), getY() + Rnd.get(-100, 100), getZ());
		activeChar.checkWaterState();

		sendActionFailed();

		getAI().notifyEvent(CtrlEvent.EVT_TELEPORTED);
		activeChar.sendUserInfo(true);
		activeChar.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);

		// Modify the position of the pet if necessary
		if(activeChar.getPet() != null)
			activeChar.getPet().teleportToOwner();
	}

	public void sendMessage(final CustomMessage message)
	{
		sendMessage(message.toString());
	}

	private long _nonAggroTime;

	public long getNonAggroTime()
	{
		return _nonAggroTime;
	}

	public void setNonAggroTime(final long time)
	{
		_nonAggroTime = time;
	}

	@Override
	public String toString()
	{
		return "Character: '" + _name + "' [" + getObjectId() + "]";
	}

	// ---------------------------- Not Implemented -------------------------------

	public void addExpAndSp(final long addToExp, final long addToSp)
	{}

	public void addExpAndSp(final long addToExp, final long addToSp, final boolean applyBonus, final boolean appyToPet)
	{}

	public void broadcastCharInfo()
	{}

	public void checkHpMessages(final double currentHp, final double newHp)
	{}

	public boolean checkPvP(final L2Character target, final L2Skill skill)
	{
		return false;
	}

	public boolean consumeItem(final int itemConsumeId, final int itemCount)
	{
		return true;
	}

	public void doPickupItem(final L2Object object)
	{}

	/**
	 * @return true если есть иммунитет к Fear
	 */
	public boolean isFearImmune()
	{
		return false;
	}

	/**
	 * @return true если есть иммунитет к летальным атакам
	 */
	public boolean isLethalImmune()
	{
		return getMaxHp() >= 50000;
	}

	/**
	 * @return true если есть иммунитет к параличу
	 */
	public boolean isParalyzeImmune()
	{
		return false;
	}

	/**
	 * @return true если есть иммунитет к параличу
	 */
	public boolean isRootImmune()
	{
		return false;
	}

	/**
	 * @return true если есть иммунитет к агру
	 */
	public boolean isAggressionImmune()
	{
		return false;
	}

	public boolean getChargedSoulShot()
	{
		return false;
	}

	public int getChargedSpiritShot()
	{
		return 0;
	}

	public Duel getDuel()
	{
		return null;
	}

	public int getIncreasedForce()
	{
		return 0;
	}

	public int getConsumedSouls()
	{
		return 0;
	}

	public int getKarma()
	{
		return 0;
	}

	public double getLevelMod()
	{
		return 1;
	}

	public int getNpcId()
	{
		return 0;
	}

	public L2Summon getPet()
	{
		return null;
	}

	public int getPvpFlag()
	{
		return 0;
	}

	/**
	 * @return <font color=blue>1 = Blue</font>, <font color=red>2 = red</font>
	 */
	public int getTeam()
	{
		return 0;
	}

	/**
	 * <font color=blue>1 = Blue</font>, <font color=red>2 = red</font>
	 */
	public void setTeam(final int team, final boolean checksForTeam)
	{}

	public boolean isSitting()
	{
		return false;
	}

	public boolean isUndead()
	{
		return false;
	}

	public boolean isUsingDualWeapon()
	{
		return false;
	}

	public void reduceArrowCount()
	{}

	public void sendChanges()
	{
		getStatsRecorder().sendChanges();
	}

	public void sendMessage(final String message)
	{}

	public void sendPacket(final L2GameServerPacket... mov)
	{}

	public void setIncreasedForce(final int i)
	{}

	public void setConsumedSouls(final int i, final L2NpcInstance monster)
	{}

	public void sitDown()
	{}

	public void standUp()
	{}

	public void startPvPFlag(final L2Character target)
	{}

	public boolean unChargeShots(final boolean spirit)
	{
		return false;
	}

	public void updateEffectIcons()
	{}

	/**
	 * Выставить предельные значения HP/MP/CP и запустить регенерацию, если в этом есть необходимость
	 */
	private final void refreshHpMpCp()
	{
		final int maxHp = getMaxHp();
		final int maxMp = getMaxMp();
		final int maxCp = isPlayer() ? getMaxCp() : 0;

		if(_currentHp > maxHp)
			setCurrentHp(maxHp, false, false);
		if(_currentMp > maxMp)
			setCurrentMp(maxMp, false);
		if(_currentCp > maxCp)
			setCurrentCp(maxCp, false);

		if(_currentHp < maxHp || _currentMp < maxMp || _currentCp < maxCp)
			startRegeneration();
	}

	public void updateStats()
	{
		refreshHpMpCp();
		sendChanges();
	}

	public void callMinionsToAssist(final L2Character attacker)
	{}

	public void setOverhitAttacker(final L2Character attacker)
	{}

	public void setOverhitDamage(final double damage)
	{}

	public boolean hasMinions()
	{
		return false;
	}

	public boolean isCursedWeaponEquipped()
	{
		return false;
	}

	public boolean isHero()
	{
		return false;
	}

	@Override
	public boolean isInVehicle()
	{
		return false;
	}

	public int getAccessLevel()
	{
		return 0;
	}

	public void setFollowStatus(final boolean state, final boolean changeIntention)
	{}

	public void setLastClientPosition(final Location charPosition)
	{}

	public void setLastServerPosition(final Location charPosition)
	{}

	public boolean hasRandomAnimation()
	{
		return true;
	}

	public boolean hasRandomWalk()
	{
		return true;
	}

	public int getClanCrestId()
	{
		return 0;
	}

	public int getClanCrestLargeId()
	{
		return 0;
	}

	public int getAllyCrestId()
	{
		return 0;
	}

	public void disableItem(final L2Skill handler, final long timeTotal, final long timeLeft)
	{}

	public double getRateAdena()
	{
		return 1.0;
	}

	public double getRateItems()
	{
		return 1.0;
	}

	public double getRateExp()
	{
		return 1.0;
	}

	public double getRateSp()
	{
		return 1.0;
	}

	public double getRateSpoil()
	{
		return 1.0;
	}

	public boolean isHiddenName()
	{
		return false;
	}

	public boolean isHalfHeightName()
	{
		return false;
	}

	public boolean isProtectionBlessing()
	{
		return false;
	}

	public void setProtectionBlessing(final boolean value)
	{}

	public void startProtectionBlessing()
	{}

	public void stopProtectionBlessing()
	{}

	// --------------------------- End Of Not Implemented ------------------------------

	@Override
	public float getColRadius()
	{
		return getTemplate().collisionRadius;
	}

	@Override
	public float getColHeight()
	{
		return getTemplate().collisionHeight;
	}

	public boolean canAttackCharacter(final L2Character target)
	{
		return target.getPlayer() != null;
	}

	private ConcurrentHashMap<L2NpcInstance, HateInfo> _hateList = null;

	/**
	 * Добавляет чара в агро лист списку нпс
	 *
	 * @param damage
	 *            - урон
	 * @param aggro
	 *            - агро очки
	 * @param npcs
	 *            - список нпс
	 */
	public void addDamageHate(final int damage, final int aggro, final L2NpcInstance... npcs)
	{
		for(final L2NpcInstance npc : npcs)
			addDamageHate(npc, damage, aggro);
	}

	public void addDamage(final L2NpcInstance npc, final int damage)
	{
		addDamageHate(npc, damage, damage);

		// Добавляем хейта к хозяину саммона
		if(npc.hasAI() && npc.getAI() instanceof DefaultAI && (isSummon() || isPet()) && getPlayer() != null)
			getPlayer().addDamageHate(npc, damage, npc.getTemplate().getAIParams().getBool(AiOptionsType.SEARCHING_MASTER, false) ? damage : 1);
	}

	public void addDamageHate(final L2NpcInstance npc, final int damage, int aggro)
	{
		if(npc == null)
			return;

		if(damage <= 0 && aggro <= 0)
			return;

		if(damage > 0 && aggro <= 0)
			aggro = damage;

		if(_hateList == null)
			_hateList = new ConcurrentHashMap<L2NpcInstance, HateInfo>();

		HateInfo ai = _hateList.get(npc);

		if(ai != null)
		{
			ai.damage += damage;
			ai.hate += aggro;
			ai.hate = Math.max(ai.hate, 0);
		}
		else if(aggro > 0)
		{
			ai = new HateInfo(npc);
			ai.damage = damage;
			ai.hate = aggro;
			_hateList.put(npc, ai);
		}
	}

	public ConcurrentHashMap<L2NpcInstance, HateInfo> getHateList()
	{
		if(_hateList == null)
			return new ConcurrentHashMap<L2NpcInstance, HateInfo>();
		return _hateList;
	}

	public void removeFromHatelist(final L2NpcInstance npc, final boolean onlyHate)
	{
		if(npc != null && _hateList != null)
			if(onlyHate)
			{
				final HateInfo i = _hateList.get(npc);
				if(i != null)
					i.hate = 0;
			}
			else
				_hateList.remove(npc);
	}

	public void clearHateList(final boolean onlyHate)
	{
		if(_hateList != null)
			if(onlyHate)
				for(final HateInfo i : _hateList.values())
					i.hate = 0;
			else
				_hateList = null;
	}

	public boolean isMassUpdating()
	{
		return _massUpdating;
	}

	public void setMassUpdating(final boolean updating)
	{
		_massUpdating = updating;
	}

	public Collection<L2TrapInstance> getTraps()
	{
		if(_traps == null)
			return null;
		final Collection<L2TrapInstance> result = new GArray<L2TrapInstance>(getTrapsCount());
		_traps.forEachEntry(new TIntLongProcedure()
		{
			@Override
			public boolean execute(final int objectId, final long storeId)
			{
				final L2Object trap = L2ObjectsStorage.get(storeId);
				if(trap != null && trap.isTrap())
					result.add((L2TrapInstance) trap);
				else
					_traps.remove(objectId);
				return true;
			}
		});

		return result;
	}

	public int getTrapsCount()
	{
		return _traps == null ? 0 : _traps.size();
	}

	public void addTrap(final L2TrapInstance trap)
	{
		if(_traps == null)
			_traps = new TIntLongHashMap();
		_traps.put(trap.getObjectId(), trap.getStoredId());
	}

	public void removeTrap(final L2TrapInstance trap)
	{
		final TIntLongHashMap traps = _traps;
		if(traps == null || traps.isEmpty())
			return;
		traps.remove(trap.getObjectId());
	}

	public void destroyFirstTrap()
	{
		final TIntLongHashMap traps = _traps;
		if(traps == null || traps.isEmpty())
			return;

		L2Object trap;
		for(final int trapId : traps.keys())
		{
			if((trap = L2ObjectsStorage.get(traps.get(trapId))) != null)
			{
				if(trap instanceof L2TrapInstance)
					((L2TrapInstance) trap).destroy();
				return;
			}
			traps.remove(trapId);
			return;
		}
	}

	public void destroyAllTraps()
	{
		final TIntLongHashMap traps = _traps;
		if(traps == null || traps.isEmpty())
			return;

		_traps.forEachEntry(new TIntLongProcedure()
		{
			@Override
			public boolean execute(final int objectId, final long storeId)
			{
				final L2Object trap = L2ObjectsStorage.get(storeId);
				if(trap != null && trap.isTrap())
					((L2TrapInstance) trap).destroy();
				return true;
			}
		});
	}

	public boolean paralizeOnAttack(final L2Character attacker)
	{
		// Mystic Immunity Makes a target temporarily immune to raid curce
		if(attacker.getEffectList().getFirstEffect(L2Skill.SKILL_MYSTIC_IMMUNITY) != null)
			return false;

		int max_attacker_level = 0xFFFF;

		L2Character leader = this;
		if(isRaid() || isMinion() && (leader = ((L2MinionInstance) this).getLeader()) != null && leader.isRaid())
			max_attacker_level = leader.getLevel() + Config.RAID_MAX_LEVEL_DIFF;
		else if(getAI() instanceof DefaultAI)
		{
			final int max_level_diff = ((DefaultAI) getAI()).getInt(AiOptionsType.PARALIZE_ON_ATTACK, -1000);
			if(max_level_diff != -1000)
				max_attacker_level = leader.getLevel() + max_level_diff;
		}

		if(attacker.getLevel() > max_attacker_level)
		{
			if(max_attacker_level > 0)
				attacker.sendMessage(new CustomMessage("l2n.game.model.L2Character.ParalizeOnAttack", attacker).addCharName(this).addNumber(max_attacker_level));
			return true;
		}

		return false;
	}

	public Calculator[] getCalculators()
	{
		return _calculators;
	}

	@Override
	public void deleteMe()
	{
		setTarget(null);
		stopMove();
		super.deleteMe();

		try
		{
			if(_skillTask != null)
				_skillTask.cancel(true);
			if(_skillLaunchedTask != null)
				_skillLaunchedTask.cancel(true);
			if(_stanceTask != null)
				_stanceTask.cancel(true);
			if(_moveTask != null)
				_moveTask.cancel(true);
		}
		catch(final NullPointerException e)
		{}

		_skillTask = null;
		_skillLaunchedTask = null;
		_stanceTask = null;
		_moveTask = null;
	}

	// --------------------------------- Abstract --------------------------------------

	public abstract byte getLevel();

	public abstract L2ItemInstance getActiveWeaponInstance();

	public abstract L2Weapon getActiveWeaponItem();

	public abstract L2ItemInstance getSecondaryWeaponInstance();

	public abstract L2Weapon getSecondaryWeaponItem();

	// ----------------------------- End Of Abstract -----------------------------------

	public CharStatsChangeRecorder<? extends L2Character> getStatsRecorder()
	{
		if(_statsRecorder == null)
			synchronized (this)
			{
				if(_statsRecorder == null)
					_statsRecorder = new CharStatsChangeRecorder<L2Character>(this);
			}

		return _statsRecorder;
	}

	public void validateLocation(final int broadcast)
	{
		if(isVehicle()) // FIXME для кораблей что-то иное
			return;

		L2GameServerPacket sp;
		if(isInVehicle() && isPlayer())
		{
			if(getPlayer().getVehicle().isAirShip())
				sp = new ExValidateLocationInAirShip(getPlayer());
			else
				sp = new ValidateLocationInVehicle(getPlayer());
		}
		else
			sp = new ValidateLocation(this);

		if(broadcast == 0)
			sendPacket(sp);
		else if(broadcast == 1)
			broadcastPacket(sp);
		else
			broadcastPacketToOthers(sp);
	}

	@Override
	public void setXYZInvisible(final int x, final int y, final int z)
	{
		stopMove();
		super.setXYZInvisible(x, y, z);
	}

	@Override
	public void setLoc(final Location loc)
	{
		setXYZ(loc.x, loc.y, loc.z);
	}

	public void setLoc(final Location loc, final boolean MoveTask)
	{
		setXYZ(loc.x, loc.y, loc.z, MoveTask);
	}

	@Override
	public void setXYZ(final int x, final int y, final int z)
	{
		setXYZ(x, y, z, false);
	}

	public void setXYZ(final int x, final int y, final int z, final boolean MoveTask)
	{
		super.setXYZ(x, y, z);
		updateTerritories();
	}

	@Override
	public void spawnMe()
	{
		super.spawnMe();
		refreshHpMpCp();
		updateTerritories();
	}

	@Override
	protected void onDespawn()
	{
		stopMove();
		stopAttackStanceTask();
		updateTerritories();
		clearStatusListeners();

		super.onDespawn();
	}

	protected L2GameServerPacket stopMovePacket()
	{
		return new StopMove(this);
	}

	public void updateTerritories()
	{
		final GArray<L2Territory> current_territories = L2World.getTerritories(getX(), getY(), getZ());
		GArray<L2Territory> new_territories = null;
		GArray<L2Territory> old_territories = null;

		// TODO move inside the same block

		territoriesLock.lock();
		try
		{
			if(_territories == null)
				new_territories = current_territories;
			else
			{
				if(current_territories != null)
					for(final L2Territory terr : current_territories)
						if(!_territories.contains(terr))
						{
							if(new_territories == null)
								new_territories = new GArray<L2Territory>();
							new_territories.add(terr);
						}

				if(_territories.size() > 0)
					for(final L2Territory terr : _territories)
						if(current_territories == null || !current_territories.contains(terr))
						{
							if(old_territories == null)
								old_territories = new GArray<L2Territory>();
							old_territories.add(terr);
						}
			}

			if(current_territories != null && current_territories.size() > 0)
				_territories = current_territories;
			else
				_territories = null;
		}
		finally
		{
			territoriesLock.unlock();
		}

		// выход из зоны/терретории должен обрабатываться раньше входа в неё
		if(old_territories != null)
			for(final L2Territory terr : old_territories)
				if(terr != null)
					terr.doLeave(this, true);

		if(new_territories != null)
			for(final L2Territory terr : new_territories)
				if(terr != null)
					terr.doEnter(this);

		firePropertyChanged(new TerritoryChangeEvent(old_territories, new_territories, this));
	}

	@Override
	public void clearTerritories()
	{
		territoriesLock.lock();
		try
		{
			if(_territories != null)
				for(final L2Territory t : _territories)
					if(t != null)
						t.doLeave(this, false);
			_territories = null;
		}
		finally
		{
			territoriesLock.unlock();
		}
	}

	public void doZoneCheck(final int messageNumber)
	{}

	public boolean isInZonePeace()
	{
		if(_reflection == ReflectionTable.EVENT_TOWN_SIEGE) // FIXME убрать эти кастыли
			return false;
		return isInZone(ZoneType.peace_zone) && !isInZoneBattle();
	}

	public boolean isInZoneSiege()
	{
		return isInZone(ZoneInfo.ZONE_SIEGE);
	}

	public boolean isInZoneBattle()
	{
		return isInZone(ZoneInfo.ZONE_BATTLE);
	}

	public boolean isInZoneDanger()
	{
		return isInZone(ZoneInfo.ZONE_DANGERAREA);
	}

	public boolean isInZoneOlympiad()
	{
		return isInZone(ZoneType.OlympiadStadia);
	}

	public boolean isInZoneWater()
	{
		return isInZone(ZoneType.water) && !isInZone(ZoneType.no_water) && !isInVehicle();
	}

	public void addZone(final L2Zone zone)
	{
		_zoneInfo.addZone(zone);
	}

	public void removeZone(final L2Zone zone)
	{
		_zoneInfo.removeZone(zone);
	}

	public final boolean isInZone(final ZoneType type)
	{
		return _zoneInfo.isInsideZone(type);
	}

	public final boolean isInZone(final int zone)
	{
		return _zoneInfo.isInsideZone(zone);
	}

	public boolean isInZone(final L2Zone zone)
	{
		return _zoneInfo.isInZone(zone);
	}

	public L2Zone getZone(final ZoneType type)
	{
		return _zoneInfo.getZone(type);
	}

	/** Возвращает координаты поверхности воды, если мы находимся в ней, или над ней. */
	public int getWaterZ()
	{
		if(_zoneInfo.isEmpty() || isInZone(ZoneType.no_water) || !isPlayer() || isInVehicle() || isVehicle() || isFlying())
			return Integer.MIN_VALUE;

		final int z = GeoEngine.getHeight(getLoc()) + 1;
		int water_z = Integer.MIN_VALUE;

		final GArray<L2Territory> terrlist = L2World.getTerritories(getX(), getY(), z);
		if(terrlist != null)
			for(final L2Territory terr : terrlist)
				if(terr != null && terr.getZone() != null && terr.getZone().getType() == ZoneType.water && (water_z == Integer.MIN_VALUE || water_z < terr.getZmax()))
					water_z = terr.getZmax();

		return water_z;
	}

	public boolean isSwimming()
	{
		return getWaterZ() != Integer.MIN_VALUE;
	}

	public boolean isActionBlocked(final String action)
	{
		final L2Zone[] zones = getZones();
		for(final L2Zone z : zones)
			if(z != null && z.getType() == ZoneType.unblock_actions && z.isActionBlocked(action))
				return false;
		for(final L2Zone z : zones)
			if(z != null && z.getType() != ZoneType.unblock_actions && z.isActionBlocked(action))
				return true;
		return false;
	}

	public L2Zone[] getZones()
	{
		return _zoneInfo.getAllZones();
	}

	@Override
	public boolean isCharacter()
	{
		return true;
	}
}
