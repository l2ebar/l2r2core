package l2n.game.model.restrictions.impl;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.restrictions.IPlayerRestriction;
import l2n.game.model.restrictions.PlayerRestrictionType;
import l2n.util.ArrayUtil;

public class SkillUseRestriction implements IPlayerRestriction
{
	private final int[] skill_ids;

	public SkillUseRestriction(final int... ids)
	{
		skill_ids = ids;
	}

	@Override
	public boolean test(final L2Character actor, final L2Character target, final L2Skill skill, final L2ItemInstance item)
	{
		if(skill == null || skill_ids.length == 0)
			return true;
		return ArrayUtil.arrayFirstIndexOf(skill_ids, skill.getId()) < 0;
	}

	@Override
	public PlayerRestrictionType getType()
	{
		return PlayerRestrictionType.SKILL_USE;
	}
}
