package l2n.game.model.entity.residence;

public enum ResidenceType
{
	None,
	Clanhall,
	Castle,
	Fortress;
}
