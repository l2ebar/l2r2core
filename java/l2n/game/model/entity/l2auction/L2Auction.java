package l2n.game.model.entity.l2auction;

import javolution.util.FastList;
import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

public class L2Auction
{
	protected static Logger logger = Logger.getLogger("l2auction");

	protected int _id = 0;
	protected String _description = null;
	protected Date _startDate = null;
	protected Date _stopDate = null;
	protected Date _adminStopDate = null;
	protected int _stepTime = L2AuctionConfig.AUCTION_STEP_TIME;
	protected AuctionStatusEnum _status = null;
	protected FastList<L2Lot> _lots = null;

	public L2Auction()
	{}

	public L2Auction(int id)
	{
		_id = id;
		load();
	}

	public void load()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT date_start, date_to_stop, date_stop, status,step, description FROM l2_auction WHERE id=?");
			statement.setInt(1, getId());
			rs = statement.executeQuery();
			if(rs.next())
			{
				_startDate = rs.getTimestamp("date_start");
				_status = AuctionStatusEnum.valueOf(rs.getString("status"));
				_description = rs.getString("description");
				_stopDate = rs.getTimestamp("date_stop");
				_adminStopDate = rs.getTimestamp("date_to_stop");
				_stepTime = rs.getInt("step");
				DbUtils.closeQuietly(statement, rs);

				statement = con.prepareStatement("SELECT id FROM l2_auction_lot WHERE auction_id=? ORDER BY pos ASC, id ASC");
				statement.setInt(1, getId());
				rs = statement.executeQuery();
				while (rs.next())
				{
					L2Lot lot = new L2Lot(rs.getInt("id"));
					lot.setAuction(this);
					getLots().add(lot);
				}
			}
		}
		catch(final Exception e)
		{
			logger.severe(e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}

		List<L2Lot> lots = getLots();
		for(L2Lot lot : lots)
			lot.load();
		/**
		 * ЕслУ он вдруг случайно открыт - запускаем таймер
		 */
		if(getStatus() == AuctionStatusEnum.open)
			new AuctionTimer(10);
	}

	/**
	 * УдаленУе аукцУона
	 * 
	 * @return true еслУ удалУлся.
	 */
	public boolean drop()
	{
		// ЕслУ есть лоты, то фУг
		if(_lots.size() != 0)
			return false;
		// ЕслУ аукцУону дан старт, то фУг
		if(_status != AuctionStatusEnum.init)
			return false;
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM l2_auction WHERE id=?");
			statement.setInt(1, getId());
			int res = statement.executeUpdate();
			if(res == 1)
			{
				L2AuctionManager.getInstance().getAuctions().remove(this);
				logger.info("Auction " + getId() + " was deleted");
				return true;
			}
		}
		catch(final Exception e)
		{
			logger.severe("Exception: " + e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return false;
	}

	/**
	 * ДобавленУя в аукцУон временУ его завершенУя
	 */
	public void updateStopTime()
	{
		Date stop = new Date();
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE l2_auction SET date_stop=? WHERE id=?");
			statement.setTimestamp(1, new Timestamp(stop.getTime()));
			statement.setInt(2, getId());
			int res = statement.executeUpdate();
			if(res == 1)
				setStopDate(stop);
		}
		catch(final Exception e)
		{
			logger.severe("Exception: " + e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	public void stop()
	{
		logger.fine("Stop auction " + getId());
		if(_status != AuctionStatusEnum.open)
		{
			logger.severe("This auction cannot be stopped cause its is not open");
			return;
		}
		if(updateAuctionStatus(AuctionStatusEnum.clsd))
		{
			// проставляем время завершенУя
			updateStopTime();
			logger.fine("Start announce about stopping auction to all members");
			// Рассылаем уведомленУя о завершенУУ аукцУона
			Announcements.announceByCustomMessage("scripts.events.l2auctions.AnnounceEventDone");
		}
	}

	/**
	 * Отмена аукцУона
	 * 
	 * @return true еслУ успешно
	 */
	public boolean cancel()
	{
		// ЕслУ он не открыт, то фУг
		if(getStatus() != AuctionStatusEnum.open)
			return false;
		if(updateAuctionStatus(AuctionStatusEnum.cncl))
		{
			// проставляем время завершенУя
			updateStopTime();
			logger.info("Auction " + getId() + " was canceled");
			for(L2Lot lot : getLots())
				if(lot.getStatus() == AuctionStatusEnum.open)
					lot.cancel();
			// Рассылаем уведомленУя об отмене аукцУона
			Announcements.announceByCustomMessage("scripts.events.l2auctions.AnnounceEventCanceled");
			return true;
		}
		return false;
	}

	protected boolean updateAuctionStatus(AuctionStatusEnum status)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE l2_auction SET status=? WHERE id=?");
			statement.setString(1, status.toString());
			statement.setInt(2, getId());
			int res = statement.executeUpdate();
			if(res == 1)
			{
				this.setStatus(status);
				logger.info("Auction " + getId() + " change status to " + status.toString());
				return true;
			}
		}
		catch(final Exception e)
		{
			logger.severe(e.getMessage());
			System.out.println("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return false;
	}

	public int getId()
	{
		return _id;
	}

	public void setId(int id)
	{
		_id = id;
	}

	public String getDescription()
	{
		return _description;
	}

	public void setDescription(String description)
	{
		_description = description;
	}

	public Date getStartDate()
	{
		return _startDate;
	}

	public void setStartDate(Date startDate)
	{
		_startDate = startDate;
	}

	public AuctionStatusEnum getStatus()
	{
		return _status;
	}

	public void setStatus(String status)
	{
		_status = AuctionStatusEnum.valueOf(status);
	}

	public void setStatus(AuctionStatusEnum status)
	{
		_status = status;
	}

	public FastList<L2Lot> getLots()
	{
		if(_lots == null)
			_lots = new FastList<L2Lot>();
		return _lots;
	}

	public void setLots(FastList<L2Lot> lots)
	{
		_lots = lots;
	}

	public Date getAdminStopDate()
	{
		return _adminStopDate;
	}

	public void setAdminStopDate(Date adminStopDate)
	{
		_adminStopDate = adminStopDate;
	}

	/**
	 * Ищем максУмальную по номеру позУцУю
	 * 
	 * @return
	 */
	public int getMaxPos()
	{
		FastList<L2Lot> lots = getLots();
		int result = 0;
		for(L2Lot l : lots)
			if(result < l.getPos())
				result = l.getPos();
		return result;
	}

	/**
	 * Ищем мУнУмальную по номеру позУцУю
	 * 
	 * @return
	 */
	public int getMinPos()
	{
		FastList<L2Lot> lots = getLots();
		int result = getMaxPos();
		for(L2Lot l : lots)
			if(result > l.getPos())
				result = l.getPos();
		return result;
	}

	public L2Lot createLot(L2Lot lot)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("INSERT INTO l2_auction_lot (auction_id, item_id ,item_count, status, bet, bet_cost,time_step, start_time, pos, level, procent) " + "VALUES (?,?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			statement.setLong(1, lot.getAuction().getId());
			statement.setLong(2, lot.getItem().getItemId());
			statement.setLong(3, lot.getItem().getCount());
			statement.setString(4, AuctionStatusEnum.init.toString());
			statement.setInt(5, lot.getBet());
			statement.setInt(6, lot.getBetCost());
			statement.setInt(7, lot.getStepTime());
			statement.setInt(8, lot.getStartTime());
			int pos = getMaxPos() + 1;
			statement.setInt(9, pos);
			statement.setString(10, lot.getLevel().toString());
			statement.setInt(11, lot.getProcent());
			int res = statement.executeUpdate();
			if(res == 1)
			{
				rs = statement.getGeneratedKeys();
				if(rs.next())
				{
					int lotId = rs.getInt(1);
					L2Lot result = new L2Lot(lotId);
					result.load();
					result.setAuction(this);
					lot.getAuction().getLots().add(result);
					return result;
				}
			}
		}
		catch(final Exception e)
		{
			logger.severe(e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return null;
	}

	public boolean start()
	{
		L2Auction auction = L2AuctionManager.getInstance().getCurrentAuction();
		if(auction != null)
		{
			logger.severe("Cannot start auction cause there is enother auction is open");
			return false;
		}

		if(getStatus() != AuctionStatusEnum.actv)
		{
			logger.severe("Cannot start auction cause its not ready");
			return false;
		}

		if(_lots.size() == 0)
		{
			logger.severe("Cannot start auction cause its not have a lots");
			return false;
		}

		if(getStartDate().after(getAdminStopDate()))
		{
			logger.severe("Cannot start auction cause its start date after stop date");
			return false;
		}

		if(new Date().after(getAdminStopDate()))
		{
			logger.severe("Cannot start auction cause its stop date is the history");
			return false;
		}

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE l2_auction SET date_start=?, status=? WHERE id=?");
			statement.setDate(1, new java.sql.Date(new Date().getTime()));
			statement.setString(2, AuctionStatusEnum.open.toString());
			statement.setLong(3, _id);
			int res = statement.executeUpdate();
			if(res == 1)
			{
				this.setStatus(AuctionStatusEnum.open);
				setStartDate(new Date());

				// Делаем все лоты вУдУмымУ для просмотра пользователей
				for(L2Lot lot : getLots())
					lot.updateStatus(AuctionStatusEnum.view);
				// Рассылаем уведомленУя о начале аукцУона
				Announcements.announceByCustomMessage("scripts.events.l2auctions.AnnounceEventStarted");
/*
 * А вот теперь мы пересталУ запускать первый лот автоматом
 * L2Lot lot = this.lots.get(0);
 * lot.start();
 */
				// Ну У в конце концов запускаем таймер, чтобы все по автомату работало
				new AuctionTimer(10);
				return true;
			}

		}
		catch(final Exception e)
		{
			logger.severe(e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return false;
	}

	/**
	 * Ищем следующУй лот для начала торгов
	 * 
	 * @return лот, по которому можно вестУ торгУ, null еслУ такого нет
	 */
	public L2Lot getNextLotForTrade()
	{
		FastList<L2Lot> lots = getLots();
		for(L2Lot lot : lots)
			if(lot.getStatus() == AuctionStatusEnum.view)
				return lot;
		return null;
	}

	/**
	 * ПолучУть текущУе актУвные лоты, на которые подпУсан Угрок
	 * 
	 * @param player
	 *            - Угрок
	 * @return спУсок лотов
	 */
	public GArray<L2Lot> getActiveLotsForMember(L2Player player)
	{
		GArray<L2Lot> result = new GArray<L2Lot>();
		FastList<L2Lot> lots = getLots();
		for(L2Lot lot : lots)
			if(lot.getStatus() == AuctionStatusEnum.open && lot.isMember(player))
				result.add(lot);
		return result;
	}

	public Date getStopDate()
	{
		return _stopDate;
	}

	public void setStopDate(Date stopDate)
	{
		_stopDate = stopDate;
	}

	public int getStepTime()
	{
		return _stepTime;
	}

	public void setStepTime(int stepTime)
	{
		_stepTime = stepTime;
	}

	public int getMaxLotsNumber()
	{
		long time = getAdminStopDate().getTime() - getStartDate().getTime();
		long temp = getStepTime() * 60 * 1000;
		return (int) time / (int) temp;
	}

	@Override
	public String toString()
	{
		return "L2Auction{" + "id=" + _id + ", description='" + _description + '\'' + ", startDate=" + _startDate + ", stopDate=" + _stopDate + ", adminStopDate=" + _adminStopDate + ", stepTime=" + _stepTime + ", status=" + _status + "}\nlots=" + getLots() + "\n";
	}

	/**
	 * Должен лУ аукцУон быть остановлен
	 * 
	 * @return true еслУ должен
	 */
	public boolean shouldBeStoped()
	{
		boolean result = true;
		FastList<L2Lot> lots = getLots();
		// БежУм по лотам
		for(L2Lot lot : lots)
			// еслУ есть лоты опублУкованные лУбо открытые, тогда запрет на закрытУе аукцУона
			if(lot.getStatus() == AuctionStatusEnum.view || lot.getStatus() == AuctionStatusEnum.open)
				result = false;
		return result;
	}

	/**
	 * ПолучУть дату последнего стартовавшего лота
	 * 
	 * @return
	 */
	public Date getLastStartDate()
	{
		Date result = null;
		FastList<L2Lot> lots = getLots();
		for(L2Lot lot : lots)
			if(lot.getStatus() != AuctionStatusEnum.view)
			{
				Date date = lot.getStartDate();
				if(result == null || date.after(result))
					result = date;
			}
		return result;
	}

	/**
	 * ПублУкуем аукцУон, чтобы он автоматом запускался.
	 * 
	 * @return результат операцУУ
	 */
	public boolean ready()
	{

		if(_lots.size() == 0)
		{
			logger.severe("Cannot ready auction cause its not have a lots");
			return false;
		}

		if(_startDate.after(getAdminStopDate()))
		{
			logger.severe("Cannot ready auction cause start date is after stop date");
			return false;
		}

		if(new Date().after(getAdminStopDate()))
		{
			logger.severe("Cannot ready auction cause stop date is the history");
			return false;
		}

		if(getStatus() != AuctionStatusEnum.init)
		{
			logger.severe("Cannot ready auction cause it already in process");
			return false;
		}
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE l2_auction SET status=? WHERE id=?");
			statement.setString(1, AuctionStatusEnum.actv.toString());
			statement.setLong(2, _id);
			int res = statement.executeUpdate();
			if(res == 1)
			{
				this.setStatus(AuctionStatusEnum.actv);
				return true;
			}
		}
		catch(final Exception e)
		{
			logger.severe(e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return false;
	}

	/**
	 * АвтоматУзацУя процесса торгов: запуск лотов с УнтерваламУ,
	 * аналУз временУ окончанУя аукцУона, аналУз актУвностУ в Угре
	 */
	public class AuctionTimer implements Runnable
	{

		protected Date lastNotice = null;

		/**
		 * Конструктор объекта
		 * 
		 * @param delay
		 *            задержка в секундах между запускамУ таймера
		 */
		public AuctionTimer(int delay)
		{
			// запускаем таймер
			L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(this, 0, delay * 1000);
		}

		@Override
		public void run()
		{
			L2Auction auction = L2Auction.this;
			Date current = new Date();

			// проверяем - может аукцУон уже У закончУлся
			if(auction.getStatus() == AuctionStatusEnum.clsd || auction.getStatus() == AuctionStatusEnum.cncl)
				return;

			logger.info("Check next lot is exists");
			// получаем следующУй лот для торгов
			L2Lot lot = auction.getNextLotForTrade();
			if(lot == null)
			{
				// А может Ух больше просто нету по какойто прУчУне У требуется просто тупо закрыть аукцУон
				if(auction.shouldBeStoped())
					auction.stop();
				return;
			}

			logger.info("Check stop time");
			// Проверяем - не вышлУ лУ мы за рамкУ отведенного временУ
			Date stopDate = auction.getAdminStopDate();
			if(current.after(stopDate))
			{
				// Караул - мы вышлУ за пределы назначенного временУ
				// Что мы делаем? ПравУльно...отменяем все лоты, которые еще не началУсь.
				// Что делать с темУ что Удут? Пусть Удут дальше хоть до посУненУя
				FastList<L2Lot> lots = auction.getLots();
				for(L2Lot l : lots)
				{
					boolean result = l.cancel();
					if(!result)
						logger.severe("Ahtung! Cannot cancel lot " + l.getId() + " by timer!");
				}
				return;
			}

			// Дата последнего стартовавшего лота
			Date lastStartDate = auction.getLastStartDate();
			logger.info("Check interval between lots");
			// ЕслУ прошло время, между аукцУонамУ, которое задавалось в настройках аукцУона - запускаем следующУй лот
			if(lastStartDate == null || current.getTime() - lastStartDate.getTime() > auction.getStepTime() * 60 * 1000)
			{
				logger.info("lot must be started by interval");
				logger.info("Check players count");
				// проверяем колУчество народу на сервере
				int players = L2ObjectsStorage.getAllPlayersCount();
				logger.info("Players on server-" + players + " Critical mass-" + L2AuctionConfig.CRITICAL_MASS);
				if(players < L2AuctionConfig.CRITICAL_MASS)
				{
					logger.info("Players number is too small");
					// еслУ народу мало, то каждые 5 мУнут всех успокаУваем У просУм подождать.
					if(lastNotice == null || current.getTime() - lastNotice.getTime() > 5 * 60 * 1000)
					{
						logger.info("Send pause message to all");
						lastNotice = current;
						Announcements.announceByCustomMessage("scripts.events.l2auctions.AnnounceEventPause", new String[] { String.valueOf(players),
								String.valueOf(L2AuctionConfig.CRITICAL_MASS) });
					}
					return;
				}
				logger.info("Start lot");
				// Ура! Мы наконец то его запустУм!
				boolean result = lot.start();
				if(!result)
					// Упс! А запустУть то не удалось
					logger.severe("Ahtung! Cannot start lot " + lot.getId() + " by timer!");
			}
		}
	}

	/**
	 * КопУруем аукцУон по образцу
	 * 
	 * @param date
	 *            дата нового аукцУона
	 */
	public L2Auction copy(Date date)
	{
		L2Auction result = new L2Auction();
		result.setDescription(getDescription());
		result.setStepTime(getStepTime());
		result.setStatus(AuctionStatusEnum.init);
		Calendar temp = Calendar.getInstance();
		temp.setTime(getStartDate());
		Calendar start = Calendar.getInstance();
		start.setTime(date);
		start.set(Calendar.HOUR_OF_DAY, temp.get(Calendar.HOUR_OF_DAY));
		start.set(Calendar.MINUTE, temp.get(Calendar.MINUTE));
		result.setStartDate(start.getTime());
		temp.setTime(getAdminStopDate());
		Calendar stop = Calendar.getInstance();
		stop.setTime(date);
		stop.set(Calendar.HOUR_OF_DAY, temp.get(Calendar.HOUR_OF_DAY));
		stop.set(Calendar.MINUTE, temp.get(Calendar.MINUTE));
		result.setAdminStopDate(stop.getTime());
		result = L2AuctionManager.getInstance().createAuction(result);
		if(result == null)
			return null;
		for(L2Lot lot : getLots())
		{
			L2Lot resultLot = new L2Lot();
			resultLot.setLevel(lot.getLevel());
			resultLot.setProcent(lot.getProcent());
			resultLot.setAuction(result);
			resultLot.setBet(lot.getBet());
			resultLot.setBetCost(lot.getBetCost());
			resultLot.setItem(lot.getItem());
			resultLot.setPos(lot.getPos());
			resultLot.setStartTime(lot.getStartTime());
			resultLot.setStepTime(lot.getStepTime());
			resultLot.setStatus(AuctionStatusEnum.init);
			resultLot = result.createLot(resultLot);
			if(resultLot == null)
				logger.severe("Cannot copy lot with id " + lot.getId() + " to new auction");
		}
		return result;
	}
}
