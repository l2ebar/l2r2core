package l2n.game.model.entity.olympiad;

import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;

public class CompEndTask implements Runnable
{
	@Override
	public void run()
	{
		if(Olympiad.isOlympiadEnd())
			return;

		Olympiad._inCompPeriod = false;
		try
		{
			OlympiadManager manager = Olympiad._manager;
			// Если остались игры, ждем их завершения еще одну минуту
			if(manager != null && !manager.getOlympiadGames().isEmpty())
			{
				L2GameThreadPools.getInstance().scheduleGeneral(new CompEndTask(), 60000);
				return;
			}

			Announcements.announceToAll(Msg.THE_OLYMPIAD_GAME_HAS_ENDED);
			Olympiad._log.info("Olympiad System: Olympiad Game Ended");
			OlympiadDatabase.save();
		}
		catch(Exception e)
		{
			Olympiad._log.warning("Olympiad System: Failed to save Olympiad configuration:");
			e.printStackTrace();
		}
		Olympiad.init();
	}
}
