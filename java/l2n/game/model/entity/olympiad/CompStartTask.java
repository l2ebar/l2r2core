package l2n.game.model.entity.olympiad;

import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;

class CompStartTask implements Runnable
{
	@Override
	public void run()
	{
		if(Olympiad.isOlympiadEnd())
			return;

		Olympiad._manager = new OlympiadManager();
		Olympiad._inCompPeriod = true;

		new Thread(Olympiad._manager).start();

		L2GameThreadPools.getInstance().scheduleGeneral(new CompEndTask(), Olympiad.getMillisToCompEnd());

		Announcements.announceToAll(Msg.THE_OLYMPIAD_GAME_HAS_STARTED);
		Olympiad._log.info("Olympiad System: Olympiad Game Started");
	}
}
