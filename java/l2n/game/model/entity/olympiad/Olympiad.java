package l2n.game.model.entity.olympiad;

import javolution.util.FastMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.list.GCSArray;
import l2n.commons.map.MultiValueIntegerMap;
import l2n.commons.util.StatsSet;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.event.HWIDChecker;
import l2n.game.event.L2EventType;
import l2n.game.instancemanager.ServerVariables;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Party;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.ClassId;
import l2n.game.model.entity.Hero;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.instances.L2OlympiadManagerInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.DoorTable;
import l2n.util.Location;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

public class Olympiad
{
	/** Для вывода даты в формате ГГГГ/MM/дд ЧЧ:мм:сс */
	public static final DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");

	public static final Logger _log = Logger.getLogger(Olympiad.class.getName());
	public static Map<Integer, StatsSet> _nobles;
	public static Map<Integer, Integer> _noblesRank;
	public static GArray<StatsSet> _heroesToBe;

	public final static GCSArray<Integer> REGISTERS_NON_CLASSED = new GCSArray<Integer>(); // Внеклассовые бои
	public final static MultiValueIntegerMap REGISTERS_CLASSED = new MultiValueIntegerMap(); // Классовые бои
	public final static GCSArray<Integer> REGISTERS_TEAM_RANDOM = new GCSArray<Integer>(); // Командные бои 3х3 случайного типа
	public final static MultiValueIntegerMap REGISTERS_TEAM = new MultiValueIntegerMap(); // Внеклассовые командные бои 3х3

	private static final HWIDChecker hwid_checker = new HWIDChecker(Config.OLY_CHECK_HWID);

	public static final int DEFAULT_POINTS = 18;
	private static final int WEEKLY_POINTS = 3;

	private static final String OLYMPIAD_DATA_FILE = "./config/olympiad_option.ini";
	public static final String OLYMPIAD_HTML_PATH = "data/html/olympiad/";

	public static final String OLYMPIAD_LOAD_NOBLES = "SELECT * FROM `olympiad_nobles`";
	public static final String OLYMPIAD_SAVE_NOBLES = "REPLACE INTO `olympiad_nobles` (`char_id`,`class_id`,`char_name`,`olympiad_points`,`olympiad_points_past`,`olympiad_points_past_static`,`competitions_done`,`competitions_win`,`competitions_loose`) VALUES (?,?,?,?,?,?,?,?,?)";
	public static final String OLYMPIAD_REMOVE_NOBLES = "DELETE FROM `olympiad_nobles` WHERE `char_id` = ?";

	public static final String OLYMPIAD_GET_HEROS = "SELECT `char_id`, `char_name` FROM `olympiad_nobles` WHERE `class_id` = ? AND `competitions_done` >= 9 AND `competitions_win` > 0 ORDER BY `olympiad_points` DESC, `competitions_win` DESC, `competitions_done` DESC";
	public static final String OLYMPIAD_GET_HEROS_SOULHOUND = "SELECT `char_id`, `char_name` FROM `olympiad_nobles` WHERE `class_id` IN (?, 133) AND `competitions_done` >= 9 AND `competitions_win` > 0 ORDER BY `olympiad_points` DESC, `competitions_win` DESC, `competitions_done` DESC";
	public static final String GET_ALL_CLASSIFIED_NOBLESS = "SELECT `char_id` FROM `olympiad_nobles` ORDER BY olympiad_points_past_static DESC";

	public static final String GET_EACH_CLASS_LEADER = "SELECT `char_name` FROM `olympiad_nobles` WHERE `class_id` = ? AND `olympiad_points_past_static` != 0 ORDER BY `olympiad_points_past_static` DESC LIMIT 10";
	public static final String GET_EACH_CLASS_LEADER_SOULHOUND = "SELECT `char_name` FROM `olympiad_nobles` WHERE `class_id` IN (?, 133) AND `olympiad_points_past_static` != 0 ORDER BY `olympiad_points_past_static` DESC LIMIT 10";

	public static final String OLYMPIAD_CALCULATE_LAST_PERIOD = "UPDATE `olympiad_nobles` SET `olympiad_points_past` = `olympiad_points`, `olympiad_points_past_static` = `olympiad_points` WHERE `competitions_done` >= 9";
	public static final String OLYMPIAD_CLEANUP_NOBLES = "UPDATE `olympiad_nobles` SET `olympiad_points` = 18, `competitions_done` = 0, `competitions_win` = 0, `competitions_loose` = 0";

	public static final String CHAR_ID = "char_id";
	public static final String CLASS_ID = "class_id";
	public static final String CHAR_NAME = "char_name";
	public static final String POINTS = "olympiad_points";
	public static final String POINTS_PAST = "olympiad_points_past";
	public static final String POINTS_PAST_STATIC = "olympiad_points_past_static";
	public static final String COMP_DONE = "competitions_done";
	public static final String COMP_WIN = "competitions_win";
	public static final String COMP_LOOSE = "competitions_loose";

	public static long _olympiadEnd;
	public static long _validationEnd;

	/**
	 * The current period of the olympiad.<br>
	 * <b>0 -</b> Competition period<br>
	 * <b>1 -</b> Validation Period
	 */
	public static int _period;

	public static long _nextWeeklyChange;
	public static int _currentCycle;
	private static long _compEnd;
	private static Calendar _compStart;
	public static boolean _inCompPeriod;
	public static boolean _isOlympiadEnd;

	/** Начало боёв олимпиады */
	public static ScheduledFuture<CompStartTask> _scheduledCompStart;
	/** Окончание олимпиады */
	public static ScheduledFuture<OlympiadEndTask> _scheduledOlympiadEnd;
	/** Задача для начисления еженедельных очков */
	public static ScheduledFuture<WeeklyTask> _scheduledWeeklyTask;
	/** Запуск периода валидации */
	public static ScheduledFuture<ValidationTask> _scheduledValdationTask;

	public static final Stadia[] STADIUMS = new Stadia[22];
	public static OlympiadManager _manager;
	private static GArray<L2OlympiadManagerInstance> _npcs = new GArray<L2OlympiadManagerInstance>();

	public static void load()
	{
		_nobles = new FastMap<Integer, StatsSet>().shared();

		_currentCycle = ServerVariables.getInt("Olympiad_CurrentCycle", -1);
		_period = ServerVariables.getInt("Olympiad_Period", -1);
		_olympiadEnd = ServerVariables.getLong("Olympiad_End", -1);
		_validationEnd = ServerVariables.getLong("Olympiad_ValdationEnd", -1);
		_nextWeeklyChange = ServerVariables.getLong("Olympiad_NextWeeklyChange", -1);

		final Properties OlympiadProperties = new Properties();
		try
		{
			final InputStream is = new FileInputStream(new File(OLYMPIAD_DATA_FILE));
			OlympiadProperties.load(is);
			is.close();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}

		if(_currentCycle == -1)
			_currentCycle = Integer.parseInt(OlympiadProperties.getProperty("CurrentCycle", "1"));
		if(_period == -1)
			_period = Integer.parseInt(OlympiadProperties.getProperty("Period", "0"));
		if(_olympiadEnd == -1)
			_olympiadEnd = Long.parseLong(OlympiadProperties.getProperty("OlympiadEnd", "0"));
		if(_validationEnd == -1)
			_validationEnd = Long.parseLong(OlympiadProperties.getProperty("ValdationEnd", "0"));
		if(_nextWeeklyChange == -1)
			_nextWeeklyChange = Long.parseLong(OlympiadProperties.getProperty("NextWeeklyChange", "0"));

		initStadiums();
		OlympiadDatabase.loadNobles();
		OlympiadDatabase.loadNoblesRank();

		switch (_period)
		{
			case 0:
				if(_olympiadEnd == 0L || _olympiadEnd < Calendar.getInstance().getTimeInMillis())
					OlympiadDatabase.setNewOlympiadEnd();
				else
					_isOlympiadEnd = false;
				break;
			case 1:
				_isOlympiadEnd = true;
				_scheduledValdationTask = L2GameThreadPools.getInstance().scheduleGeneral(new ValidationTask(), getMillisToValidationEnd());
				break;
			default:
				_log.warning("Olympiad System: Omg something went wrong in loading!! Period = " + _period);
				return;
		}

		_log.info("Olympiad System: Loading Olympiad System....");
		if(_period == 0)
			_log.info("Olympiad System: Currently in Olympiad Period");
		else
			_log.info("Olympiad System: Currently in Validation Period");

		long milliToEnd;
		if(_period == 0)
			milliToEnd = getMillisToOlympiadEnd();
		else
			milliToEnd = getMillisToValidationEnd();

		Date dt = new Date(System.currentTimeMillis() + milliToEnd);
		_log.info("Olympiad System: Period ends in " + dateFormat.format(dt) + ".");

		if(_period == 0)
		{
			milliToEnd = getMillisToWeekChange();
			dt = new Date(System.currentTimeMillis() + milliToEnd);
			_log.info("Olympiad System: Next Weekly Change is in " + dateFormat.format(dt) + ".");
		}

		_log.info("Olympiad System: Loaded " + _nobles.size() + " Noblesses");

		if(_period == 0)
			init();
	}

	private static void initStadiums()
	{
		for(final L2DoorInstance door : DoorTable.getInstance().getDoors())
		{
			if(!door.getDoorName().startsWith("Door.OlympiadStadium"))
				continue;
			final String[] res = door.getDoorName().split("_");
			final int id = Integer.parseInt(res[1]);
			Stadia s = STADIUMS[id - 1];
			if(s == null)
				s = new Stadia();

			s.setDoor(door.getDoorId());
			STADIUMS[id - 1] = s;
		}
	}

	public static void init()
	{
		if(_period == 1)
			return;

		_compStart = Calendar.getInstance();
		_compStart.set(Calendar.HOUR_OF_DAY, Config.OLY_START_TIME);
		_compStart.set(Calendar.MINUTE, Config.OLY_MIN);
		_compEnd = _compStart.getTimeInMillis() + Config.OLY_CPERIOD;

		if(_scheduledOlympiadEnd != null)
			_scheduledOlympiadEnd.cancel(true);
		_scheduledOlympiadEnd = L2GameThreadPools.getInstance().scheduleGeneral(new OlympiadEndTask(), getMillisToOlympiadEnd());

		updateCompStatus();

		if(_scheduledWeeklyTask != null)
			_scheduledWeeklyTask.cancel(true);
		_scheduledWeeklyTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new WeeklyTask(), getMillisToWeekChange(), Config.OLY_WPERIOD);
	}

	public static synchronized boolean registerNoble(final L2Player noble, final CompType type)
	{
		if(!_inCompPeriod || _isOlympiadEnd) // Если не идёт период соревнований или олимпиада закончена
		{
			noble.sendPacket(Msg.THE_OLYMPIAD_GAME_IS_NOT_CURRENTLY_IN_PROGRESS);
			return false;
		}

		if(getMillisToOlympiadEnd() <= 600000) // Если осталась минута до окончания олимпиады
		{
			noble.sendPacket(Msg.THE_OLYMPIAD_GAME_IS_NOT_CURRENTLY_IN_PROGRESS);
			return false;
		}

		if(getMillisToCompEnd() <= 600000) // Если осталась минута до окончания периода соревнований
		{
			noble.sendPacket(Msg.THE_OLYMPIAD_GAME_IS_NOT_CURRENTLY_IN_PROGRESS);
			return false;
		}

		final StatsSet nobleInfo = _nobles.get(noble.getObjectId());
		if(!validPlayer(noble, noble, type))
			return false;

		if(getNoblePoints(noble.getObjectId()) < 1)
		{
			noble.sendMessage(new CustomMessage("l2n.game.model.entity.Olympiad.LessPoints", noble));
			return false;
		}

		if(noble.getOlympiadGameId() > 0)
		{
			noble.sendPacket(Msg.YOU_HAVE_ALREADY_BEEN_REGISTERED_IN_A_WAITING_LIST_OF_AN_EVENT);
			return false;
		}

		switch (type)
		{
			case CLASSED:
				if(REGISTERS_CLASSED.containsValue(noble.getObjectId()))
				{
					noble.sendPacket(Msg.YOU_ARE_ALREADY_ON_THE_WAITING_LIST_TO_PARTICIPATE_IN_THE_GAME_FOR_YOUR_CLASS);
					return false;
				}

				if(canParticipate(noble))
				{
					final int classId = nobleInfo.getInteger(CLASS_ID);
					REGISTERS_CLASSED.put(classId, noble.getObjectId());
					noble.sendPacket(Msg.YOU_HAVE_BEEN_REGISTERED_IN_A_WAITING_LIST_OF_CLASSIFIED_GAMES);
				}

				break;
			case NON_CLASSED:
				if(REGISTERS_NON_CLASSED.contains(noble.getObjectId()))
				{
					noble.sendPacket(Msg.YOU_ARE_ALREADY_ON_THE_WAITING_LIST_FOR_ALL_CLASSES_WAITING_TO_PARTICIPATE_IN_THE_GAME);
					return false;
				}

				if(canParticipate(noble))
				{
					REGISTERS_NON_CLASSED.add(noble.getObjectId());
					noble.sendPacket(Msg.YOU_HAVE_BEEN_REGISTERED_IN_A_WAITING_LIST_OF_NO_CLASS_GAMES);
				}

				break;
			case TEAM_RANDOM:
				if(REGISTERS_TEAM_RANDOM.contains(noble.getObjectId()))
				{
					noble.sendPacket(new SystemMessage(SystemMessage.C1_IS_ALREADY_REGISTERED_ON_THE_WAITING_LIST_FOR_THE_NON_CLASS_LIMITED_MATCH_EVENT).addName(noble));
					return false;
				}

				if(canParticipate(noble))
				{
					REGISTERS_TEAM_RANDOM.add(noble.getObjectId());
					noble.sendPacket(Msg.YOU_HAVE_REGISTERED_ON_THE_WAITING_LIST_FOR_THE_NON_CLASS_LIMITED_TEAM_MATCH_EVENT);
				}
				break;
			case TEAM:
				if(REGISTERS_TEAM.containsValue(noble.getObjectId()))
				{
					noble.sendPacket(new SystemMessage(SystemMessage.C1_IS_ALREADY_REGISTERED_ON_THE_WAITING_LIST_FOR_THE_NON_CLASS_LIMITED_MATCH_EVENT).addName(noble));
					return false;
				}

				final L2Party party = noble.getParty();
				if(party == null || party.getMemberCount() != 3)
				{
					noble.sendPacket(new SystemMessage(SystemMessage.THE_REQUEST_CANNOT_BE_MADE_BECAUSE_THE_REQUIREMENTS_HAVE_NOT_BEEN_MET_TO_PARTICIPATE_IN_A_TEAM));
					return false;
				}
				if(!noble.isPartyLeader())
				{
					noble.sendPacket(new SystemMessage(SystemMessage.ONLY_A_PARTY_LEADER_CAN_REQUEST_A_TEAM_MATCH));
					return false;
				}

				for(final L2Player member : party.getPartyMembers())
					if(!validPlayer(noble, member, type))
						return false;

				for(final L2Player member : party.getPartyMembers())
					canParticipate(member);

				REGISTERS_TEAM.putAll(noble.getObjectId(), party.getPartyMembersObjIds());
				noble.sendPacket(Msg.YOU_HAVE_REGISTERED_ON_THE_WAITING_LIST_FOR_THE_NON_CLASS_LIMITED_TEAM_MATCH_EVENT);
		}

		return true;
	}

	private static boolean validPlayer(final L2Player sendPlayer, final L2Player validPlayer, final CompType type)
	{
		if(!validPlayer.isNoble())
		{
			sendPlayer.sendPacket(new SystemMessage(SystemMessage.ONLY_NOBLESS_CAN_PARTICIPATE_IN_THE_OLYMPIAD).addName(validPlayer));
			return false;
		}

		if(validPlayer.getBaseClassId() != validPlayer.getClassId().getId())
		{
			sendPlayer.sendPacket(new SystemMessage(SystemMessage.ONLY_NOBLESS_CAN_PARTICIPATE_IN_THE_OLYMPIAD).addName(validPlayer));
			return false;
		}

		if(validPlayer.getInventoryLimit() * 0.8 <= validPlayer.getInventory().getSize())
		{
			sendPlayer.sendPacket(new SystemMessage(SystemMessage.SINCE_80_PERCENT_OR_MORE_OF_YOUR_INVENTORY_SLOTS_ARE_FULL_YOU_CANNOT_PARTICIPATE_IN_THE_OLYMPIAD).addName(validPlayer));
			return false;
		}

		if(validPlayer.isInEvent(L2EventType.NONE))
		{
			sendPlayer.sendMessage("Вы не можете участвовать в Олимпиаде, потому что вы зарегистрированы на других событиях.");
			return false;
		}

		if(validPlayer.isCursedWeaponEquipped())
		{
			sendPlayer.sendPacket(new SystemMessage(SystemMessage.CANNOT_JOIN_OLYMPIAD_POSSESSING_S1).addItemName(validPlayer.getCursedWeaponEquippedId()));
			return false;
		}

		switch (type)
		{
			case CLASSED:
				if(REGISTERS_CLASSED.containsValue(validPlayer.getObjectId()))
				{
					sendPlayer.sendPacket(Msg.YOU_ARE_ALREADY_ON_THE_WAITING_LIST_TO_PARTICIPATE_IN_THE_GAME_FOR_YOUR_CLASS);
					return false;
				}

				break;
			case NON_CLASSED:
				if(REGISTERS_NON_CLASSED.contains(validPlayer.getObjectId()))
				{
					sendPlayer.sendPacket(Msg.YOU_ARE_ALREADY_ON_THE_WAITING_LIST_FOR_ALL_CLASSES_WAITING_TO_PARTICIPATE_IN_THE_GAME);
					return false;
				}
				break;
			case TEAM_RANDOM:
				if(REGISTERS_TEAM_RANDOM.contains(validPlayer.getObjectId()))
				{
					sendPlayer.sendPacket(new SystemMessage(SystemMessage.C1_IS_ALREADY_REGISTERED_ON_THE_WAITING_LIST_FOR_THE_NON_CLASS_LIMITED_MATCH_EVENT).addName(validPlayer));
					return false;
				}
				break;
			case TEAM:
				if(REGISTERS_TEAM.containsValue(validPlayer.getObjectId()))
				{
					sendPlayer.sendPacket(new SystemMessage(SystemMessage.C1_IS_ALREADY_REGISTERED_ON_THE_WAITING_LIST_FOR_THE_NON_CLASS_LIMITED_MATCH_EVENT).addName(validPlayer));
					return false;
				}
				break;
		}

		if(isRegisteredInComp(validPlayer))
		{
			sendPlayer.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_ALREADY_BEEN_REGISTERED_IN_A_WAITING_LIST_OF_AN_EVENT).addName(validPlayer));
			return false;
		}

		return true;
	}

	public static synchronized void logoutPlayer(final L2Player player)
	{
		REGISTERS_CLASSED.removeValue(player.getObjectId());
		REGISTERS_NON_CLASSED.remove(Integer.valueOf(player.getObjectId()));
		REGISTERS_TEAM_RANDOM.remove(Integer.valueOf(player.getObjectId()));
		REGISTERS_TEAM.removeValue(player.getObjectId());
		removeHWID(player);

		final OlympiadGame game = getOlympiadGame(player.getOlympiadGameId());
		if(game == null)
			return;
		try
		{
			if(!game.logoutPlayer(player) && !game.validated)
				game.endGame(20000, true, !player.isInOfflineMode() /* при оффлайн торге не отнимаем очки */);
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}

	public static synchronized boolean unRegisterNoble(final L2Player noble)
	{
		if(!_inCompPeriod || _isOlympiadEnd)
		{
			noble.sendPacket(Msg.THE_OLYMPIAD_GAME_IS_NOT_CURRENTLY_IN_PROGRESS);
			return false;
		}

		if(!noble.isNoble())
		{
			noble.sendPacket(Msg.ONLY_NOBLESS_CAN_PARTICIPATE_IN_THE_OLYMPIAD);
			return false;
		}

		final OlympiadGame game = getOlympiadGame(noble.getOlympiadGameId());
		if(game != null)
			try
			{
				if(!game.logoutPlayer(noble) && !game.validated)
					game.endGame(20000, true, true);
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}

		if(!isRegistered(noble))
		{
			noble.sendPacket(Msg.YOU_HAVE_NOT_BEEN_REGISTERED_IN_A_WAITING_LIST_OF_A_GAME);
			return false;
		}

		REGISTERS_CLASSED.removeValue(noble.getObjectId());
		REGISTERS_NON_CLASSED.remove(Integer.valueOf(noble.getObjectId()));
		REGISTERS_TEAM_RANDOM.remove(Integer.valueOf(noble.getObjectId()));
		REGISTERS_TEAM.removeValue(noble.getObjectId());
		removeHWID(noble);

		noble.sendPacket(Msg.YOU_HAVE_BEEN_DELETED_FROM_THE_WAITING_LIST_OF_A_GAME);

		return true;
	}

	private static synchronized void updateCompStatus()
	{
		final Date dt = new Date(System.currentTimeMillis() + getMillisToCompBegin());
		_log.info("Olympiad System: Competition Period Starts in " + dateFormat.format(dt) + ".");
		_log.info("Olympiad System: Event starts/started: " + dateFormat.format(_compStart.getTime()) + ".");

		if(_scheduledCompStart != null)
			_scheduledCompStart.cancel(true);
		_scheduledCompStart = L2GameThreadPools.getInstance().scheduleGeneral(new CompStartTask(), getMillisToCompBegin());
	}

	private static long getMillisToOlympiadEnd()
	{
		return _olympiadEnd - System.currentTimeMillis();
	}

	static long getMillisToValidationEnd()
	{
		if(_validationEnd > System.currentTimeMillis())
			return _validationEnd - System.currentTimeMillis();
		return 10;
	}

	public static boolean isOlympiadEnd()
	{
		return _isOlympiadEnd;
	}

	public static boolean inCompPeriod()
	{
		return _inCompPeriod;
	}

	private static long getMillisToCompBegin()
	{
		if(_compStart.getTimeInMillis() < Calendar.getInstance().getTimeInMillis() && _compEnd > Calendar.getInstance().getTimeInMillis())
			return 10;
		if(_compStart.getTimeInMillis() > Calendar.getInstance().getTimeInMillis())
			return _compStart.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
		return setNewCompBegin();
	}

	private static long setNewCompBegin()
	{
		_compStart = Calendar.getInstance();
		_compStart.set(Calendar.HOUR_OF_DAY, Config.OLY_START_TIME);
		_compStart.set(Calendar.MINUTE, Config.OLY_MIN);
		_compStart.add(Calendar.HOUR_OF_DAY, 24);
		_compEnd = _compStart.getTimeInMillis() + Config.OLY_CPERIOD;

		_log.info("Olympiad System: New Schedule @ " + _compStart.getTime());

		return _compStart.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
	}

	public static long getMillisToCompEnd()
	{
		return _compEnd - Calendar.getInstance().getTimeInMillis();
	}

	private static long getMillisToWeekChange()
	{
		if(_nextWeeklyChange > Calendar.getInstance().getTimeInMillis())
			return _nextWeeklyChange - Calendar.getInstance().getTimeInMillis();
		return 10;
	}

	public static synchronized void addWeeklyPoints()
	{
		if(_period == 1)
			return;

		for(final int nobleId : _nobles.keySet())
		{
			final StatsSet nobleInfo = _nobles.get(nobleId);
			if(nobleInfo != null)
				nobleInfo.set(POINTS, nobleInfo.getInteger(POINTS) + WEEKLY_POINTS);
		}
	}

	public static int getCurrentCycle()
	{
		return _currentCycle;
	}

	public static synchronized void addSpectator(final int id, final L2Player spectator)
	{
		if(spectator.getOlympiadGameId() != -1 || isRegisteredInComp(spectator) || spectator.isInEvent(L2EventType.NONE))
		{
			spectator.sendPacket(Msg.WHILE_YOU_ARE_ON_THE_WAITING_LIST_YOU_ARE_NOT_ALLOWED_TO_WATCH_THE_GAME);
			return;
		}

		if(_manager == null || _manager.getOlympiadInstance(id) == null || _manager.getOlympiadInstance(id).getStatus() == BattleStatus.Begining)
		{
			spectator.sendPacket(Msg.THE_OLYMPIAD_GAME_IS_NOT_CURRENTLY_IN_PROGRESS);
			return;
		}

		final int[] c = ZoneManager.getInstance().getZoneById(ZoneType.OlympiadStadia, 3001 + id, false).getSpawns().get(0);
		final int[] c2 = ZoneManager.getInstance().getZoneById(ZoneType.OlympiadStadia, 3001 + id, false).getSpawns().get(1);

		spectator.enterOlympiadObserverMode(new Location((c[0] + c2[0]) / 2, (c[1] + c2[1]) / 2, (c[2] + c2[2]) / 2), id);

		_manager.getOlympiadInstance(id).addSpectator(spectator);
	}

	public static synchronized void removeSpectator(final int id, final L2Player spectator)
	{
		if(_manager == null || _manager.getOlympiadInstance(id) == null)
			return;
		_manager.getOlympiadInstance(id).removeSpectator(spectator);
	}

	public static GCSArray<L2Player> getSpectators(final int id)
	{
		if(_manager == null || _manager.getOlympiadInstance(id) == null)
			return null;
		return _manager.getOlympiadInstance(id).getSpectators();
	}

	public static OlympiadGame getOlympiadGame(final int gameId)
	{
		if(_manager == null || gameId < 0)
			return null;

		return _manager.getOlympiadGames().get(gameId);
	}

	public static synchronized int[] getWaitingList()
	{
		if(!inCompPeriod())
			return null;
		final int[] array = new int[4];
		array[0] = REGISTERS_CLASSED.totalSize();
		array[1] = REGISTERS_NON_CLASSED.size();
		array[2] = REGISTERS_TEAM_RANDOM.size();
		array[3] = REGISTERS_TEAM.totalSize();

		return array;
	}

	public static synchronized int getNoblessePasses(final L2Player player)
	{
		final int objId = player.getObjectId();

		final StatsSet noble = _nobles.get(objId);
		if(noble == null)
			return 0;

		int points = noble.getInteger(POINTS_PAST);
		if(points == 0) // Уже получил бонус
			return 0;

		final int rank = _noblesRank.get(objId);
		switch (rank)
		{
			case 1:
				points = Config.OLY_RANK1_POINTS;
				break;
			case 2:
				points = Config.OLY_RANK2_POINTS;
				break;
			case 3:
				points = Config.OLY_RANK3_POINTS;
				break;
			case 4:
				points = Config.OLY_RANK4_POINTS;
				break;
			default:
				points = Config.OLY_RANK5_POINTS;
		}

		if(Hero.getInstance().isHero(player.getObjectId()) || Hero.getInstance().isInactiveHero(player.getObjectId()))
			points += Config.OLY_HERO_POINTS;
		noble.set(POINTS_PAST, 0);
		OlympiadDatabase.saveNobleData(objId);

		return points * Config.OLY_GP_PER_POINT;
	}

	public static synchronized boolean isRegistered(final L2Player noble)
	{
		if(REGISTERS_CLASSED.containsValue(noble.getObjectId()))
			return true;
		if(REGISTERS_NON_CLASSED.contains(noble.getObjectId()))
			return true;
		if(REGISTERS_TEAM_RANDOM.contains(noble.getObjectId()))
			return true;
		if(REGISTERS_TEAM.containsValue(noble.getObjectId()))
			return true;

		return hwid_checker.contains(noble);
	}

	public static synchronized boolean isRegisteredInComp(final L2Player player)
	{
		if(isRegistered(player))
			return true;
		if(_manager == null || _manager.getOlympiadGames() == null)
			return false;
		for(final OlympiadGame g : _manager.getOlympiadGames().values())
			if(g != null && g.isRegistered(player.getObjectId()))
				return true;
		return false;
	}

	/**
	 * Возвращает олимпийские очки за текущий период
	 * 
	 * @param objId
	 * @return
	 */
	public static synchronized int getNoblePoints(final int objId)
	{
		final StatsSet noble = _nobles.get(objId);
		if(noble == null)
			return 0;
		return noble.getInteger(POINTS);
	}

	/**
	 * Возвращает олимпийские очки за прошлый период
	 * 
	 * @param objId
	 * @return
	 */
	public static synchronized int getNoblePointsPast(final int objId)
	{
		final StatsSet noble = _nobles.get(objId);
		if(noble == null)
			return 0;
		return noble.getInteger(POINTS_PAST);
	}

	public static synchronized int getCompetitionDone(final int objId)
	{
		final StatsSet noble = _nobles.get(objId);
		if(noble == null)
			return 0;
		return noble.getInteger(COMP_DONE);
	}

	public static synchronized int getCompetitionWin(final int objId)
	{
		final StatsSet noble = _nobles.get(objId);
		if(noble == null)
			return 0;
		return noble.getInteger(COMP_WIN);
	}

	public static synchronized int getCompetitionLoose(final int objId)
	{
		final StatsSet noble = _nobles.get(objId);
		if(noble == null)
			return 0;
		return noble.getInteger(COMP_LOOSE);
	}

	public static Stadia[] getStadiums()
	{
		return STADIUMS;
	}

	public static GArray<L2OlympiadManagerInstance> getNpcs()
	{
		return _npcs;
	}

	public static void addOlympiadNpc(final L2OlympiadManagerInstance npc)
	{
		_npcs.add(npc);
	}

	public static void changeNobleName(final int objId, final String newName)
	{
		final StatsSet noble = _nobles.get(objId);
		if(noble == null)
			return;
		noble.set(CHAR_NAME, newName);
		OlympiadDatabase.saveNobleData(objId);
	}

	public static String getNobleName(final int objId)
	{
		final StatsSet noble = _nobles.get(objId);
		if(noble == null)
			return null;
		return noble.getString(CHAR_NAME);
	}

	public static int getNobleClass(final int objId)
	{
		final StatsSet noble = _nobles.get(objId);
		if(noble == null)
			return 0;
		return noble.getInteger(CLASS_ID);
	}

	/**
	 * Устанавливает олимпийские очки
	 * 
	 * @param objId
	 * @param points
	 */
	public static void manualSetNoblePoints(final int objId, final int points)
	{
		final StatsSet noble = _nobles.get(objId);
		if(noble == null)
			return;
		noble.set(POINTS, points);
		OlympiadDatabase.saveNobleData(objId);
	}

	public static synchronized boolean isNoble(final int objId)
	{
		return _nobles.get(objId) != null;
	}

	// TODO переделать. нахрена постоянно сохранять весь список нублов? при большом списке долго делает
	public static synchronized void addNoble(final L2Player noble)
	{
		if(_nobles != null && _nobles.containsKey(noble.getObjectId()))
			return;

		int classId = noble.getBaseClassId();
		if(classId < 88)
			for(final ClassId id : ClassId.values())
			{
				if(id.level() != 3 || id.getParent(0).getId() != classId)
					continue;
				classId = id.getId();
				break;
			}

		final StatsSet statDat = new StatsSet();
		statDat.set(CLASS_ID, classId);
		statDat.set(CHAR_NAME, noble.getName());
		statDat.set(POINTS, DEFAULT_POINTS);
		statDat.set(POINTS_PAST, 0);
		statDat.set(POINTS_PAST_STATIC, 0);
		statDat.set(COMP_DONE, 0);
		statDat.set(COMP_WIN, 0);
		statDat.set(COMP_LOOSE, 0);
		_nobles.put(noble.getObjectId(), statDat);

		// FIXME зачем опять сохранять всех нублов?
		// OlympiadDatabase.saveNobleData();
		OlympiadDatabase.saveNobleData(noble.getObjectId());
	}

	public static boolean canParticipate(final L2Player player)
	{
		return hwid_checker.canParticipate(player);
	}

	public static boolean removeHWID(final L2Player player)
	{
		return hwid_checker.remove(player);
	}

	public static void clearHWID()
	{
		hwid_checker.clear();
	}

	public static synchronized void removeNoble(final L2Player noble)
	{
		_nobles.remove(noble.getObjectId());
		// FIXME зачем опять сохранять всех нублов?
		// OlympiadDatabase.saveNobleData();
		OlympiadDatabase.removeNobleData(noble.getObjectId());
	}
}
