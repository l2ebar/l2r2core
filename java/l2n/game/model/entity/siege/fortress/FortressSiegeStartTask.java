package l2n.game.model.entity.siege.fortress;

import l2n.game.L2GameThreadPools;
import l2n.game.model.entity.siege.Siege;
import l2n.game.network.serverpackets.SystemMessage;

/**
 * @author L2System Project
 * @date 08.08.2011
 * @time 5:03:15
 */
public class FortressSiegeStartTask implements Runnable
{
	private final FortressSiege _siege;
	private final int _time;

	public FortressSiegeStartTask(final FortressSiege siege, final int time)
	{
		_siege = siege;
		_time = time;
	}

	@Override
	public void run()
	{
		if(_siege.isInProgress())
			return;
		try
		{
			switch (_time)
			{
				case 3600:
					L2GameThreadPools.getInstance().scheduleGeneral(new FortressSiegeStartTask(_siege, 600), 3000000); // Prepare task for 10 minutes left.
					break;
				case 600:
					_siege.despawnCommanderNpc();
					_siege.announceToPlayer(new SystemMessage(SystemMessage.S1_MINUTE_UNTIL_THE_FORTRESS_BATTLE_STARTS).addNumber(10), false, Siege.ANNOUNCE_TO_BOTH_SIDES);
					L2GameThreadPools.getInstance().scheduleGeneral(new FortressSiegeStartTask(_siege, 300), 300000); // Prepare task for 5 minutes left.
					break;
				case 300:
					_siege.announceToPlayer(new SystemMessage(SystemMessage.S1_MINUTE_UNTIL_THE_FORTRESS_BATTLE_STARTS).addNumber(5), false, Siege.ANNOUNCE_TO_BOTH_SIDES);
					L2GameThreadPools.getInstance().scheduleGeneral(new FortressSiegeStartTask(_siege, 60), 240000); // Prepare task for 1 minute left.
					break;
				case 60:
					_siege.announceToPlayer(new SystemMessage(SystemMessage.S1_MINUTE_UNTIL_THE_FORTRESS_BATTLE_STARTS).addNumber(1), false, Siege.ANNOUNCE_TO_BOTH_SIDES);
					L2GameThreadPools.getInstance().scheduleGeneral(new FortressSiegeStartTask(_siege, 30), 30000); // Prepare task for 30 seconds left.
					break;
				case 30:
					_siege.announceToPlayer(new SystemMessage(SystemMessage.S1_SECOND_UNTIL_THE_FORTRESS_BATTLE_STARTS).addNumber(30), false, Siege.ANNOUNCE_TO_BOTH_SIDES);
					L2GameThreadPools.getInstance().scheduleGeneral(new FortressSiegeStartTask(_siege, 10), 20000); // Prepare task for 10 seconds left.
					break;
				case 10:
					_siege.announceToPlayer(new SystemMessage(SystemMessage.S1_SECOND_UNTIL_THE_FORTRESS_BATTLE_STARTS).addNumber(10), false, Siege.ANNOUNCE_TO_BOTH_SIDES);
					L2GameThreadPools.getInstance().scheduleGeneral(new FortressSiegeStartTask(_siege, 5), 5000); // Prepare task for 5 seconds left.
					break;
				case 5:
					_siege.announceToPlayer(new SystemMessage(SystemMessage.S1_SECOND_UNTIL_THE_FORTRESS_BATTLE_STARTS).addNumber(5), false, Siege.ANNOUNCE_TO_BOTH_SIDES);
					L2GameThreadPools.getInstance().scheduleGeneral(new FortressSiegeStartTask(_siege, 1), 4000); // Prepare task for 1 seconds left.
					break;
				case 1:
					_siege.announceToPlayer(new SystemMessage(SystemMessage.S1_SECOND_UNTIL_THE_FORTRESS_BATTLE_STARTS).addNumber(1), false, Siege.ANNOUNCE_TO_BOTH_SIDES);
					L2GameThreadPools.getInstance().scheduleGeneral(new FortressSiegeStartTask(_siege, 0), 1000); // Prepare task start siege.
					break;
				case 0:
					_siege.startSiege();
					break;
				default:
					break;
			}
		}
		catch(final Throwable t)
		{}
	}
}
