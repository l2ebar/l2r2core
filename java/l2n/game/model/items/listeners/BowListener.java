package l2n.game.model.items.listeners;

import l2n.game.model.actor.L2Playable;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.templates.L2Weapon.WeaponType;

public final class BowListener implements PaperdollListener
{
	public static final BowListener STATIC_INSTANCE = new BowListener();

	@Override
	public void onUnequip(final int slot, final L2ItemInstance item, final L2Playable owner)
	{
		if(owner == null || !owner.isPlayer() || slot != Inventory.PAPERDOLL_RHAND)
			return;

		if(item.getItemType() == WeaponType.BOW)
			owner.getInventory().setPaperdollItem(Inventory.PAPERDOLL_LHAND, null);
		if(item.getItemType() == WeaponType.CROSSBOW)
			owner.getInventory().setPaperdollItem(Inventory.PAPERDOLL_LHAND, null);
		if(item.getItemType() == WeaponType.ROD)
			owner.getInventory().setPaperdollItem(Inventory.PAPERDOLL_LHAND, null);
	}

	@Override
	public void onEquip(final int slot, final L2ItemInstance item, final L2Playable owner)
	{
		if(owner == null || !owner.isPlayer() || slot != Inventory.PAPERDOLL_RHAND)
			return;

		final Inventory inv = owner.getInventory();
		if(item.getItemType() == WeaponType.BOW)
		{
			final L2ItemInstance arrow = inv.findArrowForBow(item.getItem());
			if(arrow != null)
				inv.setPaperdollItem(Inventory.PAPERDOLL_LHAND, arrow);
		}
		if(item.getItemType() == WeaponType.CROSSBOW)
		{
			final L2ItemInstance bolt = inv.findArrowForCrossbow(item.getItem());
			if(bolt != null)
				inv.setPaperdollItem(Inventory.PAPERDOLL_LHAND, bolt);
		}
		if(item.getItemType() == WeaponType.ROD)
		{
			final L2ItemInstance bait = inv.findEquippedLure();
			if(bait != null)
				inv.setPaperdollItem(Inventory.PAPERDOLL_LHAND, bait);
		}
	}
}
