package l2n.game.model.items;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance.ItemLocation;

public class PcWarehouse extends Warehouse
{
	private L2Player _owner;

	public PcWarehouse(L2Player owner)
	{
		_owner = owner;
	}

	@Override
	public int getOwnerId()
	{
		return _owner.getObjectId();
	}

	@Override
	public ItemLocation getLocationType()
	{
		return ItemLocation.WAREHOUSE;
	}

	@Override
	public String getLocationId()
	{
		return "0";
	}

	@Override
	public byte getLocationId(boolean dummy)
	{
		return 0;
	}

	@Override
	public void setLocationId(L2Player dummy)
	{}

	@Override
	public final WarehouseType getWarehouseType()
	{
		return WarehouseType.PRIVATE;
	}
}
