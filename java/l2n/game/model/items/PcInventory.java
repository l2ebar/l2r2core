package l2n.game.model.items;

import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ItemInstance.ItemLocation;
import l2n.game.model.items.listeners.*;
import l2n.game.network.serverpackets.InventoryUpdate;
import l2n.game.tables.ItemTable;
import l2n.game.taskmanager.DelayedItemsManager;
import l2n.game.templates.L2Item;
import l2n.util.Log;

import java.sql.ResultSet;
import java.util.logging.Level;

public class PcInventory extends Inventory
{
	private final L2Player _owner;

	public PcInventory(final L2Player owner)
	{
		_owner = owner;
		addListener(AccessoryListener.STATIC_INSTANCE);
		addListener(BowListener.STATIC_INSTANCE);
		addListener(FormalWearListener.STATIC_INSTANCE);
		addListener(ArmorSetListener.STATIC_INSTANCE);
		addListener(ItemSkillsListener.STATIC_INSTANCE);
		addListener(ItemAugmentationListener.STATIC_INSTANCE);
	}

	@Override
	public L2Player getOwner()
	{
		return _owner;
	}

	@Override
	protected ItemLocation getBaseLocation()
	{
		return ItemLocation.INVENTORY;
	}

	@Override
	protected ItemLocation getEquipLocation()
	{
		return ItemLocation.PAPERDOLL;
	}

	@Override
	protected void sendNewItem(L2ItemInstance item)
	{
		getOwner().sendPacket(new InventoryUpdate(item, L2ItemInstance.ADDED));
	}

	@Override
	protected void sendModifyItem(L2ItemInstance item)
	{
		getOwner().sendPacket(new InventoryUpdate(item, L2ItemInstance.MODIFIED));
	}

	@Override
	protected void sendRemoveItem(L2ItemInstance item)
	{
		getOwner().sendPacket(new InventoryUpdate(item, L2ItemInstance.REMOVED));
	}

	@Override
	protected void onRefreshWeight()
	{
		getOwner().refreshOverloaded();
	}

	public long getAdena()
	{
		final L2ItemInstance _adena = getItemByItemId(L2Item.ITEM_ID_ADENA);
		if(_adena == null)
			return 0;
		return _adena.getCount();
	}

	/**
	 * Get all augmented items
	 */
	public GArray<L2ItemInstance> getAugmentedItems()
	{
		final GArray<L2ItemInstance> list = new GArray<L2ItemInstance>();
		for(final L2ItemInstance item : _items)
			if(item != null && item.isAugmented())
				list.add(item);
		return list;
	}

	/**
	 * Добавляет адену Угроку.<BR>
	 * <BR>
	 * 
	 * @param count
	 *            - сколько адены дать
	 * @return L2ItemInstance - новое колУчество адены
	 */
	public L2ItemInstance addAdena(final long count)
	{
		L2ItemInstance _adena = ItemTable.getInstance().createItem(L2Item.ITEM_ID_ADENA, 0, 0, "PcInventory.addAdena");
		_adena.setCount(count);
		_adena = addItem(_adena);
		Log.LogItem(getOwner(), Log.Sys_GetItem, _adena);
		return _adena;
	}

	public L2ItemInstance reduceAdena(final long count)
	{
		return destroyItemByItemId(L2Item.ITEM_ID_ADENA, count, true);
	}

	public static int[][] restoreVisibleInventory(final int objectId)
	{
		int[][] paperdoll = new int[Inventory.PAPERDOLL_MAX][3];
		ThreadConnection con = null;
		FiltredPreparedStatement statement2 = null;
		ResultSet invdata = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement2 = con.prepareStatement("SELECT object_id,item_id,loc_data,enchant_level FROM items WHERE owner_id=? AND loc='PAPERDOLL'");
			statement2.setInt(1, objectId);
			invdata = statement2.executeQuery();

			int slot, objId, itemId, enchant;
			while (invdata.next())
			{
				slot = invdata.getInt("loc_data");
				objId = invdata.getInt("object_id");
				itemId = invdata.getInt("item_id");
				enchant = invdata.getInt("enchant_level");

				paperdoll[slot][0] = objId;
				paperdoll[slot][1] = itemId;
				paperdoll[slot][2] = enchant;
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "could not restore inventory:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement2, invdata);
		}
		return paperdoll;
	}

	public boolean validateCapacity(final L2ItemInstance item)
	{
		int slots = getSize();

		if(!(item.isStackable() && getItemByItemId(item.getItemId()) != null))
			slots++;

		return validateCapacity(slots);
	}

	public boolean validateCapacity(final GArray<L2ItemInstance> items)
	{
		int slots = getSize();

		for(final L2ItemInstance item : items)
			if(!(item.isStackable() && getItemByItemId(item.getItemId()) != null))
				slots++;

		return validateCapacity(slots);
	}

	public boolean validateCapacity(final int slots)
	{
		final L2Player owner = getOwner();
		if(owner == null)
			return false;
		return slots <= owner.getInventoryLimit();
	}

	public short slotsLeft()
	{
		final L2Player owner = getOwner();
		if(owner == null)
			return 0;
		final short slots = (short) (owner.getInventoryLimit() - getSize());
		return slots <= 0 ? 0 : slots;
	}

	public boolean validateWeight(final L2ItemInstance item)
	{
		final long weight = item.getItem().getWeight() * item.getCount();
		return validateWeight(weight);
	}

	public boolean validateWeight(final long weight)
	{
		final L2Player owner = getOwner();
		if(owner == null)
			return false;
		return getTotalWeight() + weight <= owner.getMaxLoad();
	}

	@Override
	public void restore()
	{
		super.restore();
		DelayedItemsManager.getInstance().loadDelayed(getOwner(), false);
	}
}
