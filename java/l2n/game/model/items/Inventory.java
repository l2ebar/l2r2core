package l2n.game.model.items;

import l2n.commons.list.GArray;
import l2n.commons.listener.Listener;
import l2n.commons.listener.ListenerList;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.CursedWeaponsManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.PlayerClass;
import l2n.game.model.base.Race;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ItemInstance.ItemLocation;
import l2n.game.model.items.listeners.PaperdollListener;
import l2n.game.model.items.listeners.StatsListener;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Stats;
import l2n.game.tables.ItemTable;
import l2n.game.tables.PetDataTable;
import l2n.game.tables.ReflectionTable;
import l2n.game.templates.L2Armor;
import l2n.game.templates.L2Armor.ArmorType;
import l2n.game.templates.L2EtcItem.EtcItemType;
import l2n.game.templates.L2Item;
import l2n.game.templates.L2Weapon.WeaponType;
import l2n.util.Log;
import l2n.util.Util;

import java.sql.ResultSet;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Inventory
{
	protected static final Logger _log = Logger.getLogger(Inventory.class.getName());

	private static class ItemOrderComparator implements Comparator<L2ItemInstance>
	{
		@Override
		public int compare(final L2ItemInstance o1, final L2ItemInstance o2)
		{
			if(o1 == null || o2 == null)
				return 0;
			return o1.getEquipSlot() - o2.getEquipSlot();
		}
	}

	// Castle Lord circlets, WARNING: position == castle.id !
	public static final Integer[] CASTLE_LORD_CIRCLETS =
	{
			0, // no castle - no circlet.. :)
			6838, // Circlet of Gludio
			6835, // Circlet of Dion
			6839, // Circlet of Giran
			6837, // Circlet of Oren
			6840, // Circlet of Aden
			6834, // Circlet of Innadril
			6836, // Circlet of Goddard
			8182, // Circlet of Rune
			8183, // Circlet of Schuttgart
	};

	public static final List<Integer> CASTLE_LORD_CIRCLETS_LIST = Arrays.asList(Inventory.CASTLE_LORD_CIRCLETS);

	private static final int[][] ARROWS =
	{
			{ 17 }, // ng
			{ 1341, 22067 }, // d
			{ 1342, 22068 }, // c
			{ 1343, 22069 }, // b
			{ 1344, 22070 }, // a
			{ 1345, 22071 }, // s
			{ 1345, 22071 }, // s80
			{ 1345, 22071 } /* s84 */
	};

	private static final int[][] BOLTS =
	{
			{ 9632 }, // ng
			{ 9633, 22144 }, // d
			{ 9634, 22145 }, // c
			{ 9635, 22146 }, // b
			{ 9636, 22147 }, // a
			{ 9637, 22148 }, // s
			{ 9637, 22148 }, // s80
			{ 9637, 22148 } /* s84 */
	};

	public static final ItemOrderComparator orderComparator = new ItemOrderComparator();

	public static final byte PAPERDOLL_UNDER = 0;
	public static final byte PAPERDOLL_REAR = 1;
	public static final byte PAPERDOLL_LEAR = 2;
	public static final byte PAPERDOLL_NECK = 3;
	public static final byte PAPERDOLL_RFINGER = 4;
	public static final byte PAPERDOLL_LFINGER = 5;
	public static final byte PAPERDOLL_HEAD = 6;
	public static final byte PAPERDOLL_RHAND = 7;
	public static final byte PAPERDOLL_LHAND = 8;
	public static final byte PAPERDOLL_GLOVES = 9;
	public static final byte PAPERDOLL_CHEST = 10;
	public static final byte PAPERDOLL_LEGS = 11;
	public static final byte PAPERDOLL_FEET = 12;
	public static final byte PAPERDOLL_BACK = 13;
	public static final byte PAPERDOLL_LRHAND = 14;
	public static final byte PAPERDOLL_HAIR = 15;
	public static final byte PAPERDOLL_DHAIR = 16;
	public static final byte PAPERDOLL_RBRACELET = 17;
	public static final byte PAPERDOLL_LBRACELET = 18;
	public static final byte PAPERDOLL_DECO1 = 19;
	public static final byte PAPERDOLL_DECO2 = 20;
	public static final byte PAPERDOLL_DECO3 = 21;
	public static final byte PAPERDOLL_DECO4 = 22;
	public static final byte PAPERDOLL_DECO5 = 23;
	public static final byte PAPERDOLL_DECO6 = 24;
	public static final byte PAPERDOLL_BELT = 25;
	public static final byte PAPERDOLL_MAX = 26;

	private final class InventoryListenerList extends ListenerList<L2Playable>
	{


		public InventoryListenerList() {

		}

		public void onEquip(final int slot, final L2ItemInstance item)
		{
			for(final Listener<L2Playable> listener : getListeners2())
				((PaperdollListener) listener).onEquip(slot, item, getOwner());
		}

		public void onUnequip(final int slot, final L2ItemInstance item)
		{
			for(final Listener<L2Playable> listener : getListeners2())
				((PaperdollListener) listener).onUnequip(slot, item, getOwner());
		}
	}

	private final L2ItemInstance[] _paperdoll = new L2ItemInstance[PAPERDOLL_MAX];
	protected final InventoryListenerList _listeners = new InventoryListenerList();
	private CopyOnWriteArrayList<L2ItemInstance> _listenedItems = new CopyOnWriteArrayList<L2ItemInstance>();

	// protected to be accessed from child classes only
	// Отдельно синхронизировать этот список не надо, ибо ConcurrentLinkedQueue уже синхронизирован
	protected final ConcurrentLinkedQueue<L2ItemInstance> _items;
	protected ConcurrentLinkedQueue<L2ItemInstance> _refundItems;

	private int _totalWeight;

	private boolean isRefreshingListeners;

	// used to quickly check for using of items of special type
	private long _wearedMask;

	/**
	 * Constructor of the inventory
	 */
	protected Inventory()
	{
		_items = new ConcurrentLinkedQueue<L2ItemInstance>();
		addListener(StatsListener.STATIC_INSTANCE);
	}

	public abstract L2Playable getOwner();

	protected abstract ItemLocation getBaseLocation();

	protected abstract ItemLocation getEquipLocation();

	/**
	 * Returns the ownerID of the inventory
	 * 
	 * @return int
	 */
	public int getOwnerId()
	{
		return getOwner() == null ? 0 : getOwner().getObjectId();
	}

	/**
	 * Returns the quantity of items in the inventory
	 * 
	 * @return int
	 */
	public int getSize()
	{
		return _items.size();
	}

	/**
	 * Returns the list of items in inventory
	 * 
	 * @return L2ItemInstance : items in inventory
	 */
	public L2ItemInstance[] getItems()
	{
		return _items.toArray(new L2ItemInstance[_items.size()]);
	}

	public ConcurrentLinkedQueue<L2ItemInstance> getItemsList()
	{
		return _items;
	}

	public ConcurrentLinkedQueue<L2ItemInstance> getRefundItemsList()
	{
		return getRefundItemsList(false);
	}

	public ConcurrentLinkedQueue<L2ItemInstance> getRefundItemsList(final boolean create)
	{
		if(create && _refundItems == null)
			_refundItems = new ConcurrentLinkedQueue<L2ItemInstance>();
		return _refundItems;
	}

	/**
	 * Добавление предмета (L2ItemInstance) в инвентарь
	 * 
	 * @param id
	 *            - id предмета
	 * @param count
	 *            - количество
	 * @param source
	 *            - источник (обычно id игрока или монстра с которого дропнулся предмет)
	 * @param create_type
	 *            - тип созданого предмета (откуда был получен: мультисел, дроп, магазины и прочее)
	 * @return возвращает инстанс нового предмета (если предмет стыкуемый, то инстанс уже существующего предмета в инвентаре)
	 */
	public L2ItemInstance addItem(final int id, final long count, final int source, final String create_type)
	{
		final L2ItemInstance i = ItemTable.getInstance().createItem(id, getOwnerId(), source, create_type);
		i.setCount(count);
		return addItem(i, true, true);
	}

	/**
	 * Adds an item in inventory and update database
	 * 
	 * @param newItem
	 *            : L2ItemInstance
	 * @return L2ItemInstance equal to parameter newItem.
	 */
	public L2ItemInstance addItem(final L2ItemInstance newItem)
	{
		return addItem(newItem, true, true);
	}

	/**
	 * Adds item in inventory and returns parameter newItem
	 * 
	 * @param newItem
	 *            : L2ItemInstance designating the item to add
	 * @param dbUpdate
	 *            : boolean
	 * @return L2ItemInstance corresponding to the new item or the updated item in inventory
	 */
	private L2ItemInstance addItem(final L2ItemInstance newItem, final boolean dbUpdate, final boolean log)
	{
		final L2Character owner = getOwner();
		if(owner == null || newItem == null)
			return null;

		if(newItem.isHerb() && !owner.getPlayer().isGM())
		{
			Util.handleIllegalPlayerAction(owner.getPlayer(), "Inventory[179]", "tried to pickup herb into inventory", 1);
			return null;
		}

		if(newItem.getCount() < 1)
		{
			newItem.deleteMe();
			_log.log(Level.SEVERE, "AddItem: count < 0 owner: " + owner.getName(), new Exception("Inventory[280]"));
			return null;
		}

		L2ItemInstance result = newItem;
		boolean stackableFound = false;

		if(log)
			Log.add("Inventory|" + owner.getName() + "|Get item|" + result.getItemId() + "|" + result.getCount() + "|" + result.getObjectId(), "items");

		// If stackable, search item in inventory in order to add to current quantity
		if(newItem.isStackable())
		{
			final int itemId = newItem.getItemId();
			final L2ItemInstance old = getItemByItemId(itemId);
			if(old != null)
			{
				// add new item quantity to existing stack
				old.setCount(old.getCount() + newItem.getCount());
				// reset new item to null
				if(log)
					Log.add("Inventory|" + owner.getName() + "|join item from-to|" + result.getItemId() + "|" + newItem.getObjectId() + "|" + old.getObjectId(), "items");

				newItem.setCount(0);
				newItem.setOwnerId(0);
				newItem.setLocation(ItemLocation.VOID);
				newItem.removeFromDb();
				// destroy new item
				newItem.deleteMe();

				stackableFound = true;

				sendModifyItem(old);

				// update old item in inventory
				old.updateDatabase();

				result = old;
			}
		}

		// If item hasn't be found in inventory
		if(!stackableFound)
		{
			// Add item in inventory
			if(getItemByObjectId(newItem.getObjectId()) == null)
			{
				_items.add(newItem);
				if(newItem.getItemType() == EtcItemType.RUNE)
				{
					_listenedItems.add(newItem);
					_listeners.onEquip(-1, newItem);
				}
			}
			else if(log)
				Log.add("Inventory|" + owner.getName() + "|add double link to item in inventory list!|" + newItem.getItemId() + "|" + newItem.getObjectId(), "items");

			if(newItem.getOwnerId() != owner.getPlayer().getObjectId() || dbUpdate)
			{
				newItem.setOwnerId(owner.getPlayer().getObjectId());
				newItem.setLocation(getBaseLocation(), findSlot(0));
				sendNewItem(newItem);
			}
			// If database wanted to be updated, update item
			if(dbUpdate)
				newItem.updateDatabase();
		}

		if(dbUpdate && result.isCursed() && owner.isPlayer())
			CursedWeaponsManager.getInstance().checkPlayer((L2Player) owner, result);

		// Refresh weigth
		refreshWeight();
		return result;
	}

	/**
	 * Returns the item in the paperdoll slot
	 * 
	 * @param slot
	 *            Слот в котором ищем предмет
	 * @return L2ItemInstance
	 */
	public L2ItemInstance getPaperdollItem(final int slot)
	{
		return _paperdoll[slot];
	}

	/**
	 * Returns the ID of the item in the paperdol slot
	 * 
	 * @param slot
	 *            : int designating the slot
	 * @return int designating the ID of the item
	 */
	public int getPaperdollItemId(final int slot)
	{
		L2ItemInstance item = _paperdoll[slot];
		if(item != null)
			return item.getItemId();
		else if(slot == PAPERDOLL_HAIR)
		{
			item = _paperdoll[PAPERDOLL_DHAIR];
			if(item != null)
				return item.getItemId();
		}
		return 0;
	}

	/**
	 * Returns the objectID associated to the item in the paperdoll slot
	 * 
	 * @param slot
	 *            : int pointing out the slot
	 * @return int designating the objectID
	 */
	public int getPaperdollObjectId(final int slot)
	{
		L2ItemInstance item = _paperdoll[slot];
		if(item != null)
			return item.getObjectId();
		else if(slot == PAPERDOLL_HAIR)
		{
			item = _paperdoll[PAPERDOLL_DHAIR];
			if(item != null)
				return item.getObjectId();
		}
		return 0;
	}

	/**
	 * Adds new inventory's paperdoll listener
	 * 
	 * @param listener
	 *            pointing out the listener
	 */
	protected void addListener(final PaperdollListener listener)
	{
		_listeners.add(listener);
	}

	/**
	 * Removes a paperdoll listener
	 * 
	 * @param listener
	 *            pointing out the listener to be deleted
	 */
	protected void removeListener(final PaperdollListener listener)
	{
		_listeners.add(listener);
	}

	/**
	 * Equips an item in the given slot of the paperdoll. <U><I>Remark :</I></U> The item <B>HAS TO BE</B> already in the inventory
	 * 
	 * @param slot
	 *            : int pointing out the slot of the paperdoll
	 * @param item
	 *            : L2ItemInstance pointing out the item to add in slot
	 * @return L2ItemInstance designating the item placed in the slot before
	 */
	public L2ItemInstance setPaperdollItem(final int slot, final L2ItemInstance item)
	{
		final L2ItemInstance old = _paperdoll[slot];
		if(old != item)
		{
			if(old != null)
			{
				_paperdoll[slot] = null;
				onUnequip(slot, old);
			}
			// Add new item in slot of paperdoll
			if(item != null)
			{
				_paperdoll[slot] = item;
				onEquip(slot, item);
			}
		}
		return old;
	}

	/**
	 * Return the mask of weared item
	 * 
	 * @return int
	 */
	public long getWearedMask()
	{
		return _wearedMask;
	}

	public void unEquipItem(final L2ItemInstance item)
	{
		if(item.isEquipped())
			unEquipItemInBodySlot(item.getBodyPart(), item);
	}

	public void unEquipItemInBodySlotAndNotify(final int slot, final L2ItemInstance item)
	{
		if(item.isEquipped())
		{
			unEquipItemInBodySlot(item.getBodyPart(), item);
			final L2Player cha = getOwner().getPlayer();
			if(cha == null)
				return;

			cha.sendDisarmMessage(item);
		}
	}

	/**
	 * Sets item in slot of the paperdoll to null value
	 * 
	 * @param pdollSlot
	 *            : int designating the slot
	 * @return L2ItemInstance designating the item in slot before change
	 */
	public L2ItemInstance unEquipItemInSlot(final int pdollSlot)
	{
		return setPaperdollItem(pdollSlot, null);
	}

	/**
	 * Unequips item in slot (i.e. equips with default value)
	 * 
	 * @param slot
	 *            : int designating the slot
	 */
	public void unEquipItemInBodySlot(final int slot, final L2ItemInstance item)
	{
		byte pdollSlot = -1;
		switch (slot)
		{
			case L2Item.SLOT_NECK:
				pdollSlot = PAPERDOLL_NECK;
				break;
			case L2Item.SLOT_L_EAR:
				pdollSlot = PAPERDOLL_LEAR;
				break;
			case L2Item.SLOT_R_EAR:
				pdollSlot = PAPERDOLL_REAR;
				break;
			case L2Item.SLOT_L_FINGER:
				pdollSlot = PAPERDOLL_LFINGER;
				break;
			case L2Item.SLOT_R_FINGER:
				pdollSlot = PAPERDOLL_RFINGER;
				break;
			case L2Item.SLOT_HAIR:
				pdollSlot = PAPERDOLL_HAIR;
				break;
			case L2Item.SLOT_DHAIR:
				pdollSlot = PAPERDOLL_DHAIR;
				break;
			case L2Item.SLOT_HAIRALL:
				setPaperdollItem(PAPERDOLL_HAIR, null);
				setPaperdollItem(PAPERDOLL_DHAIR, null); // This should be the same as in DHAIR
				pdollSlot = PAPERDOLL_HAIR;
				break;
			case L2Item.SLOT_HEAD:
				pdollSlot = PAPERDOLL_HEAD;
				break;
			case L2Item.SLOT_R_HAND:
				pdollSlot = PAPERDOLL_RHAND;
				break;
			case L2Item.SLOT_L_HAND:
				pdollSlot = PAPERDOLL_LHAND;
				break;
			case L2Item.SLOT_GLOVES:
				pdollSlot = PAPERDOLL_GLOVES;
				break;
			case L2Item.SLOT_LEGS:
				pdollSlot = PAPERDOLL_LEGS;
				break;
			case L2Item.SLOT_CHEST:
			case L2Item.SLOT_FULL_ARMOR:
			case L2Item.SLOT_FORMAL_WEAR:
				pdollSlot = PAPERDOLL_CHEST;
				break;
			case L2Item.SLOT_BACK:
				pdollSlot = PAPERDOLL_BACK;
				break;
			case L2Item.SLOT_FEET:
				pdollSlot = PAPERDOLL_FEET;
				break;
			case L2Item.SLOT_UNDERWEAR:
				pdollSlot = PAPERDOLL_UNDER;
				break;
			case L2Item.SLOT_BELT:
				pdollSlot = PAPERDOLL_BELT;
				break;
			case L2Item.SLOT_LR_HAND:
				setPaperdollItem(PAPERDOLL_LHAND, null);
				setPaperdollItem(PAPERDOLL_RHAND, null); // this should be the same as in LRHAND
				pdollSlot = PAPERDOLL_RHAND;
				break;
			case L2Item.SLOT_L_BRACELET:
				pdollSlot = PAPERDOLL_LBRACELET;
				break;
			case L2Item.SLOT_R_BRACELET:
				pdollSlot = PAPERDOLL_RBRACELET;
				break;
			case L2Item.SLOT_DECO:
				if(item == null)
					return;
				if(getPaperdollObjectId(PAPERDOLL_DECO1) == item.getObjectId())
					pdollSlot = PAPERDOLL_DECO1;
				else if(getPaperdollObjectId(PAPERDOLL_DECO2) == item.getObjectId())
					pdollSlot = PAPERDOLL_DECO2;
				else if(getPaperdollObjectId(PAPERDOLL_DECO3) == item.getObjectId())
					pdollSlot = PAPERDOLL_DECO3;
				else if(getPaperdollObjectId(PAPERDOLL_DECO4) == item.getObjectId())
					pdollSlot = PAPERDOLL_DECO4;
				else if(getPaperdollObjectId(PAPERDOLL_DECO5) == item.getObjectId())
					pdollSlot = PAPERDOLL_DECO5;
				else if(getPaperdollObjectId(PAPERDOLL_DECO6) == item.getObjectId())
					pdollSlot = PAPERDOLL_DECO6;
				break;
			case L2Item.SLOT_LR_EAR:
				if(item == null)
					return;
				else if(getPaperdollObjectId(PAPERDOLL_REAR) == item.getObjectId())
					pdollSlot = PAPERDOLL_REAR;
				else if(getPaperdollObjectId(PAPERDOLL_LEAR) == item.getObjectId())
					pdollSlot = PAPERDOLL_LEAR;
				break;
			case L2Item.SLOT_LR_FINGER:
				if(item == null)
					return;
				else if(getPaperdollObjectId(PAPERDOLL_LFINGER) == item.getObjectId())
					pdollSlot = PAPERDOLL_LFINGER;
				else if(getPaperdollObjectId(PAPERDOLL_RFINGER) == item.getObjectId())
					pdollSlot = PAPERDOLL_RFINGER;
				break;
		}
		if(pdollSlot >= 0)
			setPaperdollItem(pdollSlot, null);
	}

	/**
	 * Equips item in slot of paperdoll.
	 * 
	 * @param item
	 *            : L2ItemInstance designating the item and slot used.
	 * @param checks
	 *            : проверять различные условия или нет
	 */
	public synchronized void equipItem(final L2ItemInstance item, final boolean checks)
	{
		final int targetSlot = item.getItem().getBodyPart();

		final L2Character owner = getOwner();
		// Проверка условий
		if(checks)
			if(owner.isPlayer() && owner.getName() != null)
			{
				final SystemMessage msg = checkConditions(owner.getPlayer(), item, false);
				if(msg != null)
				{
					owner.sendPacket(msg);
					return;
				}
			}

		// TODO затычка на статы повышающие HP/MP/CP
		final double hp = owner.getCurrentHp();
		final double mp = owner.getCurrentMp();
		final double cp = owner.getCurrentCp();

		switch (targetSlot)
		{
			case L2Item.SLOT_LR_HAND:
			{
				setPaperdollItem(PAPERDOLL_LHAND, null);
				setPaperdollItem(PAPERDOLL_RHAND, null);
				setPaperdollItem(PAPERDOLL_RHAND, item);
				break;
			}
			case L2Item.SLOT_L_HAND:
			{
				final L2ItemInstance slot = getPaperdollItem(PAPERDOLL_RHAND);

				final L2Item oldItem = slot == null ? null : slot.getItem();
				final L2Item newItem = item.getItem();

				if(oldItem != null && newItem.getItemType() == EtcItemType.ARROW && oldItem.getItemType() == WeaponType.BOW && oldItem.getCrystalType() != newItem.getCrystalType())
					return;
				if(oldItem != null && newItem.getItemType() == EtcItemType.BOLT && oldItem.getItemType() == WeaponType.CROSSBOW && oldItem.getCrystalType() != newItem.getCrystalType())
					return;

				if(newItem.getItemType() != EtcItemType.ARROW && newItem.getItemType() != EtcItemType.BOLT && newItem.getItemType() != EtcItemType.BAIT)
				{
					if(oldItem != null && oldItem.getBodyPart() == L2Item.SLOT_LR_HAND)
					{
						setPaperdollItem(PAPERDOLL_RHAND, null);
						setPaperdollItem(PAPERDOLL_LHAND, null);
					}
					else
						setPaperdollItem(PAPERDOLL_LHAND, null);

					setPaperdollItem(PAPERDOLL_LHAND, item);
				}
				else if(oldItem != null && (newItem.getItemType() == EtcItemType.ARROW && oldItem.getItemType() == WeaponType.BOW || newItem.getItemType() == EtcItemType.BOLT && oldItem.getItemType() == WeaponType.CROSSBOW || newItem.getItemType() == EtcItemType.BAIT && oldItem.getItemType() == WeaponType.ROD))
				{
					setPaperdollItem(PAPERDOLL_LHAND, item);
					if(newItem.getItemType() == EtcItemType.BAIT && owner.isPlayer())
						owner.getPlayer().setVar("LastLure", String.valueOf(item.getObjectId()));
				}
				break;
			}
			case L2Item.SLOT_R_HAND:
			{
				setPaperdollItem(PAPERDOLL_RHAND, item);
				break;
			}
			case L2Item.SLOT_L_EAR | L2Item.SLOT_R_EAR:
			{
				if(_paperdoll[PAPERDOLL_LEAR] == null)
				{
					item.setBodyPart(L2Item.SLOT_L_EAR);
					setPaperdollItem(PAPERDOLL_LEAR, item);
				}
				else if(_paperdoll[PAPERDOLL_REAR] == null)
				{
					item.setBodyPart(L2Item.SLOT_R_EAR);
					setPaperdollItem(PAPERDOLL_REAR, item);
				}
				else
				{
					item.setBodyPart(L2Item.SLOT_L_EAR);
					setPaperdollItem(PAPERDOLL_LEAR, null);
					setPaperdollItem(PAPERDOLL_LEAR, item);
				}
				break;
			}
			case L2Item.SLOT_L_FINGER | L2Item.SLOT_R_FINGER:
			{
				if(_paperdoll[PAPERDOLL_LFINGER] == null)
				{
					item.setBodyPart(L2Item.SLOT_L_FINGER);
					setPaperdollItem(PAPERDOLL_LFINGER, item);
				}
				else if(_paperdoll[PAPERDOLL_RFINGER] == null)
				{
					item.setBodyPart(L2Item.SLOT_R_FINGER);
					setPaperdollItem(PAPERDOLL_RFINGER, item);
				}
				else
				{
					item.setBodyPart(L2Item.SLOT_L_FINGER);
					setPaperdollItem(PAPERDOLL_LFINGER, null);
					setPaperdollItem(PAPERDOLL_LFINGER, item);
				}
				break;
			}
			case L2Item.SLOT_NECK:
				setPaperdollItem(PAPERDOLL_NECK, item);
				break;
			case L2Item.SLOT_FULL_ARMOR:
				setPaperdollItem(PAPERDOLL_LEGS, null);
				setPaperdollItem(PAPERDOLL_CHEST, item);
				break;
			case L2Item.SLOT_CHEST:
				setPaperdollItem(PAPERDOLL_CHEST, item);
				break;
			case L2Item.SLOT_LEGS:
			{
				// handle full armor
				final L2ItemInstance chest = getPaperdollItem(PAPERDOLL_CHEST);
				if(chest != null && chest.getBodyPart() == L2Item.SLOT_FULL_ARMOR)
					setPaperdollItem(PAPERDOLL_CHEST, null);

				if(getPaperdollItemId(PAPERDOLL_CHEST) == 6408)
					setPaperdollItem(PAPERDOLL_CHEST, null);

				setPaperdollItem(PAPERDOLL_LEGS, item);
				break;
			}
			case L2Item.SLOT_FEET:
				if(getPaperdollItemId(PAPERDOLL_CHEST) == 6408) // Formal Wear
					setPaperdollItem(PAPERDOLL_CHEST, null);
				setPaperdollItem(PAPERDOLL_FEET, item);
				break;
			case L2Item.SLOT_GLOVES:
				if(getPaperdollItemId(PAPERDOLL_CHEST) == 6408) // Formal Wear
					setPaperdollItem(PAPERDOLL_CHEST, null);
				setPaperdollItem(PAPERDOLL_GLOVES, item);
				break;
			case L2Item.SLOT_HEAD:
				if(getPaperdollItemId(PAPERDOLL_CHEST) == 6408) // Formal Wear
					setPaperdollItem(PAPERDOLL_CHEST, null);
				setPaperdollItem(PAPERDOLL_HEAD, item);
				break;
			case L2Item.SLOT_HAIR:
				final L2ItemInstance slot = getPaperdollItem(PAPERDOLL_DHAIR);
				if(slot != null && slot.getItem().getBodyPart() == L2Item.SLOT_HAIRALL)
				{
					setPaperdollItem(PAPERDOLL_HAIR, null);
					setPaperdollItem(PAPERDOLL_DHAIR, null);
				}
				setPaperdollItem(PAPERDOLL_HAIR, item);
				break;
			case L2Item.SLOT_DHAIR:
				final L2ItemInstance slot2 = getPaperdollItem(PAPERDOLL_DHAIR);
				if(slot2 != null && slot2.getItem().getBodyPart() == L2Item.SLOT_HAIRALL)
				{
					setPaperdollItem(PAPERDOLL_HAIR, null);
					setPaperdollItem(PAPERDOLL_DHAIR, null);
				}
				setPaperdollItem(PAPERDOLL_DHAIR, item);
				break;
			case L2Item.SLOT_HAIRALL:
				setPaperdollItem(PAPERDOLL_HAIR, null);
				setPaperdollItem(PAPERDOLL_DHAIR, null);
				setPaperdollItem(PAPERDOLL_DHAIR, item);
				break;
			case L2Item.SLOT_R_BRACELET:
				setPaperdollItem(PAPERDOLL_RBRACELET, null);
				setPaperdollItem(PAPERDOLL_RBRACELET, item);
				break;
			case L2Item.SLOT_L_BRACELET:
				setPaperdollItem(PAPERDOLL_LBRACELET, null);
				setPaperdollItem(PAPERDOLL_LBRACELET, item);
				break;
			case L2Item.SLOT_UNDERWEAR:
				setPaperdollItem(PAPERDOLL_UNDER, item);
				break;
			case L2Item.SLOT_BACK:
				setPaperdollItem(PAPERDOLL_BACK, item);
				break;
			case L2Item.SLOT_BELT:
				setPaperdollItem(PAPERDOLL_BELT, item);
				break;
			case L2Item.SLOT_DECO:
				if(_paperdoll[PAPERDOLL_DECO1] == null)
					setPaperdollItem(PAPERDOLL_DECO1, item);
				else if(_paperdoll[PAPERDOLL_DECO2] == null)
					setPaperdollItem(PAPERDOLL_DECO2, item);
				else if(_paperdoll[PAPERDOLL_DECO3] == null)
					setPaperdollItem(PAPERDOLL_DECO3, item);
				else if(_paperdoll[PAPERDOLL_DECO4] == null)
					setPaperdollItem(PAPERDOLL_DECO4, item);
				else if(_paperdoll[PAPERDOLL_DECO5] == null)
					setPaperdollItem(PAPERDOLL_DECO5, item);
				else if(_paperdoll[PAPERDOLL_DECO6] == null)
					setPaperdollItem(PAPERDOLL_DECO6, item);
				else
					setPaperdollItem(PAPERDOLL_DECO1, item);
				break;
			case L2Item.SLOT_FORMAL_WEAR:
				// При одевании свадебного платья руки не трогаем
				setPaperdollItem(PAPERDOLL_LEGS, null);
				setPaperdollItem(PAPERDOLL_CHEST, null);
				setPaperdollItem(PAPERDOLL_HEAD, null);
				setPaperdollItem(PAPERDOLL_FEET, null);
				setPaperdollItem(PAPERDOLL_GLOVES, null);
				setPaperdollItem(PAPERDOLL_CHEST, item);
				break;
			default:
				_log.warning("unknown body slot:" + targetSlot + " for item id: " + item.getItemId());
		}

		// TODO затычка на статы повышающие HP/MP/CP
		owner.setCurrentHp(hp, false);
		owner.setCurrentMp(mp);
		owner.setCurrentCp(cp);

		if(owner.isPlayer())
			((L2Player) owner).rechargeAutoSoulShot();
	}

	/**
	 * Returns the item from inventory by using its <B>itemId</B>
	 * 
	 * @param itemId
	 *            : int designating the ID of the item
	 * @return L2ItemInstance designating the item or null if not found in inventory
	 */
	public L2ItemInstance getItemByItemId(final int itemId)
	{
		for(final L2ItemInstance temp : getItemsList())
			if(temp.getItemId() == itemId)
				return temp;
		return null;
	}

	/**
	 * Return the L2ItemInstance corresponding to the item identifier.<BR>
	 * <BR>
	 * 
	 * @param itemId
	 *            : int designating the ID of the item
	 */
	public L2ItemInstance findItemByItemId(final int itemId)
	{
		return getItemByItemId(itemId);
	}

	public L2ItemInstance[] getAllItemsById(final int itemId)
	{
		final GArray<L2ItemInstance> ar = new GArray<L2ItemInstance>();
		for(final L2ItemInstance i : _items)
			if(i.getItemId() == itemId)
				ar.add(i);
		return ar.toArray(new L2ItemInstance[ar.size()]);
	}

	public int getPaperdollAugmentationId(final int slot)
	{
		final L2ItemInstance item = _paperdoll[slot];
		if(item != null && item.getAugmentation() != null)
			return item.getAugmentation().getAugmentationId();
		return 0;
	}

	public L2ItemInstance getItemByItemIdExclude(final int itemId, final Set<L2ItemInstance> excludeObjIds)
	{
		for(final L2ItemInstance temp : _items)
			if(temp.getItemId() == itemId && !excludeObjIds.contains(temp))
				return temp;
		return null;
	}

	/**
	 * Returns item from inventory by using its <B>objectId</B>
	 * 
	 * @param objectId
	 *            : int designating the ID of the object
	 * @return L2ItemInstance designating the item or null if not found in inventory
	 */
	public L2ItemInstance getItemByObjectId(final int objectId)
	{
		for(final L2ItemInstance temp : _items)
			if(temp.getObjectId() == objectId)
				return temp;
		return null;
	}

	/**
	 * Destroy item from inventory by using its <B>objectID</B> and updates database
	 * 
	 * @param objectId
	 *            : int pointing out the objectID of the item
	 * @param count
	 *            : long designating the quantity of item to destroy
	 * @return L2ItemInstance designating the item up-to-date
	 */
	public L2ItemInstance destroyItem(final int objectId, final long count, final boolean toLog)
	{
		final L2ItemInstance item = getItemByObjectId(objectId);
		return destroyItem(item, count, toLog);
	}

	/**
	 * Destroy item from inventory and updates database
	 * 
	 * @param item
	 *            : L2ItemInstance item to destroy
	 * @param count
	 *            : long designating the quantity of item to destroy
	 * @param toLog
	 *            : boolean, should we log this item destruction
	 * @return L2ItemInstance designating the item up-to-date
	 */
	public L2ItemInstance destroyItem(final L2ItemInstance item, final long count, final boolean toLog)
	{
		final L2Character owner = getOwner();
		if(owner == null)
			return null;

		if(count <= 0)
		{
			_log.log(Level.SEVERE, "DestroyItem: count < 0 owner: " + owner.getName(), new Exception("Inventory[1040]"));
			return null;
		}

		if(item != null && toLog)
		{
			Log.LogItem(owner, null, Log.DeleteItem, item, count);
			Log.add("Inventory|" + owner.getName() + "|Destroys item|" + item.getItemId() + "|" + count + "|" + item.getObjectId(), "items");
		}

		if(item == null)
			return null;

		if(item.getCount() <= count)
		{
			if(item.getCount() < count && toLog)
				Log.add("!Inventory|" + owner.getName() + "|Destroys item|" + item.getItemId() + "|" + count + " but item count " + item.getCount() + "|" + item.getObjectId(), "items");
			removeItemFromInventory(item, true);
			// check destroy pet controls items
			if(PetDataTable.isPetControlItem(item))
				PetDataTable.deletePet(item, owner);
		}
		else
		{
			item.setCount(item.getCount() - count);
			sendModifyItem(item);
			item.updateDatabase();
		}

		refreshWeight();

		return item;
	}

	// we need this one cuz warehouses send itemId only
	/**
	 * Destroy item from inventory by using its <B>itemID</B> and updates database
	 * 
	 * @param itemId
	 *            : int pointing out the itemID of the item
	 * @param count
	 *            : long designating the quantity of item to destroy
	 * @param toLog
	 *            : boolean, should we log this item destruction
	 * @return L2ItemInstance designating the item up-to-date
	 */
	public L2ItemInstance destroyItemByItemId(final int itemId, final long count, final boolean toLog)
	{
		final L2ItemInstance item = findItemByItemId(itemId);
		if(item != null)
			Log.LogItem(getOwner(), Log.Sys_DeleteItem, item, count);
		return destroyItem(item, count, toLog);
	}

	/**
	 * Destroy item from inventory and from database.
	 * 
	 * @param item
	 *            : L2ItemInstance designating the item to remove from inventory
	 * @param clearCount
	 *            : boolean : if true, set the item quantity to 0
	 */
	private void removeItemFromInventory(final L2ItemInstance item, final boolean clearCount)
	{
		final L2Character owner = getOwner();
		if(owner == null)
			return;

		if(owner.isPlayer())
		{
			final L2Player player = (L2Player) owner;
			player.removeItemFromShortCut(item.getObjectId());
			if(item.isEquipped())
				unEquipItem(item);
		}

		_items.remove(item);
		item.shadowNotify(false);

		if(item.getItemType() == EtcItemType.RUNE)
		{
			_listenedItems.remove(item);
			_listeners.onUnequip(-1, item);
		}

		if(clearCount)
			item.setCount(0);

		item.setOwnerId(0);
		item.setLocation(ItemLocation.VOID);
		sendRemoveItem(item);
		item.updateDatabase(true);
		item.deleteMe();
	}

	/**
	 * Drop item from inventory by using its <B>objectID</B>
	 * 
	 * @param objectId
	 *            : int pointing out the objectID
	 * @param count
	 *            : int designating the quantity of item to drop
	 * @return L2ItemInstance
	 */
	public L2ItemInstance dropItem(final int objectId, final long count)
	{
		final L2ItemInstance item = getItemByObjectId(objectId);
		if(item == null)
		{
			_log.warning("DropItem: item objectId: " + objectId + " does not exist in inventory");
			Thread.dumpStack();
			return null;
		}

		return dropItem(item, count);
	}

	/**
	 * Флаг нужен для определенУя удалять лУ УнкрустацУю
	 */
	public L2ItemInstance dropItem(final L2ItemInstance item, final long count, final boolean whflag)
	{
		item.setWhFlag(whflag);
		return dropItem(item, count);
	}

	/**
	 * Флаг нужен для определенУя удалять лУ УнкрустацУю
	 */
	public L2ItemInstance dropItem(final int objectId, final long count, final boolean allowRemoveAttributes)
	{
		final L2ItemInstance item = getItemByObjectId(objectId);
		if(item == null)
		{
			_log.log(Level.WARNING, "DropItem[1160]: item objectId: " + objectId + " does not exist in inventory", new Exception("dropItem"));
			return null;
		}
		return dropItem(item, count, allowRemoveAttributes);
	}

	/**
	 * Drop item from inventory by using <B>object L2ItemInstance</B><BR>
	 * <BR>
	 * <U><I>Concept :</I></U><BR>
	 * item equipped are unequipped <LI>If quantity of items in inventory after drop is negative or null, change location of item</LI> <LI>Otherwise, change quantity in inventory and create new object with quantity dropped</LI>
	 * 
	 * @param oldItem
	 *            : L2ItemInstance designating the item to drop
	 * @param count
	 *            : int designating the quantity of item to drop
	 * @return L2ItemInstance designating the item dropped
	 */
	public L2ItemInstance dropItem(final L2ItemInstance oldItem, final long count)
	{
		final L2Character owner = getOwner();
		if(owner == null)
			return null;

		if(owner.isPlayer() && ((L2Player) owner).getPlayerAccess() != null && ((L2Player) owner).getPlayerAccess().BlockInventory)
			return null;

		if(count <= 0)
		{
			_log.log(Level.SEVERE, "DropItem: count < 0 owner: " + owner.getName(), new Exception("Inventory[1207]"));
			return null;
		}

		if(oldItem == null)
		{
			_log.warning("DropItem: item id does not exist in inventory");
			return null;
		}

		Log.LogItem(owner, null, Log.Drop, oldItem, count);

		if(oldItem.getCount() <= count || oldItem.getCount() <= 1)
		{
			Log.add("Inventory|" + owner.getName() + "|Drop item|" + oldItem.getItemId() + "|" + count + "|" + oldItem.getObjectId(), "items");
			removeItemFromInventory(oldItem, false);
			refreshWeight();

			// check drop pet controls items
			if(PetDataTable.isPetControlItem(oldItem))
				PetDataTable.unSummonPet(oldItem, owner);
			return oldItem;
		}

		oldItem.setCount(oldItem.getCount() - count);
		sendModifyItem(oldItem);
		final L2ItemInstance newItem = ItemTable.getInstance().createItem(oldItem.getItemId(), owner.getObjectId(), oldItem.getOwnerId(), "Inventory.dropItem");
		newItem.setCount(count);
		oldItem.updateDatabase();
		refreshWeight();
		Log.add("Inventory|" + owner.getName() + "|Split item from-to|" + oldItem.getItemId() + "|" + oldItem.getObjectId() + "|" + newItem.getObjectId(), "items");
		Log.add("Inventory|" + owner.getName() + "|Drop item|" + newItem.getItemId() + "|" + count + "|" + newItem.getObjectId(), "items");
		return newItem;
	}

	protected abstract void sendNewItem(L2ItemInstance item);

	protected abstract void sendModifyItem(L2ItemInstance item);

	protected abstract void sendRemoveItem(L2ItemInstance item);

	/**
	 * Refresh the weight of equipment loaded
	 */
	private void refreshWeight()
	{
		long weight = 0;

		for(final L2ItemInstance element : _items)
			weight += element.getItem().getWeight() * element.getCount();

		if(weight > Integer.MAX_VALUE)
			_totalWeight = Integer.MAX_VALUE;
		else
			_totalWeight = (int) weight;
		// notify char for overload checking
		onRefreshWeight();
	}

	protected abstract void onRefreshWeight();

	/**
	 * Returns the totalWeight.
	 * 
	 * @return int
	 */
	public int getTotalWeight()
	{
		return _totalWeight;
	}

	/**
	 * Return the L2ItemInstance of the arrows needed for this bow.<BR>
	 * <BR>
	 * 
	 * @param bow
	 *            : L2Item designating the bow
	 * @return L2ItemInstance pointing out arrows for bow
	 */
	public L2ItemInstance findArrowForBow(final L2Item bow)
	{
		final int[] arrowsId = ARROWS[bow.getCrystalType().externalOrdinal];
		L2ItemInstance ret = null;
		for(final int id : arrowsId)
			if((ret = getItemByItemId(id)) != null)
				return ret;
		return null;
	}

	/**
	 * Return the L2ItemInstance of the arrows needed for this crossbow.<BR>
	 * <BR>
	 * 
	 * @param bow
	 *            : L2Item designating the crossbow
	 * @return L2ItemInstance pointing out arrows for crossbow
	 */
	public L2ItemInstance findArrowForCrossbow(final L2Item xbow)
	{
		final int[] boltsId = BOLTS[xbow.getCrystalType().externalOrdinal];
		L2ItemInstance ret = null;
		for(final int id : boltsId)
			if((ret = getItemByItemId(id)) != null)
				return ret;
		return null;
	}

	public L2ItemInstance findEquippedLure()
	{
		L2ItemInstance res = null;
		int last_lure = 0;

		final L2Character owner = getOwner();
		if(owner != null && owner.isPlayer())
			try
			{
				final L2Player player = (L2Player) owner;
				final String LastLure = player.getVar("LastLure");
				if(LastLure != null && !LastLure.isEmpty())
					last_lure = Integer.valueOf(LastLure);
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}
		for(final L2ItemInstance temp : getItemsList())
			if(temp.getItemType() == EtcItemType.BAIT)
				if(temp.getLocation() == ItemLocation.PAPERDOLL && temp.getEquipSlot() == Inventory.PAPERDOLL_LHAND)
					return temp;
				else if(last_lure > 0 && res == null && temp.getObjectId() == last_lure)
					res = temp;
		return res;
	}

	/**
	 * Delete item object from world
	 */
	public synchronized void deleteMe()
	{
		for(final L2ItemInstance inst : getItemsList())
		{
			inst.updateInDb();
			inst.deleteMe();
		}
		getItemsList().clear();
	}

	/**
	 * Update database with items in inventory
	 */
	public void updateDatabase()
	{
		updateDatabase(getItemsList(), false);
	}

	public void updateDatabase(final boolean commit)
	{
		updateDatabase(_items, commit);
	}

	/**
	 * Update database with item
	 * 
	 * @param items
	 *            : ArrayList &lt;L2ItemInstance&gt; pointing out the list of items
	 */
	private void updateDatabase(final ConcurrentLinkedQueue<L2ItemInstance> items, final boolean commit)
	{
		if(getOwner() != null)
			for(final L2ItemInstance inst : items)
				inst.updateDatabase(commit);
	}

	/**
	 * Функция для валдации вещей в инвентаре. Вызывается при загрузке персонажа.
	 */
	public void validateItems()
	{
		final L2Player owner = (L2Player) getOwner();
		if(!owner.isPlayer())
			return;

		for(final L2ItemInstance item : getItemsList())
			// Clan Apella armor
			if(item.isClanApellaItem())
			{
				if(owner.getPledgeClass() < L2Player.RANK_BARON)
					unEquipItem(item);
			}
			// Cloack
			else if(item.getItem().isCloak() && item.getName().contains("Knight") && owner.getPledgeClass() < L2Player.RANK_KNIGHT)
				unEquipItem(item);
			else if(item.getItemId() >= 7850 && item.getItemId() <= 7859)
			{
				if(owner.getClan() == null || owner.getPledgeType() != L2Clan.SUBUNIT_ACADEMY)
					unEquipItem(item);
			}
			// Hero Items
			else if(item.isHeroItem() && !owner.isHero())
			{
				unEquipItem(item);
				// Удаляем все геройские предметы кроме Wings of Destiny Circlet
				if(item.getItemId() != 6842)
					destroyItem(item, 1, false);
			}
	}

	/**
	 * Get back items in inventory from database
	 */
	public void restore()
	{
		final int OWNER = getOwner().getObjectId();

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM items WHERE owner_id=? AND (loc=? OR loc=?) ORDER BY object_id DESC");
			statement.setInt(1, OWNER);
			statement.setString(2, getBaseLocation().name());
			statement.setString(3, getEquipLocation().name());
			rset = statement.executeQuery();

			L2ItemInstance item, newItem;
			while (rset.next())
			{
				if((item = L2ItemInstance.restoreFromDb(rset, con, true)) == null)
					continue;
				newItem = addItem(item, false, false);
				if(newItem == null)
					continue;
				if(item.isEquipped())
					equipItem(item, false);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "could not restore inventory for player " + getOwner().getName() + ":", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	/**
	 * Refresh all listeners дергать осторожно, если какой-то предмет дает хп/мп то текущее значение будет сброшено
	 */
	public void refreshListeners()
	{
		setRefreshingListeners(true);
		L2ItemInstance item;
		for(int i = 0; i < PAPERDOLL_MAX; i++)
		{
			item = getPaperdollItem(i);
			if(item == null)
				continue;

			// уведомим слушателей, что якобы снимаем/одеваем каждый предмет инвентаря (реально ничего не снимая)
			// при этом BracleletListener реально снимал все талисманы при уведомлении о снятии браслета (fixed)
			_listeners.onUnequip(i, item);
			_listeners.onEquip(i, item);
		}

		for(final Iterator<L2ItemInstance> items = _listenedItems.iterator(); items.hasNext();)
		{
			item = items.next();
			_listeners.onUnequip(-1, item);
			_listeners.onEquip(-1, item);
		}

		setRefreshingListeners(false);
	}

	public boolean isRefreshingListeners()
	{
		return isRefreshingListeners;
	}

	public void setRefreshingListeners(final boolean refreshingListeners)
	{
		isRefreshingListeners = refreshingListeners;
	}

	protected void onEquip(final int slot, final L2ItemInstance item)
	{
		item.setLocation(getEquipLocation(), slot);
		sendModifyItem(item);
		_listeners.onEquip(slot, item);
		_wearedMask |= item.getItem().getItemMask();
		item.shadowNotify(true);
	}

	protected void onUnequip(final int slot, final L2ItemInstance old)
	{
		// Put old item from paperdoll slot to base location
		old.setLocation(getBaseLocation(), findSlot(0));
		// old.setChargedSpiritshot(ItemInstance.CHARGED_NONE);
		// old.setChargedSoulshot(ItemInstance.CHARGED_NONE);
		sendModifyItem(old);

		_wearedMask &= ~old.getItem().getItemMask();

		_listeners.onUnequip(slot, old);

		old.shadowNotify(false);
	}

	/**
	 * Находит и возвращает пустой слот в инвентаре.
	 */
	public int findSlot(final int slot)
	{
		for(final L2ItemInstance item : _items)
		{
			if(item.isEquipped() || item.getItem().getType2() == L2Item.TYPE2_QUEST) // игнорируем надетое и квестовые вещи
				continue;
			if(item.getEquipSlot() == slot) // слот занят?
				return findSlot(slot + 1);
		}
		return slot; // слот не занят, возвращаем
	}

	/**
	 * Возвращает все итемы на пейпердолле.<BR>
	 * Если есть повторяющися итемы, то они игнорируются.<BR>
	 * <B>Удаление итема из даного массива не приведет к удалению из paperdoll</B>
	 * 
	 * @return Итемы одетые на персонажа
	 */
	public GArray<L2ItemInstance> getPaperdollItems()
	{
		final GArray<L2ItemInstance> ret = new GArray<L2ItemInstance>(_paperdoll.length);
		for(final L2ItemInstance i : _paperdoll)
			if(i != null && !ret.contains(i))
				ret.addLastUnsafe(i);
		return ret;
	}

	/**
	 * Вызывается из RequestSaveInventoryOrder
	 */
	public void sort(final int[][] order)
	{
		L2ItemInstance _item;
		ItemLocation _itemloc;
		for(final int[] element : order)
		{
			_item = getItemByObjectId(element[0]);
			if(_item == null)
				continue;
			_itemloc = _item.getLocation();
			if(_itemloc != ItemLocation.INVENTORY)
				continue;
			_item.setLocation(_itemloc, (short) element[1]);
		}
	}

	public long getCountOf(final int itemId)
	{
		long result = 0;
		for(final L2ItemInstance item : getItems())
			if(item != null && item.getItemId() == itemId)
				result += item.getCount();
		return result;
	}

	/**
	 * Снимает все вещи, которые нельзя носить.
	 * Применяется при смене саба, захвате замка, выходе из клана.
	 */
	public void checkAllConditions()
	{
		final L2Player owner = getOwner().getPlayer();
		for(final L2ItemInstance item : _paperdoll)
		{
			if(item == null || checkConditions(owner, item, true) == null)
				continue;
			unEquipItem(item);
			owner.sendDisarmMessage(item);
		}
	}

	/**
	 * Проверяет возможность носить эту вещь.
	 * 
	 * @param owner
	 *            TODO
	 * @param checkAll
	 *            TODO
	 */
	private SystemMessage checkConditions(final L2Player owner, final L2ItemInstance item, final boolean checkAll)
	{
		final int itemId = item.getItemId();
		final int targetSlot = item.getItem().getBodyPart();
		final L2Clan ownersClan = owner.getClan();

		// Wings of Destiny Circlet[6842]
		if((item.isHeroItem() || item.getItemId() == 6842) && !owner.isHero())
			return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;

		// Don't allow weapon/shield hero equipment during Olympiads
		if(owner.isInOlympiadMode() && (item.isHeroItem() || item.isOlyRestrictedItem()))
			return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;

		// Камаелям нельзя одевать тяж, маг шмот.
		if(owner.getRace() == Race.kamael && (item.getItemType() == ArmorType.HEAVY || item.getItemType() == ArmorType.MAGIC || item.getItemType() == ArmorType.SIGIL || item.getItemType() == WeaponType.NONE && !item.getItem().isCombatFlag()))
			return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;

		if(owner.getRace() != Race.kamael && (item.getItemType() == WeaponType.CROSSBOW || item.getItemType() == WeaponType.RAPIER || item.getItemType() == WeaponType.ANCIENTSWORD))
			return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;

		if(itemId >= 7850 && itemId <= 7859) // Clan Oath Armor
			if(owner.getClan() == null || owner.getPledgeType() != L2Clan.SUBUNIT_ACADEMY)
				return Msg.THIS_ITEM_CAN_ONLY_BE_WORN_BY_A_MEMBER_OF_THE_CLAN_ACADEMY;

		if(item.isClanApellaItem() && owner.getPledgeClass() < L2Player.RANK_BARON)
			return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;

		if(item.getItemType() == WeaponType.DUALDAGGER && owner.getSkillLevel(L2Skill.SKILL_DUAL_DAGGER_MASTERY) < 0)
			return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;

		// check item class restriction
		if(item.getItem() instanceof L2Armor && ((L2Armor) item.getItem()).getClassType() != null && !PlayerClass.values()[owner.getActiveClassId()].isOfType(((L2Armor) item.getItem()).getClassType()))
			return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;

		if(CASTLE_LORD_CIRCLETS_LIST.contains(itemId) && (ownersClan == null || itemId != CASTLE_LORD_CIRCLETS[ownersClan.getHasCastle()]))
			return new SystemMessage(new CustomMessage("l2n.game.model.Inventory.CircletWorn", owner).addString(CastleManager.getInstance().getCastleByIndex(CASTLE_LORD_CIRCLETS_LIST.indexOf(itemId)).getName()));

		// Корона лидера клана, владеющего замком (The Lord's Crown)
		if(itemId == 6841 && (ownersClan == null || !owner.isClanLeader() || ownersClan.getHasCastle() == 0))
			return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;

		// Нельзя одевать оружие, если уже одето проклятое оружие. Проверка двумя способами, для надежности.
		if(targetSlot == L2Item.SLOT_LR_HAND || targetSlot == L2Item.SLOT_L_HAND || targetSlot == L2Item.SLOT_R_HAND)
		{
			if(itemId != getPaperdollItemId(PAPERDOLL_RHAND) && CursedWeaponsManager.getInstance().isCursed(getPaperdollItemId(PAPERDOLL_RHAND)))
				return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;
			if(owner.isCursedWeaponEquipped() && itemId != owner.getCursedWeaponEquippedId())
				return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;
		}

		// Cloack
		if(item.getItem().isCloak())
		{
			if(item.getName().contains("Knight") && (owner.getPledgeClass() < L2Player.RANK_KNIGHT || owner.getCastle() == null))
				return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;

			if(item.getName().contains("Kamael") && owner.getRace() != Race.kamael)
				return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;

			if(owner.calcStat(Stats.CLOAK_SLOT, 0, null, null) == 0)
				return Msg.THE_CLOAK_CANNOT_BE_EQUIPPED_BECAUSE_A_NECESSARY_ITEM_IS_NOT_EQUIPPED;
		}

		// Проверка условий для браслетов фортов/замка
		if(targetSlot == L2Item.SLOT_L_BRACELET)
		{
			// Agathion Seal Bracelet - Castle
			if(itemId >= 9607 && itemId <= 9615 && (ownersClan == null || itemId - 9606 != ownersClan.getHasCastle()))
				return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;
			// Agathion Seal Bracelet - Fortress
			if(itemId == 10018 && (ownersClan == null || ownersClan.getHasFortress() == 0))
				return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;
		}

		// Для квеста _128_PailakaSongofIceandFire
		// SpritesSword = 13034; EnhancedSpritesSword = 13035; SwordofIceandFire = 13036;
		if((itemId == 13034 || itemId == 13035 || itemId == 13036) && owner.getReflection().getInstancedZoneId() != ReflectionTable.PAILAKA_SONG_OF_ICE_AND_FIRE)
			return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;

		// Для квеста _129_PailakaDevilsLegacy
		// SWORD = 13042; ENCHSWORD = 13043; LASTSWORD = 13044;
		if((itemId == 13042 || itemId == 13043 || itemId == 13044) && owner.getReflection().getInstancedZoneId() != ReflectionTable.PAILAKA_DEVILS_LEGACY)
			return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;

		// Для квеста _144_PailakaInjuredDragon
		// SPEAR = 13052; ENCHSPEAR = 13053; LASTSPEAR = 13054;
		if((itemId == 13052 || itemId == 13053 || itemId == 13054) && owner.getReflection().getInstancedZoneId() != ReflectionTable.PAILAKA_INJURED_DRAGON)
			return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;

		// Для квеста _196_SevenSignSealOfTheEmperor
		if(itemId == 15310 && owner.getReflection().getInstancedZoneId() != ReflectionTable.DISCIPLES_NECROPOLIS)
			return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;

		// Проверка для талисманов
		if(targetSlot == L2Item.SLOT_DECO)
		{
			if(_paperdoll[PAPERDOLL_RBRACELET] == null)
				return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;

			int talismans_count = 0;
			boolean fail = false;

			for(int slot = PAPERDOLL_DECO1; slot <= PAPERDOLL_DECO6; slot++)
				if(_paperdoll[slot] != null)
				{
					// при общей проверке не ищем одинаковые талисманы
					if(!checkAll && _paperdoll[slot].getItemId() == itemId)
						fail = true;
					talismans_count++;
				}

			if(fail)
				return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;
			if(talismans_count >= getMaxTalismanCount())
				return Msg.YOU_DO_NOT_MEET_THE_REQUIRED_CONDITION_TO_EQUIP_THAT_ITEM;
		}

		return null;
	}

	public int getMaxTalismanCount()
	{
		return (int) getOwner().calcStat(Stats.TALISMANS_LIMIT, 0, null, null);
	}

	public int getCloakStatus()
	{
		final L2Character owner = getOwner();
		if(owner == null)
			return 0;

		return (int) owner.calcStat(Stats.CLOAK_SLOT, 0, null, null);
	}
}
