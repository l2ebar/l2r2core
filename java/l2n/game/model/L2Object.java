package l2n.game.model;

import l2n.commons.lang.reference.HardReferences;
import l2n.commons.lang.reference.IHardReference;
import l2n.extensions.listeners.engine.DefaultListenerEngine;
import l2n.extensions.listeners.engine.ListenerEngine;
import l2n.extensions.listeners.engine.MethodInvokeListener;
import l2n.extensions.listeners.engine.PropertyChangeListener;
import l2n.extensions.listeners.events.MethodEvent;
import l2n.extensions.listeners.events.PropertyEvent;
import l2n.extensions.scripts.Events;
import l2n.extensions.scripts.Script;
import l2n.extensions.scripts.ScriptObject;
import l2n.extensions.scripts.Scripts;
import l2n.game.ai.L2CharacterAI;
import l2n.game.geodata.GeoEngine;
import l2n.game.instancemanager.FortressSiegeManager;
import l2n.game.instancemanager.MercTicketManager;
import l2n.game.instancemanager.QuestManager;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.model.entity.vehicle.L2Ship;
import l2n.game.model.entity.vehicle.L2Vehicle;
import l2n.game.model.instances.*;
import l2n.game.model.quest.Quest;
import l2n.game.tables.ReflectionTable;
import l2n.game.tables.TerritoryTable;
import l2n.util.Location;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class L2Object
{
	private static final Logger _log = Logger.getLogger(L2Object.class.getName());

	protected long _reflection = Long.MIN_VALUE;

	protected int _objectId;
	protected Long _storedId;

	private int _x;
	private int _y;
	private int _z;

	private int _polyid;
	private String _polytype;

	private L2WorldRegion _currentRegion;

	protected boolean _hidden;

	public L2Object(final Integer objectId, final boolean putInStorage)
	{
		_objectId = objectId;
		_storedId = putInStorage ? L2ObjectsStorage.put(this) : L2ObjectsStorage.putDummy(this);
	}

	public L2Object(final Integer objectId)
	{
		this(objectId, objectId > 0);
	}

	public IHardReference<? extends L2Object> getRef()
	{
		return HardReferences.emptyRef();
	}

	private final void clearRef()
	{
		IHardReference<? extends L2Object> reference = getRef();
		if(reference != null)
			reference.clear();
	}

	public Object callScripts(final Script script, final Method method)
	{
		return callScripts(script, method, null, null);
	}

	public Object callScripts(final Script scriptClass, final Method method, final Object[] args)
	{
		return callScripts(scriptClass, method, args, null);
	}

	public Object callScripts(final Script scriptClass, final Method method, final Object[] args, final HashMap<String, Object> variables)
	{
		if(Scripts.loading)
			return null;

		ScriptObject o;
		try
		{
			o = scriptClass.newInstance();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "", e);
			return null;
		}

		if(variables != null && variables.size() > 0)
			for(final Map.Entry<String, Object> obj : variables.entrySet())
				try
				{
					o.setProperty(obj.getKey(), obj.getValue());
				}
				catch(final Exception e)
				{}

		try
		{
			if(o.isFunctions())
				o.setProperty("self", getStoredId());
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "", e);
		}

		method.setAccessible(true);
		final Object ret = args != null ? o.invokeMethod(method, args) : o.invokeMethod(method);
		try
		{
			if(o.isFunctions())
				o.setProperty("self", null);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "", e);
		}

		return ret;
	}

	public Object callScripts(final String _class, final String method)
	{
		return callScripts(_class, method, null, null);
	}

	public Object callScripts(final String _class, final String method, final Object[] args)
	{
		return callScripts(_class, method, args, null);
	}

	public Object callScripts(final String _class, final String method, final Object[] args, final HashMap<String, Object> variables)
	{
		if(Scripts.loading)
			return null;

		final Script scriptClass = Scripts.getInstance().getClasses().get(_class);
		if(scriptClass == null)
		{
			_log.info("Script class " + _class + " not found");
			return null;
		}

		ScriptObject o;
		try
		{
			o = scriptClass.newInstance();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "", e);
			return null;
		}

		if(variables != null && variables.size() > 0)
			for(final Entry<String, Object> obj : variables.entrySet())
				try
				{
					o.setProperty(obj.getKey(), obj.getValue());
				}
				catch(final Exception e)
				{}

		try
		{
			o.setProperty("self", getStoredId());
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "", e);
		}

		final Object ret = args == null ? o.invokeMethod(method) : o.invokeMethod(method, args);
		try
		{
			o.setProperty("self", null);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "", e);
		}

		return ret;
	}

	public Reflection getReflection()
	{
		Reflection result = ReflectionTable.getInstance().get(_reflection);
		if(result == null)
		{
			result = ReflectionTable.getInstance().getDefault();
			_reflection = result.getId();
		}
		return result;
	}

	public long getReflectionId()
	{
		return _reflection;
	}

	public void setReflection(final long i)
	{
		if(_reflection == i)
			return;

		boolean blink = false;
		if(!_hidden)
		{
			decayMe();
			blink = true;
		}

		if(_reflection > 0)
			getReflection().removeObject(this);

		_reflection = i;

		if(_reflection > 0)
			getReflection().addObject(this);

		if(blink)
			spawnMe();
	}

	public void setReflection(final Reflection i)
	{
		setReflection(i.getId());
	}

	@Override
	public final int hashCode()
	{
		return _objectId;
	}

	public final int getObjectId()
	{
		return _objectId;
	}

	public final Long getStoredId()
	{
		return _storedId;
	}

	public int getX()
	{
		return _x;
	}

	public int getY()
	{
		return _y;
	}

	public int getZ()
	{
		return _z;
	}

	public String getBackCoords()
	{
		return getX() + " " + getY() + " " + getZ() + " " + getReflection().getId();
	}

	public void setPolyInfo(final String polytype, final int polyid)
	{
		_polytype = polytype;
		_polyid = polyid;
	}

	public void setPolyInfo(final String polytype, final String polyid)
	{
		_polytype = polytype;
		_polyid = Integer.parseInt(polyid);
	}

	public Location getLoc()
	{
		return new Location(_x, _y, _z, getHeading());
	}

	public int getGeoZ(final Location loc)
	{
		if(isFlying() || isInWater() || isInVehicle() || isVehicle() || isDoor())
			return loc.z;
		if(isNpc())
		{
			final L2Spawn spawn = ((L2NpcInstance) this).getSpawn();
			if(spawn != null && spawn.getLocx() == 0 && spawn.getLocy() == 0)
				return GeoEngine.getHeight(loc);
			return loc.z;
		}
		return GeoEngine.getHeight(loc);
	}

	public void setLoc(final Location loc)
	{
		setXYZ(loc.x, loc.y, loc.z);
	}

	public void setXYZ(int x, int y, int z)
	{
		if(!L2World.validCoords(x, y))
			if(isPlayer())
			{
				_log.warning("Player " + this + " (" + _objectId + ") at bad coords: (" + getX() + ", " + getY() + ").");
				final L2Player player = getPlayer();
				player.abortAttack();
				player.abortCast();
				player.sendActionFailed();
				player.stopMove(true, true);
				player.teleToClosestTown();
				return;
			}
			else if(isNpc())
			{
				final L2Spawn spawn = ((L2NpcInstance) this).getSpawn();
				if(spawn == null)
					return;
				if(spawn.getLocx() != 0)
				{
					x = spawn.getLocx();
					y = spawn.getLocy();
					z = spawn.getLocz();
				}
				else
				{
					final int p[] = TerritoryTable.getInstance().getRandomPoint(spawn.getLocation());
					x = p[0];
					y = p[1];
					z = p[2];
				}
			}
			else if(isCharacter())
			{
				decayMe();
				return;
			}

		_x = x;
		_y = y;
		_z = z;

		L2World.addVisibleObject(this, null);
	}

	public void setXYZInvisible(final Location loc)
	{
		setXYZInvisible(loc.x, loc.y, loc.z);
	}

	public void setXYZInvisible(int x, int y, int z)
	{
		if(x > L2World.MAP_MAX_X)
			x = L2World.MAP_MAX_X - 5000;
		if(x < L2World.MAP_MIN_X)
			x = L2World.MAP_MIN_X + 5000;
		if(y > L2World.MAP_MAX_Y)
			y = L2World.MAP_MAX_Y - 5000;
		if(y < L2World.MAP_MIN_Y)
			y = L2World.MAP_MIN_Y + 5000;
		if(z < -32768 || z > 32768)
			z = 0;

		_x = x;
		_y = y;
		_z = z;

		_hidden = true;
	}

	public boolean isPolymorphed()
	{
		return _polytype != null;
	}

	public String getPolytype()
	{
		return _polytype;
	}

	public int getPolyid()
	{
		return _polyid;
	}

	public final boolean isVisible()
	{
		return !_hidden;
	}

	public boolean isInvisible()
	{
		return false;
	}

	public void dropMe(final L2Character dropper, final Location loc)
	{
		if(dropper != null)
			setReflection(dropper.getReflection());

		_hidden = false;

		_x = loc.x;
		_y = loc.y;
		_z = getGeoZ(loc);

		L2World.addVisibleObject(this, dropper);
	}

	public final void spawnMe(final Location loc)
	{
		if(loc.x > L2World.MAP_MAX_X)
			loc.x = L2World.MAP_MAX_X - 5000;
		if(loc.x < L2World.MAP_MIN_X)
			loc.x = L2World.MAP_MIN_X + 5000;
		if(loc.y > L2World.MAP_MAX_Y)
			loc.y = L2World.MAP_MAX_Y - 5000;
		if(loc.y < L2World.MAP_MIN_Y)
			loc.y = L2World.MAP_MIN_Y + 5000;

		_x = loc.x;
		_y = loc.y;
		_z = getGeoZ(loc);
		setHeading(loc.h);

		spawnMe();
	}

	public void spawnMe()
	{
		_hidden = false;
		L2World.addVisibleObject(this, null);
	}

	public void toggleVisible()
	{
		if(isVisible())
			decayMe();
		else
			spawnMe();
	}

	public void onSpawn()
	{}

	public final boolean pickupMe(final L2Character target)
	{

		if(isItem())
		{
			final L2ItemInstance item = (L2ItemInstance) this;
			final int itemId = item.getItemId();

			if(itemId >= 3960 && itemId <= 4026 || itemId >= 5205 && itemId <= 5214 || itemId >= 6038 && itemId <= 6306 || itemId >= 6779 && itemId <= 6833 || itemId >= 7918 && itemId <= 8029)
				MercTicketManager.getInstance().removeTicket(item);

			if(target != null && target.isPlayer())
			{
				final L2Player player = (L2Player) target;

				if(item.getItem().isCombatFlag() && !FortressSiegeManager.checkIfCanPickup(player))
					return false;
				if(item.getItem().isTerritoryFlag())
					return false;
				if(itemId == 57 || itemId == 6353)
				{
					final Quest q = QuestManager.getQuest(255);
					if(q != null)
						player.processQuestEvent(q.getName(), "CE" + itemId, null);
				}
			}
			else if(FortressSiegeManager.isCombatFlag(itemId))
				return false;
		}

		_hidden = true;
		L2World.removeVisibleObject(this);

		return true;
	}

	public final void decayMe()
	{
		_hidden = true;
		L2World.removeVisibleObject(this);
		onDespawn();
	}

	protected void onDespawn()
	{

	}

	public void deleteMe()
	{
		decayMe();
		L2World.removeObject(this);
		L2ObjectsStorage.remove(_storedId);
		clearRef();
	}

	public void onAction(final L2Player player, final boolean shift)
	{
		if(Events.onAction(player, this, shift))
			return;

		player.sendActionFailed();
	}

	public void onForcedAttack(final L2Player player, final boolean shift)
	{
		player.sendActionFailed();
	}

	public boolean isAttackable(final L2Character attacker)
	{
		return false;
	}

	public abstract boolean isAutoAttackable(L2Character attacker);

	public boolean isMarker()
	{
		return false;
	}

	public String getL2ClassShortName()
	{
		return getClass().getName().replaceAll("^.*\\.(.*?)$", "$1");
	}

	private final long getXYDeltaSq(final int x, final int y)
	{
		final long dx = x - getX();
		final long dy = y - getY();
		return dx * dx + dy * dy;
	}

	private final long getXYDeltaSq(final Location loc)
	{
		return getXYDeltaSq(loc.x, loc.y);
	}

	private final long getZDeltaSq(final int z)
	{
		final long dz = z - getZ();
		return dz * dz;
	}

	private final long getXYZDeltaSq(final int x, final int y, final int z)
	{
		return getXYDeltaSq(x, y) + getZDeltaSq(z);
	}

	private final long getXYZDeltaSq(final Location loc)
	{
		return getXYDeltaSq(loc.x, loc.y) + getZDeltaSq(loc.z);
	}

	public final double getDistance(final int x, final int y)
	{
		return Math.sqrt(getXYDeltaSq(x, y));
	}

	public final double getDistance(final int x, final int y, final int z)
	{
		return Math.sqrt(getXYZDeltaSq(x, y, z));
	}

	public final double getDistance(final Location loc)
	{
		return getDistance(loc.x, loc.y, loc.z);
	}

	public final boolean isInRange(final L2Object obj, final long range)
	{
		if(obj == null)
			return false;
		final long dx = Math.abs(obj.getX() - getX());
		if(dx > range)
			return false;
		final long dy = Math.abs(obj.getY() - getY());
		if(dy > range)
			return false;
		final long dz = Math.abs(obj.getZ() - getZ());
		return dz <= 1500 && dx * dx + dy * dy <= range * range;
	}

	public final boolean isInRangeZ(final L2Object obj, final long range)
	{
		if(obj == null)
			return false;
		final long dx = Math.abs(obj.getX() - getX());
		if(dx > range)
			return false;
		final long dy = Math.abs(obj.getY() - getY());
		if(dy > range)
			return false;
		final long dz = Math.abs(obj.getZ() - getZ());
		return dz <= range && dx * dx + dy * dy + dz * dz <= range * range;
	}

	public final boolean isInRange(final Location loc, final long range)
	{
		if(loc == null)
		{
			_log.warning("Error detected!");
			_log.warning("Location is NULL!");
			return false;
		}
		return isInRangeSq(loc, range * range);
	}

	public final boolean isInRangeSq(final Location loc, final long range)
	{
		return getXYDeltaSq(loc) <= range;
	}

	public final boolean isInRangeZ(final Location loc, final long range)
	{
		return isInRangeZSq(loc, range * range);
	}

	public final boolean isInRangeZSq(final Location loc, final long range)
	{
		return getXYZDeltaSq(loc) <= range;
	}

	public final double getDistance(final L2Object obj)
	{
		if(obj == null)
			return 0;
		return Math.sqrt(getXYDeltaSq(obj.getX(), obj.getY()));
	}

	public final double getDistance3D(final L2Object obj)
	{
		if(obj == null)
			return 0;
		return Math.sqrt(getXYZDeltaSq(obj.getX(), obj.getY(), obj.getZ()));
	}

	public final double getRealDistance(final L2Object obj)
	{
		return getRealDistance3D(obj, true);
	}

	public final double getRealDistance3D(final L2Object obj)
	{
		return getRealDistance3D(obj, false);
	}

	public final double getRealDistance3D(final L2Object obj, final boolean ignoreZ)
	{
		double distance = ignoreZ ? getDistance(obj) : getDistance3D(obj);
		if(isCharacter())
			distance -= ((L2Character) this).getTemplate().collisionRadius;
		if(obj.isCharacter())
			distance -= ((L2Character) obj).getTemplate().collisionRadius;
		return distance > 0 ? distance : 0;
	}

	public final long getSqDistance(final int x, final int y)
	{
		return getXYDeltaSq(x, y);
	}

	public final long getSqDistance(final L2Object obj)
	{
		if(obj == null)
			return 0;
		return getXYDeltaSq(obj.getLoc());
	}

	public final boolean isInsideRadius(final L2Object object, final int radius, final boolean checkZ, final boolean strictCheck)
	{
		if(object == null)
			return false;

		return isInsideRadius(object.getX(), object.getY(), object.getZ(), radius, checkZ, strictCheck);
	}


	public final boolean isInsideRadius(final int x, final int y, final int radius, final boolean strictCheck)
	{
		return isInsideRadius(x, y, 0, radius, false, strictCheck);
	}

	public final boolean isInsideRadius(final int x, final int y, final int z, final int radius, final boolean checkZ, final boolean strictCheck)
	{
		final double dx = x - getX();
		final double dy = y - getY();
		final double dz = z - getZ();

		if(strictCheck)
		{
			if(checkZ)
				return dx * dx + dy * dy + dz * dz < radius * radius;

			return dx * dx + dy * dy < radius * radius;
		}

		if(checkZ)
			return dx * dx + dy * dy + dz * dz <= radius * radius;

		return dx * dx + dy * dy <= radius * radius;
	}

	public final boolean isOutsideRadius(final int x, final int y, final int radius)
	{
		final double dx = x - getX();
		final double dy = y - getY();

		return dx * dx + dy * dy >= radius * radius;
	}

	public L2Player getPlayer()
	{
		return null;
	}

	public int getHeading()
	{
		return 0;
	}

	public int getMoveSpeed()
	{
		return 0;
	}

	public boolean isInWater()
	{
		return false;
	}

	public void clearTerritories()
	{}

	public L2WorldRegion getCurrentRegion()
	{
		return _currentRegion;
	}

	public void setCurrentRegion(final L2WorldRegion region)
	{
		_currentRegion = region;
	}

	public L2CharacterAI getAI()
	{
		return null;
	}

	public boolean hasAI()
	{
		return false;
	}

	public boolean inObserverMode()
	{
		return false;
	}

	public boolean isInOlympiadMode()
	{
		return false;
	}

	public void startAttackStanceTask()
	{}

	public float getColRadius()
	{
		_log.log(Level.WARNING, "L2Object: getColRadius called directly from L2Object", new Exception("Stack trace"));
		return 0;
	}

	public float getColHeight()
	{
		_log.log(Level.WARNING, "L2Object: getColHeight called directly from L2Object", new Exception("Stack trace"));
		return 0;
	}

	public void setHeading(final int heading)
	{}

	@Override
	protected void finalize()
	{
		if(_reflection > 0)
			getReflection().removeObject(this);
	}

	public boolean isInVehicle()
	{
		return false;
	}

	public boolean isFlying()
	{
		return false;
	}

	public boolean isCharacter()
	{
		return false;
	}

	public boolean isPlayable()
	{
		return false;
	}

	public boolean isPlayer()
	{
		return false;
	}

	public boolean isPet()
	{
		return false;
	}

	public boolean isSummon()
	{
		return false;
	}

	public boolean isSummon0()
	{
		return false;
	}

	public boolean isMonster()
	{
		return false;
	}

	public boolean isNpc()
	{
		return false;
	}

	public boolean isStaticObject()
	{
		return false;
	}

	public boolean isItem()
	{
		return this instanceof L2ItemInstance;
	}

	public boolean isRaid()
	{
		return this instanceof L2RaidBossInstance && !(this instanceof L2ReflectionBossInstance);
	}

	public boolean isSpecialMonster()
	{
		return this instanceof L2SpecialMonsterInstance;
	}

	public boolean isMinion()
	{
		return this instanceof L2MinionInstance;
	}

	public boolean isBoss()
	{
		return this instanceof L2BossInstance;
	}

	public boolean isTrap()
	{
		return this instanceof L2TrapInstance;
	}

	public boolean isChest()
	{
		return this instanceof L2ChestInstance;
	}

	public boolean isDoor()
	{
		return this instanceof L2DoorInstance;
	}

	public boolean isArtefact()
	{
		return this instanceof L2ArtefactInstance;
	}

	public boolean isSiegeGuard()
	{
		return this instanceof L2SiegeGuardInstance;
	}

	public boolean isVehicle()
	{
		return this instanceof L2Vehicle;
	}

	public boolean isShip()
	{
		return this instanceof L2Ship;
	}

	public boolean isAirShip()
	{
		return this instanceof L2AirShip;
	}

	public boolean isGuard()
	{
		return this instanceof L2GuardInstance;
	}

	public boolean canBeWhithIcon()
	{
		return this instanceof L2MerchantInstance || this instanceof L2ManorManagerInstance || this instanceof L2WarehouseInstance || this instanceof L2SignsPriestInstance || this instanceof L2MercManagerInstance || this instanceof L2TrainerInstance || this instanceof L2VillageMasterInstance;
	}

	public String getName()
	{
		return super.getClass().getSimpleName() + ":" + _objectId;
	}

	private DefaultListenerEngine<L2Object> listenerEngine;

	public void addPropertyChangeListener(final PropertyChangeListener listener)
	{
		getListenerEngine().addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(final PropertyChangeListener listener)
	{
		getListenerEngine().removePropertyChangeListener(listener);
	}

	public void addPropertyChangeListener(final String value, final PropertyChangeListener listener)
	{
		getListenerEngine().addPropertyChangeListener(value, listener);
	}

	public void removePropertyChangeListener(final String value, final PropertyChangeListener listener)
	{
		getListenerEngine().removePropertyChangeListener(value, listener);
	}

	public void firePropertyChanged(final String value, final Object oldValue, final Object newValue)
	{
		getListenerEngine().firePropertyChanged(value, this, oldValue, newValue);
	}

	public void firePropertyChanged(final PropertyEvent event)
	{
		getListenerEngine().firePropertyChanged(event);
	}

	public void addProperty(final String property, final Object value)
	{
		getListenerEngine().addProperty(property, value);
	}

	public Object getProperty(final String property)
	{
		return getListenerEngine().getProperty(property);
	}

	public void addMethodInvokeListener(final MethodInvokeListener listener)
	{
		getListenerEngine().addMethodInvokedListener(listener);
	}

	public void addMethodInvokeListener(final String methodName, final MethodInvokeListener listener)
	{
		getListenerEngine().addMethodInvokedListener(methodName, listener);
	}

	public void removeMethodInvokeListener(final MethodInvokeListener listener)
	{
		getListenerEngine().removeMethodInvokedListener(listener);
	}

	public void removeMethodInvokeListener(final String methodName, final MethodInvokeListener listener)
	{
		getListenerEngine().removeMethodInvokedListener(methodName, listener);
	}

	public void removeMethodInvokeListener(final String methodName)
	{
		getListenerEngine().removeMethodInvokedListener(methodName);
	}

	public void fireMethodInvoked(final MethodEvent event)
	{
		getListenerEngine().fireMethodInvoked(event);
	}

	public void fireMethodInvoked(final String methodName, final Object[] args)
	{
		getListenerEngine().fireMethodInvoked(methodName, this, args);
	}

	public ListenerEngine<L2Object> getListenerEngine()
	{
		if(listenerEngine == null)
			listenerEngine = new DefaultListenerEngine<L2Object>(this);
		return listenerEngine;
	}
}
