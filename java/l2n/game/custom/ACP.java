package l2n.game.custom;

import l2n.Config;
import l2n.game.handler.ItemHandler;
import l2n.game.handler.VoicedCommandHandler;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;

import java.util.HashMap;
import java.util.logging.Level;

import static l2n.game.L2GameThreadPools._log;

public class ACP implements IVoicedCommandHandler {
    private static final ACP instance = new ACP();
    private static final String[] COMMANDS =
            {
                    "acpon",
                    "acpoff"
            };
    // *********************** ИД Банок

    protected static int ID_HEAL_CP = 5592;
    protected static int ID_HEAL_MP = 728;
    protected static int ID_HEAL_HP = 1539;
    // *********************** USE FULL
    // Включить / Выключить
   // private static boolean ACP_ON = true;
    // Минимальный уровень, с которого можно использовать ACP
    protected static int ACP_MIN_LVL = 0;
    // Указываеться в % уровень, с которого начинаем регенить
    protected static int ACP_HP_LVL = 99;
    protected static int ACP_CP_LVL = 99;
    protected static int ACP_MP_LVL = 99;
    protected static int ACP_MILI_SECONDS_FOR_LOOP = 1000;
    // Что именно автоматически регеним
    protected static boolean ACP_CP = true;
    protected static boolean ACP_MP = true;
    protected static boolean ACP_HP = true;
    protected static final HashMap<String, Thread> userAcpMap = new HashMap<>();

    @Override
    public String[] getVoicedCommandList() {
        return COMMANDS;
    }

    @Override
    public boolean useVoicedCommand(String command, L2Player activeChar, String target) {
        // Есть ли такой вообще в мире
        if (activeChar == null) {
            return false;
        }

        if (command.equals("acpon")) {
            if (!Config.SERVICES_ACP_ENABLE) {
                activeChar.sendMessage("Функция отключена на сервере!");
                return false;
            }

            if (userAcpMap.containsKey(activeChar.toString())) {
                activeChar.sendMessage("Уже включено!");
            } else {
                activeChar.sendMessage("Acp включен!");
                Thread t = new Thread(new AcpHealer(activeChar));
                userAcpMap.put(activeChar.toString(), t);
                t.start();
                return true;
            }
        } else if (command.equals("acpoff")) {
            if (!userAcpMap.containsKey(activeChar.toString())) {
                activeChar.sendMessage("Не было включено");
            } else {
                // here we get thread and remove it from map and interrupt it
                userAcpMap.remove(activeChar.toString()).interrupt();
                activeChar.sendMessage("Отключено");
            }
        }
        return false;
    }

    private static class AcpHealer implements Runnable {

        L2Player activeChar;

        public AcpHealer(L2Player activeChar) {
            this.activeChar = activeChar;
        }

        @Override
        public void run() {
            try {
                while (true) {
                    L2ItemInstance cpBottle = activeChar.getInventory().getItemByItemId(ID_HEAL_CP);
                    L2ItemInstance hpBottle = activeChar.getInventory().getItemByItemId(ID_HEAL_HP);
                    L2ItemInstance mpBottle = activeChar.getInventory().getItemByItemId(ID_HEAL_MP);

                    if ((hpBottle != null) && (hpBottle.getCount() > 0)) {
                        // Проверяем  уровень здоровья
                        if ((((activeChar.getCurrentHp() / activeChar.getMaxHp()) * 100) < ACP_HP_LVL) && ACP_HP) {

                            IItemHandler handlerHP = ItemHandler.getInstance().getItemHandler(hpBottle.getItemId());
                            if (handlerHP != null) {
                                handlerHP.useItem(activeChar, hpBottle);

                                activeChar.sendMessage("ACP: Восстановлено HP");
                            }
                        }
                        // Проверяем  уровень CP
                        if ((cpBottle != null) && (cpBottle.getCount() > 0)) {
                            if ((((activeChar.getCurrentCp() / activeChar.getMaxCp()) * 100) < ACP_CP_LVL) && ACP_CP) {
                                    IItemHandler handlerCP = ItemHandler.getInstance().getItemHandler(cpBottle.getItemId());
                                if (handlerCP != null) {
                                    handlerCP.useItem(activeChar, cpBottle);
                                    activeChar.sendMessage("ACP: Восстановлено CP");
                                }
                            }
                        }
                        // Проверяем  уровень MP
                        if ((mpBottle != null) && (mpBottle.getCount() > 0)) {
                            if ((((activeChar.getCurrentMp() / activeChar.getMaxMp()) * 100) < ACP_MP_LVL) && ACP_MP) {
                                IItemHandler handlerMP = ItemHandler.getInstance().getItemHandler(mpBottle.getItemId());
                                if (handlerMP != null) {
                                    handlerMP.useItem(activeChar, mpBottle);
                                    activeChar.sendMessage("ACP: Восстановлено MP");
                                }
                            }
                        }
                    } else {
                        activeChar.sendMessage("У Вас нечем регениться");
                        return;
                    }


                    Thread.sleep(ACP_MILI_SECONDS_FOR_LOOP);
                }
            } catch (InterruptedException e) {
                System.out.println(e+"тут");
            } catch (Exception e) {
                _log.log(Level.WARNING, e.getMessage());
                Thread.currentThread().interrupt();
            } finally {
                userAcpMap.remove(activeChar.toString());
            }
        }

    }

    public void init() {
        if (ACP_ON()) {
            VoicedCommandHandler.getInstance().registerVoicedCommandHandler(this);
            _log.info("ACPService: Loaded");
        }
    }

    private boolean ACP_ON() {
        return Config.SERVICES_ACP_ENABLE;
    }
    public static ACP getInstance()
    {
        return instance;
    }
}