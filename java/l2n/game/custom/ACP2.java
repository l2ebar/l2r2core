package l2n.game.custom;

import l2n.Config;
import l2n.commons.lang.reference.IHardReference;
import l2n.extensions.listeners.ICharacterListener;
import l2n.extensions.listeners.IPlayerListener;
import l2n.extensions.listeners.OnCurrentHpDamageListener;
import l2n.extensions.listeners.actor.OnPlayerEnterListener;
import l2n.extensions.listeners.actor.OnPlayerExitListener;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;

import l2n.game.L2GameThreadPools;

import l2n.game.handler.VoicedCommandHandler;

import l2n.game.handler.interfaces.IItemHandler;

import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.L2Skill;

import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;

import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.skills.Stats;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Date: 28.12.2015
 */
public class ACP2 extends Functions implements OnPlayerEnterListener, OnPlayerExitListener, IVoicedCommandHandler
{
    private static final Logger LOG = LoggerFactory.getLogger(ACP2.class);

    private static final long ACP_ACT_DELAY = 100;

    private String[] _commandList = { "acp" };

    private static final ACP2 instance = new ACP2();

    enum ACPType
    {
        HPACP("HP")
                {
                    @Override
                    public boolean isEnabled()
                    {
                        return Config.SERVICES_HPACP_ENABLE;
                    }

                    @Override
                    public void apply(final L2Player player)
                    {
                        for (final Object listener : player.getListeners().getListeners())
                        {
                            if (listener instanceof HPACPHelper)
                            {
                                return;
                            }
                        }
                        player.addListener(new HPACPHelper(player));
                    }

                    @Override
                    public void remove(final L2Player player)
                    {
                        for (final Object listener : player.getListeners().getListeners())
                        {
                            if (listener instanceof HPACPHelper)
                            {
                                player.getListeners().removeListener((ICharacterListener) listener);
                            }
                        }
                    }

                    @Override
                    public int[] getPotionsItemIds()
                    {
                        return Config.SERVICES_HPACP_POTIONS_ITEM_IDS;
                    }

                    @Override
                    protected int getActMinPercent()
                    {
                        return Config.SERVICES_HPACP_MIN_PERCENT;
                    }

                    @Override
                    protected int getActMaxPercent()
                    {
                        return Config.SERVICES_HPACP_MAX_PERCENT;
                    }

                    @Override
                    protected int getActDefPercent()
                    {
                        return Config.SERVICES_HPACP_DEF_PERCENT;
                    }
                },
        CPACP("CP")
                {
                    @Override
                    public boolean isEnabled()
                    {
                        return Config.SERVICES_CPACP_ENABLE;
                    }

                    @Override
                    public void apply(final L2Player player)
                    {
                        for (final Object listener :  player.getListeners().getListeners())
                        {
                            if (listener instanceof CPACPHelper)
                            {
                                return;
                            }
                        }

                        player.addListener(new CPACPHelper(player));
                    }

                    @Override
                    public void remove(final L2Player player)
                    {
                        for (final Object listener :  player.getListeners().getListeners())
                        {
                            if (listener instanceof CPACPHelper)
                            {
                                player.getListeners().removeListener((ICharacterListener) listener);
                            }
                        }
                    }

                    @Override
                    public int[] getPotionsItemIds()
                    {
                        return Config.SERVICES_CPACP_POTIONS_ITEM_IDS;
                    }

                    @Override
                    protected int getActMinPercent()
                    {
                        return Config.SERVICES_CPACP_MIN_PERCENT;
                    }

                    @Override
                    protected int getActMaxPercent()
                    {
                        return Config.SERVICES_CPACP_MAX_PERCENT;
                    }

                    @Override
                    protected int getActDefPercent()
                    {
                        return Config.SERVICES_CPACP_DEF_PERCENT;
                    }
                },
        MPACP("MP")
                {
                    @Override
                    public boolean isEnabled()
                    {
                        return Config.SERVICES_MPACP_ENABLE;
                    }

                    @Override
                    public void apply(final L2Player player)
                    {
                        for (final Object listener :  player.getListeners().getListeners())
                        {
                            if (listener instanceof MPACPHelper)
                            {
                                return;
                            }
                        }
                        player.addListener(new MPACPHelper(player));
                    }

                    @Override
                    public void remove(final L2Player player)
                    {
                        for (final Object listener :  player.getListeners().getListeners())
                        {
                            if (listener instanceof MPACPHelper)
                            {
                                player.getListeners().removeListener((ICharacterListener) listener);
                            }
                        }
                    }

                    @Override
                    public int[] getPotionsItemIds()
                    {
                        return Config.SERVICES_MPACP_POTIONS_ITEM_IDS;
                    }

                    @Override
                    protected int getActMinPercent()
                    {
                        return Config.SERVICES_MPACP_MIN_PERCENT;
                    }

                    @Override
                    protected int getActMaxPercent()
                    {
                        return Config.SERVICES_MPACP_MAX_PERCENT;
                    }

                    @Override
                    protected int getActDefPercent()
                    {
                        return Config.SERVICES_MPACP_DEF_PERCENT;
                    }
                };

        private final String cfgName;
        private static String ENABLE_VAR_NAME_SUFIX = "Enabled";
        private static String ACT_PERCENT_VAR_NAME_VAR_NAME_SUFIX = "ActPercent";
        public static final ACPType[] VALUES = values();

        private ACPType(final String cfgName) {
            this.cfgName = cfgName;
        }

        public String getCfgName() {
            return cfgName;
        }

        public abstract boolean isEnabled();

        public boolean isEnabled(final L2Player player) {
            return isEnabled() && player.getVarB(name() + ACPType.ENABLE_VAR_NAME_SUFIX, false);
        }

        public int getActPercent(final L2Player player) {
            final int percent = player.getVarInt(name() + ACPType.ACT_PERCENT_VAR_NAME_VAR_NAME_SUFIX, getActDefPercent());
            return Math.min(Math.max(getActMinPercent(), percent), getActMaxPercent());
        }

        public int setActPercent(final L2Player player, int percent) {
            percent = Math.min(Math.max(getActMinPercent(), percent), getActMaxPercent());
            player.setVar(name() + ACPType.ACT_PERCENT_VAR_NAME_VAR_NAME_SUFIX, String.valueOf(percent), -1);
            return percent;
        }

        public void setEnabled(final L2Player player, final boolean enabled) {
            if (enabled) {
                player.setVar(name() + ACPType.ENABLE_VAR_NAME_SUFIX, "true", -1);
                return;
            }
            player.unsetVar(name() + ACPType.ENABLE_VAR_NAME_SUFIX);
            player.unsetVar(name() + ACPType.ACT_PERCENT_VAR_NAME_VAR_NAME_SUFIX);
        }

        public abstract void apply(final L2Player p0);

        public abstract void remove(final L2Player p0);

        public abstract int[] getPotionsItemIds();

        protected abstract int getActMinPercent();

        protected abstract int getActMaxPercent();

        protected abstract int getActDefPercent();
    }

    private static abstract class ACPHelper implements Runnable, IPlayerListener
    {
        private final IHardReference<L2Player> _pleyerRef;
        private final AtomicReference<ACPHelperState> _state;
        private final ACP2.ACPType _type;

        static enum ACPHelperState
        {
            IDLE,  APPLY,  USE
        }

        protected ACPHelper(L2Player player, ACP2.ACPType type)
        {
            _type = type;
            _pleyerRef = player.getRef();
            _state = new AtomicReference<>(ACPHelperState.IDLE);
        }

        protected L2Player getPlayer()
        {
            return _pleyerRef.get();
        }

        public ACP2.ACPType getType()
        {
            return _type;
        }

        protected void act(L2Player player)
        {
            if (getPlayer() != player)
            {
                if (player != null) {
                    player.removeListener(this);
                }
                if (getPlayer() != null) {
                    getPlayer().removeListener(this);
                }
                return;
            }
            if (_state.compareAndSet(ACPHelperState.IDLE, ACPHelperState.APPLY)) {
                schedule(ACP_ACT_DELAY);
            }
        }

        @Override
        public void run()
        {
            L2Player player = getPlayer();
            if (player == null) {
                return;
            }
            try
            {
                if (_state.compareAndSet(ACPHelperState.APPLY, ACPHelperState.USE)) {
                    use(player);
                }
            }
            catch (Exception ex)
            {
                LOG.error("Exception in ACP helper task", ex);
                _state.set(ACPHelperState.IDLE);
            }
        }

        private void schedule(long delay)
        {
            L2GameThreadPools.getInstance().scheduleGeneral(this, delay);
        }

        protected boolean canUse(L2Player player)
        {
            return player != null && !(player.isDead() || player.isOutOfControl() || player.isInStoreMode()) && !(player.isFishing() || player.isHealHPBlocked(false) || player.isInOlympiadMode());
        }

        private void use(L2Player player)
        {
            if (!canUse(player))
            {
                _state.compareAndSet(ACPHelperState.USE, ACPHelperState.IDLE);
                return;
            }

            L2ItemInstance potionItem = findUsableItem(player);
            if (potionItem == null)
            {
                _state.compareAndSet(ACPHelperState.USE, ACPHelperState.IDLE);
                return;
            }

            if (potionItem.getItemId() != 13750) //Quick healing potion
            {
                useItem(player, potionItem);
                if (canUse(player) && _state.compareAndSet(ACPHelperState.USE, ACPHelperState.APPLY))
                {
                    schedule(ACP_ACT_DELAY);
                }
                else
                {
                    _state.compareAndSet(ACPHelperState.USE, ACPHelperState.IDLE);
                }
            }
            else if (_state.compareAndSet(ACPHelperState.USE, ACPHelperState.APPLY))
            {
                schedule(ACP_ACT_DELAY + 5000);
            }
        }

        private L2ItemInstance findUsableItem(L2Player player)
        {
            if (player.isInStoreMode() || player.isOutOfControl())
            {
                return null;
            }

            final int[] itemIds = _type.getPotionsItemIds();
            L2ItemInstance candidateItem = null;

            for (int itemId : itemIds)
            {
                L2ItemInstance item = player.getInventory().getItemByItemId(itemId);
                if (item != null)
                {
                    if (candidateItem == null)
                    {
                        candidateItem = item;
                    }
                }
            }
            return candidateItem;
        }

        private boolean useItem(L2Player player, L2ItemInstance item)
        {
            IItemHandler handler = item.getItem().getHandler();
            if (handler != null)
            {
                handler.useItem(player, item);
                return true;
            }
            return false;
        }
    }

    private static final class HPACPHelper extends ACP2.ACPHelper implements OnCurrentHpDamageListener
    {
        protected HPACPHelper(L2Player player)
        {
            super(player, ACP2.ACPType.HPACP);
        }

        @Override
        protected boolean canUse(L2Player player)
        {
            if (super.canUse(player))
            {
                final double useLim = player.getMaxHp() / 100.0 * getType().getActPercent(player);
                return player.getCurrentHp() < useLim;
            }
            return false;
        }

        @Override
        public void onCurrentHpDamage(L2Character actor, double damage, L2Character attacker, L2Skill skill, boolean awake, boolean standUp, boolean directHp)
        {
            L2Player player = actor.getPlayer();
            if (player == null)
            {
                actor.removeListener(this);
                return;
            }

            if (!getType().isEnabled(player))
            {
                actor.removeListener(this);
            }

            act(player);
        }
    }

    private static final class CPACPHelper extends ACP2.ACPHelper implements OnCurrentHpDamageListener
    {
        protected CPACPHelper(L2Player player)
        {
            super(player, ACP2.ACPType.CPACP);
        }

        @Override
        protected boolean canUse(L2Player player)
        {
            if (super.canUse(player))
            {
                final double useLim = player.calcStat(Stats.CP_LIMIT, 0, null,null) * player.getMaxCp() / 100.0 * (getType().getActPercent(player) / 100.0);
                return player.getCurrentCp() < useLim;
            }
            return false;
        }

        @Override
        public void onCurrentHpDamage(L2Character actor, double damage, L2Character attacker, L2Skill skill, boolean awake, boolean standUp, boolean directHp)
        {
            L2Player player = actor.getPlayer();
            if (player == null)
            {
                actor.removeListener(this);
                return;
            }

            if (!getType().isEnabled(player))
            {
                actor.removeListener(this);
            }
            act(player);
        }
    }

    private static final class MPACPHelper extends ACP2.ACPHelper implements OnCurrentHpDamageListener
    {
        protected MPACPHelper(L2Player player)
        {
            super(player, ACP2.ACPType.MPACP);
        }

        @Override
        protected boolean canUse(L2Player player)
        {
            if (super.canUse(player))
            {
                final double useLim = player.calcStat(Stats.MP_LIMIT, 0, null,null) * player.getMaxMp() / 100.0 * (getType().getActPercent(player) / 100.0);
                return player.getCurrentMp() < useLim;
            }
            return false;
        }

        @Override
        public void onCurrentHpDamage(L2Character actor, double damage, L2Character attacker, L2Skill skill, boolean awake, boolean standUp, boolean directHp)
        {
            L2Player player = actor.getPlayer();
            if (player == null)
            {
                actor.removeListener(this);
                return;
            }

            if (!getType().isEnabled(player))
            {
                actor.removeListener(this);
            }
            act(player);
        }
    }

    @Override
    public String[] getVoicedCommandList()
    {
        return _commandList;
    }

    @Override
    public boolean useVoicedCommand(String command, L2Player player, String args)
    {
        if (isACPEnabled() && command.equalsIgnoreCase("acp"))
        {
            String[] param = args.split("\\s+");
            if (param.length > 1)
            {
                String type = param[0];
                String val = param[1];
                for (ACPType acpType : ACPType.VALUES)
                {
                    if (!acpType.isEnabled())
                    {
                        acpType.remove(player);
                    }
                    else if (acpType.getCfgName().equalsIgnoreCase(type))
                    {
                        if (val.equalsIgnoreCase("true") || val.equalsIgnoreCase("on") || val.equalsIgnoreCase("enable") ||
                                val.equalsIgnoreCase("1") || val.equalsIgnoreCase("yes"))
                        {
                            acpType.apply(player);
                            acpType.setEnabled(player, true);
                            player.addListener(new HPACPHelper(player));
                            player.addListener(new MPACPHelper(player));
                            player.addListener(new CPACPHelper(player));
                            player.sendMessage(new CustomMessage("services.ACP.Enabled", player, acpType.getCfgName()));
                        }
                        else if (val.equalsIgnoreCase("false") || val.equalsIgnoreCase("of") || val.equalsIgnoreCase("off") ||
                                val.equalsIgnoreCase("disable") || val.equalsIgnoreCase("0") || val.equalsIgnoreCase("no"))
                        {
                            acpType.remove(player);
                            acpType.setEnabled(player, false);
                            player.removeListener(new HPACPHelper(player));
                            player.removeListener(new MPACPHelper(player));
                            player.removeListener(new CPACPHelper(player));
                            player.sendMessage(new CustomMessage("services.ACP.Disabled", player, acpType.getCfgName()));
                        }
                        else if (acpType.isEnabled(player))
                        {
                            final int actPercent = acpType.setActPercent(player, NumberUtils.toInt(val, acpType.getActDefPercent()));
                            acpType.apply(player);
                            player.sendMessage(new CustomMessage("services.ACP.ActPercentSet", player, acpType.getCfgName(), actPercent));
                        }
                    }
                }
            }
            showACPInteface(player);
            return true;
        }
        return false;
    }

    private static void showACPInteface(L2Player player)
    {
        NpcHtmlMessage dialog = new NpcHtmlMessage(5);
        dialog.setFile("data/html/command/acp.htm");

        if (ACPType.HPACP.isEnabled(player))
        {
            dialog.replace("%hpap%", String.valueOf(ACPType.HPACP.getActPercent(player)) + "%");
        }
        else
        {
            dialog.replace("%hpap%", "Off");
        }

        if (ACPType.CPACP.isEnabled(player))
        {
            dialog.replace("%cpap%", String.valueOf(ACPType.CPACP.getActPercent(player)) + "%");
        }
        else
        {
            dialog.replace("%cpap%", "Off");
        }

        if (ACPType.MPACP.isEnabled(player))
        {
            dialog.replace("%mpap%", String.valueOf(ACPType.MPACP.getActPercent(player)) + "%");
        }
        else
        {
            dialog.replace("%mpap%", "Off");
        }

        player.sendPacket(dialog);
    }

    private static boolean isACPEnabled()
    {
        return Config.SERVICES_HPACP_ENABLE || Config.SERVICES_CPACP_ENABLE || Config.SERVICES_MPACP_ENABLE;
    }

    @Override
    public void onPlayerEnter(L2Player player)
    {
        if (isACPEnabled())
        {
            for (ACPType acpType : ACPType.VALUES)
            {
                if ((acpType.isEnabled()) && (acpType.isEnabled(player)))
                {
                    acpType.apply(player);
                }
            }
        }
    }

    @Override
    public void onPlayerExit(L2Player player)
    {
        if (isACPEnabled())
        {
            for (ACPType acpType : ACPType.VALUES)
            {
                acpType.remove(player);
            }
        }
    }

    public void init()
    {
        if (isACPEnabled())
        {
            VoicedCommandHandler.getInstance().registerVoicedCommandHandler(this);
            LOG.info("ACPService: Loaded");
        }
    }

        public static ACP2 getInstance()
        {
            return instance;
        }
}

