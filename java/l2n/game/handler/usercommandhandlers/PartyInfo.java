package l2n.game.handler.usercommandhandlers;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IUserCommandHandler;
import l2n.game.model.L2Party;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;

/**
 * Support for /partyinfo command
 */
public class PartyInfo implements IUserCommandHandler
{
	private static final int[] COMMAND_IDS = { 81 };

	@Override
	public boolean useUserCommand(int id, L2Player activeChar)
	{
		if(id != COMMAND_IDS[0])
			return false;

		if(!activeChar.isInParty())
			return false;

		L2Party playerParty = activeChar.getParty();
		int memberCount = playerParty.getMemberCount();
		int lootDistribution = playerParty.getLootDistribution();
		String partyLeader = playerParty.getPartyLeader().getName();

		activeChar.sendPacket(Msg._PARTY_INFORMATION_);

		switch (lootDistribution)
		{
			case L2Party.ITEM_LOOTER:
				activeChar.sendPacket(Msg.LOOTING_METHOD_FINDERS_KEEPERS);
				break;
			case L2Party.ITEM_ORDER:
				activeChar.sendPacket(Msg.LOOTING_METHOD_BY_TURN);
				break;
			case L2Party.ITEM_ORDER_SPOIL:
				activeChar.sendPacket(Msg.LOOTING_METHOD_BY_TURN_INCLUDING_SPOIL);
				break;
			case L2Party.ITEM_RANDOM:
				activeChar.sendPacket(Msg.LOOTING_METHOD_RANDOM);
				break;
			case L2Party.ITEM_RANDOM_SPOIL:
				activeChar.sendPacket(Msg.LOOTING_METHOD_RANDOM_INCLUDING_SPOIL);
				break;
		}

		activeChar.sendPacket(new SystemMessage(SystemMessage.PARTY_LEADER_S1).addString(partyLeader));
		activeChar.sendMessage(new CustomMessage("scripts.commands.user.PartyInfo.Members", activeChar).addNumber(memberCount));
		activeChar.sendPacket(Msg.__DASHES__);
		return true;
	}

	@Override
	public final int[] getUserCommandList()
	{
		return COMMAND_IDS;
	}
}
