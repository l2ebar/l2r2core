package l2n.game.handler;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.game.handler.interfaces.ICommandHandler;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.handler.itemhandlers.*;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;

/**
 * This class manages handlers of items
 */
public class ItemHandler implements ICommandHandler
{
	private final TIntObjectHashMap<IItemHandler> _datatable;

	/**
	 * Create ItemHandler if doesn't exist and returns ItemHandler
	 * 
	 * @return ItemHandler
	 */
	public static ItemHandler getInstance()
	{
		return SingletonHolder._instance;
	}

	/**
	 * Returns the number of elements contained in datatable
	 * 
	 * @return int : Size of the datatable
	 */
	@Override
	public int size()
	{
		return _datatable.size();
	}

	/**
	 * Constructor of ItemHandler
	 */
	private ItemHandler()
	{
		_datatable = new TIntObjectHashMap<IItemHandler>();

		registerItemHandler(new AttributeStones());
		registerItemHandler(new BallistaBomb());
		registerItemHandler(new BeastShot());
		registerItemHandler(new BlessedSpiritShot());
		registerItemHandler(new Book());
		registerItemHandler(new CharChangePotions());
		registerItemHandler(new ColorName());
		registerItemHandler(new CrystalCarol());
		registerItemHandler(new DoorKey());
		registerItemHandler(new EnchantScrolls());
		registerItemHandler(new ExtractableItems());
		registerItemHandler(new FishItem());
		registerItemHandler(new FishShots());
		registerItemHandler(new GrowthAccelerator());
		registerItemHandler(new Harvester());
		registerItemHandler(new HolyWater());
		registerItemHandler(new MagicBottle());
		registerItemHandler(new MercTicket());
		registerItemHandler(new PathfinderEquipment());
		registerItemHandler(new PetSummon());
		registerItemHandler(new Recipes());
		registerItemHandler(new RevitaPop());
		registerItemHandler(new RollingDice());
		registerItemHandler(new Seed());
		registerItemHandler(new SevenSignsRecord());
		registerItemHandler(new SkillLearnItems());
		registerItemHandler(new SoulBreakingArrow());
		registerItemHandler(new SoulCrystals());
		registerItemHandler(new SoulShots());
		registerItemHandler(new SpecialXMas());
		registerItemHandler(new SpiritLake());
		registerItemHandler(new SpiritShot());
		registerItemHandler(new TeleportBookmark());
		registerItemHandler(new WondrousCubic());
		registerItemHandler(new WorldMap());
		registerItemHandler(new PotionHandler());
	}

	/**
	 * Adds handler of item type in <I>datatable</I>.<BR>
	 * <BR>
	 * <B><I>Concept :</I></U><BR>
	 * This handler is put in <I>datatable</I> Map &lt;Integer ; IItemHandler &gt; for each ID corresponding to an item type
	 * (existing in classes of package itemhandlers) sets as key of the Map.
	 * 
	 * @param handler
	 *            (IItemHandler)
	 */
	public void registerItemHandler(final IItemHandler handler)
	{
		final int[] ids = handler.getItemIds(); // Get all ID corresponding to the item type of the handler
		for(final int element : ids) {
			try {
				if(element != -1) {
					L2Item template = ItemTable.getInstance().getTemplate(element);
					_datatable.put(element, handler); // Add handler for each ID found
					if (template != null && handler != null) {
						template.setHandler(handler);
					}
				}
			}catch (Exception e ){
				System.out.println(e);
			}

		}

	}

	/**
	 * Returns the handler of the item
	 * 
	 * @param itemId
	 *            : int designating the itemID
	 * @return IItemHandler
	 */
	public IItemHandler getItemHandler(final int itemId)
	{
		return _datatable.get(itemId);
	}

	public void clear()
	{
		_datatable.clear();
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final ItemHandler _instance = new ItemHandler();
	}
}
