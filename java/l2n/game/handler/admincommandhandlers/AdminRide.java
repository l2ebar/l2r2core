package l2n.game.handler.admincommandhandlers;

import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.NpcHtmlMessage;

import java.io.File;
import java.io.FileInputStream;

public class AdminRide implements IAdminCommandHandler
{
	private static String[] _adminCommands = { "admin_ride" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Admin)
			return false;

		if(command.startsWith("admin_ride"))
			try
			{
				final String val = command.substring(11);
				showRide(activeChar, val);
			}
			catch(final StringIndexOutOfBoundsException e)
			{
				// case of empty filename
			}

		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	// FIXME: implement method to send html to player in L2Player directly
	// PUBLIC & STATIC so other classes from package can include it directly
	public static void showRide(final L2Player targetChar, final String filename)
	{
		final File file = new File("data/html/admin/" + filename);
		FileInputStream fis = null;

		try
		{
			fis = new FileInputStream(file);
			final byte[] raw = new byte[fis.available()];
			fis.read(raw);

			final String content = new String(raw, "UTF-8");

			final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);

			adminReply.setHtml(content);
			targetChar.sendPacket(adminReply);
		}
		catch(final Exception e)
		{
			// problem with adminride is ignored
		}
		finally
		{
			try
			{
				if(fis != null)
					fis.close();
			}
			catch(final Exception e1)
			{
				// problems ignored
			}
		}
	}

	@Override
	public String[] getDescription()
	{
		return new String[] { "admin_ride" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] { AdminCommandHandler.GROUP_EDITCHAR };
	}
}
