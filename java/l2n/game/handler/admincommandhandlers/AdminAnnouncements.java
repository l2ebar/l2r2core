package l2n.game.handler.admincommandhandlers;

import l2n.Config;
import l2n.game.Announcements;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2ObjectProcedures.SendPacketProc;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.network.serverpackets.ExShowScreenMessage;
import l2n.game.network.serverpackets.ExShowScreenMessage.ScreenMessageAlign;

public class AdminAnnouncements implements IAdminCommandHandler
{
	private static final String[] _adminCommands = {
			"admin_list_announcements",
			"admin_announce_announcements",
			"admin_add_announcement",
			"admin_del_announcement",
			"admin_announce",
			"admin_a",
			"admin_announce_menu",
			"admin_crit_announce",
			"admin_c",
			"admin_toscreen",
			"admin_s" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Announcements)
			return false;

		final String[] args = command.split(" ");
		final Announcements a = Announcements.getInstance();

		if(args[0].equals("admin_list_announcements"))
			a.listAnnouncements(activeChar);
		else if(args[0].equals("admin_announce_menu"))
		{
			a.handleAnnounce(command, 20, null);
			a.listAnnouncements(activeChar);
		}
		else if(args[0].equals("admin_announce_announcements"))
		{
			for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
				a.showAnnouncements(player);
			a.listAnnouncements(activeChar);
		}
		else if(args[0].equals("admin_add_announcement"))
		{
			if(args.length < 2)
				return false;
			try
			{
				final String val = command.substring(23);
				a.addAnnouncement(val);
				a.listAnnouncements(activeChar);
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		}
		else if(args[0].equals("admin_del_announcement"))
		{
			if(args.length < 2)
				return false;
			try
			{
				final int val = new Integer(command.substring(23));
				a.delAnnouncement(val);
				a.listAnnouncements(activeChar);
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		}
		else if(args[0].equals("admin_announce") || args[0].equals("admin_a"))
			a.handleAnnounce(command, args[0].length() + 1, Config.GM_SHOW_ANNOUNCER_NAME ? activeChar.getName() : null);
		else if(args[0].equals("admin_crit_announce") || args[0].equals("admin_c"))
			a.handleAnnounce(command, args[0].length() + 1, Say2C.CRITICAL_ANNOUNCEMENT, Config.GM_SHOW_ANNOUNCER_NAME ? activeChar.getName() : null);
		else if(args[0].equals("admin_toscreen") || args[0].equals("admin_s"))
		{
			if(args.length < 2)
				return false;
			final String text = command.substring(args[0].length() + 1);
			final int time = 3000 + text.length() * 100; // 3 секунды + 100мс на символ
			final boolean font_big = text.length() < 64;

			final ExShowScreenMessage sm = new ExShowScreenMessage(text, time, ScreenMessageAlign.TOP_CENTER, font_big);
			L2ObjectsStorage.forEachPlayer(new SendPacketProc(sm));
		}

		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"file://com_list_announcements.htm",
				"file://com_announce_announcements.htm",
				"file://com_add_announcement.htm",
				"file://com_del_announcement.htm",
				"file://com_announce.htm",
				"file://com_a.htm",
				"file://com_announce_menu.htm",
				"file://com_crit_announce.htm",
				"file://com_crit_announce.htm",
				"file://com_toscreen.htm",
				"file://com_toscreen.htm" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER };
	}
}
