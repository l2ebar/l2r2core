package l2n.game.handler.admincommandhandlers;

import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;

public class AdminEvents implements IAdminCommandHandler
{
	private static String[] _adminCommands = { "admin_events" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().IsEventGm)
			return false;
		if(command.startsWith("admin_events"))
			AdminHelpPage.showHelpPage(activeChar, "events.htm");

		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] { "Отображает страницу управления эвентами" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] { AdminCommandHandler.GROUP_MANAGESERVER2 };
	}
}
