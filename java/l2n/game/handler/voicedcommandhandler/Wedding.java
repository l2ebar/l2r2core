package l2n.game.handler.voicedcommandhandler;

import l2n.Config;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlIntention;
import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.instancemanager.CoupleManager;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.Couple;
import l2n.game.network.clientpackets.ConfirmDlg;
import l2n.game.network.serverpackets.*;
import l2n.game.skills.AbnormalEffect;
import l2n.game.tables.SkillTable;
import l2n.util.Location;

import java.sql.ResultSet;

import static l2n.game.model.L2Zone.ZoneType.*;

public class Wedding implements IVoicedCommandHandler
{
	private static final String[] _voicedCommands = { "divorce", "engage", "gotolove" };

	/**
	 * (non-Javadoc)
	 * 
	 * @see l2n.game.handler.interfaces.IUserCommandHandler#useUserCommand(int, l2n.game.model.actor.L2Player)
	 */
	@Override
	public boolean useVoicedCommand(String command, L2Player activeChar, String target)
	{
		if(command.startsWith("engage"))
			return engage(activeChar);
		else if(command.startsWith("divorce"))
			return divorce(activeChar);
		else if(command.startsWith("gotolove"))
			return goToLove(activeChar);
		return false;
	}

	public boolean divorce(L2Player activeChar)
	{
		if(activeChar.getPartnerId() == 0)
			return false;

		int _partnerId = activeChar.getPartnerId();
		long AdenaAmount = 0;

		if(activeChar.isMaried())
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.Divorced", activeChar));
			AdenaAmount = Math.abs(activeChar.getAdena() / 100 * Config.WEDDING_DIVORCE_COSTS - 10);
			activeChar.reduceAdena(AdenaAmount, true);
		}
		else
			activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.Disengaged", activeChar));

		activeChar.setMaried(false);
		activeChar.setPartnerId(0);
		Couple couple = CoupleManager.getInstance().getCouple(activeChar.getCoupleId());
		couple.divorce();
		couple = null;

		L2Player partner;
		partner = (L2Player) L2ObjectsStorage.findObject(_partnerId);

		if(partner != null)
		{
			partner.setPartnerId(0);
			if(partner.isMaried())
				partner.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.PartnerDivorce", partner));
			else
				partner.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.PartnerDisengage", partner));
			partner.setMaried(false);

			// give adena
			if(AdenaAmount > 0)
				partner.addAdena(AdenaAmount);
		}
		return true;
	}

	public boolean engage(L2Player activeChar)
	{
		// check target
		if(activeChar.getTarget() == null)
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.NoneTargeted", activeChar));
			return false;
		}
		// check if target is a L2Player
		if(!activeChar.getTarget().isPlayer())
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.OnlyAnotherPlayer", activeChar));
			return false;
		}
		// check if player is already engaged
		if(activeChar.getPartnerId() != 0)
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.AlreadyEngaged", activeChar));
			if(Config.WEDDING_PUNISH_INFIDELITY)
			{
				activeChar.startAbnormalEffect(AbnormalEffect.BIG_HEAD); // give player a Big
				// Head
				// lets recycle the sevensigns debuffs
				int skillId;

				int skillLevel = 1;

				if(activeChar.getLevel() > 40)
					skillLevel = 2;

				if(activeChar.isMageClass())
					skillId = 4361;
				else
					skillId = 4362;

				L2Skill skill = SkillTable.getInstance().getInfo(skillId, skillLevel);

				if(activeChar.getEffectList().getFirstEffect(skill) == null)
				{
					skill.getEffects(activeChar, activeChar, false, false);
					SystemMessage sm = new SystemMessage(SystemMessage.YOU_CAN_FEEL_S1S_EFFECT);
					sm.addSkillName(skillId, (short) skillLevel);
					activeChar.sendPacket(sm);
				}
			}
			return false;
		}

		L2Player ptarget = (L2Player) activeChar.getTarget();

		// check if player target himself
		if(ptarget.getObjectId() == activeChar.getObjectId())
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.EngagingYourself", activeChar));
			return false;
		}

		if(ptarget.isMaried())
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.PlayerAlreadyMarried", activeChar));
			return false;
		}

		if(ptarget.getPartnerId() != 0)
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.PlayerAlreadyEngaged", activeChar));
			return false;
		}

		if(ptarget.isEngageRequest())
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.PlayerAlreadyAsked", activeChar));
			return false;
		}

		if(ptarget.getPartnerId() != 0)
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.PlayerAlreadyEngaged", activeChar));
			return false;
		}

		if(ptarget.getSex() == activeChar.getSex() && !Config.WEDDING_SAMESEX)
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.SameSex", activeChar));
			return false;
		}

		// check if target has player on friendlist
		boolean FoundOnFriendList = false;
		int objectId;
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement("SELECT friend_id FROM character_friends WHERE char_id=?");
			statement.setInt(1, ptarget.getObjectId());
			rset = statement.executeQuery();

			while (rset.next())
			{
				objectId = rset.getInt("friend_id");
				if(objectId == activeChar.getObjectId())
				{
					FoundOnFriendList = true;
					break;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		if(!FoundOnFriendList)
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.NotInFriendlist", activeChar));
			return false;
		}

		ptarget.setEngageRequest(true, activeChar.getObjectId());
		ptarget.sendMessage("Игрок " + activeChar.getName() + " хочет жениться на вас.");
		ptarget.sendPacket(new ConfirmDlgPacket(SystemMessage.S1_S2, 60000, ConfirmDlg.ENGAGE_ANSWER).addString("Игрок" + activeChar.getName() + " просит вашей руки. Вы хотите, чтобы начать новые отношения?"));
		return true;
	}

	public boolean goToLove(L2Player activeChar)
	{
		if(!activeChar.isMaried())
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.YoureNotMarried", activeChar));
			return false;
		}

		if(activeChar.getPartnerId() == 0)
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.PartnerNotInDB", activeChar));
			return false;
		}

		L2Player partner;
		partner = (L2Player) L2ObjectsStorage.findObject(activeChar.getPartnerId());
		if(partner == null)
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.PartnerOffline", activeChar));
			return false;
		}

		if(partner.isInOlympiadMode() || partner.isFestivalParticipant() || activeChar.isMovementDisabled() || activeChar.isMuted(null) || activeChar.isInOlympiadMode() || activeChar.getDuel() != null || activeChar.isFestivalParticipant())
		{
			activeChar.sendMessage(new CustomMessage("common.TryLater", activeChar));
			return false;
		}

		if(activeChar.isInParty() && activeChar.getParty().isInDimensionalRift() || partner.isInParty() && partner.getParty().isInDimensionalRift())
		{
			activeChar.sendMessage(new CustomMessage("common.TryLater", activeChar));
			return false;
		}

		if(activeChar.getTeleMode() != 0 || activeChar.getUnstuck() != 0)
		{
			activeChar.sendMessage(new CustomMessage("common.TryLater", activeChar));
			return false;
		}

		// "Нельзя вызывать персонажей в/Уз зоны свободного PvP"
		// "в зоны осад"
		// "на ОлУмпУйскУй стадУон"
		// "в зоны определенных рейд-боссов У эпУк-боссов"
		if(partner.isInZoneBattle() || partner.isInZone(Siege) || partner.isInZone(no_restart) || partner.isInZone(OlympiadStadia) || activeChar.isInZoneBattle() || activeChar.isInZone(Siege) || activeChar.isInZone(no_restart) || partner.getReflection().getId() != 0 || activeChar.isInZone(OlympiadStadia))
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.YOUR_TARGET_IS_IN_AN_AREA_WHICH_BLOCKS_SUMMONING));
			return false;
		}

		if(activeChar.getVarB("jailed?"))
		{
			activeChar.sendMessage("You are jailed.");
			return false;
		}

		activeChar.abortCast();
		activeChar.abortAttack();
		activeChar.sendActionFailed();
		activeChar.broadcastPacket(new StopMove(activeChar));
		activeChar.block();
		activeChar.setUnstuck(1);

		int teleportTimer = Config.WEDDING_TELEPORT_INTERVAL * 1000;

		if(activeChar.getInventory().getAdena() < Config.WEDDING_TELEPORT_PRICE)
		{
			activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			return false;
		}

		activeChar.reduceAdena(Config.WEDDING_TELEPORT_PRICE, true);

		activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Wedding.Teleport", activeChar).addNumber(teleportTimer / 60000));
		activeChar.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE, null, null);

		// SoE Animation section
		activeChar.broadcastPacket(new MagicSkillUse(activeChar, activeChar, 1050, 1, teleportTimer, 0));
		activeChar.sendPacket(new SetupGauge(0, teleportTimer));
		// End SoE Animation section

		// continue execution later
		L2GameThreadPools.getInstance().scheduleGeneral(new EscapeFinalizer(activeChar, partner.getLoc()), teleportTimer);
		return true;
	}

	static class EscapeFinalizer implements Runnable
	{
		private L2Player _activeChar;
		private Location _loc;

		EscapeFinalizer(L2Player activeChar, Location loc)
		{
			_activeChar = activeChar;
			_loc = loc;
		}

		@Override
		public void run()
		{
			if(_activeChar.isDead() || _activeChar.getUnstuck() == 0)
				return;
			_activeChar.unblock();
			_activeChar.setUnstuck(0);
			_activeChar.teleToLocation(_loc);
		}
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _voicedCommands;
	}
}
