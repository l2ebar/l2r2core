package l2n.game.handler.voicedcommandhandler;

import gnu.trove.iterator.TIntObjectIterator;
import l2n.database.utils.mysql;
import l2n.extensions.scripts.Functions;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.actor.L2Player;

public class Repair extends Functions implements IVoicedCommandHandler
{
	private static final String[] _commandList = new String[] { "repair" };

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

	@Override
	public boolean useVoicedCommand(String command, L2Player activeChar, String args)
	{
		if(args != null && !args.equals("") && !args.isEmpty())
		{
			repair(activeChar, args.trim());
			return true;
		}

		show("data/html/command/repair.htm", activeChar);
		return true;
	}

	public void repair(L2Player activeChar, String name)
	{
		if(!activeChar.getAccountChars().containsValue(name))
		{
			show("Вы не можете восстановить персонажа! зайдите на свой Аккаунт!", activeChar);
			return;
		}
		if(activeChar.getName().equalsIgnoreCase(name))
		{
			show("Вы не можете устранить сами!", activeChar);
			return;
		}

		for(TIntObjectIterator<String> iter = activeChar.getAccountChars().iterator(); iter.hasNext();)
		{
			iter.advance();
			int obj_id = iter.key();
			String char_name = iter.value();
			if(!name.equalsIgnoreCase(char_name))
				continue;
			int karma = mysql.simple_get_int("karma", "characters", "`obj_Id`=" + obj_id);
			if(karma > 0)
				mysql.set("UPDATE `characters` SET `x`='17144', `y`='170156', `z`='-3502', `heading`='0' WHERE `obj_Id`='" + obj_id + "' LIMIT 1");
			else
			{
				mysql.set("UPDATE `characters` SET `x`='0', `y`='0', `z`='0', `heading`='0' WHERE `obj_Id`='" + obj_id + "' LIMIT 1");
				mysql.set("UPDATE `items` SET `loc`='WAREHOUSE' WHERE `loc`='PAPERDOLL' AND `owner_id`=" + obj_id);
			}
			show("Персонаж восстановлен в ближайший Город,Все вещи с персонажа в приватном Банке.", activeChar);
			break;
		}
	}
}
