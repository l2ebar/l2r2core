package l2n.game.handler.voicedcommandhandler;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.L2Zone;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.network.clientpackets.ConfirmDlg;
import l2n.game.network.serverpackets.ConfirmDlgPacket;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.PremiumAccess;
import l2n.util.PremiumAccess.ServiceType;

public class Offline extends Functions implements IVoicedCommandHandler
{
	private static final String[] _commandList = new String[] { "offline", "ghost" };

	private static final ConfirmDlgPacket RU_DIALOG = new ConfirmDlgPacket(SystemMessage.S1_S2, 30000, ConfirmDlg.OFFLINE_ANSWER).addString(new CustomMessage("scripts.commands.user.offline.AskPlayer", "ru").toString());
	private static final ConfirmDlgPacket EN_DIALOG = new ConfirmDlgPacket(SystemMessage.S1_S2, 30000, ConfirmDlg.OFFLINE_ANSWER).addString(new CustomMessage("scripts.commands.user.offline.AskPlayer", "en").toString());

	@Override
	public boolean useVoicedCommand(String command, L2Player activeChar, String target)
	{
		if(!Config.SERVICES_OFFLINE_TRADE_ALLOW)
		{
			show(new CustomMessage("scripts.commands.user.offline.Disabled", activeChar), activeChar);
			return false;
		}
		if(activeChar.getOlympiadGameId() != -1 || Olympiad.isRegisteredInComp(activeChar))
		{
			activeChar.sendActionFailed();
			return false;
		}
		// проверка прав доступа для игроков без ПА
		if(!PremiumAccess.checkAccessService(activeChar, ServiceType.OFFLINE_TRADE))
			return false;

		if(!activeChar.isInStoreMode())
		{
			show(new CustomMessage("scripts.commands.user.offline.IncorrectUse", activeChar), activeChar);
			return false;
		}

		if(activeChar.getNoChannelRemained() > 0)
		{
			show(new CustomMessage("scripts.commands.user.offline.BanChat", activeChar), activeChar);
			return false;
		}

		if(activeChar.isActionBlocked(L2Zone.BLOCKED_ACTION_PRIVATE_STORE) || activeChar.isInJail())
		{
			activeChar.sendMessage(new CustomMessage("trade.OfflineNoTradeZone", activeChar));
			return false;
		}
		if(activeChar.getLevel() < Config.SERVICES_OFFLINE_TRADE_MIN_LEVEL)
		{
			show(new CustomMessage("scripts.commands.user.offline.LowLevel", activeChar).addNumber(Config.SERVICES_OFFLINE_TRADE_MIN_LEVEL), activeChar);
			return false;
		}
		activeChar.sendPacket(activeChar.isLangRus() ? RU_DIALOG : EN_DIALOG);
		return true;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}
