package l2n.game.handler.interfaces;

import l2n.game.model.actor.L2Playable;
import l2n.game.model.instances.L2ItemInstance;
import org.apache.commons.lang3.ArrayUtils;

public interface IItemHandler
{
	IItemHandler NULL = new IItemHandler()
	{
		@Override
		public void useItem(final L2Playable playable, final L2ItemInstance item)
		{

		}

		@Override
		public int[] getItemIds()
		{
			return ArrayUtils.EMPTY_INT_ARRAY;
		}
	};
	void useItem(L2Playable playable, L2ItemInstance item);

	int[] getItemIds();
}
