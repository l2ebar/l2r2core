package l2n.game.handler.itemhandlers;

import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.tables.ExtractableItemsTable;
import l2n.game.tables.ExtractableItemsTable.L2ExtractableItem;

/**
 * @author L2System Project
 * @date 27.10.2010
 * @time 16:49:47
 */
public class ExtractableItems implements IItemHandler
{
	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;

		L2Player player = playable.getPlayer();
		if(player == null || item == null || item.getCount() < 1)
			return;

		int itemID = item.getItemId();

		final L2ExtractableItem exitem = ExtractableItemsTable.getInstance().getExtractableItem(itemID);
		if(exitem == null)
			return;

		if(exitem.getType() == 0)
			exitem.getItemByChance(item, player);
		else if(exitem.getType() == 1)
			exitem.getAllItems(item, player);
	}

	@Override
	public int[] getItemIds()
	{
		return ExtractableItemsTable.getInstance().itemIDs();
	}
}
