package l2n.game.handler.itemhandlers;

import l2n.extensions.scripts.Functions;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.skills.skillclasses.EffectsFromSkills;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2Item;

public class PotionHandler implements IItemHandler {
    private static final int[] _itemIds = {
            5592,//GCP
            728,//MP
            1539//HP
    };

    @Override
    public void useItem(L2Playable playable, L2ItemInstance item) {
        if (playable == null || !playable.isPlayer())
            return;
        L2Player player = (L2Player) playable;

        int itemId = item.getItemId();

        if (player.isActionsDisabled()) {
            player.sendActionFailed();
            return;
        }

        switch (itemId) {
            case 5592:
                int skillId = 0;
                if (!player.isDead() && player.getCurrentCp() < player.getMaxCp()) {
                    skillId = 2166;
                    L2Skill skill = SkillTable.getInstance().getInfo(2166, 2);
                    player.getPlayer().doCast(skill, player, true);
                    Functions.removeItem(player, 5592, 1);
                    player.broadcastPacket(new MagicSkillUse(player, player, skillId, 1, 0, 0));

                }
            case 728:
                if (!player.isDead() && player.getCurrentMp() < player.getMaxMp()) {
                    skillId = 9007;
                    L2Skill skill = SkillTable.getInstance().getInfo(skillId, 1);
                    player.getPlayer().doCast(skill, player, true);
                    Functions.removeItem(player, 728, 1);
                    player.broadcastPacket(new MagicSkillUse(player, player, skillId, 1, 0, 0));
                }

                break;
            case 1539:
                if(!player.isDead() && player.getCurrentHp() < player.getMaxHp()){
                    skillId = 2037;
                    L2Skill skill = SkillTable.getInstance().getInfo(skillId, 1);
                    player.getPlayer().doCast(skill, player, true);
                    Functions.removeItem(player, 1539, 1);
                    player.broadcastPacket(new MagicSkillUse(player, player, skillId, 1, 0, 0));
                }


        }

    }

    @Override
    public int[] getItemIds() {
        return _itemIds;
    }
}
