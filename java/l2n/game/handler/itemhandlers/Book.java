package l2n.game.handler.itemhandlers;

import l2n.extensions.scripts.Functions;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.RadarControl;
import l2n.util.Location;

public class Book implements IItemHandler
{
	private static final int[] _itemIds = { 5588, 6317, 7561, 7063, 7064, 7065, 7066, 7082, 7083, 7084, 7085, 7086, 7087, 7088, 7089, 7090, 7091, 7092, 7093, 7094, 7095, 7096, 7097, 7098, 7099,
			7100, 7101, 7102, 7103, 7104, 7105, 7106, 7107, 7108, 7109, 7110, 7111, 7112 };

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(!playable.isPlayer())
			return;

		L2Player activeChar = (L2Player) playable;
		Functions.show("data/html/help/" + item.getItemId() + ".htm", activeChar);
		if(item.getItemId() == 7063)
			activeChar.sendPacket(new RadarControl(0, 2, new Location(51995, -51265, -3104)));
		activeChar.sendActionFailed();
	}

	@Override
	public int[] getItemIds()
	{
		return _itemIds;
	}
}
