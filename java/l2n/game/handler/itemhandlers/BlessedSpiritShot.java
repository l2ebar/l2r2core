package l2n.game.handler.itemhandlers;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExAutoSoulShot;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.templates.L2Weapon;
import l2n.util.ArrayUtil;

public class BlessedSpiritShot implements IItemHandler
{
	// all the items ids that this handler knowns
	private static final int[] ITEM_IDS = { 3947, 3948, 3949, 3950, 3951, 3952, 22072, 22073, 22074, 22075, 22076 };
	private static final short[] SKILL_IDS = { 2061, 2160, 2161, 2162, 2163, 2164 };

	private static final int[][] BSPIRIT_SHOTS = {
			{ 3947 }, // NONE grade
			{ 3948, 22072 }, // D grade
			{ 3949, 22073 }, // C grade
			{ 3950, 22074 }, // B grade
			{ 3951, 22075 }, // A grade
			{ 3952, 22076 }, // S grade
	};

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;

		L2Player player = (L2Player) playable;

		L2ItemInstance weaponInst = player.getActiveWeaponInstance();
		L2Weapon weaponItem = player.getActiveWeaponItem();
		int bspiritshot_id = item.getItemId();
		boolean auto = false;

		if(player.getAutoSoulShot().contains(bspiritshot_id))
			auto = true;

		// Check if Soul shot can be used
		if(weaponInst == null || weaponItem.getSpiritShotCount() == 0)
		{
			if(!auto)
				player.sendPacket(Msg.CANNOT_USE_SPIRITSHOTS);
			return;
		}

		// Check if Blessed SpiritShot is already active (it can be charged over SpiritShot)
		if(weaponInst.getChargedSpiritshot() != L2ItemInstance.CHARGED_NONE)
			return;

		// Check for correct grade
		int grade = weaponItem.getCrystalType().externalOrdinal;
		if(!ArrayUtil.arrayContains(BSPIRIT_SHOTS[grade], bspiritshot_id))
		{
			// wrong grade for weapon
			if(!auto)
				player.sendPacket(Msg.SPIRITSHOT_DOES_NOT_MATCH_WEAPON_GRADE);
			return;
		}

		long count = item.getCount();
		if(count < weaponItem.getSpiritShotCount())
		{
			if(auto)
			{
				player.removeAutoSoulShot(bspiritshot_id);
				player.sendPacket(new ExAutoSoulShot(bspiritshot_id, false));
				player.sendPacket(new SystemMessage(SystemMessage.THE_AUTOMATIC_USE_OF_S1_WILL_NOW_BE_CANCELLED).addItemName(bspiritshot_id));
			}
			else
				player.sendPacket(Msg.NOT_ENOUGH_SPIRITSHOTS);
			return;
		}

		weaponInst.setChargedSpiritshot(L2ItemInstance.CHARGED_BLESSED_SPIRITSHOT);
		if(!Config.INFINITE_BLESSEDSPIRITSHOT)
			player.getInventory().destroyItem(item, weaponItem.getSpiritShotCount(), false);
		player.sendPacket(Msg.POWER_OF_MANA_ENABLED);
		player.broadcastPacket(new MagicSkillUse(player, player, SKILL_IDS[grade], 1, 0, 0));
	}

	@Override
	public final int[] getItemIds()
	{
		return ITEM_IDS;
	}
}
