package l2n.game.event;

import l2n.game.model.actor.L2Player;
import l2n.util.HWID.HardwareID;

import java.util.concurrent.CopyOnWriteArrayList;

public final class HWIDChecker
{
	private CopyOnWriteArrayList<String> list;
	private final boolean checkHWID;

	public HWIDChecker(final boolean check)
	{
		checkHWID = check;
	}

	public final boolean canParticipate(final L2Player player)
	{
		// если проверять HardwareID не надо
		if(!checkHWID)
			return true;

		if(player == null || !player.isOnline() || !player.hasHWID())
			return false;

		final String hwid = player.getHWID().HWID;
		if(hwid == null)
			return false;

		// если новый список
		if(list == null)
		{
			list = new CopyOnWriteArrayList<String>();
			return list.add(hwid);
		}

		// уже есть такой
		if(list.contains(hwid))
			return false;

		return list.add(hwid);
	}

	public boolean contains(final L2Player player)
	{
		if(player == null || !player.isOnline() || !player.hasHWID())
			return false;

		final HardwareID hwid = player.getHWID();
		if(hwid == null)
			return false;
		if(list != null)
			return list.contains(hwid);
		return false;
	}

	public boolean remove(final L2Player player)
	{
		if(player == null || !player.isOnline() || !player.hasHWID())
			return false;

		if(list != null)
			return list.remove(player.getHWID());
		return false;
	}

	public final void clear()
	{
		if(list != null)
		{
			list.clear();
			list = null;
		}
	}
}
