package l2n.extensions.listeners.actor;

import l2n.extensions.listeners.ICharacterListener;
import l2n.game.model.actor.L2Player;


/**
 * @author bloodshed <a href="http://l2nextgen.ru/">L2NextGen</a>
 * @email rkx.bloodshed@gmail.com
 * @date 19.05.2012
 * @time 5:56:43
 */
public interface OnPlayerExitListener extends ICharacterListener
{
	void onPlayerExit(L2Player player);
}
