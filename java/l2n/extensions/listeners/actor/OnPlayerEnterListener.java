package l2n.extensions.listeners.actor;


import l2n.extensions.listeners.IPlayerListener;
import l2n.game.model.actor.L2Player;


public interface OnPlayerEnterListener extends IPlayerListener
{
    public void onPlayerEnter(L2Player player);
}
