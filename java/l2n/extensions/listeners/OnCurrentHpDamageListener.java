package l2n.extensions.listeners;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;

public interface OnCurrentHpDamageListener extends ICharacterListener
{
	void onCurrentHpDamage(L2Character actor, double damage, L2Character attacker, L2Skill skill, boolean awake, boolean standUp, boolean directHp);
}
