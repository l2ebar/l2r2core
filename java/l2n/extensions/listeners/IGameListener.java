package l2n.extensions.listeners;

import l2n.commons.listener.Listener;

/**
 * @author bloodshed <a href="http://l2nextgen.ru/">L2NextGen</a>
 * @email rkx.bloodshed@gmail.com
 * @date 04.05.2012
 * @time 7:41:01
 */
public interface IGameListener extends Listener
{

}
