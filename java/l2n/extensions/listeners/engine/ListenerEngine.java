package l2n.extensions.listeners.engine;

import l2n.extensions.listeners.events.MethodEvent;
import l2n.extensions.listeners.events.PropertyEvent;

import java.util.concurrent.LinkedBlockingQueue;

public interface ListenerEngine<T>
{

	public void addPropertyChangeListener(PropertyChangeListener listener);

	public void removePropertyChangeListener(PropertyChangeListener listener);

	public void addPropertyChangeListener(String value, PropertyChangeListener listener);

	public void removePropertyChangeListener(String value, PropertyChangeListener listener);

	public void firePropertyChanged(String value, T source, Object oldValue, Object newValue);

	public void firePropertyChanged(PropertyEvent event);

	public void addProperty(String property, Object value);

	public Object getProperty(String property);

	public T getOwner();

	public void addMethodInvokedListener(MethodInvokeListener listener);

	public void removeMethodInvokedListener(MethodInvokeListener listener);

	public void addMethodInvokedListener(String methodName, MethodInvokeListener listener);

	public void removeMethodInvokedListener(String methodName, MethodInvokeListener listener);

	public void fireMethodInvoked(MethodEvent event);

	public void fireMethodInvoked(String methodName, T source, Object[] args);

	public LinkedBlockingQueue<MethodInvokeListener> getMethodInvokedListeners();

	public LinkedBlockingQueue<PropertyChangeListener> getPropertyChangeListeners();

	public void removeMethodInvokedListener(String methodName);
}
