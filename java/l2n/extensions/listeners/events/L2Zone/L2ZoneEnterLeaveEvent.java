package l2n.extensions.listeners.events.L2Zone;

import l2n.extensions.listeners.events.DefaultMethodInvokeEvent;
import l2n.game.model.L2Zone;
import l2n.game.model.actor.L2Character;

public class L2ZoneEnterLeaveEvent extends DefaultMethodInvokeEvent
{
	public L2ZoneEnterLeaveEvent(String methodName, L2Zone owner, L2Character... args)
	{
		super(methodName, owner, args);
	}

	@Override
	public L2Zone getOwner()
	{
		return (L2Zone) super.getOwner();
	}

	@Override
	public L2Character[] getArgs()
	{
		return (L2Character[]) super.getArgs();
	}
}
