package l2n.extensions.listeners;

import l2n.commons.listener.Listener;
import l2n.commons.listener.ListenerList;


public interface IListenerContainer<C extends ListenerList<L>, L extends Listener>
{
	C getListeners();

	/**
	 * @param listener
	 * @return
	 */
	boolean addListener(final L listener);

	/**
	 * @param listener
	 * @return
	 */
	boolean removeListener(final L listener);

	/**
	 * Проверяем 'пустой' ли список, если пустой, то создаёт 'нормальный'
	 */
	void createListenerList();
}
