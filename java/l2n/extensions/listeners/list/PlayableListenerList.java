package l2n.extensions.listeners.list;

import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;

public class PlayableListenerList extends CharacterListenerList
{
	public PlayableListenerList(final L2Character actor)
	{
		super(actor);
	}

	@Override
	public L2Playable getActor()
	{
		return (L2Playable) _ref.get();
	}

	@Override
	public PlayableListenerList getPlayable()
	{
		return this;
	}
}
