package l2n.extensions.listeners.list;

import l2n.commons.lang.reference.HardReferences;
import l2n.commons.lang.reference.IHardReference;
import l2n.commons.listener.ListenerList;
import l2n.extensions.listeners.ICharacterListener;
import l2n.extensions.listeners.OnCurrentHpDamageListener;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import org.apache.commons.lang3.ArrayUtils;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author bloodshed <a href="http://l2nextgen.ru/">L2NextGen</a>
 * @email rkx.bloodshed@gmail.com
 * @date 03.05.2012
 * @time 22:05:26
 */
public class CharacterListenerList extends ListenerList<ICharacterListener>
{
	public boolean isEmptyList() {
		return false;
	}

	private static final class EmptyCharacterListenerList extends CharacterListenerList
	{
		public EmptyCharacterListenerList(final L2Character actor)
		{
			super(actor);
		}

		@Override
		public final Object[] getListeners()
		{
			return ArrayUtils.EMPTY_OBJECT_ARRAY;
		}

		@Override
		public boolean addListener(final ICharacterListener listener)
		{
			return false;
		}

		@Override
		public boolean removeListener(final ICharacterListener listener)
		{
			return false;
		}

		@Override
		public boolean isEmpty()
		{
			return true;
		}


	}



	private static final class EmptyPlayableListenerList extends PlayableListenerList
	{
		public EmptyPlayableListenerList(final L2Character actor)
		{
			super(actor);
		}

		@Override
		public final Object[] getListeners()
		{
			return ArrayUtils.EMPTY_OBJECT_ARRAY;
		}

		@Override
		public boolean addListener(final ICharacterListener listener)
		{
			return false;
		}

		@Override
		public boolean removeListener(final ICharacterListener listener)
		{
			return false;
		}

		@Override
		public boolean isEmpty()
		{
			return true;
		}


	}

	private static final class EmptyPlayerListenerList extends PlayerListenerList
	{
		public EmptyPlayerListenerList(final L2Character actor)
		{
			super(actor);
		}

		@Override
		public final Object[] getListeners()
		{
			if(listeners2 == null){
				return (this.listeners2 = new CopyOnWriteArrayList<>()).toArray();
			}
			return this.listeners2.toArray();
		}

		@Override
		public boolean addListener(final ICharacterListener listener)
		{
			if(listeners2 == null){
				this.listeners2=new CopyOnWriteArrayList<>();
			}
			return listeners2.addIfAbsent(listener);
		}

		@Override
		public boolean removeListener(final ICharacterListener listener)
		{
			return false;
		}

		@Override
		public boolean isEmpty()
		{
			return false;
		}


	}

	private static final CharacterListenerList CHARACTER_EMPTY_LIST = new EmptyCharacterListenerList(null);
	private static final PlayableListenerList PLAYABLE_EMPTY_LIST = new EmptyPlayableListenerList(null);
	private static final PlayerListenerList PLAYER_EMPTY_LIST = new EmptyPlayerListenerList(null);

	public static final CharacterListenerList emptyList()
	{
		return CHARACTER_EMPTY_LIST;
	}

	protected final IHardReference<? extends L2Character> _ref;

	public CharacterListenerList(final L2Character actor)
	{
		super(actor == null);
		_ref = actor == null ? HardReferences.<L2Character> emptyRef() : actor.getRef();
	}

	public L2Character getActor()
	{
		return _ref.get();
	}

	public CharacterListenerList getCharacter()
	{
		return this;
	}


	public PlayableListenerList getPlayable()
	{
		return PLAYABLE_EMPTY_LIST;
	}

	public PlayerListenerList getPlayer()
	{
		return PLAYER_EMPTY_LIST;
	}

	public void onCurrentHpDamage(L2Character player,final double damage, final L2Character attacker, final L2Skill skill, final boolean awake, final boolean standUp, final boolean directHp)
	{
		if(!isEmpty())
			for(final Object listener : getListeners())
				if(OnCurrentHpDamageListener.class.isInstance(listener))
					((OnCurrentHpDamageListener) listener).onCurrentHpDamage(player, damage, attacker, skill, awake, standUp, directHp);
	}

}
