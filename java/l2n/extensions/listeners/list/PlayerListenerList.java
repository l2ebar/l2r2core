package l2n.extensions.listeners.list;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.extensions.listeners.actor.OnPlayerEnterListener;
import l2n.extensions.listeners.actor.OnPlayerExitListener;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;


/**
 * @author bloodshed <a href="http://l2nextgen.ru/">L2NextGen</a>
 * @email rkx.bloodshed@gmail.com
 * @date 04.05.2012
 * @time 0:17:42
 */
public class PlayerListenerList extends PlayableListenerList
{
    public static final int HeroChatLaunched = "Say2C.HeroChatLaunched".hashCode();
    public static final int WhisperChatLaunched = "Say2C.WhisperChatLaunched".hashCode();
	public static final int TradeChatLaunched = "Say2C.TradeChatLaunched".hashCode();
	public static final int ShoutChatLaunched = "Say2C.ShoutChatLaunched".hashCode();
	public static final int EnteredNoLandingZone = "L2Zone.EnteredNoLandingZone".hashCode();

	protected TIntObjectHashMap<Object> properties;

	public PlayerListenerList(final L2Character actor)
	{
		super(actor);
	}

	@Override
	public L2Player getActor()
	{
		return (L2Player) _ref.get();
	}

	public void onEnter()
	{
		if(!isEmpty())
			for(final Object listener : getListeners())
				if(OnPlayerEnterListener.class.isInstance(listener))
					((OnPlayerEnterListener) listener).onPlayerEnter(getActor());
	}

	public void onExit()
	{
		if(!isEmpty())
			for(final Object listener : getListeners())
				if(OnPlayerExitListener.class.isInstance(listener))
					((OnPlayerExitListener) listener).onPlayerExit(getActor());
	}

	@Override
	public PlayerListenerList getPlayer()
	{
		return this;
	}
}
