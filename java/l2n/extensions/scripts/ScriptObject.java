package l2n.extensions.scripts;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("rawtypes")
public class ScriptObject
{
	private static final Logger _log = Logger.getLogger(ScriptObject.class.getName());
	private final Class<?> _class;
	private final Object _instance;

	public ScriptObject(final Class<?> c, final Object o)
	{
		_class = c;
		_instance = o;
	}

	public void setProperty(final String s, final Object o)
	{
		if(_class == null || _instance == null)
			return;
		try
		{
			final Field fld = _class.getField(s);
			if(fld == null)
				_log.warning("Class<?> " + getName() + " field " + s + " not found!");
			else if(!Modifier.isPublic(fld.getModifiers()))
				_log.warning("Class<?> " + getName() + " field " + s + " is not public!");
			else
			{
				fld.setAccessible(true);
				fld.set(_instance, o);
			}
		}
		catch(final NoSuchFieldException e)
		{
			_log.log(Level.WARNING, "Class<?> " + getName() + " field " + s + " not found or private!", e);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "Class<?> " + getName() + " field " + s + "  error!", e);
		}
	}

	public Object getProperty(final String s)
	{
		if(_class == null || _instance == null)
			return null;
		try
		{
			final Field fld = _class.getField(s);
			if(fld == null)
				_log.warning("Class<?> " + getName() + " field " + s + " not found!");
			else if(!Modifier.isPublic(fld.getModifiers()))
				_log.warning("Class<?> " + getName() + " field " + s + " is not public!");
			else
			{
				fld.setAccessible(true);
				return fld.get(_instance);
			}
		}
		catch(final NoSuchFieldException e)
		{
			_log.log(Level.WARNING, "Class<?> " + getName() + " field " + s + " not found or private!", e);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "Class<?> " + getName() + " field " + s + "  error!", e);
		}
		return null;
	}

	public Object invokeMethod(final Method m)
	{
		if(_class == null || _instance == null)
			return null;
		try
		{
			m.setAccessible(true);
			return m.invoke(Modifier.isStatic(m.getModifiers()) ? null : _instance);
		}
		catch(final InvocationTargetException f)
		{
			_log.log(Level.WARNING, "Class<?> " + getName() + " method " + m + " return a error!", f);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "Class<?> " + getName() + " method " + m + "  error!", e);
		}
		return null;
	}

	public Object invokeMethod(final Method m, final Object[] args)
	{
		if(_class == null || _instance == null)
			return null;
		try
		{
			m.setAccessible(true);
			return m.invoke(Modifier.isStatic(m.getModifiers()) ? null : _instance, args);
		}
		catch(final InvocationTargetException f)
		{
			_log.log(Level.WARNING, "Class<?> " + getName() + " method " + m + " return a error!", f);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "Class<?> " + getName() + " method " + m + "  error!", e);
		}
		return null;
	}

	public Object invokeMethod(final String s)
	{
		if(_class == null || _instance == null)
			return null;
		try
		{
			final Method m = _class.getMethod(s, new Class[0]);
			if(m == null)
				_log.warning("Class<?> " + getName() + " method " + s + " not found!");
			else
			{
				m.setAccessible(true);
				return m.invoke(Modifier.isStatic(m.getModifiers()) ? null : _instance, new Object[0]);
			}
		}
		catch(final NoSuchMethodException e)
		{

		}
		catch(final InvocationTargetException f)
		{
			_log.log(Level.WARNING, "Class<?> " + getName() + " method " + s + " return a error!", f);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "Class<?> " + getName() + " method " + s + "  error!", e);
		}
		return null;
	}

	public Object invokeMethod(final String s, final Object[] args)
	{
		if(_class == null || _instance == null)
			return null;
		Object o = null;
		final Class[] types = new Class[args.length];
		try
		{
			boolean onlynull = true;
			for(int i = 0; i < args.length; i++)
				if(args[i] != null)
				{
					types[i] = args[i].getClass();
					onlynull = false;
				}

			Method meth = null;
			if(!onlynull && getMethod(s, types) == null)
			{
				final boolean[] accept = new boolean[args.length];
				for(final Method m : _class.getMethods())
				{
					m.setAccessible(true);
					if(m.getName().equals(s) && m.getParameterTypes().length == args.length)
						for(int i = 0; i < m.getParameterTypes().length; i++)
						{
							final Class<?> p = m.getParameterTypes()[i];
							if(args[i] != null && args[i].getClass() == p)
								accept[i] = true;
							else if(args[i] != null)
							{
								Class<?> argc = args[i].getClass();
								while (true)
									if(argc != null && argc.getSuperclass() == p)
									{
										accept[i] = true;
										break;
									}
									else if(argc == null)
										break;
									else
										argc = argc.getSuperclass();
							}
							if(!accept[i])
								accept[i] = args[i] == null;
						}

					boolean result = true;
					for(final boolean a : accept)
						if(!a)
						{
							result = false;
							break;
						}

					if(result)
					{
						meth = m;
						break;
					}
				}
			}
			else
			{
				for(int i = 0; i < args.length; i++)
					types[i] = args[i] != null ? args[i].getClass() : Object.class;
				meth = getMethod(s, types);
			}

			if(meth == null && onlynull)
				for(final Method m : _class.getMethods())
				{
					m.setAccessible(true);
					if(m.getName().equals(s) && m.getParameterTypes().length == args.length)
					{
						meth = m;
						break;
					}
				}
			if(meth == null && onlynull)
				return invokeMethod(s);

			if(meth == null)
			{
				_log.warning("Class<?> " + getName() + " method " + s + " not found!");
				return null;
			}

			meth.setAccessible(true);
			if(Modifier.isStatic(meth.getModifiers()))
				o = meth.invoke(null, args);
			else
				o = meth.invoke(_instance, args);
		}
		catch(final InvocationTargetException f)
		{
			_log.log(Level.WARNING, "Class<?> " + getName() + " method " + s + " return a error!", f);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "Class<?> " + getName() + " method " + s + "  error!", e);
		}
		return o;
	}

	private Method getMethod(final String s, final Class[] types)
	{
		try
		{
			return _class.getMethod(s, types);
		}
		catch(final NoSuchMethodException e)
		{

		}
		return null;
	}

	public String getName()
	{
		return _class.getName();
	}

	public Class<?> getRawClass()
	{
		return _class;
	}

	public boolean isFunctions()
	{
		return Functions.class.isInstance(_instance);
	}
}
