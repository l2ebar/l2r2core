package l2n.login.protect;

public final class FloodInformation
{
	public long lastAttempTime;
	public long blockTime;
	public byte countTrys;
}
