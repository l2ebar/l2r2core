package l2n.login.gameservercon;

import l2n.login.gameservercon.receive.*;

import java.io.IOException;
import java.util.logging.Logger;

public class PacketHandler
{
	private static final Logger log = Logger.getLogger(PacketHandler.class.getName());

	public static ClientBasePacket handlePacket(byte[] data, AttGameServer gameserver)
	{
		ClientBasePacket packet = null;
		try
		{
			data = gameserver.decrypt(data);
			int opcode = data[0] & 0xff;

			if(!gameserver.isCryptInitialized() && opcode > 0)
			{
				log.severe("Packet id[" + opcode + "] from not crypt initialized server.");
				return null;
			}

			if(!gameserver.isAuthed() && opcode > 1)
			{
				log.severe("Packet id[" + opcode + "] from not authed server.");
				return null;
			}

			if(!gameserver.isAuthed())
			{
				switch (opcode)
				{
					case 0x00:
						new BlowFishKey(data, gameserver).run();
						break;
					case 0x01:
						new AuthRequest(data, gameserver).run();
						break;
				}
			}
			else
			{
				switch (opcode)
				{
					case 0x02:
						packet = new PlayerInGame(data, gameserver);
						break;
					case 0x03:
						packet = new PlayerLogout(data, gameserver);
						break;
					case 0x04:
						packet = new ChangeAccessLevel(data, gameserver);
						break;
					case 0x05:
						packet = new PlayerAuthRequest(data, gameserver);
						break;
					case 0x06:
						packet = new ServerStatus(data, gameserver);
						break;
					case 0x07:
						packet = new BanIP(data, gameserver);
						break;
					case 0x08:
						packet = new ChangePassword(data, gameserver);
						break;
					case 0x09:
						packet = new Restart(data, gameserver);
						break;
					case 0x0a:
						packet = new UnbanIP(data, gameserver);
						break;
					case 0x0b:
						packet = new LockAccountIP(data, gameserver);
						break;
					case 0x0c:
						packet = new LockAccountHWID(data, gameserver);
						break;
					case 0x0e:
						packet = new PlayersInGame(data, gameserver);
						break;
					default:
						log.severe("Unknown packet from GS: " + getOpcodes(opcode) + " " + gameserver.getGameServerInfo());

				}
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}

		return packet;
	}

	private static String getOpcodes(int... opcodes)
	{
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < opcodes.length; i++)
		{
			if(i != 0)
				sb.append(" : ");
			sb.append("0x").append(Integer.toHexString(opcodes[i]));
		}

		return sb.toString();
	}
}
