package l2n.login.network.serverpackets;

public final class GGAuth extends L2LoginServerPacket
{
	private int _response;

	public GGAuth(int response)
	{
		_response = response;
	}

	@Override
	protected void write()
	{
		writeC(0x0b);
		writeD(_response);
		writeD(0x00);
		writeD(0x00);
		writeD(0x00);
		writeD(0x00);
	}
}
