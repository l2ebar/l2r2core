package l2n.login.network.clientpackets;

import l2n.commons.network.ReceivablePacket;
import l2n.login.network.L2LoginClient;
import l2n.login.network.serverpackets.L2LoginServerPacket;

import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class L2LoginClientPacket extends ReceivablePacket<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket>
{
	protected final static Logger _log = Logger.getLogger(L2LoginClientPacket.class.getName());

	@Override
	protected final boolean read()
	{
		try
		{
			return readImpl();
		}
		catch(Exception e)
		{
			_log.log(Level.SEVERE, "ERROR READING: " + this.getClass().getSimpleName(), e);
			return false;
		}
	}

	@Override
	public void run()
	{
		try
		{
			runImpl();
		}
		catch(Exception e)
		{
			_log.log(Level.SEVERE, "runImpl error: Client: " + getClient().toString(), e);
		}
		getClient().can_runImpl = true;
	}

	protected abstract boolean readImpl();

	protected abstract void runImpl() throws Exception;
}
