package l2n;

import ftGuard.ftGuard;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import javolution.text.TypeFormat;
import l2n.commons.configuration.L2Properties;
import l2n.commons.crypt.AlgorithmType;
import l2n.commons.list.GArray;
import l2n.commons.list.primitive.IntArrayList;
import l2n.commons.misc.Version;
import l2n.commons.network.utils.NetList;
import l2n.database.L2DatabaseFactory.ConnectingPoolType;
import l2n.game.loginservercon.AdvIP;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Experience;
import l2n.game.model.base.PlayerAccess;
import l2n.game.model.restrictions.IPlayerRestriction;
import l2n.game.model.restrictions.impl.ItemUseRestrictionByType;
import l2n.game.model.restrictions.impl.SkillUseRestriction;
import l2n.game.network.clientpackets.AbstractEnchantPacket.EnchantFormulaType;
import l2n.game.skills.AbnormalEffect;
import l2n.util.ArrayUtil;
import l2n.util.HWID.HWIDComparator;
import l2n.util.Rnd;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import static l2n.game.model.L2Zone.ZoneType.peace_zone;

public class Config {

    private final static Logger _log = Logger.getLogger(Config.class.getName());

    public static final int NCPUS = Runtime.getRuntime().availableProcessors();

    public static final class L2RewardItem {
        public final int item_id;
        public final float chance;
        public final long min;
        public final long max;

        public L2RewardItem(final int id, final float drop_chance, final long min_count, final long max_count) {
            item_id = id;
            chance = drop_chance;
            min = min_count;
            max = max_count;
        }

        public boolean calcChance(final int diff) {
            if (diff > 0)
                return Rnd.chance(chance * Experience.penaltyModifier(diff, 9));
            return Rnd.chance(chance);
        }
    }

    // ========================================================================
    public static final String SIEGE_CASTLE_CONFIGURATION_FILE = "./config/siege_castle.ini";
    public static final String SIEGE_FORTRESS_CONFIGURATION_FILE = "./config/siege_fortress.ini";
    public static final String SIEGE_CLANHALL_CONFIGURATION_FILE = "./config/siege_clanhall.ini";
    public static final String SIEGE_TERRITORY_CONFIGURATION_FILE = "./config/siege_territory.ini";

    public static final String LOGIN_TELNET_FILE = "./config/login_telnet.ini";
    public static final String TELNET_FILE = "./config/game_telnet.ini";
    public static final String LOGIN_CONFIGURATION_FILE = "./config/loginserver.ini";
    // ========================================================================

    /**
     * Debug/release mode
     */
    public static boolean LOGIN_DEBUG;
    public static boolean COMBO_MODE;

    /**
     * GS Packets Logger
     */
    public static boolean LOG_CLIENT_PACKETS;
    public static boolean LOG_SERVER_PACKETS;
    public static int PACKETLOGGER_FLUSH_SIZE;
    public static NetList PACKETLOGGER_IPS;
    public static GArray<String> PACKETLOGGER_ACCOUNTS;
    public static GArray<String> PACKETLOGGER_CHARACTERS;

    /**
     * Game/Login Server ports
     */
    public static int PORT_GAME;
    public static int PORT_LOGIN;
    public static String GAMESERVER_HOSTNAME;
    public static boolean ADVIPSYSTEM;
    public static GArray<AdvIP> GAMEIPS = new GArray<AdvIP>();
    public static int IP_UPDATE_TIME;
    public static boolean LOGIN_PING;
    public static int LOGIN_PING_TIME;

    public static AlgorithmType DEFAULT_PASSWORD_ENCODING;
    public static String LEGACY_PASSWORD_ENCODING;
    public static String DOUBLE_WHIRPOOL_SALT;
    public static int LOGIN_BLOWFISH_KEYS;
    public static int LOGIN_RSA_KEYPAIRS;

    public static int ALT_DROP_LS_BOSS;
    public static int ALT_DROP_LS_RAID;

    public static boolean LEVEL_UP_NOTIFICATION;

    /**
     * AntiFlood for Game/Login
     */
    public static boolean ANTIFLOOD_ENABLE;
    public static int MAX_UNHANDLED_SOCKETS_PER_IP;
    public static int UNHANDLED_SOCKET_MIN_TTL;

    public static String DATABASE_DRIVER;
    public static String DATABASE_URL;
    public static String DATABASE_LOGIN;
    public static String DATABASE_PASSWORD;
    public static int DATABASE_MAX_CONNECTIONS;
    public static int DATABASE_MAX_IDLE_TIMEOUT;
    public static int DATABASE_IDLE_TEST_PERIOD;

    public static ConnectingPoolType DATABASE_TYPE_CONNECTING_POOL;

    public static String ACCOUNTS_DATABASE_URL;
    public static String ACCOUNTS_DATABASE_LOGIN;
    public static String ACCOUNTS_DATABASE_PASSWORD;

    // Database additional options
    /**
     * lazy item update
     */
    public static boolean LAZY_ITEM_UPDATE;
    public static boolean LAZY_ITEM_UPDATE_ALL;
    /**
     * lazy item update time
     */
    public static int LAZY_ITEM_UPDATE_TIME;
    public static int LAZY_ITEM_UPDATE_ALL_TIME;
    public static int DELAYED_ITEMS_UPDATE_INTERVAL;

    public static int USER_INFO_INTERVAL;
    public static boolean BROADCAST_STATS_INTERVAL;
    public static int BROADCAST_CHAR_INFO_INTERVAL;

    public static int SAVE_GAME_TIME_INTERVAL;

    public static int MAXIMUM_ONLINE_USERS;
    public static boolean AUTO_CREATE_ACCOUNTS;

    public static boolean SHOW_OFFLINE_PLAYERS;

    public static boolean CHECK_LANG_FILES_MODIFY;
    public static boolean USE_FILE_CACHE;

    public static int LINEAR_TERRITORY_CELL_SIZE;

    /**
     * Clan Hall Auction bid limits
     */
    public static int CH_BID_GRADE1_MINCLANLEVEL;
    public static int CH_BID_GRADE1_MINCLANMEMBERS;
    public static int CH_BID_GRADE1_MINCLANMEMBERSLEVEL;
    public static int CH_BID_GRADE2_MINCLANLEVEL;
    public static int CH_BID_GRADE2_MINCLANMEMBERS;
    public static int CH_BID_GRADE2_MINCLANMEMBERSLEVEL;
    public static int CH_BID_GRADE3_MINCLANLEVEL;
    public static int CH_BID_GRADE3_MINCLANMEMBERS;
    public static int CH_BID_GRADE3_MINCLANMEMBERSLEVEL;
    public static double RESIDENCE_LEASE_MULTIPLIER;

    /**
     * ChatBan
     */
    public static boolean SIMPLE_MAT_CHECK;
    public static boolean MAT_BANCHAT;
    public static boolean PRIVATE_STORE_MSG_FILTER;
    public static boolean GM_SHOW_ANNOUNCER_NAME;
    public static boolean INDECENT_BLOCKCHAT;
    public static int MAT_KARMA;
    public static String BAN_CHANNEL;
    public static String INDECENT_BLOCK_CHANNEL;
    public static int[] BAN_CHANNEL_LIST = new int[19];
    public static int[] INDECENT_CHANNEL_LIST = new int[19];
    public static int MAT_BAN_COUNT_CHANNELS;
    public static int INDECENT_BLOCK_COUNT_CHANNELS;
    public static boolean MAT_REPLACE;
    public static String MAT_REPLACE_STRING;
    public static int UNCHATBANTIME;
    public static Pattern[] OBSCENE_LIST = {};
    public static Pattern[] INDECENT_LIST = {};
    public static Pattern[] SHOUT_LIST = {};
    public static boolean SHOUT_FILTER;
    public static boolean MAT_ANNOUNCE;
    public static boolean MAT_ANNOUNCE_FOR_ALL_WORLD;
    public static boolean MAT_ANNOUNCE_NICK;

    public static boolean SAVING_SPS;
    public static boolean MANAHEAL_SPS_BONUS;

    public static int ALT_ADD_RECIPES;
    public static boolean ALT_100_RECIPES;

    public static int ALT_MAX_ALLY_SIZE;

    public static boolean ALT_CH_ALLOW_1H_BUFFS;
    public static boolean ALT_CH_ALL_BUFFS;
    public static boolean ALT_BUFF_SUMMON;
    public static int ALT_BUFF_MIN_LEVEL;
    public static int ALT_BUFF_MAX_LEVEL;
    public static boolean ALT_IMPROVED_PETS_LIMITED_USE;
    public static boolean ALT_ALLOW_RECHARG_KOOKABURRA;
    public static int ALT_PARTY_DISTRIBUTION_RANGE;
    public static int ALT_PARTY_MAX_LVL_DIFF;

    public static double MAXLOAD_MODIFIER;

    /**
     * Augmentation Options
     */
    public static int AUGMENTATION_NG_SKILL_CHANCE;
    public static int AUGMENTATION_NG_GLOW_CHANCE;
    public static int AUGMENTATION_MID_SKILL_CHANCE;
    public static int AUGMENTATION_MID_GLOW_CHANCE;
    public static int AUGMENTATION_HIGH_SKILL_CHANCE;
    public static int AUGMENTATION_HIGH_GLOW_CHANCE;
    public static int AUGMENTATION_TOP_SKILL_CHANCE;
    public static int AUGMENTATION_TOP_GLOW_CHANCE;
    public static int AUGMENTATION_BASESTAT_CHANCE;

    public static int AUGMENTATION_ACC_SKILL_CHANCE;

    /**
     * Count kills?
     */
    public static boolean KILL_COUNTER;
    public static boolean KILL_COUNTER_PRELOAD;

    /**
     * Count dropped?
     */
    public static boolean DROP_COUNTER;

    /**
     * Count crafted?
     */
    public static boolean CRAFT_COUNTER;

    public static int SELECTOR_SLEEP_TIME;
    public static long TIMEOUT_CHECKER_CLIENT;

    /**
     * Auto-loot
     */
    public static boolean AUTO_LOOT;
    /**
     * Автолут денег
     */
    public static boolean AUTO_LOOT_ADENA;
    /**
     * Auto-loot herbs
     */
    public static boolean AUTO_LOOT_HERBS;
    /**
     * Auto-loot RaidBoss
     */
    public static boolean AUTO_LOOT_RAIDBOSS;
    /**
     * Auto-loot for/from players with karma also?
     */
    public static boolean AUTO_LOOT_PK;

    /**
     * Разделение эффектов хербов с петом или саммоном пополам
     */
    public static boolean HERBS_DIVIDE;

    /**
     * Account name template
     */
    public static String ANAME_TEMPLATE;
    /**
     * Account password template
     */
    public static String APASSWD_TEMPLATE;

    /**
     * Character name template
     */
    public static Pattern CNAME_TEMPLATE;
    /**
     * Clan name template
     */
    public static Pattern CLAN_NAME_TEMPLATE;
    /**
     * Clan title template
     */
    public static Pattern CLAN_TITLE_TEMPLATE;
    /**
     * Ally name template
     */
    public static Pattern ALLY_NAME_TEMPLATE;

    /**
     * Fame settings
     */
    public static int MAX_PERSONAL_FAME_POINTS;
    public static long FORTRESS_ZONE_FAME_TASK_FREQUENCY;
    public static int FORTRESS_ZONE_FAME_AQUIRE_POINTS;
    public static long CASTLE_ZONE_FAME_TASK_FREQUENCY;
    public static int CASTLE_ZONE_FAME_AQUIRE_POINTS;
    public static int TerritorySiege_ZONE_FAME_AQUIRE_POINTS;
    public static int TerritoryFortress_ZONE_FAME_AQUIRE_POINTS;
    public static boolean ENABLE_CHARNAME_BLOCKSPAM;
    public static GArray<String> CHARNAME_BLOCKSPAM_LIST = new GArray<String>();
    public static boolean ENABLE_TRADE_BLOCKSPAM;
    public static GArray<String> TRADE_LIST = new GArray<String>();
    public static GArray<String> TRADE_LIST_SYMBOLS = new GArray<String>();

    /**
     * Global chat state
     */
    public static int GLOBAL_CHAT;
    public static int MAIN_LEVEL_CHAT;
    public static int GLOBAL_TRADE_CHAT;
    public static int SHOUT_CHAT_MODE;
    public static int TRADE_CHAT_MODE;
    public static boolean GM_ANNOUNCE_LOGIN;
    public static boolean ENT_SHOWENTERMESSON;
    public static String ENT_SHOWENTERMESS;
    public static boolean ANNOUNCE_CASTLE_LORDS;

    /**
     * For test servers - evrybody has admin rights
     */
    public static boolean EVERYBODY_HAS_ADMIN_RIGHTS;
    public static boolean ALLOW_SPECIAL_COMMANDS;

    /**
     * For small servers
     */
    public static boolean ALT_GAME_MATHERIALSDROP;

    /**
     * Все мобы не являющиеся рейдами спавнятся в двойном количестве
     */
    public static boolean ALT_DOUBLE_SPAWN;

    /**
     * Give exp and sp for craft
     */
    public static boolean ALT_GAME_EXP_FOR_CRAFT;

    public static boolean ALT_MASTERWORK_CONFIG;
    public static boolean ALLOW_MASTERWORK;
    public static boolean ALLOW_CRITICAL_CRAFT;

    public static boolean ALT_GAME_UNREGISTER_RECIPE;

    /**
     * Delay for announce SS period (in minutes)
     */
    public static int SS_ANNOUNCE_PERIOD;

    /**
     * Save Coords command?
     */
    public static boolean COMMAND_ALLOW_SAVE_LOCATION;
    public static int COMMAND_ALLOW_SAVE_LOCATION_PRICE;
    public static int COMMAND_ALLOW_SAVE_LOCATION_ITEM;
    /**
     * Show mob stats/droplist to players?
     */
    public static boolean ALT_GAME_SHOW_DROPLIST;
    public static boolean ALT_GAME_GEN_DROPLIST_ON_DEMAND;
    public static boolean ALT_FULL_NPC_STATS_PAGE;
    public static boolean ALLOW_NPC_SHIFTCLICK;

    public static boolean ALT_ALLOW_DROP_COMMON;

    /**
     * Mobs use UD config
     */
    public static boolean ALT_ALLOW_MOBS_USE_UD;
    public static int ALT_MOBS_UD_SKILL_ID;
    public static int ALT_MOBS_UD_SKILL_LVL;
    public static int ALT_MOBS_UD_USE_CHANCE;

    public static long ALT_MAX_COUNT_ADENA;
    public static long ALT_MAX_COUNT_OTHER;

    public static boolean ALT_DELETE_PET_IF_HUNGRY;

    public static boolean ALT_SEVEN_SIGNS_RESTRICT;

    public static boolean ALT_SHOP_CONDITION_ENABLE;
    public static boolean ALT_ALLOW_SELL_COMMON;
    public static IntArrayList ALT_DISABLED_MULTISELL;
    public static long[] ALT_SHOP_PRICE_LIMITS;
    public static IntArrayList ALT_SHOP_UNALLOWED_ITEMS;
    public static int ALT_SOUL_CRYSTAL_LEVEL_CHANCE;

    /* Show html window at login */
    public static boolean SHOW_HTML_WELCOME;
    // Show Online Players announce
    public static boolean ONLINE_PLAYERS_AT_STARTUP;

    public static boolean ENABLE_STARTING_ITEM;

    /**
     * Система наподобии той, что в WoW
     */
    public static boolean ALT_HONOR_SYSTEM;
    public static boolean ALT_HONOR_SYSTEM_IN_PVP_ZONE;
    public static int ALT_HONOR_SYSTEM_WON_ITEMID;
    public static int ALT_HONOR_SYSTEM_WON_ITEM_COUNT;
    public static int ALT_HONOR_SYSTEM_LOSE_ITEMID;
    public static int ALT_HONOR_SYSTEM_LOSE_ITEM_COUNT;
    public static int ALT_HONOR_SYSTEM_PVP_ITEMID;
    public static int ALT_HONOR_SYSTEM_PVP_ITEM_COUNT;

    /**
     * Таймаут на использование social action
     */
    public static boolean ALT_SOCIAL_ACTION_REUSE;

    /**
     * Alternative gameing - loss of XP on death
     */
    public static boolean ALT_GAME_DELEVEL;

    public static boolean ALT_SHORTCUTS_SKILL_VALIDATE;

    public static boolean INFINITE_BLESSEDSPIRITSHOT;
    public static boolean INFINITE_SPIRITSHOT;
    public static boolean INFINITE_SOULSHOT;
    public static boolean DONT_DESTROY_ARROWS;
    public static boolean INFINITE_BEASTSHOT;

    public static boolean ALT_GAME_SUBCLASS_WITHOUT_QUESTS;
    public static boolean ALT_GAME_SUBCLASS_FOR_ITEMS;
    public static boolean ALT_GAME_SUBCLASS_WITHOUT_FATES_WHISPER;
    public static int ALT_GAME_LEVEL_TO_GET_SUBCLASS;
    public static int ALT_MAX_LEVEL;
    public static int ALT_MAX_SUB_LEVEL;
    public static int ALT_GAME_SUB_ADD;

    /**
     * Alternative shield defence
     */
    public static boolean ALT_SHIELD_BLOCKS;
    public static int CHANGE_CLAN_LEADER_TIME;

    public static boolean ALT_DONT_ALLOW_PETS_ON_SIEGE;

    public static boolean ALT_SIMPLE_SIGNS;
    public static int ALT_MAMMON_EXCHANGE;
    public static int ALT_MAMMON_UPGRADE;
    public static boolean ALT_ALLOW_TATTOO;

    public static boolean ALT_PROF_SOCIAL;
    public static int ALT_PROF_SOCIAL_ID;

    public static int ALT_BUFF_LIMIT;
    public static int ALT_DANCE_SONG_LIMIT;

    public static int GATEKEEPER_FREE;
    public static int CRUMA_GATEKEEPER_LVL;
    public static boolean USE_GK_NIGHT_PRICE;

    public static int MULTISELL_SIZE;

    public static boolean ALT_ALLOW_OTHERS_WITHDRAW_FROM_CLAN_WAREHOUSE;
    public static boolean ALT_GAME_REQUIRE_CLAN_CASTLE;
    public static boolean ALT_GAME_REQUIRE_CASTLE_DAWN;

    public static int ALT_TRUE_CHESTS;

    public static String ALT_KAMALOKA_LIMITS;
    public static boolean ALT_KAMALOKA_NIGHTMARES_PREMIUM_ONLY;
    public static boolean ALT_KAMALOKA_NIGHTMARE_REENTER;
    public static boolean ALT_KAMALOKA_ABYSS_REENTER;
    public static boolean ALT_KAMALOKA_LAB_REENTER;

    public static int NON_OWNER_ITEM_PICKUP_DELAY;

    public static boolean ALT_NO_LASTHIT;

    /**
     * Logging Chat Window
     */
    public static boolean LOG_CHAT;
    public static boolean LOG_KILLS;
    public static boolean LOG_TELNET;

    public static HashMap<Integer, PlayerAccess> gmlist = new HashMap<Integer, PlayerAccess>();

    /**
     * Rate control
     */
    public static double RATE_XP;
    public static double RATE_SP;

    /**
     * DYNAMIC Rate control
     */
    public static boolean DYNAMIC_RATE_XP;
    public static boolean DYNAMIC_RATE_SP;
    public static boolean DYNAMIC_RATE_DROP_ADENA;
    public static boolean DYNAMIC_RATE_DROP_ITEMS;
    public static boolean DYNAMIC_RATE_SPOIL;

    /**
     * craft Rate control
     */
    public static double CRAFT_RATE_XP;
    public static double CRAFT_RATE_SP;
    public static double ALT_GAME_CREATION_RARE_XPSP_RATE;

    /**
     * рейты награды в квестах
     */
    public static double RATE_QUESTS_REWARD;
    /**
     * модификатор для получаемого EXP в квестах
     */
    public static double RATE_QUESTS_REWARD_XP;
    /**
     * модификатор для получаемого SP в квестах
     */
    public static double RATE_QUESTS_REWARD_SP;
    /**
     * учитывать ли RATE_QUESTS_REWARD для квестов на профессии
     */
    public static boolean RATE_QUESTS_OCCUPATION_CHANGE;
    /**
     * включить дополнительные модификаторы
     */
    public static boolean RATE_QUESTS_ALT_MULTIPLIERS_REWARD;
    public static double RATE_QUESTS_REWARD_ADENA;
    public static double RATE_QUESTS_REWARD_POTION;
    public static double RATE_QUESTS_REWARD_SCROLL;
    public static double RATE_QUESTS_REWARD_RECIPE;
    public static double RATE_QUESTS_REWARD_MATERIAL;

    /**
     * шанс выбить квест вещь
     */
    public static double RATE_QUESTS_DROP;
    /**
     * шанс выбить квест вещь в квестах на профу
     */
    public static double RATE_QUESTS_DROP_PROF;

    public static double GATEKEEPER_MODIFIER;
    public static double RATE_CLAN_REP_SCORE;
    public static int RATE_CLAN_REP_SCORE_MAX_AFFECTED;
    public static double RATE_DROP_ADENA;
    public static float RATE_DROP_ADENA_PARTY;
    public static float RATE_DROP_ITEMS_PARTY;
    public static float RATE_XP_PARTY;
    public static float RATE_SP_PARTY;
    public static double RATE_DROP_ITEMS;
    public static double RATE_DROP_COMMON_ITEMS;
    public static double RATE_DROP_RAIDBOSS;
    public static double RATE_DROP_EPIC_RAIDBOSS;
    public static IntArrayList RATE_DROP_EPIC_RAIDBOSS_IDs;
    public static double RATE_DROP_SPOIL;
    public static int MAX_SPOIL_LEVEL_DEPEND;
    public static int RATE_MANOR;
    public static double RATE_FISH_DROP_COUNT;
    public static double RATE_SIEGE_GUARDS_PRICE;
    public static int RATE_MASTERWORK;
    public static int RATE_CRITICAL_CRAFT_CHANCE;
    public static int RATE_CRITICAL_CRAFT_MULTIPLIER;


    /**
     * Player Drop Rate control
     */
    public static boolean KARMA_DROP_GM;
    public static boolean KARMA_NEEDED_TO_DROP;

    public static int KARMA_DROP_ITEM_LIMIT;

    public static int KARMA_RANDOM_DROP_LOCATION_LIMIT;

    public static int KARMA_DROPCHANCE_MINIMUM;
    public static int KARMA_DROPCHANCE_MULTIPLIER;
    public static int KARMA_DROPCHANCE_EQUIPMENT;
    public static int KARMA_DROPCHANCE_EQUIPPED_WEAPON;

    public static int AUTODESTROY_ITEM_AFTER;

    public static int DELETE_DAYS;

    public static boolean ENABLE_GC_TASK;
    public static int GARBAGE_COLLECTOR_TASK_FREQUENCY;

    public static boolean AUTOSAVE;
    public static long AUTOSAVE_MIN_TIME;

    /**
     * Datapack root directory
     */
    public static File DATAPACK_ROOT;

    public static boolean WEAR_TEST_ENABLED;
    public static boolean USE_DATABASE_LAYER;

    /**
     * Allow L2Walker client
     */
    public static enum L2WalkerAllowed {
        True,
        False,
        Peace
    }

    public static L2WalkerAllowed ALLOW_L2WALKER_CLIENT;
    public static int MAX_L2WALKER_LEVEL;
    public static boolean ALLOW_CRAFT_BOT;
    public static boolean ALLOW_BOT_CAST;
    public static boolean KILL_BOTS_WITH_SKILLCOOLTIME;
    public static int L2WALKER_PUNISHMENT;
    public static int BUGUSER_PUNISH;

    public static boolean ALLOW_DISCARDITEM;
    public static boolean ALLOW_FREIGHT;
    public static boolean ALLOW_WAREHOUSE;
    public static boolean ALLOW_WATER;
    public static boolean ALLOW_BOAT;
    public static boolean ALLOW_LOTTERY;
    public static boolean ALLOW_CURSED_WEAPONS;
    public static int CURSED_WEAPONS_MIN_PLAYERS_DROP;
    public static boolean DROP_CURSED_WEAPONS_ON_KICK;
    public static boolean ALLOW_NOBLE_TP_TO_ALL;

    /**
     * Pets
     */
    public static int FLYING_SPEED;
    public static int RIDING_SPEED;
    public static int SWIMING_SPEED;

    /**
     * protocol revision
     */
    public static int MIN_PROTOCOL_REVISION;
    public static int MAX_PROTOCOL_REVISION;
    public static boolean PROTOCOL_ENABLE_OBFUSCATION;

    public static String DEFAULT_LANG;
    public static boolean ENG_QUEST_NAMES;

    /**
     * Время регулярного рестарта (через определенное время)
     */
    public static int RESTART_TIME;
    /**
     * Время запланированного на определенное время суток рестарта
     */
    public static int RESTART_AT_TIME;
    public static int LRESTART_TIME;

    public static String LOGIN_HOST;
    public static int GAME_SERVER_LOGIN_PORT;
    public static String GAME_SERVER_LOGIN_HOST;
    public static String INTERNAL_HOSTNAME;
    public static String EXTERNAL_HOSTNAME;

    public static boolean SERVER_SIDE_NPC_NAME;
    public static boolean SERVER_SIDE_NPC_TITLE;
    public static boolean SERVER_SIDE_NPC_TITLE_WITH_LVL;

    public static Version VERSION;
    public static String SERVER_REVISION;
    public static String SERVER_BUILD_DATE;

    /**
     * Inventory slots limits
     */
    public static int INVENTORY_MAXIMUM_NO_DWARF;
    public static int INVENTORY_MAXIMUM_DWARF;
    public static int INVENTORY_MAXIMUM_GM;

    public static int MAX_ITEM_IN_PACKET;

    /**
     * Warehouse slots limits
     */
    public static int WAREHOUSE_SLOTS_NO_DWARF;
    public static int WAREHOUSE_SLOTS_DWARF;
    public static int WAREHOUSE_SLOTS_CLAN;

    /**
     * Spoil Rates
     */
    public static double BASE_SPOIL_RATE;
    public static double MINIMUM_SPOIL_RATE;
    public static boolean ALT_SPOIL_FORMULA;

    /**
     * Manor Config
     */
    public static double MANOR_SOWING_BASIC_SUCCESS;
    public static double MANOR_SOWING_ALT_BASIC_SUCCESS;
    public static double MANOR_HARVESTING_BASIC_SUCCESS;
    public static double MANOR_DIFF_PLAYER_TARGET;
    public static double MANOR_DIFF_PLAYER_TARGET_PENALTY;
    public static double MANOR_DIFF_SEED_TARGET;
    public static double MANOR_DIFF_SEED_TARGET_PENALTY;

    /**
     * Karma System Variables
     */
    public static int KARMA_MIN_KARMA;
    public static int KARMA_SP_DIVIDER;
    public static int KARMA_LOST_BASE;

    public static int MIN_PK_TO_ITEMS_DROP;

    public static String KARMA_NONDROPPABLE_ITEMS;
    public static GArray<Integer> KARMA_LIST_NONDROPPABLE_ITEMS = new GArray<Integer>();

    public static int PVP_TIME;

    /**
     * Karma Punishment
     **/
    public static boolean ALT_ALL_CAN_KILL_IN_PEACE;
    public static boolean ALT_GAME_KARMAPLAYERCANBEKILLEDINPEACEZONE;
    public static boolean ALT_GAME_KARMA_PLAYER_CAN_SHOP;
    public static boolean ALT_GAME_KARMA_PLAYER_CAN_TELEPORT;

    // Player Protection Level
    public static int ALT_PLAYER_PROTECTION_LEVEL;

    public static boolean SHOW_COPYRIGHT;

    public static boolean HARD_DB_CLEANUP_ON_START;

    public static boolean REGEN_SIT_WAIT;

    public static double RATE_RAID_REGEN;
    public static int RAID_MAX_LEVEL_DIFF;
    public static boolean PARALIZE_ON_RAID_DIFF;

    public static int RATE_BREAKPOINT;
    public static int MAX_DROP_ITERATIONS;

    public static double ALT_PK_DEATH_RATE;
    public static int STARTING_ADENA;
    public static boolean CHAR_TITLE;
    public static String ADD_CHAR_TITLE;
    public static boolean SPAWN_CHAR;
    /**
     * X Coordinate of the SPAWN_CHAR setting.
     */
    public static int SPAWN_X;
    /**
     * Y Coordinate of the SPAWN_CHAR setting.
     */
    public static int SPAWN_Y;
    /**
     * Z Coordinate of the SPAWN_CHAR setting.
     */
    public static int SPAWN_Z;
    public static int STARTING_LEVEL;
    public static int STARTING_SP;
    public static boolean AUTO_CHARACTER_NOBLESSE;

    public static IntArrayList STARTING_ITEM_ID_LIST;
    public static IntArrayList STARTING_ITEM_COUNT_LIST;
    public static IntArrayList STARTING_ITEM_ID_LISTM;
    public static IntArrayList STARTING_ITEM_COUNT_LISTM;

    /**
     * Deep Blue Mobs' Drop Rules Enabled
     */
    public static boolean DEEPBLUE_DROP_RULES;
    public static int DEEPBLUE_DROP_MAXDIFF;
    public static int DEEPBLUE_DROP_RAID_MAXDIFF;
    public static boolean UNSTUCK_SKILL;

    /**
     * telnet enabled
     */
    public static boolean IS_TELNET_ENABLED;
    public static boolean IS_LOGIN_TELNET_ENABLED;

    /**
     * Percent CP is restore on respawn
     */
    public static double RESPAWN_RESTORE_CP;
    /**
     * Percent HP is restore on respawn
     */
    public static double RESPAWN_RESTORE_HP;
    /**
     * Percent MP is restore on respawn
     */
    public static double RESPAWN_RESTORE_MP;

    /**
     * Maximum number of available slots for pvt stores (sell/buy) - Dwarves
     */
    public static int MAX_PVTSTORE_SLOTS_DWARF;
    /**
     * Maximum number of available slots for pvt stores (sell/buy) - Others
     */
    public static int MAX_PVTSTORE_SLOTS_OTHER;
    public static int MAX_PVTCRAFT_SLOTS;

    public static boolean ALLOW_CH_DOOR_OPEN_ON_CLICK;

    public static boolean DISABLE_GRADE_PENALTY;

    /**
     * Show licence or not just after login (if false, will directly go to the Server List
     */
    public static boolean SHOW_LICENCE;

    /**
     * Deafult punishment for illegal actions
     */
    public static int DEFAULT_PUNISH;

    public static boolean ACCEPT_NEW_GAMESERVER;
    public static byte[] HEX_ID;
    public static boolean ACCEPT_ALTERNATE_ID;
    public static int REQUEST_ID;

    public static boolean ANNOUNCE_MAMMON_SPAWN;

    public static int GM_NAME_COLOUR;
    public static boolean GM_HERO_AURA;
    public static int NORMAL_NAME_COLOUR;
    public static int CLANLEADER_NAME_COLOUR;
    public static int BOT_NAME_COLOUR;
    public static String BOT_NAME_HEX_COLOUR;

    public static boolean ALT_DEATH_PENALTY;
    public static boolean ALLOW_DEATH_PENALTY_C5;
    public static int ALT_DEATH_PENALTY_C5_CHANCE;
    public static boolean ALT_DEATH_PENALTY_C5_CHAOTIC_RECOVERY;
    public static int ALT_DEATH_PENALTY_C5_EXPERIENCE_PENALTY;
    public static int ALT_DEATH_PENALTY_C5_KARMA_PENALTY;

    public static boolean HIDE_GM_STATUS;
    public static boolean SAVE_GM_EFFECTS; // Silence, gmspeed, etc...
    public static boolean GM_STARTUP_INVULNERABLE;
    public static boolean GM_STARTUP_INVISIBLE;

    /**
     * security options
     */
    public static String DISABLE_CREATION_IDs;
    public static GArray<Integer> DISABLE_CREATION_ID_LIST = new GArray<Integer>();
    public static String LOG_MULTISELL_IDs;
    public static GArray<Integer> LOG_MULTISELL_ID_LIST = new GArray<Integer>();

    public static boolean ENABLE_FISHING_CHECK_WATER;

    public static int CLIENT_Z_SHIFT;

    public static int MOVE_PACKET_DELAY;
    public static int ATTACK_PACKET_DELAY;

    public static boolean DAMAGE_FROM_FALLING;

    public static String GEOEDITOR_HOST;

    /**
     * Wedding Options
     */
    public static boolean ALLOW_WEDDING;
    public static int WEDDING_GIVE_ITEM;
    public static int WEDDING_GIVE_ITEM_COUNT;
    public static int WEDDING_PRICE;
    public static boolean WEDDING_PUNISH_INFIDELITY;
    public static boolean WEDDING_TELEPORT;
    public static int WEDDING_TELEPORT_PRICE;
    public static int WEDDING_TELEPORT_INTERVAL;
    public static boolean WEDDING_SAMESEX;
    public static boolean WEDDING_FORMALWEAR;
    public static int WEDDING_DIVORCE_COSTS;

    public static long PLAYER_DISCONNECT_INGAME_TIME;
    public static String PLAYER_DISCONNECT_TITILE;
    public static long PLAYER_LOGOUT_INGAME_TIME;

    /**
     * Castle siege options
     **/
    public static int SIEGE_CASTLE_CLAN_MIN_LEVEL;
    public static int SIEGE_FORT_CLAN_MIN_LEVEL;
    public static int SIEGE_CLANHALL_CLAN_MIN_LEVEL;
    public static int SIEGE_CASTLE_LENGTH;
    public static int CASTLE_SIEGE_DAY;
    public static int TERRITORY_SIEGE_DAY;
    public static int CLANHALL_SIEGE_DAY;
    public static int SIEGE_FORT_LENGTH;
    public static int SIEGE_CLANHALL_LENGTH;
    public static int SIEGE_CASTLE_DEFENDER_RESPAWN;
    public static int SIEGE_FORT_DEFENDER_RESPAWN;
    public static int SIEGE_CLANHALL_DEFENDER_RESPAWN;
    public static boolean SIEGE_OPERATE_DOORS;
    public static boolean SIEGE_OPERATE_DOORS_LORD_ONLY;
    public static int SIEGE_CASTLE_CONTROL_TOWER_LOSS_PENALTY;


    /**
     * limits of stats
     **/
    public static int LIM_PATK;
    public static int LIM_MATK;
    public static int LIM_PDEF;
    public static int LIM_MDEF;
    public static int LIM_MATK_SPD;
    public static int LIM_PATK_SPD;

    public static int LIM_PCRIT;
    public static int LIM_MCRIT;
    public static int LIM_EVASION;
    public static int LIM_RUNSPEED;

    public static int BOW_ATK_SPEED;
    public static int CROSS_BOW_ATK_SPEED;

    public static int FESTIVAL_MIN_PARTY_SIZE;
    public static double FESTIVAL_RATE_PRICE;

    /**
     * Dimensional Rift Config
     **/
    public static int RIFT_MIN_PARTY_SIZE;
    public static int RIFT_MAX_JUMPS;
    public static int RIFT_AUTO_JUMPS_TIME;
    public static int RIFT_AUTO_JUMPS_TIME_RAND;
    public static int RIFT_SPAWN_DELAY;
    public static int RIFT_ENTER_COST_RECRUIT;
    public static int RIFT_ENTER_COST_SOLDIER;
    public static int RIFT_ENTER_COST_OFFICER;
    public static int RIFT_ENTER_COST_CAPTAIN;
    public static int RIFT_ENTER_COST_COMMANDER;
    public static int RIFT_ENTER_COST_HERO;
    public static double RIFT_BOSS_ROOM_TIME_MUTIPLY;

    /**
     * Some more LS Settings
     */
    public static GArray<String> INTERNAL_IP = null;
    /**
     * Продвинутый список локальных сетей / ip-адресов
     */
    public static NetList INTERNAL_NETLIST = null;

    public static boolean SUPER_ACCESS_ENABLE;
    public static String SUPER_ACCESS_PASSWORD;
    public static NetList SUPER_ACCESS_IP_LIST;

    public static boolean ALLOW_TALK_WHILE_SITTING;
    public static boolean ALLOW_FAKE_PLAYERS;
    public static Integer FAKE_PLAYERS_PERCENT;

    public static boolean PARTY_LEADER_ONLY_CAN_INVITE;

    public static boolean LOGIN_GG_CHECK;
    public static boolean GG_CHECK;

    /**
     * Allow Manor system
     */
    public static boolean ALLOW_MANOR;
    /**
     * Manor Refresh Starting time
     */
    public static int MANOR_REFRESH_TIME;
    /**
     * Manor Refresh Min
     */
    public static int MANOR_REFRESH_MIN;
    /**
     * Manor Next Period Approve Starting time
     */
    public static int MANOR_APPROVE_TIME;
    /**
     * Manor Next Period Approve Min
     */
    public static int MANOR_APPROVE_MIN;
    /**
     * Manor Maintenance Time
     */
    public static int MANOR_MAINTENANCE_PERIOD;
    /**
     * Manor Save All Actions
     */
    public static boolean MANOR_SAVE_ALL_ACTIONS;
    /**
     * Manor Save Period Rate
     */
    public static int MANOR_SAVE_PERIOD_RATE;

    public static int EVENT_CofferOfShadowsPriceRate;
    public static double EVENT_CofferOfShadowsRewardRate;

    /**
     * Last Hero
     */
    public static int EVENT_LASTHERO_ITEM_ID;
    public static int EVENT_LASTHERO_ITEM_COUNT;
    public static int EVENT_LASTHERO_TIME_TO_START;
    public static int EVENT_LASTHERO_TIME_FOR_FIGHT;
    public static boolean EVENT_LASTHERO_RATE;
    public static GArray<EventInterval> EVENT_LASTHERO_INTERVAL;
    public static boolean EVENT_LASTHERO_ALLOW_CLAN_SKILL;
    public static boolean EVENT_LASTHERO_ALLOW_HERO_SKILL;
    public static boolean EVENT_LASTHERO_ALLOW_BUFFS;
    public static boolean EVENT_LASTHERO_DISPEL_TRANSFORMATION;
    public static boolean EVENT_LASTHERO_ALLOW_SUMMONS;
    public static boolean EVENT_LASTHERO_GIVE_HERO;
    public static boolean EVENT_LASTHERO_PARTY_DISABLE;

    public static IPlayerRestriction EVENT_LASTHERO_ITEM_RESTRICTION;
    public static IPlayerRestriction EVENT_LASTHERO_SKILL_RESTRICTION;

    /**
     * Death Match
     */
    public static int EVENT_DEATHMATCH_ITEM_ID;
    public static int EVENT_DEATHMATCH_ITEM_COUNT;
    public static int EVENT_DEATHMATCH_REWARD_ITEM_ID;
    public static int EVENT_DEATHMATCH_REWARD_ITEM_COUNT;
    public static int EVENT_DEATHMATCH_TIME_TO_START;
    public static int EVENT_DEATHMATCH_TIME_FOR_FIGHT;
    public static int EVENT_DEATHMATCH_MIN_PLAYER_COUNT;
    public static int EVENT_DEATHMATCH_REVIVE_DELAY;
    public static boolean EVENT_DEATHMATCH_RATE;
    public static GArray<EventInterval> EVENT_DEATHMATCH_INTERVAL;
    public static boolean EVENT_DEATHMATCH_ALLOW_CLAN_SKILL;
    public static boolean EVENT_DEATHMATCH_ALLOW_HERO_SKILL;
    public static boolean EVENT_DEATHMATCH_ALLOW_BUFFS;
    public static boolean EVENT_DEATHMATCH_DISPEL_TRANSFORMATION;
    public static boolean EVENT_DEATHMATCH_ALLOW_SUMMONS;
    public static boolean EVENT_DEATHMATCH_GIVE_HERO;
    public static boolean EVENT_DEATHMATCH_PARTY_DISABLE;

    public static IPlayerRestriction EVENT_DEATHMATCH_ITEM_RESTRICTION;
    public static IPlayerRestriction EVENT_DEATHMATCH_SKILL_RESTRICTION;

    public static String EVENT_DEATHMATCH_BUFFS_FIGHTER;
    public static String EVENT_DEATHMATCH_BUFFS_MAGE;
    public static boolean EVENT_DEATHMATCH_BUFFS_ONSTART;

    /**
     * TvT by XoTTa6bI4
     **/
    public static int EVENT_TVT_ITEM_ID;
    public static int EVENT_TVT_ITEM_COUNT;
    public static int EVENT_TVT_MAX_LENGTH_TEAM;
    public static int EVENT_TVT_TIME_TO_START;
    public static int EVENT_TVT_TIME_FOR_FIGHT;
    public static int EVENT_TVT_REVIVE_DELAY;
    public static boolean EVENT_TVT_RATE;
    public static GArray<EventInterval> EVENT_TVT_INTERVAL;
    public static boolean EVENT_TVT_ALLOW_CLAN_SKILL;
    public static boolean EVENT_TVT_ALLOW_HERO_SKILL;
    public static boolean EVENT_TVT_ALLOW_BUFFS;
    public static boolean EVENT_TVT_DISPEL_TRANSFORMATION;
    public static boolean EVENT_TVT_ALLOW_SUMMONS;

    public static IPlayerRestriction EVENT_TVT_ITEM_RESTRICTION;
    public static IPlayerRestriction EVENT_TVT_SKILL_RESTRICTION;

    public static String EVENT_TVT_BUFFS_FIGHTER;
    public static String EVENT_TVT_BUFFS_MAGE;
    public static String EVENT_CTF_BUFFS_FIGHTER;
    public static String EVENT_CTF_BUFFS_MAGE;
    public static boolean EVENT_TVT_BUFFS_ONSTART;

    public static int EVENT_CFT_ITEM_ID;
    public static int EVENT_CFT_ITEM_COUNT;
    public static int EVENT_CTF_MAX_LENGTH_TEAM;
    public static int EVENT_CFT_TIME_TO_START;
    public static int EVENT_CTF_TIME_FOR_FIGHT;
    public static int EVENT_CTF_REVIVE_DELAY;
    public static boolean EVENT_CFT_RATE;
    public static GArray<EventInterval> EVENT_CFT_INTERVAL;
    public static boolean EVENT_CFT_ALLOW_CLAN_SKILL;
    public static boolean EVENT_CFT_ALLOW_HERO_SKILL;
    public static boolean EVENT_CFT_ALLOW_BUFFS;
    public static boolean EVENT_CFT_DISPEL_TRANSFORMATION;
    public static boolean EVENT_CFT_ALLOW_SUMMONS;

    public static IPlayerRestriction EVENT_CFT_ITEM_RESTRICTION;
    public static IPlayerRestriction EVENT_CFT_SKILL_RESTRICTION;

    public static boolean EVENT_LASTHERO_CHECK_HWID;
    public static boolean EVENT_DEATHMATCH_CHECK_HWID;
    public static boolean EVENT_TVT_CHECK_HWID;
    public static boolean EVENT_CTF_CHECK_HWID;

    /**
     * Hearts event
     */
    public static double EVENT_CHANGE_OF_HEART_CHANCE;
    public static double EVENT_TRICK_OF_TRANS_CHANCE;

    public static int TFH_POLLEN_CHANCE;

    public static double GLIT_MEDAL_CHANCE;
    public static double GLIT_GLITTMEDAL_CHANCE;
    public static IntArrayList EXCEPTION_MONSTER_LIST;

    /**
     * Autoanoncer
     **/
    public static String EVENT_message1;
    public static String EVENT_time1;
    public static String EVENT_message2;
    public static String EVENT_time2;
    public static String EVENT_message3;
    public static String EVENT_time3;


    /**
     * Shutdown settings
     **/
    public static int SHUTDOWN_MSG_TYPE;

    /**
     * lottery
     **/
    public static int ALT_LOTTERY_PRIZE;
    public static int ALT_LOTTERY_TICKET_PRICE;
    public static int ALT_LOTTERY_2_AND_1_NUMBER_PRIZE;
    public static double ALT_LOTTERY_5_NUMBER_RATE;
    public static double ALT_LOTTERY_4_NUMBER_RATE;
    public static double ALT_LOTTERY_3_NUMBER_RATE;

    /**
     * Конфиги для кланов
     **/

    public static int DAYS_BEFORE_CREATE_A_CLAN;
    public static int HOURS_BEFORE_JOIN_A_CLAN;
    public static int ALT_CLAN_MEMBERS_FOR_WAR;
    public static int ALT_MIN_CLAN_LVL_FOR_WAR;
    public static int ALT_CLAN_WAR_MAX;
    public static int DAYS_BEFORE_CREATE_NEW_ALLY_WHEN_DISSOLVED;
    public static int DAYS_BEFORE_CLAN_INVITE;
    public static int MIN_LEVEL_TO_CREATE_PLEDGE;

    public static int MEMBERFORLEVEL6;
    public static int MEMBERFORLEVEL7;
    public static int MEMBERFORLEVEL8;
    public static int MEMBERFORLEVEL9;
    public static int MEMBERFORLEVEL10;
    public static int MEMBERFORLEVEL11;

    public static boolean USE_REDUCE_REPSCORE_RATE;
    public static boolean USE_REDUCE_REPSCORE_RATE_MULTISELL;
    public static boolean USE_REDUCE_REPSCORE_RATE_CLAN_SKILL;
    public static int CLANREP_HERO_REP;
    public static int CLANREP_FESTIVAL;
    public static int JOIN_ACADEMY_MIN_REP_SCORE;
    public static int JOIN_ACADEMY_MAX_REP_SCORE;
    public static int CLANREP_QUEST_508;
    public static int CLANREP_QUEST_509;
    public static int CLANREP_QUEST_510;
    public static int CLANREP_SIEGE_CASLE_NEW_OWNER;
    public static int CLANREP_SIEGE_CASLE_OLD_OWNER;
    public static int CLANREP_SIEGE_CASLE_LOSS;
    public static int CLANREP_SIEGE_FORT_NEW_OWNER;
    public static int CLANREP_SIEGE_FORT_OLD_OWNER;
    public static int CLANREP_SIEGE_FORT_LOSS;
    public static int CLANREP_SIEGE_CH_WIN;
    public static int CLANREP_SIEGE_CH_LOSS;
    public static int CLANREP_SIEGE_CH_BADATTACK;
    public static int BALLISTA_POINTS;
    public static int REPUTATION_SCORE_PER_KILL;
    public static int CLANREP_CLAN_LVL_6;
    public static int CLANREP_CLAN_LVL_7;
    public static int CLANREP_CLAN_LVL_8;
    public static int CLANREP_CLAN_LVL_9;
    public static int CLANREP_CLAN_LVL_10;
    public static int CLANREP_CLAN_LVL_11;
    public static int CLANREP_CREATE_ROYAL1;
    public static int CLANREP_CREATE_ROYAL2;
    public static int CLANREP_CREATE_ROYAL3;
    public static int CLANREP_CREATE_KNIGHT1;
    public static int CLANREP_CREATE_KNIGHT2;
    public static int CLANREP_CREATE_KNIGHT3;
    public static int CLANREP_CREATE_KNIGHT4;
    public static int CLANREP_CREATE_KNIGHT5;
    public static int CLANREP_CREATE_KNIGHT6;
    public static int CLAN_EXP_TO_LVL_1;
    public static int CLAN_EXP_TO_LVL_2;
    public static int CLAN_EXP_TO_LVL_3;
    public static int CLAN_EXP_TO_LVL_4;
    public static int CLAN_EXP_TO_LVL_5;
    public static int CLAN_ADENA_TO_LVL_1;
    public static int CLAN_ADENA_TO_LVL_2;
    public static int CLAN_ITEM_TO_LVL_3;
    public static int CLAN_ITEM_TO_LVL_4;
    public static int CLAN_ITEM_TO_LVL_5;
    public static int CLAN_ITEM_TO_LVL_9;
    public static int CLAN_ITEM_TO_LVL_10;
    public static int CLAN_COUNT_ITEM_TO_LVL_3;
    public static int CLAN_COUNT_ITEM_TO_LVL_4;
    public static int CLAN_COUNT_ITEM_TO_LVL_5;
    public static int CLAN_COUNT_ITEM_TO_LVL_9;
    public static int CLAN_COUNT_ITEM_TO_LVL_10;
    public static int CLAN_MAX_USER_IN_AKADEM;
    public static int CLAN_MAX_USER_IN_CLAN_0;
    public static int CLAN_MAX_USER_IN_CLAN_1;
    public static int CLAN_MAX_USER_IN_CLAN_2;
    public static int CLAN_MAX_USER_IN_CLAN_3;
    public static int CLAN_MAX_USER_IN_CLAN_4;
    public static int CLAN_MAX_USER_IN_CLAN_5;
    public static int CLAN_MAX_USER_IN_CLAN_6;
    public static int CLAN_MAX_USER_IN_CLAN_7;
    public static int CLAN_MAX_USER_IN_CLAN_8;
    public static int CLAN_MAX_USER_IN_CLAN_9;
    public static int CLAN_MAX_USER_IN_CLAN_10;
    public static int CLAN_MAX_USER_IN_CLAN_11;
    public static int CLAN_MAX_USER_IN_ROYAL_5_LVL;
    public static int CLAN_MAX_USER_IN_ROYAL_6_LVL;
    public static int CLAN_MAX_USER_IN_ROYAL_7_LVL;
    public static int CLAN_MAX_USER_IN_ROYAL_8_LVL;
    public static int CLAN_MAX_USER_IN_ROYAL_9_LVL;
    public static int CLAN_MAX_USER_IN_ROYAL_10_LVL;
    public static int CLAN_MAX_USER_IN_ROYAL_11_LVL;
    public static int CLAN_MAX_USER_IN_KNIGHT_7_LVL;
    public static int CLAN_MAX_USER_IN_KNIGHT_8_LVL;
    public static int CLAN_MAX_USER_IN_KNIGHT_9_LVL;
    public static int CLAN_MAX_USER_IN_KNIGHT_10_LVL;
    public static int CLAN_MAX_USER_IN_KNIGHT_11_LVL;

    public static int CLAN_MAX_USER_IN_ROYAL_3;

    public static void loadAntiFlood(final L2Properties _settings) {
        try {
            ANTIFLOOD_ENABLE = _settings.getBoolean("AntiFloodEnable", false);
            if (!ANTIFLOOD_ENABLE)
                return;
            MAX_UNHANDLED_SOCKETS_PER_IP = _settings.getInteger("MaxUnhandledSocketsPerIP", 5);
            UNHANDLED_SOCKET_MIN_TTL = _settings.getInteger("UnhandledSocketsMinTTL", 5000);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load AntiFlood L2Properties.");
        }
    }

    public static void reloadPacketLoggerConfig() {
        try {
            final L2Properties serverSettings = new L2Properties(CONFIGURATION_FILE);

            LOG_CLIENT_PACKETS = serverSettings.getBoolean("LogClientPackets", false);
            LOG_SERVER_PACKETS = serverSettings.getBoolean("LogServerPackets", false);
            PACKETLOGGER_FLUSH_SIZE = serverSettings.getInteger("LogPacketsFlushSize", 8192);

            String temp = serverSettings.getProperty("LogPacketsFromIPs", "").trim();
            if (temp.isEmpty())
                PACKETLOGGER_IPS = null;
            else {
                PACKETLOGGER_IPS = new NetList();
                PACKETLOGGER_IPS.LoadFromString(temp, ",");
            }

            temp = serverSettings.getProperty("LogPacketsFromAccounts", "").trim();
            if (temp.isEmpty())
                PACKETLOGGER_ACCOUNTS = null;
            else {
                PACKETLOGGER_ACCOUNTS = new GArray<String>();
                for (final String s : temp.split(","))
                    PACKETLOGGER_ACCOUNTS.add(s);
            }

            temp = serverSettings.getProperty("LogPacketsFromChars", "").trim();
            if (temp.isEmpty())
                PACKETLOGGER_CHARACTERS = null;
            else {
                PACKETLOGGER_CHARACTERS = new GArray<String>();
                for (final String s : temp.split(","))
                    PACKETLOGGER_CHARACTERS.add(s);
            }

            temp = null;
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load PacketLogger Config.");
        }
    }

    /**
     * Create input stream for log file -- or store file data into memory
     */
    public static void initLogging() throws IOException {
        FileUtils.forceMkdir(new File("log"));
        FileUtils.forceMkdir(new File("log/game"));
        InputStream is = null;
        try {
            is = new FileInputStream(new File("./config/log.ini"));
            LogManager.getLogManager().readConfiguration(is);
            is.close();
        } catch (final IOException e) {
            _log.log(Level.WARNING, "", e);
        } finally {
            if (is != null)
                try {
                    is.close();
                } catch (final Exception e) {
                }
        }
    }

    // XXX загрузка конфигов
    public static void load() {
        if (Server.SERVER_MODE == Server.MODE_GAMESERVER || Server.SERVER_MODE == Server.MODE_COMBOSERVER) {
            _log.info("Loading gameserver config.");
            reloadPacketLoggerConfig();
            loadl2sSettings();
            loadFameSettings();
            loadServerSettings();
            loadClanSettings();
            loadResSettings();

            // telnet
            try {
                final L2Properties telnetSettings = new L2Properties(TELNET_FILE);
                IS_TELNET_ENABLED = telnetSettings.getBoolean("EnableTelnet", false);
            } catch (final Exception e) {
                _log.log(Level.WARNING, "", e);
                throw new Error("Failed to Load " + TELNET_FILE + " File.");
            }

            loadRateSettings();
            loadWeddingSettings();
            loadLotterySettings();
            loadWalkerSettings();
            loadChatSettings();
            loadOtherSettings();
            loadSpoilSettings();
            loadCommandSettings();
            loadManorSettings();
            loadAlternativeSettings();
            loadVitalitySettings();
            loadBossSettings();
            loadOlympiadSettings();
            loadServiceSettings();
            loadKarmaSettings();
            loadAISettings();
            loadSiegeSettings();
            loadHexidSettings();
            loadEventsSettings();
            loadPcCafeConfig();
            loadCommunityBoardSettings();
            loadGeoConfig();
            loadEnchantSettings();
            loadSkillsSettings();
            loadChampionSettings();

            loadProtectionSettings();
            loadTerritoryWarSettings();
            loadInstancesSettings();
            loadHellboundSettings();
            loadItemAuctionSettings();

            abuseLoad();

            loadGMAccess();
            _log.info("loading GMAccess");

            if (ADVIPSYSTEM)
                ipsLoad();
        } else if (Server.SERVER_MODE == Server.MODE_LOGINSERVER) {
            _log.info("loading login config");
            try {
                final L2Properties serverSettings = new L2Properties(LOGIN_CONFIGURATION_FILE);

                SELECTOR_SLEEP_TIME = serverSettings.getInteger("SelectorSleepTime", 5);
                LOGIN_HOST = serverSettings.getProperty("LoginserverHostname", "127.0.0.1");
                GAME_SERVER_LOGIN_PORT = serverSettings.getInteger("LoginPort", 9013);
                GAME_SERVER_LOGIN_HOST = serverSettings.getProperty("LoginHost", "127.0.0.1");
                PORT_LOGIN = serverSettings.getInteger("LoginserverPort", 2106);

                DEFAULT_PASSWORD_ENCODING = AlgorithmType.getAlgorithm(serverSettings.getProperty("DefaultPasswordEncoding", "Whirlpool"));
                LEGACY_PASSWORD_ENCODING = serverSettings.getProperty("LegacyPasswordEncoding", "SHA1;DES");
                DOUBLE_WHIRPOOL_SALT = serverSettings.getProperty("DoubleWhirlpoolSalt", "L2System");

                LOGIN_BLOWFISH_KEYS = serverSettings.getInteger("BlowFishKeys", 20);
                LOGIN_RSA_KEYPAIRS = serverSettings.getInteger("RSAKeyPairs", 10);

                COMBO_MODE = serverSettings.getBoolean("ComboMode", false);
                LOGIN_DEBUG = serverSettings.getBoolean("Debug", false);

                ACCEPT_NEW_GAMESERVER = serverSettings.getBoolean("AcceptNewGameServer", true);

                LOGIN_PING = serverSettings.getBoolean("PingServer", true);
                LOGIN_PING_TIME = serverSettings.getInteger("WaitPingTime", 5);

                DATABASE_DRIVER = serverSettings.getProperty("Driver", "com.mysql.jdbc.Driver");
                DATABASE_URL = serverSettings.getProperty("URL", "jdbc:mysql://localhost/l2s");
                DATABASE_LOGIN = serverSettings.getProperty("Login", "root");
                DATABASE_PASSWORD = serverSettings.getProperty("Password", "");
                DATABASE_MAX_CONNECTIONS = serverSettings.getInteger("MaximumDbConnections", 10);
                DATABASE_MAX_IDLE_TIMEOUT = serverSettings.getInteger("MaxIdleConnectionTimeout", 600);
                DATABASE_IDLE_TEST_PERIOD = serverSettings.getInteger("IdleConnectionTestPeriod", 60);
                USE_DATABASE_LAYER = serverSettings.getBoolean("UseDatabaseLayer", true);

                DATABASE_TYPE_CONNECTING_POOL = ConnectingPoolType.valueOf(serverSettings.getProperty("DataBaseTypeConnectingPool", "DBCP"));

                SHOW_LICENCE = serverSettings.getBoolean("ShowLicence", true);

                AUTO_CREATE_ACCOUNTS = serverSettings.getBoolean("AutoCreateAccounts", true);
                IP_UPDATE_TIME = serverSettings.getInteger("IpUpdateTime", 15);
                ANAME_TEMPLATE = serverSettings.getProperty("AnameTemplate", "[A-Za-z0-9]{3,14}"); // FIXME сделать проверку
                APASSWD_TEMPLATE = serverSettings.getProperty("ApasswdTemplate", "[A-Za-z0-9]{5,16}"); // FIXME сделать проверку
                LOGIN_GG_CHECK = serverSettings.getBoolean("GGCheck", true);
                LRESTART_TIME = serverSettings.getInteger("AutoRestart", -1);
                TIMEOUT_CHECKER_CLIENT = serverSettings.getLong("TimeOutChecker", 2000);

                final String internalIpList = serverSettings.getProperty("InternalIpList", "127.0.0.1,192.168.0.0-192.168.255.255,10.0.0.0-10.255.255.255,172.16.0.0-172.16.31.255");
                if (internalIpList.startsWith("NetList@")) {
                    INTERNAL_NETLIST = new NetList();
                    INTERNAL_NETLIST.LoadFromFile(internalIpList.replaceFirst("NetList@", ""));
                    _log.info("Loaded " + INTERNAL_NETLIST.NetsCount() + " Internal Nets");
                } else {
                    INTERNAL_IP = new GArray<String>();
                    INTERNAL_IP.addAll(Arrays.asList(internalIpList.split(",")));
                }

                SUPER_ACCESS_ENABLE = serverSettings.getBoolean("AllowSuperAccess", false);
                SUPER_ACCESS_PASSWORD = serverSettings.getProperty("SuperPassword", "000000");
                final String IpList = serverSettings.getProperty("AllowIpList", "127.0.0.1");
                SUPER_ACCESS_IP_LIST = new NetList();
                SUPER_ACCESS_IP_LIST.LoadFromString(IpList, ",");

            } catch (final Exception e) {
                _log.log(Level.WARNING, "", e);
                throw new Error("Failed to Load " + LOGIN_CONFIGURATION_FILE + " File.");
            }

            // telnet
            try {
                final L2Properties telnetSettings = new L2Properties(LOGIN_TELNET_FILE);
                IS_LOGIN_TELNET_ENABLED = telnetSettings.getBoolean("EnableTelnet", false);
            } catch (final Exception e) {
                _log.log(Level.WARNING, "", e);
                throw new Error("Failed to Load " + TELNET_FILE + " File.");
            }

            loadLoginProtectionSettings();
        } else
            _log.severe("Could not Load Config: server mode was not set");

    }

    // ========================================================================
    public static final String LOGIN_PROTECTION_FILE = "./config/login_protection.ini";

    // FIXME public static int LOGIN_CLIENT_EXPIRE_TIME;
    // FIXME public static int LOGIN_CLIENT_EXPIRE_TIME_PENDING;

    public static boolean LOGIN_CLIENT_LOGIN_IP_TRY_SHOW_LOG;
    public static boolean LOGIN_CLIENT_LOGIN_IPBANN_SHOW_LOG;

    public static int LOGIN_CLIENT_IP_FLOOD_FORGET_TIME;
    public static int LOGIN_CLIENT_AUTH_FLOOD_FORGET_TIME;

    public static int LOGIN_CLIENT_CLEANUP_TIMEOUT;

    public static int LOGIN_CLIENT_LOGIN_AUTH_TRY_BLOCK_TIME;
    public static int LOGIN_CLIENT_LOGIN_AUTH_TRY_TIME;
    public static int LOGIN_CLIENT_LOGIN_AUTH_TRY_MAX_COUNT;

    public static int LOGIN_CLIENT_LOGIN_IP_TRY_BLOCK_TIME;
    public static int LOGIN_CLIENT_LOGIN_IP_TRY_TIME;
    public static int LOGIN_CLIENT_LOGIN_IP_TRY_MAX_COUNT;

    public static int LOGIN_CLIENT_FLOOD_INFO_POOL_MAX_SIZE;

    /**
     * Конфиг на включение защиты от брута паролей.
     **/
    public static boolean ENABLE_FAKE_LOGIN;
    public static boolean ENABLE_BAN_SUSPICIOUS_ACTIVITY;
    public static int BAN_SUSPICIOUS_BLOCK_TIME;

    // ========================================================================
    public static void loadLoginProtectionSettings() {
        try {
            final L2Properties serverSettings = new L2Properties(LOGIN_PROTECTION_FILE);
            loadAntiFlood(serverSettings);
            ENABLE_FAKE_LOGIN = serverSettings.getBoolean("EnableFakeLogin", true);
            ENABLE_BAN_SUSPICIOUS_ACTIVITY = serverSettings.getBoolean("EnableBanSuspiciousActivity", true);
            BAN_SUSPICIOUS_BLOCK_TIME = serverSettings.getInteger("BanSuspiciousActivityBlockTime", 60000);

            // LOGIN_CLIENT_EXPIRE_TIME = serverSettings.getInteger("ExpireTime", 30000);
            // LOGIN_CLIENT_EXPIRE_TIME_PENDING = serverSettings.getInteger("ExpireTimePending", 12000);
            LOGIN_CLIENT_IP_FLOOD_FORGET_TIME = serverSettings.getInteger("IpFloodForgetTime", 60000);
            LOGIN_CLIENT_AUTH_FLOOD_FORGET_TIME = serverSettings.getInteger("AuthFloodForgetTime", 60000);
            LOGIN_CLIENT_CLEANUP_TIMEOUT = serverSettings.getInteger("CleanupTimeout", 10000);

            LOGIN_CLIENT_FLOOD_INFO_POOL_MAX_SIZE = serverSettings.getInteger("FloodInfoPoolMaxSize", 200);
            LOGIN_CLIENT_LOGIN_IPBANN_SHOW_LOG = serverSettings.getBoolean("LoginIPBannShowLog", true);

            LOGIN_CLIENT_LOGIN_AUTH_TRY_BLOCK_TIME = serverSettings.getInteger("LoginAuthTryBlockTime", 60000);
            LOGIN_CLIENT_LOGIN_AUTH_TRY_TIME = serverSettings.getInteger("LoginAuthTryTime", 30000);
            LOGIN_CLIENT_LOGIN_AUTH_TRY_MAX_COUNT = serverSettings.getInteger("LoginAuthTryMaxCount", 5);

            LOGIN_CLIENT_LOGIN_IP_TRY_SHOW_LOG = serverSettings.getBoolean("LoginIpTryShowLog", true);
            LOGIN_CLIENT_LOGIN_IP_TRY_BLOCK_TIME = serverSettings.getInteger("LoginIpTryBlockTime", 60000);
            LOGIN_CLIENT_LOGIN_IP_TRY_TIME = serverSettings.getInteger("LoginIpTryTime", 30000);
            LOGIN_CLIENT_LOGIN_IP_TRY_MAX_COUNT = serverSettings.getInteger("LoginIpTryMaxCount", 10);

        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + LOGIN_PROTECTION_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String CONFIGURATION_FILE = "./config/gameserver.ini";

    // ========================================================================
    public static void loadServerSettings() {
        try {
            final L2Properties serverSettings = new L2Properties(CONFIGURATION_FILE);

            loadAntiFlood(serverSettings);
            GAME_SERVER_LOGIN_HOST = serverSettings.getProperty("LoginHost", "127.0.0.1");
            GAME_SERVER_LOGIN_PORT = serverSettings.getInteger("LoginPort", 9013);
            ADVIPSYSTEM = serverSettings.getBoolean("AdvIPSystem", false);

            HIDE_GM_STATUS = serverSettings.getBoolean("HideGMStatus", false);
            SAVE_GM_EFFECTS = serverSettings.getBoolean("SaveGMEffects", false);

            GM_STARTUP_INVULNERABLE = serverSettings.getBoolean("GMStartupInvulnerable", false);
            GM_STARTUP_INVISIBLE = serverSettings.getBoolean("GMStartupInvisible", false);

            REQUEST_ID = serverSettings.getInteger("RequestServerID", 0);
            ACCEPT_ALTERNATE_ID = serverSettings.getBoolean("AcceptAlternateID", true);

            PORT_GAME = serverSettings.getInteger("GameserverPort", 7777);
            CNAME_TEMPLATE = Pattern.compile(serverSettings.getProperty("CnameTemplate", "[A-Za-z0-9\u0410-\u042f\u0430-\u044f]{2,16}"));
            CLAN_NAME_TEMPLATE = Pattern.compile(serverSettings.getProperty("ClanNameTemplate", "[A-Za-z0-9\u0410-\u042f\u0430-\u044f]{3,16}"));
            CLAN_TITLE_TEMPLATE = Pattern.compile(serverSettings.getProperty("ClanTitleTemplate", "[A-Za-z0-9\u0410-\u042f\u0430-\u044f \\p{Punct}]{1,16}"));
            ALLY_NAME_TEMPLATE = Pattern.compile(serverSettings.getProperty("AllyNameTemplate", "[A-Za-z0-9\u0410-\u042f\u0430-\u044f]{3,16}"));
            GAMESERVER_HOSTNAME = serverSettings.getProperty("GameserverHostname");

            ALLOW_SPECIAL_COMMANDS = serverSettings.getBoolean("AllowSpecialCommands", false);
            LOG_CHAT = serverSettings.getBoolean("LogChat", false);
            LOG_KILLS = serverSettings.getBoolean("LogKills", false);
            LOG_TELNET = serverSettings.getBoolean("LogTelnet", false);

            AUTODESTROY_ITEM_AFTER = serverSettings.getInteger("AutoDestroyDroppedItemAfter", 0);
            DELETE_DAYS = serverSettings.getInteger("DeleteCharAfterDays", 7);
            ENABLE_GC_TASK = serverSettings.getBoolean("GC_Task", true);
            GARBAGE_COLLECTOR_TASK_FREQUENCY = serverSettings.getInteger("GCTaskFrequency", 1800);

            AUTOSAVE = serverSettings.getBoolean("Autosave", true);
            AUTOSAVE_MIN_TIME = serverSettings.getInteger("AutosaveMinTime", 600) * 1000;

            DATAPACK_ROOT = new File(serverSettings.getProperty("DatapackRoot", ".")).getCanonicalFile();

            ALLOW_DISCARDITEM = serverSettings.getBoolean("AllowDiscardItem", true);
            ALLOW_FREIGHT = serverSettings.getBoolean("AllowFreight", true);
            ALLOW_WAREHOUSE = serverSettings.getBoolean("AllowWarehouse", true);
            ALLOW_LOTTERY = serverSettings.getBoolean("AllowLottery", false);
            ALLOW_WATER = serverSettings.getBoolean("AllowWater", true);
            ALLOW_BOAT = serverSettings.getBoolean("AllowBoat", false);
            ALLOW_CURSED_WEAPONS = serverSettings.getBoolean("AllowCursedWeapons", false);
            CURSED_WEAPONS_MIN_PLAYERS_DROP = serverSettings.getInteger("CursedWeaponsMinPlayersDrop", 0);
            DROP_CURSED_WEAPONS_ON_KICK = serverSettings.getBoolean("DropCursedWeaponsOnKick", false);

            MIN_PROTOCOL_REVISION = serverSettings.getInteger("MinProtocolRevision", 83);
            MAX_PROTOCOL_REVISION = serverSettings.getInteger("MaxProtocolRevision", 87);

            if (MIN_PROTOCOL_REVISION > MAX_PROTOCOL_REVISION)
                throw new Error("MinProtocolRevision is bigger than MaxProtocolRevision in server configuration file.");

            INTERNAL_HOSTNAME = serverSettings.getProperty("InternalHostname", "*");
            EXTERNAL_HOSTNAME = serverSettings.getProperty("ExternalHostname", "*");

            SERVER_SIDE_NPC_NAME = serverSettings.getBoolean("ServerSideNpcName", false);
            SERVER_SIDE_NPC_TITLE_WITH_LVL = serverSettings.getBoolean("ServerSideNpcTitleWithLvl", false);
            SERVER_SIDE_NPC_TITLE = serverSettings.getBoolean("ServerSideNpcTitle", false);

            HARD_DB_CLEANUP_ON_START = serverSettings.getBoolean("HardDbCleanUpOnStart", false);

            MAXIMUM_ONLINE_USERS = serverSettings.getInteger("MaximumOnlineUsers", 100);

            SHOW_COPYRIGHT = serverSettings.getBoolean("ShowCopyright", true);

            DATABASE_DRIVER = serverSettings.getProperty("Driver", "com.mysql.jdbc.Driver");
            DATABASE_URL = serverSettings.getProperty("URL", "jdbc:mysql://localhost/l2s");
            DATABASE_LOGIN = serverSettings.getProperty("Login", "root");
            DATABASE_PASSWORD = serverSettings.getProperty("Password", "");
            DATABASE_MAX_CONNECTIONS = serverSettings.getInteger("MaximumDbConnections", 10);
            DATABASE_MAX_IDLE_TIMEOUT = serverSettings.getInteger("MaxIdleConnectionTimeout", 600);
            DATABASE_IDLE_TEST_PERIOD = serverSettings.getInteger("IdleConnectionTestPeriod", 60);
            USE_DATABASE_LAYER = serverSettings.getBoolean("UseDatabaseLayer", true);

            DATABASE_TYPE_CONNECTING_POOL = ConnectingPoolType.valueOf(serverSettings.getProperty("DataBaseTypeConnectingPool", "DBCP"));

            ACCOUNTS_DATABASE_URL = serverSettings.getProperty("LoginDB_URL", DATABASE_URL);
            ACCOUNTS_DATABASE_LOGIN = serverSettings.getProperty("LoginDB_Login", DATABASE_LOGIN);
            ACCOUNTS_DATABASE_PASSWORD = serverSettings.getProperty("LoginDB_Password", ACCOUNTS_DATABASE_LOGIN.equals(DATABASE_LOGIN) ? DATABASE_PASSWORD : "");

            LAZY_ITEM_UPDATE = serverSettings.getBoolean("LazyItemUpdate", false);
            LAZY_ITEM_UPDATE_ALL = serverSettings.getBoolean("LazyItemUpdateAll", false);
            LAZY_ITEM_UPDATE_TIME = serverSettings.getInteger("LazyItemUpdateTime", 60000);
            LAZY_ITEM_UPDATE_ALL_TIME = serverSettings.getInteger("LazyItemUpdateAllTime", 60000);
            DELAYED_ITEMS_UPDATE_INTERVAL = serverSettings.getInteger("DelayedItemsUpdateInterval", 10000);

            USER_INFO_INTERVAL = serverSettings.getInteger("UserInfoInterval", 100);
            BROADCAST_STATS_INTERVAL = serverSettings.getBoolean("BroadcastStatsInterval", true);
            BROADCAST_CHAR_INFO_INTERVAL = serverSettings.getInteger("BroadcastCharInfoInterval", 100);
            SAVE_GAME_TIME_INTERVAL = serverSettings.getInteger("SaveGameTimeInterval", 120);

            DEFAULT_LANG = serverSettings.getProperty("DefaultLang", "ru");
            ENG_QUEST_NAMES = serverSettings.getBoolean("EngQuestNames", true);
            RESTART_TIME = serverSettings.getInteger("AutoRestart", 0);
            RESTART_AT_TIME = serverSettings.getInteger("AutoRestartAt", 5);
            if (RESTART_AT_TIME > 24)
                RESTART_AT_TIME = 24;

            CHECK_LANG_FILES_MODIFY = serverSettings.getBoolean("checkLangFilesModify", false);

            USE_FILE_CACHE = serverSettings.getBoolean("useFileCache", true);

            LINEAR_TERRITORY_CELL_SIZE = serverSettings.getInteger("LinearTerritoryCellSize", 32);

            DISABLE_CREATION_IDs = serverSettings.getProperty("DisableCreateItems", "");
            if (DISABLE_CREATION_IDs.length() != 0)
                for (final String id : DISABLE_CREATION_IDs.split(","))
                    DISABLE_CREATION_ID_LIST.add(TypeFormat.parseInt(id));

            LOG_MULTISELL_IDs = serverSettings.getProperty("LogMultisellId", "");
            if (LOG_MULTISELL_IDs.length() != 0)
                for (final String id : LOG_MULTISELL_IDs.split(","))
                    LOG_MULTISELL_ID_LIST.add(TypeFormat.parseInt(id));

            MOVE_PACKET_DELAY = serverSettings.getInteger("MovePacketDelay", 100);
            ATTACK_PACKET_DELAY = serverSettings.getInteger("AttackPacketDelay", 500);

            DAMAGE_FROM_FALLING = serverSettings.getBoolean("DamageFromFalling", true);

            GEOEDITOR_HOST = serverSettings.getProperty("GeoEditorHost", "127.0.0.1");

            PLAYER_LOGOUT_INGAME_TIME = serverSettings.getLong("LogoutIngameTime", 60) * 1000;
            PLAYER_DISCONNECT_INGAME_TIME = serverSettings.getLong("DisconnectedIngameTime", 90) * 1000;
            PLAYER_DISCONNECT_TITILE = serverSettings.getProperty("DisconnectedTitile", "");

            GG_CHECK = serverSettings.getBoolean("GGCheck", true);

            SELECTOR_SLEEP_TIME = serverSettings.getInteger("SelectorSleepTime", 5);
            TIMEOUT_CHECKER_CLIENT = serverSettings.getLong("TimeOutChecker", 2000);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + CONFIGURATION_FILE + " File.");
        }
    }


    // ========================================================================
    public static final String CLAN_FILE = "./config/clan.ini";

    // ========================================================================
    public static void loadClanSettings() {
        try {
            final L2Properties clanRepSettings = new L2Properties(CLAN_FILE);

            DAYS_BEFORE_CREATE_A_CLAN = clanRepSettings.getInteger("DaysBeforeCreateAClan", 24);
            HOURS_BEFORE_JOIN_A_CLAN = clanRepSettings.getInteger("HoursBeforeJoinAClan", 24);
            ALT_CLAN_MEMBERS_FOR_WAR = clanRepSettings.getInteger("AltClanMembersForWar", 15);
            ALT_MIN_CLAN_LVL_FOR_WAR = clanRepSettings.getInteger("AltMinClanLvlForWar", 3);
            ALT_CLAN_WAR_MAX = clanRepSettings.getInteger("AltClanWarMax", 30);
            DAYS_BEFORE_CREATE_NEW_ALLY_WHEN_DISSOLVED = clanRepSettings.getInteger("DaysBeforeCreateNewAllyWhenDissolved", 24);
            DAYS_BEFORE_CLAN_INVITE = clanRepSettings.getInteger("InveteNewClanChar", 24);
            MIN_LEVEL_TO_CREATE_PLEDGE = clanRepSettings.getInteger("MinLevelToCreateClan", 10);

            RATE_CLAN_REP_SCORE = clanRepSettings.getDouble("RateClanRepScore", 1);
            RATE_CLAN_REP_SCORE_MAX_AFFECTED = clanRepSettings.getInteger("RateClanRepScoreMaxAffected", 2);
            USE_REDUCE_REPSCORE_RATE = clanRepSettings.getBoolean("UseReduceRepScoreRate", false);
            USE_REDUCE_REPSCORE_RATE_MULTISELL = clanRepSettings.getBoolean("UseReduceRepScoreRate", false);
            USE_REDUCE_REPSCORE_RATE_CLAN_SKILL = clanRepSettings.getBoolean("UseReduceRepScoreClanSkill", false);
            CLANREP_HERO_REP = clanRepSettings.getInteger("HeroAddRepScore", 1000);
            CLANREP_FESTIVAL = clanRepSettings.getInteger("SevenSignsFestivalRepScore", 200);
            JOIN_ACADEMY_MIN_REP_SCORE = clanRepSettings.getInteger("CompleteAcademyMinPoints", 190);
            JOIN_ACADEMY_MAX_REP_SCORE = clanRepSettings.getInteger("CompleteAcademyMaxPoints", 650);
            CLANREP_QUEST_508 = clanRepSettings.getInteger("Quest508RepScore", 250);
            CLANREP_QUEST_509 = clanRepSettings.getInteger("Quest509RepScore", 150);
            CLANREP_QUEST_510 = clanRepSettings.getInteger("Quest510RepScore", 50);

            CLANREP_SIEGE_CASLE_NEW_OWNER = clanRepSettings.getInteger("SiegeCastleNewOwner", 1500);
            CLANREP_SIEGE_CASLE_OLD_OWNER = clanRepSettings.getInteger("SiegeCastleOldOwner", 750);
            CLANREP_SIEGE_CASLE_LOSS = clanRepSettings.getInteger("SiegeCastleLoss", 3000);
            CLANREP_SIEGE_FORT_NEW_OWNER = clanRepSettings.getInteger("SiegeFortNewOwner", 500);
            CLANREP_SIEGE_FORT_OLD_OWNER = clanRepSettings.getInteger("SiegeFortOldOwner", 750);
            CLANREP_SIEGE_FORT_LOSS = clanRepSettings.getInteger("SiegeFortLoss", 200);
            CLANREP_SIEGE_CH_WIN = clanRepSettings.getInteger("SiegeChOldOwner", 500);
            CLANREP_SIEGE_CH_LOSS = clanRepSettings.getInteger("SiegeChLoss", 300);
            CLANREP_SIEGE_CH_BADATTACK = clanRepSettings.getInteger("SiegeChBadAttack", 1000);
            BALLISTA_POINTS = clanRepSettings.getInteger("KillBallistaPoints", 30);
            REPUTATION_SCORE_PER_KILL = clanRepSettings.getInteger("ReputationScorePerKill", 1);

            CLAN_EXP_TO_LVL_1 = clanRepSettings.getInteger("ClanLvlUpExpForLevel1", 30000);
            CLAN_EXP_TO_LVL_2 = clanRepSettings.getInteger("ClanLvlUpExpForLevel2", 150000);
            CLAN_EXP_TO_LVL_3 = clanRepSettings.getInteger("ClanLvlUpExpForLevel3", 500000);
            CLAN_EXP_TO_LVL_4 = clanRepSettings.getInteger("ClanLvlUpExpForLevel4", 1400000);
            CLAN_EXP_TO_LVL_5 = clanRepSettings.getInteger("ClanLvlUpExpForLevel5", 3100000);
            CLAN_ADENA_TO_LVL_1 = clanRepSettings.getInteger("ClanLvlUpAdenaForLevel1", 650000);
            CLAN_ADENA_TO_LVL_2 = clanRepSettings.getInteger("ClanLvlUpAdenaForLevel2", 2500000);
            CLAN_ITEM_TO_LVL_3 = clanRepSettings.getInteger("ClanLvlUpItemForLevel3", 1419);
            CLAN_ITEM_TO_LVL_4 = clanRepSettings.getInteger("ClanLvlUpItemForLevel4", 3874);
            CLAN_ITEM_TO_LVL_5 = clanRepSettings.getInteger("ClanLvlUpItemForLevel5", 3870);
            CLAN_ITEM_TO_LVL_9 = clanRepSettings.getInteger("ClanLvlUpItemForLevel9", 9910);
            CLAN_ITEM_TO_LVL_10 = clanRepSettings.getInteger("ClanLvlUpItemForLevel10", 9911);
            CLAN_COUNT_ITEM_TO_LVL_3 = clanRepSettings.getInteger("ClanLvlUpItemCountForLevel3", 1);
            CLAN_COUNT_ITEM_TO_LVL_4 = clanRepSettings.getInteger("ClanLvlUpItemCountForLevel4", 1);
            CLAN_COUNT_ITEM_TO_LVL_5 = clanRepSettings.getInteger("ClanLvlUpItemCountForLevel5", 1);
            CLAN_COUNT_ITEM_TO_LVL_9 = clanRepSettings.getInteger("ClanLvlUpItemCountForLevel9", 150);
            CLAN_COUNT_ITEM_TO_LVL_10 = clanRepSettings.getInteger("ClanLvlUpItemCountForLevel10", 5);
            CLANREP_CLAN_LVL_6 = clanRepSettings.getInteger("ClanRepForLevel6", 10000);
            CLANREP_CLAN_LVL_7 = clanRepSettings.getInteger("ClanRepForLevel7", 20000);
            CLANREP_CLAN_LVL_8 = clanRepSettings.getInteger("ClanRepForLevel8", 40000);
            CLANREP_CLAN_LVL_9 = clanRepSettings.getInteger("ClanRepForLevel9", 40000);
            CLANREP_CLAN_LVL_10 = clanRepSettings.getInteger("ClanRepForLevel10", 40000);
            CLANREP_CLAN_LVL_11 = clanRepSettings.getInteger("ClanRepForLevel11", 75000);
            CLANREP_CREATE_ROYAL1 = clanRepSettings.getInteger("ClanCreateRoyal1", 5000);
            CLANREP_CREATE_ROYAL2 = clanRepSettings.getInteger("ClanCreateRoyal2", 5000);
            CLANREP_CREATE_ROYAL3 = clanRepSettings.getInteger("ClanCreateRoyal3", 5000);
            CLANREP_CREATE_KNIGHT1 = clanRepSettings.getInteger("ClanCreateKnight1", 10000);
            CLANREP_CREATE_KNIGHT2 = clanRepSettings.getInteger("ClanCreateKnight2", 10000);
            CLANREP_CREATE_KNIGHT3 = clanRepSettings.getInteger("ClanCreateKnight3", 10000);
            CLANREP_CREATE_KNIGHT4 = clanRepSettings.getInteger("ClanCreateKnight4", 10000);
            CLANREP_CREATE_KNIGHT5 = clanRepSettings.getInteger("ClanCreateKnight5", 10000);
            CLANREP_CREATE_KNIGHT6 = clanRepSettings.getInteger("ClanCreateKnight6", 10000);
            CLAN_MAX_USER_IN_AKADEM = clanRepSettings.getInteger("ClanMaxUserInAkadem", 20);
            CLAN_MAX_USER_IN_CLAN_0 = clanRepSettings.getInteger("ClanMaxUserInClanLvl0", 10);
            CLAN_MAX_USER_IN_CLAN_1 = clanRepSettings.getInteger("ClanMaxUserInClanLvl1", 15);
            CLAN_MAX_USER_IN_CLAN_2 = clanRepSettings.getInteger("ClanMaxUserInClanLvl2", 20);
            CLAN_MAX_USER_IN_CLAN_3 = clanRepSettings.getInteger("ClanMaxUserInClanLvl3", 30);
            CLAN_MAX_USER_IN_CLAN_4 = clanRepSettings.getInteger("ClanMaxUserInClanLvl4", 40);
            CLAN_MAX_USER_IN_CLAN_5 = clanRepSettings.getInteger("ClanMaxUserInClanLvl5", 40);
            CLAN_MAX_USER_IN_CLAN_6 = clanRepSettings.getInteger("ClanMaxUserInClanLvl6", 40);
            CLAN_MAX_USER_IN_CLAN_7 = clanRepSettings.getInteger("ClanMaxUserInClanLvl7", 40);
            CLAN_MAX_USER_IN_CLAN_8 = clanRepSettings.getInteger("ClanMaxUserInClanLvl8", 40);
            CLAN_MAX_USER_IN_CLAN_9 = clanRepSettings.getInteger("ClanMaxUserInClanLvl9", 40);
            CLAN_MAX_USER_IN_CLAN_10 = clanRepSettings.getInteger("ClanMaxUserInClanLvl10", 40);
            CLAN_MAX_USER_IN_CLAN_11 = clanRepSettings.getInteger("ClanMaxUserInClanLvl11", 40);
            CLAN_MAX_USER_IN_ROYAL_5_LVL = clanRepSettings.getInteger("ClanMaxUserInClanRoyalLvl5", 20);
            CLAN_MAX_USER_IN_ROYAL_6_LVL = clanRepSettings.getInteger("ClanMaxUserInClanRoyalLvl6", 20);
            CLAN_MAX_USER_IN_ROYAL_7_LVL = clanRepSettings.getInteger("ClanMaxUserInClanRoyalLvl7", 20);
            CLAN_MAX_USER_IN_ROYAL_8_LVL = clanRepSettings.getInteger("ClanMaxUserInClanRoyalLvl8", 20);
            CLAN_MAX_USER_IN_ROYAL_9_LVL = clanRepSettings.getInteger("ClanMaxUserInClanRoyalLvl9", 20);
            CLAN_MAX_USER_IN_ROYAL_10_LVL = clanRepSettings.getInteger("ClanMaxUserInClanRoyalLvl10", 20);
            CLAN_MAX_USER_IN_ROYAL_11_LVL = clanRepSettings.getInteger("ClanMaxUserInClanRoyalLvl11", 20);
            CLAN_MAX_USER_IN_KNIGHT_7_LVL = clanRepSettings.getInteger("ClanMaxUserInKnightInClanLvl7", 10);
            CLAN_MAX_USER_IN_KNIGHT_8_LVL = clanRepSettings.getInteger("ClanMaxUserInKnightInClanLvl8", 10);
            CLAN_MAX_USER_IN_KNIGHT_9_LVL = clanRepSettings.getInteger("ClanMaxUserInKnightInClanLvl9", 25);
            CLAN_MAX_USER_IN_KNIGHT_10_LVL = clanRepSettings.getInteger("ClanMaxUserInKnightInClanLvl10", 25);
            CLAN_MAX_USER_IN_KNIGHT_11_LVL = clanRepSettings.getInteger("ClanMaxUserInKnightInClanLvl11", 25);
            MEMBERFORLEVEL6 = clanRepSettings.getInteger("ClanMemberForLevel6", 30);
            MEMBERFORLEVEL7 = clanRepSettings.getInteger("ClanMemberForLevel7", 80);
            MEMBERFORLEVEL8 = clanRepSettings.getInteger("ClanMemberForLevel8", 120);
            MEMBERFORLEVEL9 = clanRepSettings.getInteger("ClanMemberForLevel9", 120);
            MEMBERFORLEVEL10 = clanRepSettings.getInteger("ClanMemberForLevel10", 140);
            MEMBERFORLEVEL11 = clanRepSettings.getInteger("ClanMemberForLevel11", 170);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + CLAN_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String TERRITORY_WAR = "./config/territorywar.ini";

    public static int SIEGE_DAY_OF_WEEK;
    public static int SIEGE_HOUR_OF_DAY;
    public static int DEFENDER_MAX_CLANS; // Max number of clans
    public static int DEFENDER_MAX_PLAYERS; // Max number of individual player
    public static int CLAN_MIN_LEVEL;
    public static int PLAYER_MIN_LEVEL;
    public static int MIN_TWBADGE_FOR_NOBLESS;
    public static int MIN_TWBADGE_FOR_STRIDERS;
    public static int MIN_TWBADGE_FOR_BIG_STRIDER;
    public static int TW_LENGTH;
    public static long TERRETORY_ALT_TIME_FLAG;

    // ========================================================================
    public static void loadTerritoryWarSettings() {
        try {
            final L2Properties territoryWarSettings = new L2Properties(TERRITORY_WAR);

            // Siege setting
            SIEGE_DAY_OF_WEEK = territoryWarSettings.getInteger("TerritoryWarSiegeDayOfWeek", 7);
            SIEGE_HOUR_OF_DAY = territoryWarSettings.getInteger("TerritoryWarSiegeHourOfDay", 20);
            TERRETORY_ALT_TIME_FLAG = territoryWarSettings.getLong("TerritoryWarAltGlag", 600000);
            DEFENDER_MAX_CLANS = territoryWarSettings.getInteger("DefenderMaxClans", 500);
            DEFENDER_MAX_PLAYERS = territoryWarSettings.getInteger("DefenderMaxPlayers", 500);
            CLAN_MIN_LEVEL = territoryWarSettings.getInteger("ClanMinLevel", 0);
            PLAYER_MIN_LEVEL = territoryWarSettings.getInteger("PlayerMinLevel", 40);
            TW_LENGTH = territoryWarSettings.getInteger("WarLength", 120);
            MIN_TWBADGE_FOR_NOBLESS = territoryWarSettings.getInteger("MinTerritoryBadgeForNobless", 100);
            MIN_TWBADGE_FOR_STRIDERS = territoryWarSettings.getInteger("MinTerritoryBadgeForStriders", 50);
            MIN_TWBADGE_FOR_BIG_STRIDER = territoryWarSettings.getInteger("MinTerritoryBadgeForBigStrider", 80);

        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + TERRITORY_WAR + " File.");
        }
    }

    // *******************************************************************************************

    public static boolean GIVE_NEW_CHARACTER_SS;
    public static long GIVE_NEW_CHARACTER_SS_COUNT;

    // ========================================================================
    public static final String OTHER_CONFIG_FILE = "./config/other.ini";

    // ========================================================================
    public static void loadOtherSettings() {
        try {
            final L2Properties otherSettings = new L2Properties(OTHER_CONFIG_FILE);

            DEEPBLUE_DROP_RULES = otherSettings.getBoolean("UseDeepBlueDropRules", true);
            DEEPBLUE_DROP_MAXDIFF = otherSettings.getInteger("DeepBlueDropMaxDiff", 8);
            DEEPBLUE_DROP_RAID_MAXDIFF = otherSettings.getInteger("DeepBlueDropRaidMaxDiff", 2);

            FLYING_SPEED = otherSettings.getInteger("FlyingSpeed", 220);
            RIDING_SPEED = otherSettings.getInteger("RidingSpeed", 200);
            SWIMING_SPEED = otherSettings.getInteger("SwimingSpeedTemplate", 80);

            /* Inventory slots limits */
            INVENTORY_MAXIMUM_NO_DWARF = otherSettings.getInteger("MaximumSlotsForNoDwarf", 80);
            INVENTORY_MAXIMUM_DWARF = otherSettings.getInteger("MaximumSlotsForDwarf", 100);
            INVENTORY_MAXIMUM_GM = otherSettings.getInteger("MaximumSlotsForGMPlayer", 250);
            MAX_ITEM_IN_PACKET = Math.max(INVENTORY_MAXIMUM_NO_DWARF, Math.max(INVENTORY_MAXIMUM_DWARF, INVENTORY_MAXIMUM_GM));

            MULTISELL_SIZE = otherSettings.getInteger("MultisellPageSize", 10);

            /* Warehouse slots limits */
            WAREHOUSE_SLOTS_NO_DWARF = otherSettings.getInteger("BaseWarehouseSlotsForNoDwarf", 100);
            WAREHOUSE_SLOTS_DWARF = otherSettings.getInteger("BaseWarehouseSlotsForDwarf", 120);
            WAREHOUSE_SLOTS_CLAN = otherSettings.getInteger("MaximumWarehouseSlotsForClan", 200);

            REGEN_SIT_WAIT = otherSettings.getBoolean("RegenSitWait", false);

            STARTING_ADENA = otherSettings.getInteger("StartingAdena", 0);
            CHAR_TITLE = otherSettings.getBoolean("CharTitle", false);
            ADD_CHAR_TITLE = otherSettings.getProperty("CharAddTitle", "Welcome");
            ENABLE_STARTING_ITEM = otherSettings.getBoolean("EnableStartingItem", false);

            SPAWN_CHAR = otherSettings.getBoolean("CustomSpawn", false);
            SPAWN_X = otherSettings.getInteger("SpawnX", 0);
            SPAWN_Y = otherSettings.getInteger("SpawnY", 0);
            SPAWN_Z = otherSettings.getInteger("SpawnZ", 0);

            STARTING_ITEM_ID_LIST = new IntArrayList(otherSettings.getIntArray("StartingItemsMageId"), false);
            STARTING_ITEM_COUNT_LIST = new IntArrayList(otherSettings.getIntArray("StartingItemsMageCount"), false);
            STARTING_ITEM_ID_LISTM = new IntArrayList(otherSettings.getIntArray("StartingItemsFighterId"), false);
            STARTING_ITEM_COUNT_LISTM = new IntArrayList(otherSettings.getIntArray("StartingItemsFighterCount"), false);

            STARTING_LEVEL = Math.min(otherSettings.getInteger("StartingLevel", 1), 86);
            STARTING_SP = otherSettings.getInteger("StartingSP", 0);
            AUTO_CHARACTER_NOBLESSE = otherSettings.getBoolean("NewCharacterNoblesse", false);

            UNSTUCK_SKILL = otherSettings.getBoolean("UnstuckSkill", true);

            /* Amount of HP, MP, and CP is restored */
            RESPAWN_RESTORE_CP = otherSettings.getDouble("RespawnRestoreCP", -1D) / 100D;
            RESPAWN_RESTORE_HP = otherSettings.getDouble("RespawnRestoreHP", 65D) / 100D;
            RESPAWN_RESTORE_MP = otherSettings.getDouble("RespawnRestoreMP", -1D) / 100D;

            /* Maximum number of available slots for pvt stores */
            MAX_PVTSTORE_SLOTS_DWARF = otherSettings.getInteger("MaxPvtStoreSlotsDwarf", 5);
            MAX_PVTSTORE_SLOTS_OTHER = otherSettings.getInteger("MaxPvtStoreSlotsOther", 4);
            MAX_PVTCRAFT_SLOTS = otherSettings.getInteger("MaxPvtManufactureSlots", 20);

            ANNOUNCE_MAMMON_SPAWN = otherSettings.getBoolean("AnnounceMammonSpawn", true);

            GM_NAME_COLOUR = Integer.decode("0x" + otherSettings.getProperty("GMNameColour", "FFFFFF"));
            GM_HERO_AURA = otherSettings.getBoolean("GMHeroAura", true);
            NORMAL_NAME_COLOUR = Integer.decode("0x" + otherSettings.getProperty("NormalNameColour", "FFFFFF"));
            CLANLEADER_NAME_COLOUR = Integer.decode("0x" + otherSettings.getProperty("ClanleaderNameColour", "FFFFFF"));
            BOT_NAME_COLOUR = Integer.decode("0x" + otherSettings.getProperty("BotNameColour", "FFFFFF"));
            BOT_NAME_HEX_COLOUR = otherSettings.getProperty("BotNameHexColour", "FFFFFF");

            SHOW_HTML_WELCOME = otherSettings.getBoolean("ShowHTMLWelcome", false);

            ONLINE_PLAYERS_AT_STARTUP = otherSettings.getBoolean("ShowOnlinePlayersAtStartup", true);

            ALLOW_CH_DOOR_OPEN_ON_CLICK = otherSettings.getBoolean("AllowChDoorOpenOnClick", true);
            DISABLE_GRADE_PENALTY = otherSettings.getBoolean("DisableGradePenalty", false);

            GIVE_NEW_CHARACTER_SS = otherSettings.getBoolean("GiveSSNewCharacter", true);
            GIVE_NEW_CHARACTER_SS_COUNT = otherSettings.getLong("GiveSSNewCharacterCount", 1000);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + OTHER_CONFIG_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String SPOIL_CONFIG_FILE = "./config/spoil.ini";

    // ========================================================================
    public static void loadSpoilSettings() {
        try {
            final L2Properties spoilSettings = new L2Properties(SPOIL_CONFIG_FILE);

            BASE_SPOIL_RATE = spoilSettings.getDouble("BasePercentChanceOfSpoilSuccess", 78);
            MINIMUM_SPOIL_RATE = spoilSettings.getDouble("MinimumPercentChanceOfSpoilSuccess", 1);
            ALT_SPOIL_FORMULA = spoilSettings.getBoolean("AltFormula", false);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + SPOIL_CONFIG_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String COMMAND_CONFIG_FILE = "./config/commands.ini";

    /**
     * Auto-loot command?
     */
    public static boolean COMMAND_ALLOW_AUTOLOOT_COMMAND;
    public static int ITEM_SUMMON;
    public static int ITEM_SUMMON_PRICE;
    public static boolean COMMAND_ALLOW_BUFF_COMMAND;
    public static boolean COMMAND_ALLOW_BUFF_COMMAND_OLY;
    public static boolean COMMAND_ALLOW_BUFF_COMMAND_SIEGE;

    /**
     * Стоимость группого баффа для магов
     */
    public static int COMMAND_BUFF_PRICE_MBUFF;
    /**
     * Итем который необходим для баффов для магов
     */
    public static int COMMAND_BUFF_PRICE_MBUFF_ITEM_ID;

    /**
     * Стоимость группого баффа для войнов
     */
    public static int COMMAND_BUFF_PRICE_FBUFF;
    /**
     * Итем который необходим для баффов для войнов
     */
    public static int COMMAND_BUFF_PRICE_FBUFF_ITEM_ID;

    public static List<String> COMMAND_FORBIDDEN_COMMAND_LIST;

    /***/
    public static int BUFF_COMMAND_PRICE_SBUFF = 60000;
    /***/
    public static int BUFF_COMMAND_PRICE_SBUFF_ITEM_ID = 57;

    // ========================================================================
    public static void loadCommandSettings() {
        try {
            final L2Properties commandSettings = new L2Properties(COMMAND_CONFIG_FILE);

            ITEM_SUMMON = commandSettings.getInteger("RelocateLeaderItemId", 57);
            ITEM_SUMMON_PRICE = commandSettings.getInteger("RelocateLeaderItemCount", 1000);
            COMMAND_ALLOW_AUTOLOOT_COMMAND = commandSettings.getBoolean("AllowAutoLootCommand", false);

            COMMAND_ALLOW_SAVE_LOCATION = commandSettings.getBoolean("EnableSaveCords", false);
            COMMAND_ALLOW_SAVE_LOCATION_PRICE = commandSettings.getInteger("PriceSaveCords", 1000);
            COMMAND_ALLOW_SAVE_LOCATION_ITEM = commandSettings.getInteger("ItemSaveCords", 4037);

            COMMAND_ALLOW_BUFF_COMMAND = commandSettings.getBoolean("AllowBuffCommand", false);
            COMMAND_ALLOW_BUFF_COMMAND_OLY = commandSettings.getBoolean("AllowBuffCommandOnOly", false);
            COMMAND_ALLOW_BUFF_COMMAND_SIEGE = commandSettings.getBoolean("AllowBuffCommandOnSiege", false);

            COMMAND_BUFF_PRICE_MBUFF = commandSettings.getInteger("BuffCommandPriceMbuff", 60000);
            COMMAND_BUFF_PRICE_MBUFF_ITEM_ID = commandSettings.getInteger("BuffCommandPriceMbuffItemId", 57);
            COMMAND_BUFF_PRICE_FBUFF = commandSettings.getInteger("BuffCommandPriceFbuff", 60000);
            COMMAND_BUFF_PRICE_FBUFF_ITEM_ID = commandSettings.getInteger("BuffCommandPriceFbuffItemId", 57);

            final String[] commands = commandSettings.getStringArray("ForbiddenCommandList");
            if (commands.length == 0)
                COMMAND_FORBIDDEN_COMMAND_LIST = Collections.emptyList();
            else
                COMMAND_FORBIDDEN_COMMAND_LIST = Arrays.asList(commands);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + SPOIL_CONFIG_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String ALT_SETTINGS_FILE = "./config/alternative.ini";

    public static boolean ALT_ENABLE_FIREWORKS;
    public static boolean ALT_LOAD_ELEMENTSTABLE;
    public static boolean ALT_ALLOW_TAKE_WEAPON;

    // ========================================================================
    public static void loadAlternativeSettings() {
        try {
            final L2Properties altSettings = new L2Properties(ALT_SETTINGS_FILE);

            ALT_SHORTCUTS_SKILL_VALIDATE = altSettings.getBoolean("ShortcutsSkillValidate", true);
            ALT_GAME_DELEVEL = altSettings.getBoolean("Delevel", true);

            INFINITE_BLESSEDSPIRITSHOT = altSettings.getBoolean("InfiniteBlessedSpiritShot", false);
            INFINITE_BEASTSHOT = altSettings.getBoolean("InfiniteBeastShot", false);
            INFINITE_SPIRITSHOT = altSettings.getBoolean("InfiniteSpiritShot", false);
            INFINITE_SOULSHOT = altSettings.getBoolean("InfiniteSoulShot", false);
            DONT_DESTROY_ARROWS = Boolean.parseBoolean(altSettings.getProperty("DontDestroyArrows", "false"));
            WEAR_TEST_ENABLED = altSettings.getBoolean("WearTestEnabled", false);
            AUTO_LOOT = altSettings.getBoolean("AutoLoot", false);
            AUTO_LOOT_ADENA = altSettings.getBoolean("AutoLootAdena", false);
            AUTO_LOOT_PK = altSettings.getBoolean("AutoLootPK", false);
            AUTO_LOOT_HERBS = altSettings.getBoolean("AutoLootHerbs", false);
            AUTO_LOOT_RAIDBOSS = altSettings.getBoolean("AutoLootRaidBoss", false);
            HERBS_DIVIDE = altSettings.getBoolean("HerbsDivide", false);
            ALT_ALL_CAN_KILL_IN_PEACE = altSettings.getBoolean("AltAllCanKillInPeaceZone", false);
            ALT_GAME_KARMAPLAYERCANBEKILLEDINPEACEZONE = altSettings.getBoolean("AltKarmaPlayerCanBeKilledInPeaceZone", false);
            ALT_GAME_KARMA_PLAYER_CAN_SHOP = altSettings.getBoolean("AltKarmaPlayerCanShop", false);
            ALT_GAME_KARMA_PLAYER_CAN_TELEPORT = altSettings.getBoolean("AltKarmaPlayerCanTeleport", true);
            ALT_PLAYER_PROTECTION_LEVEL = altSettings.getInteger("AltPlayerProtectionLevel", 0);
            SAVING_SPS = altSettings.getBoolean("SavingSpS", false);
            MANAHEAL_SPS_BONUS = altSettings.getBoolean("ManahealSpSBonus", false);
            KILL_COUNTER = altSettings.getBoolean("KillCounter", true);
            KILL_COUNTER_PRELOAD = altSettings.getBoolean("KillCounterPreload", true);
            DROP_COUNTER = altSettings.getBoolean("DropCounter", true);
            CRAFT_COUNTER = altSettings.getBoolean("CraftCounter", true);
            ALT_TRUE_CHESTS = altSettings.getInteger("TrueChests", 33);
            ALT_GAME_MATHERIALSDROP = altSettings.getBoolean("AltMatherialsDrop", false);
            ALT_DOUBLE_SPAWN = altSettings.getBoolean("DoubleSpawn", false);
            ALT_MASTERWORK_CONFIG = altSettings.getBoolean("AltMasterworkConfig", false);
            ALLOW_MASTERWORK = altSettings.getBoolean("AllowMasterwork", false);
            ALLOW_CRITICAL_CRAFT = altSettings.getBoolean("AllowCriticalCraft", false);
            ALT_GAME_EXP_FOR_CRAFT = altSettings.getBoolean("AltExpForCraft", false);
            ALT_GAME_UNREGISTER_RECIPE = altSettings.getBoolean("AltUnregisterRecipe", true);
            ALT_GAME_SHOW_DROPLIST = altSettings.getBoolean("AltShowDroplist", true);
            ALT_GAME_GEN_DROPLIST_ON_DEMAND = altSettings.getBoolean("AltGenerateDroplistOnDemand", false);
            ALLOW_NPC_SHIFTCLICK = altSettings.getBoolean("AllowShiftClick", true);
            ALT_FULL_NPC_STATS_PAGE = altSettings.getBoolean("AltFullStatsPage", false);
            ALT_GAME_SUBCLASS_WITHOUT_QUESTS = altSettings.getBoolean("AltAllowSubClassWithoutQuest", false);
            ALT_GAME_SUBCLASS_FOR_ITEMS = altSettings.getBoolean("AltAllowSubClassForItems", false);
            ALT_GAME_SUBCLASS_WITHOUT_FATES_WHISPER = altSettings.getBoolean("AltAllowSubClassWithoutFatesWhisper", false);
            ALT_GAME_LEVEL_TO_GET_SUBCLASS = altSettings.getInteger("AltLevelToGetSubclass", 75);
            ALT_DROP_LS_BOSS = altSettings.getInteger("AltDropLsFromBoss", 15);
            ALT_DROP_LS_RAID = altSettings.getInteger("AltDropLsFromRaid", 1);
            ALT_GAME_SUB_ADD = altSettings.getInteger("AltSubAdd", 0);
            ALT_MAX_LEVEL = Math.min(altSettings.getInteger("AltMaxLevel", 85), Experience.LEVEL.length - 1);
            ALT_MAX_SUB_LEVEL = Math.min(altSettings.getInteger("AltMaxSubLevel", 80), Experience.LEVEL.length - 1);
            ALT_SHIELD_BLOCKS = altSettings.getBoolean("AltShieldBlocks", false);
            CHANGE_CLAN_LEADER_TIME = altSettings.getInteger("ChangeClanLeaderTime", 30);
            ALT_ALLOW_OTHERS_WITHDRAW_FROM_CLAN_WAREHOUSE = altSettings.getBoolean("AltAllowOthersWithdrawFromClanWarehouse", true);
            ALT_GAME_REQUIRE_CLAN_CASTLE = altSettings.getBoolean("AltRequireClanCastle", false);
            ALT_GAME_REQUIRE_CASTLE_DAWN = altSettings.getBoolean("AltRequireCastleDawn", true);
            ALT_ADD_RECIPES = altSettings.getInteger("AltAddRecipes", 0);
            ALT_100_RECIPES = altSettings.getBoolean("Alt100PercentRecipes", false);
            SS_ANNOUNCE_PERIOD = altSettings.getInteger("SSAnnouncePeriod", 0);
            ENABLE_FISHING_CHECK_WATER = altSettings.getBoolean("EnableFishingWaterCheck", true);

            ALT_HONOR_SYSTEM = altSettings.getBoolean("HonorSystem", false);
            ALT_HONOR_SYSTEM_IN_PVP_ZONE = altSettings.getBoolean("HonorSystemInPvPZone", false);
            ALT_HONOR_SYSTEM_WON_ITEMID = altSettings.getInteger("HonorSystemWonItemId", 4674);
            ALT_HONOR_SYSTEM_WON_ITEM_COUNT = altSettings.getInteger("HonorSystemWonItemCount", 3);
            ALT_HONOR_SYSTEM_LOSE_ITEMID = altSettings.getInteger("HonorSystemLoseItemId", 4674);
            ALT_HONOR_SYSTEM_LOSE_ITEM_COUNT = altSettings.getInteger("HonorSystemLoseItemCount", 1);
            ALT_HONOR_SYSTEM_PVP_ITEMID = altSettings.getInteger("HonorSystemPvPItemId", 775);
            ALT_HONOR_SYSTEM_PVP_ITEM_COUNT = altSettings.getInteger("HonorSystemPvPItemCount", 3);

            ALT_SOCIAL_ACTION_REUSE = altSettings.getBoolean("AltSocialActionReuse", false);
            ALT_PROF_SOCIAL = altSettings.getBoolean("AltEnableProffSocialAction", true);
            ALT_PROF_SOCIAL_ID = altSettings.getInteger("AltProffSocialActionID", 3);
            ALT_SIMPLE_SIGNS = altSettings.getBoolean("PushkinSignsOptions", false);
            ALT_MAMMON_UPGRADE = altSettings.getInteger("MammonUpgrade", 6680500);
            ALT_MAMMON_EXCHANGE = altSettings.getInteger("MammonExchange", 10091400);
            ALT_ALLOW_TATTOO = altSettings.getBoolean("AllowTattoo", false);
            ALT_BUFF_LIMIT = altSettings.getInteger("BuffLimit", 20);
            ALT_DANCE_SONG_LIMIT = altSettings.getInteger("DanceSongLimit", 12);
            ALT_DEATH_PENALTY = altSettings.getBoolean("EnableAltDeathPenalty", false);
            ALLOW_DEATH_PENALTY_C5 = altSettings.getBoolean("EnableDeathPenaltyC5", true);
            ALT_DEATH_PENALTY_C5_CHANCE = altSettings.getInteger("DeathPenaltyC5Chance", 10);
            ALT_DEATH_PENALTY_C5_CHAOTIC_RECOVERY = altSettings.getBoolean("ChaoticCanUseScrollOfRecovery", false);
            ALT_DEATH_PENALTY_C5_EXPERIENCE_PENALTY = altSettings.getInteger("DeathPenaltyC5RateExpPenalty", 1);
            ALT_DEATH_PENALTY_C5_KARMA_PENALTY = altSettings.getInteger("DeathPenaltyC5RateKarma", 1);
            ALT_PK_DEATH_RATE = altSettings.getDouble("AltPKDeathRate", 0);
            NON_OWNER_ITEM_PICKUP_DELAY = altSettings.getInteger("NonOwnerItemPickupDelay", 15) * 1000;

            ALT_NO_LASTHIT = altSettings.getBoolean("NoLasthitOnRaid", false);

            ALT_DONT_ALLOW_PETS_ON_SIEGE = altSettings.getBoolean("DontAllowPetsOnSiege", false);
            LIM_PATK = altSettings.getInteger("LimitPAtk", 15000);
            LIM_MATK = altSettings.getInteger("LimitMAtk", 10000);
            LIM_PDEF = altSettings.getInteger("LimitPDef", 10000);
            LIM_MDEF = altSettings.getInteger("LimitMDef", 10000);
            LIM_PATK_SPD = altSettings.getInteger("LimitPAtkSpd", 2200);
            LIM_MATK_SPD = altSettings.getInteger("LimitMAtkSpd", 2000);
            LIM_PCRIT = altSettings.getInteger("LimitPCritical", 500);
            LIM_MCRIT = altSettings.getInteger("LimitMCritical", 20);
            LIM_EVASION = altSettings.getInteger("LimitEvasion", 250);
            LIM_RUNSPEED = altSettings.getInteger("LimitRunSpeed", 250);
            BOW_ATK_SPEED = altSettings.getInteger("BowAtkSpeed", 1500);
            CROSS_BOW_ATK_SPEED = altSettings.getInteger("CrossBowAtkSpeed", 820);
            FESTIVAL_MIN_PARTY_SIZE = altSettings.getInteger("FestivalMinPartySize", 5);
            FESTIVAL_RATE_PRICE = altSettings.getDouble("FestivalRatePrice", 1.0);

            // rift settings
            RIFT_MIN_PARTY_SIZE = altSettings.getInteger("RiftMinPartySize", 5);
            RIFT_MAX_JUMPS = altSettings.getInteger("MaxRiftJumps", 4);
            RIFT_AUTO_JUMPS_TIME = altSettings.getInteger("AutoJumpsDelay", 8);
            RIFT_AUTO_JUMPS_TIME_RAND = altSettings.getInteger("AutoJumpsDelayRandom", 120000);
            RIFT_SPAWN_DELAY = altSettings.getInteger("RiftSpawnDelay", 10000);
            // rift cost
            RIFT_ENTER_COST_RECRUIT = altSettings.getInteger("RecruitFC", 18);
            RIFT_ENTER_COST_SOLDIER = altSettings.getInteger("SoldierFC", 21);
            RIFT_ENTER_COST_OFFICER = altSettings.getInteger("OfficerFC", 24);
            RIFT_ENTER_COST_CAPTAIN = altSettings.getInteger("CaptainFC", 27);
            RIFT_ENTER_COST_COMMANDER = altSettings.getInteger("CommanderFC", 30);
            RIFT_ENTER_COST_HERO = altSettings.getInteger("HeroFC", 33);
            RIFT_BOSS_ROOM_TIME_MUTIPLY = altSettings.getDouble("BossRoomTimeMultiply", 1.5);

            ALLOW_FAKE_PLAYERS = altSettings.getBoolean("AllowFakePlayers", false);
            FAKE_PLAYERS_PERCENT = altSettings.getInteger("FakePlayersPercent", 100);

            PARTY_LEADER_ONLY_CAN_INVITE = altSettings.getBoolean("PartyLeaderOnlyCanInvite", true);
            ALLOW_TALK_WHILE_SITTING = altSettings.getBoolean("AllowTalkWhileSitting", true);
            ALLOW_NOBLE_TP_TO_ALL = altSettings.getBoolean("AllowNobleTPToAll", false);
            MAXLOAD_MODIFIER = altSettings.getDouble("MaxLoadModifier", 1);
            SHUTDOWN_MSG_TYPE = altSettings.getInteger("ShutdownMsgType", 3);
            ALT_MAX_ALLY_SIZE = altSettings.getInteger("AltMaxAllySize", 3);
            ALT_PARTY_DISTRIBUTION_RANGE = altSettings.getInteger("AltPartyDistributionRange", 2500);
            ALT_PARTY_MAX_LVL_DIFF = altSettings.getInteger("AltPartyMaxLvlDiff", 20);
            ALT_CH_ALLOW_1H_BUFFS = altSettings.getBoolean("AltChAllowHourBuff", false);
            ALT_CH_ALL_BUFFS = altSettings.getBoolean("AltChAllBuffs", false);
            ALT_BUFF_SUMMON = altSettings.getBoolean("BuffSummon", true);
            ALT_BUFF_MIN_LEVEL = altSettings.getInteger("BuffMinLevel", 6);
            ALT_BUFF_MAX_LEVEL = altSettings.getInteger("BuffMaxLevel", 75);
            ALT_IMPROVED_PETS_LIMITED_USE = altSettings.getBoolean("ImprovedPetsLimitedUse", false);
            ALT_ALLOW_RECHARG_KOOKABURRA = altSettings.getBoolean("AllowRechargKookaburra", false);
            AUGMENTATION_NG_SKILL_CHANCE = altSettings.getInteger("AugmentationNGSkillChance", 15);
            AUGMENTATION_NG_GLOW_CHANCE = altSettings.getInteger("AugmentationNGGlowChance", 0);
            AUGMENTATION_MID_SKILL_CHANCE = altSettings.getInteger("AugmentationMidSkillChance", 30);
            AUGMENTATION_MID_GLOW_CHANCE = altSettings.getInteger("AugmentationMidGlowChance", 40);
            AUGMENTATION_HIGH_SKILL_CHANCE = altSettings.getInteger("AugmentationHighSkillChance", 45);
            AUGMENTATION_HIGH_GLOW_CHANCE = altSettings.getInteger("AugmentationHighGlowChance", 70);
            AUGMENTATION_TOP_SKILL_CHANCE = altSettings.getInteger("AugmentationTopSkillChance", 60);
            AUGMENTATION_TOP_GLOW_CHANCE = altSettings.getInteger("AugmentationTopGlowChance", 100);
            AUGMENTATION_BASESTAT_CHANCE = altSettings.getInteger("AugmentationBaseStatChance", 1);
            AUGMENTATION_ACC_SKILL_CHANCE = altSettings.getInteger("AugmentationAccessorySkillChance", 15);

            GATEKEEPER_FREE = altSettings.getInteger("GkFree", 40);
            CRUMA_GATEKEEPER_LVL = altSettings.getInteger("GkCruma", 56);
            USE_GK_NIGHT_PRICE = altSettings.getBoolean("NightGk", false);
            ALT_KAMALOKA_LIMITS = altSettings.getProperty("KamalokaLimit", "All");
            ALT_KAMALOKA_NIGHTMARES_PREMIUM_ONLY = altSettings.getBoolean("KamalokaNightmaresPremiumOnly", false);
            ALT_KAMALOKA_NIGHTMARE_REENTER = altSettings.getBoolean("SellReenterNightmaresTicket", true);
            ALT_KAMALOKA_ABYSS_REENTER = altSettings.getBoolean("SellReenterAbyssTicket", true);
            ALT_KAMALOKA_LAB_REENTER = altSettings.getBoolean("SellReenterLabyrinthTicket", true);

            /** Load mobs UD settings */
            ALT_ALLOW_MOBS_USE_UD = altSettings.getBoolean("AllowMobsUseUD", true);
            ALT_MOBS_UD_SKILL_ID = altSettings.getInteger("MobsUDSkillID", 5044);
            ALT_MOBS_UD_SKILL_LVL = altSettings.getInteger("MobsUDSkillLevel", 1);
            ALT_MOBS_UD_USE_CHANCE = altSettings.getInteger("MobsUDUseChance", 10);

            // Теоретический максимум клиента в финале Long.MAX_VALUE = 9223372036854775807L
            ALT_MAX_COUNT_ADENA = altSettings.getLong("AltMaxCountAdena", Long.MAX_VALUE);
            ALT_MAX_COUNT_OTHER = altSettings.getLong("AltMaxCountOther", Long.MAX_VALUE);

            ALT_DELETE_PET_IF_HUNGRY = altSettings.getBoolean("AltDeletePetIfHungry", true);

            ALT_ALLOW_DROP_COMMON = altSettings.getBoolean("AllowDropCommon", true);
            ALT_ALLOW_SELL_COMMON = altSettings.getBoolean("AllowSellCommon", true);
            ALT_SHOP_CONDITION_ENABLE = altSettings.getBoolean("AltConditionEnable", false);
            // ALT_ALLOW_SHADOW_WEAPONS = altSettings.getBoolean("AllowShadowWeapons", true);

            ALT_DISABLED_MULTISELL = new IntArrayList(altSettings.getIntArray("DisabledMultisells"), false);
            ALT_SHOP_PRICE_LIMITS = altSettings.getLongArray("ShopPriceLimits");
            ALT_SHOP_UNALLOWED_ITEMS = new IntArrayList(altSettings.getIntArray("ShopUnallowedItems"), false);

            ALT_SOUL_CRYSTAL_LEVEL_CHANCE = altSettings.getInteger("AltSoulCrystalLevelChance", 32);

            ALT_SEVEN_SIGNS_RESTRICT = altSettings.getBoolean("StrictSevenSigns", true);

            ALT_ENABLE_FIREWORKS = altSettings.getBoolean("AltEnableFireworks", true);

            ALT_LOAD_ELEMENTSTABLE = altSettings.getBoolean("AltLoadElementsTable", false);

            ALT_ALLOW_TAKE_WEAPON = altSettings.getBoolean("AltAllowTakeWeapon", false);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + ALT_SETTINGS_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String PVP_CONFIG_FILE = "./config/karma.ini";

    // ========================================================================
    public static void loadKarmaSettings() {
        try {
            final L2Properties pvpSettings = new L2Properties(PVP_CONFIG_FILE);

            /* KARMA SYSTEM */
            KARMA_MIN_KARMA = pvpSettings.getInteger("MinKarma", 240);
            KARMA_SP_DIVIDER = pvpSettings.getInteger("SPDivider", 7);
            KARMA_LOST_BASE = pvpSettings.getInteger("BaseKarmaLost", 0);

            KARMA_DROP_GM = pvpSettings.getBoolean("CanGMDropEquipment", false);
            KARMA_NEEDED_TO_DROP = pvpSettings.getBoolean("KarmaNeededToDrop", true);

            KARMA_NONDROPPABLE_ITEMS = pvpSettings.getProperty("ListOfNonDroppableItems", "57,1147,425,1146,461,10,2368,7,6,2370,2369,3500,3501,3502,4422,4423,4424,2375,6648,6649,6650,6842,6834,6835,6836,6837,6838,6839,6840,5575,7694,6841,8181");

            KARMA_DROP_ITEM_LIMIT = pvpSettings.getInteger("MaxItemsDroppable", 10);
            MIN_PK_TO_ITEMS_DROP = pvpSettings.getInteger("MinPKToDropItems", 5);

            KARMA_RANDOM_DROP_LOCATION_LIMIT = pvpSettings.getInteger("MaxDropThrowDistance", 70);

            KARMA_DROPCHANCE_MINIMUM = pvpSettings.getInteger("ChanceOfDropMinimum", 1);
            KARMA_DROPCHANCE_MULTIPLIER = pvpSettings.getInteger("ChanceOfDropMultiplier", 1);
            KARMA_DROPCHANCE_EQUIPMENT = pvpSettings.getInteger("ChanceOfDropEquipped", 20);
            KARMA_DROPCHANCE_EQUIPPED_WEAPON = pvpSettings.getInteger("ChanceOfDropEquippedWeapon", 25);

            KARMA_LIST_NONDROPPABLE_ITEMS = new GArray<Integer>();
            for (final String id : KARMA_NONDROPPABLE_ITEMS.split(","))
                KARMA_LIST_NONDROPPABLE_ITEMS.add(TypeFormat.parseInt(id));

            PVP_TIME = pvpSettings.getInteger("PvPTime", 120000);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + PVP_CONFIG_FILE + " File.");
        }
    }


    // ========================================================================
    public static final String GM_PERSONAL_ACCESS_FILE = "./config/GMAccess.xml";

    // ========================================================================
    public static void loadGMAccess() {
        gmlist.clear();
        loadGMAccess(new File(GM_PERSONAL_ACCESS_FILE));
        final File dir = new File("./config/GMAccess.d/");
        if (!dir.exists()) {
            _log.config("Dir " + dir.getAbsolutePath() + " not exists");
            return;
        }
        final File[] files = dir.listFiles();
        for (final File f : files)
            if (f.getName().endsWith(".xml"))
                loadGMAccess(f);
    }

    public static void loadGMAccess(final File file) {
        try {
            Field fld;
            // File file = new File(filename);
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);
            factory.setIgnoringComments(true);
            final Document doc = factory.newDocumentBuilder().parse(file);

            for (Node z = doc.getFirstChild(); z != null; z = z.getNextSibling())
                for (Node n = z.getFirstChild(); n != null; n = n.getNextSibling()) {
                    if (!n.getNodeName().equalsIgnoreCase("char"))
                        continue;

                    final PlayerAccess pa = new PlayerAccess();
                    for (Node d = n.getFirstChild(); d != null; d = d.getNextSibling()) {
                        final Class<? extends PlayerAccess> cls = pa.getClass();
                        final String node = d.getNodeName();

                        if (node.equalsIgnoreCase("#text"))
                            continue;
                        try {
                            fld = cls.getField(node);
                        } catch (final NoSuchFieldException e) {
                            _log.info("Not found desclarate ACCESS name: " + node + " in XML Player access Object");
                            continue;
                        }

                        if (fld.getType().getName().equalsIgnoreCase("boolean"))
                            fld.setBoolean(pa, TypeFormat.parseBoolean(d.getAttributes().getNamedItem("set").getNodeValue()));
                        else if (fld.getType().getName().equalsIgnoreCase("int"))
                            fld.setInt(pa, TypeFormat.parseInt(d.getAttributes().getNamedItem("set").getNodeValue()));
                        else if (fld.getType().getName().equalsIgnoreCase("javolution.util.FastTable")) {
                            final GArray<Integer> temp = new GArray<Integer>();
                            for (final int id : ArrayUtil.toIntArray(d.getAttributes().getNamedItem("set").getNodeValue(), ";"))
                                temp.add(id);
                            fld.set(pa, temp);
                        }
                    }
                    gmlist.put(pa.PlayerID, pa);
                }
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + GM_PERSONAL_ACCESS_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String L2SYSTEM_FILE = "./config/l2system.ini";

    public static boolean DONTLOADSPAWN;
    public static boolean DONTLOADQUEST;

    public static boolean SERVER_LIST_BRACKET;
    public static boolean SERVER_LIST_CLOCK;
    public static boolean SERVER_GMONLY;

    /**
     * Thread pools size
     */
    public static int THREAD_SP_GENERAL;
    public static int THREAD_EP_GENERAL;
    public static int THREAD_P_MOVE;
    public static int IG_PACKET_THREAD_CORE_SIZE;

    public static int EFFECT_TASK_MANAGER_COUNT;
    public static int AI_MAX_THREAD;

    public static int INTEREST_MAX_THREAD;
    public static boolean INTEREST_ALT;

    public static int DEADLOCK_CHECK_PERIOD;
    public static int THREAD_NUMBER_CHECK_PERIOD;
    public static boolean DEADLOCKFOUND_RESTART;
    public static int MAX_LENGTH_HTML;

    public static boolean SUMMON_ATTRIBUTE_TRANSFER;

    public static boolean PING_ENABLED;
    public static int PING_INTERVAL;
    public static int PING_IGNORED_REQEST_LIMIT;

    public static boolean CHARTS_ENABLED;
    public static long CHARTS_UPDATE_TIME;

    public static boolean ACTIVATE_POSITION_RECORDER;

    public static boolean ALLOW_REPAIR_TABLES;
    public static boolean ALLOW_OPTIMIZE_TABLES;

    // ========================================================================
    public static void loadl2sSettings() {
        // Опции для разработчиков
        try {
            final L2Properties l2sSettings = new L2Properties(L2SYSTEM_FILE);

            EVERYBODY_HAS_ADMIN_RIGHTS = l2sSettings.getBoolean("EverybodyHasAdminRights", false);

            SERVER_LIST_BRACKET = l2sSettings.getBoolean("ServerListBrackets", false);
            SERVER_LIST_CLOCK = l2sSettings.getBoolean("ServerListClock", false);
            SERVER_GMONLY = l2sSettings.getBoolean("ServerGMOnly", false);

            THREAD_SP_GENERAL = l2sSettings.getInteger("STPGeneralSize", 16);
            THREAD_EP_GENERAL = l2sSettings.getInteger("ETPGeneralSize", 8);
            THREAD_P_MOVE = l2sSettings.getInteger("STPMoveSize", 16);
            AI_MAX_THREAD = l2sSettings.getInteger("STPAiSize", 10);

            IG_PACKET_THREAD_CORE_SIZE = l2sSettings.getInteger("GameClientPacketCoreSize", 6);
            EFFECT_TASK_MANAGER_COUNT = l2sSettings.getInteger("EffectTaskManagers", 2);

            INTEREST_MAX_THREAD = l2sSettings.getInteger("InterestMaxThread", 20);
            INTEREST_ALT = l2sSettings.getBoolean("InterestAlt", false);

            DONTLOADSPAWN = l2sSettings.getBoolean("StartWhisoutSpawn", false);
            DONTLOADQUEST = l2sSettings.getBoolean("StartWhisoutQuest", false);

            SHOW_OFFLINE_PLAYERS = l2sSettings.getBoolean("ShowOfflinePlayers", true);

            DEADLOCK_CHECK_PERIOD = l2sSettings.getInteger("DeadlockCheckPeriod", 10000);
            DEADLOCKFOUND_RESTART = l2sSettings.getBoolean("DeadLockFoundRestart", true);

            MAX_LENGTH_HTML = l2sSettings.getInteger("MaxLengthHtml", 8192);
            SUMMON_ATTRIBUTE_TRANSFER = l2sSettings.getBoolean("SummonAttributeTransfer", true);

            PING_INTERVAL = l2sSettings.getInteger("PingInterval", 30000);
            PING_ENABLED = l2sSettings.getBoolean("PingEnabled", false);
            PING_IGNORED_REQEST_LIMIT = l2sSettings.getInteger("PingIgnoredRequestLimit", 2);

            CHARTS_ENABLED = l2sSettings.getBoolean("UseCharts", true);
            CHARTS_UPDATE_TIME = l2sSettings.getLong("ChartsUpdateDelay", 30) * 1000L;

            ACTIVATE_POSITION_RECORDER = l2sSettings.getBoolean("ActivatePositionRecorder", false);

            ALLOW_REPAIR_TABLES = l2sSettings.getBoolean("RepairTables", true);
            ALLOW_OPTIMIZE_TABLES = l2sSettings.getBoolean("OptimizeTables", true);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + L2SYSTEM_FILE + " File.");
        }
    }

    // ========================================================================
    public static final String DEBUG_FILE = "./config/debug.ini";
    // ========================================================================
    public static boolean DEBUG;
    public static boolean DEBUG_PACKETS;
    public static boolean DEBUG_OTHER;
    public static boolean DEBUG_HEAVY_TERR;
    public static boolean SERVER_LIST_TESTSERVER;
    public static boolean DEBUG_OLYMPIAD;

    public static void loadbedSettings() {
        // Опции для Дебага
        try {
            final L2Properties debSettings = new L2Properties(DEBUG_FILE);
            DEBUG = debSettings.getBoolean("Debug", false);
            DEBUG_PACKETS = debSettings.getBoolean("DebugPackets", false);
            DEBUG_OTHER = debSettings.getBoolean("DebugOther", false);
            DEBUG_HEAVY_TERR = debSettings.getBoolean("DebugHeavyTerr", false);
            SERVER_LIST_TESTSERVER = debSettings.getBoolean("TestServer", false);
            DEBUG_OLYMPIAD = debSettings.getBoolean("OlympiadDebugMode", false);

        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + DEBUG_FILE + " File.");
        }
    }

    // ========================================================================
    public static final String RESIDENCE_FILE = "./config/residence.ini";

    // ========================================================================
    public static void loadResSettings() {
        try {
            final L2Properties residenceSettings = new L2Properties(RESIDENCE_FILE);

            CH_BID_GRADE1_MINCLANLEVEL = residenceSettings.getInteger("ClanHallBid_Grade1_MinClanLevel", 2);
            CH_BID_GRADE1_MINCLANMEMBERS = residenceSettings.getInteger("ClanHallBid_Grade1_MinClanMembers", 1);
            CH_BID_GRADE1_MINCLANMEMBERSLEVEL = residenceSettings.getInteger("ClanHallBid_Grade1_MinClanMembersAvgLevel", 1);
            CH_BID_GRADE2_MINCLANLEVEL = residenceSettings.getInteger("ClanHallBid_Grade2_MinClanLevel", 2);
            CH_BID_GRADE2_MINCLANMEMBERS = residenceSettings.getInteger("ClanHallBid_Grade2_MinClanMembers", 1);
            CH_BID_GRADE2_MINCLANMEMBERSLEVEL = residenceSettings.getInteger("ClanHallBid_Grade2_MinClanMembersAvgLevel", 1);
            CH_BID_GRADE3_MINCLANLEVEL = residenceSettings.getInteger("ClanHallBid_Grade3_MinClanLevel", 2);
            CH_BID_GRADE3_MINCLANMEMBERS = residenceSettings.getInteger("ClanHallBid_Grade3_MinClanMembers", 1);
            CH_BID_GRADE3_MINCLANMEMBERSLEVEL = residenceSettings.getInteger("ClanHallBid_Grade3_MinClanMembersAvgLevel", 1);
            RESIDENCE_LEASE_MULTIPLIER = residenceSettings.getDouble("ResidenceLeaseMultiplier", 1);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + RESIDENCE_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String FAME_FILE = "./config/fame.ini";

    // ========================================================================
    public static void loadFameSettings() {
        try {
            final L2Properties fameSettings = new L2Properties(FAME_FILE);
            MAX_PERSONAL_FAME_POINTS = fameSettings.getInteger("MaxPersonalFamePoints", 65535);
            FORTRESS_ZONE_FAME_TASK_FREQUENCY = fameSettings.getInteger("FortressZoneFameTaskFrequency", 300000);
            FORTRESS_ZONE_FAME_AQUIRE_POINTS = fameSettings.getInteger("FortressZoneFameAquirePoints", 31);
            CASTLE_ZONE_FAME_TASK_FREQUENCY = fameSettings.getInteger("CastleZoneFameTaskFrequency", 300000);
            CASTLE_ZONE_FAME_AQUIRE_POINTS = fameSettings.getInteger("CastleZoneFameAquirePoints", 125);
            TerritorySiege_ZONE_FAME_AQUIRE_POINTS = fameSettings.getInteger("TerritorySiegeZoneFameAquirePoints", 125);
            TerritoryFortress_ZONE_FAME_AQUIRE_POINTS = fameSettings.getInteger("TerritoryFortressSiegeZoneFameAquirePoints", 31);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + FAME_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String SIEGE_CONFIGURATION_FILE = "./config/siege.ini";

    // ========================================================================
    public static void loadSiegeSettings() {
        try {
            final L2Properties siegeSettings = new L2Properties(SIEGE_CONFIGURATION_FILE);

            SIEGE_OPERATE_DOORS = siegeSettings.getBoolean("OperateDoors", true);
            SIEGE_OPERATE_DOORS_LORD_ONLY = siegeSettings.getBoolean("OperateDoorsLordOnly", true);
            SIEGE_CASTLE_CONTROL_TOWER_LOSS_PENALTY = siegeSettings.getInteger("ControlTowerLossPenalty", 20000);
            CASTLE_SIEGE_DAY = siegeSettings.getInteger("CastleSiegeDay", 14);
            TERRITORY_SIEGE_DAY = siegeSettings.getInteger("TerritorySiegeDay", 14);
            CLANHALL_SIEGE_DAY = siegeSettings.getInteger("ClanHallSiegeDay", 14);
            SIEGE_CASTLE_CLAN_MIN_LEVEL = siegeSettings.getInteger("CastleSiegeClanMinLevel", 5);
            SIEGE_FORT_CLAN_MIN_LEVEL = siegeSettings.getInteger("FortSiegeClanMinLevel", 4);
            SIEGE_CLANHALL_CLAN_MIN_LEVEL = siegeSettings.getInteger("ClanHallSiegeClanMinLevel", 4);

            SIEGE_CASTLE_LENGTH = siegeSettings.getInteger("CastleSiegeLength", 120);
            SIEGE_FORT_LENGTH = siegeSettings.getInteger("FortSiegeLength", 60);
            SIEGE_CLANHALL_LENGTH = siegeSettings.getInteger("ClanHallSiegeLength", 60);

            SIEGE_CASTLE_DEFENDER_RESPAWN = siegeSettings.getInteger("CastleDefenderRespawn", 20000);
            SIEGE_FORT_DEFENDER_RESPAWN = siegeSettings.getInteger("FortDefenderRespawn", 20000);
            SIEGE_CLANHALL_DEFENDER_RESPAWN = siegeSettings.getInteger("ClanHallDefenderRespawn", 20000);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + SIEGE_CASTLE_CONFIGURATION_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String HEXID_FILE = "./config/hexid/hexid.txt";

    // ========================================================================
    public static void loadHexidSettings() {
        try {
            final L2Properties Settings = new L2Properties(HEXID_FILE);
            HEX_ID = new BigInteger(Settings.getProperty("HexID"), 16).toByteArray();
        } catch (final Exception e) {
        }
    }

    // *******************************************************************************************

    // ========================================================================

    public static final String OBSCENE_CONFIG_FILE = "./config/chat/obscene.txt";
    public static final String INDECENT_CONFIG_FILE = "./config/chat/indecent.txt";
    public static final String SHOUT_CONFIG_FILE = "./config/chat/shout.txt";

    // ========================================================================
    public static void abuseLoad() {
        GArray<Pattern> tmp = new GArray<Pattern>();
        LineNumberReader lnr = null;
        try {
            lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(Server.L2sHomeDir + OBSCENE_CONFIG_FILE), "UTF-8"));

            String line;
            while ((line = lnr.readLine()) != null) {
                final StringTokenizer st = new StringTokenizer(line, "\n\r");
                if (st.hasMoreTokens())
                    tmp.add(Pattern.compile(".*" + st.nextToken() + ".*", Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE));
            }

            OBSCENE_LIST = tmp.toArray(new Pattern[tmp.size()]);
            tmp.clear();
            _log.info("Obscene: Loaded " + OBSCENE_LIST.length + " abuse words.");
        } catch (final IOException e1) {
            _log.warning("Error reading abuse: " + e1);
        } finally {
            try {
                if (lnr != null)
                    lnr.close();
            } catch (final Exception e2) {
            }
        }

        tmp = new GArray<Pattern>();
        lnr = null;
        try {
            String line;
            lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(Server.L2sHomeDir + INDECENT_CONFIG_FILE), "UTF-8"));
            while ((line = lnr.readLine()) != null) {
                final StringTokenizer st = new StringTokenizer(line, "\n\r");
                if (st.hasMoreTokens())
                    tmp.add(Pattern.compile(".*" + st.nextToken() + ".*", Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE));
            }

            INDECENT_LIST = tmp.toArray(new Pattern[tmp.size()]);
            tmp.clear();
            _log.info("Indecent: Loaded " + INDECENT_LIST.length + " abuse words.");
        } catch (final IOException e1) {
            _log.warning("Error reading abuse: " + e1);
        } finally {
            try {
                if (lnr != null)
                    lnr.close();
            } catch (final Exception e2) {
            }
        }

        tmp = new GArray<Pattern>();
        lnr = null;
        try {
            String line;
            lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(Server.L2sHomeDir + SHOUT_CONFIG_FILE), "UTF-8"));
            while ((line = lnr.readLine()) != null) {
                final StringTokenizer st = new StringTokenizer(line, "\n\r");
                if (st.hasMoreTokens())
                    tmp.add(Pattern.compile(".*" + st.nextToken() + ".*", Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE));
            }

            SHOUT_LIST = tmp.toArray(new Pattern[tmp.size()]);
            tmp.clear();
            _log.info("Shout: Loaded " + SHOUT_LIST.length + " abuse words.");

        } catch (final IOException e1) {
            _log.warning("Error reading abuse: " + e1);
        } finally {
            try {
                if (lnr != null)
                    lnr.close();
            } catch (final Exception e2) {
            }
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String ADV_IP_FILE = "./config/advipsystem.ini";

    // ========================================================================
    public static void ipsLoad() {
        try {
            final L2Properties ipsSettings = new L2Properties(ADV_IP_FILE);

            for (int i = 0; i < ipsSettings.size() / 2; i++) {
                final String NetMask = ipsSettings.getProperty("NetMask" + (i + 1));
                final String ip = ipsSettings.getProperty("IPAdress" + (i + 1));
                for (final String mask : NetMask.split(",")) {
                    final AdvIP advip = new AdvIP();
                    advip.ipadress = ip;
                    advip.ipmask = mask.split("/")[0];
                    advip.bitmask = mask.split("/")[1];
                    GAMEIPS.add(advip);
                }
            }
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + ADV_IP_FILE + " File.");
        }
    }

    // *******************************************************************************************
    public static int FOLLOW_RANGE;

    /**
     * Алтернативный метода работы AI
     */
    public static boolean ALT_AI_ENGINE;
    public static int ALT_AI_ENGINE_THREAD_SLEEP;

    /**
     * AI
     */
    public static boolean RND_WALK;
    public static int RND_WALK_RATE;
    public static int RND_ANIMATION_RATE;

    public static int AGGRO_CHECK_INTERVAL;

    /**
     * Maximum range mobs can randomly go from spawn point
     */
    public static int MAX_DRIFT_RANGE;

    /**
     * Maximum range mobs can pursue agressor from spawn point
     */
    public static int MAX_PURSUE_RANGE;
    public static int MAX_PURSUE_RANGE_RAID;

    public static boolean ALT_AI_KELTIRS;

    public static int AI_TASK_DELAY;
    public static int AI_TASK_ACTIVE_DELAY;
    public static boolean BLOCK_ACTIVE_TASKS;
    public static boolean SAY_CASTING_SKILL_NAME;

    /**
     * random animation interval
     */
    public static int MIN_NPC_ANIMATION;
    public static int MAX_NPC_ANIMATION;
    public static int MIN_MONSTER_ANIMATION;
    public static int MAX_MONSTER_ANIMATION;

    // ========================================================================
    public static final String AI_CONFIG_FILE = "./config/ai.ini";

    // ========================================================================
    public static void loadAISettings() {
        try {
            final L2Properties aiSettings = new L2Properties(AI_CONFIG_FILE);
            ALT_AI_ENGINE = aiSettings.getBoolean("AltAiEngine", false);
            ALT_AI_ENGINE_THREAD_SLEEP = aiSettings.getInteger("AltAiEngineThreadSleep", 100);

            AI_TASK_DELAY = aiSettings.getInteger("AiTaskDelay", 1000);

            AI_TASK_ACTIVE_DELAY = aiSettings.getInteger("AiTaskActiveDelay", 10000);
            BLOCK_ACTIVE_TASKS = aiSettings.getBoolean("BlockActiveTasks", false);
            SAY_CASTING_SKILL_NAME = aiSettings.getBoolean("SayCastingSkillName", false);

            RND_WALK = aiSettings.getBoolean("RndWalk", true);
            RND_WALK_RATE = aiSettings.getInteger("RndWalkRate", 2);
            RND_ANIMATION_RATE = aiSettings.getInteger("RndAnimationRate", 3);

            AGGRO_CHECK_INTERVAL = aiSettings.getInteger("AggroCheckInterval", 250);

            MAX_DRIFT_RANGE = aiSettings.getInteger("MaxDriftRange", 300);
            MAX_PURSUE_RANGE = aiSettings.getInteger("MaxPursueRange", 2000);
            MAX_PURSUE_RANGE_RAID = aiSettings.getInteger("MaxPursueRangeRaid", 5000);

            ALT_AI_KELTIRS = aiSettings.getBoolean("AltAiKeltirs", false);
            FOLLOW_RANGE = aiSettings.getInteger("FollowRange", 100);

            MIN_NPC_ANIMATION = aiSettings.getInteger("MinNPCAnimation", 10);
            MAX_NPC_ANIMATION = aiSettings.getInteger("MaxNPCAnimation", 20);
            MIN_MONSTER_ANIMATION = aiSettings.getInteger("MinMonsterAnimation", 5);
            MAX_MONSTER_ANIMATION = aiSettings.getInteger("MaxMonsterAnimation", 20);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + AI_CONFIG_FILE + " File.");
        }
    }

    public static class EventInterval {
        public final int hour;
        public final int minute;
        public final int category;

        public EventInterval(final int h, final int m, final int category) {
            hour = h;
            minute = m;
            this.category = category;
        }

        @Override
        public final String toString() {
            return "interval: " + hour + ":" + minute + ", g: " + category;
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String EVENTS = "./config/events.ini";

    public static boolean ENABLE_BLOCK_CHECKER_EVENT;
    public static int MIN_BLOCK_CHECKER_TEAM_MEMBERS;
    public static boolean BLOCK_CHECKER_EVENT_FAIR_PLAY;

    /**
     * Saving Santa
     **/
    public static float EVENT_RABBITS_TO_RICHES_REWARD_RATE;
    public static float EVENT_TREASURE_SACK_CHANCE;

    /**
     * Rabbits To Riches
     **/
    public static long EVENT_SAVING_SNOWMAN_LOTERY_PRICE;
    public static int EVENT_SAVING_SNOWMAN_REWARDER_CHANCE;

    /**
     * Master Of Enchanting
     **/
    public static float ENCHANT_MASTER_DROP_CHANCE;
    public static boolean ENCHANT_MASTER_USE_RATES;
    public static long ENCHANT_MASTER_STAFF_PRICE;
    public static long ENCHANT_MASTER_24SCROLL_PRICE;
    public static long ENCHANT_MASTER_1SCROLL_PRICE;
    public static int ENCHANT_MASTER_PRICE_ID;

    // ========================================================================
    public static void loadEventsSettings() {
        try {
            final L2Properties eventSettings = new L2Properties(EVENTS);

            EVENT_CofferOfShadowsPriceRate = eventSettings.getInteger("CofferOfShadowsPriceRate", 1);
            EVENT_CofferOfShadowsRewardRate = eventSettings.getDouble("CofferOfShadowsRewardRate", 1);

            EVENT_LASTHERO_ITEM_ID = eventSettings.getInteger("LastHeroItemId", 57);
            EVENT_LASTHERO_ITEM_COUNT = eventSettings.getInteger("LastHeroItemCount", 5000);
            EVENT_LASTHERO_TIME_TO_START = eventSettings.getInteger("LastHeroTimeToStart", 5);
            EVENT_LASTHERO_TIME_FOR_FIGHT = eventSettings.getInteger("LastHeroEventTime", 5);
            EVENT_LASTHERO_RATE = eventSettings.getBoolean("LastHeroRate", true);
            EVENT_LASTHERO_ALLOW_CLAN_SKILL = eventSettings.getBoolean("LastHeroAllowClanSkill", false);
            EVENT_LASTHERO_ALLOW_HERO_SKILL = eventSettings.getBoolean("LastHeroAllowHeroSkill", false);
            EVENT_LASTHERO_ALLOW_BUFFS = eventSettings.getBoolean("LastHeroAllowBuffs", false);
            EVENT_LASTHERO_ALLOW_SUMMONS = eventSettings.getBoolean("LastHeroAllowSummons", false);
            EVENT_LASTHERO_GIVE_HERO = eventSettings.getBoolean("LastHeroGiveHero", false);
            EVENT_LASTHERO_PARTY_DISABLE = eventSettings.getBoolean("LastHeroPartyDisable", true);

            EVENT_LASTHERO_ITEM_RESTRICTION = new ItemUseRestrictionByType(eventSettings.getProperty("LastHeroItemRestriction", null));
            EVENT_LASTHERO_SKILL_RESTRICTION = new SkillUseRestriction(eventSettings.getIntArray("LastHeroSkillRestriction"));

            EVENT_DEATHMATCH_ITEM_ID = eventSettings.getInteger("DeathMatchItemId", 57);
            EVENT_DEATHMATCH_ITEM_COUNT = eventSettings.getInteger("DeathMatchItemCount", 5000);
            EVENT_DEATHMATCH_REWARD_ITEM_ID = eventSettings.getInteger("DeathMatchRewardItemId", 57);
            EVENT_DEATHMATCH_REWARD_ITEM_COUNT = eventSettings.getInteger("DeathMatchRewardItemCount", 150000);
            EVENT_DEATHMATCH_TIME_TO_START = eventSettings.getInteger("DeathMatchTimeToStart", 5);
            EVENT_DEATHMATCH_TIME_FOR_FIGHT = eventSettings.getInteger("DeathMatchEventTime", 5);
            EVENT_DEATHMATCH_MIN_PLAYER_COUNT = eventSettings.getInteger("DeathMatchPlayerCount", 2);
            EVENT_DEATHMATCH_REVIVE_DELAY = eventSettings.getInteger("DeathMatchReviveDelay", 10);
            EVENT_DEATHMATCH_RATE = eventSettings.getBoolean("DeathMatchRate", true);
            EVENT_DEATHMATCH_ALLOW_CLAN_SKILL = eventSettings.getBoolean("DeathMatchAllowClanSkill", false);
            EVENT_DEATHMATCH_ALLOW_HERO_SKILL = eventSettings.getBoolean("DeathMatchAllowHeroSkill", false);
            EVENT_DEATHMATCH_ALLOW_BUFFS = eventSettings.getBoolean("DeathMatchAllowBuffs", false);
            EVENT_DEATHMATCH_ALLOW_SUMMONS = eventSettings.getBoolean("DeathMatchAllowSummons", false);
            EVENT_DEATHMATCH_GIVE_HERO = eventSettings.getBoolean("DeathMatchGiveHero", false);
            EVENT_DEATHMATCH_PARTY_DISABLE = eventSettings.getBoolean("DeathMatchPartyDisable", true);

            EVENT_DEATHMATCH_ITEM_RESTRICTION = new ItemUseRestrictionByType(eventSettings.getProperty("DeathMatchItemRestriction", null));
            EVENT_DEATHMATCH_SKILL_RESTRICTION = new SkillUseRestriction(eventSettings.getIntArray("DeathMatchSkillRestriction"));

            EVENT_TVT_ITEM_ID = eventSettings.getInteger("TvTItemId", 4037);
            EVENT_TVT_ITEM_COUNT = eventSettings.getInteger("TvTItemCount", 1);
            EVENT_TVT_MAX_LENGTH_TEAM = eventSettings.getInteger("TvTMaxLengthTeam", 12);
            EVENT_TVT_TIME_TO_START = eventSettings.getInteger("TvTTimeToStart", 3);
            EVENT_TVT_TIME_FOR_FIGHT = eventSettings.getInteger("TvTEventTime", 5);
            EVENT_TVT_REVIVE_DELAY = eventSettings.getInteger("TvTReviveDelay", 10);
            EVENT_TVT_RATE = eventSettings.getBoolean("TvTRate", true);
            EVENT_TVT_ALLOW_CLAN_SKILL = eventSettings.getBoolean("TvTAllowClanSkill", false);
            EVENT_TVT_ALLOW_HERO_SKILL = eventSettings.getBoolean("TvTAllowHeroSkill", false);
            EVENT_TVT_ALLOW_BUFFS = eventSettings.getBoolean("TvTAllowBuffs", false);
            EVENT_TVT_ALLOW_SUMMONS = eventSettings.getBoolean("TvTAllowSummons", false);

            EVENT_TVT_ITEM_RESTRICTION = new ItemUseRestrictionByType(eventSettings.getProperty("TvTItemRestriction", null));
            EVENT_TVT_SKILL_RESTRICTION = new SkillUseRestriction(eventSettings.getIntArray("TvTSkillRestriction"));

            EVENT_CFT_ITEM_ID = eventSettings.getInteger("CTFItemId", 4037);
            EVENT_CFT_ITEM_COUNT = eventSettings.getInteger("CTFItemCount", 1);
            EVENT_CTF_MAX_LENGTH_TEAM = eventSettings.getInteger("CTFMaxLengthTeam", 12);
            EVENT_CFT_TIME_TO_START = eventSettings.getInteger("CTFTimeToStart", 3);
            EVENT_CTF_TIME_FOR_FIGHT = eventSettings.getInteger("CTFEventTime", 5);
            EVENT_CTF_REVIVE_DELAY = eventSettings.getInteger("CTFReviveDelay", 10);
            EVENT_CFT_RATE = eventSettings.getBoolean("CTFRate", true);
            EVENT_CFT_ALLOW_CLAN_SKILL = eventSettings.getBoolean("CTFAllowClanSkill", false);
            EVENT_CFT_ALLOW_HERO_SKILL = eventSettings.getBoolean("CTFAllowHeroSkill", false);
            EVENT_CFT_ALLOW_BUFFS = eventSettings.getBoolean("CTFAllowBuffs", false);
            EVENT_CFT_ALLOW_SUMMONS = eventSettings.getBoolean("CTFAllowSummons", false);

            EVENT_CFT_ITEM_RESTRICTION = new ItemUseRestrictionByType(eventSettings.getProperty("CTFItemRestriction", null));
            EVENT_CFT_SKILL_RESTRICTION = new SkillUseRestriction(eventSettings.getIntArray("CTFSkillRestriction"));

            EVENT_LASTHERO_CHECK_HWID = eventSettings.getBoolean("LastHeroCheckHardwareID", false);
            EVENT_DEATHMATCH_CHECK_HWID = eventSettings.getBoolean("DeathMatchCheckHardwareID", false);
            EVENT_TVT_CHECK_HWID = eventSettings.getBoolean("TvTCheckHardwareID", false);
            EVENT_CTF_CHECK_HWID = eventSettings.getBoolean("CTFCheckHardwareID", false);
            EVENT_CTF_BUFFS_FIGHTER = eventSettings.getProperty("CTFEventFighterBuffs", null);
            EVENT_CTF_BUFFS_MAGE = eventSettings.getProperty("CTFEventMageBuffs", null);
            EVENT_LASTHERO_DISPEL_TRANSFORMATION = eventSettings.getBoolean("LastHeroDispelTransformation", false);
            EVENT_DEATHMATCH_DISPEL_TRANSFORMATION = eventSettings.getBoolean("DeathMatchDispelTransformation", false);
            EVENT_TVT_DISPEL_TRANSFORMATION = eventSettings.getBoolean("TvTDispelTransformation", false);
            EVENT_CFT_DISPEL_TRANSFORMATION = eventSettings.getBoolean("CTFDispelTransformation", false);

            EVENT_DEATHMATCH_BUFFS_FIGHTER = eventSettings.getProperty("DeathMatchEventFighterBuffs", null);
            EVENT_DEATHMATCH_BUFFS_MAGE = eventSettings.getProperty("DeathMatchEventMageBuffs", null);
            EVENT_DEATHMATCH_BUFFS_ONSTART = eventSettings.getBoolean("DeathMatchEventBuffOnStart", false);

            EVENT_TVT_BUFFS_FIGHTER = eventSettings.getProperty("TvTEventFighterBuffs", null);
            EVENT_TVT_BUFFS_MAGE = eventSettings.getProperty("TvTEventMageBuffs", null);
            EVENT_TVT_BUFFS_ONSTART = eventSettings.getBoolean("TvTEventBuffOnStart", false);

            EVENT_LASTHERO_INTERVAL = new GArray<EventInterval>();
            StringTokenizer st = new StringTokenizer(eventSettings.getProperty("LastHeroEventInterval", ""), "[]");
            while (st.hasMoreTokens()) {
                final String interval = st.nextToken();
                final String[] t = interval.split(";");
                final String[] t2 = t[0].split(":");
                final int category = TypeFormat.parseInt(t[1]);
                final int h = TypeFormat.parseInt(t2[0]);
                final int m = TypeFormat.parseInt(t2[1]);
                EVENT_LASTHERO_INTERVAL.add(new EventInterval(h, m, category));
            }

            EVENT_TVT_INTERVAL = new GArray<EventInterval>();
            st = new StringTokenizer(eventSettings.getProperty("TvTEventInterval", ""), "[]");
            while (st.hasMoreTokens()) {
                final String interval = st.nextToken();
                final String[] t = interval.split(";");
                final String[] t2 = t[0].split(":");
                final int category = TypeFormat.parseInt(t[1]);
                final int h = TypeFormat.parseInt(t2[0]);
                final int m = TypeFormat.parseInt(t2[1]);
                EVENT_TVT_INTERVAL.add(new EventInterval(h, m, category));
            }

            EVENT_CFT_INTERVAL = new GArray<EventInterval>();
            st = new StringTokenizer(eventSettings.getProperty("CTFEventInterval", ""), "[]");
            while (st.hasMoreTokens()) {
                final String interval = st.nextToken();
                final String[] t = interval.split(";");
                final String[] t2 = t[0].split(":");
                final int category = TypeFormat.parseInt(t[1]);
                final int h = TypeFormat.parseInt(t2[0]);
                final int m = TypeFormat.parseInt(t2[1]);
                EVENT_CFT_INTERVAL.add(new EventInterval(h, m, category));
            }

            EVENT_DEATHMATCH_INTERVAL = new GArray<EventInterval>();
            st = new StringTokenizer(eventSettings.getProperty("DeathMatchEventInterval", ""), "[]");
            while (st.hasMoreTokens()) {
                final String interval = st.nextToken();
                final String[] t = interval.split(";");
                final String[] t2 = t[0].split(":");
                final int category = TypeFormat.parseInt(t[1]);
                final int h = TypeFormat.parseInt(t2[0]);
                final int m = TypeFormat.parseInt(t2[1]);
                EVENT_DEATHMATCH_INTERVAL.add(new EventInterval(h, m, category));
            }

            TFH_POLLEN_CHANCE = eventSettings.getInteger("TFH_POLLEN_CHANCE", 5);

            GLIT_MEDAL_CHANCE = eventSettings.getDouble("MedalChance", 10);
            GLIT_GLITTMEDAL_CHANCE = eventSettings.getDouble("GlitteringMedalChance", 5);
            EXCEPTION_MONSTER_LIST = new IntArrayList(eventSettings.getIntArray("ExceptionMonsterList"), false);

            EVENT_message1 = eventSettings.getProperty("Message1", "Автоанонс1");
            EVENT_message2 = eventSettings.getProperty("Message2", "Автоанонс2");
            EVENT_message3 = eventSettings.getProperty("Message3", "Автоанонс3");
            EVENT_time1 = eventSettings.getProperty("Time1", "120000");
            EVENT_time2 = eventSettings.getProperty("Time2", "60000");
            EVENT_time3 = eventSettings.getProperty("Time3", "30000");

            // hearts
            EVENT_CHANGE_OF_HEART_CHANCE = eventSettings.getDouble("HeartsChance", 10);
            EVENT_TRICK_OF_TRANS_CHANCE = eventSettings.getDouble("TrickorTransmutationDropChance", 10);

            ENABLE_BLOCK_CHECKER_EVENT = eventSettings.getBoolean("EnableBlockCheckerEvent", false);
            MIN_BLOCK_CHECKER_TEAM_MEMBERS = eventSettings.getInteger("BlockCheckerMinTeamMembers", 2);
            if (MIN_BLOCK_CHECKER_TEAM_MEMBERS < 1)
                MIN_BLOCK_CHECKER_TEAM_MEMBERS = 1;
            else if (MIN_BLOCK_CHECKER_TEAM_MEMBERS > 6)
                MIN_BLOCK_CHECKER_TEAM_MEMBERS = 6;
            BLOCK_CHECKER_EVENT_FAIR_PLAY = eventSettings.getBoolean("BlockCheckerFairPlay", false);

            EVENT_RABBITS_TO_RICHES_REWARD_RATE = eventSettings.getFloat("RabbitsToRichesRewardRate", 1f);
            EVENT_TREASURE_SACK_CHANCE = eventSettings.getFloat("TreasureSackChance", 10f);

            EVENT_SAVING_SNOWMAN_LOTERY_PRICE = eventSettings.getLong("SavingSnowmanLoteryPrice", 50000);
            EVENT_SAVING_SNOWMAN_REWARDER_CHANCE = eventSettings.getInteger("SavingSnowmanRewarderChance", 2);

            ENCHANT_MASTER_DROP_CHANCE = eventSettings.getFloat("EnchMasterDropChance", 1f);
            ENCHANT_MASTER_USE_RATES = eventSettings.getBoolean("EnchMasterUseAdenaRates", true);
            ENCHANT_MASTER_STAFF_PRICE = eventSettings.getLong("EnchMasterStaffPrice", 1000);
            ENCHANT_MASTER_STAFF_PRICE = (long) (ENCHANT_MASTER_USE_RATES ? ENCHANT_MASTER_STAFF_PRICE * RATE_DROP_ADENA : ENCHANT_MASTER_STAFF_PRICE);
            ENCHANT_MASTER_24SCROLL_PRICE = eventSettings.getLong("EnchMaster24ScrollPrice", 6000);
            ENCHANT_MASTER_24SCROLL_PRICE = (long) (ENCHANT_MASTER_USE_RATES ? ENCHANT_MASTER_24SCROLL_PRICE * RATE_DROP_ADENA : ENCHANT_MASTER_24SCROLL_PRICE);
            ENCHANT_MASTER_1SCROLL_PRICE = eventSettings.getLong("EnchMaster1ScrollPrice", 77777);
            ENCHANT_MASTER_1SCROLL_PRICE = (long) (ENCHANT_MASTER_USE_RATES ? ENCHANT_MASTER_1SCROLL_PRICE * RATE_DROP_ADENA : ENCHANT_MASTER_1SCROLL_PRICE);
            ENCHANT_MASTER_PRICE_ID = eventSettings.getInteger("PriceEnchantMasterId", 57);
            LEVEL_UP_NOTIFICATION = eventSettings.getBoolean("EnableLevelUpNotification", false);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + EVENTS + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String SERVICES_FILE = "./config/services.ini";

    public static boolean ALLOW_EVENT_GATEKEEPER;

    public static boolean ALLOW_SHORT_2ND_PROF_QUEST;

    public static boolean SERVICES_CHANGE_NICK_ENABLED;
    public static int SERVICES_CHANGE_NICK_PRICE;
    public static int SERVICES_CHANGE_NICK_ITEM;

    public static boolean SERVICES_OLYMPIAD_POINTS_RESET_ENABLED;
    public static boolean SERVICES_OLYMPIAD_POINTS_SELL_ENABLED;
    public static long SERVICES_OLYMPIAD_POINTS_RESET_COUNT;
    public static int SERVICES_OLYMPIAD_POINTS_RESET_POINTS;
    public static int SERVICES_OLYMPIAD_POINTS_RESET_ITEM_ID;
    public static int[] SERVICES_OLYMPIAD_POINTS_SELL_COUNTS;
    public static long[] SERVICES_OLYMPIAD_POINTS_SELL_PRICES;
    public static int[] SERVICES_OLYMPIAD_POINTS_SELL_ITEMS;
    public static int SERVICES_OLYMPIAD_POINTS_SELL_LIMIT;

    public static boolean ACC_MOVE_ENABLED;
    public static int ACC_MOVE_PRICE;
    public static int ACC_MOVE_ITEM;
    public static Pattern SERVICES_CHANGE_NICK_TEMPLATE;

    public static boolean SERVICES_CHANGE_CLAN_NAME_ENABLED;
    public static int SERVICES_CHANGE_CLAN_NAME_PRICE;
    public static int SERVICES_CHANGE_CLAN_NAME_ITEM;

    public static boolean SERVICES_CHANGE_PET_NAME_ENABLED;
    public static int SERVICES_CHANGE_PET_NAME_PRICE;
    public static int SERVICES_CHANGE_PET_NAME_ITEM;

    public static boolean SERVICES_EXCHANGE_BABY_PET_ENABLED;
    public static int SERVICES_EXCHANGE_BABY_PET_PRICE;
    public static int SERVICES_EXCHANGE_BABY_PET_ITEM;

    public static boolean SERVICES_CHANGE_SEX_ENABLED;
    public static int SERVICES_CHANGE_SEX_PRICE;
    public static int SERVICES_CHANGE_SEX_ITEM;

    public static boolean SERVICES_CHANGE_BASE_ENABLED;
    public static int SERVICES_CHANGE_BASE_PRICE;
    public static int SERVICES_CHANGE_BASE_ITEM;

    public static boolean SERVICES_CHANGE_NICK_COLOR_ENABLED;
    public static int SERVICES_CHANGE_NICK_COLOR_PRICE;
    public static int SERVICES_CHANGE_NICK_COLOR_ITEM;
    public static String[] SERVICES_CHANGE_NICK_COLOR_LIST;
    public static boolean SERVICES_CHANGE_NICK_COLOR_FREE_PA;

    public static boolean SERVICES_CHANGE_TITLE_COLOR_ENABLED;
    public static int SERVICES_CHANGE_TITLE_COLOR_PRICE;
    public static int SERVICES_CHANGE_TITLE_COLOR_ITEM;
    public static String[] SERVICES_CHANGE_TITLE_COLOR_LIST;
    public static boolean SERVICES_CHANGE_TITLE_COLOR_FREE_PA;

    public static boolean SHOW_BONUS_INFO_PAGE;

    public static boolean SERVICES_RATE_BONUS_ENABLED;
    public static boolean SERVICES_RATE_BONUS_DISABLE_EFFECT_ON_RB;
    public static boolean SERVICES_BONUS_APPLY_RATES_THEN_SERVICE_DISABLED;
    public static int[] SERVICES_RATE_BONUS_PRICE;
    public static int[] SERVICES_RATE_BONUS_ITEM;
    public static double[] SERVICES_RATE_BONUS_VALUE;
    public static int[] SERVICES_RATE_BONUS_DAYS;
    public static List<L2RewardItem> SERVICES_RATE_BONUS_GIVE_ITEMID;
    public static  boolean SERVICES_ACP_ENABLE;
    public static boolean SERVICES_HPACP_ENABLE;
    public static int SERVICES_HPACP_MIN_PERCENT;
    public static int SERVICES_HPACP_MAX_PERCENT;
    public static int SERVICES_HPACP_DEF_PERCENT;
    public static int[] SERVICES_HPACP_POTIONS_ITEM_IDS;
    public static boolean SERVICES_CPACP_ENABLE;
    public static int SERVICES_CPACP_MIN_PERCENT;
    public static int SERVICES_CPACP_MAX_PERCENT;
    public static int SERVICES_CPACP_DEF_PERCENT;
    public static int[] SERVICES_CPACP_POTIONS_ITEM_IDS;
    public static boolean SERVICES_MPACP_ENABLE;
    public static int SERVICES_MPACP_MIN_PERCENT;
    public static int SERVICES_MPACP_MAX_PERCENT;
    public static int SERVICES_MPACP_DEF_PERCENT;
    public static int[] SERVICES_MPACP_POTIONS_ITEM_IDS;

    //TODO добавленно
    public static boolean SERVICES_BASH_ENABLED;
    public static boolean SERVICES_BASH_SKIP_DOWNLOAD;
    public static int SERVICES_BASH_RELOAD_TIME;

    public static boolean SERVICES_NOBLESS_SELL_ENABLED;
    public static int SERVICES_NOBLESS_SELL_PRICE;
    public static int SERVICES_NOBLESS_SELL_ITEM;

    public static boolean SERVICES_CLAN_BUY_POINTS_ENABLED;
    public static int[][] SERVICES_CLAN_BUY_POINTS_PRICE;
    public static int SERVICES_CLAN_BUY_POINTS_ITEM;

    public static boolean SERVICES_CLAN_BUY_LEVEL_ENABLED;
    public static int[] SERVICES_CLAN_BUY_LEVEL_PRICE;
    public static int SERVICES_CLAN_BUY_LEVEL_ITEM;

    public static boolean SERVICES_EXPAND_INVENTORY_ENABLED;
    public static int SERVICES_EXPAND_INVENTORY_PRICE;
    public static int SERVICES_EXPAND_INVENTORY_ITEM;
    public static int SERVICES_EXPAND_INVENTORY_MAX;
    public static boolean SERVICES_EXPAND_WAREHOUSE_ENABLED;
    public static int SERVICES_EXPAND_WAREHOUSE_PRICE;
    public static int SERVICES_EXPAND_WAREHOUSE_ITEM;
    public static boolean SERVICES_EXPAND_CWH_ENABLED;
    public static int SERVICES_EXPAND_CWH_PRICE;
    public static int SERVICES_EXPAND_CWH_ITEM;

    public static boolean SERVICES_HOW_TO_GET_COL;

    public static boolean SERVICES_OFFLINE_TRADE_ALLOW;
    public static int SERVICES_OFFLINE_TRADE_MIN_LEVEL;
    public static int SERVICES_OFFLINE_TRADE_NAME_COLOR;
    public static boolean SERVICES_OFFLINE_TRADE_KICK_NOT_TRADING;
    public static int SERVICES_OFFLINE_TRADE_PRICE;
    public static int SERVICES_OFFLINE_REQUIRED_ITEM;
    public static int SERVICES_OFFLINE_TRADE_PRICE_ITEM;


    //TODO ДОБАВЛЕННО
    public static boolean SERVICES_GIRAN_HARBOR_ENABLED;
    public static boolean SERVICES_PARNASSUS_ENABLED;
    public static boolean SERVICES_PARNASSUS_NOTAX;
    public static long SERVICES_PARNASSUS_PRICE;


    /**
     * Через сколько нужно кикать оффлайн торговцев (в секундах)
     */
    public static long SERVICES_OFFLINE_TRADE_DAYS_TO_KICK;
    public static boolean SERVICES_OFFLINE_TRADE_RESTORE_AFTER_RESTART;
    public static AbnormalEffect SERVICES_OFFLINE_ABNORMAL_EFFECT;

    public static boolean SERVICES_NO_TRADE_ONLY_OFFLINE;
    public static double SERVICES_TRADE_TAX;
    public static boolean SERVICES_TRADE_TAX_ONLY_OFFLINE;
    public static boolean SERVICES_TRADE_ONLY_FAR;
    public static int SERVICES_TRADE_RADIUS;


    public static boolean SERVICES_CHANGE_PASSWORD;
    public static boolean SERVICES_LOCK_ACCOUNT;
    public static boolean SERVICES_LOCK_ACCOUNT_IP;
    public static boolean SERVICES_LOCK_ACCOUNT_HWID;
    public static HWIDComparator LOCK_ACCOUNT_HWID_COMPARATOR;

    public static boolean SERVECES_RIDEHIRE;

    public static boolean SERVECES_SELL_NEW_PET;

    public static boolean L2HOPZONE_SERVICE_ENABLED;
    public static int L2HOPZONE_SERVICE_INTERVAL;
    public static int[] L2HOPZONE_SERVICE_ITEMID;
    public static int[] L2HOPZONE_SERVICE_COUNT;
    public static int L2HOPZONE_SERVICE_VOTES_REQUIRED_FOR_REWARD;
    public static String L2HOPZONE_SERVICE_WEB_ADRESS;

    public static boolean USE_OFFEMULATION;
    public static int ONLINE_COUNT_MODE;
    public static int ONLINE_ADD_COUNT;

    public static boolean L2TOPDEMON_ENABLED;
    public static int L2TOPDEMON_POLLINTERVAL;
    public static int L2TOPDEMON_SERVERID;
    public static String L2TOPDEMON_KEY;
    public static TopReward L2TOPDEMON_ITEM;
    public static String L2TOPDEMON_PREFIX;
    public static boolean L2TOPDEMON_IGNOREFIRST;
    public static String L2TOP_REW_MODE;

    public static boolean MMOTOP_DEMON_ENABLED;
    public static int MMOTOP_DEMON_POLLINTERVAL;
    public static String MMOTOP_DEMON_URL;
    public static TopReward MMOTOP_DEMON_ITEM;

    public static boolean SERVICES_DELEVEL_ENABLE;
    public static int SERVICES_DELEVEL_ITEM_ID;
    public static long SERVICES_DELEVEL_ITEM_COUNT;
    public static int SERVICES_DELEVEL_MIN_LEVEL;

    public static boolean SERVICES_LEVELUP_ENABLE;
    public static int SERVICES_LEVELUP_ITEM_ID;
    public static long SERVICES_LEVELUP_ITEM_COUNT;
    public static int SERVICES_LEVELUP_MIN_LEVEL;
    public static int SERVICES_LEVELUP_MAX_LEVEL;

    public static boolean SERVICES_KARMA_ENABLE;
    public static int SERVICES_KARMA_ITEM_ID;
    public static long SERVICES_KARMA_ITEM_COUNT;
    public static int SERVICES_PK_ITEM_ID;
    public static long SERVICES_PK_ITEM_COUNT;

    public static long SERVECES_PREMIUM_ACCESS;

    public static boolean TITLE_PVP_MODE;
    public static boolean TITLE_PVP_MODE_FOR_SELF;
    public static float TITLE_PVP_MODE_RATE;

    public static boolean SERVICES_HERO_SELL_ENABLED;
    public static int[] SERVICES_HERO_SELL_PRICE;
    public static int[] SERVICES_HERO_SELL_ITEM;
    public static int[] SERVICES_HERO_SELL_DAYS;

    public static boolean SERVICES_ALL_MOB_DROP_ENABLED;
    public static boolean SERVICES_ALL_MOB_NO_DROP_RATE;
    public static String ALL_MOB_DROP_ITEMS;
    public static boolean SERVICES_ALL_RB_DROP_ENABLED;
    public static boolean SERVICES_ALL_RB_NO_DROP_RATE;
    public static String ALL_RB_DROP_ITEMS;

    public static class TopReward {
        private final GArray<int[]> rewardItems = new GArray<int[]>();
        private final GArray<int[]> levels = new GArray<int[]>();

        public TopReward(final String configLine) {
            if (configLine != null && configLine.length() > 0)
                parseConfigLine(configLine);
        }

        private void parseConfigLine(final String configLine) {
            // Делим на разные блоки по уровням
            final StringTokenizer st = new StringTokenizer(configLine, ";");

            int i = 0;
            while (st.hasMoreTokens()) {
                // Обрабатываем первый блок
                final StringTokenizer st2 = new StringTokenizer(st.nextToken(), ",");

                final String[] level = st2.nextToken().split("-");
                final int minLevel = TypeFormat.parseInt(level[0]);
                final int maxLevel = TypeFormat.parseInt(level[1]);

                final StringTokenizer st3 = new StringTokenizer(st2.nextToken(), "()");
                final int itemId = TypeFormat.parseInt(st3.nextToken());
                final int quantity = TypeFormat.parseInt(st3.nextToken());

                rewardItems.add(i, new int[]{itemId, quantity});
                levels.add(i, new int[]{minLevel, maxLevel});
                i++;
            }
        }

        public int[] getReward(final int level) {
            int i = 0;
            for (final int[] arr : levels) {
                // Если уровень больше или равен минимальному и если уровень меньше или равен максимальному
                if (arr[0] <= level && level <= arr[1]) {
                    final int[] rewards = rewardItems.get(i);
                    return rewards;
                }
                i++;
            }
            return null;
        }
    }

    // ========================================================================
    public static void loadServiceSettings() {
        try {
            final L2Properties servicesSettings = new L2Properties(SERVICES_FILE);
            ftGuard.Init();

            SERVICES_OLYMPIAD_POINTS_RESET_ENABLED = servicesSettings.getBoolean("OlympiadPointsResetEnable", true);
            SERVICES_OLYMPIAD_POINTS_RESET_POINTS = servicesSettings.getInteger("OlympiadPointsResetPoints", 100);
            SERVICES_OLYMPIAD_POINTS_RESET_COUNT = servicesSettings.getInteger("OlympiadPointsResetItemCount", 4037);
            SERVICES_OLYMPIAD_POINTS_RESET_ITEM_ID = servicesSettings.getInteger("OlympiadPointsResetItemId", 4037);
            SERVICES_OLYMPIAD_POINTS_SELL_COUNTS = servicesSettings.getIntArray("OlympiadPointsSellCount", 4037, 4037, 4037, 4037);
            SERVICES_OLYMPIAD_POINTS_SELL_PRICES = servicesSettings.getLongArray("OlympiadPointsSellPrice", 4037, 4037, 4037, 4037);
            SERVICES_OLYMPIAD_POINTS_SELL_ITEMS = servicesSettings.getIntArray("OlympiadPointsSellItem", 4037, 4037, 4037, 4037);
            SERVICES_OLYMPIAD_POINTS_SELL_ENABLED = servicesSettings.getBoolean("OlympiadPointsSellEnabled", true);
            SERVICES_OLYMPIAD_POINTS_SELL_LIMIT = servicesSettings.getInteger("OlympiadPointsSellLimit", 4037);

            SERVICES_BASH_ENABLED = servicesSettings.getBoolean("BashEnabled", false);
            SERVICES_BASH_SKIP_DOWNLOAD = servicesSettings.getBoolean("BashSkipDownload", false);
            SERVICES_BASH_RELOAD_TIME = servicesSettings.getInteger("BashReloadTime", 24);

            ALLOW_SHORT_2ND_PROF_QUEST = servicesSettings.getBoolean("Short2ndProfQuest", true);
            SERVICES_CHANGE_NICK_ENABLED = servicesSettings.getBoolean("NickChangeEnabled", false);
            SERVICES_CHANGE_NICK_PRICE = servicesSettings.getInteger("NickChangePrice", 100);
            SERVICES_CHANGE_NICK_ITEM = servicesSettings.getInteger("NickChangeItem", 4037);
            ACC_MOVE_ENABLED = servicesSettings.getBoolean("AccMoveEnabled", false);
            ACC_MOVE_PRICE = servicesSettings.getInteger("AccMovePrice", 100);
            ACC_MOVE_ITEM = servicesSettings.getInteger("AccMoveItem", 4037);

            SERVICES_CHANGE_NICK_TEMPLATE = Pattern.compile(servicesSettings.getProperty("NickTemplate", "[A-Za-z0-9\u0410-\u042f\u0430-\u044f]{2,16}"));

            SERVICES_CHANGE_CLAN_NAME_ENABLED = servicesSettings.getBoolean("ClanNameChangeEnabled", false);
            SERVICES_CHANGE_CLAN_NAME_PRICE = servicesSettings.getInteger("ClanNameChangePrice", 100);
            SERVICES_CHANGE_CLAN_NAME_ITEM = servicesSettings.getInteger("ClanNameChangeItem", 4037);

            SERVICES_CHANGE_PET_NAME_ENABLED = servicesSettings.getBoolean("PetNameChangeEnabled", false);
            SERVICES_CHANGE_PET_NAME_PRICE = servicesSettings.getInteger("PetNameChangePrice", 100);
            SERVICES_CHANGE_PET_NAME_ITEM = servicesSettings.getInteger("PetNameChangeItem", 4037);

            SERVICES_EXCHANGE_BABY_PET_ENABLED = servicesSettings.getBoolean("BabyPetExchangeEnabled", false);
            SERVICES_EXCHANGE_BABY_PET_PRICE = servicesSettings.getInteger("BabyPetExchangePrice", 100);
            SERVICES_EXCHANGE_BABY_PET_ITEM = servicesSettings.getInteger("BabyPetExchangeItem", 4037);

            SERVICES_CHANGE_SEX_ENABLED = servicesSettings.getBoolean("SexChangeEnabled", false);
            SERVICES_CHANGE_SEX_PRICE = servicesSettings.getInteger("SexChangePrice", 100);
            SERVICES_CHANGE_SEX_ITEM = servicesSettings.getInteger("SexChangeItem", 4037);

            SERVICES_CHANGE_BASE_ENABLED = servicesSettings.getBoolean("BaseChangeEnabled", false);
            SERVICES_CHANGE_BASE_PRICE = servicesSettings.getInteger("BaseChangePrice", 100);
            SERVICES_CHANGE_BASE_ITEM = servicesSettings.getInteger("BaseChangeItem", 4037);

            SERVICES_CHANGE_NICK_COLOR_ENABLED = servicesSettings.getBoolean("NickColorChangeEnabled", false);
            SERVICES_CHANGE_NICK_COLOR_PRICE = servicesSettings.getInteger("NickColorChangePrice", 100);
            SERVICES_CHANGE_NICK_COLOR_ITEM = servicesSettings.getInteger("NickColorChangeItem", 4037);
            SERVICES_CHANGE_NICK_COLOR_LIST = servicesSettings.getProperty("NickColorChangeList", "00FF00").split(";");
            SERVICES_CHANGE_NICK_COLOR_FREE_PA = servicesSettings.getBoolean("NickColorChangeFreePremium", false);

            SERVICES_CHANGE_TITLE_COLOR_ENABLED = servicesSettings.getBoolean("TitleColorChangeEnabled", false);
            SERVICES_CHANGE_TITLE_COLOR_PRICE = servicesSettings.getInteger("TitleColorChangePrice", 100);
            SERVICES_CHANGE_TITLE_COLOR_ITEM = servicesSettings.getInteger("TitleColorChangeItem", 4037);
            SERVICES_CHANGE_TITLE_COLOR_LIST = servicesSettings.getProperty("TitleColorChangeList", "00FF00").split(";");
            SERVICES_CHANGE_TITLE_COLOR_FREE_PA = servicesSettings.getBoolean("TitleColorChangeFreePremium", false);

            SHOW_BONUS_INFO_PAGE = servicesSettings.getBoolean("ShowPremiumAccountPage", true);

            SERVICES_RATE_BONUS_ENABLED = servicesSettings.getBoolean("RateBonusEnabled", false);
            SERVICES_RATE_BONUS_DISABLE_EFFECT_ON_RB = servicesSettings.getBoolean("RateBonusDisableEffectOnRB", false);
            SERVICES_BONUS_APPLY_RATES_THEN_SERVICE_DISABLED = servicesSettings.getBoolean("RateBonusApplyRatesThenServiceDisabled", false);

            SERVICES_RATE_BONUS_PRICE = servicesSettings.getIntArray("RateBonusPrice", 1500, 250);
            SERVICES_RATE_BONUS_ITEM = servicesSettings.getIntArray("RateBonusItem", 4037, 4037);
            SERVICES_RATE_BONUS_VALUE = servicesSettings.getDoubleArray("RateBonusValue", 2, 2);
            SERVICES_RATE_BONUS_DAYS = servicesSettings.getIntArray("RateBonusTime", 30, 2);
            SERVICES_ACP_ENABLE = servicesSettings.getBoolean("AcpEnable",true);
            SERVICES_HPACP_ENABLE = servicesSettings.getBoolean("ACPServiceEnableHP", true);
            SERVICES_HPACP_MIN_PERCENT = Math.min(Math.max(0, servicesSettings.getInteger("ACPServiceHPMinPercent", 10)), 100);
            SERVICES_HPACP_MAX_PERCENT = Math.min(Math.max(0, servicesSettings.getInteger("ACPServiceHPMaxPercent", 90)), 100);
            SERVICES_HPACP_DEF_PERCENT = Math.min(Math.max(SERVICES_HPACP_MIN_PERCENT, servicesSettings.getInteger("ACPServiceHPDefaultPercent", 50)), SERVICES_HPACP_MAX_PERCENT);
            SERVICES_HPACP_POTIONS_ITEM_IDS = servicesSettings.getIntArray("ACPServiceHPItemIds", 1539);

            SERVICES_CPACP_ENABLE = servicesSettings.getBoolean("ACPServiceEnableCP", true);
            SERVICES_CPACP_MIN_PERCENT = Math.min(Math.max(0, servicesSettings.getInteger("ACPServiceCPMinPercent", 10)), 100);
            SERVICES_CPACP_MAX_PERCENT = Math.min(Math.max(0, servicesSettings.getInteger("ACPServiceCPMaxPercent", 90)), 100);
            SERVICES_CPACP_DEF_PERCENT = Math.min(Math.max(SERVICES_CPACP_MIN_PERCENT, servicesSettings.getInteger("ACPServiceCPDefaultPercent", 50)), SERVICES_CPACP_MAX_PERCENT);
            SERVICES_CPACP_POTIONS_ITEM_IDS = servicesSettings.getIntArray("ACPServiceCPItemIds", 5592);

            SERVICES_MPACP_ENABLE = servicesSettings.getBoolean("ACPServiceEnableMP", true);
            SERVICES_MPACP_MIN_PERCENT = Math.min(Math.max(0, servicesSettings.getInteger("ACPServiceMPMinPercent", 10)), 100);
            SERVICES_MPACP_MAX_PERCENT = Math.min(Math.max(0, servicesSettings.getInteger("ACPServiceMPMaxPercent", 90)), 100);
            SERVICES_MPACP_DEF_PERCENT = Math.min(Math.max(SERVICES_MPACP_MIN_PERCENT, servicesSettings.getInteger("ACPServiceMPDefaultPercent", 50)), SERVICES_MPACP_MAX_PERCENT);
            SERVICES_MPACP_POTIONS_ITEM_IDS = servicesSettings.getIntArray("ACPServiceMPItemIds", 728);




            final String rateBonusGiveItemID = servicesSettings.getProperty("RateBonusReward", "");
            if (rateBonusGiveItemID.length() == 0)
                SERVICES_RATE_BONUS_GIVE_ITEMID = Collections.emptyList();
            else {
                SERVICES_RATE_BONUS_GIVE_ITEMID = new ArrayList<L2RewardItem>(rateBonusGiveItemID.length());
                final StringTokenizer st = new StringTokenizer(rateBonusGiveItemID, ";");
                while (st.hasMoreTokens()) {
                    final String[] data = st.nextToken().split(",");
                    final int itemId = Integer.parseInt(data[0]);
                    final String[] count = data[1].split("-");
                    final long min = Long.parseLong(count[0]);
                    final long max = Long.parseLong(count[1]);
                    final float chance = Float.parseFloat(data[2]);
                    SERVICES_RATE_BONUS_GIVE_ITEMID.add(new L2RewardItem(itemId, chance, min, max));
                }
            }

            SERVICES_NOBLESS_SELL_ENABLED = servicesSettings.getBoolean("NoblessSellEnabled", false);
            SERVICES_NOBLESS_SELL_PRICE = servicesSettings.getInteger("NoblessSellPrice", 1000);
            SERVICES_NOBLESS_SELL_ITEM = servicesSettings.getInteger("NoblessSellItem", 4037);

            SERVICES_CLAN_BUY_POINTS_ENABLED = servicesSettings.getBoolean("ClanBuyPointsEnabled", false);

            final String[] clanBuyPointsPrice = String.valueOf(servicesSettings.getProperty("ClanBuyPointsPrice", "")).split(";");
            SERVICES_CLAN_BUY_POINTS_PRICE = new int[clanBuyPointsPrice.length][2];
            for (int i = 0; i < clanBuyPointsPrice.length; i++)
                SERVICES_CLAN_BUY_POINTS_PRICE[i] = ArrayUtil.toIntArray(clanBuyPointsPrice[i], "-");

            SERVICES_CLAN_BUY_POINTS_ITEM = servicesSettings.getInteger("ClanBuyPointsItem", 4037);

            SERVICES_CLAN_BUY_LEVEL_ENABLED = servicesSettings.getBoolean("ClanBuyLevelEnabled", false);
            SERVICES_CLAN_BUY_LEVEL_PRICE = servicesSettings.getIntArray("ClanBuyLevelPrice", 10, 20, 30, 40, 100, 120, 150, 200, 270, 350, 500);
            SERVICES_CLAN_BUY_LEVEL_ITEM = servicesSettings.getInteger("ClanBuyLevelItem", 4037);

            SERVICES_EXPAND_INVENTORY_ENABLED = servicesSettings.getBoolean("ExpandInventoryEnabled", false);
            SERVICES_EXPAND_INVENTORY_PRICE = servicesSettings.getInteger("ExpandInventoryPrice", 1000);
            SERVICES_EXPAND_INVENTORY_ITEM = servicesSettings.getInteger("ExpandInventoryItem", 4037);
            SERVICES_EXPAND_INVENTORY_MAX = servicesSettings.getInteger("ExpandInventoryMax", 250);

            SERVICES_EXPAND_WAREHOUSE_ENABLED = servicesSettings.getBoolean("ExpandWarehouseEnabled", false);
            SERVICES_EXPAND_WAREHOUSE_PRICE = servicesSettings.getInteger("ExpandWarehousePrice", 1000);
            SERVICES_EXPAND_WAREHOUSE_ITEM = servicesSettings.getInteger("ExpandWarehouseItem", 4037);

            SERVICES_EXPAND_CWH_ENABLED = servicesSettings.getBoolean("ExpandCWHEnabled", false);
            SERVICES_EXPAND_CWH_PRICE = servicesSettings.getInteger("ExpandCWHPrice", 1000);
            SERVICES_EXPAND_CWH_ITEM = servicesSettings.getInteger("ExpandCWHItem", 4037);

            SERVICES_HOW_TO_GET_COL = servicesSettings.getBoolean("HowToGetCoL", false);

            SERVICES_OFFLINE_TRADE_ALLOW = servicesSettings.getBoolean("AllowOfflineTrade", false);
            SERVICES_OFFLINE_TRADE_MIN_LEVEL = servicesSettings.getInteger("OfflineMinLevel", 0);
            SERVICES_OFFLINE_TRADE_NAME_COLOR = Integer.decode("0x" + servicesSettings.getProperty("OfflineTradeNameColor", "B0FFFF"));
            SERVICES_OFFLINE_TRADE_KICK_NOT_TRADING = servicesSettings.getBoolean("KickOfflineNotTrading", true);
            SERVICES_OFFLINE_TRADE_PRICE_ITEM = servicesSettings.getInteger("OfflineTradePriceItem", 0);
            SERVICES_OFFLINE_TRADE_PRICE = servicesSettings.getInteger("OfflineTradePrice", 0);
            SERVICES_OFFLINE_REQUIRED_ITEM = servicesSettings.getInteger("OfflineTradeRequiredItem", -1);

            SERVICES_OFFLINE_TRADE_DAYS_TO_KICK = servicesSettings.getLong("OfflineTradeDaysToKick", 14) * 60 * 60 * 24;
            SERVICES_OFFLINE_TRADE_RESTORE_AFTER_RESTART = servicesSettings.getBoolean("OfflineRestoreAfterRestart", true);
            SERVICES_OFFLINE_ABNORMAL_EFFECT = AbnormalEffect.valueOf(servicesSettings.getProperty("OfflineAbnormalEffect", "NULL").toUpperCase());

            SERVICES_NO_TRADE_ONLY_OFFLINE = servicesSettings.getBoolean("NoTradeOnlyOffline", false);
            SERVICES_TRADE_TAX = servicesSettings.getDouble("TradeTax", 0);
            SERVICES_TRADE_TAX_ONLY_OFFLINE = servicesSettings.getBoolean("TradeTaxOnlyOffline", false);
            SERVICES_TRADE_RADIUS = servicesSettings.getInteger("TradeRadius", 30);
            SERVICES_TRADE_ONLY_FAR = servicesSettings.getBoolean("TradeOnlyFar", false);

            SERVICES_GIRAN_HARBOR_ENABLED = servicesSettings.getBoolean("GiranHarborZone", false);
            SERVICES_PARNASSUS_ENABLED = servicesSettings.getBoolean("ParnassusZone", false);
            SERVICES_PARNASSUS_NOTAX = servicesSettings.getBoolean("ParnassusNoTax", false);
            SERVICES_PARNASSUS_PRICE = servicesSettings.getLong("ParnassusPrice", 500000);


            SERVICES_LOCK_ACCOUNT = servicesSettings.getBoolean("LockAccount", false);
            SERVICES_LOCK_ACCOUNT_IP = servicesSettings.getBoolean("LockAccountIP", false);
            SERVICES_LOCK_ACCOUNT_HWID = servicesSettings.getBoolean("LockAccountHWID", false);
            final int hwid_lock = servicesSettings.getInteger("HWIDLockMask", 14);
            LOCK_ACCOUNT_HWID_COMPARATOR = new HWIDComparator((hwid_lock & 1) != 0, (hwid_lock & 2) != 0);

            SERVICES_CHANGE_PASSWORD = servicesSettings.getBoolean("ChangePassword", true);

            SERVECES_RIDEHIRE = servicesSettings.getBoolean("RidePet", false);

            SERVECES_SELL_NEW_PET = servicesSettings.getBoolean("SellNewPets", false);

            L2HOPZONE_SERVICE_ENABLED = servicesSettings.getBoolean("L2HopZoneServiceEnabled", false);
            L2HOPZONE_SERVICE_INTERVAL = servicesSettings.getInteger("L2HopZoneServiceInterval", 30);

            L2HOPZONE_SERVICE_ITEMID = servicesSettings.getIntArray("L2HopZoneServiceItemID", 0, 0);
            L2HOPZONE_SERVICE_COUNT = servicesSettings.getIntArray("L2HopZoneServiceItemCount", 0, 0);

            L2HOPZONE_SERVICE_WEB_ADRESS = servicesSettings.getProperty("L2HopZoneServiceWebAddress", "");
            L2HOPZONE_SERVICE_VOTES_REQUIRED_FOR_REWARD = servicesSettings.getInteger("L2HopZoneVotesRequiredForReward", 10);

            USE_OFFEMULATION = servicesSettings.getBoolean("UseEmulation", true);
            ONLINE_COUNT_MODE = servicesSettings.getInteger("OnlineCountMode", 1);
            ONLINE_ADD_COUNT = servicesSettings.getInteger("AddOnlineCount", 500);

            L2TOPDEMON_ENABLED = servicesSettings.getBoolean("L2TopManagerEnabled", false);
            L2TOPDEMON_SERVERID = servicesSettings.getInteger("L2TopManagerServerID", 0);
            L2TOPDEMON_KEY = servicesSettings.getProperty("L2TopManagerServerKey", "");
            L2TOPDEMON_POLLINTERVAL = servicesSettings.getInteger("L2TopManagerPollInterval", 10);
            L2TOPDEMON_PREFIX = servicesSettings.getProperty("L2TopManagerPrefix", "");

            L2TOPDEMON_ITEM = new TopReward(servicesSettings.getProperty("L2TopManagerRewardItem", "[1-85,0(0)]"));

            L2TOPDEMON_IGNOREFIRST = servicesSettings.getBoolean("L2TopManagerDoNotRewardAtFirstTime", false);
            L2TOP_REW_MODE = servicesSettings.getProperty("L2TopRewardMode", "ALL");

            MMOTOP_DEMON_ENABLED = servicesSettings.getBoolean("MMOTopManagerEnable", false);
            MMOTOP_DEMON_POLLINTERVAL = servicesSettings.getInteger("MMOTopManagerRefreshTime", 10);
            MMOTOP_DEMON_URL = servicesSettings.getProperty("MMOTopManagerUrl", "");
            MMOTOP_DEMON_ITEM = new TopReward(servicesSettings.getProperty("MMOTopManagerRewardItem", "[1-85,0(0)]"));

            SERVICES_DELEVEL_ENABLE = servicesSettings.getBoolean("DelevelEnabled", false);
            SERVICES_DELEVEL_ITEM_ID = servicesSettings.getInteger("DelevelItemId", 4037);
            SERVICES_DELEVEL_ITEM_COUNT = servicesSettings.getLong("DelevelItemCount", 10);
            SERVICES_DELEVEL_MIN_LEVEL = servicesSettings.getInteger("DelevelMinLevel", 40);

            SERVICES_LEVELUP_ENABLE = servicesSettings.getBoolean("LevelUpEnabled", false);
            SERVICES_LEVELUP_ITEM_ID = servicesSettings.getInteger("LevelUpItemId", 4037);
            SERVICES_LEVELUP_ITEM_COUNT = servicesSettings.getLong("LevelUpItemCount", 10);
            SERVICES_LEVELUP_MIN_LEVEL = servicesSettings.getInteger("LevelUpMinLevel", 0);
            SERVICES_LEVELUP_MAX_LEVEL = servicesSettings.getInteger("LevelUpMaxLevel", 85);

            SERVICES_KARMA_ENABLE = servicesSettings.getBoolean("NoKarmaEnabled", false);
            SERVICES_KARMA_ITEM_ID = servicesSettings.getInteger("NoKarmaKarmaItemId", 4037);
            SERVICES_KARMA_ITEM_COUNT = servicesSettings.getLong("NoKarmaKarmaItemCount", 10);
            SERVICES_PK_ITEM_ID = servicesSettings.getInteger("NoKarmaPKItemId", 4037);
            SERVICES_PK_ITEM_COUNT = servicesSettings.getLong("NoKarmaPKItemCount", 50);

            SERVECES_PREMIUM_ACCESS = servicesSettings.getLong("PremiumAccessRule", 0);

            TITLE_PVP_MODE = servicesSettings.getBoolean("TitlePVPMode", false);
            TITLE_PVP_MODE_FOR_SELF = servicesSettings.getBoolean("TitlePVPModeSelf", false);
            TITLE_PVP_MODE_RATE = servicesSettings.getFloat("TitlePVPModeRate", 1.5f);

            SERVICES_HERO_SELL_ENABLED = servicesSettings.getBoolean("HeroSellEnabled", false);
            SERVICES_HERO_SELL_DAYS = servicesSettings.getIntArray("HeroSellTime", 5, 10, 30, 60);
            SERVICES_HERO_SELL_PRICE = servicesSettings.getIntArray("HeroSellPrice", 80, 160, 500, 1000);
            SERVICES_HERO_SELL_ITEM = servicesSettings.getIntArray("HeroSellItem", 4037, 4037, 4037, 4037);

            SERVICES_ALL_MOB_DROP_ENABLED = servicesSettings.getBoolean("AltMonsterDropEnabled", false);
            SERVICES_ALL_MOB_NO_DROP_RATE = servicesSettings.getBoolean("AltMonsterDropRateEnable", false);
            ALL_MOB_DROP_ITEMS = servicesSettings.getProperty("AltMonsterMobDropItems", "");
            SERVICES_ALL_RB_DROP_ENABLED = servicesSettings.getBoolean("AltRaidBossDropEnabled", false);
            SERVICES_ALL_RB_NO_DROP_RATE = servicesSettings.getBoolean("AltRaidBossDropRateEnable", false);
            ALL_RB_DROP_ITEMS = servicesSettings.getProperty("AltRaidBossDropItems", "");
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + SERVICES_FILE + " File.");
        }
    }

    // *******************************************************************************************

    public static boolean ENABLE_OLYMPIAD;
    public static boolean OLYMP_TIME_TELEPORT;
    public static boolean ENABLE_OLYMPIAD_SPECTATING;

    /**
     * Olympiad Compitition Starting time
     */
    public static int OLY_START_TIME;
    /**
     * Olympiad Compition Min
     */
    public static int OLY_MIN;
    /**
     * Olympaid Comptetition Period
     */
    public static long OLY_CPERIOD;
    /**
     * Olympaid Weekly Period
     */
    public static long OLY_WPERIOD;
    /**
     * Olympaid Validation Period
     */
    public static long OLY_VPERIOD;

    public static boolean OLY_CHECK_HWID;
    public static boolean OLY_REDUCE_POINTS_ON_TIE;

    public static int OLY_BATTLE_REWARD_ITEM;
    public static int OLY_COMP_RITEM;

    public static int OLY_GP_PER_POINT;

    public static int OLY_HERO_POINTS;

    public static int OLY_RANK1_POINTS;
    public static int OLY_RANK2_POINTS;
    public static int OLY_RANK3_POINTS;
    public static int OLY_RANK4_POINTS;
    public static int OLY_RANK5_POINTS;

    public static int OLY_CLASSED_RITEM_C;
    public static int OLY_NONCLASSED_RITEM_C;
    public static int OLY_RANDOM_TEAM_RITEM_C;
    public static int OLY_TEAM_RITEM_C;

    public static GArray<Integer> OLY_LIST_RESTRICTED_ITEMS;

    public static int CLASS_GAME_MIN;
    public static int NONCLASS_GAME_MIN;
    public static int RANDOM_TEAM_GAME_MIN;
    public static int TEAM_GAME_MIN;

    public static int OLY_REG_DISPLAY;
    public static boolean OLYMP_PERIOD;
    public static int OLYMP_DAYS;
    public static boolean OLYMPIAD_HEAL_FULL_STATS;

    // ========================================================================
    public static final String OLYMPIAD_FILE = "./config/olympiad_option.ini";

    // ========================================================================
    public static void loadOlympiadSettings() {
        try {
            L2Properties olysettings = new L2Properties(OLYMPIAD_FILE);

            ENABLE_OLYMPIAD = olysettings.getBoolean("EnableOlympiad", false);
            OLYMP_TIME_TELEPORT = olysettings.getBoolean("OlympiadTimeTeleport", false);
            ENABLE_OLYMPIAD_SPECTATING = olysettings.getBoolean("EnableOlympiadSpectating", false);
            OLY_REDUCE_POINTS_ON_TIE = olysettings.getBoolean("OlyReducePointsOnTie", true);
            OLY_START_TIME = olysettings.getInteger("AltOlyStartTime", 18);
            OLY_MIN = olysettings.getInteger("AltOlyMin", 0);

            OLY_CPERIOD = olysettings.getLong("AltOlyCPeriod", 21600000);
            OLY_WPERIOD = olysettings.getLong("AltOlyWPeriod", 604800000);
            OLY_VPERIOD = olysettings.getLong("AltOlyVPeriod", 86400000);

            OLY_CHECK_HWID = olysettings.getBoolean("OlyCheckHardwareID", false);

            CLASS_GAME_MIN = olysettings.getInteger("ClassGameMin", 5);
            NONCLASS_GAME_MIN = olysettings.getInteger("NonClassGameMin", 9);
            RANDOM_TEAM_GAME_MIN = olysettings.getInteger("RandomTeamGameMin", 12);
            TEAM_GAME_MIN = olysettings.getInteger("TeamGameMin", 4);

            OLY_BATTLE_REWARD_ITEM = olysettings.getInteger("AltOlyBattleRewItem", 13722);
            OLY_COMP_RITEM = olysettings.getInteger("AltOlyCompRewItem", 13722);

            OLY_REG_DISPLAY = olysettings.getInteger("AltOlyRegistrationDisplayNumber", 100);

            OLY_CLASSED_RITEM_C = olysettings.getInteger("AltOlyClassedRewItemCount", 50);
            OLY_NONCLASSED_RITEM_C = olysettings.getInteger("AltOlyNonClassedRewItemCount", 40);
            OLY_RANDOM_TEAM_RITEM_C = olysettings.getInteger("AltOlyRandomTeamRewItemCount", 30);
            OLY_TEAM_RITEM_C = olysettings.getInteger("AltOlyTeamRewItemCount", 50);

            OLY_RANK1_POINTS = olysettings.getInteger("AltOlyRank1Points", 120);
            OLY_RANK2_POINTS = olysettings.getInteger("AltOlyRank2Points", 80);
            OLY_RANK3_POINTS = olysettings.getInteger("AltOlyRank3Points", 55);
            OLY_RANK4_POINTS = olysettings.getInteger("AltOlyRank4Points", 35);
            OLY_RANK5_POINTS = olysettings.getInteger("AltOlyRank5Points", 20);

            OLY_GP_PER_POINT = olysettings.getInteger("AltOlyGPPerPoint", 1000);
            OLY_HERO_POINTS = olysettings.getInteger("AltOlyHeroPoints", 180);
            OLYMP_DAYS = olysettings.getInteger("OlympiadDays", 15);
            final String list = olysettings.getProperty("AltOlyRestrictedItems", "0");
            OLY_LIST_RESTRICTED_ITEMS = new GArray<Integer>();
            for (final String id : list.split(","))
                OLY_LIST_RESTRICTED_ITEMS.add(TypeFormat.parseInt(id));

            OLYMP_PERIOD = olysettings.getBoolean("AllowOlympiad2Weeks", false);
            OLYMPIAD_HEAL_FULL_STATS = olysettings.getBoolean("AllowOlympiadHeall", false);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + OLYMPIAD_FILE + " File.");
        }
    }

    // *******************************************************************************************

    public static double EPAULETTE_DROP_RATE;

    // ========================================================================
    public static final String RATE_FILE = "./config/rate.ini";

    // ========================================================================
    public static void loadRateSettings() {
        try {
            final L2Properties rateSettings = new L2Properties(RATE_FILE);

            RATE_XP = rateSettings.getDouble("RateXp", 1);
            RATE_SP = rateSettings.getDouble("RateSp", 1);

            DYNAMIC_RATE_XP = rateSettings.getBoolean("DynamicRateXp", false);
            DYNAMIC_RATE_SP = rateSettings.getBoolean("DynamicRateSp", false);
            DYNAMIC_RATE_DROP_ADENA = rateSettings.getBoolean("DynamicRateDropAdena", false);
            DYNAMIC_RATE_DROP_ITEMS = rateSettings.getBoolean("DynamicRateDropItems", false);
            DYNAMIC_RATE_SPOIL = rateSettings.getBoolean("DynamicRateDropSpoil", false);

            CRAFT_RATE_XP = rateSettings.getDouble("CraftRateXp", 1);
            CRAFT_RATE_SP = rateSettings.getDouble("CraftRateSp", 1);
            ALT_GAME_CREATION_RARE_XPSP_RATE = rateSettings.getDouble("AltGameCreationRareXpSpRate", 2);

            // ========================================================================
            // Рейты для квестов
            RATE_QUESTS_REWARD = rateSettings.getDouble("RateQuestsReward", 1);
            RATE_QUESTS_REWARD_XP = rateSettings.getDouble("RateQuestRewardXP", 1);
            RATE_QUESTS_REWARD_SP = rateSettings.getDouble("RateQuestRewardSP", 1);
            RATE_QUESTS_OCCUPATION_CHANGE = rateSettings.getBoolean("RateQuestsRewardOccupationChange", true);
            RATE_QUESTS_ALT_MULTIPLIERS_REWARD = rateSettings.getBoolean("UseQuestRewardMultipliers", false);
            RATE_QUESTS_REWARD_ADENA = rateSettings.getDouble("RateQuestRewardAdena", 1);
            RATE_QUESTS_REWARD_POTION = rateSettings.getDouble("RateQuestRewardPotion", 1);
            RATE_QUESTS_REWARD_SCROLL = rateSettings.getDouble("RateQuestRewardScroll", 1);
            RATE_QUESTS_REWARD_RECIPE = rateSettings.getDouble("RateQuestRewardRecipe", 1);
            RATE_QUESTS_REWARD_MATERIAL = rateSettings.getDouble("RateQuestRewardMaterial", 1);

            RATE_QUESTS_DROP = rateSettings.getDouble("RateQuestsDrop", 1);
            RATE_QUESTS_DROP_PROF = rateSettings.getDouble("RateQuestsDropProf", 1);
            // ========================================================================

            RATE_DROP_ADENA = rateSettings.getDouble("RateDropAdena", 1);
            RATE_DROP_ADENA_PARTY = rateSettings.getFloat("RateDropAdenaParty", 1);
            RATE_DROP_ITEMS_PARTY = rateSettings.getFloat("RateDropItemsParty", 1);
            RATE_XP_PARTY = rateSettings.getFloat("RateXpParty", 1);
            RATE_SP_PARTY = rateSettings.getFloat("RateSpParty", 1);
            RATE_DROP_ITEMS = rateSettings.getDouble("RateDropItems", 1);
            RATE_DROP_COMMON_ITEMS = rateSettings.getDouble("RateDropCommonItems", 1);
            RATE_DROP_RAIDBOSS = rateSettings.getDouble("RateRaidBoss", 1);
            RATE_DROP_EPIC_RAIDBOSS = rateSettings.getDouble("RateEpicRaidBoss", 1);
            RATE_DROP_EPIC_RAIDBOSS_IDs = new IntArrayList(rateSettings.getIntArray("EpicRaidIds"), false);
            RATE_DROP_SPOIL = rateSettings.getDouble("RateDropSpoil", 1);
            MAX_SPOIL_LEVEL_DEPEND = rateSettings.getInteger("MaxSpoilLevelDepend", 11);
            RATE_MANOR = rateSettings.getInteger("RateManor", 1);
            RATE_FISH_DROP_COUNT = rateSettings.getDouble("RateFishDropCount", 1);
            RATE_SIEGE_GUARDS_PRICE = rateSettings.getDouble("RateSiegeGuardsPrice", 1);
            RATE_RAID_REGEN = rateSettings.getDouble("RateRaidRegen", 1);
            RAID_MAX_LEVEL_DIFF = rateSettings.getInteger("RaidMaxLevelDiff", 8);
            PARALIZE_ON_RAID_DIFF = rateSettings.getBoolean("ParalizeOnRaidLevelDiff", true);
            RATE_MASTERWORK = rateSettings.getInteger("RateMasterwork", 20);
            RATE_CRITICAL_CRAFT_CHANCE = rateSettings.getInteger("RateCriticalCraftChance", 5);
            RATE_CRITICAL_CRAFT_MULTIPLIER = rateSettings.getInteger("RateCriticalCraftMutliplier", 2);
            GATEKEEPER_MODIFIER = rateSettings.getDouble("GkCostMultiplier", 1);

            RATE_BREAKPOINT = rateSettings.getInteger("RateBreakpoint", 15);
            MAX_DROP_ITERATIONS = rateSettings.getInteger("RateMaxIterations", 30);

            EPAULETTE_DROP_RATE = rateSettings.getDouble("EpauletteDropRate", 1);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + RATE_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String WEDDING_FILE = "./config/wedding.ini";

    // ========================================================================
    public static void loadWeddingSettings() {
        try {
            final L2Properties weddingSettings = new L2Properties(WEDDING_FILE);

            ALLOW_WEDDING = weddingSettings.getBoolean("AllowWedding", false);
            WEDDING_GIVE_ITEM = weddingSettings.getInteger("WeddingGiveItem", 0);
            WEDDING_GIVE_ITEM_COUNT = weddingSettings.getInteger("WeddingGiveCount", 1);
            WEDDING_PRICE = weddingSettings.getInteger("WeddingPrice", 500000);
            WEDDING_PUNISH_INFIDELITY = weddingSettings.getBoolean("WeddingPunishInfidelity", true);
            WEDDING_TELEPORT = weddingSettings.getBoolean("WeddingTeleport", true);
            WEDDING_TELEPORT_PRICE = weddingSettings.getInteger("WeddingTeleportPrice", 500000);
            WEDDING_TELEPORT_INTERVAL = weddingSettings.getInteger("WeddingTeleportInterval", 120);
            WEDDING_SAMESEX = weddingSettings.getBoolean("WeddingAllowSameSex", true);
            WEDDING_FORMALWEAR = weddingSettings.getBoolean("WeddingFormalWear", true);
            WEDDING_DIVORCE_COSTS = weddingSettings.getInteger("WeddingDivorceCosts", 20);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + WEDDING_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String WALKER_FILE = "./config/walker.ini";

    // ========================================================================
    public static void loadWalkerSettings() {
        try {
            final L2Properties walkerSettings = new L2Properties(WALKER_FILE);

            ALLOW_L2WALKER_CLIENT = L2WalkerAllowed.valueOf(walkerSettings.getProperty("AllowL2Walker", "false"));
            MAX_L2WALKER_LEVEL = walkerSettings.getInteger("MaxL2WalkerLevel", 5);
            ALLOW_CRAFT_BOT = walkerSettings.getBoolean("AllowCraftBot", true);
            ALLOW_BOT_CAST = walkerSettings.getBoolean("AllowBotCast", false);
            KILL_BOTS_WITH_SKILLCOOLTIME = walkerSettings.getBoolean("KillBotsWithSkillCoolTime", true);
            L2WALKER_PUNISHMENT = walkerSettings.getInteger("L2WalkerPunishment", 1);
            BUGUSER_PUNISH = walkerSettings.getInteger("BugUserPunishment", 2);
            DEFAULT_PUNISH = walkerSettings.getInteger("IllegalActionPunishment", 1);

        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + WALKER_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String LOTTERY_FILE = "./config/lottery.ini";

    // ========================================================================
    public static void loadLotterySettings() {
        try {
            final L2Properties lotterySettings = new L2Properties(LOTTERY_FILE);

            ALT_LOTTERY_PRIZE = lotterySettings.getInteger("AltLotteryPrize", 50000);
            ALT_LOTTERY_TICKET_PRICE = lotterySettings.getInteger("AltLotteryTicketPrice", 2000);
            ALT_LOTTERY_5_NUMBER_RATE = lotterySettings.getDouble("AltLottery5NumberRate", 0.6);
            ALT_LOTTERY_4_NUMBER_RATE = lotterySettings.getDouble("AltLottery4NumberRate", 0.2);
            ALT_LOTTERY_3_NUMBER_RATE = lotterySettings.getDouble("AltLottery3NumberRate", 0.2);
            ALT_LOTTERY_2_AND_1_NUMBER_PRIZE = lotterySettings.getInteger("AltLottery2and1NumberPrize", 200);

        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + LOTTERY_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String MANOR_FILE = "./config/manor.ini";

    // ========================================================================
    public static void loadManorSettings() {
        try {
            final L2Properties manorSettings = new L2Properties(MANOR_FILE);

            MANOR_SOWING_BASIC_SUCCESS = manorSettings.getDouble("BasePercentChanceOfSowingSuccess", 100);
            MANOR_SOWING_ALT_BASIC_SUCCESS = manorSettings.getDouble("BasePercentChanceOfSowingAltSuccess", 10);
            MANOR_HARVESTING_BASIC_SUCCESS = manorSettings.getDouble("BasePercentChanceOfHarvestingSuccess", 90);
            MANOR_DIFF_PLAYER_TARGET = manorSettings.getDouble("MinDiffPlayerMob", 5);
            MANOR_DIFF_PLAYER_TARGET_PENALTY = manorSettings.getDouble("DiffPlayerMobPenalty", 5);
            MANOR_DIFF_SEED_TARGET = manorSettings.getDouble("MinDiffSeedMob", 5);
            MANOR_DIFF_SEED_TARGET_PENALTY = manorSettings.getDouble("DiffSeedMobPenalty", 5);
            ALLOW_MANOR = manorSettings.getBoolean("AllowManor", true);
            MANOR_REFRESH_TIME = manorSettings.getInteger("AltManorRefreshTime", 20);
            MANOR_REFRESH_MIN = manorSettings.getInteger("AltManorRefreshMin", 00);
            MANOR_APPROVE_TIME = manorSettings.getInteger("AltManorApproveTime", 6);
            MANOR_APPROVE_MIN = manorSettings.getInteger("AltManorApproveMin", 00);
            MANOR_MAINTENANCE_PERIOD = manorSettings.getInteger("AltManorMaintenancePeriod", 360000);
            MANOR_SAVE_ALL_ACTIONS = manorSettings.getBoolean("AltManorSaveAllActions", false);
            MANOR_SAVE_PERIOD_RATE = manorSettings.getInteger("AltManorSavePeriodRate", 2);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + MANOR_FILE + " File.");
        }
    }

    // *******************************************************************************************

    public static boolean PETITIONING_ALLOWED;
    public static int MAX_PETITIONS_PER_PLAYER;
    public static int MAX_PETITIONS_PENDING;

    // ========================================================================
    public static final String CHAT_FILE = "./config/chat.ini";

    // ========================================================================
    public static void loadChatSettings() {
        try {
            final L2Properties chatSettings = new L2Properties(CHAT_FILE);
            GM_ANNOUNCE_LOGIN = chatSettings.getBoolean("GmAnnounceLogin", false);
            ENT_SHOWENTERMESSON = chatSettings.getBoolean("EntWelcomeMessageOn", false);
            ENT_SHOWENTERMESS = String.valueOf(chatSettings.getProperty("EntWelcomeMessage", "")).trim();
            ANNOUNCE_CASTLE_LORDS = chatSettings.getBoolean("AnnounceCastleLords", false);
            GLOBAL_CHAT = chatSettings.getInteger("GlobalChat", 0);
            MAIN_LEVEL_CHAT = chatSettings.getInteger("MainLevelChat", 0);
            GLOBAL_TRADE_CHAT = chatSettings.getInteger("GlobalTradeChat", 0);
            SHOUT_CHAT_MODE = chatSettings.getInteger("ShoutChatMode", 1);
            TRADE_CHAT_MODE = chatSettings.getInteger("TradeChatMode", 1);

            SIMPLE_MAT_CHECK = chatSettings.getBoolean("SimpleMatCheck", false);
            MAT_BANCHAT = chatSettings.getBoolean("MAT_BANCHAT", false);
            PRIVATE_STORE_MSG_FILTER = chatSettings.getBoolean("EnableStoreMsgFilter", false);
            INDECENT_BLOCKCHAT = chatSettings.getBoolean("INDECENT_BANCHAT", false);
            SHOUT_FILTER = chatSettings.getBoolean("SHOUT_FILTER", false);

            MAT_KARMA = chatSettings.getInteger("MAT_KARMA", 0);

            BAN_CHANNEL = chatSettings.getProperty("MAT_BAN_CHANNEL", "0");
            INDECENT_BLOCK_CHANNEL = chatSettings.getProperty("INDECENT_BAN_CHANNEL", "0");

            MAT_BAN_COUNT_CHANNELS = 1;
            for (final String id : BAN_CHANNEL.split(",")) {
                BAN_CHANNEL_LIST[MAT_BAN_COUNT_CHANNELS] = TypeFormat.parseInt(id);
                MAT_BAN_COUNT_CHANNELS++;
            }
            INDECENT_BLOCK_COUNT_CHANNELS = 1;
            for (final String id : INDECENT_BLOCK_CHANNEL.split(",")) {
                INDECENT_CHANNEL_LIST[INDECENT_BLOCK_COUNT_CHANNELS] = TypeFormat.parseInt(id);
                INDECENT_BLOCK_COUNT_CHANNELS++;
            }

            MAT_REPLACE = chatSettings.getBoolean("MAT_REPLACE", false);
            MAT_REPLACE_STRING = chatSettings.getProperty("MAT_REPLACE_STRING", "[censored]");
            MAT_ANNOUNCE = chatSettings.getBoolean("MAT_ANNOUNCE", true);
            MAT_ANNOUNCE_FOR_ALL_WORLD = chatSettings.getBoolean("MAT_ANNOUNCE_FOR_ALL_WORLD", true);
            MAT_ANNOUNCE_NICK = chatSettings.getBoolean("MAT_ANNOUNCE_NICK", true);
            UNCHATBANTIME = chatSettings.getInteger("Timer_to_UnBan", 30);

            PETITIONING_ALLOWED = chatSettings.getBoolean("PetitioningAllowed", false);
            MAX_PETITIONS_PER_PLAYER = chatSettings.getInteger("MaxPetitionsPerPlayer", 5);
            MAX_PETITIONS_PENDING = chatSettings.getInteger("MaxPetitionsPending", 25);

            GM_SHOW_ANNOUNCER_NAME = chatSettings.getBoolean("GMShowAnnouncerName", false);
            ENABLE_CHARNAME_BLOCKSPAM = chatSettings.getBoolean("EnableCharnameBlockList", false);
            if (ENABLE_CHARNAME_BLOCKSPAM) {
                final String nameBlockList = chatSettings.getProperty("CharnameBlockList", "");
                CHARNAME_BLOCKSPAM_LIST = new GArray<String>();
                CHARNAME_BLOCKSPAM_LIST.addAll(Arrays.asList(nameBlockList.split(",")));
            }

            ENABLE_TRADE_BLOCKSPAM = chatSettings.getBoolean("EnableTradeBlockList", false);
            if (ENABLE_TRADE_BLOCKSPAM) {
                final String tradeBlockList = chatSettings.getProperty("TradeBlockList", "");
                TRADE_LIST = new GArray<String>();
                TRADE_LIST.addAll(Arrays.asList(tradeBlockList.split(",")));
                final String tradeSymbolList = chatSettings.getProperty("TradeSymbolList", "");
                TRADE_LIST_SYMBOLS = new GArray<String>();
                TRADE_LIST_SYMBOLS.addAll(Arrays.asList(tradeSymbolList.split(",")));
            }
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + CHAT_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String BOSS_FILE = "./config/boss.ini";
    // ========================================================================
    /*****************************************
     * Antharas CONFIG *
     *****************************************/
    public static int FWA_FIXINTERVALOFANTHARAS;
    public static int FWA_RANDOMINTERVALOFANTHARAS;
    public static int FWA_APPTIMEOFANTHARAS;
    public static int FWA_ACTIVITYTIMEOFANTHARAS;
    public static boolean FWA_OLDANTHARAS;
    public static int FWA_LIMITOFWEAK;
    public static int FWA_LIMITOFNORMAL;
    public static int FWA_INTERVALOFBEHEMOTHONWEAK;
    public static int FWA_INTERVALOFBEHEMOTHONNORMAL;
    public static int FWA_INTERVALOFBEHEMOTHONSTRONG;
    public static int FWA_INTERVALOFBOMBERONWEAK;
    public static int FWA_INTERVALOFBOMBERONNORMAL;
    public static int FWA_INTERVALOFBOMBERONSTRONG;

    /******************************************
     * Baium CONFIG *
     ******************************************/
    public static int FWB_FIXINTERVALOFBAIUM;
    public static int FWB_RANDOMINTERVALOFBAIUM;
    public static int FWB_ACTIVITYTIMEOFBAIUM;
    public static int FWB_LIMITUNTILSLEEP;

    /******************************************
     * Valakas CONFIG *
     ******************************************/
    public static int FWV_FIXINTERVALOFVALAKAS;
    public static int FWV_RANDOMINTERVALOFVALAKAS;
    public static int FWV_APPTIMEOFVALAKAS;
    public static int FWV_ACTIVITYTIMEOFVALAKAS;
    public static int FWV_CAPACITYOFLAIR;

    /*******************************************
     * Baylor CONFIG *
     *******************************************/
    public static boolean FWBA_ENABLESINGLEPLAYER;
    public static int FWBA_FIXINTERVALOFBAYLORSPAWN;
    public static int FWBA_RANDOMINTERVALOFBAYLORSPAWN;
    public static int FWBA_INTERVALOFNEXTMONSTER;
    public static int FWBA_ACTIVITYTIMEOFMOBS;

    /*******************************************
     * Frintezza CONFIG *
     *******************************************/
    public static int FWF_INTERVALOFNEXTMONSTER;
    public static int FWF_INTERVALOFFRINTEZZA;
    public static int FWF_ACTIVITYTIMEOFFRINTEZZA;
    public static int FWF_RANDOMINTERVALOFFRINTEZZA;

    /*******************************************
     * Sailren CONFIG *
     *******************************************/
    public static boolean FWS_ENABLESINGLEPLAYER;
    public static int FWS_FIXINTERVALOFSAILRENSPAWN;
    public static int FWS_RANDOMINTERVALOFSAILRENSPAWN;
    public static int FWS_INTERVALOFNEXTMONSTER;
    public static int FWS_ACTIVITYTIMEOFMOBS;


    /*******************************************
     * Spinozavr CONFIG *
     *******************************************/
    public static int SPINOZAVR_SPAWN;
    public static String[] SPINOZAVR_REWARD;
    /*******************************************
     * High Priestess van Halter CONFIG *
     *******************************************/
    public static int HPH_FIXINTERVALOFHALTER;
    public static int HPH_RANDOMINTERVALOFHALTER;
    public static int HPH_APPTIMEOFHALTER;
    public static int HPH_ACTIVITYTIMEOFHALTER;
    public static int HPH_FIGHTTIMEOFHALTER;
    public static int HPH_CALLROYALGUARDHELPERCOUNT;
    public static int HPH_CALLROYALGUARDHELPERINTERVAL;
    public static int HPH_INTERVALOFDOOROFALTER;
    public static int HPH_TIMEOFLOCKUPDOOROFALTAR;

    /***************************************************************************
     * Attack Last Imperial Tomb Custom CONFIG *
     **************************************************************************/
    public static int LIT_REGISTRATION_MODE;
    public static int LIT_REGISTRATION_TIME;
    public static int LIT_MIN_PARTY_CNT;
    public static int LIT_MAX_PARTY_CNT;
    public static int LIT_MIN_PLAYER_CNT;
    public static int LIT_MAX_PLAYER_CNT;
    public static int LIT_TIME_LIMIT;

    /***************************************************************************
     * Four Sepulchers CONFIG *
     **************************************************************************/
    public static int FS_TIME_ATTACK;
    public static int FS_TIME_COOLDOWN;
    public static int FS_TIME_ENTRY;
    public static int FS_TIME_WARMUP;
    public static int FS_PARTY_MEMBER_COUNT;

    public static double RATE_RAID_DEFENSE;
    public static double RATE_RAID_ATTACK;
    public static double RATE_EPIC_DEFENSE;
    public static double RATE_EPIC_ATTACK;

    /******************************************
     * Epidos CONFIG *
     ******************************************/
    public static int FWE_MIN_PLAYER_COUNT;
    public static int FWE_FIXINTERVALOFEPIDOS;
    public static int FWE_RANDOMINTERVALOFEPIDOS;
    public static int FWE_LIMITUNTILSLEEP;

    public static int FWE_INDEX_NEED_COUNT;
    public static int FWE_INDEX_KILL_OTHER_SPORE;
    public static int FWE_INDEX_KILL_SPORE;

    /******************************************
     * Beleth CONFIG *
     ******************************************/
    public static int BELETH_MIN_PLAYER_COUNT;
    public static int BELETH_FIXINTERVAL;
    public static int BELETH_RANDOMINTERVAL;
    public static int BELETH_LIMITUNTILSLEEP;
    public static int BELETH_APPTIME;
    public static boolean ENABLE_ANNOUNCE_BOSS;
    public static GArray<Integer> ANNOUNCE_BOSS_RESPAWN;

    public static void loadBossSettings() {
        try {
            final L2Properties bossSettings = new L2Properties(BOSS_FILE);

            /***************************************************************************
             * Antharas *
             **************************************************************************/
            FWA_FIXINTERVALOFANTHARAS = bossSettings.getInteger("FixIntervalOfAntharas", 11520);
            if (FWA_FIXINTERVALOFANTHARAS < 5 || FWA_FIXINTERVALOFANTHARAS > 20160)
                FWA_FIXINTERVALOFANTHARAS = 11520;
            FWA_FIXINTERVALOFANTHARAS *= 60000;

            FWA_RANDOMINTERVALOFANTHARAS = bossSettings.getInteger("RandomIntervalOfAntharas", 0);
            if (FWA_RANDOMINTERVALOFANTHARAS < 0 || FWA_RANDOMINTERVALOFANTHARAS > 20160)
                FWA_RANDOMINTERVALOFANTHARAS = 8640;
            FWA_RANDOMINTERVALOFANTHARAS *= 60000;

            FWA_APPTIMEOFANTHARAS = bossSettings.getInteger("AppTimeOfAntharas", 10);
            if (FWA_APPTIMEOFANTHARAS < 5 || FWA_APPTIMEOFANTHARAS > 60)
                FWA_APPTIMEOFANTHARAS = 10;
            FWA_APPTIMEOFANTHARAS *= 60000;

            FWA_ACTIVITYTIMEOFANTHARAS = bossSettings.getInteger("ActivityTimeOfAntharas", 120);
            if (FWA_ACTIVITYTIMEOFANTHARAS < 120 || FWA_ACTIVITYTIMEOFANTHARAS > 720)
                FWA_ACTIVITYTIMEOFANTHARAS = 120;
            FWA_ACTIVITYTIMEOFANTHARAS *= 60000;

            FWA_OLDANTHARAS = bossSettings.getBoolean("OldAntharas", false);
            FWA_LIMITOFWEAK = bossSettings.getInteger("LimitOfWeak", 299);

            FWA_LIMITOFNORMAL = bossSettings.getInteger("LimitOfNormal", 399);
            if (FWA_LIMITOFWEAK >= FWA_LIMITOFNORMAL)
                FWA_LIMITOFNORMAL = FWA_LIMITOFWEAK + 1;

            FWA_INTERVALOFBEHEMOTHONWEAK = bossSettings.getInteger("IntervalOfBehemothOnWeak", 8);
            if (FWA_INTERVALOFBEHEMOTHONWEAK < 1 || FWA_INTERVALOFBEHEMOTHONWEAK > 10)
                FWA_INTERVALOFBEHEMOTHONWEAK = 8;
            FWA_INTERVALOFBEHEMOTHONWEAK *= 60000;

            FWA_INTERVALOFBEHEMOTHONNORMAL = bossSettings.getInteger("IntervalOfBehemothOnNormal", 5);
            if (FWA_INTERVALOFBEHEMOTHONNORMAL < 1 || FWA_INTERVALOFBEHEMOTHONNORMAL > 10)
                FWA_INTERVALOFBEHEMOTHONNORMAL = 5;
            FWA_INTERVALOFBEHEMOTHONNORMAL *= 60000;

            FWA_INTERVALOFBEHEMOTHONSTRONG = bossSettings.getInteger("IntervalOfBehemothOnStrong", 3);
            if (FWA_INTERVALOFBEHEMOTHONSTRONG < 1 || FWA_INTERVALOFBEHEMOTHONSTRONG > 10)
                FWA_INTERVALOFBEHEMOTHONSTRONG = 3;
            FWA_INTERVALOFBEHEMOTHONSTRONG *= 60000;

            FWA_INTERVALOFBOMBERONWEAK = bossSettings.getInteger("IntervalOfBomberOnWeak", 6);
            if (FWA_INTERVALOFBOMBERONWEAK < 1 || FWA_INTERVALOFBOMBERONWEAK > 10)
                FWA_INTERVALOFBOMBERONWEAK = 6;
            FWA_INTERVALOFBOMBERONWEAK *= 60000;

            FWA_INTERVALOFBOMBERONNORMAL = bossSettings.getInteger("IntervalOfBomberOnNormal", 4);
            if (FWA_INTERVALOFBOMBERONNORMAL < 1 || FWA_INTERVALOFBOMBERONNORMAL > 10)
                FWA_INTERVALOFBOMBERONNORMAL = 4;
            FWA_INTERVALOFBOMBERONNORMAL *= 60000;

            FWA_INTERVALOFBOMBERONSTRONG = bossSettings.getInteger("IntervalOfBomberOnStrong", 3);
            if (FWA_INTERVALOFBOMBERONSTRONG < 1 || FWA_INTERVALOFBOMBERONSTRONG > 10)
                FWA_INTERVALOFBOMBERONSTRONG = 3;
            FWA_INTERVALOFBOMBERONSTRONG *= 60000;

            /***************************************************************************
             * Bauim *
             **************************************************************************/
            FWB_FIXINTERVALOFBAIUM = bossSettings.getInteger("FixIntervalOfBaium", 7200);
            if (FWB_FIXINTERVALOFBAIUM < 5 || FWB_FIXINTERVALOFBAIUM > 12960)
                FWB_FIXINTERVALOFBAIUM = 7200;
            FWB_FIXINTERVALOFBAIUM *= 60000;

            FWB_RANDOMINTERVALOFBAIUM = bossSettings.getInteger("RandomIntervalOfBaium", 0);
            if (FWB_RANDOMINTERVALOFBAIUM < 0 || FWB_RANDOMINTERVALOFBAIUM > 12960)
                FWB_RANDOMINTERVALOFBAIUM = 5760;
            FWB_RANDOMINTERVALOFBAIUM *= 60000;

            FWB_ACTIVITYTIMEOFBAIUM = bossSettings.getInteger("ActivityTimeOfBaium", 120);
            if (FWB_ACTIVITYTIMEOFBAIUM < 120 || FWB_ACTIVITYTIMEOFBAIUM > 720)
                FWB_ACTIVITYTIMEOFBAIUM = 120;
            FWB_ACTIVITYTIMEOFBAIUM *= 60000;

            FWB_LIMITUNTILSLEEP = bossSettings.getInteger("LimitUntilSleep", 30) * 60000;

            /***************************************************************************
             * Valakas *
             **************************************************************************/
            FWV_FIXINTERVALOFVALAKAS = bossSettings.getInteger("FixIntervalOfValakas", 11520);
            if (FWV_FIXINTERVALOFVALAKAS < 5 || FWV_FIXINTERVALOFVALAKAS > 20160)
                FWV_FIXINTERVALOFVALAKAS = 11520;
            FWV_FIXINTERVALOFVALAKAS *= 60000;

            FWV_RANDOMINTERVALOFVALAKAS = bossSettings.getInteger("RandomIntervalOfValakas", 0);
            if (FWV_RANDOMINTERVALOFVALAKAS < 0 || FWV_RANDOMINTERVALOFVALAKAS > 20160)
                FWV_RANDOMINTERVALOFVALAKAS = 8640;
            FWV_RANDOMINTERVALOFVALAKAS *= 60000;

            FWV_APPTIMEOFVALAKAS = bossSettings.getInteger("AppTimeOfValakas", 20);
            if (FWV_APPTIMEOFVALAKAS < 5 || FWV_APPTIMEOFVALAKAS > 60)
                FWV_APPTIMEOFVALAKAS = 10;
            FWV_APPTIMEOFVALAKAS *= 60000;

            FWV_ACTIVITYTIMEOFVALAKAS = bossSettings.getInteger("ActivityTimeOfValakas", 120);
            if (FWV_ACTIVITYTIMEOFVALAKAS < 120 || FWV_ACTIVITYTIMEOFVALAKAS > 720)
                FWV_ACTIVITYTIMEOFVALAKAS = 120;
            FWV_ACTIVITYTIMEOFVALAKAS *= 60000;

            FWV_CAPACITYOFLAIR = bossSettings.getInteger("CapacityOfLairOfValakas", 200);

            /***************************************************************************
             * Baylor *
             **************************************************************************/
            FWBA_ENABLESINGLEPLAYER = bossSettings.getBoolean("EnableSinglePlayerBaylor", false);

            FWBA_FIXINTERVALOFBAYLORSPAWN = bossSettings.getInteger("FixIntervalOfBaylorSpawn", 1440);
            if (FWBA_FIXINTERVALOFBAYLORSPAWN < 5 || FWBA_FIXINTERVALOFBAYLORSPAWN > 2880)
                FWBA_FIXINTERVALOFBAYLORSPAWN = 1440;
            FWBA_FIXINTERVALOFBAYLORSPAWN *= 60000;

            FWBA_RANDOMINTERVALOFBAYLORSPAWN = bossSettings.getInteger("RandomIntervalOfBaylorSpawn", 0);
            if (FWBA_RANDOMINTERVALOFBAYLORSPAWN < 0 || FWBA_RANDOMINTERVALOFBAYLORSPAWN > 2880)
                FWBA_RANDOMINTERVALOFBAYLORSPAWN = 1440;
            FWBA_RANDOMINTERVALOFBAYLORSPAWN *= 60000;

            FWBA_INTERVALOFNEXTMONSTER = bossSettings.getInteger("IntervalOfNextMonsterBaylor", 1);
            if (FWBA_INTERVALOFNEXTMONSTER < 1 || FWBA_INTERVALOFNEXTMONSTER > 10)
                FWBA_INTERVALOFNEXTMONSTER = 1;
            FWBA_INTERVALOFNEXTMONSTER *= 60000;

            FWBA_ACTIVITYTIMEOFMOBS = bossSettings.getInteger("ActivityTimeOfMobsBaylor", 120);
            if (FWBA_ACTIVITYTIMEOFMOBS < 1 || FWBA_ACTIVITYTIMEOFMOBS > 120)
                FWS_ACTIVITYTIMEOFMOBS = 120;
            FWBA_ACTIVITYTIMEOFMOBS *= 60000;

            /***************************************************************************
             * Frintezza *
             **************************************************************************/
            FWF_INTERVALOFFRINTEZZA = bossSettings.getInteger("IntervalOfFrintezzaSpawn", 2880);
            if (FWF_INTERVALOFFRINTEZZA < 5 || FWF_INTERVALOFFRINTEZZA > 5760)
                FWF_INTERVALOFFRINTEZZA = 2880;
            FWF_INTERVALOFFRINTEZZA *= 60000;

            FWF_RANDOMINTERVALOFFRINTEZZA = bossSettings.getInteger("RandomIntervalOfFrintezzaSpawn", 0);
            if (FWF_RANDOMINTERVALOFFRINTEZZA < 0 || FWF_RANDOMINTERVALOFFRINTEZZA > 5760)
                FWF_RANDOMINTERVALOFFRINTEZZA = 1440;
            FWF_RANDOMINTERVALOFFRINTEZZA *= 60000;

            FWF_INTERVALOFNEXTMONSTER = bossSettings.getInteger("IntervalOfNextMonsterFrintezza", 1);
            if (FWF_INTERVALOFNEXTMONSTER < 1 || FWF_INTERVALOFNEXTMONSTER > 10)
                FWF_INTERVALOFNEXTMONSTER = 1;
            FWF_INTERVALOFNEXTMONSTER *= 60000;

            FWF_ACTIVITYTIMEOFFRINTEZZA = bossSettings.getInteger("ActivityTimeOfMobsFrintezza", 120);
            if (FWF_ACTIVITYTIMEOFFRINTEZZA < 120 || FWF_ACTIVITYTIMEOFFRINTEZZA > 240)
                FWF_ACTIVITYTIMEOFFRINTEZZA = 120;
            FWF_ACTIVITYTIMEOFFRINTEZZA *= 60000;

            /***************************************************************************
             * Sailren *
             **************************************************************************/
            FWS_ENABLESINGLEPLAYER = bossSettings.getBoolean("EnableSinglePlayer", false);

            FWS_FIXINTERVALOFSAILRENSPAWN = bossSettings.getInteger("FixIntervalOfSailrenSpawn", 1440);
            if (FWS_FIXINTERVALOFSAILRENSPAWN < 5 || FWS_FIXINTERVALOFSAILRENSPAWN > 2880)
                FWS_FIXINTERVALOFSAILRENSPAWN = 1440;
            FWS_FIXINTERVALOFSAILRENSPAWN *= 60000;

            FWS_RANDOMINTERVALOFSAILRENSPAWN = bossSettings.getInteger("RandomIntervalOfSailrenSpawn", 0);
            if (FWS_RANDOMINTERVALOFSAILRENSPAWN < 0 || FWS_RANDOMINTERVALOFSAILRENSPAWN > 2880)
                FWS_RANDOMINTERVALOFSAILRENSPAWN = 1440;
            FWS_RANDOMINTERVALOFSAILRENSPAWN *= 60000;

            FWS_INTERVALOFNEXTMONSTER = bossSettings.getInteger("IntervalOfNextMonster", 1);
            if (FWS_INTERVALOFNEXTMONSTER < 1 || FWS_INTERVALOFNEXTMONSTER > 10)
                FWS_INTERVALOFNEXTMONSTER = 1;
            FWS_INTERVALOFNEXTMONSTER *= 60000;

            FWS_ACTIVITYTIMEOFMOBS = bossSettings.getInteger("ActivityTimeOfMobs", 120);
            if (FWS_ACTIVITYTIMEOFMOBS < 1 || FWS_ACTIVITYTIMEOFMOBS > 120)
                FWS_ACTIVITYTIMEOFMOBS = 120;
            FWS_ACTIVITYTIMEOFMOBS *= 60000;


            /*******************************************
             * Spinozavr CONFIG *
             *******************************************/
            SPINOZAVR_SPAWN = bossSettings.getInteger("FixIntervalOfSpinozavrSpawn", 5);
            SPINOZAVR_REWARD = bossSettings.getStringArray("SpinozavrReward");

            /***************************************************************************
             * High Priestess van Halter *
             **************************************************************************/
            HPH_FIXINTERVALOFHALTER = bossSettings.getInteger("FixIntervalOfHalter", 172800);

            if (HPH_FIXINTERVALOFHALTER < 300 || HPH_FIXINTERVALOFHALTER > 864000)
                HPH_FIXINTERVALOFHALTER = 172800;
            HPH_FIXINTERVALOFHALTER *= 6000;

            HPH_RANDOMINTERVALOFHALTER = bossSettings.getInteger("RandomIntervalOfHalter", 86400);
            if (HPH_RANDOMINTERVALOFHALTER < 300 || HPH_RANDOMINTERVALOFHALTER > 864000)
                HPH_RANDOMINTERVALOFHALTER = 86400;
            HPH_RANDOMINTERVALOFHALTER *= 6000; // TODO исправить

            HPH_APPTIMEOFHALTER = bossSettings.getInteger("AppTimeOfHalter", 20);
            if (HPH_APPTIMEOFHALTER < 5 || HPH_APPTIMEOFHALTER > 60)
                HPH_APPTIMEOFHALTER = 20;
            HPH_APPTIMEOFHALTER *= 6000;

            HPH_ACTIVITYTIMEOFHALTER = bossSettings.getInteger("ActivityTimeOfHalter", 21600);
            if (HPH_ACTIVITYTIMEOFHALTER < 7200 || HPH_ACTIVITYTIMEOFHALTER > 86400)
                HPH_ACTIVITYTIMEOFHALTER = 21600;
            HPH_ACTIVITYTIMEOFHALTER *= 1000;

            HPH_FIGHTTIMEOFHALTER = bossSettings.getInteger("FightTimeOfHalter", 7200);
            if (HPH_FIGHTTIMEOFHALTER < 7200 || HPH_FIGHTTIMEOFHALTER > 21600)
                HPH_FIGHTTIMEOFHALTER = 7200;
            HPH_FIGHTTIMEOFHALTER *= 6000;

            HPH_CALLROYALGUARDHELPERCOUNT = bossSettings.getInteger("CallRoyalGuardHelperCount", 6);
            if (HPH_CALLROYALGUARDHELPERCOUNT < 1 || HPH_CALLROYALGUARDHELPERCOUNT > 6)
                HPH_CALLROYALGUARDHELPERCOUNT = 6;

            HPH_CALLROYALGUARDHELPERINTERVAL = bossSettings.getInteger("CallRoyalGuardHelperInterval", 10);
            if (HPH_CALLROYALGUARDHELPERINTERVAL < 1 || HPH_CALLROYALGUARDHELPERINTERVAL > 60)
                HPH_CALLROYALGUARDHELPERINTERVAL = 10;
            HPH_CALLROYALGUARDHELPERINTERVAL *= 6000;

            HPH_INTERVALOFDOOROFALTER = bossSettings.getInteger("IntervalOfDoorOfAlter", 5400);
            if (HPH_INTERVALOFDOOROFALTER < 60 || HPH_INTERVALOFDOOROFALTER > 5400)
                HPH_INTERVALOFDOOROFALTER = 5400;
            HPH_INTERVALOFDOOROFALTER *= 6000;

            HPH_TIMEOFLOCKUPDOOROFALTAR = bossSettings.getInteger("TimeOfLockUpDoorOfAltar", 180);
            if (HPH_TIMEOFLOCKUPDOOROFALTAR < 60 || HPH_TIMEOFLOCKUPDOOROFALTAR > 600)
                HPH_TIMEOFLOCKUPDOOROFALTAR = 180;
            HPH_TIMEOFLOCKUPDOOROFALTAR *= 6000;

            /***************************************************************************
             * Last imperial tomb *
             **************************************************************************/
            LIT_REGISTRATION_MODE = bossSettings.getInteger("RegistrationMode", 0);
            LIT_REGISTRATION_TIME = bossSettings.getInteger("RegistrationTime", 10);
            LIT_MIN_PARTY_CNT = bossSettings.getInteger("MinPartyCount", 4);
            LIT_MAX_PARTY_CNT = bossSettings.getInteger("MaxPartyCount", 5);
            LIT_MIN_PLAYER_CNT = bossSettings.getInteger("MinPlayerCount", 7);
            LIT_MAX_PLAYER_CNT = bossSettings.getInteger("MaxPlayerCount", 45);
            LIT_TIME_LIMIT = bossSettings.getInteger("TimeLimit", 35);

            /***************************************************************************
             * Four Sepulchers CONFIG *
             **************************************************************************/
            FS_TIME_ATTACK = bossSettings.getInteger("FourSepulchersTimeOfAttack", 50);
            FS_TIME_COOLDOWN = bossSettings.getInteger("FourSepulchersTimeOfCoolDown", 5);
            FS_TIME_ENTRY = bossSettings.getInteger("FourSepulchersTimeOfEntry", 3);
            FS_TIME_WARMUP = bossSettings.getInteger("FourSepulchersTimeOfWarmUp", 2);
            FS_PARTY_MEMBER_COUNT = bossSettings.getInteger("FourSepulchersNumberOfNecessaryPartyMembers", 4);
            if (FS_TIME_ATTACK <= 0)
                FS_TIME_ATTACK = 50;
            if (FS_TIME_COOLDOWN <= 0)
                FS_TIME_COOLDOWN = 5;
            if (FS_TIME_ENTRY <= 0)
                FS_TIME_ENTRY = 3;
            if (FS_TIME_ENTRY <= 0)
                FS_TIME_ENTRY = 3;
            if (FS_TIME_ENTRY <= 0)
                FS_TIME_ENTRY = 3;

            /***************************************************************************
             * Epidos - Tower of Naia CONFIG *
             **************************************************************************/
            FWE_MIN_PLAYER_COUNT = bossSettings.getInteger("EpidosMinPlayerCount", 27);
            FWE_FIXINTERVALOFEPIDOS = bossSettings.getInteger("EpidosFixInterval", 2160) * 60 * 1000;
            FWE_RANDOMINTERVALOFEPIDOS = bossSettings.getInteger("EpidosRandomInterval", 1440) * 60 * 1000;
            FWE_LIMITUNTILSLEEP = bossSettings.getInteger("EpidosLimitUntilSleep", 10) * 60 * 1000;

            FWE_INDEX_NEED_COUNT = bossSettings.getInteger("IndexNeedCount", 120);
            FWE_INDEX_KILL_OTHER_SPORE = bossSettings.getInteger("IndexKillOtherSpore", 2);
            FWE_INDEX_KILL_SPORE = bossSettings.getInteger("IndexKillSpore", 1);

            /***************************************************************************
             * Beleth CONFIG *
             **************************************************************************/
            BELETH_MIN_PLAYER_COUNT = bossSettings.getInteger("BelethMinPlayerCount", 36);
            BELETH_FIXINTERVAL = bossSettings.getInteger("BelethFixInterval", 15840) * 60 * 1000;
            BELETH_RANDOMINTERVAL = bossSettings.getInteger("BelethRandomInterval", 4320) * 60 * 1000;
            BELETH_LIMITUNTILSLEEP = bossSettings.getInteger("BelethLimitUntilSleep", 30) * 60 * 1000;
            BELETH_APPTIME = bossSettings.getInteger("BelethAppTime", 10) * 60 * 1000;

            RATE_RAID_DEFENSE = bossSettings.getDouble("RateRaidDefense", 1);
            RATE_RAID_ATTACK = bossSettings.getDouble("RateRaidAttack", 1);
            RATE_EPIC_DEFENSE = bossSettings.getDouble("RateEpicDefense", RATE_RAID_DEFENSE);
            RATE_EPIC_ATTACK = bossSettings.getDouble("RateEpicAttack", RATE_RAID_ATTACK);
            ENABLE_ANNOUNCE_BOSS = bossSettings.getBoolean("EnableAnnounceBoss", false);
            if (ENABLE_ANNOUNCE_BOSS) {
                final String bossList = bossSettings.getProperty("AnnounceBossList", "");
                ANNOUNCE_BOSS_RESPAWN = new GArray<Integer>();
                for (final String id : bossList.trim().split(","))
                    ANNOUNCE_BOSS_RESPAWN.add(Integer.parseInt(id.trim()));
            }
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + BOSS_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String VITALITY_FILE = "./config/vitality.ini";

    /**
     * Конфиги системы виталити
     **/
    public static boolean ENABLE_VITALITY;
    public static boolean RECOVER_VITALITY_ON_RECONNECT;

    public static double RATE_DROP_VITALITY_HERBS;

    public static double RATE_VITALITY_LEVEL_1;
    public static double RATE_VITALITY_LEVEL_2;
    public static double RATE_VITALITY_LEVEL_3;
    public static double RATE_VITALITY_LEVEL_4;
    public static float RATE_RECOVERY_VITALITY_PEACE_ZONE;
    public static double RATE_VITALITY_LOST;
    public static double RATE_VITALITY_GAIN;
    public static double RATE_RECOVERY_ON_RECONNECT;

    public static int VITALITY_RAID_BONUS;

    // ========================================================================
    public static void loadVitalitySettings() {
        try {
            final L2Properties vitSettings = new L2Properties(VITALITY_FILE);

            ENABLE_VITALITY = vitSettings.getBoolean("EnableVitality", true);
            RECOVER_VITALITY_ON_RECONNECT = vitSettings.getBoolean("RecoverVitalityOnReconnect", true);
            RATE_DROP_VITALITY_HERBS = vitSettings.getDouble("RateVitalityHerbs", 5);
            RATE_VITALITY_LEVEL_1 = vitSettings.getDouble("RateVitalityLevel1", 1.5);
            RATE_VITALITY_LEVEL_2 = vitSettings.getDouble("RateVitalityLevel2", 2);
            RATE_VITALITY_LEVEL_3 = vitSettings.getDouble("RateVitalityLevel3", 2.5);
            RATE_VITALITY_LEVEL_4 = vitSettings.getDouble("RateVitalityLevel4", 3);
            RATE_RECOVERY_VITALITY_PEACE_ZONE = vitSettings.getFloat("RateRecoveryPeaceZone", 1);
            RATE_RECOVERY_ON_RECONNECT = vitSettings.getDouble("RateRecoveryOnReconnect", 4);
            RATE_VITALITY_LOST = vitSettings.getDouble("RateVitalityLost", 1);
            RATE_VITALITY_GAIN = vitSettings.getDouble("RateVitalityGain", 1);

            VITALITY_RAID_BONUS = vitSettings.getInteger("VitalityRaidBonus", 1000);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + VITALITY_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String PCCAFE_CONFIGURATION_FILE = "./config/pccafe_system.ini";
    // ========================================================================

    public static boolean PCCAFE_ENABLED;
    public static boolean PCCAFE_IPCHECK_ADDRESSES;

    public static boolean PCCAFE_ACQUISITION_POINTS_RANDOM;
    public static boolean PCCAFE_ENABLE_DOUBLE_ACQUISITION_POINTS;
    public static int PCCAFE_DOUBLE_ACQUISITION_POINTS_CHANCE;
    public static double PCCAFE_ACQUISITION_POINTS_RATE;

    public static String[] PCCAFE_IPS;
    public static String[] PCCAFE_NICKNAMES;
    public static boolean PCCAFE_CHECK_NICKNAMES;

    public static void loadPcCafeConfig() {
        try {
            final L2Properties pccaffeSettings = new L2Properties(PCCAFE_CONFIGURATION_FILE);

            PCCAFE_ENABLED = pccaffeSettings.getBoolean("Enabled", false);
            PCCAFE_IPCHECK_ADDRESSES = pccaffeSettings.getBoolean("PcCafeIpCheckAddresses", true);
            PCCAFE_ACQUISITION_POINTS_RANDOM = pccaffeSettings.getBoolean("AcquisitionPointsRandom", false);
            PCCAFE_ENABLE_DOUBLE_ACQUISITION_POINTS = pccaffeSettings.getBoolean("DoublingAcquisitionPoints", false);
            PCCAFE_DOUBLE_ACQUISITION_POINTS_CHANCE = pccaffeSettings.getInteger("DoublingAcquisitionPointsChance", 1);
            PCCAFE_ACQUISITION_POINTS_RATE = pccaffeSettings.getDouble("AcquisitionPointsRate", 1);

            PCCAFE_IPS = pccaffeSettings.getProperty("PcCafeIpAddresses", "127.0.0.1").split(";");
            PCCAFE_NICKNAMES = pccaffeSettings.getProperty("PcCafeNickNames", ";").split(";");
            PCCAFE_CHECK_NICKNAMES = pccaffeSettings.getBoolean("PcCafeCheckNickNames", false);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + PCCAFE_CONFIGURATION_FILE + " File.");
        }
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String COMMUNITY_BOARD_CONFIG_FILE = "./config/community_board.ini";

    /**
     * Community Board
     */
    public static boolean ALLOW_COMMUNITYBOARD;
    public static String BBS_HOME_DIR;
    public static boolean REGION_PAGE_DISABLE;
    public static boolean MEMO_PAGE_DISABLE;
    public static boolean REGION_PAGE_SHOW_ONLINE;
    public static boolean REGION_PAGE_SHOW_PLAYERS;
    public static boolean COMMUNITYBOARDLOGGIN;
    public static String BBS_DEFAULT;
    public static int NAME_PAGE_SIZE_COMMUNITYBOARD;
    public static int NAME_PER_ROW_COMMUNITYBOARD;
    public static boolean SHOW_OFFLINE_TRADERS;
    public static boolean CHECK_DEATH_TIME;
    public static int CHECK_DEATH_TIME_VAL;
    public static boolean COMMUNITY_BOARD_ALLOW_BUFFER;
    public static boolean COMMUNITY_BOARD_ALLOW_SHOP;
    public static boolean COMMUNITY_BOARD_ALLOW_TELEPORT;
    public static boolean COMMUNITY_BOARD_ALLOW_ECHANT;

    /**
     * Settings of CommunityBoard Buffer
     */
    public static long COMMUNITY_BOARD_BUFFER_ALT_TIME;
    public static int COMMUNITY_BOARD_BUFFER_MAX_SKILLS_IN_SCHEME;
    public static int COMMUNITY_BOARD_BUFFER_PRICE_ITEM;
    public static int COMMUNITY_BOARD_BUFFER_PRICE_ONE;
    public static int COMMUNITY_BOARD_BUFFER_PRICE_MOD_GRP;
    public static int COMMUNITY_BOARD_BUFFER_MIN_LVL;
    public static int COMMUNITY_BOARD_BUFFER_MAX_LVL;
    public static boolean COMMUNITY_BOARD_BUFFER_ALLOW_SAVE_RESTOR;
    public static int[] COMMUNITY_BOARD_BUFFER_LVL_MOD;

    public static int COMMUNITY_BOARD_SHOP_LS_PRICE_ID;
    public static long COMMUNITY_BOARD_SHOP_LS_PRICE_COUNT;

    public static boolean COMMUNITY_BOARD_BUFFER_ALLOW_SIEGE;
    public static boolean COMMUNITY_BOARD_BUFFER_ALLOW_EVENT;
    public static boolean COMMUNITY_BOARD_BUFFER_ALLOW_INSTANCE;
    public static boolean COMMUNITY_BOARD_BUFFER_NO_IS_IN_PEACE;
    public static boolean COMMUNITY_BOARD_BUFFER_ALLOW_PK;
    public static boolean COMMUNITY_BOARD_BUFFER_ALLOW_FLAG;
    public static boolean COMMUNITY_BOARD_BUFFER_ALLOW_OTHER;
    public static boolean COMMUNITY_BOARD_BUFFER_ALLOW_OLYMPIAD;
    public static boolean COMMUNITY_BOARD_BUFFER_ALLOW_TRANSFORM;

    public static boolean COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF;
    public static boolean COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF_AFTER_DEATH;
    public static boolean COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF_EFFECT_EXIT;
    public static boolean COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF_ATTACK_STOP;

    /**
     * Settings of CommunityBoard Teleport
     */
    public static int COMMUNITY_BOARD_TELEPORT_ITEM;
    public static int COMMUNITY_BOARD_TELEPORT_PRICE;
    public static int COMMUNITY_BOARD_TELEPORT_COUNT;
    public static boolean COMMUNITY_BOARD_TELEPORT_IN_SIEGE_TOWN;
    public static boolean COMMUNITY_BOARD_TELEPORT_IN_SIEGE_ZONE;
    public static boolean COMMUNITY_BOARD_TELEPORT_FIGHT;
    public static boolean COMMUNITY_BOARD_TELEPORT_NO_IS_IN_PEACE;
    public static boolean COMMUNITY_BOARD_TELEPORT_ADDITIONAL_RULES;
    public static boolean COMMUNITY_BOARD_TELEPORT_ALLOW_INSTANCE;
    public static boolean COMMUNITY_BOARD_TELEPORT_ALLOW_PK;

    /**
     * Settings of CommunityBoard Enchant
     */
    public static int COMMUNITY_BOARD_ENCHANT_MAX;
    public static int COMMUNITY_BOARD_ENCH_ITEM;
    public static double[][] COMMUNITY_BOARD_ENCH_PRICE_WEAPON;
    public static double[][] COMMUNITY_BOARD_ENCH_PRICE_OTHER;
    public static double[][] COMMUNITY_BOARD_ENCH_PRICE_WEAPON_ATR;
    public static double[][] COMMUNITY_BOARD_ENCH_PRICE_OTHER_ATR;

    public static IntArrayList COMMUNITY_BOARD_AVAILABLE_MULTISELL;
    public static IntArrayList COMMUNITY_BOARD_DISABLED_MULTISELL;

    public static int COMMUNITY_BOARD_PREMIUM_ACCESS;

    public static int COMMUNITY_BOARD_RESTORE_CP_ITEMID;
    public static long COMMUNITY_BOARD_RESTORE_CP_ITEMCOUNT;
    public static int COMMUNITY_BOARD_RESTORE_HP_ITEMID;
    public static long COMMUNITY_BOARD_RESTORE_HP_ITEMCOUNT;
    public static int COMMUNITY_BOARD_RESTORE_MP_ITEMID;
    public static long COMMUNITY_BOARD_RESTORE_MP_ITEMCOUNT;
    public static int COMMUNITY_BOARD_RESTORE_CANCEL_ITEMID;
    public static long COMMUNITY_BOARD_RESTORE_CANCEL_ITEMCOUNT;
    public static boolean COMMUNITY_BOARD_RESTORE_CANCEL_POSITIVE;

    public static String COMMUNITY_BOARD_CLASS_MASTER_SETTINGS_LINE;
    public static ClassMasterSettings COMMUNITY_BOARD_CLASS_MASTER_SETTINGS;
    public static Pattern COMMUNITYBOARD_BYPASS_PATERN;

    // ========================================================================
    public static void loadCommunityBoardSettings() {
        try {
            final L2Properties communityBoardSettings = new L2Properties(COMMUNITY_BOARD_CONFIG_FILE);

            COMMUNITYBOARD_BYPASS_PATERN = Pattern.compile(communityBoardSettings.getProperty("CommunityBoardByPassPatern", "^(_bbsteleport|_bbsechant|_bbsbuff|_bbsmultisell|_bbsclass|_bbsscripts).*"), Pattern.DOTALL);

            ALLOW_COMMUNITYBOARD = communityBoardSettings.getBoolean("AllowCommunityBoard", true);
            BBS_HOME_DIR = communityBoardSettings.getProperty("BBSHomeDir", "data/html/CommunityBoardPVP/");
            REGION_PAGE_DISABLE = communityBoardSettings.getBoolean("PageRegionDisable", false);
            MEMO_PAGE_DISABLE = communityBoardSettings.getBoolean("MemoPageDisable", false);
            REGION_PAGE_SHOW_ONLINE = communityBoardSettings.getBoolean("PageRegionShowOnline", true);
            REGION_PAGE_SHOW_PLAYERS = communityBoardSettings.getBoolean("PageRegionShowPlayers", true);

            COMMUNITY_BOARD_SHOP_LS_PRICE_ID = communityBoardSettings.getInteger("CommunityLsShopitem", 57);
            COMMUNITY_BOARD_SHOP_LS_PRICE_COUNT = communityBoardSettings.getLong("CommunityLsShopitemcount", 100);
            COMMUNITYBOARDLOGGIN = communityBoardSettings.getBoolean("CommunityBoardLoggin", false);
            BBS_DEFAULT = communityBoardSettings.getProperty("BBSDefault", "_bbshome");
            NAME_PAGE_SIZE_COMMUNITYBOARD = communityBoardSettings.getInteger("NamePageSizeOnCommunityBoard", 50);
            NAME_PER_ROW_COMMUNITYBOARD = communityBoardSettings.getInteger("NamePerRowOnCommunityBoard", 5);
            SHOW_OFFLINE_TRADERS = communityBoardSettings.getBoolean("ShowOfflineTraders", true);

            COMMUNITY_BOARD_ALLOW_SHOP = communityBoardSettings.getBoolean("AllowShop", false);
            CHECK_DEATH_TIME = Boolean.parseBoolean(communityBoardSettings.getProperty("CheckDeathTime", "true"));
            CHECK_DEATH_TIME_VAL = (Integer.parseInt(communityBoardSettings.getProperty("CheckDeathTimeVal", "30")) * 1000);
            COMMUNITY_BOARD_ALLOW_BUFFER = communityBoardSettings.getBoolean("AllowBuffer", false);
            COMMUNITY_BOARD_BUFFER_MAX_SKILLS_IN_SCHEME = communityBoardSettings.getInteger("BufferMaxSkillsInScheme", 60);
            COMMUNITY_BOARD_BUFFER_ALT_TIME = communityBoardSettings.getLong("BufferTime", 14400000);
            COMMUNITY_BOARD_BUFFER_PRICE_ITEM = communityBoardSettings.getInteger("BufferPriceItem", 57);
            COMMUNITY_BOARD_BUFFER_PRICE_ONE = communityBoardSettings.getInteger("BufferPriceOne", 1000);
            COMMUNITY_BOARD_BUFFER_PRICE_MOD_GRP = communityBoardSettings.getInteger("BufferPriceModGrp", 10);
            COMMUNITY_BOARD_BUFFER_MIN_LVL = communityBoardSettings.getInteger("BufferMinLvl", 1);
            COMMUNITY_BOARD_BUFFER_MAX_LVL = communityBoardSettings.getInteger("BufferMaxLvl", 99);
            COMMUNITY_BOARD_BUFFER_ALLOW_SIEGE = communityBoardSettings.getBoolean("BufferAllowOnSiege", false);
            COMMUNITY_BOARD_BUFFER_ALLOW_EVENT = communityBoardSettings.getBoolean("BufferAllowOnEvent", false);
            COMMUNITY_BOARD_BUFFER_ALLOW_INSTANCE = communityBoardSettings.getBoolean("BufferAllowInInstance", false);
            COMMUNITY_BOARD_BUFFER_NO_IS_IN_PEACE = communityBoardSettings.getBoolean("BufferNoIsInPeace", false);
            COMMUNITY_BOARD_BUFFER_ALLOW_PK = communityBoardSettings.getBoolean("BufferAllowPk", false);
            COMMUNITY_BOARD_BUFFER_ALLOW_FLAG = communityBoardSettings.getBoolean("BufferAllowFlag", false);
            COMMUNITY_BOARD_BUFFER_ALLOW_OTHER = communityBoardSettings.getBoolean("BufferAllowOther", false);
            COMMUNITY_BOARD_BUFFER_ALLOW_OLYMPIAD = communityBoardSettings.getBoolean("BufferAllowInOlmpiad", false);
            COMMUNITY_BOARD_BUFFER_ALLOW_TRANSFORM = communityBoardSettings.getBoolean("BufferAllowInTransform", false);
            COMMUNITY_BOARD_BUFFER_ALLOW_SAVE_RESTOR = communityBoardSettings.getBoolean("BufferAllowSaveRestor", true);
            COMMUNITY_BOARD_BUFFER_LVL_MOD = communityBoardSettings.getIntArray("BufferLevelModificator", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

            COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF = communityBoardSettings.getBoolean("BufferAllowAutoBuff", false);
            COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF_AFTER_DEATH = communityBoardSettings.getBoolean("BufferAllowAutoBuffAfterDeath", false);
            COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF_EFFECT_EXIT = communityBoardSettings.getBoolean("BufferAllowAutoBuffEffectExit", false);
            COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF_ATTACK_STOP = communityBoardSettings.getBoolean("BufferAllowAutoBuffAttackStop", false);

            COMMUNITY_BOARD_ALLOW_TELEPORT = communityBoardSettings.getBoolean("AllowTeleport", false);
            COMMUNITY_BOARD_TELEPORT_ITEM = communityBoardSettings.getInteger("TeleportItem", 57);
            COMMUNITY_BOARD_TELEPORT_PRICE = communityBoardSettings.getInteger("TeleportPrice", 100000);
            COMMUNITY_BOARD_TELEPORT_COUNT = communityBoardSettings.getInteger("TeleportCount", 10);
            COMMUNITY_BOARD_TELEPORT_IN_SIEGE_TOWN = communityBoardSettings.getBoolean("TeleportInSiegeTown", false);
            COMMUNITY_BOARD_TELEPORT_IN_SIEGE_ZONE = communityBoardSettings.getBoolean("TeleportInSiegeZone", false);
            COMMUNITY_BOARD_TELEPORT_NO_IS_IN_PEACE = communityBoardSettings.getBoolean("TeleportNoIsInPeace", false);
            COMMUNITY_BOARD_TELEPORT_ALLOW_INSTANCE = communityBoardSettings.getBoolean("TeleportAllowInInstance", false);
            COMMUNITY_BOARD_TELEPORT_ALLOW_PK = communityBoardSettings.getBoolean("TeleportAllowPK", false);
            COMMUNITY_BOARD_TELEPORT_FIGHT = communityBoardSettings.getBoolean("TeleportFight", false);
            COMMUNITY_BOARD_TELEPORT_ADDITIONAL_RULES = communityBoardSettings.getBoolean("TeleportAdditionalRules", false);
            COMMUNITY_BOARD_ALLOW_ECHANT = communityBoardSettings.getBoolean("AllowEnchant", false);
            COMMUNITY_BOARD_ENCH_ITEM = communityBoardSettings.getInteger("EnchantPriceItem", 57);
            COMMUNITY_BOARD_ENCHANT_MAX = communityBoardSettings.getInteger("EnchantMax", 20);
            COMMUNITY_BOARD_ENCH_PRICE_WEAPON = getEnchantSettings(communityBoardSettings.getProperty("EnchantPriceWeapon", "2-1.1;4-1.2;6-1.3;8-1.4;10-1.5;12-1.6;14-1.7"));
            COMMUNITY_BOARD_ENCH_PRICE_OTHER = getEnchantSettings(communityBoardSettings.getProperty("EnchantPriceOther", "1-1.1;2-1.2;4-1.3;6-1.4;8-1.5;10-1.6;12-1.7"));
            COMMUNITY_BOARD_ENCH_PRICE_WEAPON_ATR = getEnchantSettings(communityBoardSettings.getProperty("EnchantPriceWeaponAtr", "10-1.5;12-1.6;14-1.7"));
            COMMUNITY_BOARD_ENCH_PRICE_OTHER_ATR = getEnchantSettings(communityBoardSettings.getProperty("EnchantPriceOtherAtr", "8-1.5;10-1.6;12-1.7"));

            COMMUNITY_BOARD_PREMIUM_ACCESS = communityBoardSettings.getInteger("PremiumAccessRule", AGGRO_CHECK_INTERVAL);

            COMMUNITY_BOARD_AVAILABLE_MULTISELL = new IntArrayList(communityBoardSettings.getIntArray("AllowMultisells"), false);
            COMMUNITY_BOARD_DISABLED_MULTISELL = new IntArrayList(communityBoardSettings.getIntArray("DisabledMultisellsWithoutPA"), false);

            final File dir = new File(Config.DATAPACK_ROOT, "data/multisell/CommunityBoard");
            if (dir.exists())
                for (final File f : dir.listFiles()) {
                    int id = 0;
                    try {
                        id = Integer.parseInt(f.getName().replaceAll(".xml", ""));
                    } catch (final Exception e) {
                    }

                    if (id > 0)
                        COMMUNITY_BOARD_AVAILABLE_MULTISELL.add(id);
                }
            COMMUNITY_BOARD_AVAILABLE_MULTISELL.sort();

            COMMUNITY_BOARD_RESTORE_CP_ITEMID = communityBoardSettings.getInteger("RestoreCpItemId", 57);
            COMMUNITY_BOARD_RESTORE_CP_ITEMCOUNT = communityBoardSettings.getLong("RestoreCpItemCount", 100000);
            COMMUNITY_BOARD_RESTORE_HP_ITEMID = communityBoardSettings.getInteger("RestoreHpItemId", 57);
            COMMUNITY_BOARD_RESTORE_HP_ITEMCOUNT = communityBoardSettings.getLong("RestoreHpItemCount", 100000);
            COMMUNITY_BOARD_RESTORE_MP_ITEMID = communityBoardSettings.getInteger("RestoreMpItemId", 57);
            COMMUNITY_BOARD_RESTORE_MP_ITEMCOUNT = communityBoardSettings.getLong("RestoreMpItemCount", 100000);
            COMMUNITY_BOARD_RESTORE_CANCEL_ITEMID = communityBoardSettings.getInteger("RestoreCancelItemId", 57);
            COMMUNITY_BOARD_RESTORE_CANCEL_ITEMCOUNT = communityBoardSettings.getLong("RestoreCancelItemCount", 100000);
            COMMUNITY_BOARD_RESTORE_CANCEL_POSITIVE = communityBoardSettings.getBoolean("RestoreCancelOnlyPositive", false);

            if (!communityBoardSettings.getProperty("CommunityBoardConfigClassMaster").trim().equalsIgnoreCase("false")) {
                COMMUNITY_BOARD_CLASS_MASTER_SETTINGS_LINE = communityBoardSettings.getProperty("CommunityBoardConfigClassMaster");
                COMMUNITY_BOARD_CLASS_MASTER_SETTINGS = new ClassMasterSettings(COMMUNITY_BOARD_CLASS_MASTER_SETTINGS_LINE);
            } else
                COMMUNITY_BOARD_CLASS_MASTER_SETTINGS_LINE = "false"; // иначе останется null, и вуаля, NPE в SpawnTable.java:105
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + COMMUNITY_BOARD_CONFIG_FILE + " File.");
        }
    }

    private static double[][] getEnchantSettings(final String param) {
        final String[] temp = param.split(";");
        final double[][] settings = new double[temp.length][2];
        for (int i = 0; i < temp.length; i++)
            settings[i] = ArrayUtil.toDoubleArray(temp[i], "-");
        return settings;
    }

    // *******************************************************************************************

    // ========================================================================
    public static final String GEODATA_CONFIG_FILE = "./config/geodata.ini";
    public static final String FAKE_PLAYERS_LIST = "./config/fake_players.ini";

    /**
     * Debug, only for GM
     */
    public static boolean GEODATA_DEBUG;
    public static boolean PATHFIND_DEBUG;
    public static boolean GEODATA_ENABLED;

    public static boolean PATH_CLEAN;

    public static boolean ALLOW_DOORS;
    public static boolean ALLOW_FALL_FROM_WALLS;
    public static boolean ALLOW_KEYBOARD_MOVE;
    public static boolean COMPACT_MODE;

    public static boolean PATHFIND_DIAGONAL;
    public static int PATHFIND_BOOST;
    public static long PATHFIND_MAX_TIME;

    public static int MAX_Z_DIFF;
    public static int MIN_LAYER_HEIGHT;
    public static int PATHFIND_MAX_Z_DIFF;
    public static float WEIGHT0;
    public static float WEIGHT1;
    public static float WEIGHT2;

    public static String PATH_FIND_BUFFERS;
    public static String GEOFILES_PATTERN;
    public static String VERTICAL_SPLIT_REGIONS;

    public static int GEO_X_FIRST;
    public static int GEO_Y_FIRST;
    public static int GEO_X_LAST;
    public static int GEO_Y_LAST;
    public static int DIV_BY;
    public static int DIV_BY_FOR_Z;

    public static int VIEW_OFFSET;

    public static boolean DELAYED_SPAWN;

    // ========================================================================
    public static void loadGeoConfig() {
        try {
            final L2Properties geoDataSettings = new L2Properties(GEODATA_CONFIG_FILE);

            GEODATA_DEBUG = geoDataSettings.getBoolean("GeodataDebug", false);
            PATHFIND_DEBUG = geoDataSettings.getBoolean("PathfindDebug", false);
            GEODATA_ENABLED = geoDataSettings.getBoolean("GeodataEnabled", true);
            ALLOW_KEYBOARD_MOVE = geoDataSettings.getBoolean("AllowMoveWithKeyboard", true);
            ALLOW_FALL_FROM_WALLS = geoDataSettings.getBoolean("AllowFallFromWalls", false);
            GEO_X_FIRST = geoDataSettings.getInteger("GeoFirstX", 11);
            GEO_X_LAST = geoDataSettings.getInteger("GeoLastX", 26);

            GEO_Y_FIRST = geoDataSettings.getInteger("GeoFirstY", 10);
            GEO_Y_LAST = geoDataSettings.getInteger("GeoLastY", 26);

            DIV_BY = geoDataSettings.getInteger("DivBy", 2048);
            DIV_BY_FOR_Z = geoDataSettings.getInteger("DivByForZ", 1024);

            PATH_CLEAN = geoDataSettings.getBoolean("PathClean", true);
            ALLOW_DOORS = geoDataSettings.getBoolean("AllowDoors", true);

            COMPACT_MODE = geoDataSettings.getBoolean("CompactMode", false);
            PATHFIND_BOOST = geoDataSettings.getInteger("PathFindBoost", 2);

            MAX_Z_DIFF = geoDataSettings.getInteger("MaxZDiff", 64);
            PATHFIND_MAX_Z_DIFF = geoDataSettings.getInteger("PathFindMaxZDiff", 32);
            WEIGHT0 = geoDataSettings.getFloat("Weight0", 1f);
            WEIGHT1 = geoDataSettings.getFloat("Weight1", 3f);
            WEIGHT2 = geoDataSettings.getFloat("Weight2", 2f);

            PATHFIND_DIAGONAL = geoDataSettings.getBoolean("PathFindDiagonal", true);

            PATH_FIND_BUFFERS = geoDataSettings.getProperty("PathFindBuffers", "8x100;8x128;8x192;4x256;2x320;2x384;1x500");
            PATHFIND_MAX_TIME = geoDataSettings.getLong("PathFindMaxTime", 10000000);

            GEOFILES_PATTERN = geoDataSettings.getProperty("GeoFilesPattern", "(\\d{2}_\\d{2})\\.l2j");

            MIN_LAYER_HEIGHT = geoDataSettings.getInteger("MinLayerHeight", 64);
            VIEW_OFFSET = geoDataSettings.getInteger("ViewOffset", 1);
            CLIENT_Z_SHIFT = geoDataSettings.getInteger("ClientZShift", 16);

            VERTICAL_SPLIT_REGIONS = geoDataSettings.getProperty("VerticalSplitRegions", "20_21;22_16;22_25;23_18;25_14;25_15;25_19;24_23");

            DELAYED_SPAWN = geoDataSettings.getBoolean("DelayedSpawn", false);

        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + GEODATA_CONFIG_FILE + " File.");
        }
    }

    // *******************************************************************************************

    /**
     * Chance that an item will succesfully be enchanted
     */
    public static int ENCHANT_CHANCE_WEAPON;
    public static int ENCHANT_CHANCE_ARMOR;
    public static int ENCHANT_CHANCE_ACCESSORY;
    public static int ENCHANT_FAIL_TYPE;
    public static int ENCHANT_FAIL_ITEM_ID;
    public static int ENCHANT_FAIL_ITEM_COUNT;
    public static int ENCHANT_FAIL_LEVEL;

    public static int ENCHANT_CHANCE_CRYSTAL_WEAPON;
    public static int ENCHANT_CHANCE_CRYSTAL_ARMOR;
    public static int ENCHANT_CHANCE_CRYSTAL_ACCESSORY;
    public static int ENCHANT_FAIL_CRYSTAL_TYPE;
    public static int ENCHANT_FAIL_CRYSTAL_ITEM_ID;
    public static int ENCHANT_FAIL_CRYSTAL_ITEM_COUNT;
    public static int ENCHANT_FAIL_CRYSTAL_LEVEL;

    public static int ENCHANT_CHANCE_BLESSED_WEAPON;
    public static int ENCHANT_CHANCE_BLESSED_ARMOR;
    public static int ENCHANT_CHANCE_BLESSED_ACCESSORY;
    public static int ENCHANT_FAIL_BLESSED_TYPE;
    public static int ENCHANT_FAIL_BLESSED_ITEM_ID;
    public static int ENCHANT_FAIL_BLESSED_ITEM_COUNT;
    public static int ENCHANT_FAIL_BLESSED_LEVEL;

    public static int ENCHANT_MAX_WEAPON;
    public static int ENCHANT_MAX_ACCESSORY;
    public static int ENCHANT_MAX_ARMOR;

    public static int ENCHANT_ATTRIBUTE_CHANCE;
    public static int ENCHANT_ATTRIBUTE_FIRST_WEAPON_BONUS;
    public static int ENCHANT_ATTRIBUTE_WEAPON_BONUS;
    public static int ENCHANT_ATTRIBUTE_ARMOR_BONUS;

    public static double ENCHANT_MODIFER;

    /**
     * Config Alt Enchant Formula
     */
    public static EnchantFormulaType ENCHANT_FORMULA_TYPE;
    public static int ALT_ENCHANT_CHANCE_W;
    public static int ALT_ENCHANT_CHANCE_MAGE_W;
    public static int ALT_ENCHANT_CHANCE_ARMOR;
    public static int PENALTY_TO_TWOHANDED_BLUNTS;
    /**
     * блять
     */
    public static double[] ALT_FIX_ENCHANT_CHANCE_WEAPON;
    public static double[] ALT_FIX_ENCHANT_CHANCE_ARMOR;
    public static double[] ALT_FIX_ENCHANT_CHANCE_ARMOR_FULLBODY;

    /**
     * Enchant Config
     **/
    public static int SAFE_ENCHANT_COMMON;
    public static int SAFE_ENCHANT_FULL_BODY;
    public static boolean ENCHANT_BRACELETITEM;
    public static boolean ENCHANT_CLOAKITEM;
    public static boolean ENCHANT_BELTITEM;
    public static boolean AUGMENTATION_CAN_PVP;
    public static boolean ENCHANTELEMENT_CAN_PVP;
    public static boolean ALT_ALLOW_AUGMENT_ALL;
    public static boolean ALT_ALLOW_DROP_AUGMENTED;
    public static int ARMOR_OVERENCHANT_HPBONUS_LIMIT;

    // ========================================================================
    public static final String ENCHANT_CONFIG_FILE = "./config/enchant.ini";

    // ========================================================================
    public static void loadEnchantSettings() {
        try {
            final L2Properties enchantSettings = new L2Properties(ENCHANT_CONFIG_FILE);

            /* Alt Enchant Formula */
            ENCHANT_FORMULA_TYPE = EnchantFormulaType.valueOf(enchantSettings.getProperty("EnchantFormulaType", "normal").toUpperCase());
            ALT_ENCHANT_CHANCE_W = enchantSettings.getInteger("AltEnchantChanceWeapon", 80);
            ALT_ENCHANT_CHANCE_MAGE_W = enchantSettings.getInteger("AltEnchantChanceMageWeapon", 80);
            ALT_ENCHANT_CHANCE_ARMOR = enchantSettings.getInteger("AltEnchantChanceArmor", 80);
            PENALTY_TO_TWOHANDED_BLUNTS = enchantSettings.getInteger("PenaltyforEChanceToHandBlunt", 18);
            AUGMENTATION_CAN_PVP = Boolean.parseBoolean(enchantSettings.getProperty("AugmentationCanPvP", "false"));
            ENCHANTELEMENT_CAN_PVP = Boolean.parseBoolean(enchantSettings.getProperty("EnchantElementCanPvP", "false"));
            ALT_ALLOW_AUGMENT_ALL = enchantSettings.getBoolean("AugmentAll", false);
            ALT_ALLOW_DROP_AUGMENTED = enchantSettings.getBoolean("AlowDropAugmented", false);
            ALT_FIX_ENCHANT_CHANCE_WEAPON = enchantSettings.getDoubleArray("AltFixChanceWeapon");
            ALT_FIX_ENCHANT_CHANCE_ARMOR = enchantSettings.getDoubleArray("AltFixChanceArmor");
            ALT_FIX_ENCHANT_CHANCE_ARMOR_FULLBODY = enchantSettings.getDoubleArray("AltFixChanceArmorFullBody");

            /* chance to enchant an item over safe level */
            ENCHANT_CHANCE_WEAPON = enchantSettings.getInteger("EnchantChanceWeapon", 68);
            ENCHANT_CHANCE_ARMOR = enchantSettings.getInteger("EnchantChanceArmor", 52);
            ENCHANT_CHANCE_ACCESSORY = enchantSettings.getInteger("EnchantChanceAccessory", 54);
            ENCHANT_FAIL_TYPE = enchantSettings.getInteger("EnchantFailType", 1);
            ENCHANT_FAIL_ITEM_ID = enchantSettings.getInteger("EnchantFailItemId", 4037);
            ENCHANT_FAIL_ITEM_COUNT = enchantSettings.getInteger("EnchantFailItemCount", 10);
            ENCHANT_FAIL_LEVEL = enchantSettings.getInteger("EnchantFailLevel", 0);

            ENCHANT_CHANCE_CRYSTAL_WEAPON = enchantSettings.getInteger("EnchantChanceCrystalWeapon", 68);
            ENCHANT_CHANCE_CRYSTAL_ARMOR = enchantSettings.getInteger("EnchantChanceCrystalArmor", 52);
            ENCHANT_CHANCE_CRYSTAL_ACCESSORY = enchantSettings.getInteger("EnchantChanceCrystalAccessory", 54);
            ENCHANT_FAIL_CRYSTAL_TYPE = enchantSettings.getInteger("EnchantFailCrystalType", 1);
            ENCHANT_FAIL_CRYSTAL_ITEM_ID = enchantSettings.getInteger("EnchantFailCrystalItemId", 4037);
            ENCHANT_FAIL_CRYSTAL_ITEM_COUNT = enchantSettings.getInteger("EnchantFailCrystalItemCount", 10);
            ENCHANT_FAIL_CRYSTAL_LEVEL = enchantSettings.getInteger("EnchantFailCrystalLevel", 0);

            ENCHANT_CHANCE_BLESSED_WEAPON = enchantSettings.getInteger("EnchantChanceBlessedWeapon", 68);
            ENCHANT_CHANCE_BLESSED_ARMOR = enchantSettings.getInteger("EnchantChanceBlessedArmor", 52);
            ENCHANT_CHANCE_BLESSED_ACCESSORY = enchantSettings.getInteger("EnchantChanceBlessedAccessory", 54);
            ENCHANT_FAIL_BLESSED_TYPE = enchantSettings.getInteger("EnchantFailBlessedType", 2);
            ENCHANT_FAIL_BLESSED_ITEM_ID = enchantSettings.getInteger("EnchantFailBlessedItemId", 4037);
            ENCHANT_FAIL_BLESSED_ITEM_COUNT = enchantSettings.getInteger("EnchantFailBlessedItemCount", 10);
            ENCHANT_FAIL_BLESSED_LEVEL = enchantSettings.getInteger("EnchantFailBlessedLevel", 0);

            SAFE_ENCHANT_COMMON = enchantSettings.getInteger("SafeEnchantCommon", 3);
            SAFE_ENCHANT_FULL_BODY = enchantSettings.getInteger("SafeEnchantFullBody", 4);
            ENCHANT_BRACELETITEM = enchantSettings.getBoolean("EnchantBraceletItem", false);
            ENCHANT_CLOAKITEM = enchantSettings.getBoolean("EnchantCloakItem", false);
            ENCHANT_BELTITEM = enchantSettings.getBoolean("EnchantBeltItem", false);

            if (ENCHANT_FORMULA_TYPE == EnchantFormulaType.FIXED) {
                ENCHANT_MAX_WEAPON = ALT_FIX_ENCHANT_CHANCE_WEAPON.length;
                ENCHANT_MAX_ARMOR = ALT_FIX_ENCHANT_CHANCE_ARMOR.length;
                ENCHANT_MAX_ACCESSORY = ALT_FIX_ENCHANT_CHANCE_ARMOR.length;
            } else {
                ENCHANT_MAX_WEAPON = enchantSettings.getInteger("EnchantMaxWeapon", 20);
                ENCHANT_MAX_ARMOR = enchantSettings.getInteger("EnchantMaxArmor", 20);
                ENCHANT_MAX_ACCESSORY = enchantSettings.getInteger("EnchantMaxAccessory", 20);
            }

            ENCHANT_MODIFER = enchantSettings.getDouble("EnchantModifer", 0);

            ENCHANT_ATTRIBUTE_CHANCE = enchantSettings.getInteger("EnchantAttributeChance", 50);
            ENCHANT_ATTRIBUTE_FIRST_WEAPON_BONUS = enchantSettings.getInteger("EnchantAttributeFirstWeaponBonus", 20);
            ENCHANT_ATTRIBUTE_WEAPON_BONUS = enchantSettings.getInteger("EnchantAttributeNextWeaponBonus", 5);
            ENCHANT_ATTRIBUTE_ARMOR_BONUS = enchantSettings.getInteger("EnchantAttributeArmorBonus", 6);

            ARMOR_OVERENCHANT_HPBONUS_LIMIT = enchantSettings.getInteger("ArmorOverEnchantHPBonusLimit", 10);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + ENCHANT_CONFIG_FILE + " File.");
        }
    }

    // *******************************************************************************************

    public static int SKILLS_CHANCE_BLOW_DMG_FRONT;
    public static int SKILLS_CHANCE_BLOW_DMG_SIDE;
    public static int SKILLS_CHANCE_BLOW_DMG_BEHIND;

    public static int SKILLS_CHANCE_LETHAL;

    public static double SKILLS_CHANCE_MOD;
    public static double SKILLS_CHANCE_MIN;
    public static double SKILLS_CHANCE_CAP;
    public static int SKILLS_CAST_TIME_MIN;

    public static boolean ATTIBUTE_SHOW_CALC;
    public static boolean SKILLS_SHOW_CHANCE;
    public static boolean SKILLS_SHOW_CHANCE_MONSTER;
    public static boolean ALT_DELETE_SA_BUFFS;
    public static boolean ALT_SHOW_REUSE_MSG;
    /**
     * Отключение книг для изучения скилов
     */
    public static boolean ALT_DISABLE_SPELLBOOKS;

    public static boolean ALT_DISABLE_ENCHANT_BOOKS;

    /**
     * Отключение яйца для изучения клан скилов
     */
    public static boolean ALT_DISABLE_EGGS;
    public static boolean ALT_ALL_PHYS_SKILLS_OVERHIT;
    public static float ALT_ABSORB_DAMAGE_MODIFIER;
    public static boolean AUTO_LEARN_SKILLS;
    public static int AUTO_LEARN_SKILLS_MAX_LEVEL;
    /**
     * Разрешены ли клановые скилы?
     **/
    public static boolean ALLOW_CLANSKILLS;

    /**
     * Разрешено ли изучение скилов трансформации без наличия выполненного квеста
     */
    public static boolean ALLOW_LEARN_TRANS_SKILLS_WO_QUEST;
    public static boolean ALLOW_LEARN_SUBCLASS_SKILLS_WO_ITEM;
    public static double BUFFTIME_MODIFIER;
    public static double SELF_BUFFTIME_MODIFIER;
    public static double SONGDANCETIME_MODIFIER;
    public static double SUMMON_BUFF_MODIFIER;
    public static int SUMMON_BUFF_TIME;
    public static int SUMMON_SET_BUFF_TYPE;
    public static boolean ALT_REMOVE_SKILLS_ON_DELEVEL;

    public static boolean SKILLS_ALLOW_DISPEL_DANSSONG;

    public static boolean ALT_SAVE_UNSAVEABLE;

    public static int TRANSFER_SKILL_RESET_ADENA_COST;

    public static GArray<Integer> UNAFFECTED_SKILL_LIST;
    public static boolean ENABLE_MODIFY_SKILL_DURATION;
    public static TIntIntHashMap SKILL_DURATION_LIST;

    public static int ALT_MDAM_PERFECT_SHILEDS_RATE;
    public static boolean ALT_MDAM_SHILEDS;

    public static boolean ALT_GAME_MAGICFAILURES;
    public static int ALT_MIN_LVLDEPEND_MAGIC_SUCCES;

    // ========================================================================
    public static final String SKILL_CONFIG_FILE = "./config/skills.ini";

    // ========================================================================
    public static void loadSkillsSettings() {
        try {
            final L2Properties skillsSettings = new L2Properties(SKILL_CONFIG_FILE);

            ALT_DISABLE_SPELLBOOKS = skillsSettings.getBoolean("AltDisableSpellbooks", false);
            ALT_DISABLE_ENCHANT_BOOKS = skillsSettings.getBoolean("AltDisableEnchantBooks", false);
            ALT_DISABLE_EGGS = skillsSettings.getBoolean("AltDisableEggs", false);
            SKILLS_CHANCE_MOD = skillsSettings.getDouble("SkillsChanceMod", 1);
            SKILLS_CHANCE_MIN = skillsSettings.getDouble("SkillsChanceMin", 5);
            SKILLS_CHANCE_CAP = skillsSettings.getDouble("SkillsChanceCap", 95);

            SKILLS_CAST_TIME_MIN = skillsSettings.getInteger("SkillsCastTimeMin", 333);
            ATTIBUTE_SHOW_CALC = skillsSettings.getBoolean("AttibutShowCalc", true);
            SKILLS_SHOW_CHANCE = skillsSettings.getBoolean("SkillsShowChance", true);
            SKILLS_SHOW_CHANCE_MONSTER = skillsSettings.getBoolean("SkillsShowChanceMonster", true);

            ALT_DELETE_SA_BUFFS = skillsSettings.getBoolean("AltDeleteSABuffs", false);
            ALT_SHOW_REUSE_MSG = skillsSettings.getBoolean("AltShowSkillReuseMessage", true);
            AUTO_LEARN_SKILLS = skillsSettings.getBoolean("AutoLearnSkills", false);
            AUTO_LEARN_SKILLS_MAX_LEVEL = skillsSettings.getInteger("AutoLearnSkillsMaxLevel", 85);
            ALLOW_CLANSKILLS = skillsSettings.getBoolean("AllowClanSkills", true);
            ALLOW_LEARN_TRANS_SKILLS_WO_QUEST = skillsSettings.getBoolean("AllowLearnTransSkillsWOQuest", false);
            ALLOW_LEARN_SUBCLASS_SKILLS_WO_ITEM = skillsSettings.getBoolean("AllowLearnSubClassSkillsWOItem", false);
            BUFFTIME_MODIFIER = skillsSettings.getDouble("BuffTimeModifier", 1);
            SELF_BUFFTIME_MODIFIER = skillsSettings.getDouble("SelfBuffTimeModifier", 1);
            SONGDANCETIME_MODIFIER = skillsSettings.getDouble("SongDanceTimeModifier", 1);
            SUMMON_BUFF_MODIFIER = skillsSettings.getDouble("SummonBuffModifier", 1);
            SUMMON_BUFF_TIME = skillsSettings.getInteger("SummonBuffTime", 1800000);
            SUMMON_SET_BUFF_TYPE = skillsSettings.getInteger("SummonBuffType", 1);
            ALT_REMOVE_SKILLS_ON_DELEVEL = skillsSettings.getBoolean("AltRemoveSkillsOnDelevel", true);
            ALT_ALL_PHYS_SKILLS_OVERHIT = skillsSettings.getBoolean("AltAllPhysSkillsOverhit", true);
            ALT_ABSORB_DAMAGE_MODIFIER = skillsSettings.getFloat("AbsorbDamageModifier", 1.0f);
            ALT_SAVE_UNSAVEABLE = skillsSettings.getBoolean("AltSaveUnsaveable", false);

            SKILLS_ALLOW_DISPEL_DANSSONG = skillsSettings.getBoolean("SkillsAllowDispelDansSong", false);

            TRANSFER_SKILL_RESET_ADENA_COST = skillsSettings.getInteger("TransferSkillResetAdenaCost", 10000000);

            ENABLE_MODIFY_SKILL_DURATION = skillsSettings.getBoolean("EnableModifySkillDuration", false);

            final String unaffected_skills = skillsSettings.getProperty("UnaffectedSkills");
            if (unaffected_skills.length() > 0) {
                UNAFFECTED_SKILL_LIST = new GArray<Integer>();
                for (final String id : unaffected_skills.trim().split(","))
                    UNAFFECTED_SKILL_LIST.add(Integer.parseInt(id.trim()));
            }

            if (ENABLE_MODIFY_SKILL_DURATION) {
                SKILL_DURATION_LIST = new TIntIntHashMap();

                final String[] propertySplit = skillsSettings.getProperty("SkillDurationList", "").split(";");
                for (final String skill : propertySplit) {
                    final String[] skillSplit = skill.split(",");
                    if (skillSplit.length != 2)
                        _log.warning("SkillDurationList: invalid config property -> SkillDurationList \"" + skill + "\"");
                    else
                        try {
                            SKILL_DURATION_LIST.put(Integer.valueOf(skillSplit[0]), Integer.valueOf(skillSplit[1]));
                        } catch (final NumberFormatException nfe) {
                            if (!skill.equals(""))
                                _log.warning("SkillDurationList: invalid config property -> SkillList \"" + skillSplit[0] + "\"" + skillSplit[1]);
                        }
                }
            }

            ALT_MDAM_PERFECT_SHILEDS_RATE = skillsSettings.getInteger("MagicPerfectShieldBlockRate", 5);
            ALT_MDAM_SHILEDS = skillsSettings.getBoolean("MagicShieldBlock", true);

            ALT_GAME_MAGICFAILURES = skillsSettings.getBoolean("MagicFailures", true);
            ALT_MIN_LVLDEPEND_MAGIC_SUCCES = skillsSettings.getInteger("MinLvlDependMagicSuccess", 5);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + SKILL_CONFIG_FILE + " File.");
        }
    }

    // *******************************************************************************************

    public static double CHAMPION_WEAK_CHANCE;
    public static double CHAMPION_STRONG_CHANCE;
    public static boolean CHAMPION_CAN_BE_AGGRO;
    public static boolean CHAMPION_CAN_BE_SOCIAL;
    public static boolean CHAMPION_CAN_BE_RAIDBOSS;
    public static final String CHAMPION_CONFIG_FILE = "./config/champion.ini";

    public static double[] CHAMPION_WEAK_MODIFIER;
    public static double[] CHAMPION_STRONG_MODIFIER;

    public static double CHAMPION_HP_REGEN;
    public static boolean CHAMPION_LETHAL_IMMUNE;
    public static boolean CHAMPION_FEAR_IMMUNE;
    public static boolean CHAMPION_PARALYZE_IMMUNE;

    public static byte CHAMPION_MIN_LEVEL;
    public static byte CHAMPION_MAX_LEVEL;

    // ========================================================================
    public static void loadChampionSettings() {
        try {
            final L2Properties championSettings = new L2Properties(CHAMPION_CONFIG_FILE);

            CHAMPION_WEAK_CHANCE = championSettings.getDouble("ChampionChanceWeak", 0);
            CHAMPION_STRONG_CHANCE = championSettings.getDouble("ChampionChanceStrong", 0);
            CHAMPION_CAN_BE_AGGRO = championSettings.getBoolean("ChampionAggro", false);
            CHAMPION_CAN_BE_SOCIAL = championSettings.getBoolean("ChampionSocial", false);
            CHAMPION_CAN_BE_RAIDBOSS = championSettings.getBoolean("ChampionRaidBoss", false);

            CHAMPION_HP_REGEN = championSettings.getDouble("ChampionHpRegen", 0);
            CHAMPION_LETHAL_IMMUNE = championSettings.getBoolean("ChampionLethalImmune", true);
            CHAMPION_FEAR_IMMUNE = championSettings.getBoolean("ChampionFearImmune", true);
            CHAMPION_PARALYZE_IMMUNE = championSettings.getBoolean("ChampionParalyzeImmune", true);

            CHAMPION_WEAK_MODIFIER = ArrayUtil.toDoubleArray(championSettings.getProperty("WeakChampionModifier", "3.0;3.0;1.0;1.0;1.0;1.0;1.0"), ";");
            CHAMPION_STRONG_MODIFIER = ArrayUtil.toDoubleArray(championSettings.getProperty("StrongChampionModifier", "5.0;5.0;1.0;1.0;1.0;1.0;1.0"), ";");

            CHAMPION_MIN_LEVEL = championSettings.getByte("ChampionMinLevel", 20);
            CHAMPION_MAX_LEVEL = championSettings.getByte("ChampionMaxLevel", 85);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + CHAMPION_CONFIG_FILE + " File.");
        }
    }

    // *******************************************************************************************

    public static boolean CAPTCHA_ENABLE;
    public static String CAPTCHA_TYPE;
    public static int CAPTCHA_TIME;
    public static String[] CAPTCHA_IMAGE_WORDS;

    public static boolean CAPTCHA_SHOW_PLAYERS_WITH_PA;
    public static boolean CAPTCHA_COMMAND_ENABLE;
    public static int REUSE_COMMAND_TIME;
    public static GArray<Integer> CAPTCHA_ANTIBOT_LIST;
    public static boolean CAPTCHA_ANTIBOT_ENABLE;
    public static int CAPTCHA_ANTIBOT_COUNT_40;
    public static int CAPTCHA_ANTIBOT_COUNT_60;
    public static int CAPTCHA_ANTIBOT_COUNT_75;
    public static int CAPTCHA_ANTIBOT_COUNT_79;
    public static int CAPTCHA_ANTIBOT_COUNT_83;
    public static int CAPTCHA_ANTIBOT_COUNT_85;
    public static int CAPTCHA_COUNT_ERROR;
    public static boolean CHAR_PASS_ENABLE;
    /** Ghost Engine **/
    /**
     * Define either actions should be logged or not.e
     **/
    public static boolean GHOST_ENGINE_LOG_BOTS_DETECTIONS_AND_PUNISHMENTS;
    /**
     * Fake monsters disapears after 90s.e
     **/
    public static int GHOST_ENGINE_INVISIBLE_NPC_EXPIRATION_TIME;
    /**
     * One fake monster per 15 minute maximum allowed.e
     **/
    public static int GHOST_ENGINE_MINIMUM_INTERVAL_BETWEEN_FAKE_CHECKS;
    /**
     * Minimum level from which chances to check for possible bot will greatly increase.e
     **/
    public static int GHOST_ENGINE_MINIMUM_LEVEL_TO_CHECK_OFTEN;
    /**
     * Defines the NPC TEMPLATE ID for the Rabbit.e
     **/
    public static int GHOST_ENGINE_RABBIT_TEMPLATE_ID;
    public static boolean GHOST_ENGINE_ANTIBOT_ALLOW_BAN;
    public static boolean GHOST_ENGINE_DEVELOPMENT_MODE;

    public static boolean PROTECT_ENABLE;
    public static NetList PROTECT_UNPROTECTED_IPS;
    public static boolean PROTECT_GS_STORE_HWID;

    public static boolean PROTECT_GS_LOG_HWID;
    public static String PROTECT_GS_LOG_HWID_QUERY;

    public static boolean PROTECT_GS_ENABLE_HWID_BANS;
    public static String HWID_BANS_TABLE;

    public static boolean PROTECT_GS_ENABLE_HWID_BONUS;
    public static int PROTECT_GS_MAX_SAME_HWIDs;

    public static final String PROTECT_FILE = "./config/protection.ini";

    // ========================================================================
    public static void loadProtectionSettings() {
        try {
            final L2Properties protectionSettings = new L2Properties(PROTECT_FILE);

            CAPTCHA_ENABLE = protectionSettings.getBoolean("CaptchaEnable", false);
            CAPTCHA_TYPE = protectionSettings.getProperty("CaptchaType", "IMAGE");
            CAPTCHA_IMAGE_WORDS = protectionSettings.getStringArray("CaptchaImageWords", new String[]{"lineage2", "epilogue", "l2Re"});
            CAPTCHA_TIME = protectionSettings.getInteger("CaptchaTimeout", 40);
            CAPTCHA_SHOW_PLAYERS_WITH_PA = protectionSettings.getBoolean("CaptchaShowPlayersWithPA", true);
            CAPTCHA_ANTIBOT_ENABLE = protectionSettings.getBoolean("CaptchaAntibotEnable", false);
            CAPTCHA_ANTIBOT_COUNT_40 = protectionSettings.getInteger("CaptchaAntibotCount40", 100);
            CAPTCHA_ANTIBOT_COUNT_60 = protectionSettings.getInteger("CaptchaAntibotCount60", 200);
            CAPTCHA_ANTIBOT_COUNT_75 = protectionSettings.getInteger("CaptchaAntibotCount75", 300);
            CAPTCHA_ANTIBOT_COUNT_79 = protectionSettings.getInteger("CaptchaAntibotCount79", 400);
            CAPTCHA_ANTIBOT_COUNT_83 = protectionSettings.getInteger("CaptchaAntibotCount83", 500);
            CAPTCHA_ANTIBOT_COUNT_85 = protectionSettings.getInteger("CaptchaAntibotCount85", 600);
            CAPTCHA_COMMAND_ENABLE = protectionSettings.getBoolean("CaptchaCommandEnable", false);
            REUSE_COMMAND_TIME = protectionSettings.getInteger("ReuseCommandTime", 10);
            final String antibotList = protectionSettings.getProperty("CaptchaAntibotList");
            if (antibotList.length() > 0 && CAPTCHA_ANTIBOT_ENABLE) {
                CAPTCHA_ANTIBOT_LIST = new GArray<Integer>();
                for (final String id : antibotList.trim().split(","))
                    CAPTCHA_ANTIBOT_LIST.add(Integer.parseInt(id.trim()));
            }

            CAPTCHA_COUNT_ERROR = protectionSettings.getInteger("CaptchaCountError", 3);
            CHAR_PASS_ENABLE = protectionSettings.getBoolean("CharPassEnable", true);

            PROTOCOL_ENABLE_OBFUSCATION = protectionSettings.getBoolean("ProtocolEnableOpcodeObfuscation", false);

            GHOST_ENGINE_ANTIBOT_ALLOW_BAN = protectionSettings.getBoolean("GhostEngineAntibotAllowBan", false);
            GHOST_ENGINE_LOG_BOTS_DETECTIONS_AND_PUNISHMENTS = protectionSettings.getBoolean("GhostEngineLogBotsDetectionsAndPunishments", true);
            GHOST_ENGINE_INVISIBLE_NPC_EXPIRATION_TIME = protectionSettings.getInteger("GhostEngineInvisibleNpcExpirationTime", 90000);
            GHOST_ENGINE_MINIMUM_INTERVAL_BETWEEN_FAKE_CHECKS = protectionSettings.getInteger("GhostEngineMinimumIntervalBetweenFakeChecks", 900000);
            GHOST_ENGINE_MINIMUM_LEVEL_TO_CHECK_OFTEN = protectionSettings.getInteger("GhostEngineMinimumLevelToCheckOften", 65);
            GHOST_ENGINE_RABBIT_TEMPLATE_ID = protectionSettings.getInteger("GhostEngineRabbitTemplateId", 155001);
            GHOST_ENGINE_DEVELOPMENT_MODE = protectionSettings.getBoolean("GhostEngineDevelopmentMode", false);

            PROTECT_ENABLE = protectionSettings.getBoolean("ProtectionEnabled", false);
            PROTECT_UNPROTECTED_IPS = new NetList();
            final String ips = protectionSettings.getProperty("UpProtectedIPs", "");
            if (!ips.equals(""))
                PROTECT_UNPROTECTED_IPS.LoadFromString(ips, ",");

            PROTECT_GS_STORE_HWID = protectionSettings.getBoolean("StoreHWID", true);

            PROTECT_GS_LOG_HWID = protectionSettings.getBoolean("LogHWIDs", false);
            PROTECT_GS_LOG_HWID_QUERY = "INSERT INTO " + protectionSettings.getProperty("LogHWIDsTable", "hwids_log") + " (account,ip,hwid,server_id) VALUES (?,?,?,?);";

            HWID_BANS_TABLE = protectionSettings.getProperty("BanHWIDsTable", "hwid_bans");
            PROTECT_GS_ENABLE_HWID_BANS = protectionSettings.getBoolean("EnableHWIDBans", false);
            PROTECT_GS_ENABLE_HWID_BONUS = protectionSettings.getBoolean("EnableHWIDBonus", false);
            PROTECT_GS_MAX_SAME_HWIDs = protectionSettings.getInteger("MaxSameHWIDs", 0);
        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + PROTECT_FILE + " File.");
        }
    }

    // *******************************************************************************************

    public static final String INSTANCE_FILE = "./config/instances.ini";
    public static int SOD_TIAT_KILL_COUNT;
    public static long SOD_STAGE_2_LENGTH;
    public static int POINTS_OPEN_2_CYCLE;
    public static int SOF_STAGE_2_FROM;
    public static int SOF_STAGE_3_FROM;
    public static int SOF_STAGE_4_FROM;
    public static int SOF_STAGE_OTHER_FROM;
    public static int SOF_STAGE_2_TO;
    public static int SOF_STAGE_3_TO;
    public static int SOF_STAGE_4_TO;
    public static int SOF_STAGE_OTHER_TO;

    public static int INSTANCE_FIRE_ENERGY_COMPRESSION_STONE;
    public static int INSTANCE_WATER_ENERGY_COMPRESSION_STONE;
    public static int INSTANCE_WIND_ENERGY_COMPRESSION_STONE;
    public static int INSTANCE_EARTH_ENERGY_COMPRESSION_STONE;
    public static int INSTANCE_DARKNESS_ENERGY_COMPRESSION_STONE;
    public static int INSTANCE_SACRED_ENERGY_COMPRESSION_STONE;

    // ========================================================================
    public static void loadInstancesSettings() {
        try {
            final L2Properties instanceSettings = new L2Properties(INSTANCE_FILE);

            SOD_TIAT_KILL_COUNT = instanceSettings.getInteger("TiatKillCountForNextState", 10);
            SOD_STAGE_2_LENGTH = instanceSettings.getLong("Stage2Length", 720) * 60 * 1000;
            POINTS_OPEN_2_CYCLE = instanceSettings.getInteger("PointsOpen2Cycle", 20);

            SOF_STAGE_2_FROM = instanceSettings.getInteger("SofStage2From", 60);
            SOF_STAGE_2_TO = instanceSettings.getInteger("SofStage2To", 120);
            SOF_STAGE_3_FROM = instanceSettings.getInteger("SofStage3From", 239);
            SOF_STAGE_3_TO = instanceSettings.getInteger("SofStage3To", 241);
            SOF_STAGE_4_FROM = instanceSettings.getInteger("SofStage4From", 239);
            SOF_STAGE_4_TO = instanceSettings.getInteger("SofStage4To", 241);
            SOF_STAGE_OTHER_FROM = instanceSettings.getInteger("SofStageOtherFrom", 60);
            SOF_STAGE_OTHER_TO = instanceSettings.getInteger("SofStageOtherTo", 240);

            INSTANCE_FIRE_ENERGY_COMPRESSION_STONE = instanceSettings.getInteger("FireEnergyCompressionStone", 10);
            INSTANCE_WATER_ENERGY_COMPRESSION_STONE = instanceSettings.getInteger("WaterEnergyCompressionStone", 10);
            INSTANCE_WIND_ENERGY_COMPRESSION_STONE = instanceSettings.getInteger("WindEnergyCompressionStone", 10);
            INSTANCE_EARTH_ENERGY_COMPRESSION_STONE = instanceSettings.getInteger("EarthEnergyCompressionStone", 10);
            INSTANCE_DARKNESS_ENERGY_COMPRESSION_STONE = instanceSettings.getInteger("DarknessEnergyCompressionStone", 10);
            INSTANCE_SACRED_ENERGY_COMPRESSION_STONE = instanceSettings.getInteger("SacredEnergyCompressionStone", 10);

        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + INSTANCE_FILE + " File.");
        }
    }

    // *******************************************************************************************

    public static final String HELLBOUND_FILE = "./config/hellbound.ini";
    public static float RATE_HELLBOUND_POINTS;

    public static long LEODAS;
    public static long DEREK;
    public static long TORTURED_NATIVE;
    public static long STEEL_CITADEL_KEYMASTER;
    public static long AMASKARI;
    public static long HANNIBAL;
    public static long HELLBOUND_NPC_LIST1;
    public static long HELLBOUND_NPC_LIST2;
    public static long HELLBOUND_NPC_LIST3;
    public static long HELLBOUND_NPC_LIST4;
    public static long HELLBOUND_NPC_LIST5;

    public static int INFINITUM_RATE_UP;
    public static int INFINITUM_RATE_DOWN;

    // ========================================================================
    public static void loadHellboundSettings() {
        try {
            final L2Properties settings = new L2Properties(HELLBOUND_FILE);
            RATE_HELLBOUND_POINTS = settings.getFloat("RateHellBoundPoints", 1);

            HELLBOUND_NPC_LIST1 = settings.getLong("NpcList1", 10);
            HELLBOUND_NPC_LIST2 = settings.getLong("NpcList2", 3);
            HELLBOUND_NPC_LIST3 = settings.getLong("NpcList3", -10);
            HELLBOUND_NPC_LIST4 = settings.getLong("NpcList4", 5);
            HELLBOUND_NPC_LIST5 = settings.getLong("NpcList5", 3);

            LEODAS = settings.getLong("Leodas", -1000);
            DEREK = settings.getLong("Derek", 10000);

            STEEL_CITADEL_KEYMASTER = settings.getLong("SteelCitadelKeymaster", 20);
            AMASKARI = settings.getLong("Amaskari", 50);
            TORTURED_NATIVE = settings.getLong("TorturedNative", -10);
            HANNIBAL = settings.getLong("Hannibal", 200);

            INFINITUM_RATE_UP = settings.getInteger("InfinitumRateUp", 40);
            INFINITUM_RATE_DOWN = settings.getInteger("InfinitumRateDouwn", 10);

        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + HELLBOUND_FILE + " File.");
        }
    }

    // *******************************************************************************************

    public static final String ITEM_AUCTION_FILE = "./config/item_auction.ini";

    public static boolean ALT_ITEM_AUCTION_ENABLED;
    public static boolean ALT_ITEM_AUCTION_CAN_REBID;
    public static int ALT_ITEM_AUCTION_BID_ITEM_ID;
    public static long ALT_ITEM_AUCTION_MAX_BID;
    public static int ALT_ITEM_AUCTION_MAX_CANCEL_TIME_IN_MILLIS;

    // ========================================================================
    public static void loadItemAuctionSettings() {
        try {
            final L2Properties settings = new L2Properties(ITEM_AUCTION_FILE);
            ALT_ITEM_AUCTION_ENABLED = settings.getBoolean("ItemAuctionEnabled", true);
            ALT_ITEM_AUCTION_CAN_REBID = settings.getBoolean("ItemAuctionCanRebid", true);
            ALT_ITEM_AUCTION_BID_ITEM_ID = settings.getInteger("ItemAuctionBidItemId", 57);
            ALT_ITEM_AUCTION_MAX_BID = settings.getLong("ItemAuctionMaxBid", 2100000000);
            ALT_ITEM_AUCTION_MAX_CANCEL_TIME_IN_MILLIS = settings.getInteger("ItemAuctionMaxCancelTimeInMillis", 604800000);

        } catch (final Exception e) {
            _log.log(Level.WARNING, "", e);
            throw new Error("Failed to Load " + ITEM_AUCTION_FILE + " File.");
        }
    }

    // *******************************************************************************************

    public static boolean allowL2Walker(final L2Player player) {
        return ALLOW_L2WALKER_CLIENT == L2WalkerAllowed.True || player.getPlayerAccess().AllowWalker || ALLOW_L2WALKER_CLIENT == L2WalkerAllowed.Peace && player.isInZone(peace_zone) && player.getLevel() <= MAX_L2WALKER_LEVEL;
    }

    // it has no instancies
    private Config() {
    }

    public static void saveHexid(final String string) {
        saveHexid(string, HEXID_FILE);
    }

    public static void saveHexid(final String string, final String fileName) {
        try {
            final Properties hexSetting = new Properties();
            final File file = new File(fileName);
            file.createNewFile();
            final OutputStream out = new FileOutputStream(file);
            hexSetting.setProperty("HexID", string);
            hexSetting.store(out, "the hexID to auth into login");
            out.close();
        } catch (final Exception e) {
            _log.log(Level.WARNING, "Failed to save hex id to " + fileName + " File.", e);
        }
    }

    public static class ClassMasterSettings {
        private final TIntObjectHashMap<TIntIntHashMap> _claimItems;
        private final TIntObjectHashMap<TIntIntHashMap> _rewardItems;
        private final TIntObjectHashMap<Boolean> _allowedClassChange;

        public ClassMasterSettings(final String _configLine) {
            _claimItems = new TIntObjectHashMap<TIntIntHashMap>();
            _rewardItems = new TIntObjectHashMap<TIntIntHashMap>();
            _allowedClassChange = new TIntObjectHashMap<Boolean>();
            if (_configLine != null)
                parseConfigLine(_configLine.trim());
        }

        private void parseConfigLine(final String _configLine) {
            final StringTokenizer st = new StringTokenizer(_configLine, ";");
            while (st.hasMoreTokens()) {
                // get allowed class change
                final int job = TypeFormat.parseInt(st.nextToken());

                _allowedClassChange.put(job, true);

                TIntIntHashMap _items = new TIntIntHashMap();
                // parse items needed for class change
                if (st.hasMoreTokens()) {
                    final StringTokenizer st2 = new StringTokenizer(st.nextToken(), "[],");

                    while (st2.hasMoreTokens()) {
                        final StringTokenizer st3 = new StringTokenizer(st2.nextToken(), "()");
                        final int _itemId = TypeFormat.parseInt(st3.nextToken());
                        final int _quantity = TypeFormat.parseInt(st3.nextToken());
                        _items.put(_itemId, _quantity);
                    }
                }

                _claimItems.put(job, _items);

                _items = new TIntIntHashMap();
                // parse gifts after class change
                if (st.hasMoreTokens()) {
                    final StringTokenizer st2 = new StringTokenizer(st.nextToken(), "[],");

                    while (st2.hasMoreTokens()) {
                        final StringTokenizer st3 = new StringTokenizer(st2.nextToken(), "()");
                        final int _itemId = TypeFormat.parseInt(st3.nextToken());
                        final int _quantity = TypeFormat.parseInt(st3.nextToken());
                        _items.put(_itemId, _quantity);
                    }
                }
                _rewardItems.put(job, _items);
            }
        }

        public boolean isAllowed(final int job) {
            if (_allowedClassChange == null)
                return false;
            if (_allowedClassChange.containsKey(job))
                return _allowedClassChange.get(job);
            return false;
        }

        public TIntIntHashMap getRewardItems(final int job) {
            if (_rewardItems.containsKey(job))
                return _rewardItems.get(job);
            return null;
        }

        public TIntIntHashMap getRequireItems(final int job) {
            if (_claimItems.containsKey(job))
                return _claimItems.get(job);
            return null;
        }
    }

    // XXX перезагрузка конфигов
    public static String reloadConfigByName(final String name) {
        final File file = new File("./config/" + name);
        if (!file.exists())
            return "configuration file '" + name + "' not found!";

        if (name.equalsIgnoreCase("ai.ini"))
            loadAISettings();
        else if (name.equalsIgnoreCase("alternative.ini"))
            loadAlternativeSettings();
        else if (name.equalsIgnoreCase("boss.ini"))
            loadBossSettings();
        else if (name.equalsIgnoreCase("community_board.ini"))
            loadCommunityBoardSettings();
        else if (name.equalsIgnoreCase("champion.ini"))
            loadChampionSettings();
        else if (name.equalsIgnoreCase("l2system.ini"))
            loadl2sSettings();
        else if (name.equalsIgnoreCase("fame.ini"))
            loadFameSettings();
        else if (name.equalsIgnoreCase("gameserver.ini"))
            loadServerSettings();
        else if (name.equalsIgnoreCase("clan.ini"))
            loadClanSettings();

        else if (name.equalsIgnoreCase("residence.ini"))
            loadResSettings();
        else if (name.equalsIgnoreCase("rate.ini"))
            loadRateSettings();
        else if (name.equalsIgnoreCase("wedding.ini"))
            loadWeddingSettings();
        else if (name.equalsIgnoreCase("lottery.ini"))
            loadLotterySettings();
        else if (name.equalsIgnoreCase("walker.ini"))
            loadWalkerSettings();
        else if (name.equalsIgnoreCase("chat.ini"))
            loadChatSettings();
        else if (name.equalsIgnoreCase("other.ini"))
            loadOtherSettings();
        else if (name.equalsIgnoreCase("spoil.ini"))
            loadSpoilSettings();
        else if (name.equalsIgnoreCase("commands.ini"))
            loadCommandSettings();
        else if (name.equalsIgnoreCase("manor.ini"))
            loadManorSettings();
        else if (name.equalsIgnoreCase("vitality.ini"))
            loadVitalitySettings();
        else if (name.equalsIgnoreCase("olympiad_option.ini"))
            loadOlympiadSettings();
        else if (name.equalsIgnoreCase("services.ini"))
            loadServiceSettings();
        else if (name.equalsIgnoreCase("karma.ini"))
            loadKarmaSettings();
        else if (name.equalsIgnoreCase("siege.ini"))
            loadSiegeSettings();
        else if (name.equalsIgnoreCase("events.ini"))
            loadEventsSettings();
        else if (name.equalsIgnoreCase("geodata.ini"))
            loadGeoConfig();
        else if (name.equalsIgnoreCase("enchant.ini"))
            loadEnchantSettings();
        else if (name.equalsIgnoreCase("skills.ini"))
            loadSkillsSettings();
        else if (name.equalsIgnoreCase("instances.ini"))
            loadInstancesSettings();
        else if (name.equalsIgnoreCase("hellbound.ini"))
            loadHellboundSettings();
        else if (name.equalsIgnoreCase("protection.ini"))
            loadProtectionSettings();
        else if (name.equalsIgnoreCase("item_auction.ini"))
            loadItemAuctionSettings();
        else
            return "reload file '" + name + "' not implemented!";

        return "configuration file '" + name + "' reloaded.";
    }
}