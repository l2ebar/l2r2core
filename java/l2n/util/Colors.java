package l2n.util;

import l2n.Config;

public class Colors
{
	private static final int COLORS[] = new int[766];

	public static int loadColors()
	{
		int pos = 0;
		
		for(int i = 255; i > 0; i--)
			COLORS[pos++] = ((i & 0xFF) << 0) + ((255 & 0xFF) << 8) + ((i & 0xFF) << 16);

		for(int i = 0; i < 255; i++)
			COLORS[pos++] = ((i & 0xFF) << 0) + ((255 & 0xFF) << 8) + ((0 & 0xFF) << 16);

		for(int i = 255; i >= 0; i--)
			COLORS[pos++] = ((255 & 0xFF) << 0) + ((i & 0xFF) << 8) + ((0 & 0xFF) << 16);

		return COLORS.length;
	}

	public static final int getColor(int pvpkils)
	{
		pvpkils = (int) Math.ceil(pvpkils / Config.TITLE_PVP_MODE_RATE);
		if(pvpkils >= COLORS.length)
			return COLORS[COLORS.length - 1];
		return COLORS[pvpkils];
	}
}
