package l2n.util;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.commons.text.Strings;
import l2n.game.model.actor.L2Player;

import java.io.*;
import java.util.logging.Logger;

public class Files
{
	private static final Logger _log = Logger.getLogger(Files.class.getName());

	private static final TIntObjectHashMap<String> cache = new TIntObjectHashMap<String>();

	public static String read(String name)
	{
		if(name == null)
			return null;

		File file = new File(Config.DATAPACK_ROOT, name);
		if(!file.exists())
			return null;

		final String relpath = Util.getRelativePath(Config.DATAPACK_ROOT, file);
		final int hashcode = relpath.hashCode();


		if(Config.USE_FILE_CACHE && cache.containsKey(hashcode))
			return cache.get(hashcode);


		String content = null;
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new UnicodeReader(new FileInputStream(file), "UTF-8"));
			StringBuilder sb = StringUtil.startAppend(200);
			String s = "";
			while ((s = br.readLine()) != null)
				StringUtil.append(sb, s, "\n");
			content = sb.toString();
			sb = null;
		}
		catch(Exception ignored)
		{}
		finally
		{
			try
			{
				if(br != null)
					br.close();
			}
			catch(Exception ignored)
			{}
		}

		if(Config.USE_FILE_CACHE)
			cache.put(hashcode, content);

		return content;
	}

	public static String read(File file)
	{
		if(!file.exists())
			return null;

		String content = null;
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new UnicodeReader(new FileInputStream(file), "UTF-8"));
			StringBuilder sb = StringUtil.startAppend(200);
			String s = "";
			while ((s = br.readLine()) != null)
				StringUtil.append(sb, s, "\n");
			content = sb.toString();
			sb = null;
		}
		catch(Exception ignored)
		{}
		finally
		{
			try
			{
				if(br != null)
					br.close();
			}
			catch(Exception ignored)
			{}
		}

		return content;
	}

	public static void cacheClean()
	{
		cache.clear();
	}

	public static int size()
	{
		return cache.size();
	}

	public static long lastModified(String name)
	{
		if(name == null)
			return 0;
		return new File(name).lastModified();
	}

	public static String read(String name, L2Player player)
	{
		if(player == null)
			return "";
		return read(name, player.getVar("lang@"));
	}

	public static String langFileName(String name, String lang)
	{
		if(lang == null || lang.equalsIgnoreCase("en"))
			lang = "";

		String tmp;

		tmp = name.replaceAll("(.+)(\\.htm)", "$1-" + lang + "$2");
		if(!tmp.equals(name) && lastModified(tmp) > 0)
			return tmp;

		tmp = name.replaceAll("(.+)(/[^/].+\\.htm)", "$1/" + lang + "$2");
		if(!tmp.equals(name) && lastModified(tmp) > 0)
			return tmp;

		tmp = name.replaceAll("(.+?/html)/", "$1-" + lang + "/");
		if(!tmp.equals(name) && lastModified(tmp) > 0)
			return tmp;

		if(lastModified(name) > 0)
			return name;

		return null;
	}

	public static String read(String name, String lang)
	{
		String tmp = langFileName(name, lang);

		long last_modif = lastModified(tmp);
		if(last_modif > 0)
		{
			if(last_modif >= lastModified(name) || !Config.CHECK_LANG_FILES_MODIFY)
				return Strings.bbParse(read(tmp));

			_log.warning("Last modify of " + name + " more then " + tmp);
		}

		return Strings.bbParse(read(name));
	}

	public static void writeFile(String path, String string)
	{
		if(string == null || string.length() == 0)
			return;

		File target = new File(path);
		if(!target.exists())
			try
			{
				target.createNewFile();
			}
			catch(IOException e)
			{
				e.printStackTrace(System.err);
			}

		try
		{
			FileOutputStream fos = new FileOutputStream(target);
			fos.write(string.getBytes("UTF-8"));
			fos.close();
		}
		catch(IOException e)
		{
			e.printStackTrace(System.err);
		}
	}
}
