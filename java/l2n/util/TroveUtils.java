package l2n.util;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.hash.TIntHashSet;

import java.util.NoSuchElementException;

public class TroveUtils
{
	@SuppressWarnings("rawtypes")
	private transient static final TIntObjectHashMap EMPTY_INT_OBJECT_MAP = new TIntObjectHashMapEmpty();
	@SuppressWarnings("unused")
	private transient static final TIntArrayList EMPTY_INT_ARRAY_LIST = new TIntArrayListEmpty();

	private transient static final TIntIterator EMPTY_ITERATOR = new TIntIteratorEmpty();
	private transient static final TIntHashSet EMPTY_INT_HASH_SET = new TIntHashSetEmpty();

	@SuppressWarnings("unchecked")
	public static <V> TIntObjectHashMap<V> emptyIntObjectMap()
	{
		return EMPTY_INT_OBJECT_MAP;
	}

	public static final <T> TIntIterator emptyIterator()
	{
		return EMPTY_ITERATOR;
	}

	public static final <T> TIntHashSet emptyIntHashSet()
	{
		return EMPTY_INT_HASH_SET;
	}

	private static class TIntObjectHashMapEmpty<V> extends TIntObjectHashMap<V>
	{
		private static final long serialVersionUID = -1;

		TIntObjectHashMapEmpty()
		{
			super(0);
		}

		@Override
		public V remove(int val)
		{
			return null;
		}

		@Override
		public boolean contains(int val)
		{
			return false;
		}

		@Override
		public boolean isEmpty()
		{
			return true;
		}

		@Override
		public V put(int key, V value)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public V get(int key)
		{
			return null;
		}

		@Override
		public V putIfAbsent(int key, V value)
		{
			throw new UnsupportedOperationException();
		}
	}

	private static class TIntArrayListEmpty extends TIntArrayList
	{
		private static final long serialVersionUID = -1;

		TIntArrayListEmpty()
		{
			super(0);
		}

		@Override
		public TIntIterator iterator()
		{
			return EMPTY_ITERATOR;
		}

		@Override
		public boolean remove(int val)
		{
			return false;
		}

		@Override
		public boolean contains(int val)
		{
			return false;
		}

		@Override
		public boolean isEmpty()
		{
			return true;
		}

		@Override
		public boolean add(int val)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public int get(int key)
		{
			return no_entry_value;
		}
	}

	private static class TIntIteratorEmpty implements TIntIterator
	{
		@Override
		public boolean hasNext()
		{
			return false;
		}

		@Override
		public int next()
		{
			throw new NoSuchElementException();
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException();
		}
	}

	private static class TIntHashSetEmpty extends TIntHashSet
	{
		private static final long serialVersionUID = -1;

		TIntHashSetEmpty()
		{
			super(0);
		}

		@Override
		public TIntIterator iterator()
		{
			return EMPTY_ITERATOR;
		}

		@Override
		protected void rehash(int arg0)
		{}

		@Override
		public boolean add(int val)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean remove(int val)
		{
			return false;
		}

		@Override
		public boolean contains(int val)
		{
			return false;
		}

		@Override
		public boolean isEmpty()
		{
			return true;
		}

		@Override
		public boolean addAll(int[] values)
		{
			throw new UnsupportedOperationException();
		}
	}
}
