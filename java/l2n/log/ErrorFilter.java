package l2n.log;

import java.util.logging.Filter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class ErrorFilter implements Filter
{
	@Override
	public boolean isLoggable(LogRecord record)
	{
		return record.getLevel().intValue() > Level.INFO.intValue() || record.getThrown() != null;
	}
}
