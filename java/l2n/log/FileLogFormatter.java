package l2n.log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class FileLogFormatter extends Formatter
{

	private static final String CRLF = "\r\n";
	private static final String space = "\t";
	/** Для вывода даты в формате ГГГГ/MM/дд ЧЧ:мм:сс */
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	@Override
	public String format(LogRecord record)
	{
		StringBuffer output = new StringBuffer();
		output.append(dateFormat.format(new Date(record.getMillis())));
		output.append(space);
		output.append(record.getLevel().getName());
		output.append(space);
		output.append(record.getThreadID());
		output.append(space);
		output.append(record.getLoggerName());
		output.append(space);
		output.append(record.getMessage());
		output.append(CRLF);
		return output.toString();
	}
}
