package l2n.status.gshandlers;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.loginservercon.LSConnection;
import l2n.game.loginservercon.send.BanIP;
import l2n.game.loginservercon.send.ChangeAccessLevel;
import l2n.game.loginservercon.send.UnbanIP;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.login.protect.BanInformation;
import l2n.util.AutoBan;
import l2n.util.HWID;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Collection;

public class HandlerBan
{
	public static void BanIP(final String fullCmd, final String[] argv, final PrintWriter _print, final Socket _csocket)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty())
		{
			final Collection<BanInformation> baniplist = LSConnection.getInstance().getBannedIpList();
			if(baniplist != null && baniplist.size() > 0)
			{
				_print.println("Ban IP List:");
				for(final BanInformation temp : baniplist)
					_print.println("Ip:" + temp.ip + " banned by " + temp.admin);
			}
			else
				_print.println("No banned ips.");
		}
		else if(argv[1].equalsIgnoreCase("?"))
			_print.println("USAGE: banip [IP]");
		else
		{
			LSConnection.getInstance().sendPacket(new BanIP(argv[1], "Telnet: " + _csocket.getInetAddress().getHostAddress()));
			_print.println("IP " + argv[1] + " banned");
		}
	}

	public static void UnBanIP(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty() || argv[1].equalsIgnoreCase("?"))
			_print.println("USAGE: unbanip IP");
		else
		{
			LSConnection.getInstance().sendPacket(new UnbanIP(argv[1]));
			_print.println("IP " + argv[1] + " unbanned");
		}
	}
	public static void BanHWID(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		_print.println(HWID.handleBanHWID(argv));
	}
	
	public static void UnBanHWID(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty())
		{
			_print.println("USAGE: unbanhwid HWID");
		}
		else
		{
			if(HWID.UnbanHWID(argv[1]) == true)
			{
				_print.println("Unbanned HWID: " + argv[1]);
			}
			else
			{
				_print.println("Protection: Failed to unban HWID: " + argv[1]);
			}
		}
	}
	public static void Kick(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty() || argv[1].equalsIgnoreCase("?"))
			_print.println("USAGE: kick Player");
		else
		{
			final L2Player player = L2ObjectsStorage.getPlayer(argv[1]);
			if(player == null)
				_print.println("Unable to find Player: " + argv[1]);
			else
			{
				player.sendMessage("You are kicked by admin");
				player.logout(false, false, true, true);
				_print.println("Player " + argv[1] + " kicked");
			}
		}
	}
	
	public static void Ban(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty() || argv[1].equalsIgnoreCase("?"))
		{
			_print.println("USAGE: ban Player");
		}
		else
		{
			final L2Player player = L2ObjectsStorage.getPlayer(argv[1]);
			if(player == null)
			{
				if(AutoBan.Banned(argv[1], -100, Integer.MAX_VALUE, "You are banned by admin", "[L2Dream]Админ"))
				{
					_print.println("You banned " + argv[1]);
				}
				else
				{
					_print.println("Can't find char: " + argv[1]);
				}
			}
			else
			{
				player.sendMessage(new CustomMessage("scripts.commands.admin.AdminBan.YoureBannedByGM", player).addString("[L2Dream]Админ"));
				player.setAccessLevel(-100);
				AutoBan.Banned(player, Integer.MAX_VALUE, "You are banned by admin", "[L2Dream]Админ");
				player.logout(false, false, true, true);
				_print.println("You banned " + player.getName());
			}
		}
	}
	
	public static void UnBan(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty())
		{
			_print.println("USAGE: unban Player");
		}
		else
		{
			final L2Player player = L2ObjectsStorage.getPlayer(argv[1]);
			if(player == null)
			{
				_print.println("Unable to find Player: " + argv[1]);
			}
			else
			{
				LSConnection.getInstance().sendPacket(new ChangeAccessLevel(player.getAccountName(), 0, "command admin_unban", 0));
				_print.println("You unbanned " + player);
			}
			if(AutoBan.Banned(argv[1], 0, 0, "", "[System]Admin"))
			{
				_print.println("You unbanned: " + argv[1]);
			}
			else
			{
				_print.println("Can't find char: " + argv[1]);
			}
		}
	}
}
