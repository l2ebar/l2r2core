@echo off
color 17
cls
title SQL L2System Account Manager

SET java_settings=%java_settings% -Dfile.encoding=UTF-8
SET java_settings=%java_settings% -Djava.util.logging.config.file=config/console.ini

java %java_settings% -cp ./lib/*;L2System.jar l2n.accountmanager.SQLAccountManager 2> NUL
if %errorlevel% == 0
(
	echo.
	echo Execution successful
	echo.
)
else
(
	echo.
	echo An error has occurred while running the L2System Account Manager!
	echo.
	echo Possible reasons for this to happen:
	echo.
	echo - MySQL server not running or incorrect MySQL settings.
	echo - Wrong data types or values out of range were provided.
	echo    specify correct values for each required field
	echo.
)
pause
