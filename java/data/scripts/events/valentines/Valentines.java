package events.valentines;

import l2n.Config;
import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.Announcements;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.ItemTable;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Files;
import l2n.util.Rnd;

import java.util.ArrayList;

public class Valentines extends EventScript implements ScriptFile
{
	private final static int EVENT_MANAGER_ID = 32100;

	private final static int[][] _dropdata = {
			// Item, chance
			{ 4209, 2 },
			{ 4210, 2 },
			{ 4211, 2 },
			{ 4212, 2 },
			{ 4213, 2 },
			{ 4214, 2 },
			{ 4215, 2 },
			{ 4216, 2 },
			{ 4217, 2 } };

	private final static ArrayList<L2Spawn> _spawns = new ArrayList<L2Spawn>();

	private static boolean _active = false;

	@Override
	public void onLoad()
	{
		if(isActive("Valentines"))
		{
			_active = true;
			addEventId(ScriptEventType.ON_DIE);
			addEventId(ScriptEventType.ON_ENTER_WORLD);
			spawnEventManagers();
			System.out.println("Loaded Event: Valentines [state: activated]");
		}
		else
			System.out.println("Loaded Event: Valentines [state: deactivated]");
	}

	/**
	 * Запускает эвент
	 */
	public void startEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("Valentines", true))
		{
			addEventId(ScriptEventType.ON_DIE);
			addEventId(ScriptEventType.ON_ENTER_WORLD);
			spawnEventManagers();
			System.out.println("Event 'Valentines' started.");
			Announcements.announceByCustomMessage("scripts.events.valentines.Valentines.AnnounceEventStarted");
		}
		else
			player.sendMessage("Event 'Valentines' already started.");

		_active = true;

		show(Files.read("data/html/admin/events.htm", player), player);
	}

	/**
	 * Останавливает эвент
	 */
	public void stopEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("Valentines", false))
		{
			removeEventId(ScriptEventType.ON_DIE);
			removeEventId(ScriptEventType.ON_ENTER_WORLD);
			unSpawnEventManagers();
			System.out.println("Event 'Valentines' stopped.");
			Announcements.announceByCustomMessage("scripts.events.valentines.Valentines.AnnounceEventStoped");
		}
		else
			player.sendMessage("Event 'Valentines' not started.");

		_active = false;

		show(Files.read("data/html/admin/events.htm", player), player);
	}

	/**
	 * Спавнит эвент менеджеров и рядом ёлки
	 */
	private void spawnEventManagers()
	{
		final int EVENT_MANAGERS[][] = {

				{ -85351, 244441, -3730, 32100 },
				{ -80999, 150969, -3044, 32100 },
				{ -45820, -113726, -217, 32100 },
				{ -13799, 122115, -2989, 32100 },
				{ 11293, 15643, -4584, 32100 },
				{ 16148, 142896, -2706, 32100 },
				{ 42687, -47933, -797, 32100 },
				{ 46627, 51374, -3022, 32100 },
				{ 81692, 148384, -3467, 32100 },
				{ 83184, 53476, -1454, 32100 },
				{ 87480, -144613, -1319, 32100 },
				{ 111523, 218627, -3466, 32100 },
				{ 114773, -178683, -821, 32100 },
				{ 118143, 76004, -2688, 32100 },
				{ 147602, -54111, -2734, 32100 },
				{ 147713, 25609, -2013, 32100 } };
		L2NpcTemplate template = NpcTable.getTemplate(EVENT_MANAGER_ID);
		for(int[] element : EVENT_MANAGERS)
			try
			{
				L2Spawn sp = new L2Spawn(template);
				sp.setLocx(element[0]);
				sp.setLocy(element[1]);
				sp.setLocz(element[2]);
				sp.setAmount(1);
				sp.setHeading(element[3]);
				sp.setRespawnDelay(0);
				sp.init();
				_spawns.add(sp);
			}
			catch(ClassNotFoundException e)
			{
				e.printStackTrace();
			}

	}

	/**
	 * Удаляет спавн эвент менеджеров
	 */
	private void unSpawnEventManagers()
	{
		for(L2Spawn sp : _spawns)
		{
			sp.stopRespawn();
			sp.getLastSpawn().deleteMe();
		}
		_spawns.clear();
	}

	@Override
	public void onReload()
	{
		unSpawnEventManagers();
	}

	@Override
	public void onShutdown()
	{
		unSpawnEventManagers();
	}

	/**
	 * Обработчик смерти мобов, управляющий эвентовым дропом
	 */
	@Override
	protected void onDie(L2Character cha, L2Character killer)
	{
		if(_active && cha.isMonster() && !cha.isRaid() && killer != null && killer.getPlayer() != null && Math.abs(cha.getLevel() - killer.getLevel()) < 10)
		{
			int dropCounter = 0;
			for(int[] drop : _dropdata)
				if(Rnd.get(1000) <= drop[1] * killer.getPlayer().getRateItems() * Config.RATE_DROP_ITEMS)
				{
					dropCounter++;

					L2ItemInstance item = ItemTable.getInstance().createItem(drop[0], killer.getPlayer().getObjectId(), 0, "Valentines");
					((L2NpcInstance) cha).dropItem(killer.getPlayer(), item);

					// Из одного моба выпадет не более 3-х эвентовых предметов
					if(dropCounter > 3)
						break;
				}
		}
	}

	@Override
	protected void onEnterWorld(L2Player player)
	{
		if(_active)
			Announcements.announceToPlayerByCustomMessage(player, "scripts.events.valentines.Valentines.AnnounceEventStarted", null);
	}
}
