
package services;
//Database

import l2n.Config;
import l2n.database.FiltredPreparedStatement;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.tables.ItemTable;

import java.sql.ResultSet;

public class Account extends Functions implements ScriptFile
{
	
	public void CharToAcc()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;
			
		if(!Config.ACC_MOVE_ENABLED)
			return;

		String append = "Перенос персонажей между аккаунтами.<br>";
		append += "Цена: " + Config.ACC_MOVE_PRICE + " " + ItemTable.getInstance().getTemplate(Config.ACC_MOVE_ITEM).getName() + ".<br>";
		append += "Внимание !!! При переносе персонажа на другой аккаунт, убедитесь что персонажей там меньше чем 7, иначе могут возникнуть непредвиденные ситуации за которые Администрация не отвечает.<br>";
		append += "Внимательно вводите логин куда переносите, администрация не возвращает персонажей.";
		append += "Вы переносите персонажа " + player.getName() + ", на какой аккаунт его перенести ?";
		append += "<edit var=\"new_acc\" width=150>";
		append += "<button value=\"Перенести\" action=\"bypass -h scripts_services.Account:NewAccount $new_acc\" width=130 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"><br>";
		show(append, player);
	}

	public void NewAccount(String[] name)
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;
			
		if(!Config.ACC_MOVE_ENABLED)
			return;

		if(player.getInventory().getCountOf(Config.ACC_MOVE_ITEM) < Config.ACC_MOVE_PRICE)
		{
			player.sendMessage("У вас нету " + Config.ACC_MOVE_PRICE + " " + ItemTable.getInstance().getTemplate(Config.ACC_MOVE_ITEM));
			CharToAcc();
			return;
		}
		String _name = name[0];
		ThreadConnection con = null;
        ThreadConnection conGS = null;
		FiltredPreparedStatement offline = null;
        FiltredStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstanceLogin().getConnection();
			offline = con.prepareStatement("SELECT `login` FROM `accounts` WHERE `login` = ?");
			offline.setString(1, _name);
			rs = offline.executeQuery();
			if(rs.next())
			{
				removeItem(player, Config.ACC_MOVE_ITEM, Config.ACC_MOVE_PRICE);
                conGS = L2DatabaseFactory.getInstance().getConnection();
			    statement = conGS.createStatement();
				statement.executeUpdate("UPDATE `characters` SET `account_name` = '" + _name + "' WHERE `char_name` = '" + player.getName() + "'");
				player.sendMessage("Персонаж успешно перенесен.");
				player.logout(false, false, false, true);
			}
			else
			{
				player.sendMessage("Введенный аккаунт не найден.");
				CharToAcc();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}
		finally
		{
			DbUtils.closeQuietly(con, offline, rs);
            DbUtils.closeQuietly(conGS, statement);
		}
	}

	public void onLoad()
	{
         System.out.println("Loaded Service: Account Transfer [" + (Config.ACC_MOVE_ENABLED ? "enabled]" : "disabled]"));	
	}

	public void onReload()
	{}

	public void onShutdown()
	{}
}