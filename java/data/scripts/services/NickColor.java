package services;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import l2n.util.PremiumAccess;
import l2n.util.PremiumAccess.ServiceType;

public class NickColor extends Functions implements ScriptFile
{
	public void list()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		StringBuilder append = new StringBuilder();
		if(player.hasPremiumAccount() && Config.SERVICES_CHANGE_NICK_COLOR_FREE_PA)
		{
			append.append("<br>Possible colors:<br>");
			for(String color : Config.SERVICES_CHANGE_NICK_COLOR_LIST)
				append.append("<br><a action=\"bypass -h scripts_services.NickColor:changeFree ").append(color).append("\"><font color=\"").append(color.substring(4, 6) + color.substring(2, 4) + color.substring(0, 2)).append("\">").append(color.substring(4, 6) + color.substring(2, 4) + color.substring(0, 2)).append("</font></a>");
		}
		else
		{
			append.append("Вы можете изменить цвет ника за небольшую цену ").append(Config.SERVICES_CHANGE_NICK_COLOR_PRICE).append(" ").append(ItemTable.getInstance().getTemplate(Config.SERVICES_CHANGE_NICK_COLOR_ITEM).getName()).append(".");
			append.append("<br>Возможные цвета:<br>");
			for(String color : Config.SERVICES_CHANGE_NICK_COLOR_LIST)
				append.append("<br><a action=\"bypass -h scripts_services.NickColor:change ").append(color).append("\"><font color=\"").append(color.substring(4, 6) + color.substring(2, 4) + color.substring(0, 2)).append("\">").append(color.substring(4, 6) + color.substring(2, 4) + color.substring(0, 2)).append("</font></a>");
		}

		append.append("<br><a action=\"bypass -h scripts_services.NickColor:change FFFFFF\"><font color=\"FFFFFF\">Восстановить значения по умолчанию (бесплатно)</font></a>");
		show(append.toString(), player);
	}

	public void change(String[] param)
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_CHANGE_NICK_COLOR_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		// проверка прав доступа для игроков без ПА
		if(!PremiumAccess.checkAccessService(player, ServiceType.NICK_COLOR_CHANGE))
			return;

		if(param[0].equalsIgnoreCase("FFFFFF"))
		{
			player.setNameColor(Integer.decode("0xFFFFFF"));
			player.broadcastUserInfo(true);
			return;
		}

		L2Item item = ItemTable.getInstance().getTemplate(Config.SERVICES_CHANGE_NICK_COLOR_ITEM);
		L2ItemInstance pay = player.getInventory().getItemByItemId(item.getItemId());
		if(pay != null && pay.getCount() >= Config.SERVICES_CHANGE_NICK_COLOR_PRICE)
		{
			player.getInventory().destroyItem(pay, Config.SERVICES_CHANGE_NICK_COLOR_PRICE, true);
			player.setNameColor(Integer.decode("0x" + param[0]));
			player.broadcastUserInfo(true);
		}
		else if(Config.SERVICES_CHANGE_NICK_COLOR_ITEM == 57)
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
		else
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	public void changeFree(String[] param)
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_CHANGE_NICK_COLOR_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		// проверка прав доступа для игроков без ПА
		if(!PremiumAccess.checkAccessService(player, ServiceType.NICK_COLOR_CHANGE))
			return;

		if(param[0].equalsIgnoreCase("FFFFFF"))
		{
			player.setNameColor(Integer.decode("0xFFFFFF"));
			player.broadcastUserInfo(true);
			return;
		}

		player.setNameColor(Integer.decode("0x" + param[0]));
		player.broadcastUserInfo(true);
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Nick color");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
