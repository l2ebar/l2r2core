package data.scripts.services;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.SkillTable;
import l2n.util.Files;

public class GrandIsleofPrayerRace extends Functions implements ScriptFile
{
	private static final int RACE_STAMP = 10013;
	private static final int SECRET_KEY = 9694;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Grand Isle of Prayer Race");
	}

	public void startRace()
	{
		L2Skill skill = SkillTable.getInstance().getInfo(L2Skill.SKILL_EVENT_TIMER, 1);
		L2Player player = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(skill == null || player == null || npc == null)
			return;

		getNpc().altUseSkill(skill, player);
		removeItem(player, RACE_STAMP, getItemCount(player, RACE_STAMP));
		show(Files.read("data/html/defautl/32349-2.htm", player), player);
	}

	public String DialogAppend_32349(Integer val)
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return "";

		if(player.getEffectList().getFirstEffect(L2Skill.SKILL_EVENT_TIMER) == null)
			return "<br>[scripts_services.GrandIsleofPrayerRace:startRace|Start the Race.]";

		long raceStampsCount = getItemCount(player, RACE_STAMP);
		if(raceStampsCount < 4)
			return "<br>*Гонка продолжается, спешите!*";
		removeItem(player, RACE_STAMP, raceStampsCount);
		addItem(player, SECRET_KEY, 3);
		player.getEffectList().stopEffect(L2Skill.SKILL_EVENT_TIMER);
		return "<br>Хорошая работа, вот ваши ключи.";
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
