package quests._190_LostDream;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * <hr>
 * <em>Квест</em> <strong>Lost Dream</strong>
 * <hr>
 * 
 * @author HellSinger
 * @lastfix
 * @version CT2
 */
public class _190_LostDream extends Quest implements ScriptFile
{
	// NPCs
	private static final int Kusto = 30512;
	private static final int Nikola = 30621;
	private static final int Lorain = 30673;
	private static final int Juris = 30113;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _190_LostDream()
	{
		super(190, -1);

		addStartNpc(Kusto);
		addTalkId(new int[] { Nikola, Lorain, Juris });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("30512-02.htm"))
		{
			st.playSound(SOUND_ACCEPT);
			st.setState(STARTED);
			st.set("cond", "1");
		}
		else if(event.equalsIgnoreCase("30113-03.htm"))
		{
			st.playSound("SOUND_MIDDLE");
			st.set("cond", "2");
		}
		else if(event.equalsIgnoreCase("30512-05.htm"))
		{
			st.playSound("SOUND_MIDDLE");
			st.set("cond", "3");
		}
		else if(event.equalsIgnoreCase("wait_timer"))
		{
			st.giveItems(57, 109427); // если не использовать таймер и завершить квест в onTalk,
			if(st.getPlayer().getLevel() < 48)
				st.addExpAndSp(309467, 20614);
			st.playSound(SOUND_FINISH); // то вместо последнего диалога "30512-07.htm"
			st.exitCurrentQuest(false); // отображается "Квест завершен" :/
			return null;
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(st.getState() == CREATED && npcId == Kusto)
		{
			QuestState qs187 = st.getPlayer().getQuestState("_187_NikolasHeart");
			if(st.getPlayer().getLevel() < 42)
				htmltext = "30512-00.htm";
			else if(qs187 != null && qs187.isCompleted())
				return "30512-01.htm";
			st.exitCurrentQuest(true);
		}
		else if(st.isStarted())
			switch (npcId)
			{
				case Kusto:
				{
					if(cond == 1)
						htmltext = "30512-03.htm";
					else if(cond == 2)
						htmltext = "30512-04.htm";
					else if(cond == 3)
						htmltext = "30512-06.htm";
					else if(cond == 5)
					{
						htmltext = "30512-07.htm";
						st.startQuestTimer("wait_timer", 1000);
					}
					break;
				}
				case Juris:
				{
					if(cond == 1)
						htmltext = "30113-01.htm";
					else if(cond == 2)
						htmltext = "30113-04.htm";
					break;
				}
				case Lorain:
				{
					if(cond == 3)
					{
						htmltext = "30673-01.htm";
						st.playSound("SOUND_MIDDLE");
						st.set("cond", "4");
					}
					else if(cond == 4)
						htmltext = "30673-02.htm";
					break;
				}
				case Nikola:
				{
					if(cond == 4)
					{
						htmltext = "30621-01.htm";
						st.playSound("SOUND_MIDDLE");
						st.set("cond", "5");
					}
					else if(cond == 5)
						htmltext = "30621-02.htm";
					break;
				}
			}
		return htmltext;
	}
}
