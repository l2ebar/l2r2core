package quests._146_TheZeroHour;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * @<!-- L2System -->
 * @date 14.12.2010
 * @time 12:22:30
 */
public class _146_TheZeroHour extends Quest implements ScriptFile
{
	// NPC's
	private static int KAHMAN = 31554;
	// ITEMS
	private static int FANG = 14859;
	private static final int KAHMAN_SUPPLY_BOX = 14849;

	private static final int QUEEN = 25671;

	private static int DROP_CHANCE = 100;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _146_TheZeroHour()
	{
		super(146, 0);

		addStartNpc(KAHMAN);
		addKillId(QUEEN);
		addQuestItem(FANG);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;

		if(event.equals("31554-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("31554-04.htm"))
			if(st.getQuestItemsCount(FANG) >= 1)
			{
				st.takeItems(FANG, 1);
				st.giveItems(KAHMAN_SUPPLY_BOX, 1);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "Not enough fang!";

		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		L2Player player = st.getPlayer();
		if(player == null)
			return null;

		QuestState InSearchOfTheNest = player.getQuestState("_109_InSearchOfTheNest");
		if(cond == 0)
		{
			if(player.getLevel() >= 81)
				if(InSearchOfTheNest != null && InSearchOfTheNest.isCompleted() || player.isGM())
					htmltext = "31554-01.htm";
				else
				{
					htmltext = "31554-00.htm";
					st.exitCurrentQuest(true);
				}
		}
		else if(st.getQuestItemsCount(FANG) >= 1)
			htmltext = "31554-03.htm";
		else
			htmltext = "31554-02.htm";

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getInt("cond") == 1)
		{
			st.rollAndGive(FANG, 1, DROP_CHANCE);
			st.set("cond", "2");
		}
		return null;
	}
}
