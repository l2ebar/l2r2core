package quests._10271_TheEnvelopingDarkness;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _10271_TheEnvelopingDarkness extends Quest implements ScriptFile
{
	// NPC'S
	private final static int ORBYU = 32560;
	private final static int EL = 32556;
	private final static int MEDIBAL = 32528;

	// ITEMS
	private final static int INSPECTOR_MEDIBALS_DOCUMENT = 13852;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _10271_TheEnvelopingDarkness()
	{
		super(10271, PARTY_NONE);
		addStartNpc(ORBYU);
		addTalkId(EL, MEDIBAL);
		addQuestItem(INSPECTOR_MEDIBALS_DOCUMENT);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32560-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32556-02.htm"))
		{
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32556-05.htm"))
		{
			st.set("cond", "4");
			st.playSound(SOUND_MIDDLE);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == ORBYU)
		{
			if(st.getState() == CREATED)
			{
				if(st.getPlayer().getLevel() >= 75)
					htmltext = "32560-01.htm";
				else
				{
					htmltext = "32560-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond >= 1 && cond <= 3)
				htmltext = "32560-03.htm";
			else if(cond == 4 && st.getQuestItemsCount(INSPECTOR_MEDIBALS_DOCUMENT) >= 1)
			{
				st.takeItems(INSPECTOR_MEDIBALS_DOCUMENT, 1);
				htmltext = "32560-04.htm";
				st.addExpAndSp(1109665, 1229015);
				st.giveItems(57, 236510);
				st.setState(COMPLETED);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
		}
		else if(npcId == EL)
		{
			if(cond == 1)
				htmltext = "32556-01.htm";
			else if(cond == 2)
				htmltext = "32556-03.htm";
			else if(cond == 3)
				htmltext = "32556-04.htm";
			else if(cond == 4)
				htmltext = "32556-06.htm";
		}
		else if(npcId == MEDIBAL)
			if(cond == 2)
			{
				st.set("cond", "3");
				st.giveItems(INSPECTOR_MEDIBALS_DOCUMENT, 1);
				htmltext = "32528-01.htm";
			}
			else
				htmltext = "32528-02.htm";
		return htmltext;
	}
}
