package quests._372_LegacyOfInsolence;

import javolution.util.FastMap;
import l2n.Config;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _372_LegacyOfInsolence extends Quest implements ScriptFile
{
	// NPCs
	private static int HOLLY = 30839;
	private static int WALDERAL = 30844;
	private static int DESMOND = 30855;
	private static int PATRIN = 30929;
	private static int CLAUDIA = 31001;
	// Mobs
	private static int CORRUPT_SAGE = 20817;
	private static int ERIN_EDIUNCE = 20821;
	private static int HALLATE_INSP = 20825;
	private static int PLATINUM_OVL = 20829;
	private static int PLATINUM_PRE = 21069;
	private static int MESSENGER_A1 = 21062;
	private static int MESSENGER_A2 = 21063;
	// Items
	private static int ADENA = 57;
	private static int Ancient_Red_Papyrus = 5966;
	private static int Ancient_Blue_Papyrus = 5967;
	private static int Ancient_Black_Papyrus = 5968;
	private static int Ancient_White_Papyrus = 5969;

	private static int[] Revelation_of_the_Seals_Range = { 5972, 5978 };
	private static int[] Ancient_Epic_Chapter_Range = { 5979, 5983 };
	private static int[] Imperial_Genealogy_Range = { 5984, 5988 };
	private static int[] Blueprint_Tower_of_Insolence_Range = { 5989, 6001 };

	// Rewards items
	private static int[] REWARD_DARK_CRYSTAL = { 5525, 5508, 5496 };
	private static int[] REWARD_TALLUM = { 5526, 5509, 5497 };
	private static int[] REWARD_NIGHTMARE = { 5527, 5514, 5502 };
	private static int[] REWARD_MAJESTIC = { 5528, 5515, 5503 };
	// Rewards items with Recipes
	private static int[] REWARD_DARK_CRYSTAL_WITH_RECIPES = { 5368, 5392, 5426, 5525, 5508, 5496 };
	private static int[] REWARD_TALLUM_WITH_RECIPES = { 5370, 5394, 5428, 5526, 5509, 5497 };
	private static int[] REWARD_NIGHTMARE_WITH_RECIPES = { 5380, 5404, 5430, 5527, 5514, 5502 };
	private static int[] REWARD_MAJESTIC_WITH_RECIPES = { 5382, 5406, 5432, 5528, 5515, 5503 };

	private static int[] RECIPES = { 5368, 5392, 5426, 5370, 5394, 5428, 5380, 5404, 5430, 5382, 5406, 5432 };

	// Chances
	private static int Three_Recipes_Reward_Chance = 1;
	private static int Two_Recipes_Reward_Chance = 2;
	private static int Adena4k_Reward_Chance = 2;

	private final FastMap<Integer, int[]> DROPLIST = new FastMap<Integer, int[]>();

	public _372_LegacyOfInsolence()
	{
		super(372, 0);
		addStartNpc(WALDERAL);

		addTalkId(HOLLY);
		addTalkId(DESMOND);
		addTalkId(PATRIN);
		addTalkId(CLAUDIA);

		addKillId(CORRUPT_SAGE);
		addKillId(ERIN_EDIUNCE);
		addKillId(HALLATE_INSP);
		addKillId(PLATINUM_OVL);
		addKillId(PLATINUM_PRE);
		addKillId(MESSENGER_A1);
		addKillId(MESSENGER_A2);

		DROPLIST.put(CORRUPT_SAGE, new int[] { Ancient_Red_Papyrus, 35 });
		DROPLIST.put(ERIN_EDIUNCE, new int[] { Ancient_Red_Papyrus, 40 });
		DROPLIST.put(HALLATE_INSP, new int[] { Ancient_Red_Papyrus, 45 });
		DROPLIST.put(PLATINUM_OVL, new int[] { Ancient_Blue_Papyrus, 40 });
		DROPLIST.put(PLATINUM_PRE, new int[] { Ancient_Black_Papyrus, 25 });
		DROPLIST.put(MESSENGER_A1, new int[] { Ancient_White_Papyrus, 25 });
		DROPLIST.put(MESSENGER_A2, new int[] { Ancient_White_Papyrus, 25 });
	}

	private static void giveRecipe(QuestState st, int recipe_id)
	{
		st.giveItems(Config.ALT_100_RECIPES ? recipe_id + 1 : recipe_id, 1, true);
	}

	private static boolean in(int[] reward, int reward_item)
	{
		for(int item : reward)
			if(item == reward_item)
				return true;
		return false;
	}

	private static boolean check_and_reward(QuestState st, int[] items_range, int[] reward, boolean isBlueprint)
	{
		for(int item_id = items_range[0]; item_id <= items_range[1]; item_id++)
			if(st.getQuestItemsCount(item_id) < 1)
				return false;

		for(int item_id = items_range[0]; item_id <= items_range[1]; item_id++)
			st.takeItems(item_id, 1);

		if(isBlueprint)
		{
			if(Rnd.chance(Three_Recipes_Reward_Chance)) // Выдаём 3 рецепта
			{
				for(int reward_item_id : reward)
					if(in(RECIPES, reward_item_id))
						giveRecipe(st, reward_item_id);
				st.playSound(SOUND_JACKPOT);
			}
			else if(Rnd.chance(Two_Recipes_Reward_Chance)) // Выдаём 2 рецепта
			{
				int num = 0;

				for(int reward_item_id : reward)
				{
					if(in(RECIPES, reward_item_id))
					{
						giveRecipe(st, reward_item_id);
						num++;
					}
					st.playSound(SOUND_JACKPOT);

					if(num >= 2)
						break;
				}
			}
			else
			{
				int num = Rnd.get(reward.length); // Получаем рандомно коллУчество выдаваемых Утемов.
				int reward_id = reward[Rnd.get(reward.length)]; // Получаем рандомно Утем Уз спУска наград

				if(in(RECIPES, reward_id))
					giveRecipe(st, reward[Rnd.get(reward.length)]);
				else
					st.giveItems(reward_id, num, true);
			}
		}
		else
		{
			int num = Rnd.get(reward.length); // Получаем рандомно коллУчество выдаваемых Утемов.
			int reward_id = reward[Rnd.get(reward.length)]; // Получаем рандомно Утем Уз спУска наград
			st.giveItems(reward_id, num, true); // Выдаем прУз с учётом рейтов.

			if(Rnd.chance(Adena4k_Reward_Chance))
				st.giveItems(ADENA, 4000, false);
		}
		return true;
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		int _state = st.getState();
		if(event.equalsIgnoreCase("30844-6.htm") && _state == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30844-9.htm") && _state == STARTED)
			st.set("cond", "2");
		else if(event.equalsIgnoreCase("30844-7.htm") && _state == STARTED)
		{
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		else if(event.equalsIgnoreCase("30839-exchange") && _state == STARTED)
			htmltext = check_and_reward(st, Imperial_Genealogy_Range, REWARD_DARK_CRYSTAL, false) ? "30839-2.htm" : "30839-3.htm";
		else if(event.equalsIgnoreCase("30855-exchange") && _state == STARTED)
			htmltext = check_and_reward(st, Revelation_of_the_Seals_Range, REWARD_MAJESTIC, false) ? "30855-2.htm" : "30855-3.htm";
		else if(event.equalsIgnoreCase("30929-exchange") && _state == STARTED)
			htmltext = check_and_reward(st, Ancient_Epic_Chapter_Range, REWARD_TALLUM, false) ? "30839-2.htm" : "30839-3.htm";
		else if(event.equalsIgnoreCase("31001-exchange") && _state == STARTED)
			htmltext = check_and_reward(st, Revelation_of_the_Seals_Range, REWARD_NIGHTMARE, false) ? "30839-2.htm" : "30839-3.htm";
		else if(event.equalsIgnoreCase("30844-DarkCrystal") && _state == STARTED)
			htmltext = check_and_reward(st, Blueprint_Tower_of_Insolence_Range, REWARD_DARK_CRYSTAL_WITH_RECIPES, true) ? "30844-11.htm" : "30844-12.htm";
		else if(event.equalsIgnoreCase("30844-Tallum") && _state == STARTED)
			htmltext = check_and_reward(st, Blueprint_Tower_of_Insolence_Range, REWARD_TALLUM_WITH_RECIPES, true) ? "30844-11.htm" : "30844-12.htm";
		else if(event.equalsIgnoreCase("30844-Nightmare") && _state == STARTED)
			htmltext = check_and_reward(st, Blueprint_Tower_of_Insolence_Range, REWARD_NIGHTMARE_WITH_RECIPES, true) ? "30844-11.htm" : "30844-12.htm";
		else if(event.equalsIgnoreCase("30844-Majestic") && _state == STARTED)
			htmltext = check_and_reward(st, Blueprint_Tower_of_Insolence_Range, REWARD_MAJESTIC_WITH_RECIPES, true) ? "30844-11.htm" : "30844-12.htm";

		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int _state = st.getState();
		int npcId = npc.getNpcId();

		if(_state == CREATED)
		{
			if(npcId != WALDERAL)
				return htmltext;
			if(st.getPlayer().getLevel() >= 59)
			{
				htmltext = "30844-4.htm";
				st.set("cond", "0");
			}
			else
			{
				htmltext = "30844-5.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(_state == STARTED)
			htmltext = String.valueOf(npcId) + "-1.htm";

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		if(qs.getState() != STARTED)
			return null;

		int[] drop = DROPLIST.get(npc.getNpcId());
		if(drop == null)
			return null;

		qs.rollAndGive(drop[0], 1, drop[1]);
		return null;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 372: Legacy of Insolence");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
