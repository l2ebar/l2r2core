package quests._659_IdRatherBeCollectingFairyBreath;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _659_IdRatherBeCollectingFairyBreath extends Quest implements ScriptFile
{
	// NPC
	public final int GALATEA = 30634;
	// Mobs
	public final int[] MOBS = { 20078, 21026, 21025, 21024, 21023 };
	// Quest Item
	public final int FAIRY_BREATH = 8286;
	// Item
	public final int ADENA = 57;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 659: I'd Rather Be Collecting Fairy Breath");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _659_IdRatherBeCollectingFairyBreath()
	{
		super(659, -1);

		addStartNpc(GALATEA);
		addTalkId(GALATEA);
		addTalkId(GALATEA);
		addTalkId(GALATEA);

		for(int i : MOBS)
			addKillId(i);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30634-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30634-06.htm"))
		{
			long count = st.getQuestItemsCount(FAIRY_BREATH);
			if(count > 0)
			{
				long reward = 0;
				if(count < 10)
					reward = count * 50;
				else
					reward = count * 50 + 5365;
				st.takeItems(FAIRY_BREATH, -1);
				st.giveItems(ADENA, reward);
			}
		}
		else if(event.equalsIgnoreCase("30634-08.htm"))
			st.exitCurrentQuest(true);
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int id = st.getState();
		int cond = 0;
		if(id != CREATED)
			cond = st.getInt("cond");
		if(npcId == GALATEA)
			if(st.getPlayer().getLevel() < 26)
			{
				htmltext = "30634-01.htm";
				st.exitCurrentQuest(true);
			}
			else if(cond == 0)
				htmltext = "30634-02.htm";
			else if(cond == 1)
				if(st.getQuestItemsCount(FAIRY_BREATH) == 0)
					htmltext = "30634-04.htm";
				else
					htmltext = "30634-05.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(cond == 1)
			for(int i : MOBS)
				if(npcId == i && Rnd.chance(30))
				{
					st.giveItems(FAIRY_BREATH, 1);
					st.playSound(SOUND_ITEMGET);
				}
		return null;
	}
}
