package quests._053_LinnaeusSpecialBait;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _053_LinnaeusSpecialBait extends Quest implements ScriptFile
{
	private static int Linnaeu = 31577;
	private static int CrimsonDrake = 20670;
	private static int HeartOfCrimsonDrake = 7624;
	private static int FlameFishingLure = 7613;
	Integer FishSkill = 1315;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _053_LinnaeusSpecialBait()
	{
		super(53, -1);

		addStartNpc(Linnaeu);

		addTalkId(Linnaeu);

		addKillId(CrimsonDrake);

		addQuestItem(HeartOfCrimsonDrake);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("31577-04.htm"))
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("31577-07.htm"))
			if(st.getQuestItemsCount(HeartOfCrimsonDrake) < 100)
				htmltext = "31577-08.htm";
			else
			{
				st.unset("cond");
				st.takeItems(HeartOfCrimsonDrake, -1);
				st.giveItems(FlameFishingLure, 4);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		int id = st.getState();
		if(npcId == Linnaeu)
			if(id == CREATED)
			{
				if(st.getPlayer().getLevel() < 60)
				{
					htmltext = "31577-03.htm";
					st.exitCurrentQuest(true);
				}
				else if(st.getPlayer().getSkillLevel(FishSkill) >= 21)
					htmltext = "31577-01.htm";
				else
				{
					htmltext = "31577-02.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1 || cond == 2)
				if(st.getQuestItemsCount(HeartOfCrimsonDrake) < 100)
				{
					htmltext = "31577-06.htm";
					st.set("cond", "1");
				}
				else
					htmltext = "31577-05.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(npcId == CrimsonDrake && st.getInt("cond") == 1)
			if(st.getQuestItemsCount(HeartOfCrimsonDrake) < 100 && Rnd.chance(30))
			{
				st.giveItems(HeartOfCrimsonDrake, 1);
				if(st.getQuestItemsCount(HeartOfCrimsonDrake) == 100)
				{
					st.playSound(SOUND_MIDDLE);
					st.set("cond", "2");
				}
				else
					st.playSound(SOUND_ITEMGET);
			}
		return null;
	}
}
