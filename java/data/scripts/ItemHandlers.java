package data.scripts;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.game.cache.Msg;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExPCCafePointInfo;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.ShowXMasSeal;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.SkillTable;
import l2n.util.Rnd;

import java.util.Random;

public class ItemHandlers extends Functions {
	public void ItemHandler_19999(L2Player player) {
		show("data/html/newspaper/00000000.htm", player);
	}

	public void ItemHandler_5555(L2Player player) {
		player.sendPacket(new ShowXMasSeal(5555));
	}

	public void ItemHandler_8534(L2Player player) {
		if (!canBeExtracted(8534, player))
			return;
		int[] list = new int[]{853, 916, 884};
		int[] counts = new int[]{1, 1, 1};
		int[] chances = new int[]{15, 15, 15};
		removeItem(player, 8534, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_8535(L2Player player) {
		if (!canBeExtracted(8535, player))
			return;
		int[] list = new int[]{854, 917, 885};
		int[] counts = new int[]{1, 1, 1};
		int[] chances = new int[]{15, 15, 15};
		removeItem(player, 8535, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_8536(L2Player player) {
		if (!canBeExtracted(8536, player))
			return;
		int[] list = new int[]{855, 119, 886};
		int[] counts = new int[]{1, 1, 1};
		int[] chances = new int[]{15, 15, 15};
		removeItem(player, 8536, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_8537(L2Player player) {
		if (!canBeExtracted(8537, player))
			return;
		int[] list = new int[]{856, 918, 887};
		int[] counts = new int[]{1, 1, 1};
		int[] chances = new int[]{15, 15, 15};
		removeItem(player, 8537, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_8538(L2Player player) {
		if (!canBeExtracted(8538, player))
			return;
		int[] list = new int[]{864, 926, 895};
		int[] counts = new int[]{1, 1, 1};
		int[] chances = new int[]{15, 15, 15};
		removeItem(player, 8538, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_8539(L2Player player) {
		if (!canBeExtracted(8539, player))
			return;
		int[] list = new int[]{8179, 8178, 8177};
		int[] counts = new int[]{1, 1, 1};
		int[] chances = new int[]{10, 20, 30};
		removeItem(player, 8539, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_8540(L2Player player) {
		removeItem(player, 8540, 1);
		if (Rnd.chance(30))
			addItem(player, 8175, 1);
	}

	public void ItemHandler_9104(L2Player player) {
		if (!canBeExtracted(9104, player))
			return;
		int[] list = new int[]{8980, 17};
		int[] counts = new int[]{1, 1000};
		removeItem(player, 9104, 1);
		extract_item(list, counts, player);
	}

	public void ItemHandler_9105(L2Player player) {
		if (!canBeExtracted(9105, player))
			return;
		int[] list = new int[]{8989, 1341};
		int[] counts = new int[]{1, 1000};
		removeItem(player, 9105, 1);
		extract_item(list, counts, player);
	}

	public void ItemHandler_9106(L2Player player) {
		if (!canBeExtracted(9106, player))
			return;
		int[] list = new int[]{8997, 1342};
		int[] counts = new int[]{1, 1000};
		removeItem(player, 9106, 1);
		extract_item(list, counts, player);
	}

	public void ItemHandler_9107(L2Player player) {
		if (!canBeExtracted(9107, player))
			return;
		int[] list = new int[]{9008, 1343};
		int[] counts = new int[]{1, 1000};
		removeItem(player, 9107, 1);
		extract_item(list, counts, player);
	}

	public void ItemHandler_9108(L2Player player) {
		if (!canBeExtracted(9108, player))
			return;
		int[] list = new int[]{9016, 1344};
		int[] counts = new int[]{1, 1000};
		removeItem(player, 9108, 1);
		extract_item(list, counts, player);
	}

	public void ItemHandler_9109(L2Player player) {
		if (!canBeExtracted(9109, player))
			return;
		int[] list = new int[]{9027, 1344};
		int[] counts = new int[]{1, 1000};
		removeItem(player, 9109, 1);
		extract_item(list, counts, player);
	}

	public void ItemHandler_5916(L2Player player) {
		if (!canBeExtracted(5916, player))
			return;
		int[] list = new int[]{5917, 5918, 5919, 5920, 736};
		int[] counts = new int[]{1, 1, 1, 1, 1};
		removeItem(player, 5916, 1);
		extract_item(list, counts, player);
	}

	// Quest 376: Giants Cave Exploration, Part 1
	public void ItemHandler_5944(L2Player player) {
		if (!canBeExtracted(5944, player))
			return;
		int[] list = {5922, 5923, 5924, 5925, 5926, 5927, 5928, 5929, 5930, 5931, 5932, 5933, 5934, 5935, 5936, 5937, 5938, 5939, 5940, 5941, 5942, 5943};
		int[] counts = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

		removeItem(player, 5944, 1);
		extract_item(list, counts, player);
	}

	public void ItemHandler_14559(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14559, player))
			return;
		removeItem(player, 14559, 1);
		player.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_ACQUIRED_S1_PC_CAFE_POINTS).addNumber(7000));
		player.setPcBangPoints(player.getPcBangPoints() + 7000);
		player.sendPacket(new ExPCCafePointInfo(player));
	}

	public void ItemHandler_14841(L2Player player) {
		if (!canBeExtracted(14841, player))
			return;

		int[] list = {14836, 14837, 14838, 14839, 14840};
		int[] counts = {1, 1, 1, 1, 1};

		removeItem(player, 14841, 1);
		extract_item(list, counts, player);
	}

	public void ItemHandler_5955(L2Player player) {
		if (!canBeExtracted(5955, player))
			return;
		int[] list = {5942, 5943, 5945, 5946, 5947, 5948, 5949, 5950, 5951, 5952, 5953, 5954};
		int[] counts = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

		removeItem(player, 5955, 1);
		extract_item(list, counts, player);
	}

	public void ItemHandler_14847(L2Player player) {
		if (!canBeExtracted(14847, player))
			return;
		int[] list = {14842, 14843, 14844, 14845, 14846};
		int[] counts = {1, 1, 1, 1, 1};

		removeItem(player, 14847, 1);
		extract_item(list, counts, player);
	}

	public void ItemHandler_5966(L2Player player) {
		if (!canBeExtracted(5966, player))
			return;
		int[] list = new int[]{5970, 5971, 5977, 5978, 5979, 5986, 5993, 5994, 5995, 5997, 5983, 6001};
		int[] counts = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
		removeItem(player, 5966, 1);
		extract_item(list, counts, player);
	}

	public void ItemHandler_5967(L2Player player) {
		if (!canBeExtracted(5967, player))
			return;
		int[] list = new int[]{5970, 5971, 5975, 5976, 5980, 5985, 5993, 5994, 5995, 5997, 5983, 6001};
		int[] counts = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
		removeItem(player, 5967, 1);
		extract_item(list, counts, player);
	}

	public void ItemHandler_5968(L2Player player) {
		if (!canBeExtracted(5968, player))
			return;
		int[] list = new int[]{5973, 5974, 5981, 5984, 5989, 5990, 5991, 5992, 5996, 5998, 5999, 6000, 5988, 5983, 6001};
		int[] counts = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
		removeItem(player, 5968, 1);
		extract_item(list, counts, player);
	}

	public void ItemHandler_5969(L2Player player) {
		if (!canBeExtracted(5969, player))
			return;
		int[] list = new int[]{5970, 5971, 5982, 5987, 5989, 5990, 5991, 5992, 5996, 5998, 5999, 6000, 5972, 6001};
		int[] counts = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
		removeItem(player, 5969, 1);
		extract_item(list, counts, player);
	}

	public void ItemHandler_6007(L2Player player) {
		if (!canBeExtracted(6007, player))
			return;
		int[] list = new int[]{6013, 6014, 6016, 6019};
		int[] counts = new int[]{2, 1, 1, 2};
		int[] chances = new int[]{30, 20, 20, 30};
		removeItem(player, 6007, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_6008(L2Player player) {
		if (!canBeExtracted(6008, player))
			return;
		int[] list = new int[]{6017, 6020, 6014, 6016};
		int[] counts = new int[]{2, 2, 1, 1};
		int[] chances = new int[]{10, 20, 35, 35};
		removeItem(player, 6008, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_6009(L2Player player) {
		if (!canBeExtracted(6009, player))
			return;
		int[] list = new int[]{6012, 6018, 6019, 6013};
		int[] counts = new int[]{1, 2, 2, 1};
		int[] chances = new int[]{20, 20, 20, 40};
		removeItem(player, 6009, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_6010(L2Player player) {
		if (!canBeExtracted(6010, player))
			return;
		int[] list = new int[]{6017, 6020, 6015, 6016};
		int[] counts = new int[]{2, 2, 2, 1};
		int[] chances = new int[]{20, 20, 25, 35};
		removeItem(player, 6010, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_7629(L2Player player) {
		if (!canBeExtracted(7629, player))
			return;
		int[] list = new int[]{6688, 6689, 6690, 6691, 6693, 6694, 6695, 6696, 6697, 7579, 57};
		int[] counts = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 330000};
		int[] chances = new int[]{9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10};
		removeItem(player, 7629, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_7630(L2Player player) {
		if (!canBeExtracted(7630, player))
			return;
		int[] list = new int[]{6703, 6704, 6705, 6706, 6708, 6709, 6710, 6712, 6713, 6714, 57};
		int[] counts = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 292000};
		int[] chances = new int[]{8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 20};
		removeItem(player, 7630, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_7631(L2Player player) {
		if (!canBeExtracted(7631, player))
			return;
		int[] list = new int[]{6701, 6702, 6707, 6711, 57};
		int[] counts = new int[]{1, 1, 1, 1, 930000};
		int[] chances = new int[]{20, 20, 20, 20, 20};
		removeItem(player, 7631, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_7632(L2Player player) {
		if (!canBeExtracted(7632, player))
			return;
		int[] list = new int[]{6857, 6859, 6861, 6863, 6867, 6869, 6871, 6875, 6877, 6879, 57};
		int[] counts = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 340000};
		int[] chances = new int[]{8, 9, 8, 9, 8, 9, 8, 9, 8, 9, 15};
		removeItem(player, 7632, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_7633(L2Player player) {
		if (!canBeExtracted(7633, player))
			return;
		int[] list = new int[]{6853, 6855, 6865, 6873, 57};
		int[] counts = new int[]{1, 1, 1, 1, 850000};
		int[] chances = new int[]{20, 20, 20, 20, 20};
		removeItem(player, 7633, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_7634(L2Player player) {
		if (!canBeExtracted(7634, player))
			return;
		int[] list = new int[]{1874, 1875, 1876, 1877, 1879, 1880, 1881, 1882, 57};
		int[] counts = new int[]{20, 20, 20, 20, 20, 20, 20, 20, 150000};
		int[] chances = new int[]{10, 10, 16, 11, 10, 5, 10, 18, 10};
		removeItem(player, 7634, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_7635(L2Player player) {
		if (!canBeExtracted(7635, player))
			return;
		int[] list = new int[]{4039, 4040, 4041, 4042, 4043, 4044, 57};
		int[] counts = new int[]{4, 4, 4, 4, 4, 4, 160000};
		int[] chances = new int[]{20, 10, 10, 10, 20, 20, 10};
		removeItem(player, 7635, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_7636(L2Player player) {
		if (!canBeExtracted(7636, player))
			return;
		int[] list = new int[]{1875, 1882, 1880, 1874, 1877, 1881, 1879, 1876};
		int[] counts = new int[]{3, 3, 4, 1, 3, 1, 3, 6};
		int[] chances = new int[]{10, 20, 10, 10, 10, 12, 12, 16};
		removeItem(player, 7636, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_7637(L2Player player) {
		if (!canBeExtracted(7637, player))
			return;
		int[] list = new int[]{4039, 4041, 4043, 4044, 4042, 4040};
		int[] counts = new int[]{4, 1, 4, 4, 2, 2};
		int[] chances = new int[]{20, 10, 20, 20, 15, 15};
		removeItem(player, 7637, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_7725(L2Player player) {
		if (!canBeExtracted(7725, player))
			return;
		int[] list = new int[]{6035, 1060, 735, 1540, 1061, 1539};
		int[] counts = new int[]{1, 1, 1, 1, 1, 1};
		int[] chances = new int[]{7, 39, 7, 3, 12, 32};
		removeItem(player, 7725, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_9599(L2Player player) {
		if (!canBeExtracted(9599, player))
			return;

		int[] list = new int[]{9600, 9601, 9602};
		int[] counts = new int[]{1, 1, 1};
		int[] chances = new int[]{10, 10, 10};
		removeItem(player, 9599, 1);
		extract_item_r(list, counts, chances, player);
	}

	public void ItemHandler_10408(L2Player player) {
		if (!canBeExtracted(10408, player))
			return;
		removeItem(player, 10408, 1);
		addItem(player, 6471, 20);
		addItem(player, 5094, 40);
		addItem(player, 9814, 3);
		addItem(player, 9816, 4);
		addItem(player, 9817, 4);
		addItem(player, 9815, 2);
		addItem(player, 57, 6000000);
	}

	public void ItemHandler_10473(L2Player player) {
		if (!canBeExtracted(10473, player))
			return;
		removeItem(player, 10473, 1);
		addItem(player, 10470, 2);
		addItem(player, 10471, 2);
		addItem(player, 10472, 1);
	}

	private final int[] sweet_list = {
			// Sweet Fruit Cocktail
			2404, // Might
			2405, // Shield
			2406, // Wind Walk
			2407, // Focus
			2408, // Death Whisper
			2409, // Guidance
			2410, // Bless Shield
			2411, // Bless Body
			2412, // Haste
			2413, // Vampiric Rage
	};

	// Sweet Fruit Cocktail
	public void ItemHandler_10178(L2Player player) {
		L2Player p = player;
		if (p.isInZone(ZoneType.OlympiadStadia))
			return;
		removeItem(p, 10178, 1);
		for (int skill : sweet_list) {
			p.broadcastPacket(new MagicSkillUse(p, p, skill, 1, 0, 0));
			p.altOnMagicUseTimer(p, SkillTable.getInstance().getInfo(skill, 1));
		}
	}

	private final int[] fresh_list = {
			// Fresh Fruit Cocktail
			2414, // Berserker Spirit
			2411, // Bless Body
			2415, // Magic Barrier
			2405, // Shield
			2406, // Wind Walk
			2416, // Bless Soul
			2417, // Empower
			2418, // Acumen
			2419, // Clarity
	};

	// Fresh Fruit Cocktail
	public void ItemHandler_10179(L2Player player) {
		L2Player p = player;
		if (p.isInZone(ZoneType.OlympiadStadia))
			return;
		removeItem(p, 10179, 1);
		for (int skill : fresh_list) {
			p.broadcastPacket(new MagicSkillUse(p, p, skill, 1, 0, 0));
			p.altOnMagicUseTimer(p, SkillTable.getInstance().getInfo(skill, 1));
		}
	}

	// Battleground Spell - Shield Master
	public void ItemHandler_10143(L2Player player) {
		if (!player.isInZone(ZoneType.Siege))
			return;
		removeItem(player, 10143, 1);
		for (int skill : new int[]{2379, 2380, 2381, 2382, 2383}) {
			player.broadcastPacket(new MagicSkillUse(player, player, skill, 1, 0, 0));
			player.altOnMagicUseTimer(player, SkillTable.getInstance().getInfo(skill, 1));
		}
	}

	// Battleground Spell - Wizard
	public void ItemHandler_10144(L2Player player) {
		if (!player.isInZone(ZoneType.Siege))
			return;
		removeItem(player, 10144, 1);
		for (int skill : new int[]{2379, 2380, 2381, 2384, 2385}) {
			player.broadcastPacket(new MagicSkillUse(player, player, skill, 1, 0, 0));
			player.altOnMagicUseTimer(player, SkillTable.getInstance().getInfo(skill, 1));
		}
	}

	// Battleground Spell - Healer
	public void ItemHandler_10145(L2Player player) {
		if (!player.isInZone(ZoneType.Siege))
			return;
		removeItem(player, 10145, 1);
		for (int skill : new int[]{2379, 2380, 2381, 2384, 2386}) {
			player.broadcastPacket(new MagicSkillUse(player, player, skill, 1, 0, 0));
			player.altOnMagicUseTimer(player, SkillTable.getInstance().getInfo(skill, 1));
		}
	}

	// Battleground Spell - Dagger Master
	public void ItemHandler_10146(L2Player player) {
		if (!player.isInZone(ZoneType.Siege))
			return;
		removeItem(player, 10146, 1);
		for (int skill : new int[]{2379, 2380, 2381, 2388, 2383}) {
			player.broadcastPacket(new MagicSkillUse(player, player, skill, 1, 0, 0));
			player.altOnMagicUseTimer(player, SkillTable.getInstance().getInfo(skill, 1));
		}
	}

	// Battleground Spell - Bow Master
	public void ItemHandler_10147(L2Player player) {
		if (!player.isInZone(ZoneType.Siege))
			return;
		removeItem(player, 10147, 1);
		for (int skill : new int[]{2379, 2380, 2381, 2389, 2383}) {
			player.broadcastPacket(new MagicSkillUse(player, player, skill, 1, 0, 0));
			player.altOnMagicUseTimer(player, SkillTable.getInstance().getInfo(skill, 1));
		}
	}

	// Battleground Spell - Berserker
	public void ItemHandler_10148(L2Player player) {
		if (!player.isInZone(ZoneType.Siege))
			return;
		removeItem(player, 10148, 1);
		for (int skill : new int[]{2390, 2391}) {
			player.broadcastPacket(new MagicSkillUse(player, player, skill, 1, 0, 0));
			player.altOnMagicUseTimer(player, SkillTable.getInstance().getInfo(skill, 1));
		}
	}

	// Majo Agathion Pack
	public void ItemHandler_20035(L2Player player) {
		if (!canBeExtracted(20035, player))
			return;
		removeItem(player, 20035, 1);
		addItem(player, 20006, 1);
	}

	// Gold Crown Majo Agathion Pack
	public void ItemHandler_20036(L2Player player) {
		if (!canBeExtracted(20036, player))
			return;
		removeItem(player, 20036, 1);
		addItem(player, 20007, 1);
	}

	// Black Crown Majo Agathion Pack
	public void ItemHandler_20037(L2Player player) {
		if (!canBeExtracted(20037, player))
			return;
		removeItem(player, 20037, 1);
		addItem(player, 20008, 1);
	}

	// Majo Agathion 30-Day Pack
	public void ItemHandler_20038(L2Player player) {
		if (!canBeExtracted(20038, player))
			return;
		removeItem(player, 20038, 1);
		addItem(player, 20009, 1);
	}

	// Gold Crown Majo Agathion 30-Day Pack
	public void ItemHandler_20039(L2Player player) {
		if (!canBeExtracted(20039, player))
			return;
		removeItem(player, 20039, 1);
		addItem(player, 20010, 1);
	}

	// Black Crown Majo Agathion 30-Day Pack
	public void ItemHandler_20040(L2Player player) {
		if (!canBeExtracted(20040, player))
			return;
		removeItem(player, 20040, 1);
		addItem(player, 20011, 1);
	}

	// Plaipitak Agathion Pack
	public void ItemHandler_20041(L2Player player) {
		if (!canBeExtracted(20041, player))
			return;
		removeItem(player, 20041, 1);
		addItem(player, 20012, 1);
	}

	// Plaipitak Agathion 30-Day Pack
	public void ItemHandler_20042(L2Player player) {
		if (!canBeExtracted(20042, player))
			return;
		removeItem(player, 20042, 1);
		addItem(player, 20013, 1);
	}

	// Plaipitak Agathion 30-Day Pack
	public void ItemHandler_20043(L2Player player) {
		if (!canBeExtracted(20043, player))
			return;
		removeItem(player, 20043, 1);
		addItem(player, 20014, 1);
	}

	// Plaipitak Agathion 30-Day Pack
	public void ItemHandler_20044(L2Player player) {
		if (!canBeExtracted(20044, player))
			return;
		removeItem(player, 20044, 1);
		addItem(player, 20015, 1);
	}

	// Kat the Cat Hat Pack
	public void ItemHandler_20060(L2Player player) {
		if (!canBeExtracted(20060, player))
			return;
		removeItem(player, 20060, 1);
		addItem(player, 20031, 1);
	}

	// Skull Hat Pack
	public void ItemHandler_20061(L2Player player) {
		if (!canBeExtracted(20061, player))
			return;
		removeItem(player, 20061, 1);
		addItem(player, 20032, 1);
	}

	// Baby Panda Agathion Pack
	public void ItemHandler_20069(L2Player player) {
		if (!canBeExtracted(20069, player))
			return;
		removeItem(player, 20069, 1);
		addItem(player, 20063, 1);
	}

	// Bamboo Panda Agathion Pack
	public void ItemHandler_20070(L2Player player) {
		if (!canBeExtracted(20070, player))
			return;
		removeItem(player, 20070, 1);
		addItem(player, 20064, 1);
	}

	// Sexy Panda Agathion Pack
	public void ItemHandler_20071(L2Player player) {
		if (!canBeExtracted(20071, player))
			return;
		removeItem(player, 20071, 1);
		addItem(player, 20065, 1);
	}

	// Agathion of Baby Panda 15 Day Pack
	public void ItemHandler_20072(L2Player player) {
		if (!canBeExtracted(20072, player))
			return;
		removeItem(player, 20072, 1);
		addItem(player, 20066, 1);
	}

	// Bamboo Panda Agathion 15 Day Pack
	public void ItemHandler_20073(L2Player player) {
		if (!canBeExtracted(20073, player))
			return;
		removeItem(player, 20073, 1);
		addItem(player, 20067, 1);
	}

	// Agathion of Sexy Panda 15 Day Pack
	public void ItemHandler_20074(L2Player player) {
		if (!canBeExtracted(20074, player))
			return;
		removeItem(player, 20074, 1);
		addItem(player, 20068, 1);
	}

	// The Valentine Event: Simple Valentine Cake
	public void ItemHandler_20195(L2Player player) {
		if (!canBeExtracted(20195, player))
			return;
		int[] list = new int[]{10138, 10134, 1540, 1539};
		int[] counts = new int[]{1, 1, 1, 1,};
		int[] chances = new int[]{10, 15, 30, 40};
		removeItem(player, 20195, 1);
		extract_item_r(list, counts, chances, player);
	}

	// The Valentine Event: Velvety Valentine Cake
	public void ItemHandler_20196(L2Player player) {
		if (!canBeExtracted(20196, player))
			return;
		int[] list = new int[]{952, 948, 1538, 3936, 20200};
		int[] counts = new int[]{1, 1, 1, 1,};
		int[] chances = new int[]{10, 15, 40, 30, 5};
		removeItem(player, 20196, 1);
		extract_item_r(list, counts, chances, player);
	}

	// The Valentine Event: Delectable Valentine Cake
	public void ItemHandler_20197(L2Player player) {
		if (!canBeExtracted(20197, player))
			return;
		int[] list = new int[]{947, 951, 20201};
		int[] counts = new int[]{1, 1, 1};
		int[] chances = new int[]{15, 25, 7};
		removeItem(player, 20197, 1);
		extract_item_r(list, counts, chances, player);
	}

	// The Valentine Event: Decadent Valentine Cake
	public void ItemHandler_20198(L2Player player) {
		if (!canBeExtracted(20198, player))
			return;
		int[] list = new int[]{959, 729, 20202, 20203};
		int[] counts = new int[]{1, 1, 1, 1};
		int[] chances = new int[]{1, 5, 8, 7};
		removeItem(player, 20198, 1);
		extract_item_r(list, counts, chances, player);
	}

	// Charming Valentine Gift Set
	public void ItemHandler_20210(L2Player player) {
		if (!canBeExtracted(20210, player))
			return;
		removeItem(player, 20210, 1);
		addItem(player, 20212, 1);
	}

	// Naughty Valentine Gift Set
	public void ItemHandler_20211(L2Player player) {
		if (!canBeExtracted(20211, player))
			return;
		removeItem(player, 20211, 1);
		addItem(player, 20213, 1);
	}

	// White Maneki Neko Agathion Pack
	public void ItemHandler_20215(L2Player player) {
		if (!canBeExtracted(20215, player))
			return;
		removeItem(player, 20215, 1);
		addItem(player, 20221, 1);
	}

	// Black Maneki Neko Agathion Pack
	public void ItemHandler_20216(L2Player player) {
		if (!canBeExtracted(20216, player))
			return;
		removeItem(player, 20216, 1);
		addItem(player, 20222, 1);
	}

	// Brown Maneki Neko Agathion Pack
	public void ItemHandler_20217(L2Player player) {
		if (!canBeExtracted(20217, player))
			return;
		removeItem(player, 20217, 1);
		addItem(player, 20223, 1);
	}

	// White Maneki Neko Agathion 7-Day Pack
	public void ItemHandler_20218(L2Player player) {
		if (!canBeExtracted(20218, player))
			return;
		removeItem(player, 20218, 1);
		addItem(player, 20224, 1);
	}

	// Black Maneki Neko Agathion 7-Day Pack
	public void ItemHandler_20219(L2Player player) {
		if (!canBeExtracted(20219, player))
			return;
		removeItem(player, 20219, 1);
		addItem(player, 20225, 1);
	}

	// Brown Maneki Neko Agathion 7-Day Pack
	public void ItemHandler_20220(L2Player player) {
		if (!canBeExtracted(20220, player))
			return;
		removeItem(player, 20220, 1);
		addItem(player, 20226, 1);
	}

	// One-Eyed Bat Drove Agathion Pack
	public void ItemHandler_20227(L2Player player) {
		if (!canBeExtracted(20227, player))
			return;
		removeItem(player, 20227, 1);
		addItem(player, 20230, 1);
	}

	// One-Eyed Bat Drove Agathion 7-Day Pack
	public void ItemHandler_20228(L2Player player) {
		if (!canBeExtracted(20228, player))
			return;
		removeItem(player, 20228, 1);
		addItem(player, 20231, 1);
	}

	// One-Eyed Bat Drove Agathion 7-Day Pack
	public void ItemHandler_20229(L2Player player) {
		if (!canBeExtracted(20229, player))
			return;
		removeItem(player, 20229, 1);
		addItem(player, 20232, 1);
	}

	// Pegasus Agathion Pack
	public void ItemHandler_20233(L2Player player) {
		if (!canBeExtracted(20233, player))
			return;
		removeItem(player, 20233, 1);
		addItem(player, 20236, 1);
	}

	// Pegasus Agathion 7-Day Pack
	public void ItemHandler_20234(L2Player player) {
		if (!canBeExtracted(20234, player))
			return;
		removeItem(player, 20234, 1);
		addItem(player, 20237, 1);
	}

	// Pegasus Agathion 7-Day Pack
	public void ItemHandler_20235(L2Player player) {
		if (!canBeExtracted(20235, player))
			return;
		removeItem(player, 20235, 1);
		addItem(player, 20238, 1);
	}

	// Yellow-Robed Tojigong Pack
	public void ItemHandler_20239(L2Player player) {
		if (!canBeExtracted(20239, player))
			return;
		removeItem(player, 20239, 1);
		addItem(player, 20245, 1);
	}

	// Blue-Robed Tojigong Pack
	public void ItemHandler_20240(L2Player player) {
		if (!canBeExtracted(20240, player))
			return;
		removeItem(player, 20240, 1);
		addItem(player, 20246, 1);
	}

	// Green-Robed Tojigong Pack
	public void ItemHandler_20241(L2Player player) {
		if (!canBeExtracted(20241, player))
			return;
		removeItem(player, 20241, 1);
		addItem(player, 20247, 1);
	}

	// Yellow-Robed Tojigong 7-Day Pack
	public void ItemHandler_20242(L2Player player) {
		if (!canBeExtracted(20242, player))
			return;
		removeItem(player, 20242, 1);
		addItem(player, 20248, 1);
	}

	// Blue-Robed Tojigong 7-Day Pack
	public void ItemHandler_20243(L2Player player) {
		if (!canBeExtracted(20243, player))
			return;
		removeItem(player, 20243, 1);
		addItem(player, 20249, 1);
	}

	// Green-Robed Tojigong 7-Day Pack
	public void ItemHandler_20244(L2Player player) {
		if (!canBeExtracted(20244, player))
			return;
		removeItem(player, 20244, 1);
		addItem(player, 20250, 1);
	}

	// Bugbear Agathion Pack
	public void ItemHandler_20251(L2Player player) {
		if (!canBeExtracted(20251, player))
			return;
		removeItem(player, 20251, 1);
		addItem(player, 20252, 1);
	}

	// Agathion of Love Pack (Event)
	public void ItemHandler_20254(L2Player player) {
		if (!canBeExtracted(20254, player))
			return;
		removeItem(player, 20254, 1);
		addItem(player, 20253, 1);
	}

	// Gold Afro Hair Pack
	public void ItemHandler_20278(L2Player player) {
		if (!canBeExtracted(20278, player))
			return;
		removeItem(player, 20278, 1);
		addItem(player, 20275, 1);
	}

	// Pink Afro Hair Pack
	public void ItemHandler_20279(L2Player player) {
		if (!canBeExtracted(20279, player))
			return;
		removeItem(player, 20279, 1);
		addItem(player, 20276, 1);
	}

	// S-Grade Accessory Chest(MasterOfEnchanting Event)
	private static final int[] SAccessoryChest = {6724, 6725, 6726};

	public void ItemHandler_13992(L2Player player) {
		if (!canBeExtracted(13992, player))
			return;
		removeItem(player, 13992, 1);
		addItem(player, SAccessoryChest[Rnd.get(SAccessoryChest.length)], 1);
	}

	// S-Grade Armor Chest(MasterOfEnchanting Event)
	private static final int[] SArmorChest = {6674, 6675, 6679, 6683, 6687, 6678, 6677, 6682, 6686, 6676, 6681, 6685, 9582, 10500, 10501, 10502, 6680, 6684};

	public void ItemHandler_13991(L2Player player) {
		if (!canBeExtracted(13991, player))
			return;
		removeItem(player, 13991, 1);
		addItem(player, SArmorChest[Rnd.get(SArmorChest.length)], 1);
	}

	// S-Grade Weapon Chest(MasterOfEnchanting Event)
	private static final int[] SWeaponChest = {6364, 6372, 6365, 6579, 6369, 6367, 6370, 6371, 7575, 6580};

	public void ItemHandler_13990(L2Player player) {
		if (!canBeExtracted(13990, player))
			return;
		removeItem(player, 13990, 1);
		addItem(player, SWeaponChest[Rnd.get(SWeaponChest.length)], 1);
	}

	// S80-Grade Armor Chest(MasterOfEnchanting Event)
	private static final int[] S80ArmorChest = {9514, 9515, 9516, 9517, 9518, 9519, 9520, 9521, 9522, 9523, 9524, 9525, 9526, 9527, 9528};
	private static final int[] S80ArmorChances = new int[]{6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7};
	private static final int[] S80ArmorCounts = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

	public void ItemHandler_13989(L2Player player) {
		if (!canBeExtracted(13989, player))
			return;

		removeItem(player, 13989, 1);
		extract_item(S80ArmorChest, S80ArmorCounts, S80ArmorChances, false, player);
	}

	// S80-Grade Weapon Chest(MasterOfEnchanting Event)
	private static final int[] S80WeaponChest = {9442, 9443, 9444, 9445, 9446, 9447, 9448, 9450, 10252, 10253, 10215, 10216, 10217, 10218, 10219, 10220, 10221, 10222, 10223};
	private static final int[] S80WeaponChances = new int[]{5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 5, 6, 5, 6, 5, 6, 5, 6};
	private static final int[] S80WeaponCounts = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

	public void ItemHandler_13988(L2Player player) {
		if (!canBeExtracted(13988, player))
			return;

		removeItem(player, 13988, 1);
		extract_item(S80WeaponChest, S80WeaponCounts, S80WeaponChances, false, player);
	}

	// TODO ****** Start Item Mall ******
	// Small fortuna box
	public void ItemHandler_22000(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(22000, player))
			return;
		int[] list = new int[]{22006, 22007, 22022, 22023, 22024, 22025};
		int[] counts = new int[]{2, 2, 6, 8, 10, 12}; // TODO: correct count
		int[] chances = new int[]{10, 15, 20, 25, 30, 35}; // TODO: correct chance
		removeItem(player, 22000, 1);
		extract_item_r(list, counts, chances, player);
	}

	// Middle fortuna box
	public void ItemHandler_22001(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(22001, player))
			return;
		int[] list = new int[]{22006, 22007, 22008, 22014, 22022, 22023, 22024, 22025};
		int[] counts = new int[]{2, 2, 2, 2, 6, 8, 10, 12}; // TODO: correct count
		int[] chances = new int[]{15, 10, 5, 3, 20, 25, 30, 35}; // TODO: correct chance
		removeItem(player, 22001, 1);
		extract_item_r(list, counts, chances, player);
	}

	// Large fortuna box
	public void ItemHandler_22002(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(22002, player))
			return;
		int[] list = new int[]{22008, 22009, 22014, 22015, 22018, 22019, 22022, 22023, 22024, 22025};
		int[] counts = new int[]{2, 2, 2, 2, 2, 2, 6, 8, 10, 12}; // TODO: correct count
		int[] chances = new int[]{10, 5, 4, 3, 2, 2, 20, 25, 30, 35}; // TODO: correct chance
		removeItem(player, 22002, 1);
		extract_item_r(list, counts, chances, player);
	}

	// Small fortuna cube
	public void ItemHandler_22003(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(22003, player))
			return;
		int[] list = new int[]{22010, 22011, 22012, 22022, 22023, 22024, 22025};
		int[] counts = new int[]{2, 2, 2, 6, 8, 10, 12}; // TODO: correct count
		int[] chances = new int[]{10, 5, 4, 20, 25, 30, 35}; // TODO: correct chance
		removeItem(player, 22003, 1);
		extract_item_r(list, counts, chances, player);
	}

	// Middle fortuna cube
	public void ItemHandler_22004(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(22004, player))
			return;
		int[] list = new int[]{22011, 22012, 22013, 22016, 22022, 22023, 22024, 22025};
		int[] counts = new int[]{2, 2, 2, 2, 6, 8, 10, 12}; // TODO: correct count
		int[] chances = new int[]{10, 5, 4, 2, 20, 25, 30, 35}; // TODO: correct chance
		removeItem(player, 22004, 1);
		extract_item_r(list, counts, chances, player);
	}

	// Large fortuna cube
	public void ItemHandler_22005(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(22005, player))
			return;
		int[] list = new int[]{22012, 22013, 22016, 22017, 22020, 22021, 22022, 22023, 22024, 22025};
		int[] counts = new int[]{2, 2, 2, 2, 2, 2, 6, 8, 10, 12}; // TODO: correct count
		int[] chances = new int[]{10, 5, 4, 3, 2, 2, 20, 25, 30, 35}; // TODO: correct chance
		removeItem(player, 22005, 1);
		extract_item_r(list, counts, chances, player);
	}

	// Beast Soulshot Pack
	public void ItemHandler_20326(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20326, player))
			return;
		removeItem(player, 20326, 1);
		addItem(player, 20332, 5000);
	}

	// Beast Spiritshot Pack
	public void ItemHandler_20327(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20327, player))
			return;
		removeItem(player, 20327, 1);
		addItem(player, 20333, 5000);
	}

	// Blessed Beast Spiritshot Pack
	public void ItemHandler_20328(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20328, player))
			return;
		removeItem(player, 20328, 1);
		addItem(player, 20334, 5000);
	}

	// Beast Soulshot Large Pack
	public void ItemHandler_20329(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20329, player))
			return;
		removeItem(player, 20329, 1);
		addItem(player, 20332, 10000);
	}

	// Beast Spiritshot Large Pack
	public void ItemHandler_20330(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20330, player))
			return;
		removeItem(player, 20330, 1);
		addItem(player, 20333, 10000);
	}

	// Blessed Beast Spiritshot Large Pack
	public void ItemHandler_20331(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20331, player))
			return;
		removeItem(player, 20331, 1);
		addItem(player, 20334, 10000);
	}

	// Light Purple Maned Horse Bracelet 30-Day Pack
	public void ItemHandler_20059(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20059, player))
			return;
		removeItem(player, 20059, 1);
		addItem(player, 20030, 1);
	}

	// Steam Beatle Mounting Bracelet 7 Day Pack
	public void ItemHandler_20494(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20494, player))
			return;
		removeItem(player, 20494, 1);
		addItem(player, 20449, 1);
	}

	// Light Purple Maned Horse Mounting Bracelet 7 Day Pack
	public void ItemHandler_20493(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20493, player))
			return;
		removeItem(player, 20493, 1);
		addItem(player, 20448, 1);
	}

	// Steam Beatle Mounting Bracelet Pack
	public void ItemHandler_20395(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20395, player))
			return;
		removeItem(player, 20395, 1);
		addItem(player, 20396, 1);
	}

	// Pumpkin Transformation Stick 7 Day Pack
	public void ItemHandler_13281(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13281, player))
			return;
		removeItem(player, 13281, 1);
		addItem(player, 13253, 1);
	}

	// Kat the Cat Hat 7-Day Pack
	public void ItemHandler_13282(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13282, player))
			return;
		removeItem(player, 13282, 1);
		addItem(player, 13239, 1);
	}

	// Feline Queen Hat 7-Day Pack
	public void ItemHandler_13283(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13283, player))
			return;
		removeItem(player, 13283, 1);
		addItem(player, 13240, 1);
	}

	// Monster Eye Hat 7-Day Pack
	public void ItemHandler_13284(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13284, player))
			return;
		removeItem(player, 13284, 1);
		addItem(player, 13241, 1);
	}

	// Brown Bear Hat 7-Day Pack
	public void ItemHandler_13285(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13285, player))
			return;
		removeItem(player, 13285, 1);
		addItem(player, 13242, 1);
	}

	// Fungus Hat 7-Day Pack
	public void ItemHandler_13286(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13286, player))
			return;
		removeItem(player, 13286, 1);
		addItem(player, 13243, 1);
	}

	// Skull Hat 7-Day Pack
	public void ItemHandler_13287(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13287, player))
			return;
		removeItem(player, 13287, 1);
		addItem(player, 13244, 1);
	}

	// Ornithomimus Hat 7-Day Pack
	public void ItemHandler_13288(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13288, player))
			return;
		removeItem(player, 13288, 1);
		addItem(player, 13245, 1);
	}

	// Feline King Hat 7-Day Pack
	public void ItemHandler_13289(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13289, player))
			return;
		removeItem(player, 13289, 1);
		addItem(player, 13246, 1);
	}

	// Kai the Cat Hat 7-Day Pack
	public void ItemHandler_13290(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13290, player))
			return;
		removeItem(player, 13290, 1);
		addItem(player, 13247, 1);
	}

	// Sudden Agathion 7 Day Pack
	public void ItemHandler_14267(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14267, player))
			return;
		removeItem(player, 14267, 1);
		addItem(player, 14093, 1);
	}

	// Shiny Agathion 7 Day Pack
	public void ItemHandler_14268(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14268, player))
			return;
		removeItem(player, 14268, 1);
		addItem(player, 14094, 1);
	}

	// Sobbing Agathion 7 Day Pack
	public void ItemHandler_14269(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14269, player))
			return;
		removeItem(player, 14269, 1);
		addItem(player, 14095, 1);
	}

	// Agathion of Love 7-Day Pack
	public void ItemHandler_13280(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13280, player))
			return;
		removeItem(player, 13280, 1);
		addItem(player, 20201, 1);
	}

	// A Scroll Bundle of Fighter
	public void ItemHandler_22087(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(22087, player))
			return;
		removeItem(player, 22087, 1);
		addItem(player, 22039, 1);
		addItem(player, 22040, 1);
		addItem(player, 22041, 1);
		addItem(player, 22042, 1);
		addItem(player, 22043, 1);
		addItem(player, 22044, 1);
		addItem(player, 22047, 1);
		addItem(player, 22048, 1);
	}

	// A Scroll Bundle of Mage
	public void ItemHandler_22088(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(22088, player))
			return;
		removeItem(player, 22088, 1);
		addItem(player, 22045, 1);
		addItem(player, 22046, 1);
		addItem(player, 22048, 1);
		addItem(player, 22049, 1);
		addItem(player, 22050, 1);
		addItem(player, 22051, 1);
		addItem(player, 22052, 1);
		addItem(player, 22053, 1);
	}

	// TODO ****** End Item Mall ******

	// Pathfinder's Reward - D-Grade
	public void ItemHandler_13003(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13003, player))
			return;
		removeItem(player, 13003, 1);
		if (Rnd.chance(50))
			addItem(player, 955, 1);
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Pathfinder's Reward - C-Grade
	public void ItemHandler_13004(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13004, player))
			return;
		removeItem(player, 13004, 1);
		if (Rnd.chance(50))
			addItem(player, 951, 1);
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Pathfinder's Reward - B-Grade
	public void ItemHandler_13005(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13005, player))
			return;
		removeItem(player, 13005, 1);
		if (Rnd.chance(50))
			addItem(player, 947, 1);
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Pathfinder's Reward - A-Grade
	public void ItemHandler_13006(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13006, player))
			return;
		removeItem(player, 13006, 1);
		if (Rnd.chance(50))
			addItem(player, 729, 1);
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Pathfinder's Reward - S-Grade
	public void ItemHandler_13007(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13007, player))
			return;
		removeItem(player, 13007, 1);
		if (Rnd.chance(50))
			addItem(player, 959, 1);
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Pathfinder's Reward - AU Karm
	public void ItemHandler_13270(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13270, player))
			return;
		removeItem(player, 13270, 1);
		if (Rnd.chance(50))
			addItem(player, 13236, 1);
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Pathfinder's Reward - AR Karm
	public void ItemHandler_13271(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13271, player))
			return;
		removeItem(player, 13271, 1);
		if (Rnd.chance(50))
			addItem(player, 13237, 1);
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Pathfinder's Reward - AE Karm
	public void ItemHandler_13272(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13272, player))
			return;
		removeItem(player, 13272, 1);
		if (Rnd.chance(50))
			addItem(player, 13238, 1);
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// TODO ****** Belts ******
	// Gludio Supply Box - Belt: Grade B, C
	public void ItemHandler_13713(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13713, player))
			return;
		removeItem(player, 13713, 1);
		if (Rnd.chance(50))
			addItem(player, 13894, 1); // Cloth Belt
		if (Rnd.chance(50))
			addItem(player, 13895, 1); // Leather Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Dion Supply Box - Belt: Grade B, C
	public void ItemHandler_13714(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13714, player))
			return;
		removeItem(player, 13714, 1);
		if (Rnd.chance(50))
			addItem(player, 13894, 1); // Cloth Belt
		if (Rnd.chance(50))
			addItem(player, 13895, 1); // Leather Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Giran Supply Box - Belt: Grade B, C
	public void ItemHandler_13715(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13715, player))
			return;
		removeItem(player, 13715, 1);
		if (Rnd.chance(50))
			addItem(player, 13894, 1); // Cloth Belt
		if (Rnd.chance(50))
			addItem(player, 13895, 1); // Leather Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Oren Supply Box - Belt: Grade B, C
	public void ItemHandler_13716(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13716, player))
			return;
		removeItem(player, 13716, 1);
		if (Rnd.chance(50))
			addItem(player, 13894, 1); // Cloth Belt
		if (Rnd.chance(50))
			addItem(player, 13895, 1); // Leather Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Aden Supply Box - Belt: Grade B, C
	public void ItemHandler_13717(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13717, player))
			return;
		removeItem(player, 13717, 1);
		if (Rnd.chance(50))
			addItem(player, 13894, 1); // Cloth Belt
		if (Rnd.chance(50))
			addItem(player, 13895, 1); // Leather Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Innadril Supply Box - Belt: Grade B, C
	public void ItemHandler_13718(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13718, player))
			return;
		removeItem(player, 13718, 1);
		if (Rnd.chance(50))
			addItem(player, 13894, 1); // Cloth Belt
		if (Rnd.chance(50))
			addItem(player, 13895, 1); // Leather Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Goddard Supply Box - Belt: Grade B, C
	public void ItemHandler_13719(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13719, player))
			return;
		removeItem(player, 13719, 1);
		if (Rnd.chance(50))
			addItem(player, 13894, 1); // Cloth Belt
		if (Rnd.chance(50))
			addItem(player, 13895, 1); // Leather Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Rune Supply Box - Belt: Grade B, C
	public void ItemHandler_13720(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13720, player))
			return;
		removeItem(player, 13720, 1);
		if (Rnd.chance(50))
			addItem(player, 13894, 1); // Cloth Belt
		if (Rnd.chance(50))
			addItem(player, 13895, 1); // Leather Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Schuttgart Supply Box - Belt: Grade B, C
	public void ItemHandler_13721(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13721, player))
			return;
		removeItem(player, 13721, 1);
		if (Rnd.chance(50))
			addItem(player, 13894, 1); // Cloth Belt
		if (Rnd.chance(50))
			addItem(player, 13895, 1); // Leather Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Gludio Supply Box - Belt: Grade S, A
	public void ItemHandler_14549(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14549, player))
			return;
		removeItem(player, 14549, 1);
		if (Rnd.chance(50))
			addItem(player, 13896, 1); // Iron Belt
		if (Rnd.chance(50))
			addItem(player, 13897, 1); // Mithril Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Dion Supply Box - Belt: Grade S, A
	public void ItemHandler_14550(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14550, player))
			return;
		removeItem(player, 14550, 1);
		if (Rnd.chance(50))
			addItem(player, 13896, 1); // Iron Belt
		if (Rnd.chance(50))
			addItem(player, 13897, 1); // Mithril Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Giran Supply Box - Belt: Grade S, A
	public void ItemHandler_14551(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14551, player))
			return;
		removeItem(player, 14551, 1);
		if (Rnd.chance(50))
			addItem(player, 13896, 1); // Iron Belt
		if (Rnd.chance(50))
			addItem(player, 13897, 1); // Mithril Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Oren Supply Box - Belt: Grade S, A
	public void ItemHandler_14552(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14552, player))
			return;
		removeItem(player, 14552, 1);
		if (Rnd.chance(50))
			addItem(player, 13896, 1); // Iron Belt
		if (Rnd.chance(50))
			addItem(player, 13897, 1); // Mithril Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Aden Supply Box - Belt: Grade S, A
	public void ItemHandler_14553(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14553, player))
			return;
		removeItem(player, 14553, 1);
		if (Rnd.chance(50))
			addItem(player, 13896, 1); // Iron Belt
		if (Rnd.chance(50))
			addItem(player, 13897, 1); // Mithril Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Innadril Supply Box - Belt: Grade S, A
	public void ItemHandler_14554(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14554, player))
			return;
		removeItem(player, 14554, 1);
		if (Rnd.chance(50))
			addItem(player, 13896, 1); // Iron Belt
		if (Rnd.chance(50))
			addItem(player, 13897, 1); // Mithril Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Goddard Supply Box - Belt: Grade S, A
	public void ItemHandler_14555(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14555, player))
			return;
		removeItem(player, 14555, 1);
		if (Rnd.chance(50))
			addItem(player, 13896, 1); // Iron Belt
		if (Rnd.chance(50))
			addItem(player, 13897, 1); // Mithril Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Rune Supply Box - Belt: Grade S, A
	public void ItemHandler_14556(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14556, player))
			return;
		removeItem(player, 14556, 1);
		if (Rnd.chance(50))
			addItem(player, 13896, 1); // Iron Belt
		if (Rnd.chance(50))
			addItem(player, 13897, 1); // Mithril Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Schuttgart Supply Box - Belt: Grade S, A
	public void ItemHandler_14557(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14557, player))
			return;
		removeItem(player, 14557, 1);
		if (Rnd.chance(50))
			addItem(player, 13896, 1); // Iron Belt
		if (Rnd.chance(50))
			addItem(player, 13897, 1); // Mithril Belt
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// TODO ****** Magic Pins ******
	// Gludio Supply Box - Magic Pin: Grade B, C
	public void ItemHandler_13695(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13695, player))
			return;
		removeItem(player, 13695, 1);
		if (Rnd.chance(50))
			addItem(player, 13898, 1); // Sealed Magic Pin (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13899, 1); // Sealed Magic Pin (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Dion Supply Box - Magic Pin: Grade B, C
	public void ItemHandler_13696(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13696, player))
			return;
		removeItem(player, 13696, 1);
		if (Rnd.chance(50))
			addItem(player, 13898, 1); // Sealed Magic Pin (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13899, 1); // Sealed Magic Pin (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Giran Supply Box - Magic Pin: Grade B, C
	public void ItemHandler_13697(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13697, player))
			return;
		removeItem(player, 13697, 1);
		if (Rnd.chance(50))
			addItem(player, 13898, 1); // Sealed Magic Pin (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13899, 1); // Sealed Magic Pin (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Oren Supply Box - Magic Pin: Grade B, C
	public void ItemHandler_13698(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13698, player))
			return;
		removeItem(player, 13698, 1);
		if (Rnd.chance(50))
			addItem(player, 13898, 1); // Sealed Magic Pin (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13899, 1); // Sealed Magic Pin (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Aden Supply Box - Magic Pin: Grade B, C
	public void ItemHandler_13699(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13699, player))
			return;
		removeItem(player, 13699, 1);
		if (Rnd.chance(50))
			addItem(player, 13898, 1); // Sealed Magic Pin (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13899, 1); // Sealed Magic Pin (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Innadril Supply Box - Magic Pin: Grade B, C
	public void ItemHandler_13700(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13700, player))
			return;
		removeItem(player, 13700, 1);
		if (Rnd.chance(50))
			addItem(player, 13898, 1); // Sealed Magic Pin (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13899, 1); // Sealed Magic Pin (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Goddard Supply Box - Magic Pin: Grade B, C
	public void ItemHandler_13701(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13701, player))
			return;
		removeItem(player, 13701, 1);
		if (Rnd.chance(50))
			addItem(player, 13898, 1); // Sealed Magic Pin (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13899, 1); // Sealed Magic Pin (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Rune Supply Box - Magic Pin: Grade B, C
	public void ItemHandler_13702(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13702, player))
			return;
		removeItem(player, 13702, 1);
		if (Rnd.chance(50))
			addItem(player, 13898, 1); // Sealed Magic Pin (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13899, 1); // Sealed Magic Pin (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Schuttgart Supply Box - Magic Pin: Grade B, C
	public void ItemHandler_13703(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13703, player))
			return;
		removeItem(player, 13703, 1);
		if (Rnd.chance(50))
			addItem(player, 13898, 1); // Sealed Magic Pin (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13899, 1); // Sealed Magic Pin (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Gludio Supply Box - Magic Pin: Grade S, A
	public void ItemHandler_14531(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14531, player))
			return;
		removeItem(player, 14531, 1);
		if (Rnd.chance(50))
			addItem(player, 13900, 1); // Sealed Magic Pin (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13901, 1); // Sealed Magic Pin (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Dion Supply Box - Magic Pin: Grade S, A
	public void ItemHandler_14532(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14532, player))
			return;
		removeItem(player, 14532, 1);
		if (Rnd.chance(50))
			addItem(player, 13900, 1); // Sealed Magic Pin (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13901, 1); // Sealed Magic Pin (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Giran Supply Box - Magic Pin: Grade S, A
	public void ItemHandler_14533(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14533, player))
			return;
		removeItem(player, 14533, 1);
		if (Rnd.chance(50))
			addItem(player, 13900, 1); // Sealed Magic Pin (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13901, 1); // Sealed Magic Pin (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Oren Supply Box - Magic Pin: Grade S, A
	public void ItemHandler_14534(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14534, player))
			return;
		removeItem(player, 14534, 1);
		if (Rnd.chance(50))
			addItem(player, 13900, 1); // Sealed Magic Pin (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13901, 1); // Sealed Magic Pin (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Aden Supply Box - Magic Pin: Grade S, A
	public void ItemHandler_14535(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14535, player))
			return;
		removeItem(player, 14535, 1);
		if (Rnd.chance(50))
			addItem(player, 13900, 1); // Sealed Magic Pin (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13901, 1); // Sealed Magic Pin (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Innadril Supply Box - Magic Pin: Grade S, A
	public void ItemHandler_14536(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14536, player))
			return;
		removeItem(player, 14536, 1);
		if (Rnd.chance(50))
			addItem(player, 13900, 1); // Sealed Magic Pin (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13901, 1); // Sealed Magic Pin (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Goddard Supply Box - Magic Pin: Grade S, A
	public void ItemHandler_14537(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14537, player))
			return;
		removeItem(player, 14537, 1);
		if (Rnd.chance(50))
			addItem(player, 13900, 1); // Sealed Magic Pin (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13901, 1); // Sealed Magic Pin (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Rune Supply Box - Magic Pin: Grade S, A
	public void ItemHandler_14538(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14538, player))
			return;
		removeItem(player, 14538, 1);
		if (Rnd.chance(50))
			addItem(player, 13900, 1); // Sealed Magic Pin (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13901, 1); // Sealed Magic Pin (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Schuttgart Supply Box - Magic Pin: Grade S, A
	public void ItemHandler_14539(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14539, player))
			return;
		removeItem(player, 14539, 1);
		if (Rnd.chance(50))
			addItem(player, 13900, 1); // Sealed Magic Pin (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13901, 1); // Sealed Magic Pin (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// TODO ****** Magic Pouchs ******
	// Gludio Supply Box - Magic Pouch: Grade B, C
	public void ItemHandler_13704(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13704, player))
			return;
		removeItem(player, 13704, 1);
		if (Rnd.chance(50))
			addItem(player, 13918, 1); // Sealed Magic Pouch (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13919, 1); // Sealed Magic Pouch (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Dion Supply Box - Magic Pouch: Grade B, C
	public void ItemHandler_13705(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13705, player))
			return;
		removeItem(player, 13705, 1);
		if (Rnd.chance(50))
			addItem(player, 13918, 1); // Sealed Magic Pouch (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13919, 1); // Sealed Magic Pouch (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Giran Supply Box - Magic Pouch: Grade B, C
	public void ItemHandler_13706(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13706, player))
			return;
		removeItem(player, 13706, 1);
		if (Rnd.chance(50))
			addItem(player, 13918, 1); // Sealed Magic Pouch (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13919, 1); // Sealed Magic Pouch (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Oren Supply Box - Magic Pouch: Grade B, C
	public void ItemHandler_13707(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13707, player))
			return;
		removeItem(player, 13707, 1);
		if (Rnd.chance(50))
			addItem(player, 13918, 1); // Sealed Magic Pouch (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13919, 1); // Sealed Magic Pouch (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Aden Supply Box - Magic Pouch: Grade B, C
	public void ItemHandler_13708(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13708, player))
			return;
		removeItem(player, 13708, 1);
		if (Rnd.chance(50))
			addItem(player, 13918, 1); // Sealed Magic Pouch (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13919, 1); // Sealed Magic Pouch (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Innadril Supply Box - Magic Pouch: Grade B, C
	public void ItemHandler_13709(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13709, player))
			return;
		removeItem(player, 13709, 1);
		if (Rnd.chance(50))
			addItem(player, 13918, 1); // Sealed Magic Pouch (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13919, 1); // Sealed Magic Pouch (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Goddard Supply Box - Magic Pouch: Grade B, C
	public void ItemHandler_13710(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13710, player))
			return;
		removeItem(player, 13710, 1);
		if (Rnd.chance(50))
			addItem(player, 13918, 1); // Sealed Magic Pouch (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13919, 1); // Sealed Magic Pouch (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Rune Supply Box - Magic Pouch: Grade B, C
	public void ItemHandler_13711(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13711, player))
			return;
		removeItem(player, 13711, 1);
		if (Rnd.chance(50))
			addItem(player, 13918, 1); // Sealed Magic Pouch (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13919, 1); // Sealed Magic Pouch (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Schuttgart Supply Box - Magic Pouch: Grade B, C
	public void ItemHandler_13712(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13712, player))
			return;
		removeItem(player, 13712, 1);
		if (Rnd.chance(50))
			addItem(player, 13918, 1); // Sealed Magic Pouch (C-Grade)
		if (Rnd.chance(50))
			addItem(player, 13919, 1); // Sealed Magic Pouch (B-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Gludio Supply Box - Magic Pouch: Grade S, A
	public void ItemHandler_14540(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14540, player))
			return;
		removeItem(player, 14540, 1);
		if (Rnd.chance(50))
			addItem(player, 13920, 1); // Sealed Magic Pouch (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13921, 1); // Sealed Magic Pouch (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Dion Supply Box - Magic Pouch: Grade S, A
	public void ItemHandler_14541(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14541, player))
			return;
		removeItem(player, 14541, 1);
		if (Rnd.chance(50))
			addItem(player, 13920, 1); // Sealed Magic Pouch (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13921, 1); // Sealed Magic Pouch (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Giran Supply Box - Magic Pouch: Grade S, A
	public void ItemHandler_14542(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14542, player))
			return;
		removeItem(player, 14542, 1);
		if (Rnd.chance(50))
			addItem(player, 13920, 1); // Sealed Magic Pouch (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13921, 1); // Sealed Magic Pouch (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Oren Supply Box - Magic Pouch: Grade S, A
	public void ItemHandler_14543(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14543, player))
			return;
		removeItem(player, 14543, 1);
		if (Rnd.chance(50))
			addItem(player, 13920, 1); // Sealed Magic Pouch (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13921, 1); // Sealed Magic Pouch (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Aden Supply Box - Magic Pouch: Grade S, A
	public void ItemHandler_14544(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14544, player))
			return;
		removeItem(player, 14544, 1);
		if (Rnd.chance(50))
			addItem(player, 13920, 1); // Sealed Magic Pouch (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13921, 1); // Sealed Magic Pouch (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Innadril Supply Box - Magic Pouch: Grade S, A
	public void ItemHandler_14545(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14545, player))
			return;
		removeItem(player, 14545, 1);
		if (Rnd.chance(50))
			addItem(player, 13920, 1); // Sealed Magic Pouch (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13921, 1); // Sealed Magic Pouch (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Goddard Supply Box - Magic Pouch: Grade S, A
	public void ItemHandler_14546(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14546, player))
			return;
		removeItem(player, 14546, 1);
		if (Rnd.chance(50))
			addItem(player, 13920, 1); // Sealed Magic Pouch (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13921, 1); // Sealed Magic Pouch (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Rune Supply Box - Magic Pouch: Grade S, A
	public void ItemHandler_14547(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14547, player))
			return;
		removeItem(player, 14547, 1);
		if (Rnd.chance(50))
			addItem(player, 13920, 1); // Sealed Magic Pouch (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13921, 1); // Sealed Magic Pouch (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Schuttgart Supply Box - Magic Pouch: Grade S, A
	public void ItemHandler_14548(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14548, player))
			return;
		removeItem(player, 14548, 1);
		if (Rnd.chance(50))
			addItem(player, 13920, 1); // Sealed Magic Pouch (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 13921, 1); // Sealed Magic Pouch (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// TODO ****** Magic Rune Clip ******
	// Gludio Supply Box - Magic Rune Clip: Grade S, A
	public void ItemHandler_14884(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14884, player))
			return;
		removeItem(player, 14884, 1);
		if (Rnd.chance(50))
			addItem(player, 14902, 1); // Sealed Magic Rune Clip (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 14903, 1); // Sealed Magic Rune Clip (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Dion Supply Box - Magic Rune Clip: Grade S, A
	public void ItemHandler_14885(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14885, player))
			return;
		removeItem(player, 14885, 1);
		if (Rnd.chance(50))
			addItem(player, 14902, 1); // Sealed Magic Rune Clip (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 14903, 1); // Sealed Magic Rune Clip (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Giran Supply Box - Magic Rune Clip: Grade S, A
	public void ItemHandler_14886(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14886, player))
			return;
		removeItem(player, 14886, 1);
		if (Rnd.chance(50))
			addItem(player, 14902, 1); // Sealed Magic Rune Clip (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 14903, 1); // Sealed Magic Rune Clip (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Oren Supply Box - Magic Rune Clip: Grade S, A
	public void ItemHandler_14887(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14887, player))
			return;
		removeItem(player, 14887, 1);
		if (Rnd.chance(50))
			addItem(player, 14902, 1); // Sealed Magic Rune Clip (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 14903, 1); // Sealed Magic Rune Clip (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Aden Supply Box - Magic Rune Clip: Grade S, A
	public void ItemHandler_14888(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14888, player))
			return;
		removeItem(player, 14888, 1);
		if (Rnd.chance(50))
			addItem(player, 14902, 1); // Sealed Magic Rune Clip (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 14903, 1); // Sealed Magic Rune Clip (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Innadril Supply Box - Magic Rune Clip: Grade S, A
	public void ItemHandler_14889(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14889, player))
			return;
		removeItem(player, 14889, 1);
		if (Rnd.chance(50))
			addItem(player, 14902, 1); // Sealed Magic Rune Clip (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 14903, 1); // Sealed Magic Rune Clip (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Goddard Supply Box - Magic Rune Clip: Grade S, A
	public void ItemHandler_14890(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14890, player))
			return;
		removeItem(player, 14890, 1);
		if (Rnd.chance(50))
			addItem(player, 14902, 1); // Sealed Magic Rune Clip (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 14903, 1); // Sealed Magic Rune Clip (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Rune Supply Box - Magic Rune Clip: Grade S, A
	public void ItemHandler_14891(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14891, player))
			return;
		removeItem(player, 14891, 1);
		if (Rnd.chance(50))
			addItem(player, 14902, 1); // Sealed Magic Rune Clip (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 14903, 1); // Sealed Magic Rune Clip (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Schuttgart Supply Box - Magic Rune Clip: Grade S, A
	public void ItemHandler_14892(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14892, player))
			return;
		removeItem(player, 14892, 1);
		if (Rnd.chance(50))
			addItem(player, 14902, 1); // Sealed Magic Rune Clip (A-Grade)
		if (Rnd.chance(50))
			addItem(player, 14903, 1); // Sealed Magic Rune Clip (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// TODO ****** Magic Ornament ******
	// Gludio Supply Box - Magic Ornament: Grade S, A
	public void ItemHandler_14893(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14893, player))
			return;
		removeItem(player, 14893, 1);
		if (Rnd.chance(20))
			addItem(player, 14904, 1); // Sealed Magic Ornament (A-Grade)
		if (Rnd.chance(20))
			addItem(player, 14905, 1); // Sealed Magic Ornament (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Dion Supply Box - Magic Ornament: Grade S, A
	public void ItemHandler_14894(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14894, player))
			return;
		removeItem(player, 14894, 1);
		if (Rnd.chance(20))
			addItem(player, 14904, 1); // Sealed Magic Ornament (A-Grade)
		if (Rnd.chance(20))
			addItem(player, 14905, 1); // Sealed Magic Ornament (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Giran Supply Box - Magic Ornament: Grade S, A
	public void ItemHandler_14895(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14895, player))
			return;
		removeItem(player, 14895, 1);
		if (Rnd.chance(20))
			addItem(player, 14904, 1); // Sealed Magic Ornament (A-Grade)
		if (Rnd.chance(20))
			addItem(player, 14905, 1); // Sealed Magic Ornament (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Oren Supply Box - Magic Ornament: Grade S, A
	public void ItemHandler_14896(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14896, player))
			return;
		removeItem(player, 14896, 1);
		if (Rnd.chance(20))
			addItem(player, 14904, 1); // Sealed Magic Ornament (A-Grade)
		if (Rnd.chance(20))
			addItem(player, 14905, 1); // Sealed Magic Ornament (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Aden Supply Box - Magic Ornament: Grade S, A
	public void ItemHandler_14897(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14897, player))
			return;
		removeItem(player, 14897, 1);
		if (Rnd.chance(20))
			addItem(player, 14904, 1); // Sealed Magic Ornament (A-Grade)
		if (Rnd.chance(20))
			addItem(player, 14905, 1); // Sealed Magic Ornament (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Innadril Supply Box - Magic Ornament: Grade S, A
	public void ItemHandler_14898(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14898, player))
			return;
		removeItem(player, 14898, 1);
		if (Rnd.chance(20))
			addItem(player, 14904, 1); // Sealed Magic Ornament (A-Grade)
		if (Rnd.chance(20))
			addItem(player, 14905, 1); // Sealed Magic Ornament (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Goddard Supply Box - Magic Ornament: Grade S, A
	public void ItemHandler_14899(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14899, player))
			return;
		removeItem(player, 14899, 1);
		if (Rnd.chance(20))
			addItem(player, 14904, 1); // Sealed Magic Ornament (A-Grade)
		if (Rnd.chance(20))
			addItem(player, 14905, 1); // Sealed Magic Ornament (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Rune Supply Box - Magic Ornament: Grade S, A
	public void ItemHandler_14900(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14900, player))
			return;
		removeItem(player, 14900, 1);
		if (Rnd.chance(20))
			addItem(player, 14904, 1); // Sealed Magic Ornament (A-Grade)
		if (Rnd.chance(20))
			addItem(player, 14905, 1); // Sealed Magic Ornament (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Schuttgart Supply Box - Magic Ornament: Grade S, A
	public void ItemHandler_14901(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14901, player))
			return;
		removeItem(player, 14901, 1);
		if (Rnd.chance(20))
			addItem(player, 14904, 1); // Sealed Magic Ornament (A-Grade)
		if (Rnd.chance(20))
			addItem(player, 14905, 1); // Sealed Magic Ornament (S-Grade)
		else
			player.sendPacket(Msg.THERE_WAS_NOTHING_FOUND_INSIDE_OF_THAT);
	}

	// Gift from Santa Claus
	public void ItemHandler_14616(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14616, player))
			return;
		removeItem(player, 14616, 1);

		// Santa Claus' Weapon Exchange Ticket - 12 Hour Expiration Period
		addItem(player, 20107, 1);

		// Christmas Red Sock
		addItem(player, 14612, 1);

		// Special Christmas Tree
		if (Rnd.chance(30))
			addItem(player, 5561, 1);

		// Christmas Tree
		if (Rnd.chance(50))
			addItem(player, 5560, 1);

		// Agathion Seal Bracelet - Rudolph (РїРѕСЃС‚РѕСЏРЅРЅС‹Р№ РїСЂРµРґРјРµС‚)
		if (getItemCount(player, 10606) == 0 && Rnd.chance(5))
			addItem(player, 10606, 1);

		// Agathion Seal Bracelet: Rudolph - 30 РґРЅРµР№ СЃРѕ СЃРєРёР»РѕРј РЅР° РІРёС‚Р°Р»РёС‚Рё
		if (getItemCount(player, 20094) == 0 && Rnd.chance(3))
			addItem(player, 20094, 1);

		// Chest of Experience (Event)
		if (Rnd.chance(30))
			addItem(player, 20575, 1);
	}

	// Christmas Red Sock
	public void ItemHandler_14612(L2Player player) {
		if (player == null)
			return;
		if (player.isInZone(ZoneType.OlympiadStadia))
			return;
		removeItem(player, 14612, 1);
		player.broadcastPacket(new MagicSkillUse(player, player, 23017, 1, 0, 0));
		player.altOnMagicUseTimer(player, SkillTable.getInstance().getInfo(23017, 1));
	}

	// Chest of Experience (Event)
	public void ItemHandler_20575(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20575, player))
			return;
		removeItem(player, 20575, 1);
		addItem(player, 20335, 1); // Rune of Experience: 30% - 5 hour limited time
		addItem(player, 20341, 1); // Rune of SP 30% - 5 Hour Expiration Period
	}

	// Nepal Snow Agathion Pack
	public void ItemHandler_20804(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20804, player))
			return;
		removeItem(player, 20804, 1);
		addItem(player, 20782, 1);
	}

	// Nepal Snow Agathion 7-Day Pack - Snow's Haste
	public void ItemHandler_20807(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20807, player))
			return;
		removeItem(player, 20807, 1);
		addItem(player, 20785, 1);
	}

	// Round Ball Snow Agathion Pack
	public void ItemHandler_20805(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20805, player))
			return;
		removeItem(player, 20805, 1);
		addItem(player, 20783, 1);
	}

	// Round Ball Snow Agathion 7-Day Pack - Snow's Acumen
	public void ItemHandler_20808(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20808, player))
			return;
		removeItem(player, 20808, 1);
		addItem(player, 20786, 1);
	}

	// Ladder Snow Agathion Pack
	public void ItemHandler_20806(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20806, player))
			return;
		removeItem(player, 20806, 1);
		addItem(player, 20784, 1);
	}

	// Ladder Snow Agathion 7-Day Pack - Snow's Wind Walk
	public void ItemHandler_20809(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20809, player))
			return;
		removeItem(player, 20809, 1);
		addItem(player, 20787, 1);
	}

	// Iken Agathion Pack
	public void ItemHandler_20842(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20842, player))
			return;
		removeItem(player, 20842, 1);
		addItem(player, 20818, 1);
	}

	// Iken Agathion 7-Day Pack Prominent Outsider Adventurer's Ability
	public void ItemHandler_20843(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20843, player))
			return;
		removeItem(player, 20843, 1);
		addItem(player, 20819, 1);
	}

	// Lana Agathion Pack
	public void ItemHandler_20844(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20844, player))
			return;
		removeItem(player, 20844, 1);
		addItem(player, 20820, 1);
	}

	// Lana Agathion 7-Day Pack Prominent Outsider Adventurer's Ability
	public void ItemHandler_20845(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20845, player))
			return;
		removeItem(player, 20845, 1);
		addItem(player, 20821, 1);
	}

	// Gnocian Agathion Pack
	public void ItemHandler_20846(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20846, player))
			return;
		removeItem(player, 20846, 1);
		addItem(player, 20822, 1);
	}

	// Gnocian Agathion 7-Day Pack Prominent Outsider Adventurer's Ability
	public void ItemHandler_20847(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20847, player))
			return;
		removeItem(player, 20847, 1);
		addItem(player, 20823, 1);
	}

	// Orodriel Agathion Pack
	public void ItemHandler_20848(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20848, player))
			return;
		removeItem(player, 20848, 1);
		addItem(player, 20824, 1);
	}

	// Orodriel Agathion 7-Day Pack Prominent Outsider Adventurer's Ability
	public void ItemHandler_20849(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20849, player))
			return;
		removeItem(player, 20849, 1);
		addItem(player, 20825, 1);
	}

	// Lakinos Agathion Pack
	public void ItemHandler_20850(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20850, player))
			return;
		removeItem(player, 20850, 1);
		addItem(player, 20826, 1);
	}

	// Lakinos Agathion 7-Day Pack Prominent Outsider Adventurer's Ability
	public void ItemHandler_20851(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20851, player))
			return;
		removeItem(player, 20851, 1);
		addItem(player, 20827, 1);
	}

	// Mortia Agathion Pack
	public void ItemHandler_20852(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20852, player))
			return;
		removeItem(player, 20852, 1);
		addItem(player, 20828, 1);
	}

	// Mortia Agathion 7-Day Pack Prominent Outsider Adventurer's Ability
	public void ItemHandler_20853(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20853, player))
			return;
		removeItem(player, 20853, 1);
		addItem(player, 20829, 1);
	}

	// Hayance Agathion Pack
	public void ItemHandler_20854(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20854, player))
			return;
		removeItem(player, 20854, 1);
		addItem(player, 20830, 1);
	}

	// Hayance Agathion 7-Day Pack Prominent Outsider Adventurer's Ability
	public void ItemHandler_20855(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20855, player))
			return;
		removeItem(player, 20855, 1);
		addItem(player, 20831, 1);
	}

	// Meruril Agathion Pack
	public void ItemHandler_20856(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20856, player))
			return;
		removeItem(player, 20856, 1);
		addItem(player, 20832, 1);
	}

	// Meruril Agathion 7-Day Pack Prominent Outsider Adventurer's Ability
	public void ItemHandler_20857(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20857, player))
			return;
		removeItem(player, 20857, 1);
		addItem(player, 20833, 1);
	}

	// Taman ze Lapatui Agathion Pack
	public void ItemHandler_20858(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20858, player))
			return;
		removeItem(player, 20858, 1);
		addItem(player, 20834, 1);
	}

	// Taman ze Lapatui Agathion 7-Day Pack Prominent Outsider Adventurer's Ability
	public void ItemHandler_20859(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20859, player))
			return;
		removeItem(player, 20859, 1);
		addItem(player, 20835, 1);
	}

	// Kaurin Agathion Pack
	public void ItemHandler_20860(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20860, player))
			return;
		removeItem(player, 20860, 1);
		addItem(player, 20836, 1);
	}

	// Kaurin Agathion 7-Day Pack Prominent Outsider Adventurer's Ability
	public void ItemHandler_20861(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20861, player))
			return;
		removeItem(player, 20861, 1);
		addItem(player, 20837, 1);
	}

	// Ahertbein Agathion Pack
	public void ItemHandler_20862(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20862, player))
			return;
		removeItem(player, 20862, 1);
		addItem(player, 20838, 1);
	}

	// Ahertbein Agathion 7-Day Pack Prominent Outsider Adventurer's Ability
	public void ItemHandler_20863(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20863, player))
			return;
		removeItem(player, 20863, 1);
		addItem(player, 20839, 1);
	}

	// Naonin Agathion Pack
	public void ItemHandler_20864(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20864, player))
			return;
		removeItem(player, 20864, 1);
		addItem(player, 20840, 1);
	}

	// Rocket Gun Hat Pack Continuous Fireworks
	public void ItemHandler_20811(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20811, player))
			return;
		removeItem(player, 20811, 1);
		addItem(player, 20789, 1);
	}

	// Yellow Paper Hat 7-Day Pack Bless the Body
	public void ItemHandler_20812(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20812, player))
			return;
		removeItem(player, 20812, 1);
		addItem(player, 20790, 1);
	}

	// Pink Paper Mask Set 7-Day Pack Bless the Soul
	public void ItemHandler_20813(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20813, player))
			return;
		removeItem(player, 20813, 1);
		addItem(player, 20791, 1);
	}

	// Flavorful Cheese Hat Pack
	public void ItemHandler_20814(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20814, player))
			return;
		removeItem(player, 20814, 1);
		addItem(player, 20792, 1);
	}

	// Sweet Cheese Hat Pack
	public void ItemHandler_20815(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20815, player))
			return;
		removeItem(player, 20815, 1);
		addItem(player, 20793, 1);
	}

	// Flavorful Cheese Hat 7-Day Pack Scent of Flavorful Cheese
	public void ItemHandler_20816(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20816, player))
			return;
		removeItem(player, 20816, 1);
		addItem(player, 20794, 1);
	}

	// Sweet Cheese Hat 7-Day Pack Scent of Sweet Cheese
	public void ItemHandler_20817(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20817, player))
			return;
		removeItem(player, 20817, 1);
		addItem(player, 20795, 1);
	}

	// Flame Box Pack
	public void ItemHandler_20810(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20810, player))
			return;
		removeItem(player, 20810, 1);
		addItem(player, 20725, 1);
	}

	// Naonin Agathion 7-Day Pack Prominent Outsider Adventurer's Ability
	public void ItemHandler_20865(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20865, player))
			return;
		removeItem(player, 20865, 1);
		addItem(player, 20841, 1);
	}

	// Shiny Mask of Giant Hercules 7 day Pack
	public void ItemHandler_20748(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20748, player))
			return;
		removeItem(player, 20748, 1);
		addItem(player, 20743, 1);
	}

	// Shiny Mask of Silent Scream 7 day Pack
	public void ItemHandler_20749(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20749, player))
			return;
		removeItem(player, 20749, 1);
		addItem(player, 20744, 1);
	}

	// Shiny Spirit of Wrath Mask 7 day Pack
	public void ItemHandler_20750(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20750, player))
			return;
		removeItem(player, 20750, 1);
		addItem(player, 20745, 1);
	}

	// Shiny Undecaying Corpse Mask 7 Day Pack
	public void ItemHandler_20751(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20751, player))
			return;
		removeItem(player, 20751, 1);
		addItem(player, 20746, 1);
	}

	// Shiny Planet X235 Alien Mask 7 day Pack
	public void ItemHandler_20752(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(20752, player))
			return;
		removeItem(player, 20752, 1);
		addItem(player, 20747, 1);
	}

	// TODO Seed of Infinity (Supplies)
	private static final int[] SOI_books = {14209, // Forgotten Scroll - Hide
			14212, // Forgotten Scroll - Enlightenment - Wizard
			14213, // Forgotten Scroll - Enlightenment - Healer
			10554, // Forgotten Scroll - Anti-Magic Armor
			14208, // Forgotten Scroll - Final Secret
			10577 // Forgotten Scroll - Excessive Loyalty
	};

	// Jewel Ornamented Duel Supplies
	public void ItemHandler_13777(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13777, player))
			return;
		removeItem(player, 13777, 1);

		addRewards(player);
		addItem(player, 14027, 1); // Collection Agathion Summon Bracelet
	}

	// Mother-of-Pearl Ornamented Duel Supplies
	public void ItemHandler_13778(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13778, player))
			return;
		removeItem(player, 13778, 1);

			addRewards(player);
			addItem(player, 14027, 1); // Collection Agathion Summon Bracelet
	}

	// Gold-Ornamented Duel Supplies
	public void ItemHandler_13779(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13779, player))
			return;
		removeItem(player, 13779, 1);

		addRewards(player);
		addItem(player, 14027, 1); // Collection Agathion Summon Bracelet
	}

	// Silver-Ornamented Duel Supplies
	public void ItemHandler_13780(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13780, player))
			return;
		removeItem(player, 13780, 1);

		addRewards(player);
	}

	// Bronze-Ornamented Duel Supplies
	public void ItemHandler_13781(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13781, player))
			return;
		removeItem(player, 13781, 1);

		addRewards(player);
	}

	// Non-Ornamented Duel Supplies
	public void ItemHandler_13782(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13782, player))
			return;
		removeItem(player, 13782, 1);

		addRewards(player);
	}

	// Weak-Looking Duel Supplies
	public void ItemHandler_13783(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13783, player))
			return;
		removeItem(player, 13783, 1);

		addRewards(player);
	}

	// Sad-Looking Duel Supplies
	public void ItemHandler_13784(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13784, player))
			return;
		removeItem(player, 13784, 1);

		addRewards(player);
	}

	// Poor-Looking Duel Supplies
	public void ItemHandler_13785(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13785, player))
			return;
		removeItem(player, 13785, 1);

		addRewards(player);
	}

	// Worthless Duel Supplies
	public void ItemHandler_13786(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13786, player))
			return;
		removeItem(player, 13786, 1);

		addRewards(player);
	}

	/**[custom]
	 * //FIXME говно код на скорую руку, переделать
	 * @param player
	 */
	public void addRewards(L2Player player) {
		int min = 5;
		int max = 20;
		max -= min;
		int count = (int) ((Math.random() * ++max) + min);
		int[] itemId = {6577, 6578};
		int randomIndex = new Random().nextInt(itemId.length);
		int randomItem = itemId[randomIndex];

		addItem(player, randomItem, 1);
		addItem(player, 14052, count);

	}

	// quests._640_TheZeroHour._146_TheZeroHour - Kahman's Supply Box
	public void ItemHandler_14849(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14849, player))
			return;
		removeItem(player, 14849, 1);

		if (Rnd.chance(15))
			addItem(player, 14209, 1);

		if (Rnd.chance(15))
			addItem(player, 14208, 1);

		if (Rnd.chance(15))
			addItem(player, 14212, 1);

		if (Rnd.chance(15))
			addItem(player, 10577, 1);

		if (Rnd.chance(15))
			addItem(player, 959, 1);

		if (Rnd.chance(20))
			addItem(player, 960, 2);

		if (Rnd.chance(25))
			addItem(player, 9573, 1);

		if (Rnd.chance(25))
			addItem(player, 10483, 1);

		if (Rnd.chance(10))
			addItem(player, 9625, 1);

		if (Rnd.chance(10))
			addItem(player, 9626, 1);

		if (Rnd.chance(9)) {
			int[] list = new int[]{10373, 10374, 10375, 10376, 10377, 10378, 10379, 10380, 10381};
			int[] counts = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
			int[] chances = new int[]{8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8};
			extract_item_r(list, counts, chances, player);
		}
	}

	// квест : Control Device Of The Giants (Droph's Support Items)
	public void ItemHandler_14850(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(14850, player))
			return;
		removeItem(player, 14850, 1);

		if (Rnd.chance(15))
			addItem(player, 14209, 1);

		if (Rnd.chance(15))
			addItem(player, 14208, 1);

		if (Rnd.chance(15))
			addItem(player, 14212, 1);

		if (Rnd.chance(15))
			addItem(player, 10577, 1);

		if (Rnd.chance(15))
			addItem(player, 959, 1);

		if (Rnd.chance(20))
			addItem(player, 960, 2);

		if (Rnd.chance(25))
			addItem(player, 9573, 1);

		if (Rnd.chance(25))
			addItem(player, 10483, 1);

		if (Rnd.chance(10))
			addItem(player, 9625, 1);

		if (Rnd.chance(10))
			addItem(player, 9626, 1);

		if (Rnd.chance(9)) {
			int[] list = new int[]{10373, 10374, 10375, 10376, 10377, 10378, 10379, 10380, 10381};
			int[] counts = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
			int[] chances = new int[]{8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8};
			extract_item_r(list, counts, chances, player);
		}
	}

	// Quest 10272 Light Fragment: Destroyed Darkness Fragment Powder
	public void ItemHandler_13853(L2Player player) {
		if (player == null)
			return;
		if (!canBeExtracted(13853, player))
			return;
		if (player.isInZone(ZoneType.mother_tree)) {
			removeItem(player, 13853, 1);
			addItem(player, 13854, 1); // Destroyed Light Fragment Powder
		}
	}

	private static void extract_item(int[] list, int[] counts, L2Player player) {
		int index = Rnd.get(list.length);
		int id = list[index];
		int count = counts[index];
		addItem(player, id, count);
	}

	public static GArray<int[]> mass_extract_item(long source_count, int[] list, int[] counts, L2Player player) {
		if (player == null)
			return new GArray<int[]>(0);

		GArray<int[]> result = new GArray<int[]>((int) Math.min(list.length, source_count));

		for (int n = 1; n <= source_count; n++) {
			int index = Rnd.get(list.length);
			int item = list[index];
			int count = counts[index];

			int[] old = null;
			for (int[] res : result)
				if (res[0] == item)
					old = res;

			if (old == null)
				result.add(new int[]{item, count});
			else
				old[1] += count;
		}

		return result;
	}

	private static void extract_item_r(int[] list, int[] counts, int[] chances, L2Player player) {
		int sum = 0;
		for (int i = 0; i < list.length; i++)
			sum += chances[i];

		int[] table = new int[sum];

		int k = 0;
		for (int i = 0; i < list.length; i++)
			for (int j = 0; j < chances[i]; j++) {
				table[k] = i;
				k++;
			}

		int i = table[Rnd.get(table.length)];
		int item = list[i];
		int count = counts[i];

		addItem(player, item, count);
	}

	private static void extract_item(int[] list, int[] counts, int[] chances, boolean checkAll, L2Player player) {
		for (int i = 0; i < chances.length; i++)
			if (Rnd.chance(chances[i])) {
				addItem(player, list[i], counts[i]);
				if (!checkAll)
					break;
			}
	}

	public static GArray<int[]> mass_extract_item_r(long source_count, int[] list, int[] count_min, int[] count_max, int[] chances, L2Player player) {
		int[] counts = count_min;
		for (int i = 0; i < count_min.length; i++)
			counts[i] = Rnd.get(count_min[i], count_max[i]);
		return mass_extract_item_r(source_count, list, counts, chances, player);
	}

	public static GArray<int[]> mass_extract_item_r(long source_count, int[] list, int[] counts, int[] chances, L2Player player) {
		if (player == null)
			return new GArray<int[]>(0);

		GArray<int[]> result = new GArray<int[]>((int) Math.min(list.length, source_count));

		int sum = 0;
		for (int i = 0; i < list.length; i++)
			sum += chances[i];

		int[] table = new int[sum];
		int k = 0;

		for (int i = 0; i < list.length; i++)
			for (int j = 0; j < chances[i]; j++) {
				table[k] = i;
				k++;
			}

		for (int n = 1; n <= source_count; n++) {
			int i = table[Rnd.get(table.length)];
			int item = list[i];
			int count = counts[i];

			int[] old = null;
			for (int[] res : result)
				if (res[0] == item)
					old = res;

			if (old == null)
				result.add(new int[]{item, count});
			else
				old[1] += count;
		}

		return result;
	}

	private static boolean canBeExtracted(int itemId, L2Player player) {
		if (player == null)
			return false;

		if (!player.isQuestContinuationPossible(true)) {
			player.sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
			player.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(itemId));
			return false;
		}
		return true;
	}
}
