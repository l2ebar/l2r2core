package ftGuardNo;

import l2n.commons.configuration.L2Properties;
import l2n.game.model.entity.olympiad.Olympiad;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.logging.Logger;

import static l2n.game.model.entity.olympiad.Olympiad._log;


public class ftConfig {
    public static final Logger _log = Logger.getLogger(ftConfig.class.getName());

    public static final String FIRST_GUARD_FILE = "config/FirstGuard.ini";

    public static boolean ALLOW_GUARD_SYSTEM;
    public static boolean SHOW_PROTECTION_INFO_IN_CLIENT;
    public static boolean SHOW_NAME_SERVER_IN_CLIENT;
    public static boolean SHOW_ONLINE_IN_CLIENT;
    public static boolean SHOW_SERVER_TIME_IN_CLIENT;
    public static boolean SHOW_REAL_TIME_IN_CLIENT;
    public static boolean SHOW_PING_IN_CLIENT;
    public static long TIME_REFRESH_SPECIAL_STRING;
    public static int PositionXProtectionInfoInClient;
    public static int PositionYProtectionInfoInClient;
    public static String NameServerInfoInClient;
    public static int PositionXNameServerInfoInClient;
    public static int PositionYNameServerInfoInClient;
    public static int PositionXOnlineInClient;
    public static int PositionYOnlineInClient;
    public static int PositionXServerTimeInClient;
    public static int PositionYServerTimeInClient;
    public static int PositionXRealTimeInClient;
    public static int PositionYRealTimeInClient;
    public static int PositionXPingInClient;
    public static int PositionYPingInClient;
    public static int GET_CLIENT_HWID;

    //public static String GUARD_VERSION;
    //public static boolean GUARD_ENABLE_DATABASE_LOG;
    //public static int GUARD_LOG_SERVER_ID;
    //public static String DATABASE_DATASOURCE;
    private static final File CONFIG_FILE = new File(FIRST_GUARD_FILE);
    private static final AtomicLong lastModified = new AtomicLong(CONFIG_FILE.lastModified());

    public static final void load() {
        _log.info("Loading First Guard configuration...");
        L2Properties guardSettings = load(FIRST_GUARD_FILE);

        //DEBUG = Boolean.parseBoolean(properties.getProperty("Debug", "False"));

        ALLOW_GUARD_SYSTEM = guardSettings.getBoolean("AllowGuardSystem", false);
        SHOW_PROTECTION_INFO_IN_CLIENT = guardSettings.getBoolean("ShowProtectionInfoInClient", false);
        SHOW_NAME_SERVER_IN_CLIENT = guardSettings.getBoolean("ShowNameServerInfoInClient", false);
        SHOW_ONLINE_IN_CLIENT = guardSettings.getBoolean("ShowOnlineInClient", false);
        SHOW_SERVER_TIME_IN_CLIENT = guardSettings.getBoolean("ShowServerTimeInClient", false);
        SHOW_REAL_TIME_IN_CLIENT = guardSettings.getBoolean("ShowRealTimeInClient", false);
        SHOW_PING_IN_CLIENT = guardSettings.getBoolean("ShowPingInClient", false);
        TIME_REFRESH_SPECIAL_STRING = guardSettings.getLong("TimeRefreshStringToClient", 1000);
        NameServerInfoInClient = guardSettings.getProperty("NameServerInfoInClient", "Test");
        PositionXProtectionInfoInClient = guardSettings.getInteger("PositionXProtectionInfoInClient", 320);
        PositionYProtectionInfoInClient = guardSettings.getInteger("PositionYProtectionInfoInClient", 10);
        PositionXNameServerInfoInClient = guardSettings.getInteger("PositionXNameServerInfoInClient", 320);
        PositionYNameServerInfoInClient = guardSettings.getInteger("PositionYNameServerInfoInClient", 25);
        PositionXOnlineInClient = guardSettings.getInteger("PositionXOnlineInClient", 320);
        PositionYOnlineInClient = guardSettings.getInteger("PositionYOnlineInClient", 40);
        PositionXServerTimeInClient = guardSettings.getInteger("PositionXServerTimeInClient", 320);
        PositionYServerTimeInClient = guardSettings.getInteger("PositionYServerTimeInClient", 55);
        PositionXRealTimeInClient = guardSettings.getInteger("PositionXRealTimeInClient", 320);
        PositionYRealTimeInClient = guardSettings.getInteger("PositionYRealTimeInClient", 70);
        PositionXPingInClient = guardSettings.getInteger("PositionXPingInClient", 320);
        PositionYPingInClient = guardSettings.getInteger("PositionYPingInClient", 85);
        GET_CLIENT_HWID = guardSettings.getInteger("UseClientHWID", 2);
    }

    public static L2Properties load(String filename) {
        return load(new File(filename));
    }

    public static L2Properties load(File file) {
        L2Properties result = new L2Properties(file.getName());
        return result;
    }

    public static void reload() {
        long modified = CONFIG_FILE.lastModified();

        if (lastModified.getAndSet(modified) == modified) {
            return;
        }
        load();
    }
}