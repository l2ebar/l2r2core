package ftGuardNo.network.l2.s2c;

import l2n.game.network.serverpackets.L2GameServerPacket;

public final class GameGuardQuery extends L2GameServerPacket
{
	@Override
	protected final void writeImpl()
	{}
	
	@Override
	public String getType()
	{
		return "[S] 74 GameGuardQuery";
	}
}