package ftGuardNo.network.l2.c2s;

import l2n.game.network.clientpackets.L2GameClientPacket;

public class GameGuardReply extends L2GameClientPacket
{


	@Override
	protected void readImpl()
	{}

	@Override
	protected void runImpl()
	{}

	@Override
	public String getType()
	{
		return "[C] CB GameGuardReply";
	}
}