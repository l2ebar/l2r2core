package ftGuardNo;

import ftGuard.ftConfig;
import ftGuard.utils.Util;

import java.util.logging.Logger;

public class ftGuard
{
	public static final Logger _log = Logger.getLogger(ftConfig.class.getName());

	public static void Init()
	{
		ftConfig.load();
		_log.info("************[ Protection System: Start Loading ]*************");
		_log.info("************[ Protection System: Off ]*************");
		_log.info("************[ Protection System: Finish Loading ]*************");
	}

	public static boolean isProtectionOn()
	{
		return false;
	}

	public static String getHwid(String hwid)
	{
		return Util.asHwidString(hwid);
	}

	public static byte[] getKey(byte[] key)
	{
		return key;
	}
}