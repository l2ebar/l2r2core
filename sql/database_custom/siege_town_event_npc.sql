-- Список NPC которые используются по умолчанию в ивенте 'Осада города'

DELETE FROM npc WHERE id BETWEEN 60000 AND 60018;
DELETE FROM npcskills WHERE npcid BETWEEN 60000 AND 60018;

INSERT INTO `npc` VALUES
(60000,18696,'Monster Portal','','LineageNpcEV.teleport_gate_of_legion','44.50','7.00',85,'female','ImmuneMonster','npc',40,21338,95.726,2050,3.000,40,43,30,21,20,20,0,0,2462,3286,1146,511,253,0,500,0,0,0,1,1,'siege_clan',2000,0,0,7,30,4,4,0,0,0.60,0,'NONE',0,0),
 -- raid boss
(60001,25539,'Typhoon','Siege Raid Boss','LineageMonster3.Death_Blader_Raid_sand','15.00','32.50',84,'male','L2RaidBoss','npc',40,1378432,335.274,2011,3.000,60,57,73,76,70,80,5678637,1283185,11447,2433,10558,2057,253,500,3819,0,0,0,60,180,'siege_clan',300,0,0,7,30,4,20,0,0,0.37,0,'SOUL_BSPIRIT',0,0),
(60002,25647,'Awakened Ancient Fighter','Siege Raid Boss','LineageMonster4.death_knight_n_156p','34.32','62.42',81,'male','L2RaidBoss','npc',80,440658,188.169,1847,3.000,60,57,73,76,70,80,3100452,410692,3138,1889,1876,921,253,500,500,13982,0,0,73,180,'siege_clan',300,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
(60003,25648,'Awakened Ancient Fighter','Siege Raid Boss','LineageMonster4.death_knight_n_156p','34.32','62.42',81,'male','L2RaidBoss','npc',80,440658,188.169,1847,3.000,60,57,73,76,70,80,3100452,410692,3138,1889,1876,921,253,500,500,13983,0,0,73,180,'siege_clan',300,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
(60004,25649,'Awakened Ancient Executor','Siege Raid Boss','LineageMonster4.Death_slayer_120p','24.00','40.79',81,'male','L2RaidBoss','npc',40,440658,188.169,1847,3.000,60,57,73,76,70,80,3100452,410692,3138,1889,1876,921,253,500,500,13985,13986,0,80,200,'siege_clan',300,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
(60005,25650,'Awakened Ancient Executor','Siege Raid Boss','LineageMonster4.Death_slayer_120p','24.00','40.79',81,'male','L2RaidBoss','npc',40,440658,188.169,1847,3.000,60,57,73,76,70,80,3100452,410692,3138,1889,1876,921,253,500,500,13984,0,0,80,200,'siege_clan',300,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
(60006,25651,'Awakened Ancient Prophet','Siege Raid Boss','LineageMonster4.lich_A_132p','31.68','60.72',81,'male','L2RaidBoss','npc',40,440658,188.169,1847,3.000,60,57,73,76,70,80,3100452,410692,3138,1889,1876,921,253,500,500,0,0,0,46,200,'siege_clan',300,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
(60007,25652,'Awakened Ancient Prophet','Siege Raid Boss','LineageMonster4.lich_b_132p','31.68','60.72',81,'male','L2RaidBoss','npc',40,440658,188.169,1847,3.000,60,57,73,76,70,80,3100452,410692,3138,1889,1876,921,253,500,500,0,0,0,46,200,'siege_clan',300,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
(60008,25660,'Kanadis Guide','Siege Raid Boss','LineageMonster4.pailaka_invader','30.00','33.00',80,'male','L2RaidBoss','npc',40,303374,179.036,1565,3.000,60,57,73,76,70,80,2192267,353878,1386,1180,267,799,253,500,500,0,0,0,60,200,'siege_clan',300,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
(60009,25661,'Kanadis Guide','Siege Raid Boss','LineageMonster4.pailaka_invader','30.00','33.00',89,'male','L2RaidBoss','npc',40,314186,184.461,1674,3.000,60,57,73,76,70,80,2329614,373311,1527,1257,288,851,253,500,500,0,0,0,60,200,'siege_clan',300,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
(60010,25653,'Kanadis Herald','Siege Raid Boss','LineageMonster4.pailaka_invader','30.00','33.00',81,'male','L2RaidBoss','npc',80,327851,180.826,1822,3.000,60,57,73,76,70,80,2571289,385228,2027,1699,324,920,253,500,500,0,0,0,60,200,'siege_clan',300,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
(60011,25654,'Kanadis Herald','Siege Raid Boss','LineageMonster4.pailaka_invader','30.00','33.00',83,'male','L2RaidBoss','npc',40,337881,184.461,1935,3.000,60,57,73,76,70,80,2819565,398177,2137,1794,348,972,253,500,500,0,0,0,60,200,'siege_clan',300,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
(60012,25655,'Kanadis Herald','Siege Raid Boss','LineageMonster4.pailaka_invader','30.00','33.00',85,'male','L2RaidBoss','npc',40,348119,188.169,2050,3.000,60,57,73,76,70,80,3100452,410692,2240,1889,369,1023,253,500,500,0,0,0,60,200,'siege_clan',300,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
 -- monsters
(60013,22538,'Dragon Steed Troop Commander','Siege Forces','LineageMonster4.Dragon_centurion_135p','28.00','46.50',83,'male','L2Monster','Fighter',80,51870,111.507,1973,3.000,40,43,30,21,20,20,96783,10083,11903,974,5534,1186,253,800,500,13978,0,0,55,210,'siege_clan',1000,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
(60014,22540,'White Dragon Leader','Siege Forces','LineageMonster4.Dragon_centurion_120p','22.00','42.00',81,'male','L2Monster','Fighter',40,52146,113.419,1897,3.000,40,43,30,21,20,20,82134,8708,11673,940,5427,1145,253,800,500,13979,13980,0,56,210,'siege_clan',1000,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
(60015,22547,'Dragon Steed Troop Healer','Siege Forces','LineageMonster4.dragon_mage_065p','25.00','34.50',79,'male','L2Monster','Priest',40,52385,115.296,1822,3.000,40,43,30,21,20,20,59035,6215,11354,906,5279,1104,253,800,500,13981,0,0,82,170,'siege_clan',1000,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
(60016,22542,'Dragon Steed Troop Magic Leader','Siege Forces','LineageMonster4.dragon_mage_075p','28.00','39.50',82,'male','L2Monster','Mystic',40,52055,112.558,1935,3.000,40,43,30,21,20,20,193521,21161,11789,957,5477,1166,253,800,500,0,0,0,92,170,'siege_clan',1000,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
(60017,22544,'Dragon Steed Troop Magic Soldier','Siege Forces','LineageMonster4.dragon_mage_065p','25.00','34.50',80,'male','L2Monster','Mystic',40,52288,114.400,1859,3.000,40,43,30,21,20,20,84936,9090,11557,923,5374,1125,253,800,500,13981,0,0,82,170,'siege_clan',1000,0,0,7,30,4,4,0,0,0.60,0,'SOUL_BSPIRIT',0,0),
(60018,29029,'Elderly Lavasaurus','Siege Forces','golem','13.00','75.00',84,'male','L2Monster','Mystic',1100,52288,67.007,1953,3.960,40,43,30,21,20,20,0,0,10652,626,1332,508,253,1200,500,0,0,0,1,1,'siege_clan',300,0,0,7,10,8,4,1500,0,0.48,0,'NONE',0,0);

INSERT INTO `npcskills` VALUES
(60001, 4189, 10), -- Paralysis [id=4189,lvl=10]
(60001, 4416, 7), -- Spirits [id=4416,lvl=7]
(60001, 4494, 1), -- Raid Boss [id=4494,lvl=1]
(60001, 5380, 5), -- NPC(party) -Magic Single Long Range DD - Wind [id=5380,lvl=5]
(60001, 5382, 5), -- NPC(party) -Magic Range Close Range DD - Wind [id=5382,lvl=5]
(60001, 5464, 1), -- Wind Attacks [id=5464,lvl=1]
(60001, 5474, 1), -- Raid Boss - Level 81 [id=5474,lvl=1]
(60002, 4408, 10), -- HP Increase (1x) [id=4408,lvl=10]
(60002, 4409, 10), -- MP Increase (1x) [id=4409,lvl=10]
(60002, 4415, 11), -- Spears [id=4415,lvl=11]
(60002, 4416, 1), -- Undead [id=4416,lvl=1]
(60002, 4494, 1), -- Raid Boss [id=4494,lvl=1]
(60002, 5465, 1), -- Earth Attacks [id=5465,lvl=1]
(60002, 5884, 2), -- Weakened Sweep [id=5884,lvl=2]
(60002, 5887, 1), -- Howl from Beyond [id=5887,lvl=1]
(60002, 5891, 1), -- Threatening Bellow [id=5891,lvl=1]
(60002, 5895, 1), -- Mark of Cowardice [id=5895,lvl=1]
(60002, 5915, 1), -- Mummifying Aura [id=5915,lvl=1]
(60003, 4408, 10), -- HP Increase (1x) [id=4408,lvl=10]
(60003, 4409, 10), -- MP Increase (1x) [id=4409,lvl=10]
(60003, 4415, 13), -- Two-handed Sword [id=4415,lvl=13]
(60003, 4416, 1), -- Undead [id=4416,lvl=1]
(60003, 4494, 1), -- Raid Boss [id=4494,lvl=1]
(60003, 5467, 1), -- Dark Attacks [id=5467,lvl=1]
(60003, 5855, 2), -- Weakened Sweep [id=5855,lvl=2]
(60003, 5890, 3), -- Tainted Mass Shackle [id=5890,lvl=3]
(60003, 5891, 1), -- Threatening Bellow [id=5891,lvl=1]
(60003, 5900, 1), -- Burst of Oblivion [id=5900,lvl=1]
(60003, 5985, 1), -- Decaying Aura [id=5985,lvl=1]
(60004, 4408, 10), -- HP Increase (1x) [id=4408,lvl=10]
(60004, 4409, 10), -- MP Increase (1x) [id=4409,lvl=10]
(60004, 4415, 19), -- Dual-Sword Weapons [id=4415,lvl=19]
(60004, 4416, 1), -- Undead [id=4416,lvl=1]
(60004, 4494, 1), -- Raid Boss [id=4494,lvl=1]
(60004, 5467, 1), -- Dark Attacks [id=5467,lvl=1]
(60004, 5880, 2), -- Vicious Mutilation [id=5880,lvl=2]
(60004, 5894, 1), -- Mark of Despair [id=5894,lvl=1]
(60004, 5899, 2), -- Burst of Pain [id=5899,lvl=2]
(60004, 5986, 1), -- Vampiric Aura [id=5986,lvl=1]
(60005, 4408, 10), -- HP Increase (1x) [id=4408,lvl=10]
(60005, 4409, 10), -- MP Increase (1x) [id=4409,lvl=10]
(60005, 4415, 3), -- One-handed Sword [id=4415,lvl=3]
(60005, 4416, 1), -- Undead [id=4416,lvl=1]
(60005, 4494, 1), -- Raid Boss [id=4494,lvl=1]
(60005, 5465, 1), -- Earth Attacks [id=5465,lvl=1]
(60005, 5881, 2), -- Vicious Mutilation [id=5881,lvl=2]
(60005, 5888, 2), -- Earth Shaker [id=5888,lvl=2]
(60005, 5894, 1), -- Mark of Despair [id=5894,lvl=1]
(60005, 5913, 1), -- Putrifection Cleanse [id=5913,lvl=1]
(60006, 4408, 10), -- HP Increase (1x) [id=4408,lvl=10]
(60006, 4409, 10), -- MP Increase (1x) [id=4409,lvl=10]
(60006, 4415, 3), -- One-handed Sword [id=4415,lvl=3]
(60006, 4416, 1), -- Undead [id=4416,lvl=1]
(60006, 4494, 1), -- Raid Boss [id=4494,lvl=1]
(60006, 5467, 1), -- Dark Attacks [id=5467,lvl=1]
(60006, 5898, 2), -- Blast of Anger [id=5898,lvl=2]
(60006, 5904, 2), -- Mist of Souliter [id=5904,lvl=2]
(60006, 5907, 2), -- Ritual of Entombment [id=5907,lvl=2]
(60006, 5916, 1), -- Cruising Aura [id=5916,lvl=1]
(60007, 4408, 10), -- HP Increase (1x) [id=4408,lvl=10]
(60007, 4409, 10), -- MP Increase (1x) [id=4409,lvl=10]
(60007, 4415, 3), -- One-handed Sword [id=4415,lvl=3]
(60007, 4416, 1), -- Undead [id=4416,lvl=1]
(60007, 4494, 1), -- Raid Boss [id=4494,lvl=1]
(60007, 5465, 1), -- Earth Attacks [id=5465,lvl=1]
(60007, 5896, 2), -- Soulless [id=5896,lvl=2]
(60007, 5901, 2), -- Mist of Oblivion [id=5901,lvl=2]
(60007, 5908, 2), -- Ritual of Entombment [id=5908,lvl=2]
(60007, 5917, 1), -- Shrouding Aura [id=5917,lvl=1]
(60013, 4415, 11), -- Spears [id=4415,lvl=11]
(60013, 4416, 10), -- Dragons [id=4416,lvl=10]
(60013, 5462, 1), -- Fire Attacks [id=5462,lvl=1]
(60013, 5827, 5), -- Fireball [id=5827,lvl=5]
(60013, 5830, 1), -- Polearm Thrust [id=5830,lvl=1]
(60013, 5831, 1), -- Polearm Swing [id=5831,lvl=1]
(60014, 4412, 14), -- Strong P. Def. [id=4412,lvl=14]
(60014, 4413, 15), -- Strong M. Def. [id=4413,lvl=15]
(60014, 4416, 10), -- Dragons [id=4416,lvl=10]
(60014, 5274, 5), -- NPC(party) - Physical Single Long Range Attack [id=5274,lvl=5]
(60014, 5462, 1), -- Fire Attacks [id=5462,lvl=1]
(60014, 5827, 4), -- Fireball [id=5827,lvl=4]
(60014, 5832, 1), -- Hate Aura [id=5832,lvl=1]
(60016, 4411, 13), -- Slightly Strong M. Atk. [id=4411,lvl=13]
(60016, 4416, 10), -- Dragons [id=4416,lvl=10]
(60016, 5312, 5), -- NPC(party) -Magic Single Long Range DD - Fire [id=5312,lvl=5]
(60016, 5462, 1), -- Fire Attacks [id=5462,lvl=1]
(60016, 5827, 4), -- Fireball [id=5827,lvl=4]
(60016, 5829, 5), -- Fire Flare [id=5829,lvl=5]
(60016, 5833, 1), -- Shield [id=5833,lvl=1]
(60017, 4416, 10), -- Dragons [id=4416,lvl=10]
(60017, 5312, 5), -- NPC(party) -Magic Single Long Range DD - Fire [id=5312,lvl=5]
(60017, 5462, 1), -- Fire Attacks [id=5462,lvl=1]
(60017, 5827, 3), -- Fireball [id=5827,lvl=3]
(60017, 5829, 4), -- Fire Flare [id=5829,lvl=4]
(60017, 5834, 1), -- Magic Barrier [id=5834,lvl=1]
(60015, 4416, 10), -- Dragons [id=4416,lvl=10]
(60015, 4608, 2), -- NPC Clan Buff - Berserk [id=4608,lvl=2]
(60015, 5462, 1), -- Fire Attacks [id=5462,lvl=1]
(60015, 5835, 1), -- Major Heal [id=5835,lvl=1]
(60015, 5836, 1); -- Greater Heal [id=5836,lvl=1]
