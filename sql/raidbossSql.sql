ALTER TABLE `l2s`.`epic_boss_spawn`
CHANGE COLUMN `bossId` `bossId` SMALLINT(8) UNSIGNED NOT NULL ;

INSERT INTO `ai_params` VALUES
('99009','Spinozavr','MaxPursueRange','30000'),
(99009,'Spinozavr','isMadness','15'),
('99009','Spinozavr','SelfAggressive','100');


INSERT IGNORE INTO `epic_boss_spawn` (`bossId`,`respawnDate`,`state`,`name`) VALUES
(99009,0,0,'Spinozavr');


INSERT INTO `npcskills` VALUES
(99009, 4045, 1),
(99009, 4416, 3),
(99009, 4494, 1),
(99009, 5117, 1),
(99009, 5118, 1),
(99009, 5122, 1),
(99009, 5143, 1),
(99009, 5180, 1);


INSERT INTO `droplist` VALUES ('99009', '6367', '1', '1', '0', '7558');

INSERT INTO `npc` VALUES
(99009,0,'Spinozavr','Spinozavr Evil Power','spinozavr','100.00','82.10',80,'male','L2Boss','Fighter',40,6890610,264.824,2277,3.210,60,57,73,76,70,80,4569145,592766,6669,2004,2673,1085,253,0,2901,0,0,0,41,185,'0',0,0,0,7,30,4,20,0,-18,0.50,0,'NONE',1,1);
