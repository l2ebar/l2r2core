DROP TABLE IF EXISTS `clan_skills`;
CREATE TABLE `clan_skills` (
  `clan_id` int NOT NULL default 0,
  `skill_id` smallint unsigned NOT NULL default 0,
  `skill_level` tinyint unsigned NOT NULL default 0,
  `skill_name` varchar(26) default NULL,
  `squad_index` SMALLINT NOT NULL DEFAULT '-1',
  PRIMARY KEY  (`clan_id`,`skill_id`)
) ENGINE=MyISAM;
