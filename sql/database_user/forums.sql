DROP TABLE IF EXISTS `forums`;
CREATE TABLE `forums` (
  `forum_id` int(11) NOT NULL DEFAULT '0',
  `forum_name` varchar(255) NOT NULL DEFAULT '',
  `forum_parent` int(11) NOT NULL DEFAULT '0',
  `forum_post` int(11) NOT NULL DEFAULT '0',
  `forum_type` int(11) NOT NULL DEFAULT '0',
  `forum_perm` int(11) NOT NULL DEFAULT '0',
  `forum_owner_id` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `forum_id` (`forum_id`),
  KEY `forum_id_parent` (`forum_id`,`forum_parent`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `forums` VALUES
('1','NormalRoot','0','0','0','1','0'),
('2','ClanRoot','0','0','0','0','0'),
('3','MemoRoot','0','0','0','0','0'),
('4','MailRoot','0','0','0','0','0');